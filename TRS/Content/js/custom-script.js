/*================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
================================================================================

NOTE:
------
PLACE HERE YOUR OWN JS CODES AND IF NEEDED.
WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR CUSTOM SCRIPT IT'S BETTER LIKE THIS. */


var AjaxLoadCnt = 0;

var matched, browser;

$.uaMatch = function (ua) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
        /(webkit)[ \/]([\w.]+)/.exec(ua) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
        /(msie) ([\w.]+)/.exec(ua) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
        [];

    return {
        browser: match[1] || "",
        version: match[2] || "0"
    };
};

matched = $.uaMatch(navigator.userAgent);
browser = {};

if (matched.browser) {
    browser[matched.browser] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if (browser.chrome) {
    browser.webkit = true;
} else if (browser.webkit) {
    browser.safari = true;
}

$.browser = browser;

function FillAutoComplete(element, sourceList, hdElement) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.Value,
                    value: item.Id,
                    id: item.Id
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 2,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
            }
        }
    })
        .focus(function () {
            $(this).autocomplete("search");
        });
}

function copyReferralCodeForShare(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).attr("data-code")).select();
    document.execCommand("copy");
    $temp.remove();
    Materialize.toast('Copied!', 2000)
}
//window.onload = function () {

$(function () {
    $(document).ajaxStart(function () {
        AjaxLoadCnt = AjaxLoadCnt + 1;
        //$('body').removeClass("loaded");
        //$('.section-left').addClass("loader-section-ajax");
        //$('.section-right').addClass("loader-section-ajax");
        ShowLoading();
    });
    $(document).ajaxStop(function () {
        AjaxLoadCnt = AjaxLoadCnt - 1;
        if (AjaxLoadCnt <= 0) {
            //$('body').addClass("loaded");
            //$('.section-left').removeClass("loader-section-ajax");
            //$('.section-right').removeClass("loader-section-ajax");
            HideLoading();
        }
    });
    $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
        AjaxLoadCnt = AjaxLoadCnt - 1;
    });

    $('table.demo1').floatThead({
        position: 'fixed',
        top: 65
    });
    $('table.demo1 thead').css('background', '#ECEFF1');
});

function ShowLoading() {
    $('body').removeClass("loaded");
    $('.section-left').addClass("loader-section-ajax");
    $('.section-right').addClass("loader-section-ajax");
}
function HideLoading() {
    $('body').addClass("loaded");
    $('.section-left').removeClass("loader-section-ajax");
    $('.section-right').removeClass("loader-section-ajax");
}
if (typeof WindowLoadComplete != 'undefined' && typeof WindowLoadComplete == 'function') {
    WindowLoadComplete();
}

//}
function LoadProductDetails(OrderPurchaseHdrId) {
    $.ajax({
        type: "POST",
        url: "/Member/Orders/LoadProductDetails",
        datatype: 'html',
        data: { OrderPurchaseHdrId: OrderPurchaseHdrId },
        success: function (result) {
            $("#dvOrderDetails").html(result);
            $("#mdlProductDetails").modal('open');
        },
        error: function () {
        }
    });
}
function LoadPointProductDetails(PointsPurchaseHdrId) {
    $.ajax({
        type: "POST",
        url: "/Member/Orders/LoadPointProductDetails",
        datatype: 'html',
        data: { PointsPurchaseHdrId: PointsPurchaseHdrId },
        success: function (result) {
            $("#dvOrderDetails").html(result);
            $("#mdlProductDetails").modal('open');
        },
        error: function () {
        }
    });
}

var IsValidModalDetails = false;
function GetPayoutDetails(UserId, url) {
    if (IsUptoDate == null && IsUptoDate == undefined) {
        IsUptoDate = true;
    }
    if (url == null && url == undefined) {
        url = "/Admin/Payout/GetPayoutDetails";
    }
    $.ajax({
        type: "POST",
        url: url,
        datatype: 'html',
        data: { UserId: UserId, IsUptoDate: IsUptoDate },
        success: function (result) {
            $("#dvPayoutDetails").html(result);
            $("#mdlPayoutDetails").modal({
                dismissible: false,
                complete: function (e) {
                    if (IsValidModalDetails) {
                        $("#dvPayoutDetails").html("");
                    }
                    else {
                        //e.preventDefault();
                        GetPayoutDetails(UserId, url);
                    }
                }
            });
            $("#mdlPayoutDetails").modal('open');
            window.setTimeout(function () {
                $(".datetextbox").datepicker({ dateFormat: "dd/mm/yy" });
            }, 500);

        },
        error: function () {
        }
    });
}

function GetSelectedProductsListList(cart) {
    var url = "/Utility/General/GetSelectedProductsListList";
    $.ajax({
        type: "POST",
        url: url,
        datatype: 'html',
        data: { cart: cart },
        success: function (result) {
            $("#dvOrderDetails").html(result);
            $("#mdlProductDetails").modal('open');
        },
        error: function () {
        }
    });
}


function UpdateResponse(btn, QuerySupportId) {
    var $tr = $(btn).closest("tr");
    var Response = $tr.find('.Response').html();
    swal({
        title: "Response Query",
        type: "input", showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Write something"
    },
        function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to enter response!");
                return false
            }
            $.ajax({
                url: "/Admin/Support/UpdateResponse",
                type: 'POST',
                data: {
                    Response: inputValue,
                    QuerySupportId: QuerySupportId
                },
                cache: false,
                success: function (data) {
                    if (data.Key) {
                        if (typeof BindQuerySupportList !== 'undefined' && typeof BindQuerySupportList === 'function') {
                            BindQuerySupportList();
                        }
                        swal("Success!", data.Msg, "success");
                    }
                    else {
                        if (data.IsInfo) {
                            swal("Information!", data.Msg, "info");
                        }
                        else {
                            swal("Error!", data.Msg, "error");
                        }
                    }
                }
            });
            //swal("Nice!", "You wrote: " + inputValue, "success");
        });

    window.setTimeout(function () {
        $('.sweet-alert').find('input').val(Response);
    }, 500);
}

function ResolveQuery(QuerySupportId) {
    swal({
        title: "Are you sure want to resolve query?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: "/Admin/Support/ResolveQuery",
                type: 'POST',
                data: {
                    QuerySupportId: QuerySupportId
                },
                cache: false,
                success: function (data) {
                    if (data.Key) {
                        if (typeof BindQuerySupportList !== 'undefined' && typeof BindQuerySupportList === 'function') {
                            BindQuerySupportList();
                        }
                        else {
                            $("[data-id=" + QuerySupportId + "]").hide()
                        }
                        swal("Success!", data.Msg, "success");
                    }
                    else {
                        if (data.IsInfo) {
                            swal("Information!", data.Msg, "info");
                        }
                        else {
                            swal("Error!", data.Msg, "error");
                        }
                    }
                }
            });
        });
}

function GetConfirmProductsList(cart, UserAddressDtlId, IsCOD) {
    var url = "/Utility/General/GetConfirmProductsList";
    $.ajax({
        type: "POST",
        url: url,
        datatype: 'html',
        data: { cart: cart, UserAddressDtlId: UserAddressDtlId, IsCOD: IsCOD },
        success: function (result) {
            $("#dvConfirmOrderDetails").html(result);
            $("#mdlConfirmProductDetails").modal('open');
        },
        error: function () {
        }
    });
}

