GO
CREATE Function [dbo].[FN_GET_USER_MATCHING_BV_WITHOUT_CF]  
(  
@UserId int,  
@FromDate date=null,  
@ToDate date=null  
)  
RETURNS float  
AS  
BEGIN  
   
 Declare @MatchingBV float  
 Declare @RightBV float  
 Declare @LeftBV float  
    
 Select @RightBV=SUM(RightBV) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,@FromDate,@ToDate)  
 Select @LeftBV=SUM(LeftBV) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,@FromDate,@ToDate)  
   
 SET @MatchingBV=(case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END)  
  
 RETURN ISNULL(@MatchingBV,0)  

END

GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
		object_id = OBJECT_ID(N'PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH') 
		AND type in (N'P', N'PC'))
	DROP PROCEDURE PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH
GO
CREATE PROCEDURE [dbo].[PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH]    
(    
@Month int,    
@Year int     
)    
AS    
BEGIN    
    
SET FMTONLY OFF    
    
Declare @Date datetime=cast((cast(@Year as nvarchar(10))+'-'+cast(@Month as nvarchar(10))+'-01') as date)    
    
Declare @BonanzaOfferCnt float    
Declare @BonanzaUserBVPreviousMonths float    
    
SELECT @BonanzaOfferCnt=value FROM ConfigMst Where [Key]='BonanzaUserBV'    
SELECT @BonanzaUserBVPreviousMonths=value FROM ConfigMst Where [Key]='BonanzaUserBVPreviousMonths'    

Declare @FromDate datetime    
Declare @ToDate datetime    
    
Select @FromDate=StartDate,@ToDate=EndDate from dbo.fn_GetFirstLastDateOfMonthByDate(@Date)    
    
SELECT *     
into #Previous3rdMonth    
FROM (    
 SELECT *,dbo.FN_GET_USER_MATCHING_BV_WITHOUT_CF(UserId,DATEADD(M,-3,@FromDate),DATEADD(M,-3,@ToDate)) as Previous3rdMonthBV     
 FROM(    
  SELECT UserId    
  FROM UserMst Where (IsSilverMember=1 or IsGoldMember=1 or IsDimondMember=1) 
 ) as u    
) as final    
Where Previous3rdMonthBV>=@BonanzaUserBVPreviousMonths    
    
SELECT *     
into #Previous2rdMonth    
FROM (    
 SELECT UserId,Previous3rdMonthBV,dbo.FN_GET_USER_MATCHING_BV_WITHOUT_CF(UserId,DATEADD(M,-2,@FromDate),DATEADD(M,-2,@ToDate)) as Previous2rdMonthBV    
 FROM #Previous3rdMonth    
) as final    
Where Previous2rdMonthBV>=@BonanzaUserBVPreviousMonths    
    
    
SELECT *     
into #PreviousMonth    
FROM (    
 SELECT UserId,Previous3rdMonthBV,Previous2rdMonthBV,dbo.FN_GET_USER_MATCHING_BV_WITHOUT_CF(UserId,DATEADD(M,-1,@FromDate),DATEADD(M,-1,@ToDate)) as PreviousMonthBV    
 FROM #Previous2rdMonth    
) as final    
Where PreviousMonthBV>=@BonanzaUserBVPreviousMonths    
    
    
SELECT final.*,um.RefferalCode,um.FullName,    
  CASE WHEN ISNULL(final.CurrentMonthLeftBV,0) >= ISNULL(final.CurrentMonthRightBV,0)    
   THEN ISNULL(final.CurrentMonthRightBV,0)    
   ELSE ISNULL(final.CurrentMonthLeftBV,0)    
  END as CurrentMonthBV,    
  cast(0 as bigint) as Amount    
FROM (    
SELECT *,    
 (Select SUM(LeftBV) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(UserId,@FromDate,@ToDate)) as CurrentMonthLeftBV,    
 (Select SUM(RightBV) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(UserId,@FromDate,@ToDate)) as CurrentMonthRightBV    
FROM #PreviousMonth    
) as final    
INNER JOIN UserMst as um on final.UserId = um.UserId    
Where CurrentMonthLeftBV>=@BonanzaOfferCnt AND     
  CurrentMonthRightBV>=@BonanzaOfferCnt    
    
DROP TABLE #Previous3rdMonth    
DROP TABLE #Previous2rdMonth    
DROP TABLE #PreviousMonth 

END  

GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
		object_id = OBJECT_ID(N'PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH') 
		AND type in (N'P', N'PC'))
	DROP PROCEDURE PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH
GO
CREATE PROCEDURE [dbo].[PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH]    
(    
@Month int,    
@Year int
)    
AS    
BEGIN    
    
SET FMTONLY OFF    
    
Declare @Date datetime=cast((cast(@Year as nvarchar(10))+'-'+cast(@Month as nvarchar(10))+'-01') as date)    
    
Declare @CompanyTurnOverUserBV float    
    
SELECT @CompanyTurnOverUserBV=value FROM ConfigMst Where [Key]='CompanyTurnOverUserBV'    
    
Declare @FromDate datetime    
Declare @ToDate datetime    
    
Select @FromDate=StartDate,@ToDate=EndDate from dbo.fn_GetFirstLastDateOfMonthByDate(@Date)    
    
SELECT final.*,cast(0 as float) as Previous3rdMonthBV,cast(0 as float) as Previous2rdMonthBV,cast(0 as float) as PreviousMonthBV,    
  CASE WHEN ISNULL(final.CurrentMonthLeftBV,0)>=ISNULL(final.CurrentMonthRightBV,0)    
   THEN ISNULL(final.CurrentMonthRightBV,0)    
   ELSE ISNULL(final.CurrentMonthLeftBV,0)    
  END as CurrentMonthBV,
  cast(0 as bigint) as Amount    
FROM (    
SELECT *,    
 (Select SUM(LeftBV) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(UserId,@FromDate,@ToDate)) as CurrentMonthLeftBV,    
 (Select SUM(RightBV) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(UserId,@FromDate,@ToDate)) as CurrentMonthRightBV    
FROM (SELECT UserId,RefferalCode,FullName FROM UserMst Where IsDimondMember=1 AND DimondMemberDate <= @ToDate) as a    
) as final    
Where CurrentMonthLeftBV>=@CompanyTurnOverUserBV AND     
  CurrentMonthRightBV>=@CompanyTurnOverUserBV    
    
END    
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM ServiceRunningStatus WHERE [ServiceName] = 'GenerateIncomeProcess')
BEGIN
INSERT INTO [dbo].[ServiceRunningStatus]
           ([ServiceName]
           ,[RunningStatus]
           ,[IsActive])
     VALUES
           ('GenerateIncomeProcess'
           ,NULL
           ,1)
END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
		object_id = OBJECT_ID(N'PROC_GENERATE_UPLINE_MONTHLY_INCOME') 
		AND type in (N'P', N'PC'))
	DROP PROCEDURE PROC_GENERATE_UPLINE_MONTHLY_INCOME
GO
CREATE PROCEDURE [dbo].[PROC_GENERATE_UPLINE_MONTHLY_INCOME]        
(        
 @Month int         
 ,@Year int        
)        
AS                                      
BEGIN               
       
 If(OBJECT_ID('tempdb..#GoldUsers') Is Not Null)                              
 BEGIN                              
 Drop Table #GoldUsers                     
      
 End                        
                
 Declare @SuperGoldIncome int = 75000         
   ,@StartDt DATE =  CAST(DATEADD(month,@Month-1,DATEADD(year,@Year-1900,0)) AS DATE) /*First Date*/        
     ,@EndDt DATE = CAST(DATEADD(day,-1,DATEADD(month,@Month,DATEADD(year,@Year-1900,0))) AS DATE) /*Last Date*/        
     ,@bugetPerc INT = 10        
     ,@MinQualificationMatchingBV INT = 2500        
          
 IF EXISTS (SELECT TOP 1 1 FROM [dbo].[UplineIncomeSummary] u Inner join UplineIncomeDetail ud ON ud.UplineIncomeSummaryId = u.UplineIncomeSummaryId   
               WHERE   
                u.[Month] = DATEPART(MONTH,@StartDt)   
                AND u.[Year] = DATEPART(YEAR,@StartDt)  
                AND ud.IsPayout = 1)        
 BEGIN        
 RETURN;  
 END        
        
 DECLARE @GoldUsers Table(UserId int, MatchingIncome numeric(18,2) NULL,LeftIncome numeric(18,2) NULL,RightIncome numeric(18,2) NULL)              
        
 Declare @PrimeUsers Table(UserId int,         
       MatchingIncome numeric(18,2) NULL,        
       LeftIncome numeric(18,2) NULL,        
       RightIncome numeric(18,2) NULL,        
       BVMultiplier numeric(18,5) NULL,        
       UplineIncome  numeric(18,2) NULL)        
        
 Insert into  @GoldUsers (UserId)        
 SELECT UserId FROM UserMst(NOLOCK) WHERE IsGoldMember = 1 AND (IsDimondMember IS NULL OR IsDimondMember = 0)      
        
 WHILE EXISTS (SELECT 1 FROM @GoldUsers WHERE MatchingIncome IS NULL)                                
 BEGIN          
         
  DECLARE @UserId int        
  SELECT TOP 1 @UserId = UserId FROM @GoldUsers WHERE MatchingIncome IS NULL        
        
  Declare @TotalRightPoints numeric(18,2) = 0,         
    @TotalLeftPoints numeric(18,2) = 0,        
    @FinalPoints numeric(18,2) = 0          
            
  Select @TotalLeftPoints = ISNULL(SUM(LeftPoints),0) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,@StartDt,@EndDt)          
  Select @TotalRightPoints = ISNULL(SUM(RightPoints),0) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,@StartDt,@EndDt)          
            
  SET @TotalLeftPoints = ISNULL(@TotalLeftPoints,0)          
  SET @TotalRightPoints = ISNULL(@TotalRightPoints,0)          
  SET @FinalPoints = case when @TotalLeftPoints>=@TotalRightPoints then @TotalRightPoints else @TotalLeftPoints end          
        
  UPDATE @GoldUsers SET MatchingIncome = @FinalPoints, LeftIncome = @TotalLeftPoints, RightIncome = @TotalRightPoints        
  WHERE UserId = @UserId        
        
 END        
        
 Declare @TotalMatchingIncome Decimal(18,2)        
     ,@Buget Decimal(18,2)        
             
 SELECT @TotalMatchingIncome = SUM(MatchingIncome),@Buget = (SUM(MatchingIncome) * @bugetPerc)/100  FROM @GoldUsers        
        
 Insert into @PrimeUsers(UserId)        
 SELECT UserId FROM UserMst(NOLOCK) WHERE IsPrimeMember = 1 AND (      
                 (IsGoldMember IS NULL OR IsGoldMember = 0)      
                 AND (IsSilverMember IS NULL OR IsSilverMember = 0)      
                 AND (IsDimondMember IS NULL OR IsDimondMember = 0))      
        
 WHILE EXISTS (SELECT 1 FROM @PrimeUsers WHERE MatchingIncome IS NULL)                                
 BEGIN          
         
  SET @UserId = 0        
  SELECT TOP 1 @UserId = UserId FROM @PrimeUsers WHERE MatchingIncome IS NULL        
        
  SET @TotalRightPoints = 0         
  SET @TotalLeftPoints = 0         
  SET @FinalPoints = 0         
         
  Select @TotalLeftPoints = ISNULL(SUM(LeftPoints),0) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,@StartDt,@EndDt)          
  Select @TotalRightPoints = ISNULL(SUM(RightPoints),0) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,@StartDt,@EndDt)          
            
  SET @TotalLeftPoints = ISNULL(@TotalLeftPoints,0)          
  SET @TotalRightPoints = ISNULL(@TotalRightPoints,0)          
  SET @FinalPoints = case when @TotalLeftPoints>=@TotalRightPoints then @TotalRightPoints else @TotalLeftPoints end          
        
  UPDATE @PrimeUsers SET MatchingIncome = @FinalPoints, LeftIncome = @TotalLeftPoints, RightIncome = @TotalRightPoints        
  WHERE UserId = @UserId        
        
 END        
        
 Declare @TotalBVQualifier Decimal(18,2)        
     ,@BVMultiplier Decimal(18,5)        
      
     ,@UplineIncomeSummaryId BIGINT        
        
 SELECT @TotalBVQualifier = SUM(MatchingIncome) FROM @PrimeUsers WHERE MatchingIncome >= @MinQualificationMatchingBV        
        
 SET @BVMultiplier = CAST(@Buget/@TotalBVQualifier AS Decimal(18,5))        
        
 IF NOT EXISTS (SELECT TOP 1 1 FROM UplineIncomeSummary WHERE [Month] = DATEPART(MONTH,@StartDt) AND [Year] = DATEPART(YEAR,@StartDt))  
 BEGIN  
  
 INSERT INTO [dbo].[UplineIncomeSummary]   
        ([Month]        
        ,[Year]        
        ,[TotalMatchingIncome]        
        ,[UplineBugetIncome]        
        ,[TotalBVQualifier]        
        ,[BVMultiplier]        
  ,[CreatedDate])        
   VALUES        
    (DATEPART(MONTH,@StartDt)        
    ,DATEPART(YEAR,@StartDt)        
    ,@TotalMatchingIncome        
    ,@Buget        
    ,@TotalBVQualifier        
    ,@BVMultiplier        
    ,GETDATE())        
 SET @UplineIncomeSummaryId = SCOPE_IDENTITY()   
   
 END  
 ELSE  
 BEGIN  
   
 SELECT @UplineIncomeSummaryId = UplineIncomeSummaryId FROM UplineIncomeSummary WHERE [Month] = DATEPART(MONTH,@StartDt) AND [Year] = DATEPART(YEAR,@StartDt)  
  
 UPDATE [UplineIncomeSummary] SET   
   [TotalMatchingIncome] = @TotalMatchingIncome     
        ,[UplineBugetIncome] = @Buget    
        ,[TotalBVQualifier] = @TotalBVQualifier  
        ,[BVMultiplier] = @BVMultiplier  
  ,UpdatedDate = GETDATE()  
  WHERE  
  UplineIncomeSummaryId = @UplineIncomeSummaryId  
  
 END  
        
 MERGE [UplineIncomeDetail] AS Target  
 USING @PrimeUsers AS Source  
 ON Source.userid = Target.userid AND @UplineIncomeSummaryId = Target.UplineIncomeSummaryId  
      
 -- For Inserts  
 WHEN NOT MATCHED BY Target THEN  
 INSERT ([UplineIncomeSummaryId]        
  ,[UserId]        
 ,[MatchingBVInMonth]        
 ,[UplineIncome]        
 ,[CreatedDate]      
 ,[IsQualified])   
 VALUES (@UplineIncomeSummaryId,Source.[UserId], Source.MatchingIncome,(CASE WHEN (Source.MatchingIncome >= @MinQualificationMatchingBV) THEN CAST(Source.MatchingIncome * @BVMultiplier AS Decimal(18,5)) ELSE 0 END)  
 ,GETDATE()      
 ,(CASE WHEN (Source.MatchingIncome >= @MinQualificationMatchingBV) THEN 1 ELSE 0 END))  
      
 -- For Updates  
 WHEN MATCHED THEN UPDATE SET  
   Target.[MatchingBVInMonth] = Source.MatchingIncome        
  ,Target.[UplineIncome] = (CASE WHEN (Source.MatchingIncome >= @MinQualificationMatchingBV) THEN CAST(Source.MatchingIncome * @BVMultiplier AS Decimal(18,5)) ELSE 0 END)      
  ,Target.[IsQualified] = (CASE WHEN (Source.MatchingIncome >= @MinQualificationMatchingBV) THEN 1 ELSE 0 END)  
  ,Target.UpdatedDate = GETDATE();  
     
    
 MERGE UplineIncomeGoldBvDetail AS Target  
 USING @GoldUsers AS Source  
 ON Source.userid = Target.userid AND @UplineIncomeSummaryId = Target.UplineIncomeSummaryId  
  
 -- For Inserts  
 WHEN NOT MATCHED BY Target THEN  
 INSERT  ([UplineIncomeSummaryId]    
     ,[MatchingIncome]    
           ,[LeftIncome]    
           ,[RightIncome]    
           ,[CreatedDate]    
     ,[UserId])   
 VALUES (@UplineIncomeSummaryId,Source.[MatchingIncome], Source.[LeftIncome],Source.[RightIncome], GETDATE(),Source.UserId)  
      
 -- For Updates  
 WHEN MATCHED THEN UPDATE SET  
   Target.MatchingIncome = Source.MatchingIncome        
  ,Target.LeftIncome = Source.MatchingIncome    
  ,Target.RightIncome = Source.RightIncome  
  ,Target.UpdatedDate = GETDATE();  
    
END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
		object_id = OBJECT_ID(N'PROC_GENERATE_RETAIL_REWARD_MONTHLY_INCOME') 
		AND type in (N'P', N'PC'))
	DROP PROCEDURE PROC_GENERATE_RETAIL_REWARD_MONTHLY_INCOME
GO
CREATE PROCEDURE [dbo].[PROC_GENERATE_RETAIL_REWARD_MONTHLY_INCOME]        
(        
 @Month INT     
 ,@Year INT     
)        
AS                                    
BEGIN               
       
  If(OBJECT_ID('tempdb..#tblUsers') Is Not Null)                              
  BEGIN                              
  Drop Table #tblUsers                     
  End    
     
  If(OBJECT_ID('tempdb..#tempTbl') Is Not Null)                              
  BEGIN                              
  Drop Table #tempTbl    
  End    
    
 Declare @StartDt DATE =  CAST(DATEADD(MONTH,@Month-1,DATEADD(YEAR,@Year-1900,0)) AS DATE) /*First Date*/        
    ,@EndDt DATE = CAST(DATEADD(DAY,-1,DATEADD(MONTH,@Month,DATEADD(YEAR,@Year-1900,0))) AS DATE) /*Last Date*/        
    ,@bugetPerc INT = 2        
    ,@bugetBVRsPerc INT = 10    
    ,@MinQualificationSelfPurchaseBV INT = 125        
    ,@MinQualificationDirectTeamPurchaseBV INT = 1500    
    ,@RetailRewardIncomeSummaryId INT     
    
  IF EXISTS (SELECT TOP 1 1 FROM [dbo].[RetailRewardIncomeSummary] u Inner join RetailRewardIncomeDetail ud ON ud.RetailRewardIncomeSummaryId = u.RetailRewardIncomeSummaryId 
															WHERE 
																u.[Month] = DATEPART(MONTH,@StartDt) 
																AND u.[Year] = DATEPART(YEAR,@StartDt)
																AND ud.IsPayout = 1)      
	 BEGIN      
		RETURN;
	 END

  Declare @TotalRepurchaseBV numeric(18,2)    
    ,@RetailRewardBugetIncome numeric(18,2)    
    ,@RetailRewardBugetBV numeric(18,2)    
    ,@TotalBVQualifier numeric(18,2)    
    ,@BVMultiplier numeric(18,5) = 0    
    
  SELECT @TotalRepurchaseBV = SUM(RepurchaseMatchingVolume) FROM UserPurchaseDtl WHERE CreatedDate >= @StartDt AND CreatedDate <= @EndDt    
    
  SET @RetailRewardBugetBV = (@TotalRepurchaseBV * @bugetPerc)/100;    
  SET @RetailRewardBugetIncome = @RetailRewardBugetBV * @bugetBVRsPerc;    
    
  DECLARE @tblUsers Table(UserId int,SelfPurchaseBV numeric(18,2) NULL, DirectTeamPurchaseBV numeric(18,2) NULL, TotalBV numeric(18,2) NULL, RetailRewardIncome numeric(18,2) NULL,IsQualified BIT NULL)              
     
  SELECT o.UserId,ISNULL(SUM(ot.RepurchaseMatchingVolume),0)AS SelfPurchaseBV Into #tempTbl FROM UserPurchaseHdr o Inner join UserPurchaseDtl ot ON o.UserPurchaseHdrId = ot.UserPurchaseHdrId    
  WHERE o.OrderDate >= @StartDt AND o.OrderDate <= @EndDt    
  Group BY UserId    
    
  INSERT INTO @tblUsers (UserId,SelfPurchaseBV,DirectTeamPurchaseBV,TotalBV,IsQualified)    
  SELECT t1.UserId,t1.SelfPurchaseBV,t.DirectTeamPurchaseBV,(t1.SelfPurchaseBV + t.DirectTeamPurchaseBV),    
   (CASE WHEN t1.SelfPurchaseBV > @MinQualificationSelfPurchaseBV AND  DirectTeamPurchaseBV > @MinQualificationDirectTeamPurchaseBV THEN 1 ELSE 0 END)    
  FROM #tempTbl t1     
  OUTER APPLY     
  (    
   SELECT ISNULL(SUM(otInner.RepurchaseMatchingVolume),0)AS DirectTeamPurchaseBV FROM UserPurchaseHdr oInner Inner join UserPurchaseDtl otInner ON oInner.UserPurchaseHdrId = otInner.UserPurchaseHdrId     
     Inner join UserMst u ON u.RefferalUserId = t1.UserId AND oInner.UserId = u.UserId    
   WHERE oInner.OrderDate >= @StartDt AND oInner.OrderDate <= @EndDt    
  )AS t    
     
 SELECT @TotalBVQualifier = ISNULL(SUM(TotalBV),0) FROM @tblUsers WHERE IsQualified = 1    
    
 IF(@TotalBVQualifier > 0)    
 BEGIN    
  SET @BVMultiplier = CAST(@RetailRewardBugetBV/@TotalBVQualifier AS Decimal(18,5))        
 END    
    
 IF NOT EXISTS (SELECT TOP 1 1 FROM RetailRewardIncomeSummary WHERE [Month] = DATEPART(MONTH,@StartDt) AND [Year] = DATEPART(YEAR,@StartDt))
 BEGIN

	INSERT INTO [dbo].[RetailRewardIncomeSummary]    
	   ([Month]    
	   ,[Year]    
	   ,[TotalRepurchaseBV]    
	   ,[RetailRewardBugetIncome]    
	   ,[TotalBVQualifier]    
	   ,[BVMultiplier]    
	   ,[CreatedDate]    
	   ,[IsLock]    
	   ,[RetailRewardBugetBV])    
	VALUES    
	   (@Month    
	   ,@Year    
	   ,@TotalRepurchaseBV    
	   ,@RetailRewardBugetIncome    
	   ,@TotalBVQualifier    
	   ,@BVMultiplier    
	   ,GETDATE()    
	   ,0    
	   ,@RetailRewardBugetBV)  
   
	SELECT @RetailRewardIncomeSummaryId = IDENT_CURRENT('RetailRewardIncomeSummary')    
 
 END

 ELSE 

 BEGIN
	
	SELECT @RetailRewardIncomeSummaryId = RetailRewardIncomeSummaryId FROM RetailRewardIncomeSummary WHERE [Month] = DATEPART(MONTH,@StartDt) AND [Year] = DATEPART(YEAR,@StartDt)

	UPDATE RetailRewardIncomeSummary SET
		[TotalRepurchaseBV] = @TotalRepurchaseBV   
		,[RetailRewardBugetIncome] = @RetailRewardBugetIncome   
		,[TotalBVQualifier]  = @TotalBVQualifier  
		,[BVMultiplier] = @BVMultiplier 
		,[RetailRewardBugetBV] = @RetailRewardBugetBV
		,UpdatedDate = GETDATE()
	WHERE 
		RetailRewardIncomeSummaryId = @RetailRewardIncomeSummaryId

 END
 
	DELETE FROM @tblUsers WHERE UserId NOT IN (
	SELECT UserId FROM @tblUsers    
	WHERE (SelfPurchaseBV > @MinQualificationSelfPurchaseBV OR DirectTeamPurchaseBV > @MinQualificationDirectTeamPurchaseBV))

	MERGE RetailRewardIncomeDetail AS Target
	USING @tblUsers AS Source 
	ON Source.userid = Target.userid AND @RetailRewardIncomeSummaryId = Target.RetailRewardIncomeSummaryId
	
	WHEN NOT MATCHED BY Target THEN
	INSERT  ([RetailRewardIncomeSummaryId]    
           ,[UserId]    
           ,[SelfPurchaseBV]    
           ,[DirectTeamPurchaseBV]    
           ,[TotalBV]    
           ,[RetailRewardIncome]    
           ,[CreatedDate]    
           ,[IsQualified])     
	VALUES (@RetailRewardIncomeSummaryId,Source.[UserId], Source.[SelfPurchaseBV], Source.[DirectTeamPurchaseBV], Source.[TotalBV]
			,(CASE WHEN Source.IsQualified = 1 THEN ((@BVMultiplier * Source.[TotalBV]) * @bugetBVRsPerc) ELSE 0 END)
			,GETDATE()
			,IsQualified)

	-- For Updates
	WHEN MATCHED THEN UPDATE SET
		Target.[SelfPurchaseBV] = Source.[SelfPurchaseBV]     
		,Target.[DirectTeamPurchaseBV] = Source.[DirectTeamPurchaseBV]    
		,Target.[TotalBV] = Source.[TotalBV]   
		,Target.[RetailRewardIncome] = (CASE WHEN Source.IsQualified = 1 THEN ((@BVMultiplier * Source.[TotalBV]) * @bugetBVRsPerc) ELSE 0 END)
		,Target.[IsQualified] = Source.[IsQualified]
		,Target.UpdatedDate = GETDATE();

END
