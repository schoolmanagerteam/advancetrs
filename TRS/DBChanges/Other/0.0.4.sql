USE [AdvanceTRS_03April]
GO
--Select * from dbo.[FN_GET_ADMIN_PROFITE_DETAILS]('2019-07-01','2019-07-31')  
ALTER Function [dbo].[FN_GET_ADMIN_PROFITE_DETAILS]  
(  
@FromDate datetime=null,  
@ToDate datetime=null  
)  
RETURNS @ProfiteDtl table( TotalSales float,TotalCosting float,GrossProfite float,  
       TotalTDS float,TotalAdminCharge float,TotalPayout float,  
       TotalExpense float,Profite float,PendingPayout float,  
       NetProfite float,TotalVolume float,  
       TotalDirectIncomePay float,TotalMatchingIncomePay float,  
       TotalVoltIncomePay float,TotalLeaderIncomePay float,  
       TotalUplineSupportIncomePay float,TotalBonanzaRewardsPay float,  
       TotalTopRetailerPay float,TotalSilverIncomePay float)  
AS  
BEGIN  
  
 Declare @TotalSales float,@TotalCosting float,@GrossProfite float,  
   @TotalTDS float,@TotalAdminCharge float,@TotalPayout float,@TotalExpense float,  
   @Profite float,@PendingPayout float,@NetProfite float,@TotalVolume float,  
   @TotalDirectIncomePay float,@TotalPointsIncomePay float,@TotalVoltIncomePay float,  
   @TotalLeaderIncomePay float,@TotalUplineSupportIncomePay float,@TotalSilverIncomePay float,  
   @TotalBonanzaRewardsPay float,@TotalTopRetailerPay float  
  
 Select @TotalSales=ISNULL(SUM(Amount),0),@TotalCosting=ISNULL(SUM(Qty*RegularPrice),0),  
     @TotalVolume=ISNULL(SUM(ISNULL(DirectPurchaseVolume,0)),0)+ISNULL(SUM(ISNULL(RepurchaseMatchingVolume,0)),0)  
 from UserPurchaseHdr(Nolock) as uph  
 INNER JOIN UserPurchaseDtl(Nolock) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId  
 Where cast(OrderDate as date)>=case when @FromDate is null then cast(OrderDate as date) else cast(@FromDate as date) end   
   and cast(OrderDate as date)<=case when @ToDate is null then cast(OrderDate as date) else cast(@ToDate as date) end   
  
 SET @GrossProfite=ISNULL(@TotalSales,0)-ISNULL(@TotalCosting,0)  
 SET @TotalVolume=ISNULL(@TotalVolume,0)  
  
  
 Select @TotalTDS=ISNULL(SUM(TDSAmount),0),@TotalAdminCharge=ISNULL(SUM(AdminCharge),0),@TotalPayout=ISNULL(SUM(NetPayable),0),  
  @TotalDirectIncomePay=ISNULL(SUM(DirectIncomePay),0),@TotalPointsIncomePay=ISNULL(SUM(PointsIncomePay),0),@TotalVoltIncomePay=ISNULL(SUM(VoltIncomePay),0),  
  @TotalLeaderIncomePay=ISNULL(SUM(LeaderIncomePay),0),@TotalUplineSupportIncomePay=ISNULL(SUM(UplineSupportIncomePay),0),  
  @TotalSilverIncomePay=ISNULL(SUM(SilverIncomePay),0)      
 from WalletDebitDtl(Nolock)  
 where cast(DebitDate as date)>=case when @FromDate is null then cast(DebitDate as date) else cast(@FromDate as date) end   
   and cast(DebitDate as date)<=case when @ToDate is null then cast(DebitDate as date) else cast(@ToDate as date) end   
   
 Select @TotalExpense=ISNULL(SUM(Amount),0) from ExpenseMst(Nolock)  
 Where cast(ExpenseDate as date)>=case when @FromDate is null then cast(ExpenseDate as date) else cast(@FromDate as date) end   
   and cast(ExpenseDate as date)<=case when @ToDate is null then cast(ExpenseDate as date) else cast(@ToDate as date) end   
  
 Select @TotalBonanzaRewardsPay=ISNULL(SUM(PaidAmount),0) from BonanzaRewardDtl(Nolock)  
 Where cast(PaidDate as date)>=case when @FromDate is null then cast(PaidDate as date) else cast(@FromDate as date) end   
   and cast(PaidDate as date)<=case when @ToDate is null then cast(PaidDate as date) else cast(@ToDate as date) end   
     
 Select @TotalTopRetailerPay=ISNULL(SUM(PaidAmount),0) from TopRetailerDtl(Nolock)  
 Where cast(PaidDate as date)>=case when @FromDate is null then cast(PaidDate as date) else cast(@FromDate as date) end   
   and cast(PaidDate as date)<=case when @ToDate is null then cast(PaidDate as date) else cast(@ToDate as date) end   
  
 DECLARE @RewardPaidAmt FLOAT = 0  
 SET @RewardPaidAmt = (  
  SELECT SUM(AmtUsedForRewards)  
  FROM RewardClaimedMst(Nolock)   
  Where IsActive = 1 AND IsApproved = 1  
    AND cast(ApprovalDate as date) >= (case when @FromDate is null then cast(ApprovalDate as date) else cast(@FromDate as date) end)  
    AND cast(ApprovalDate as date) <= (case when @ToDate is null then cast(ApprovalDate as date) else cast(@ToDate as date) end)  
 )  
  
 SET @Profite=@GrossProfite-(@TotalTDS+@TotalAdminCharge+@TotalPayout+@TotalExpense+@TotalBonanzaRewardsPay+@TotalTopRetailerPay + ISNULL(@RewardPaidAmt, 0))  
  
 --SELECT @PendingPayout=ISNULL(SUM(NetPayable),0)  
 --FROM dbo.FN_ADMIN_PENDING_PAYOUT_DETAILS(0)  
 SET @PendingPayout=ISNULL(@PendingPayout,0)  
  
 SET @NetProfite=@Profite-@PendingPayout  
  
 insert into @ProfiteDtl  
 Select @TotalSales as TotalSales,@TotalCosting as TotalCosting,@GrossProfite as GrossProfite,  
   @TotalTDS as TotalTDS,@TotalAdminCharge as TotalAdminCharge,@TotalPayout as TotalPayout,  
   @TotalExpense as TotalExpense,@Profite as Profite,@PendingPayout as PendingPayout,  
   @NetProfite as NetProfite,@TotalVolume as TotalVolume,  
   @TotalDirectIncomePay as TotalDirectIncomePay,@TotalPointsIncomePay as TotalMatchingIncomePay,  
   @TotalVoltIncomePay as TotalVoltIncomePay,@TotalLeaderIncomePay as TotalLeaderIncomePay,  
   @TotalUplineSupportIncomePay as TotalUplineSupportIncomePay,  
   @TotalBonanzaRewardsPay as TotalBonanzaRewardsPay,  
   @TotalTopRetailerPay as TotalTopRetailerPay,@TotalSilverIncomePay as TotalSilverIncomePay  
     
 RETURN  
END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
		object_id = OBJECT_ID(N'PROC_GET_DASHBOARD_SALES_EXPENSE_PROFITE') 
		AND type in (N'P', N'PC'))
	DROP PROCEDURE PROC_GET_DASHBOARD_SALES_EXPENSE_PROFITE
GO
--EXEC [dbo].[PROC_GET_DASHBOARD_SALES_EXPENSE_PROFITE]  
CREATE PROCEDURE [dbo].[PROC_GET_DASHBOARD_SALES_EXPENSE_PROFITE]                      
AS                  
BEGIN                  
   
 IF(OBJECT_ID('tempdb..#tblResult') Is Not Null)                                            
 BEGIN                                            
  Drop Table #tblResult                 
 End                  
  
 CREATE TABLE #tblResult(TotalSales decimal(18,2) NULL  
      ,ActiveMemberCnt INT NULL  
      ,InActiveMemberCnt INT NULL  
      ,ThisMonthDirectSales decimal(18,2) NULL  
      ,ThisMonthRepurchaseSales decimal(18,2) NULL  
      ,ThisMonthSales decimal(18,2) NULL  
      ,ThisWeekSales decimal(18,2) NULL  
      ,ThisWeekDirectSales decimal(18,2) NULL  
      ,ThisWeekRepurchaseSales decimal(18,2) NULL  
      ,ThisWeekExpense decimal(18,2) NULL  
      ,ThisMonthExpense decimal(18,2) NULL  
      ,ThisMonthProfite decimal(18,2) NULL  
      ,ThisWeekProfite decimal(18,2) NULL  
      ,TotalProfit decimal(18,2) NULL  
      ,ThisWeekBV int NULL  
      ,ThisMonthBV int NULL  
      ,ThisWeekDirectBV int NULL  
      ,ThisWeekRepurchaseBV int NULL)  
  
 IF(OBJECT_ID('tempdb..#TblBVDASHBOARD') Is Not Null)                          
 BEGIN                          
  Drop Table #TblBVDASHBOARD        
 End                    
         
 CREATE TABLE #TblBVDASHBOARD(WeekBV Decimal(18,2), MonthBV Decimal(18,2),WeekDirectBV Decimal(18,2),WeekRepurchaseBV Decimal(18,2))  
  
 Insert into #tblResult(TotalSales)  
 SELECT ISNULL(SUM(TotalOrderAmount),0) FROM UserPurchaseHdr(Nolock)
   
 Declare @ActiveMemberCnt INT,  
   @InActiveMemberCnt INT,  
   @WeekStartDt DATETIME,  
   @WeekEndDt DATETIME,  
   @MonthStartDt DATETIME,  
   @MonthEndDt DATETIME,  
   @ThisMonthDirectSales DECIMAL(18,0),  
   @ThisMonthRepurchaseSales DECIMAL(18,0),  
   @ThisMonthSales DECIMAL(18,0),  
   @ThisWeekSales DECIMAL(18,0),  
   @ThisWeekDirectSales DECIMAL(18,0),  
   @ThisWeekRepurchaseSales DECIMAL(18,2),  
   @ThisWeekExpense DECIMAL(18,2),  
   @ThisMonthExpense DECIMAL(18,2),  
   @ThisMonthProfite DECIMAL(18,2),  
   @ThisWeekProfite DECIMAL(18,2),  
   @TotalProfite DECIMAL(18,2)  
  
 SELECT @WeekStartDt= StartDate,@WeekEndDt = EndDate from [fn_GetFirstLastDateOfWeekByDate](dbo.fn_GetServerDate())  
 SELECT @MonthStartDt = StartDate, @MonthEndDt = EndDate from [fn_GetFirstLastDateOfMonthByDate](@WeekStartDt)  
  
 SELECT @ActiveMemberCnt = SUM(CASE WHEN IsRegistrationActivated = 1 THEN 1 ELSE 0 END),  
     @InActiveMemberCnt = SUM(CASE WHEN (IsRegistrationActivated IS NULL OR IsRegistrationActivated = 0) THEN 1 ELSE 0 END) FROM UserMst(Nolock)  
  
   SELECT   
 @ThisMonthDirectSales = SUM(CASE WHEN (IsRepurchaseOrder IS NULL OR IsRepurchaseOrder = 0) THEN TotalOrderAmount ELSE 0 END),  
 @ThisMonthRepurchaseSales = SUM(CASE WHEN IsRepurchaseOrder = 1 THEN TotalOrderAmount ELSE 0 END),  
 @ThisMonthSales = SUM(TotalOrderAmount)  
   FROM UserPurchaseHdr(Nolock) WHERE MONTH(OrderDate) = MONTH(@WeekStartDt) AND YEAR(OrderDate) = YEAR(@WeekStartDt)  
  
   SELECT   
    @ThisWeekSales = SUM(TotalOrderAmount),  
 @ThisWeekDirectSales = SUM(CASE WHEN (IsRepurchaseOrder IS NULL OR IsRepurchaseOrder = 0) THEN TotalOrderAmount ELSE 0 END),  
 @ThisWeekRepurchaseSales = SUM(CASE WHEN IsRepurchaseOrder = 1 THEN TotalOrderAmount ELSE 0 END)  
   FROM UserPurchaseHdr(Nolock) WHERE OrderDate >= @WeekStartDt AND OrderDate <= @WeekEndDt  
  
   SELECT   
  @ThisMonthExpense = SUM(Amount)  
 FROM   
  ExpenseMst(Nolock)  
 WHERE   
  MONTH(ExpenseDate) = MONTH(@WeekStartDt)   
  AND YEAR(ExpenseDate) = YEAR(@WeekStartDt)  
  
 SELECT   
  @ThisWeekExpense = SUM(Amount)  
 FROM   
  ExpenseMst(Nolock)   
 WHERE   
  ExpenseDate >= @WeekStartDt   
  AND ExpenseDate <= @WeekEndDt  
  
 SELECT @ThisWeekProfite = Profite from dbo.[FN_GET_ADMIN_PROFITE_DETAILS](@WeekStartDt,@WeekEndDt)  
 SELECT @ThisMonthProfite = Profite from dbo.[FN_GET_ADMIN_PROFITE_DETAILS](@MonthStartDt,@MonthEndDt)  
 SELECT @TotalProfite = Profite from dbo.[FN_GET_ADMIN_PROFITE_DETAILS](NULL,NULL)  
  
 INSERT INTO #TblBVDASHBOARD  
 EXEC PROC_GET_ADMIN_BV_DETAILS_DASHBOARD  
  
   UPDATE #tblResult SET   
  ThisMonthDirectSales = ISNULL(@ThisMonthDirectSales,0)  
  ,ThisMonthRepurchaseSales = ISNULL(@ThisMonthRepurchaseSales,0)  
  ,ThisMonthSales = ISNULL(@ThisMonthSales,0)  
  ,ThisWeekSales = ISNULL(@ThisWeekSales,0)  
  ,ThisWeekDirectSales = ISNULL(@ThisWeekDirectSales,0)  
  ,ThisWeekRepurchaseSales = ISNULL(@ThisWeekRepurchaseSales,0)  
  ,ThisMonthExpense = ISNULL(@ThisMonthExpense,0)  
  ,ThisWeekExpense = ISNULL(@ThisWeekExpense,0)  
  ,ThisMonthProfite = ISNULL(@ThisMonthProfite,0)  
  ,ThisWeekProfite = ISNULL(@ThisWeekProfite,0)  
  ,TotalProfit = ISNULL(@TotalProfite,0)  
  ,ActiveMemberCnt = ISNULL(@ActiveMemberCnt,0)  
  ,InActiveMemberCnt = ISNULL(@InActiveMemberCnt,0)  
  ,ThisWeekBV = (SELECT TOP 1 ISNULL(WeekBV,0) FROM #TblBVDASHBOARD)  
  ,ThisMonthBV = (SELECT TOP 1 ISNULL(MonthBV,0) FROM #TblBVDASHBOARD)  
  ,ThisWeekDirectBV = (SELECT TOP 1 ISNULL(WeekDirectBV,0) FROM #TblBVDASHBOARD)  
  ,ThisWeekRepurchaseBV = (SELECT TOP 1 ISNULL(WeekRepurchaseBV,0) FROM #TblBVDASHBOARD)  
   
 SELECT TotalSales   
   ,ActiveMemberCnt  
   ,InActiveMemberCnt  
   ,ThisMonthDirectSales  
   ,ThisMonthRepurchaseSales  
   ,ThisMonthSales  
   ,ThisWeekSales  
   ,ThisWeekDirectSales  
   ,ThisWeekRepurchaseSales  
   ,ThisWeekExpense  
   ,ThisMonthExpense  
   ,ThisMonthProfite  
   ,ThisWeekProfite  
   ,TotalProfit  
   ,ThisWeekBV  
   ,ThisMonthBV  
   ,ThisWeekDirectBV  
   ,ThisWeekRepurchaseBV   
 FROM #tblResult  
                           
END     
      
  
  
  