GO
IF COL_LENGTH('dbo.WalletDebitDtl', 'RetailRewardMonthlyPay') IS NULL
BEGIN
	ALTER TABLE WalletDebitDtl
	ADD RetailRewardMonthlyPay Float null;
END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
		object_id = OBJECT_ID(N'PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS') 
		AND type in (N'P', N'PC'))
	DROP PROCEDURE PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS
GO
--EXEC [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS] 246,1  
CREATE PROCEDURE [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]        
(        
@UserId numeric(18,0)=0,        
@IsUptoToday bit=1        
)        
AS        
BEGIN        
        
--SELECT *        
--FROM dbo.FN_ADMIN_PENDING_PAYOUT_DETAILS(@UserId)        
--order by NetPayable desc        
SET FMTONLY OFF        
        
Declare @ToDate date=getdate()        
IF @IsUptoToday=0        
BEGIN        
 Select @ToDate=EndDate from dbo.fn_GetFirstLastDateOfWeekByDate(DATEADD(d,-7,GETDATE()))        
END        
        
Declare @PaymentDtl table(UserId bigint,RefferalCode nvarchar(20),FullName nvarchar(max),PANNo nvarchar(50),BankAccNo nvarchar(50),ISFCCode nvarchar(50)        
       ,DirectIncome numeric(18,0),RePurchaseIncome numeric(18,0),VoltIncome numeric(18,0)        
       ,TotalPointsIncome numeric(18,0),TotalPayout numeric(18,0),TotalIncome numeric(18,0)        
       ,DirectIncomePay numeric(18,0),PointsIncomePay numeric(18,0)        
       ,TDSPerc numeric(18,2),TDS numeric(18,0),AdminChargePerc numeric(18,2),AdminCharge numeric(18,0)        
       ,NetPayable numeric(18,0),VoltIncomePay numeric(18,0),LeaderIncome numeric(18,0)        
       ,UplineSupportIncome numeric(18,0),SilverIncome numeric(18,0),DiamondIncome numeric(18,0),LeaderPointsPay numeric(18,0)        
       ,UplineSupportPointsPay numeric(18,0),SilverPointsPay numeric(18,0),DiamondPointsPay numeric(18,0),IsRegistrationActivated bit,        
       MobileNo numeric(18,0),EmailId nvarchar(max), KYCApproved VARCHAR(5), UplineMonthlyPay numeric(18,0),RetailRewardMonthlyPay numeric(18,0))        
             
 Select um.UserId,um.RefferalCode,um.FullName,um.PANNo,um.BankAccNo,um.ISFCCode,         
   ISNULL(DirectIncome,0)DirectIncome, ISNULL(RePurchaseIncome,0)RePurchaseIncome,ISNULL(VoltIncome,0)VoltIncome,        
   ISNULL(FinalPoints,0)TotalPointsIncome,ISNULL(TotalPayout,0)TotalPayout,        
   ISNULL(DirectIncomePay,0)DirectIncomePay,ISNULL(PointsIncomePay,0)PointsIncomePay,        
   ISNULL(VoltIncomePay,0)VoltIncomePay,ISNULL(leader.CreditPoints,0)FinalLeaderPoints,        
   ISNULL(uplinesupport.CreditPoints,0)FinalUplineSupportPoints,        
   ISNULL(silver.CreditPoints,0)FinalSilverPoints,ISNULL(diamond.CreditPoints,0)FinalDiamondPoints,        
   ISNULL(LeaderIncomePay,0)LeaderIncomePay,ISNULL(UplineSupportIncomePay,0)UplineSupportIncomePay,        
   ISNULL(SilverIncomePay,0) SilverPointsPay,ISNULL(DiamondIncomePay,0) DiamondIncomePay,        
   ISNULL(IsRegistrationActivated,0) IsRegistrationActivated,MobileNo,EmailId,        
   (case when (um.IsKYCVerified = 1 and um.IsPANVerified = 1 and um.IsBankDocumentVerified = 1) then 'YES' else 'NO' end) as KYCApproved,      
   ISNULL(uplineMonthly.UplineIncome,0) AS UplineMonthlyPay,    
   ISNULL(RetailRewardMonthly.RetailRewardIncome,0) AS RetailRewardMonthlyPay    
 INTO #PaymentDtl        
 from UserMst as um        
 LEFT JOIN (        
 Select UserId,SUM(case when CreditType=1 then CreditAmount end) as DirectIncome         
     ,SUM(case when CreditType=2 then CreditAmount end) as RePurchaseIncome        
     ,SUM(case when CreditType=3 then CreditAmount end) as FranchiseeIncome        
  from WalletCreditDtl         
  WHERE cast(CreditDate as date)<=@Todate        
  group by UserId        
 ) as direct on um.UserId=direct.UserId         
 LEFT JOIN (        
 Select UserId,SUM(CreditAmt) as VoltIncome        
 from CompanyShareCreditDtl        
  WHERE cast(CreditDate as date)<=@Todate        
  group by UserId        
  ) as volt on um.UserId=volt.UserId        
 LEFT JOIN(        
  Select UserId,SUM(DebitAmount) as TotalPayout,SUM(DirectIncomePay)DirectIncomePay,SUM(VoltIncomePay) as VoltIncomePay        
    ,SUM(PointsIncomePay)PointsIncomePay,SUM(LeaderIncomePay)LeaderIncomePay,SUM(UplineSupportIncomePay)UplineSupportIncomePay        
    ,SUM(SilverIncomePay)SilverIncomePay,SUM(DiamondIncomePay)DiamondIncomePay        
  from WalletDebitDtl        
  --WHERE cast(DebitDate as date)<=@Todate        
  group by UserId        
 ) as debit on um.UserId=debit.UserId        
 --LEFT JOIN dbo.FN_GET_ALL_TOTAL_MATCHING_POINTS(@UserId) as points on um.UserId=points.UserId        
 LEFT JOIN(Select UserId,SUM(MatchingPoints)FinalPoints         
     from MatchingPointsCreditDtl             
    WHERE cast(CreditDate as date)<=@Todate        
     group by UserId) as points on um.UserId=points.UserId      
        
 LEFT JOIN (SELECT d.UserId,SUM(d.UplineIncome) as UplineIncome      
 FROM UplineIncomeDetail d inner join UplineIncomeSummary s ON d.UplineIncomeSummaryId = s.UplineIncomeSummaryId        
  WHERE d.IsQualified = 1 AND (d.IsPayout IS NULL OR d.IsPayout = 0) AND DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, CAST(
      CAST(s.[Year] AS VARCHAR(4)) +
      RIGHT('0' + CAST(s.[Month] AS VARCHAR(2)), 2) +
      RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
   AS DATETIME)) + 1, 0)) < @Todate
  GROUP BY UserId        
 ) as uplineMonthly on um.UserId = uplineMonthly.UserId    
     
 LEFT JOIN (SELECT d.UserId,SUM(d.RetailRewardIncome) as RetailRewardIncome      
 FROM RetailRewardIncomeDetail d inner join RetailRewardIncomeSummary s ON d.RetailRewardIncomeSummaryId = s.RetailRewardIncomeSummaryId        
  WHERE d.IsQualified = 1 AND (d.IsPayout IS NULL OR d.IsPayout = 0) AND DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, CAST(
      CAST(s.[Year] AS VARCHAR(4)) +
      RIGHT('0' + CAST(s.[Month] AS VARCHAR(2)), 2) +
      RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
   AS DATETIME)) + 1, 0)) < @Todate
  GROUP BY UserId        
 ) as RetailRewardMonthly on um.UserId = RetailRewardMonthly.UserId    
    
 LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(1,@UserId,default,@Todate) as leader on um.UserId=leader.UserId        
 LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(0,@UserId,default,@Todate) as uplinesupport on um.UserId=uplinesupport.UserId        
 LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(0,@UserId,default,@Todate) as silver on um.UserId=silver.UserId        
 LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(1,@UserId,default,@Todate) as diamond on um.UserId=diamond.UserId        
 Where um.UserId=case when @UserId=0 then um.UserId else @UserId end        
   --AND ISNULL(IsRegistrationActivated,0)=1        
        
 Declare @TDSPercWithoutPAN decimal        
 Declare @TDSPercWithPAN NUMERIC(18,2)        
 Declare @AdminChargePerc decimal        
 Declare @TDSDexuctionLimit decimal        
 Select @TDSPercWithoutPAN=Value from ConfigMst where [Key]='TDSPercWithoutPAN'        
 Select @TDSPercWithPAN=Value from ConfigMst where [Key]='TDSPercWithPAN'        
 Select @AdminChargePerc=Value from ConfigMst where [Key]='AdminChargePerc'        
 Select @TDSDexuctionLimit=Value from ConfigMst where [Key]='TDSDexuctionLimit'         
 SET @TDSPercWithoutPAN=ISNULL(@TDSPercWithoutPAN,20)        
 SET @TDSPercWithPAN=ISNULL(@TDSPercWithPAN,5)        
 SET @AdminChargePerc=ISNULL(@AdminChargePerc,5)        
        
 insert into @PaymentDtl(UserId,RefferalCode,FullName,PANNo,BankAccNo,ISFCCode,        
      DirectIncome,RePurchaseIncome,VoltIncome,        
       TotalPointsIncome,TotalPayout,DirectIncomePay,PointsIncomePay,VoltIncomePay,        
       LeaderIncome,UplineSupportIncome,SilverIncome,DiamondIncome,        
       LeaderPointsPay,UplineSupportPointsPay,SilverPointsPay,DiamondPointsPay,        
       IsRegistrationActivated,MobileNo,EmailId,KYCApproved,UplineMonthlyPay,RetailRewardMonthlyPay)        
 Select * from #PaymentDtl        
         
 Update pd         
        
 set         
   TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome+LeaderIncome+UplineSupportIncome+SilverIncome+DiamondIncome+UplineMonthlyPay+RetailRewardMonthlyPay),0)        
   --TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome),0)        
  ,TDSPerc=case when len(PANNo)>0 then @TDSPercWithPAN else @TDSPercWithoutPAN end        
  ,AdminChargePerc=@AdminChargePerc        
 from @PaymentDtl as pd        
         
        
        
 --SELECT *        
 --Added TDS Condition as per govt. norms - If income greater than 14999 than we need to deduct the TDS        
 Update pd set TDS=(CASE WHEN TotalIncome > @TDSDexuctionLimit         
        THEN ((TotalIncome-TotalPayout)*TDSPerc/100)         
       ELSE 0         
        END)        
   ,AdminCharge=((TotalIncome-TotalPayout)*AdminChargePerc/100),        
   NetPayable=TotalIncome-TotalPayout        
 FROM @PaymentDtl as pd        
        
 Select *,(TotalIncome-TotalPayout-TDS-AdminCharge) as FinalPayoutAmt from @PaymentDtl        
 order by NetPayable desc        
         
 Drop Table #PaymentDtl        
        
END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
		object_id = OBJECT_ID(N'PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_BANKFORMAT') 
		AND type in (N'P', N'PC'))
	DROP PROCEDURE PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_BANKFORMAT
GO
CREATE PROCEDURE [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_BANKFORMAT]        
(        
 @UserId numeric(18,0) = 0,        
 @IsUptoToday bit = 1        
)        
AS        
BEGIN        
      
SET FMTONLY OFF        
        
IF OBJECT_ID('tempdb..#TblResult') IS NOT NULL      
    DROP TABLE #TblResult      
      
IF OBJECT_ID('tempdb..#PaymentDtl') IS NOT NULL      
    DROP TABLE #PaymentDtl      
      
Declare @ToDate date = getdate()        
IF @IsUptoToday=0        
BEGIN        
 Select @ToDate=EndDate from dbo.fn_GetFirstLastDateOfWeekByDate(DATEADD(d,-7,GETDATE()))        
END        
        
Declare @PaymentDtl table(UserId bigint,RefferalCode nvarchar(20),FullName nvarchar(max),PANNo nvarchar(50),BankAccNo nvarchar(50),ISFCCode nvarchar(50)        
       ,DirectIncome numeric(18,0),RePurchaseIncome numeric(18,0),VoltIncome numeric(18,0)        
       ,TotalPointsIncome numeric(18,0),TotalPayout numeric(18,0),TotalIncome numeric(18,0)        
       ,DirectIncomePay numeric(18,0),PointsIncomePay numeric(18,0)        
       ,TDSPerc numeric(18,2),TDS numeric(18,0),AdminChargePerc numeric(18,2),AdminCharge numeric(18,0)        
       ,NetPayable numeric(18,0),VoltIncomePay numeric(18,0),LeaderIncome numeric(18,0)        
       ,UplineSupportIncome numeric(18,0),SilverIncome numeric(18,0),DiamondIncome numeric(18,0),LeaderPointsPay numeric(18,0)        
       ,UplineSupportPointsPay numeric(18,0),SilverPointsPay numeric(18,0),DiamondPointsPay numeric(18,0),IsRegistrationActivated bit,        
       MobileNo numeric(18,0),EmailId nvarchar(max),BankName nvarchar(250),BranchName nvarchar(100),IsHDFCPayOut bit,
	   UplineMonthlyPay numeric(18,0),RetailRewardMonthlyPay numeric(18,0))        
        
 Select um.UserId,um.RefferalCode,um.FullName,um.PANNo,um.BankAccNo,um.ISFCCode,        
   ISNULL(DirectIncome,0)DirectIncome, ISNULL(RePurchaseIncome,0)RePurchaseIncome,ISNULL(VoltIncome,0)VoltIncome,        
   ISNULL(FinalPoints,0)TotalPointsIncome,ISNULL(TotalPayout,0)TotalPayout,        
   ISNULL(DirectIncomePay,0)DirectIncomePay,ISNULL(PointsIncomePay,0)PointsIncomePay,        
   ISNULL(VoltIncomePay,0)VoltIncomePay,ISNULL(leader.CreditPoints,0)FinalLeaderPoints,        
   ISNULL(uplinesupport.CreditPoints,0)FinalUplineSupportPoints,        
   ISNULL(silver.CreditPoints,0)FinalSilverPoints,ISNULL(diamond.CreditPoints,0)FinalDiamondPoints,        
   ISNULL(LeaderIncomePay,0)LeaderIncomePay,ISNULL(UplineSupportIncomePay,0)UplineSupportIncomePay,        
   ISNULL(SilverIncomePay,0) SilverPointsPay,ISNULL(DiamondIncomePay,0) DiamondIncomePay,        
   ISNULL(IsRegistrationActivated,0) IsRegistrationActivated,MobileNo,EmailId,BankName,BranchName,0 AS IsHDFCPayOut,
   ISNULL(uplineMonthly.UplineIncome,0) AS UplineMonthlyPay,
   ISNULL(RetailRewardMonthly.RetailRewardIncome,0) AS RetailRewardMonthlyPay
 INTO #PaymentDtl        
 from UserMst as um        
 LEFT JOIN (        
 Select UserId,SUM(case when CreditType=1 then CreditAmount end) as DirectIncome         
     ,SUM(case when CreditType=2 then CreditAmount end) as RePurchaseIncome        
     ,SUM(case when CreditType=3 then CreditAmount end) as FranchiseeIncome        
  from WalletCreditDtl         
  WHERE cast(CreditDate as date)<=@Todate        
  group by UserId        
 ) as direct on um.UserId=direct.UserId         
 LEFT JOIN (        
 Select UserId,SUM(CreditAmt) as VoltIncome        
 from CompanyShareCreditDtl        
  WHERE cast(CreditDate as date)<=@Todate        
  group by UserId        
  ) as volt on um.UserId=volt.UserId        
 LEFT JOIN(        
  Select UserId,SUM(DebitAmount) as TotalPayout,SUM(DirectIncomePay)DirectIncomePay,SUM(VoltIncomePay) as VoltIncomePay        
    ,SUM(PointsIncomePay)PointsIncomePay,SUM(LeaderIncomePay)LeaderIncomePay,SUM(UplineSupportIncomePay)UplineSupportIncomePay        
    ,SUM(SilverIncomePay)SilverIncomePay,SUM(DiamondIncomePay)DiamondIncomePay        
  from WalletDebitDtl        
  group by UserId        
 ) as debit on um.UserId=debit.UserId        
 LEFT JOIN(Select UserId,SUM(MatchingPoints)FinalPoints         
     from MatchingPointsCreditDtl             
    WHERE cast(CreditDate as date)<=@Todate        
     group by UserId) as points on um.UserId=points.UserId 
	 
	LEFT JOIN (SELECT d.UserId,SUM(d.UplineIncome) as UplineIncome  
 FROM UplineIncomeDetail d inner join UplineIncomeSummary s ON d.UplineIncomeSummaryId = s.UplineIncomeSummaryId    
  WHERE d.IsQualified = 1 AND (d.IsPayout IS NULL OR d.IsPayout = 0) AND DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, CAST(
      CAST(s.[Year] AS VARCHAR(4)) +
      RIGHT('0' + CAST(s.[Month] AS VARCHAR(2)), 2) +
      RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
   AS DATETIME)) + 1, 0)) < @Todate
  GROUP BY UserId    
 ) as uplineMonthly on um.UserId = uplineMonthly.UserId
 
 LEFT JOIN (SELECT d.UserId,SUM(d.RetailRewardIncome) as RetailRewardIncome  
 FROM RetailRewardIncomeDetail d inner join RetailRewardIncomeSummary s ON d.RetailRewardIncomeSummaryId = s.RetailRewardIncomeSummaryId    
  WHERE d.IsQualified = 1 AND (d.IsPayout IS NULL OR d.IsPayout = 0) AND DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, CAST(
      CAST(s.[Year] AS VARCHAR(4)) +
      RIGHT('0' + CAST(s.[Month] AS VARCHAR(2)), 2) +
      RIGHT('0' + CAST(1 AS VARCHAR(2)), 2) 
   AS DATETIME)) + 1, 0)) < @Todate
  GROUP BY UserId    
 ) as RetailRewardMonthly on um.UserId = RetailRewardMonthly.UserId

 LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(1,@UserId,default,@Todate) as leader on um.UserId=leader.UserId        
 LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(0,@UserId,default,@Todate) as uplinesupport on um.UserId=uplinesupport.UserId        
 LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(0,@UserId,default,@Todate) as silver on um.UserId=silver.UserId        
 LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(1,@UserId,default,@Todate) as diamond on um.UserId=diamond.UserId        
 Where um.UserId=case when @UserId=0 then um.UserId else @UserId end        
 Declare @TDSPercWithoutPAN decimal        
 Declare @TDSPercWithPAN NUMERIC(18,2)        
 Declare @AdminChargePerc decimal        
 Declare @TDSDexuctionLimit decimal        
 Select @TDSPercWithoutPAN=Value from ConfigMst where [Key]='TDSPercWithoutPAN'        
 Select @TDSPercWithPAN=Value from ConfigMst where [Key]='TDSPercWithPAN'        
 Select @AdminChargePerc=Value from ConfigMst where [Key]='AdminChargePerc'        
 Select @TDSDexuctionLimit=Value from ConfigMst where [Key]='TDSDexuctionLimit'         
 SET @TDSPercWithoutPAN=ISNULL(@TDSPercWithoutPAN,20)        
 SET @TDSPercWithPAN=ISNULL(@TDSPercWithPAN,5)        
 SET @AdminChargePerc=ISNULL(@AdminChargePerc,5)        
        
 insert into @PaymentDtl(UserId,RefferalCode,FullName,PANNo,BankAccNo,ISFCCode,        
      DirectIncome,RePurchaseIncome,VoltIncome,        
       TotalPointsIncome,TotalPayout,DirectIncomePay,PointsIncomePay,VoltIncomePay,        
       LeaderIncome,UplineSupportIncome,SilverIncome,DiamondIncome,        
       LeaderPointsPay,UplineSupportPointsPay,SilverPointsPay,DiamondPointsPay,        
       IsRegistrationActivated,MobileNo,EmailId,BankName,BranchName,IsHDFCPayOut,UplineMonthlyPay,RetailRewardMonthlyPay)        
 Select * from #PaymentDtl        
         
 Update pd         
 set         
   TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome+LeaderIncome+UplineSupportIncome+SilverIncome+DiamondIncome+UplineMonthlyPay+RetailRewardMonthlyPay),0)        
  ,TDSPerc=case when len(PANNo)>0 then @TDSPercWithPAN else @TDSPercWithoutPAN end        
  ,AdminChargePerc=@AdminChargePerc        
 from @PaymentDtl as pd        
       
 --Added TDS Condition as per govt. norms - If income greater than 14999 than we need to deduct the TDS        
 Update pd set TDS=(CASE WHEN TotalIncome > @TDSDexuctionLimit         
        THEN ((TotalIncome-TotalPayout)*TDSPerc/100)         
       ELSE 0         
        END)        
   ,AdminCharge=((TotalIncome-TotalPayout)*AdminChargePerc/100),        
   NetPayable=TotalIncome-TotalPayout        
 FROM @PaymentDtl as pd        
       
 --Select *,(TotalIncome-TotalPayout-TDS-AdminCharge) as FinalPayoutAmt from @PaymentDtl order by NetPayable desc        
      
 Select *,(TotalIncome-TotalPayout-TDS-AdminCharge) as FinalPayoutAmt INTO #TblResult from @PaymentDtl         
       
 UPDATE t SET t.IsHDFCPayOut = 1      
 FROM #TblResult t WHERE (ISFCCode like '%HDFC%' OR BankName like '%HDFC%')      
      
 SELECT (CASE WHEN IsHDFCPayOut = 1 THEN 'l' ELSE 'N' END) AS TransactionType       
  ,'' AS Blank_1      
  ,BankAccNo AS BeneficiaryAccountNumber      
  ,FinalPayoutAmt AS InstrumentAmount      
  ,FullName AS BeneficiaryName      
  ,'' AS Blank_2      
  ,'' AS Blank_3      
  ,'' AS BeneAddress1      
  ,'' AS BeneAddress2      
  ,'' AS BeneAddress3      
  ,'' AS BeneAddress4      
  ,'' AS BeneAddress5      
  ,'' AS InstructionReferenceNumber      
  ,RefferalCode AS CustomerReferenceNumber      
  ,CAST(MobileNo AS VARCHAR(50)) AS PaymentDetail1      
  ,'' AS PaymentDetail2      
  ,'' AS PaymentDetail3      
  ,'' AS PaymentDetail4      
  ,'' AS PaymentDetail5      
  ,'' AS PaymentDetail6      
  ,'' AS PaymentDetail7      
  ,'' AS Blank_4      
  ,FORMAT(GETDATE(), 'dd/MM/yyyy') AS TransactionDate      
  ,'' AS Blank_5      
  ,ISFCCode AS IFSCCode      
  ,BankName AS BeneBankName      
  ,BranchName AS BranchName      
  ,EmailId AS BeneficiaryEmailId      
  ,IsHDFCPayOut      
  FROM #TblResult       
  WHERE FinalPayoutAmt > 0     
  AND IsRegistrationActivated = 1    
  AND LEN(ISFCCode) > 0    
  AND LEN(BankAccNo) > 0    
  ORDER BY NetPayable DESC      
      
 Drop Table #TblResult      
 Drop Table #PaymentDtl        
      
/*      
Declare @ToDate date=getdate()        
IF @IsUptoToday=0        
BEGIN       Select @ToDate=EndDate from dbo.fn_GetFirstLastDateOfWeekByDate(DATEADD(d,-7,GETDATE()))        
END        
        
Declare @PaymentDtl table(UserId bigint,RefferalCode nvarchar(20),FullName nvarchar(max),PANNo nvarchar(50),BankAccNo nvarchar(50),ISFCCode nvarchar(50)        
       ,DirectIncome numeric(18,0),RePurchaseIncome numeric(18,0),VoltIncome numeric(18,0)        
       ,TotalPointsIncome numeric(18,0),TotalPayout numeric(18,0),TotalIncome numeric(18,0)        
       ,DirectIncomePay numeric(18,0),PointsIncomePay numeric(18,0)        
       ,TDSPerc numeric(18,2),TDS numeric(18,0),AdminChargePerc numeric(18,2),AdminCharge numeric(18,0)        
       ,NetPayable numeric(18,0),VoltIncomePay numeric(18,0),LeaderIncome numeric(18,0)        
       ,UplineSupportIncome numeric(18,0),SilverIncome numeric(18,0),DiamondIncome numeric(18,0),LeaderPointsPay numeric(18,0)        
       ,UplineSupportPointsPay numeric(18,0),SilverPointsPay numeric(18,0),DiamondPointsPay numeric(18,0),IsRegistrationActivated bit,        
       MobileNo numeric(18,0),EmailId nvarchar(max))        
        
 Select um.UserId,um.RefferalCode,um.FullName,um.PANNo,um.BankAccNo,um.ISFCCode,        
   ISNULL(DirectIncome,0)DirectIncome, ISNULL(RePurchaseIncome,0)RePurchaseIncome,ISNULL(VoltIncome,0)VoltIncome,        
   ISNULL(FinalPoints,0)TotalPointsIncome,ISNULL(TotalPayout,0)TotalPayout,        
   ISNULL(DirectIncomePay,0)DirectIncomePay,ISNULL(PointsIncomePay,0)PointsIncomePay,        
   ISNULL(VoltIncomePay,0)VoltIncomePay,ISNULL(leader.CreditPoints,0)FinalLeaderPoints,        
   ISNULL(uplinesupport.CreditPoints,0)FinalUplineSupportPoints,        
   ISNULL(silver.CreditPoints,0)FinalSilverPoints,ISNULL(diamond.CreditPoints,0)FinalDiamondPoints,        
   ISNULL(LeaderIncomePay,0)LeaderIncomePay,ISNULL(UplineSupportIncomePay,0)UplineSupportIncomePay,        
   ISNULL(SilverIncomePay,0) SilverPointsPay,ISNULL(DiamondIncomePay,0) DiamondIncomePay,        
   ISNULL(IsRegistrationActivated,0) IsRegistrationActivated,MobileNo,EmailId        
 INTO #PaymentDtl        
 from UserMst as um        
 LEFT JOIN (        
 Select UserId,SUM(case when CreditType=1 then CreditAmount end) as DirectIncome         
     ,SUM(case when CreditType=2 then CreditAmount end) as RePurchaseIncome        
     ,SUM(case when CreditType=3 then CreditAmount end) as FranchiseeIncome        
  from WalletCreditDtl         
  WHERE cast(CreditDate as date)<=@Todate        
  group by UserId        
 ) as direct on um.UserId=direct.UserId         
 LEFT JOIN (        
 Select UserId,SUM(CreditAmt) as VoltIncome        
 from CompanyShareCreditDtl        
  WHERE cast(CreditDate as date)<=@Todate        
  group by UserId        
  ) as volt on um.UserId=volt.UserId        
 LEFT JOIN(        
  Select UserId,SUM(DebitAmount) as TotalPayout,SUM(DirectIncomePay)DirectIncomePay,SUM(VoltIncomePay) as VoltIncomePay        
    ,SUM(PointsIncomePay)PointsIncomePay,SUM(LeaderIncomePay)LeaderIncomePay,SUM(UplineSupportIncomePay)UplineSupportIncomePay        
    ,SUM(SilverIncomePay)SilverIncomePay,SUM(DiamondIncomePay)DiamondIncomePay        
  from WalletDebitDtl        
  group by UserId        
 ) as debit on um.UserId=debit.UserId        
 LEFT JOIN(Select UserId,SUM(MatchingPoints)FinalPoints         
     from MatchingPointsCreditDtl             
    WHERE cast(CreditDate as date)<=@Todate        
     group by UserId) as points on um.UserId=points.UserId        
 LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(1,@UserId,default,@Todate) as leader on um.UserId=leader.UserId        
 LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(0,@UserId,default,@Todate) as uplinesupport on um.UserId=uplinesupport.UserId        
 LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(0,@UserId,default,@Todate) as silver on um.UserId=silver.UserId        
 LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(1,@UserId,default,@Todate) as diamond on um.UserId=diamond.UserId        
 Where um.UserId=case when @UserId=0 then um.UserId else @UserId end        
 Declare @TDSPercWithoutPAN decimal        
 Declare @TDSPercWithPAN NUMERIC(18,2)        
 Declare @AdminChargePerc decimal        
 Declare @TDSDexuctionLimit decimal        
 Select @TDSPercWithoutPAN=Value from ConfigMst where [Key]='TDSPercWithoutPAN'        
 Select @TDSPercWithPAN=Value from ConfigMst where [Key]='TDSPercWithPAN'        
 Select @AdminChargePerc=Value from ConfigMst where [Key]='AdminChargePerc'        
 Select @TDSDexuctionLimit=Value from ConfigMst where [Key]='TDSDexuctionLimit'         
 SET @TDSPercWithoutPAN=ISNULL(@TDSPercWithoutPAN,20)        
 SET @TDSPercWithPAN=ISNULL(@TDSPercWithPAN,5)        
 SET @AdminChargePerc=ISNULL(@AdminChargePerc,5)        
        
 insert into @PaymentDtl(UserId,RefferalCode,FullName,PANNo,BankAccNo,ISFCCode,        
      DirectIncome,RePurchaseIncome,VoltIncome,        
       TotalPointsIncome,TotalPayout,DirectIncomePay,PointsIncomePay,VoltIncomePay,        
       LeaderIncome,UplineSupportIncome,SilverIncome,DiamondIncome,        
       LeaderPointsPay,UplineSupportPointsPay,SilverPointsPay,DiamondPointsPay,        
       IsRegistrationActivated,MobileNo,EmailId)        
 Select * from #PaymentDtl        
         
 Update pd         
        
 set         
   TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome+LeaderIncome+UplineSupportIncome+SilverIncome+DiamondIncome),0)        
  ,TDSPerc=case when len(PANNo)>0 then @TDSPercWithPAN else @TDSPercWithoutPAN end        
  ,AdminChargePerc=@AdminChargePerc        
 from @PaymentDtl as pd        
       
 --Added TDS Condition as per govt. norms - If income greater than 14999 than we need to deduct the TDS        
 Update pd set TDS=(CASE WHEN TotalIncome > @TDSDexuctionLimit         
        THEN ((TotalIncome-TotalPayout)*TDSPerc/100)         
       ELSE 0         
        END)        
   ,AdminCharge=((TotalIncome-TotalPayout)*AdminChargePerc/100),        
   NetPayable=TotalIncome-TotalPayout        
 FROM @PaymentDtl as pd        
        
 Select *,(TotalIncome-TotalPayout-TDS-AdminCharge) as FinalPayoutAmt from @PaymentDtl        
 order by NetPayable desc        
         
 Drop Table #PaymentDtl  */      
        
END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE 
		object_id = OBJECT_ID(N'PROC_GET_ADMIN_WALLET_DEBIT_REPORT') 
		AND type in (N'P', N'PC'))
	DROP PROCEDURE PROC_GET_ADMIN_WALLET_DEBIT_REPORT
GO
CREATE Procedure [dbo].[PROC_GET_ADMIN_WALLET_DEBIT_REPORT]              
(              
 @UserId int = 0,          
 @FromDate DATE,          
 @ToDate DATE        
)              
AS              
BEGIN              
            
 If(OBJECT_ID('tempdb..#TblWallet') Is Not Null)                                
 BEGIN                                
 Drop Table #TblWallet                       
 End        
         
 If(OBJECT_ID('tempdb..#TblFinYearWallet') Is Not Null)                                
 BEGIN                                
 Drop Table #TblFinYearWallet                       
 End        
        
 If(OBJECT_ID('tempdb..#TempSumTbl') Is Not Null)                                
 BEGIN                                
 Drop Table #TempSumTbl                       
 End        
        
 Declare @finYearStartDt DATE,        
   @finYearEndDt DATE        
 select @finYearStartDt = DATEFROMPARTS(Yr, 4, 1), @finYearEndDt = DATEFROMPARTS(Yr + 1, 3, 31) from         
(select case when DATEPART(month, @FromDate) < 4 then DATEPART(year, @FromDate) - 1 else DATEPART(year, @FromDate) end Yr) a        
            
 Declare @TDSPercWithoutPAN decimal              
   ,@TDSPercWithPAN NUMERIC(18,2)              
  ,@TDSDexuctionLimit decimal              
 Select @TDSPercWithoutPAN = [Value] from ConfigMst where [Key]='TDSPercWithoutPAN'              
 Select @TDSPercWithPAN = [Value] from ConfigMst where [Key]='TDSPercWithPAN'              
 Select @TDSDexuctionLimit= [Value] from ConfigMst where [Key]='TDSDexuctionLimit'               
 SET @TDSPercWithoutPAN=ISNULL(@TDSPercWithoutPAN,20)              
 SET @TDSPercWithPAN=ISNULL(@TDSPercWithPAN,5)              
              
 Select wd.*,um.RefferalCode,um.FirstName,um.LastName,um.MiddleName,um.FullName,um.RegistrationDate,              
   BankName,BankAccNo, CONVERT(varchar(11),DebitDate,103) as PayoutDate,              
   um.MobileNo,um.EmailId,PANNo,        
   CAST(0 as float) NetPayable_Year,        
   CAST(0 as float) TDSAmount_Year        
   INTO #TblFinYearWallet              
 from WalletDebitDtl as wd              
 INNER JOIN UserMst as um on wd.UserId = um.UserId          
 WHERE wd.DebitDate BETWEEN @finYearStartDt AND @finYearEndDt          
 order by DebitDate desc              
            
 UPDATE pd               
 SET               
  TDSPerc = case when len(PANNo) > 0 then @TDSPercWithPAN else @TDSPercWithoutPAN end               
 FROM #TblFinYearWallet as pd             
            
 --Update pd set TDSAmount = (CASE WHEN DebitAmount > @TDSDexuctionLimit               
 --       THEN ((DebitAmount) * TDSPerc/100 )               
 --      ELSE 0               
 --       END)              
 --FROM #TblFinYearWallet as pd          
         
 SELECT UserId,sum(NetPayable)AS NetPayable,sum(TDSAmount) AS TDSAmount INTO #TempSumTbl FROM #TblFinYearWallet group BY UserId        
        
 UPDATE pd SET NetPayable_Year = t.NetPayable, TDSAmount_Year = CONVERT(DECIMAL(10, 2),t.TDSAmount)        
 FROM #TblFinYearWallet pd INNER JOIN #TempSumTbl t ON pd.UserId = t.UserId        
        
 SELECT * INTO #TblWallet FROM #TblFinYearWallet WHERE DebitDate BETWEEN @FromDate AND @ToDate          
 ORDER BY DebitDate desc               
        
  SELECT * FROM       
 (SELECT WalletDebitDtlId          
  ,UserId          
  ,DebitDate          
  ,DebitAmount          
  ,UTRRefNo          
  ,AccountNo          
  ,ISFCCode         
  ,TDSAmount_Year        
  ,TDSAmount          
  ,TDSPerc          
  ,AdminCharge          
  ,AdminPerc          
  ,NetPayable_Year        
  ,NetPayable        
  ,IsActive          
  ,CreatedBy          
  ,CreatedDate          
  ,DirectIncomePay          
  ,PointsIncomePay          
  ,VoltIncomePay          
  ,LeaderIncomePay          
  ,UplineSupportIncomePay          
  ,SilverIncomePay          
  ,DiamondIncomePay,RefferalCode,FirstName,LastName,MiddleName,FullName,RegistrationDate,              
  BankName,BankAccNo, CONVERT(varchar(11),DebitDate,103) as PayoutDate,              
  MobileNo,EmailId,PANNo,
  UplineMonthlyPay,
  RetailRewardMonthlyPay
 FROM #TblWallet)AS tbl          
            
 -- Select wd.*,um.RefferalCode,um.FirstName,um.LastName,um.MiddleName,um.FullName,um.RegistrationDate,              
 --  BankName,BankAccNo, CONVERT(varchar(11),DebitDate,103) as PayoutDate,              
 --  um.MobileNo,um.EmailId,PANNo              
 --from WalletDebitDtl as wd              
 --INNER JOIN UserMst as um on wd.UserId=um.UserId              
 --order by DebitDate desc              
            
END