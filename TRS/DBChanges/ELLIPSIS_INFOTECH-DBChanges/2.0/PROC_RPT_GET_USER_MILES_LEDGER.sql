/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_USER_MILES_LEDGER]    Script Date: 07-04-2021 14:15:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PROC_RPT_GET_USER_MILES_LEDGER]
(
@UserId int=0
)
AS
BEGIN
	
	;WITH cte
	AS
	(
	   SELECT	UserId,UserIdFrom,TransactionDate,UserPurchaseHdrId,CreatedDate,CreditAmount,DebitAmount, SUM(CreditAmount) CreditAmountTotal, SUM(DebitAmount) DebitAmountTotal
				, IsLapsed
	   FROM (
				SELECT UserId,UserIdFrom,CreditDate as TransactionDate,CreditAmount as CreditAmount,0 as DebitAmount,CreatedDate,0 as UserPurchaseHdrId , 0 AS IsLapsed
				FROM MilesCreditDtl Where UserId=case when ISNULL(@UserId,0)=0 then UserId ELSE @UserId END

				union

				SELECT UserId,0 as UserIdFrom,DebitDate as TransactionDate,0 as CreditAmount,DebitAmount as DebitAmount,CreatedDate,UserPurchaseHdrId, IsLapsed
				FROM MilesDebitDtl Where UserId=case when ISNULL(@UserId,0)=0 then UserId ELSE @UserId END
			
	   )as Miles 
	   gROUP BY UserId,UserIdFrom,TransactionDate,UserPurchaseHdrId,CreatedDate,CreditAmount,DebitAmount, IsLapsed
	  
	), cteRanked AS
	(
	   SELECT	CreditAmountTotal,DebitAmountTotal, UserId,UserIdFrom,TransactionDate,UserPurchaseHdrId,CreatedDate,CreditAmount,DebitAmount
				, ROW_NUMBER() OVER(Partition by UserId ORDER BY UserId,CreatedDate) rownum
				, IsLapsed
	   FROM cte
	) 

	SELECT  c1.UserId,UserIdFrom,TransactionDate,c1.UserPurchaseHdrId,c1.CreatedDate,CreditAmount,DebitAmount,
			(	SELECT SUM(CreditAmountTotal) - SUM(DebitAmountTotal) 
				FROM cteRanked c2 
				WHERE c2.rownum <= c1.rownum and c2.UserId=c1.UserId
			) AS BalanceAmount,
			RefferalCode,FullName,OrderNo,
			ROW_NUMBER() over(Partition by c1.UserId order by c1.CreatedDate) as RowNo
			, (
				CASE WHEN (c1.CreditAmount > 0) THEN 'Credit' WHEN (c1.DebitAmount > 0 AND c1.IsLapsed = 1) THEN 'Lapsed' WHEN (c1.DebitAmount > 0 AND c1.IsLapsed = 0) THEN 'Used' END
			) AS MilesStatus
	FROM cteRanked c1
	LEFT JOIN (SELECT UserId,RefferalCode,FullName FROM UserMst) as um on c1.UserIdFrom=um.UserId
	LEFT JOIN (SELECT UserPurchaseHdrId,OrderNo FROM UserPurchaseHdr) as uph on c1.UserPurchaseHdrId=uph.UserPurchaseHdrId
	
END