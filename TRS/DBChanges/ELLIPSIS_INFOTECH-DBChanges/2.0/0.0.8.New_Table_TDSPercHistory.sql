/****** Object:  Table [dbo].[TDSPercHistory]    Script Date: 09-04-2021 17:23:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TDSPercHistory]') AND type in (N'U'))
	DROP TABLE [dbo].[TDSPercHistory]

CREATE TABLE [dbo].[TDSPercHistory](
	[TDSPercHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[PercWithPAN] FLOAT NOT NULL,
	[PercWithoutPAN] FLOAT NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TDSPercHistory] PRIMARY KEY CLUSTERED 
(
	[TDSPercHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
