
IF NOT EXISTS(SELECT * FROM [TDSPercHistory] WHERE CAST([EffectiveDate] AS DATE) = CAST('01 Apr 2015' AS DATE))
BEGIN
	INSERT INTO [dbo].[TDSPercHistory] ([PercWithPAN], [PercWithoutPAN] ,[EffectiveDate])
		 VALUES (5, 20 ,CAST('01 Apr 2015' AS DATETIME))
END


IF NOT EXISTS(SELECT * FROM [TDSPercHistory] WHERE CAST([EffectiveDate] AS DATE) = CAST('01 Apr 2020' AS DATE))
BEGIN
	INSERT INTO [dbo].[TDSPercHistory] ([PercWithPAN], [PercWithoutPAN] ,[EffectiveDate])
		 VALUES (3.75 , 20,CAST('01 Apr 2020' AS DATETIME))
END
