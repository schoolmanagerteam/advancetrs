IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'PROC_INSERT_USER_MILES_LAPSED')
BEGIN
	DROP PROCEDURE PROC_INSERT_USER_MILES_LAPSED
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Harshad Chauhan>
-- Create date: <13 Apr 2021>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PROC_INSERT_USER_MILES_LAPSED
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @FirstLapsedCycle DATETIME = DATEADD(DAY, -90, GETDATE()), @SecondLapsedCycle DATETIME = DATEADD(DAY, -180, GETDATE())

	DECLARE @FirstLapsedTbl TABLE (UserId INT, MilesCreditDtlId BIGINT, LapsedAmt FLOAT)
	DECLARE @SecondLapsedTbl TABLE (UserId INT, MilesCreditDtlId BIGINT, LapsedAmt FLOAT)

	DECLARE @AdminUserId INT = (SELECT TOP 1 UserId FROM UserMst(NOLOCK) WHERE RefferalUserId = 0 AND UserName = 'sadmin')

	DECLARE @FirstLapsedPerc NUMERIC(18,2) = ISNULL((SELECT TOP 1 ISNULL(Value, 0) FROM ConfigMst WHERE [Key] = 'PercentageOfMilesLapsedIn3Month'), 0)
	DECLARE @SecondLapsedPerc NUMERIC(18,2) = ISNULL((SELECT TOP 1 ISNULL(Value, 0) FROM ConfigMst WHERE [Key] = 'PercentageOfMilesLapsedIn6Month'), 0)
	-----------------------------------------------------------------------------------------------------------------------------
	INSERT INTO @FirstLapsedTbl
	SELECT	Final.UserId, Final.MilesCreditDtlId
			, (CASE WHEN (MinLapsedAmnt <= ISNULL(UsedCreditAmount, 0)) THEN 0 ELSE (MinLapsedAmnt - ISNULL(UsedCreditAmount, 0)) END) AS RmnLapsedAmt
	FROM (
		SELECT	CAST(((CreditAmount * @FirstLapsedPerc) / 100) AS NUMERIC(18, 2)) AS MinLapsedAmnt , MilesCreditDtl.UserId, MilesCreditDtl.UsedCreditAmount
				, MilesCreditDtl.MilesCreditDtlId
		FROM	MilesCreditDtl 
		where	(CreditAmount - UsedCreditAmount) <> 0
				AND (CAST(CreditDate AS DATE) = @FirstLapsedCycle)
	) AS Final

	UPDATE	MilesCreditDtl 
	SET		UsedCreditAmount = UsedCreditAmount+ MileslapsedTbl.LapsedAmt
	FROM ( SELECT * FROM @FirstLapsedTbl ) AS MileslapsedTbl
	WHERE	MileslapsedTbl.MilesCreditDtlId = MilesCreditDtl.MilesCreditDtlId

	INSERT INTO MilesDebitDtl(UserId, UserPurchaseHdrId, DebitDate, DebitAmount, IsActive, CreatedBy, CreatedDate, IsLapsed)
	SELECT UserId, 0, GETDATE(), sum(LapsedAmt) AS UserTotalLapsed, 1, @AdminUserId, GETDATE(), 1 FROM @FirstLapsedTbl GROUP BY UserId

	UPDATE UM_Dtl
	SET UM_Dtl.TotalDebitMiles = UM_Dtl.TotalDebitMiles + FA_Tbl.UserTotalLapsed
	FROM UserMilesDtl AS UM_Dtl
	INNER JOIN (
	SELECT UserId, sum(LapsedAmt) AS UserTotalLapsed FROM @FirstLapsedTbl GROUP BY UserId
	) AS FA_Tbl ON FA_Tbl.UserId = UM_Dtl.UserId
	-----------------------------------------------------------------------------------------------------------------------------

	INSERT INTO @SecondLapsedTbl
	SELECT	Final.UserId, Final.MilesCreditDtlId
			, (CASE WHEN (MinLapsedAmnt <= ISNULL(UsedCreditAmount, 0)) THEN 0 ELSE (MinLapsedAmnt - ISNULL(UsedCreditAmount, 0)) END) AS RmnLapsedAmt
	FROM (
		SELECT	CAST(((CreditAmount * @SecondLapsedPerc) / 100) AS NUMERIC(18, 2)) AS MinLapsedAmnt , MilesCreditDtl.UserId, MilesCreditDtl.UsedCreditAmount
				, MilesCreditDtl.MilesCreditDtlId
		FROM	MilesCreditDtl 
		where	(CreditAmount - UsedCreditAmount) <> 0
				AND (CAST(CreditDate AS DATE) = @SecondLapsedCycle)
	) AS Final

	UPDATE	MilesCreditDtl 
	SET		UsedCreditAmount = UsedCreditAmount+ MileslapsedTbl.LapsedAmt
	FROM ( SELECT * FROM @SecondLapsedTbl ) AS MileslapsedTbl
	WHERE	MileslapsedTbl.MilesCreditDtlId = MilesCreditDtl.MilesCreditDtlId

	INSERT INTO MilesDebitDtl(UserId, UserPurchaseHdrId, DebitDate, DebitAmount, IsActive, CreatedBy, CreatedDate, IsLapsed)
	SELECT UserId, 0, GETDATE(), sum(LapsedAmt) AS UserTotalLapsed, 1, @AdminUserId, GETDATE(), 1 FROM @SecondLapsedTbl GROUP BY UserId

	UPDATE UM_Dtl
	SET UM_Dtl.TotalDebitMiles = UM_Dtl.TotalDebitMiles + FA_Tbl.UserTotalLapsed
	FROM UserMilesDtl AS UM_Dtl
	INNER JOIN (
	SELECT UserId, sum(LapsedAmt) AS UserTotalLapsed FROM @SecondLapsedTbl GROUP BY UserId
	) AS FA_Tbl ON FA_Tbl.UserId = UM_Dtl.UserId

	-----------------------------------------------------------------------------------------------------------------------------
END
GO