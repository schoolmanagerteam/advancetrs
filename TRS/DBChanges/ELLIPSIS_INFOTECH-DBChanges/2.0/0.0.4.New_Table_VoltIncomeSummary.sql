/****** Object:  Table [dbo].[VoltIncomeSummary]    Script Date: 05-04-2021 13:23:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VoltIncomeSummary]') AND type in (N'U'))
	DROP TABLE [dbo].[VoltIncomeSummary]

CREATE TABLE [dbo].[VoltIncomeSummary](
	[VoltIncomeSummaryId] [bigint] IDENTITY(1,1) NOT NULL,
	[Month] [smallint] NOT NULL,
	[Year] [smallint] NOT NULL,
	[WeekNo] [smallint] NOT NULL,
	[WeeklyBudget] [numeric](18, 2) NOT NULL,
	[CompanyWeeklyBudget] [numeric](18, 2) NOT NULL,
	[BvMultiplier] [numeric](18, 2) NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_VoltIncomeSummary] PRIMARY KEY CLUSTERED 
(
	[VoltIncomeSummaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
