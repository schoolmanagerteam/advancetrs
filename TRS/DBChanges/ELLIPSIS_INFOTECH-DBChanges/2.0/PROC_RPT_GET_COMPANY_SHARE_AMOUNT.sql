/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_COMPANY_SHARE_AMOUNT]    Script Date: 06-04-2021 18:03:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*   
exec PROC_RPT_GET_COMPANY_SHARE_AMOUNT 0  
exec PROC_RPT_GET_COMPANY_SHARE_AMOUNT 54   
*/  
ALTER Procedure [dbo].[PROC_RPT_GET_COMPANY_SHARE_AMOUNT]  
(  
@UserId int,  
@FromDate datetime=null,  
@ToDate datetime=null  
)  
AS  
BEGIN  
 --Declare @UserId int=14  
 --Declare @FromDate datetime  
 --Declare @ToDate datetime  

	Select	Row_Number() over(order by cast(mcdtl.CreditDate as date) desc) as SrNo
			, convert(varchar, cast(CreditDate as date), 103) as VoltIncomeDate
			, mcdtl.CreditAmount AS VoltMiles
			, wum.RefferalCode, wum.FullName

	from	MilesCreditDtl(NOLOCK) AS mcdtl
			INNER JOIN UserMst(Nolock) as wum on mcdtl.UserIdFrom = wum.UserId  
	WHERE	mcdtl.InsertType = 2
			AND (cast(mcdtl.CreditDate as DATE)>=@FromDate or @FromDate is null)   
			and (cast(mcdtl.CreditDate as DATE)<=@ToDate or @ToDate is null)  
			AND (mcdtl.UserId = (case when @UserId=0 then mcdtl.UserId else @UserId end))

	-- Select Row_Number() over(order by cast(CreditDate as date) desc) as SrNo,cast(CreditDate as date) CreditDate,  
 --   um.RefferalCode,um.FullName,uph.TotalOrderAmount,wcd.CreditAmt as ShareAmount,  
 --   convert(varchar,cast(CreditDate as date),103) as VoltIncomeDate,  
 --   wcd.UserId,wum.RefferalCode as UserCode,wum.FullName as UserName,  
 --   CompanyShareCreditDtlId 
 --from CompanyShareCreditDtl(Nolock) as wcd  
 --INNER JOIN UserPurchaseHdr(Nolock) as uph on wcd.UserPurchaseHdrId=uph.UserPurchaseHdrId  
 --       AND (cast(wcd.CreditDate as DATE)>=@FromDate or @FromDate is null)   
 --       and (cast(wcd.CreditDate as DATE)<=@ToDate or @ToDate is null)  
 --INNER JOIN UserMst(Nolock) as um on uph.UserId=um.UserId  
 --INNER JOIN UserMst(Nolock) as wum on wcd.UserId=wum.UserId  
 --Where wcd.UserId=case when @UserId=0 then wcd.UserId else @UserId end  
   
END

