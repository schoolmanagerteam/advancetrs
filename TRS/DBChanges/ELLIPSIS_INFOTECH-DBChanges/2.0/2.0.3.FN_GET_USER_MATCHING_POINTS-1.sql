IF EXISTS (SELECT * FROM   sys.objects WHERE  object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_MATCHING_POINTS]') AND type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' ))
	DROP FUNCTION FN_GET_USER_MATCHING_POINTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION FN_GET_USER_MATCHING_POINTS
(	
	-- Add the parameters for the function here
	@UserId INT,  
	@FromDate date=null,  
	@ToDate date=null  
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT SUM(LeftBV) AS LeftMatchingBV, SUM(RightBV) AS RightMatchingBV, [Month], [Year]
	FROM (
		Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R'   
							then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end)   
						 when pcd.CreatedBy<>pcd.UserId   
							then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end
				) as LeftBV  
				, 0 AS RightBV
				, MONTH(pcd.CreditDate) as [Month], YEAR(pcd.CreditDate) AS [Year]

	 from [FN_GET_ALL_LEFT_SIDED_USER_TREE](@UserId,default) as lu  
	 INNER JOIN UserPurchaseHdr(NOLOCK) as uph on lu.UserId=uph.CreatedBy  
	 INNER JOIN UserPurchaseDtl(NOLOCK) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId  
	 INNER JOIN PointsCreditDtl(NOLOCK) as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId  
	 INNER JOIN UserMst(NOLOCK) as um on lu.UserId=um.UserId  
	 Where pcd.UserId=@UserId  
	   and ((cast(pcd.CreditDate as date)>=@FromDate) or (@FromDate is null))  
	   and ((cast(pcd.CreditDate as date)<=@ToDate) or (@ToDate is null))  
	GROUP BY MONTH(pcd.CreditDate), YEAR(pcd.CreditDate)

	UNION

	   Select	0 AS LeftBV, 
				SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='L'   
						then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end)   
					when pcd.CreatedBy<>pcd.UserId   
						then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) AS RightBV
				, MONTH(pcd.CreditDate) as [Month], YEAR(pcd.CreditDate) AS [Year]
   
	 from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,default) as ru  
	 INNER JOIN UserPurchaseHdr(Nolock) as uph on ru.UserId=uph.CreatedBy  
	 INNER JOIN UserPurchaseDtl(Nolock) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId  
	 INNER JOIN PointsCreditDtl(Nolock) as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId  
	 INNER JOIN UserMst(Nolock) as um on ru.UserId=um.UserId  
	 Where pcd.UserId=@UserId  
	   and ((cast(pcd.CreditDate as date)>=@FromDate) or (@FromDate is null))  
	   and ((cast(pcd.CreditDate as date)<=@ToDate) or (@ToDate is null))  
	GROUP BY MONTH(pcd.CreditDate), YEAR(pcd.CreditDate)

	) AS Final

	GROUP BY Month, Year
	
)
GO

