/****** Object:  StoredProcedure [dbo].[PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL_BYSERVICE]    Script Date: 01-04-2021 13:51:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	ALTER PROCEDURE [dbo].[PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL_BYSERVICE]                                  
AS                            
BEGIN                            
                       
 If(OBJECT_ID('tempdb..#OrderMst') Is Not Null)                      
 BEGIN                      
 Drop Table #OrderMst                
 End                
                
 If(OBJECT_ID('tempdb..#OrderDetail') Is Not Null)                      
 BEGIN                      
 Drop Table #OrderDetail                
 End                
    
 If(OBJECT_ID('tempdb..#TblPointsCreditDtl') Is Not Null)                      
 BEGIN                      
 Drop Table #TblPointsCreditDtl    
 End                
     
 CREATE TABLE #TblPointsCreditDtl(UserId int, FullName NVARCHAR(MAX),RefferalCode NVARCHAR(MAX),Position CHAR(10))    
                   
 SELECT Distinct TOP 10 hdr.UserPurchaseHdrId                    
    ,hdr.UserId                  
	,hdr.CreatedDate                  
    ,hdr.IsRepurchaseOrder                
	,hdr.OrderStatusId        
	,hdr.IsCODOrder        
	,hdr.ModifiedDate        
   INTO #OrderMst                     
   FROM UserPurchaseHdr(Nolock) hdr Inner join UserPurchaseDtl(Nolock) dtl ON hdr.UserPurchaseHdrId = dtl.UserPurchaseHdrId                    
      WHERE hdr.OrderStatusId IN (4,5) AND (dtl.IsCreditPointsProcess = 0  OR dtl.IsCreditPointsProcess IS NULL)                    
  ORDER BY hdr.UserPurchaseHdrId DESC                 
                
 SELECT p.RepurchaseMatchingAmt                    
    ,p.RepurchaseMatchingPerc                    
    ,p.DirectMatchingAmt                    
    ,p.DirectMatchingPerc                    
    ,dtl.Qty                    
    ,dtl.UserPurchaseDtlId                    
    ,hdr.UserPurchaseHdrId                    
    ,hdr.UserId                  
	,hdr.CreatedDate                  
    ,hdr.IsRepurchaseOrder                    
    ,(CASE WHEN hdr.IsRepurchaseOrder = 1 THEN p.RepurchaseMatchingPerc ELSE p.DirectMatchingPerc END) AS CreditPerc                    
	,CAST((CASE WHEN hdr.IsRepurchaseOrder = 1 THEN CAST(((p.RepurchaseMatchingAmt * p.RepurchaseMatchingPerc)/100) AS bigint) * dtl.Qty ELSE CAST(((p.DirectMatchingAmt * p.DirectMatchingPerc)/100) AS bigint) * dtl.Qty END)AS decimal(18,2)) AS CreditPoints  
	,hdr.IsCODOrder        
	,hdr.ModifiedDate        
   INTO #OrderDetail                     
   FROM #OrderMst hdr Inner join UserPurchaseDtl(Nolock) dtl ON hdr.UserPurchaseHdrId = dtl.UserPurchaseHdrId                    
                Inner join ProductMst(Nolock) p ON p.ProductId = dtl.ProductId                    
      WHERE hdr.OrderStatusId IN (4,5) AND (dtl.IsCreditPointsProcess = 0 OR dtl.IsCreditPointsProcess IS NULL)                      
      ORDER BY hdr.UserPurchaseHdrId DESC                   
                  
 WHILE EXISTS (SELECT 1 FROM #OrderDetail)                      
 BEGIN                      
                  
  DECLARE @OrderUserId int,                        
    @UserPurchaseDtlId bigint,                      
    @CreditPerc float,                      
    @CreditPoints numeric(18,2),                      
    @includeSelf BIT,                  
    @OrderCreatedDate DATETIME,                
    @UserPurchaseHdrId BIGINT,        
	@IsCODOrder BIT,        
	@OrderModifiedDate DATETIME        
                  
   SELECT TOP 1 @OrderModifiedDate = ModifiedDate,@IsCODOrder = IsCODOrder,@UserPurchaseHdrId = UserPurchaseHdrId,@OrderCreatedDate = CreatedDate , @OrderUserId = UserId, @UserPurchaseDtlId = UserPurchaseDtlId,@CreditPerc = CreditPerc, @CreditPoints=CreditPoints, @includeSelf = IsRepurchaseOrder FROM #OrderDetail                  
                   
 IF @IsCODOrder = 1        
 BEGIN        
  SET @OrderCreatedDate = (CASE WHEN @OrderModifiedDate IS NOT NULL then @OrderModifiedDate else GETDATE() end)        
 END        
                
 DELETE FROM #TblPointsCreditDtl    
    
 Insert into #TblPointsCreditDtl     
 EXEC dbo.FN_GET_ALL_PARENT_USER_TREE_SP @OrderUserId,@includeSelf    
    
   INSERT INTO [dbo].[PointsCreditDtl]                      
    ([UserId]                      
    ,[UserPurchaseDtlId]        
    ,[CreditPerc]                      
    ,[CreditDate]                      
    ,[CreditPoints]                      
    ,[IsActive]                      
    ,[CreatedBy]                      
    ,[CreatedDate])                  
   SELECT t.UserId,@UserPurchaseDtlId,@CreditPerc,@OrderCreatedDate,@CreditPoints,1,@OrderUserId,GETDATE() FROM #TblPointsCreditDtl t                  
   WHERE NOT EXISTS (SELECT TOP 1 1 FROM PointsCreditDtl(Nolock) pcd WHERE pcd.UserId = t.UserId AND pcd.UserPurchaseDtlId = @UserPurchaseDtlId)                   
                  
   UPDATE UserPurchaseDtl SET IsCreditPointsProcess = 1, ModifiedDate = GETDATE() WHERE UserPurchaseDtlId = @UserPurchaseDtlId                  

	-----------------------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @UserId int, @LeftMatchingBV FLOAT = 0, @RightMatchingBV FLOAT = 0, @CommonMatchingBVRmn FLOAT = 0, @ExistTotalMatchingBV FLOAT = 0, @DiffRmnBV FLOAT = 0
	-------------------------------------------------------------------------------------------------------------
	DECLARE UserWalletCursor CURSOR FOR

		SELECT UserId FROM #TblPointsCreditDtl (NOLOCK)

	OPEN UserWalletCursor;
	FETCH NEXT FROM UserWalletCursor INTO @UserId

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

	----------------------------------------------------------------------------------------------------------------------------------------------
		--IF NOT EXISTS(SELECT 1 FROM UserWalletMst WITH(NOLOCK) WHERE UserId = @UserId)
		BEGIN

			SET @LeftMatchingBV = ISNULL((
				SELECT SUM(LeftBV) FROM (
					Select * from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,NULL,NULL)
				) AS TotalLeftBV
			), 0)

			SET @RightMatchingBV = ISNULL((
				SELECT SUM(RightBV) FROM (
					Select * from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,NULL,NULL)
				) AS TotalLeftBV
			), 0)

			SET @CommonMatchingBVRmn = (CASE WHEN @LeftMatchingBV >= @RightMatchingBV THEN @RightMatchingBV ELSE @LeftMatchingBV END)

			IF EXISTS(SELECT * FROM UserWalletMst(NOLOCK) WHERE UserId = @UserId)
			BEGIN

				SET @ExistTotalMatchingBV = (SELECT TotalMatchingBV FROM UserWalletMst(NOLOCK) WHERE UserId = @UserId)
				IF(@ExistTotalMatchingBV <> CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)))
				BEGIN
					SET @DiffRmnBV = CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)) - @ExistTotalMatchingBV

					UPDATE	UserWalletMst
					SET		MatchingBV = MatchingBV + CAST(@DiffRmnBV AS DECIMAL(18, 2))
							, TotalMatchingBV = TotalMatchingBV + CAST(@DiffRmnBV AS DECIMAL(18, 2))
					WHERE UserWalletMst.UserId = @UserId
				END
			END
			ELSE
			BEGIN
				INSERT INTO UserWalletMst (UserId, MatchingBV, TotalMatchingBV)
				VALUES(@UserId, CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)), CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)))
			END
		END
		-------------------------------------------------------------------------------------------------------------

		FETCH NEXT FROM UserWalletCursor INTO @UserId
	END

	CLOSE UserWalletCursor
	DEALLOCATE UserWalletCursor
	-----------------------------------------------------------------------------------------------------------------------------------------------------------
                     
IF NOT EXISTS(SELECT TOP 1 1 FROM UserPurchaseDtl(Nolock) WHERE UserPurchaseHdrId = @UserPurchaseHdrId AND (IsCreditPointsProcess = 0 OR IsCreditPointsProcess IS NULL))                
   BEGIN      
     
   UPDATE t   
  SET   
   t.DirectMatchingVolume = (CASE WHEN d.IsRepurchaseOrder = 0 THEN p.DirectMatchingAmt * t.Qty  
            ELSE t.DirectMatchingVolume END),
   t.DirectPurchaseVolume = (CASE WHEN d.IsRepurchaseOrder = 0 THEN p.DirectMatchingAmt * t.Qty  
            ELSE t.DirectPurchaseVolume END),
   t.RepurchaseMatchingVolume = (CASE WHEN d.IsRepurchaseOrder = 1 THEN p.RepurchaseMatchingAmt * t.Qty  
            ELSE t.RepurchaseMatchingVolume END)  
 FROM   
  UserPurchaseDtl t inner join UserPurchaseHdr d ON t.UserPurchaseHdrId = d.UserPurchaseHdrId   
  Inner join ProductMst p ON t.ProductId = p.ProductId  
  WHERE   
   d.UserPurchaseHdrId = @UserPurchaseHdrId  
   AND t.DirectMatchingVolume IS NULL AND t.RepurchaseMatchingVolume IS NULL  
  
  
   EXEC PROC_INSERT_ALL_PARENT_USER_NOTIFICATIONMST @OrderUserId,1,1          
   --Insert record into UserStatusUpdateQueue tbl...          
   INSERT INTO [dbo].[UserStatusUpdateQueue]          
    ([OrderUserId]          
    ,[UserPurchaseHdrId]          
    ,[CreatedBy]          
    ,[CreatedDate])          
    VALUES (@OrderUserId,@UserPurchaseHdrId,-1,GETDATE())          
   END                
                   
   DELETE FROM #OrderDetail WHERE UserPurchaseDtlId = @UserPurchaseDtlId                  
                  
 END                      
                    
END 