/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT]    Script Date: 06-04-2021 13:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT '2019-09-20','2020-02-26'
--exec PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT '2019-09-20','2019-12-26'
ALTER Procedure [dbo].[PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT]
(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	--Declare @FromDate date='2019-09-20'
	--Declare @ToDate date='2020-12-26'
	--Declare @UserId int=0
	
	Select * from (
		Select	um.UserId,um.UserName,um.RefferalCode,um.FullName,
				cscd.NoOfVolts, cscd.TotalNoOfVolts,
				--SUM(case when (cast(cscd.CreditDate as date)>=@FromDate or @FromDate IS null) and  (cast(cscd.CreditDate as date)<=@ToDate or @ToDate IS null) then CreditAmt end) as VoltIncome,
				--SUM(CreditAmt) as TotalVoltIncome
				SUM(case when (cast(VoltMilesTbl.CreditDate as date)>=@FromDate or @FromDate IS null) and (cast(VoltMilesTbl.CreditDate as date)<=@ToDate or @ToDate IS null) then VoltMilesTbl.VoltMiles end) as VoltMiles,
				SUM(VoltMilesTbl.VoltMiles) as TotalVoltMiles
		from UserMst as um
		INNER JOIN (
			SELECT	COUNT(case when (cast(cscd.CreditDate as date)>=@FromDate or @FromDate IS null) and  (cast(cscd.CreditDate as date)<=@ToDate or @ToDate IS null) then CompanyShareCreditDtlId end) as NoOfVolts,
					COUNT(CompanyShareCreditDtlId) as TotalNoOfVolts
					, cscd.UserId
				FROM CompanyShareCreditDtl AS cscd
				GROUP BY cscd.UserId
		) AS cscd on um.UserId=cscd.UserId
		INNER JOIN (
			select UserId, CreditAmount AS VoltMiles, CreditDate from MilesCreditDtl where InsertType = 2
		) AS VoltMilesTbl ON VoltMilesTbl.UserId = um.UserId
		group by um.UserId,um.UserName,um.RefferalCode,um.FullName,cscd.NoOfVolts, cscd.TotalNoOfVolts
	) as volt
	order by NoOfVolts desc
END