/****** Object:  StoredProcedure [dbo].[PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL]    Script Date: 01-04-2021 13:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL]          
(          
 @OrderUserId int,          
 @UserPurchaseDtlId bigint,        
 @CreditPerc float,        
 @CreditPoints numeric(18,2),        
 @includeSelf BIT        
)          
AS          
BEGIN          
          
 IF OBJECT_ID('tempdb..#TblPointsCreditDtl') IS NOT NULL        
 BEGIN        
  DROP TABLE #TblPointsCreditDtl        
 END        
         
         
 Create Table #TblPointsCreditDtl        
 (        
  UserId int        
  ,UserPurchaseDtlId bigint        
  ,CreditPerc float        
  ,CreditDate datetime        
  ,CreditPoints numeric(18,2)        
  ,IsActive bit        
  ,CreatedBy numeric(18,0)        
  ,CreatedDate datetime        
 )          
    
  Declare @OrderDatetime DATETIME  
  SELECT @OrderDatetime = CreatedDate FROM UserPurchaseDtl WHERE UserPurchaseDtlId = @UserPurchaseDtlId;  
        
  INSERT INTO #TblPointsCreditDtl        
  (UserId,UserPurchaseDtlId,CreditPerc,CreditDate,CreditPoints,IsActive,CreatedBy,CreatedDate)        
  SELECT UserId,@UserPurchaseDtlId,@CreditPerc,@OrderDatetime,@CreditPoints,1,@OrderUserId,GETDATE() FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@OrderUserId,@includeSelf)        
        
 INSERT INTO [dbo].[PointsCreditDtl]        
           ([UserId]        
           ,[UserPurchaseDtlId]        
           ,[CreditPerc]        
           ,[CreditDate]        
           ,[CreditPoints]        
           ,[IsActive]        
           ,[CreatedBy]        
           ,[CreatedDate])        
     SELECT t.[UserId]        
           ,t.[UserPurchaseDtlId]        
           ,t.[CreditPerc]        
           ,t.[CreditDate]        
           ,t.[CreditPoints]        
           ,t.[IsActive]        
           ,t.[CreatedBy]        
           ,t.[CreatedDate]        
  FROM #TblPointsCreditDtl t  
  WHERE NOT EXISTS (SELECT TOP 1 1 FROM [PointsCreditDtl] pcd WHERE pcd.UserId = t.UserId AND pcd.UserPurchaseDtlId = t.UserPurchaseDtlId)   

  -----------------------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @UserId int, @LeftMatchingBV FLOAT = 0, @RightMatchingBV FLOAT = 0, @CommonMatchingBVRmn FLOAT = 0, @ExistTotalMatchingBV FLOAT = 0, @DiffRmnBV FLOAT = 0
	-------------------------------------------------------------------------------------------------------------
	DECLARE UserWalletCursor CURSOR FOR

		SELECT UserId FROM #TblPointsCreditDtl (NOLOCK)

	OPEN UserWalletCursor;
	FETCH NEXT FROM UserWalletCursor INTO @UserId

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

	----------------------------------------------------------------------------------------------------------------------------------------------
		--IF NOT EXISTS(SELECT 1 FROM UserWalletMst WITH(NOLOCK) WHERE UserId = @UserId)
		BEGIN

			SET @LeftMatchingBV = ISNULL((
				SELECT SUM(LeftBV) FROM (
					Select * from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,NULL,NULL)
				) AS TotalLeftBV
			), 0)

			SET @RightMatchingBV = ISNULL((
				SELECT SUM(RightBV) FROM (
					Select * from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,NULL,NULL)
				) AS TotalLeftBV
			), 0)

			SET @CommonMatchingBVRmn = (CASE WHEN @LeftMatchingBV >= @RightMatchingBV THEN @RightMatchingBV ELSE @LeftMatchingBV END)

			IF EXISTS(SELECT * FROM UserWalletMst(NOLOCK) WHERE UserId = @UserId)
			BEGIN

				SET @ExistTotalMatchingBV = (SELECT TotalMatchingBV FROM UserWalletMst(NOLOCK) WHERE UserId = @UserId)
				IF(@ExistTotalMatchingBV <> CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)))
				BEGIN
					SET @DiffRmnBV = CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)) - @ExistTotalMatchingBV

					UPDATE	UserWalletMst 
					SET		MatchingBV = MatchingBV + CAST(@DiffRmnBV AS DECIMAL(18, 2))
							, TotalMatchingBV = TotalMatchingBV + CAST(@DiffRmnBV AS DECIMAL(18, 2))
					WHERE UserWalletMst.UserId = @UserId
				END
			END
			ELSE
			BEGIN
				INSERT INTO UserWalletMst (UserId, MatchingBV, TotalMatchingBV)
				VALUES(@UserId, CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)), CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)))
			END
		END
		-------------------------------------------------------------------------------------------------------------

		FETCH NEXT FROM UserWalletCursor INTO @UserId
	END

	CLOSE UserWalletCursor
	DEALLOCATE UserWalletCursor
	-----------------------------------------------------------------------------------------------------------------------------------------------------------
          
END     

