TRUNCATE TABLE UserMilesDtl

------------------------------
INSERT INTO UserMilesDtl
SELECT * FROM (
	SELECT	MilesCredit.UserId, MilesCredit.CreditAmount, ISNULL(MilesDebit.DebitAmount, 0) AS DebitAmount
	FROM (
		select UserId, SUM(CreditAmount) AS CreditAmount from MilesCreditDtl GROUP BY UserId
	) AS MilesCredit
	LEFT JOIN (
		select UserId, SUM(DebitAmount) AS DebitAmount from MilesDebitDtl GROUP BY UserId
	) AS MilesDebit ON MilesDebit.UserId = MilesCredit.UserId
) AS Final
WHERE Final.UserId NOT IN (SELECT UserMilesDtl.UserId FROM UserMilesDtl(NOLOCK))