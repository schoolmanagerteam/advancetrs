
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_UserMilesDtl_TotalDebitMiles]') AND type = 'D')
BEGIN
	ALTER TABLE [dbo].[UserMilesDtl] DROP CONSTRAINT [DF_UserMilesDtl_TotalDebitMiles]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_UserMilesDtl_TotalCreditMiles]') AND type = 'D')
BEGIN
	ALTER TABLE [dbo].[UserMilesDtl] DROP CONSTRAINT [DF_UserMilesDtl_TotalCreditMiles]
END


/****** Object:  Table [dbo].[UserMilesDtl]    Script Date: 27-03-2021 17:53:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserMilesDtl]') AND type in (N'U'))
	DROP TABLE [dbo].[UserMilesDtl]

CREATE TABLE [dbo].[UserMilesDtl](
	[UserMilesDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TotalCreditMiles] [float] NOT NULL,
	[TotalDebitMiles] [float] NOT NULL,
 CONSTRAINT [PK_UserMilesDtl] PRIMARY KEY CLUSTERED 
(
	[UserMilesDtlId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[UserMilesDtl] ADD  CONSTRAINT [DF_UserMilesDtl_TotalCreditMiles]  DEFAULT ((0)) FOR [TotalCreditMiles]

ALTER TABLE [dbo].[UserMilesDtl] ADD  CONSTRAINT [DF_UserMilesDtl_TotalDebitMiles]  DEFAULT ((0)) FOR [TotalDebitMiles]
