/****** Object:  StoredProcedure [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]    Script Date: 09-04-2021 22:12:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]
(
@UserId numeric(18,0)=0,
@IsUptoToday bit=1
)
AS
BEGIN

--SELECT *
--FROM	dbo.FN_ADMIN_PENDING_PAYOUT_DETAILS(@UserId)
--order by NetPayable desc
SET FMTONLY OFF

Declare @ToDate date=getdate()
IF @IsUptoToday=0
BEGIN
	Select @ToDate=EndDate from dbo.fn_GetFirstLastDateOfWeekByDate(DATEADD(d,-7,GETDATE()))
END

Declare @PaymentDtl table(UserId bigint,RefferalCode nvarchar(20),FullName nvarchar(max),PANNo nvarchar(50),BankAccNo nvarchar(50),ISFCCode nvarchar(50)
							,DirectIncome numeric(18,0),RePurchaseIncome numeric(18,0),VoltIncome numeric(18,0)
							,TotalPointsIncome numeric(18,0),TotalPayout numeric(18,0),TotalIncome numeric(18,0)
							,DirectIncomePay numeric(18,0),PointsIncomePay numeric(18,0)
							,TDSPerc numeric(18,2),TDS numeric(18,0),AdminChargePerc numeric(18,2),AdminCharge numeric(18,0)
							,NetPayable numeric(18,0),VoltIncomePay numeric(18,0),LeaderIncome numeric(18,0)
							,UplineSupportIncome numeric(18,0),SilverIncome numeric(18,0),DiamondIncome numeric(18,0),LeaderPointsPay numeric(18,0)
							,UplineSupportPointsPay numeric(18,0),SilverPointsPay numeric(18,0),DiamondPointsPay numeric(18,0),IsRegistrationActivated bit,
							MobileNo numeric(18,0),EmailId nvarchar(max), KYCApproved VARCHAR(5))
	
	Select um.UserId,um.RefferalCode,um.FullName,um.PANNo,um.BankAccNo,um.ISFCCode,
			--ISNULL(purchase.TotalPurchase,0)TotalPurchase,
			ISNULL(DirectIncome,0)DirectIncome,	ISNULL(RePurchaseIncome,0)RePurchaseIncome,
			0 AS VoltIncome,--ISNULL(VoltIncome,0)VoltIncome,
			ISNULL(FinalPoints,0)TotalPointsIncome,ISNULL(TotalPayout,0)TotalPayout,
			ISNULL(DirectIncomePay,0)DirectIncomePay,ISNULL(PointsIncomePay,0)PointsIncomePay,
			ISNULL(VoltIncomePay,0)VoltIncomePay,ISNULL(leader.CreditPoints,0)FinalLeaderPoints,
			ISNULL(uplinesupport.CreditPoints,0)FinalUplineSupportPoints,
			ISNULL(silver.CreditPoints,0)FinalSilverPoints,ISNULL(diamond.CreditPoints,0)FinalDiamondPoints,
			ISNULL(LeaderIncomePay,0)LeaderIncomePay,ISNULL(UplineSupportIncomePay,0)UplineSupportIncomePay,
			ISNULL(SilverIncomePay,0) SilverPointsPay,ISNULL(DiamondIncomePay,0) DiamondIncomePay,
			ISNULL(IsRegistrationActivated,0) IsRegistrationActivated,MobileNo,EmailId,
			(case when (um.IsKYCVerified = 1 and um.IsPANVerified = 1 and um.IsBankDocumentVerified = 1) then 'YES' else 'NO' end) as KYCApproved
	INTO #PaymentDtl
	from UserMst as um
	LEFT JOIN (
	Select UserId,SUM(case when CreditType=1 then CreditAmount end) as DirectIncome 
					,SUM(case when CreditType=2 then CreditAmount end) as RePurchaseIncome
					,SUM(case when CreditType=3 then CreditAmount end) as FranchiseeIncome
		from	WalletCreditDtl 
		WHERE cast(CreditDate as date)<=@Todate
		group by UserId
	) as direct on um.UserId=direct.UserId	
	--LEFT JOIN (
	--		Select	UserId,SUM(CreditAmt) as VoltIncome
	--		from	CompanyShareCreditDtl
	--		WHERE	cast(CreditDate as date)<=@Todate
	--		group by UserId
	--) as volt on um.UserId=volt.UserId
	LEFT JOIN(
		Select	UserId,SUM(DebitAmount) as TotalPayout,SUM(DirectIncomePay)DirectIncomePay
				,0 AS VoltIncomePay--,SUM(VoltIncomePay) as VoltIncomePay
				,SUM(PointsIncomePay)PointsIncomePay, SUM(LeaderIncomePay)LeaderIncomePay, SUM(UplineSupportIncomePay)UplineSupportIncomePay
				,SUM(SilverIncomePay)SilverIncomePay, SUM(DiamondIncomePay)DiamondIncomePay
		from	WalletDebitDtl
		--WHERE cast(DebitDate as date)<=@Todate
		group by UserId
	) as debit on um.UserId=debit.UserId
	--LEFT JOIN dbo.FN_GET_ALL_TOTAL_MATCHING_POINTS(@UserId) as points on um.UserId=points.UserId
	LEFT JOIN(Select UserId,SUM(MatchingPoints)FinalPoints 
			  from MatchingPointsCreditDtl			  
				WHERE cast(CreditDate as date)<=@Todate
			  group by UserId) as points on um.UserId=points.UserId
	LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(1,@UserId,default,@Todate) as leader on um.UserId=leader.UserId
	LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(0,@UserId,default,@Todate) as uplinesupport on um.UserId=uplinesupport.UserId
	LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(0,@UserId,default,@Todate) as silver on um.UserId=silver.UserId
	LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(1,@UserId,default,@Todate) as diamond on um.UserId=diamond.UserId
	Where um.UserId=case when @UserId=0 then um.UserId else @UserId end
			--AND ISNULL(IsRegistrationActivated,0)=1

	Declare @TDSPercWithoutPAN decimal
	Declare @TDSPercWithPAN NUMERIC(18,2)
	Declare @AdminChargePerc decimal
	Declare @TDSDexuctionLimit decimal
	Select @TDSPercWithoutPAN=Value from ConfigMst where [Key]='TDSPercWithoutPAN'
	Select @TDSPercWithPAN=Value from ConfigMst where [Key]='TDSPercWithPAN'
	Select @AdminChargePerc=Value from ConfigMst where [Key]='AdminChargePerc'
	Select @TDSDexuctionLimit=Value from ConfigMst where [Key]='TDSDexuctionLimit'	
	SET @TDSPercWithoutPAN=ISNULL(@TDSPercWithoutPAN,20)
	SET @TDSPercWithPAN=ISNULL(@TDSPercWithPAN,5)
	SET @AdminChargePerc=ISNULL(@AdminChargePerc,5)

	insert into @PaymentDtl(UserId,RefferalCode,FullName,PANNo,BankAccNo,ISFCCode,
						DirectIncome,RePurchaseIncome,VoltIncome,
							TotalPointsIncome,TotalPayout,DirectIncomePay,PointsIncomePay,VoltIncomePay,
							LeaderIncome,UplineSupportIncome,SilverIncome,DiamondIncome,
							LeaderPointsPay,UplineSupportPointsPay,SilverPointsPay,DiamondPointsPay,
							IsRegistrationActivated,MobileNo,EmailId,KYCApproved)
	Select * from #PaymentDtl
	
	
 DECLARE @TDSEffectiveDate DATETIME = (SELECT TOP 1 CAST([Value] AS DATE) from ConfigMst where [Key]='TDSEffectiveDate')
 DECLARE @ConfirRecId INT = (select COUNT(1) + 1 from TDSPercHistory)

 DECLARE @TDHistoryTbl TABLE (TDSPercHistoryId int, PercWithPAN float, PercWithoutPAN float, EffectiveDate DATETIME)
 INSERT INTO @TDHistoryTbl
 select * from (
		select * from TDSPercHistory
		UNION
		SELECT @ConfirRecId, @TDSPercWithPAN, @TDSPercWithoutPAN, @TDSEffectiveDate
 ) AS TDSHistory --where CAST(effectivedate AS DATE) <= CAST('01 apr 2021' AS DATE)  order by EffectiveDate desc, TDSPercHistoryId desc


	Update pd 
	set 
		 TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome+LeaderIncome+UplineSupportIncome+SilverIncome+DiamondIncome),0)
		 --TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome),0)
		--,TDSPerc=case when len(PANNo)>0 then @TDSPercWithPAN else @TDSPercWithoutPAN end
		, TDSPerc = (case when len(PANNo) > 0 
						then (SELECT TOP 1 PercWithPAN FROM @TDHistoryTbl where CAST(effectivedate AS DATE) <= CAST(@ToDate AS DATE) order by effectivedate desc, tdsperchistoryid desc ) 
					ELSE (SELECT TOP 1 PercWithoutPAN FROM @TDHistoryTbl where CAST(effectivedate AS DATE) <= CAST(@ToDate AS DATE) order by effectivedate desc, tdsperchistoryid desc ) 
				end)
		,AdminChargePerc=@AdminChargePerc
	from @PaymentDtl as pd
	


	--SELECT *
	--Added TDS Condition as per govt. norms - If income greater than 14999 than we need to deduct the TDS
	Update pd set TDS=(CASE WHEN TotalIncome > @TDSDexuctionLimit 
								THEN ((TotalIncome-TotalPayout)*TDSPerc/100) 
							ELSE 0 
					   END)
			,AdminCharge=((TotalIncome-TotalPayout)*AdminChargePerc/100),
			NetPayable=TotalIncome-TotalPayout
	FROM	@PaymentDtl as pd

	Select *,(TotalIncome-TotalPayout-TDS-AdminCharge) as FinalPayoutAmt from @PaymentDtl
	order by NetPayable desc
	
	Drop Table #PaymentDtl

END

