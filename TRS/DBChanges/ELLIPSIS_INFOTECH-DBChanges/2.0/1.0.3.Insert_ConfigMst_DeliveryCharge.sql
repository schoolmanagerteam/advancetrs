IF NOT EXISTS (SELECT 1 FROM ConfigMst WHERE [Key] = 'MinimumOrderAmountForDeliveryChargeApply')
BEGIN
	INSERT INTO ConfigMst
	VALUES('MinimumOrderAmountForDeliveryChargeApply','2500',1,1,GETDATE(),1,GETDATE(),1, 'Minimum Order Amount For Delivery Charge Apply')
END

IF NOT EXISTS (SELECT 1 FROM ConfigMst WHERE [Key] = 'OrderDeliveryCharge')
BEGIN
	INSERT INTO ConfigMst
	VALUES('OrderDeliveryCharge','100',1,1,GETDATE(),1,GETDATE(),1, 'Order Delivery Charge')
END

IF NOT EXISTS (SELECT 1 FROM ConfigMst WHERE [Key] = 'TDSEffectiveDate')
BEGIN
	INSERT INTO ConfigMst
	VALUES('TDSEffectiveDate','01/04/2021',1,1,GETDATE(),1,GETDATE(),1, 'TDS Effective Date')
END

IF NOT EXISTS (SELECT 1 FROM ConfigMst WHERE [Key] = 'PercentageOfMilesLapsedIn3Month')
BEGIN
	INSERT INTO ConfigMst
	VALUES('PercentageOfMilesLapsedIn3Month','01/04/2021',1,1,GETDATE(),1,GETDATE(),1, 'Percentage Of Miles Lapsed In 3 Month(%)')
END

IF NOT EXISTS (SELECT 1 FROM ConfigMst WHERE [Key] = 'PercentageOfMilesLapsedIn6Month')
BEGIN
	INSERT INTO ConfigMst
	VALUES('PercentageOfMilesLapsedIn6Month','01/04/2021',1,1,GETDATE(),1,GETDATE(),1, 'Percentage Of Miles Lapsed In 6 Month(%)')
END

UPDATE ConfigMst set IsEditable = 1, Description = 'TDS Perc With PAN', Value = '5' where [Key] = 'TDSPercWithPAN'
UPDATE ConfigMst set IsEditable = 1, Description = 'TDS Perc Without PAN' where [Key] = 'TDSPercWithoutPAN'