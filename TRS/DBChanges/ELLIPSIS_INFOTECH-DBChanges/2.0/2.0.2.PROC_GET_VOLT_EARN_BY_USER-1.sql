
/****** Object:  StoredProcedure [dbo].[PROC_GET_VOLT_EARN_BY_USER]    Script Date: 05-04-2021 14:14:26 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'PROC_GET_VOLT_EARN_BY_USER')
BEGIN
	DROP PROCEDURE [dbo].[PROC_GET_VOLT_EARN_BY_USER]
END

/****** Object:  StoredProcedure [dbo].[PROC_GET_VOLT_EARN_BY_USER]    Script Date: 05-04-2021 14:14:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[PROC_GET_VOLT_EARN_BY_USER]
(  
@UserId int = 0,  
@FromDate datetime=null,  
@ToDate datetime=null  
)  
AS  
BEGIN  
 --Declare @UserId int=14  
 --Declare @FromDate datetime  
 --Declare @ToDate datetime  
	
		 SELECT DISTINCT wum.RefferalCode AS UserCode, wum.FullName AS UserName
				, COUNT(1) OVER(PARTITION BY wcd.UserId) AS VoltCount, CAST(0 AS NUMERIC(18, 2)) AS Miles
				, CAST(0 AS NUMERIC(18, 2)) AS BVMultiplier
		 from	CompanyShareCreditDtl(Nolock) as wcd 
				INNER JOIN UserPurchaseHdr(Nolock) as uph on wcd.UserPurchaseHdrId=uph.UserPurchaseHdrId  
						AND (cast(wcd.CreditDate as DATE)>=@FromDate or @FromDate is null)   
						AND (cast(wcd.CreditDate as DATE)<=@ToDate or @ToDate is null)  
		 INNER JOIN UserMst(Nolock) as um on uph.UserId=um.UserId  
		 INNER JOIN UserMst(Nolock) as wum on wcd.UserId=wum.UserId  
		 Where wcd.UserId=case when @UserId=0 then wcd.UserId else @UserId end  

END
