/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]    Script Date: 06-04-2021 15:22:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]  
(  
@UserId int  
)  
AS  
BEGIN  
 Declare @date date=getdate()  
 Declare @i int=6  
 Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20),Income float--, MatchingInCome FLOAT
		)  
 --Select @date,MONTH(@date),YEAR(@date)  
 While @i>=1  
 BEGIN  
    
  Declare @FromDate datetime=DATEADD(mm, DATEDIFF(mm, 0, @date), 0)  
  Declare @ToDate datetime=DATEADD(ms, -3, DATEADD(mm, DATEDIFF(m, 0, @date) + 1, 0))  
    
  insert into @MonthYear  
  Select	@i as Id, MONTH(@date) as MONTH, YEAR(@date) as YEAR, LEFT(DATENAME(month, @date), 3) + '-' +  DATENAME(year,  @date) as MonthYear
			,Income--, MatchingIncome
  from	dbo.[FN_GET_USER_TOTAL_EARNING_DATE_RANGE](@UserId,@FromDate,@ToDate)  
    
  set @date=DATEADD(M,-1,@date)  
  SET @i=@i-1  
 END  

 DECLARE @StartDate DATE = DATEADD(mm, DATEDIFF(mm, 0, DATEADD(month, -5, getdate())), 0)
 Declare @EndDate datetime=DATEADD(ms, -3, DATEADD(mm, DATEDIFF(m, 0, getdate()) + 1, 0))  

SELECT Final.Id, Final.Month, Final.Year, Final.MonthYear, SUM(Final.Income) AS Income, SUM(Final.MatchingIncome) AS MatchingIncome FROM (
	Select *, 0 AS MatchingIncome from @MonthYear
 
	UNION
 
	SELECT	ROW_NUMBER() OVER(order by [Year],[Month]) AS Id, [Month], [Year]
			, (Convert(char(3), DateName( month , DateAdd( month , [Month] , 0 ) -1)) + '-' + CAST([Year] AS VARCHAR(10))) AS MonthYear
			, (CASE WHEN LeftMatchingBV >= RightMatchingBV THEN RightMatchingBV ELSE LeftMatchingBV END) AS Income
			, (CASE WHEN LeftMatchingBV >= RightMatchingBV THEN RightMatchingBV ELSE LeftMatchingBV END) AS MatchingIncome 
	FROM	FN_GET_USER_MATCHING_POINTS(@UserId, @StartDate, @EndDate)
) AS Final
GROUP BY Final.Id, Final.Month, Final.Year, Final.MonthYear
 order by [Year] desc,[Month] desc  

END  
  

