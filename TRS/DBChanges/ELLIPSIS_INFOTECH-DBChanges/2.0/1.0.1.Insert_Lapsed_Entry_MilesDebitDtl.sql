IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[FK_MilesDebitDtl_UserPurchaseHdrId]') AND type = 'F')
BEGIN
	ALTER TABLE [dbo].[MilesDebitDtl] DROP CONSTRAINT [FK_MilesDebitDtl_UserPurchaseHdrId]
END

DECLARE @AdminUserId INT = (select TOP 1 UserMst.UserId from dbo.UserMst where username = 'sadmin' and RefferalUserId = 0)

INSERT INTO MilesDebitDtl
select	UserMilesDtl.UserId, 0, GETDATE(), (CreditAmount - DebitAmount), 1, @AdminUserId, GETDATE(), 1
--from	UserMilesDtl
FROM (
	SELECT * FROM (
		SELECT	MilesCredit.UserId, MilesCredit.CreditAmount, ISNULL(MilesDebit.DebitAmount, 0) AS DebitAmount
		FROM (
			select UserId, SUM(CreditAmount) AS CreditAmount from MilesCreditDtl GROUP BY UserId
		) AS MilesCredit
		LEFT JOIN (
			select UserId, SUM(DebitAmount) AS DebitAmount from MilesDebitDtl GROUP BY UserId
		) AS MilesDebit ON MilesDebit.UserId = MilesCredit.UserId
	) AS Final
) AS UserMilesDtl
WHERE	(CreditAmount - DebitAmount) <> 0