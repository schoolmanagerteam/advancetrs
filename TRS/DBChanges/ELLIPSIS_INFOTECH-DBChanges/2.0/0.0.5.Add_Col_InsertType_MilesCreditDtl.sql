IF NOT EXISTS (SELECT	1
			   FROM		INFORMATION_SCHEMA.COLUMNS
			   WHERE	TABLE_NAME = 'MilesCreditDtl'
						AND COLUMN_NAME = 'InsertType')
BEGIN
	BEGIN TRANSACTION
			ALTER TABLE MilesCreditDtl
			ADD InsertType INT NOT NULL DEFAULT(0)
		
	COMMIT TRANSACTION

	BEGIN TRANSACTION
		EXEC('UPDATE MilesCreditDtl SET InsertType = 0')
	COMMIT TRANSACTION
END



IF EXISTS ( SELECT 1 FROM SYS.FN_LISTEXTENDEDPROPERTY('MS_Description'
        , 'SCHEMA', 'dbo' , 'TABLE', 'MilesCreditDtl' , 'COLUMN', 'InsertType' )
)
BEGIN
	EXEC sys.sp_Updateextendedproperty  
	@name=N'MS_Description'
	, @value=N'0=RegisterUser, 1=FileUpload, 2=VoltIncome' 
	, @level0type=N'SCHEMA' ,@level0name=N'dbo'
	, @level1type=N'TABLE'	,@level1name=N'MilesCreditDtl'
	, @level2type=N'COLUMN' ,@level2name=N'InsertType'
	
END
ELSE
BEGIN
	EXEC sys.sp_addextendedproperty 
	@name=N'MS_Description'
	, @value=N'0=RegisterUser, 1=FileUpload, 2=VoltIncome'
	, @level0type=N'SCHEMA' ,@level0name=N'dbo'
	, @level1type=N'TABLE'	,@level1name=N'MilesCreditDtl'
	, @level2type=N'COLUMN' ,@level2name=N'InsertType'
END