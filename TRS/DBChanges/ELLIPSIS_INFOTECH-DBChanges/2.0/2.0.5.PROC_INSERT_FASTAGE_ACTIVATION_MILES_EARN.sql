IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'PROC_INSERT_FASTAGE_ACTIVATION_MILES_EARN')
BEGIN
	DROP PROCEDURE PROC_INSERT_FASTAGE_ACTIVATION_MILES_EARN
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Harshad Chauhan>
-- Create date: <02 Apr 2021>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PROC_INSERT_FASTAGE_ACTIVATION_MILES_EARN]
	-- Add the parameters for the stored procedure here
	@UserId INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	DECLARE @TodayDate DATE = GETDATE()
	DECLARE @MinRegDate DATE = DATEADD(DAY, -6, GETDATE())
	DECLARE @AdminUserId INT = (SELECT TOP 1 UserId FROM UserMst(NOLOCK) WHERE RefferalUserId = 0 AND UserName = 'sadmin')
	DECLARE @FastageConfigBV FLOAT = (SELECT TOP 1 Value FROM ConfigMst(NOLOCK) WHERE [Key] = 'VoltIncomeBV')

	DECLARE @FastageActivationTbl TABLE(UserId INT, CreditMiles FLOAT)

	---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT INTO @FastageActivationTbl

	SELECT RefferalUserId, 5000 AS CreditMiles FROM (
		SELECT	UserMst.RefferalUserId, (SUM(ISNULL(DirectMatchingVolume, 0)) + SUM(ISNULL(RepurchaseMatchingVolume, 0))) AS FastageActivationBV
		FROM	UserPurchaseDtl (NOLOCK)
				INNER JOIN UserPurchaseHdr (NOLOCK) ON UserPurchaseHdr.UserPurchaseHdrId = UserPurchaseDtl.UserPurchaseHdrId
									AND UserPurchaseHdr.IsActive = 1
				INNER JOIN UserMst (NOLOCK) ON UserMst.UserId = UserPurchaseHdr.UserId
				INNER JOIN UserMst (NOLOCK) AS RefferUser ON UserMst.RefferalUserId = RefferUser.UserId
		WHERE	UserPurchaseDtl.IsActive = 1
				AND	UserPurchaseHdr.IsRepurchaseOrder = 0
				AND	CAST(UserPurchaseHdr.OrderDate AS DATE) BETWEEN CAST(RefferUser.RegistrationDate AS DATE) AND CAST(DATEADD(DAY, 6, RefferUser.RegistrationDate) AS DATE)
				AND (UserMst.RefferalUserId IN (
						SELECT	TOP 10000 UserMst.UserId 
						FROM	UserMst (NOLOCK)
						WHERE	UserMst.IsFastActMilesEarned = 0 
								AND CAST(RegistrationDate AS DATE) BETWEEN CAST(@MinRegDate AS DATE) AND CAST(@TodayDate AS DATE)
								AND (@UserId = 0 OR UserMst.UserId = @UserId)
					)
				)
		GROUP BY UserMst.RefferalUserId
	) AS FastageActivationTbl
	WHERE FastageActivationTbl.FastageActivationBV > @FastageConfigBV

	--SELECT * FROM @FastageActivationTbl
	------------------------------------------------------------------------------------------------
	IF EXISTS(SELECT 1 FROM @FastageActivationTbl)
	BEGIN
		INSERT INTO MilesCreditDtl(UserId, UserIdFrom, CreditDate, CreditAmount, IsActive, CreatedBy, CreatedDate, UsedCreditAmount)
		SELECT UserId, @AdminUserId, GETDATE(), CreditMiles, 1, @AdminUserId, GETDATE(), 0 FROM @FastageActivationTbl

		------------------------------------------------------------------------------------------------

		UPDATE UM_Dtl
		SET UM_Dtl.TotalCreditMiles = UM_Dtl.TotalCreditMiles + FA_Tbl.CreditMiles
		FROM UserMilesDtl AS UM_Dtl
		INNER JOIN @FastageActivationTbl AS FA_Tbl ON FA_Tbl.UserId = UM_Dtl.UserId

		------------------------

		INSERT INTO UserMilesDtl(UserId, TotalCreditMiles, TotalDebitMiles)
		SELECT fa_tbl.UserId, fa_tbl.CreditMiles, 0 
		FROM @FastageActivationTbl AS fa_tbl
		WHERE fa_tbl.UserId NOT IN (SELECT UserMilesDtl.UserId FROM UserMilesDtl (NOLOCK))

		------------------------------------------------------------------------------------------------
		UPDATE UM_Dtl
		SET UM_Dtl.IsFastActMilesEarned = 1
		FROM UserMst AS UM_Dtl
		INNER JOIN @FastageActivationTbl AS FA_Tbl ON FA_Tbl.UserId = UM_Dtl.UserId

		------------------------------------------------------------------------------------------------		
		INSERT INTO [dbo].[NotificationMst]  
				   ([NotificationTypeId]  
				   ,[UserId]  
				   ,[Notification]  
				   ,[IsActive]  
				   ,[CreatedBy]  
				   ,[CreatedDate]  
				   ,[ModifiedBy]  
				   ,[ModifiedDate])  
			 SELECT 1  
				   ,[UserId]  
				   , CAST(CreditMiles AS nvarchar(100)) + ' miles has been credited'
				   ,1
				   ,@AdminUserId  
				   ,GETDATE()
				   ,@AdminUserId  
				   ,GETDATE()
		  FROM @FastageActivationTbl  
		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	END
END
