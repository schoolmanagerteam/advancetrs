UPDATE ConfigMst set value = '40' where [Key] = 'PercentageOfMilesLapsedIn3Month'
UPDATE ConfigMst set value = '60' where [Key] = 'PercentageOfMilesLapsedIn6Month'

IF NOT EXISTS(SELECT 1 FROM ServiceRunningStatus WHERE ServiceName = 'FastageMilesCreditProcess')
BEGIN
	INSERT INTO ServiceRunningStatus (ServiceName, RunningStatus, IsActive)
	VALUES ('FastageMilesCreditProcess', '2021-04-23 09:05:43.787', 1)
END

IF NOT EXISTS(SELECT 1 FROM ServiceRunningStatus WHERE ServiceName = 'UserMilesLapsedProcess')
BEGIN
	INSERT INTO ServiceRunningStatus (ServiceName, RunningStatus, IsActive)
	VALUES ('UserMilesLapsedProcess', '2021-04-23 09:05:43.787', 1)
END