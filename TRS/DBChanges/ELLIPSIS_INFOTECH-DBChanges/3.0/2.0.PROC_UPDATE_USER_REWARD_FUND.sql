IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'PROC_UPDATE_USER_REWARD_FUND')
BEGIN
	DROP PROCEDURE PROC_UPDATE_USER_REWARD_FUND
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<HARSHAD CHAUHAN>
-- Create date: <21 Apr 2021>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PROC_UPDATE_USER_REWARD_FUND
	-- Add the parameters for the stored procedure here
	@UserId  INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		DECLARE @LeftMatchingBV FLOAT = 0, @RightMatchingBV FLOAT = 0, @CommonMatchingBVRmn FLOAT = 0, @ExistTotalMatchingBV FLOAT = 0, @DiffRmnBV FLOAT = 0
		----------------------------------------------------------------------------------------------------------------------------------------------
		--IF NOT EXISTS(SELECT 1 FROM UserWalletMst WITH(NOLOCK) WHERE UserId = @UserId)
		BEGIN

			--SET @LeftMatchingBV = ISNULL((
			--	SELECT SUM(LeftBV) FROM (
			--		Select * from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,NULL,NULL)
			--	) AS TotalLeftBV
			--), 0)

			--SET @RightMatchingBV = ISNULL((
			--	SELECT SUM(RightBV) FROM (
			--		Select * from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,NULL,NULL)
			--	) AS TotalLeftBV
			--), 0)

			select @LeftMatchingBV = ISNULL(sum(ISNULL(LeftMatchingBV, 0)), 0), @RightMatchingBV = ISNULL(sum(ISNULL(RightMatchingBV, 0)), 0) 
			from FN_GET_USER_MATCHING_POINTS(@UserId, null, null)

			SET @CommonMatchingBVRmn = (CASE WHEN @LeftMatchingBV >= @RightMatchingBV THEN @RightMatchingBV ELSE @LeftMatchingBV END)

			IF EXISTS(SELECT * FROM UserWalletMst(NOLOCK) WHERE UserId = @UserId)
			BEGIN

				SET @ExistTotalMatchingBV = (SELECT TotalMatchingBV FROM UserWalletMst(NOLOCK) WHERE UserId = @UserId)
				IF(@ExistTotalMatchingBV <> CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)))
				BEGIN
					SET @DiffRmnBV = CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)) - @ExistTotalMatchingBV

					UPDATE	UserWalletMst
					SET		MatchingBV = MatchingBV + CAST(@DiffRmnBV AS DECIMAL(18, 2))
							, TotalMatchingBV = TotalMatchingBV + CAST(@DiffRmnBV AS DECIMAL(18, 2))
					WHERE UserWalletMst.UserId = @UserId
				END
			END
			ELSE
			BEGIN
				INSERT INTO UserWalletMst (UserId, MatchingBV, TotalMatchingBV)
				VALUES(@UserId, CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)), CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)))
			END
		END
		-------------------------------------------------------------------------------------------------------------
END
GO
