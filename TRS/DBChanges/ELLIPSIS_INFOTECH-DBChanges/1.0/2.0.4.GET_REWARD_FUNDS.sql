IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'PROC_GET_REWARD_FUNDS')
BEGIN
	DROP PROCEDURE PROC_GET_REWARD_FUNDS
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Harshad Chauhan>
-- Create date: <23 Mar 2021>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PROC_GET_REWARD_FUNDS
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	UserMst.RefferalCode AS Id_No, UserMst.FullName AS UserName
			, CAST(UserWalletMst.TotalMatchingBV AS DECIMAL(18, 0)) AS TotalMatchingBV
			, CAST(UserWalletMst.MatchingBV AS DECIMAL(18, 0)) AS RewardBV
			, CAST((((UserWalletMst.MatchingBV * 4) / 100) * 10) AS DECIMAL(18, 0)) AS Budget
	FROM	UserWalletMst
			INNER JOIN UserMst ON UserMst.UserId = UserWalletMst.UserId
	WHERE	UserWalletMst.TotalMatchingBV > 0
END
GO
