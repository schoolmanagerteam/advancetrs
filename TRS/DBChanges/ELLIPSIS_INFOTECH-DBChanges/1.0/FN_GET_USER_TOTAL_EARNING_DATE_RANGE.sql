/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_TOTAL_EARNING_DATE_RANGE]    Script Date: 22-03-2021 14:36:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Function [dbo].[FN_GET_USER_TOTAL_EARNING_DATE_RANGE]
(
@UserId int=0,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(UserId numeric(18,0), Income numeric(18,0), MatchingIncome numeric(18,0))
AS
BEGIN

	insert into @CreditDtl
	Select a.UserId,SUM(Income)as Income, SUM(MatchingIncome)as MatchingIncome from (
		--Leader Create Bonus
		Select UserId,CreditPoints as Income, 0 as MatchingIncome 
		from dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE(1,@UserId,@FromDate,@ToDate)
	
		Union ALL
		--Upline Support Bonus
		Select UserId,CreditPoints as Income, 0 as MatchingIncome 
		from dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE(0,@UserId,@FromDate,@ToDate)
	
		Union ALL
		--Leader Create Bonus + Diamond
		Select UserId,CreditPoints as Income, 0 as MatchingIncome 
		from dbo.[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS](1,@UserId,@FromDate,@ToDate)
	
		Union ALL
		--Upline Support Bonus + Silver
		Select UserId,CreditPoints as Income, 0 as MatchingIncome 
		from dbo.[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS](0,@UserId,@FromDate,@ToDate)
	
		Union ALL
		--Direct Referal
		Select @UserId as UserId,SUM(CreditAmount) as Income , 0 as MatchingIncome 
		from WalletCreditDtl 
		Where UserId=@UserId 
			AND cast(CreditDate as date)>=case when @FromDate is null then cast(CreditDate as date) else @FromDate end
			AND cast(CreditDate as date)<=case when @ToDate is null then cast(CreditDate as date) else @ToDate end 
			and IsActive=1  	
		group by UserId
		
		UNION ALL
		--Weekly Charge up Bonus
		Select UserId,SUM(CreditAmt) as Income, 0 as MatchingIncome 
		from CompanyShareCreditDtl 
		Where UserId=@UserId 
			AND cast(CreditDate as date)>=case when @FromDate is null then cast(CreditDate as date) else @FromDate end
			AND cast(CreditDate as date)<=case when @ToDate is null then cast(CreditDate as date) else @ToDate end  	
		group by UserId

		--Union ALL
		---- Matching Income
		--Select @UserId as UserId,SUM(MatchingPoints) as Income , 0 as MatchingIncome 
		--from MatchingPointsCreditDtl 
		--Where UserId=@UserId 
		--	AND cast(CreditDate as date)>=case when @FromDate is null then cast(CreditDate as date) else @FromDate end
		--	AND cast(CreditDate as date)<=case when @ToDate is null then cast(CreditDate as date) else @ToDate end 
		--group by UserId
		
		--Union ALL
		---- Matching Income Saperate Column
		--Select @UserId as UserId, 0 AS Income, SUM(MatchingPoints) as MatchingIncome 
		--from MatchingPointsCreditDtl 
		--Where UserId=@UserId 
		--	AND cast(CreditDate as date)>=case when @FromDate is null then cast(CreditDate as date) else @FromDate end
		--	AND cast(CreditDate as date)<=case when @ToDate is null then cast(CreditDate as date) else @ToDate end 
		--group by UserId
		
		Union ALL
		--Matching Income
		Select @UserId AS Userid, SUM(LeftPoints) AS Income, 0 AS MatchingIncome from [FN_GET_USER_LEFT_SIDE_POINTS](@UserId,@FromDate,@ToDate)

		Union ALL
		--Matching Income Saperate Column
		Select @UserId AS Userid, 0 AS Income, SUM(LeftPoints) AS MatchingIncome from [FN_GET_USER_LEFT_SIDE_POINTS](@UserId,@FromDate,@ToDate)
		
	) as a
	
	group by UserId

	RETURN
END

