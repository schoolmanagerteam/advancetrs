/****** Object:  UserDefinedFunction [dbo].[FN_GET_ADMIN_PROFITE_DETAILS]    Script Date: 19-03-2021 11:02:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from dbo.[FN_GET_ADMIN_PROFITE_DETAILS]('2019-07-01','2019-07-31')
ALTER Function [dbo].[FN_GET_ADMIN_PROFITE_DETAILS]
(
@FromDate datetime=null,
@ToDate datetime=null
)
RETURNS @ProfiteDtl table(	TotalSales float,TotalCosting float,GrossProfite float,
							TotalTDS float,TotalAdminCharge float,TotalPayout float,
							TotalExpense float,Profite float,PendingPayout float,
							NetProfite float,TotalVolume float,
							TotalDirectIncomePay float,TotalMatchingIncomePay float,
							TotalVoltIncomePay float,TotalLeaderIncomePay float,
							TotalUplineSupportIncomePay float,TotalBonanzaRewardsPay float,
							TotalTopRetailerPay float,TotalSilverIncomePay float)
AS
BEGIN

	Declare @TotalSales float,@TotalCosting float,@GrossProfite float,
			@TotalTDS float,@TotalAdminCharge float,@TotalPayout float,@TotalExpense float,
			@Profite float,@PendingPayout float,@NetProfite float,@TotalVolume float,
			@TotalDirectIncomePay float,@TotalPointsIncomePay float,@TotalVoltIncomePay float,
			@TotalLeaderIncomePay float,@TotalUplineSupportIncomePay float,@TotalSilverIncomePay float,
			@TotalBonanzaRewardsPay float,@TotalTopRetailerPay float

	Select @TotalSales=ISNULL(SUM(Amount),0),@TotalCosting=ISNULL(SUM(Qty*RegularPrice),0),
		   @TotalVolume=ISNULL(SUM(ISNULL(DirectPurchaseVolume,0)),0)+ISNULL(SUM(ISNULL(RepurchaseMatchingVolume,0)),0)
	from UserPurchaseHdr as uph
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	Where cast(OrderDate as date)>=case when @FromDate is null then cast(OrderDate as date) else cast(@FromDate as date) end 
	  and cast(OrderDate as date)<=case when @ToDate is null then cast(OrderDate as date) else cast(@ToDate as date) end 

	SET @GrossProfite=ISNULL(@TotalSales,0)-ISNULL(@TotalCosting,0)
	SET @TotalVolume=ISNULL(@TotalVolume,0)


	Select @TotalTDS=ISNULL(SUM(TDSAmount),0),@TotalAdminCharge=ISNULL(SUM(AdminCharge),0),@TotalPayout=ISNULL(SUM(NetPayable),0),
		@TotalDirectIncomePay=ISNULL(SUM(DirectIncomePay),0),@TotalPointsIncomePay=ISNULL(SUM(PointsIncomePay),0),@TotalVoltIncomePay=ISNULL(SUM(VoltIncomePay),0),
		@TotalLeaderIncomePay=ISNULL(SUM(LeaderIncomePay),0),@TotalUplineSupportIncomePay=ISNULL(SUM(UplineSupportIncomePay),0),
		@TotalSilverIncomePay=ISNULL(SUM(SilverIncomePay),0)	 	 
	from WalletDebitDtl
	where cast(DebitDate as date)>=case when @FromDate is null then cast(DebitDate as date) else cast(@FromDate as date) end 
	  and cast(DebitDate as date)<=case when @ToDate is null then cast(DebitDate as date) else cast(@ToDate as date) end 
	
	Select @TotalExpense=ISNULL(SUM(Amount),0) from ExpenseMst
	Where cast(ExpenseDate as date)>=case when @FromDate is null then cast(ExpenseDate as date) else cast(@FromDate as date) end 
	  and cast(ExpenseDate as date)<=case when @ToDate is null then cast(ExpenseDate as date) else cast(@ToDate as date) end 

	Select @TotalBonanzaRewardsPay=ISNULL(SUM(PaidAmount),0) from BonanzaRewardDtl
	Where cast(PaidDate as date)>=case when @FromDate is null then cast(PaidDate as date) else cast(@FromDate as date) end 
	  and cast(PaidDate as date)<=case when @ToDate is null then cast(PaidDate as date) else cast(@ToDate as date) end 
	  
	Select @TotalTopRetailerPay=ISNULL(SUM(PaidAmount),0) from TopRetailerDtl
	Where cast(PaidDate as date)>=case when @FromDate is null then cast(PaidDate as date) else cast(@FromDate as date) end 
	  and cast(PaidDate as date)<=case when @ToDate is null then cast(PaidDate as date) else cast(@ToDate as date) end 

	DECLARE @RewardPaidAmt FLOAT = 0
	SET @RewardPaidAmt = (
		SELECT	SUM(AmtUsedForRewards)
		FROM	RewardClaimedMst 
		Where	IsActive = 1 AND IsApproved = 1
				AND cast(ApprovalDate as date) >= (case when @FromDate is null then cast(ApprovalDate as date) else cast(@FromDate as date) end)
				AND cast(ApprovalDate as date) <= (case when @ToDate is null then cast(ApprovalDate as date) else cast(@ToDate as date) end)
	)

	SET @Profite=@GrossProfite-(@TotalTDS+@TotalAdminCharge+@TotalPayout+@TotalExpense+@TotalBonanzaRewardsPay+@TotalTopRetailerPay + ISNULL(@RewardPaidAmt, 0))

	--SELECT @PendingPayout=ISNULL(SUM(NetPayable),0)
	--FROM	dbo.FN_ADMIN_PENDING_PAYOUT_DETAILS(0)
	SET @PendingPayout=ISNULL(@PendingPayout,0)

	SET @NetProfite=@Profite-@PendingPayout

	insert into @ProfiteDtl
	Select	@TotalSales as TotalSales,@TotalCosting as TotalCosting,@GrossProfite as GrossProfite,
			@TotalTDS as TotalTDS,@TotalAdminCharge as TotalAdminCharge,@TotalPayout as TotalPayout,
			@TotalExpense as TotalExpense,@Profite as Profite,@PendingPayout as PendingPayout,
			@NetProfite as NetProfite,@TotalVolume as TotalVolume,
			@TotalDirectIncomePay as TotalDirectIncomePay,@TotalPointsIncomePay as TotalMatchingIncomePay,
			@TotalVoltIncomePay as TotalVoltIncomePay,@TotalLeaderIncomePay as TotalLeaderIncomePay,
			@TotalUplineSupportIncomePay as TotalUplineSupportIncomePay,
			@TotalBonanzaRewardsPay as TotalBonanzaRewardsPay,
			@TotalTopRetailerPay as TotalTopRetailerPay,@TotalSilverIncomePay as TotalSilverIncomePay
			
	RETURN
END