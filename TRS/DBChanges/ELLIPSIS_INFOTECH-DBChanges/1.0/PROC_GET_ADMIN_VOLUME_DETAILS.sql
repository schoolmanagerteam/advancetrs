/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_VOLUME_DETAILS]    Script Date: 19-03-2021 10:45:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PROC_GET_ADMIN_VOLUME_DETAILS]  
(   
@FromDate datetime,  
@ToDate datetime  
)  
AS  
BEGIN  
 --Declare @FromDate datetime='2019-09-01'  
 --Declare @ToDate datetime='2019-09-15'  
  
 Declare @TotalTDS float,@TotalAdminCharge float,  
   @Profite float,@PendingPayout float,@NetProfite float,@TotalVolume float,  
   @TotalDirectVolume float,@TotalRepurchaseVolume float,  
   @TotalDirectIncomePay float,@TotalPointsIncomePay float,@TotalVoltIncomePay float,  
   @TotalLeaderIncomePay float,@TotalUplineSupportIncomePay float,@TotalSilverIncomePay float  
  
 Select  @TotalVolume=ISNULL(SUM(ISNULL(DirectPurchaseVolume,0)),0)+ISNULL(SUM(ISNULL(RepurchaseMatchingVolume,0)),0),  
   @TotalDirectVolume=ISNULL(SUM(ISNULL(DirectPurchaseVolume,0)),0),  
   @TotalRepurchaseVolume=ISNULL(SUM(ISNULL(RepurchaseMatchingVolume,0)),0)  
 from UserPurchaseHdr(Nolock) as uph  
 INNER JOIN UserPurchaseDtl(Nolock) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId  
 Where cast(OrderDate as date)>=cast(@FromDate as date) and cast(OrderDate as date)<=cast(@ToDate as date)  
  
 SET @TotalVolume=ISNULL(@TotalVolume,0)  
  
  
 Select @TotalTDS=ISNULL(SUM(TDSAmount),0),@TotalAdminCharge=ISNULL(SUM(AdminCharge),0),  
  @TotalDirectIncomePay=ISNULL(SUM(DirectIncomePay),0),@TotalPointsIncomePay=ISNULL(SUM(PointsIncomePay),0),@TotalVoltIncomePay=ISNULL(SUM(VoltIncomePay),0),  
  @TotalLeaderIncomePay=ISNULL(SUM(LeaderIncomePay),0),@TotalUplineSupportIncomePay=ISNULL(SUM(UplineSupportIncomePay),0),  
  @TotalSilverIncomePay=ISNULL(SUM(SilverIncomePay),0)    
 from WalletDebitDtl(Nolock)  
 where cast(DebitDate as date)>=cast(@FromDate as date) and cast(DebitDate as date)<=cast(@ToDate as date)   
   
 Declare @ActualDirectVolume float,@ActualDirectVolumeInRs float,  
   @ActualMatchingVolume float,@ActualMatchingVolumeInRs float,  
   @ActualLeaderCreateVolume float,@ActualLeaderCreateVolumeInRs float,  
   @ActualUplineSupportVolume float,@ActualUplineSupportVolumeInRs float,  
   @ActualVoltVolume float,@ActualVoltVolumeInRs float,  
   @ActualExpenseVolume float,@ActualExpenseVolumeInRs float,  
   @ActualCompanyTurnOverVolume float,@ActualCompanyTurnOverVolumeInRs float,  
   @ActualTopRetailerVolume float,@ActualTopRetailerVolumeInRs float,  
   @ActualRewardsVolume float,@ActualRewardsVolumeInRs float,  
   @ActualBonanzaVolume float,@ActualBonanzaVolumeInRs float,  
   @ActualSilverVolume float,@ActualSilverVolumeInRs float  
     
 SET @ActualDirectVolume=cast((@TotalVolume*10/100) as numeric(18,0))  
 SET @ActualDirectVolumeInRs=@ActualDirectVolume*10  
   
 SET @ActualMatchingVolume=cast((@TotalVolume*32/100) as numeric(18,0))  
 SET @ActualMatchingVolumeInRs=@ActualMatchingVolume*10  
   
 SET @ActualLeaderCreateVolume=cast((@TotalVolume*8/100) as numeric(18,0))  
 SET @ActualLeaderCreateVolumeInRs=@ActualLeaderCreateVolume*10  
   
 SET @ActualUplineSupportVolume=cast((@TotalVolume*0.8/100) as numeric(18,0))  
 SET @ActualUplineSupportVolumeInRs=@ActualUplineSupportVolume*10  
   
 SET @ActualSilverVolume=cast((@TotalVolume*0.8/100) as numeric(18,0))  
 SET @ActualSilverVolumeInRs=@ActualSilverVolume*10  
   
 SET @ActualVoltVolume=cast((@TotalDirectVolume*2/100) as numeric(18,0))  
 SET @ActualVoltVolumeInRs=@ActualVoltVolume*10  
   
 SET @ActualCompanyTurnOverVolume=cast((@TotalVolume*2/100) as numeric(18,0))  
 SET @ActualCompanyTurnOverVolumeInRs=@ActualCompanyTurnOverVolume*10  
    
 SET @ActualTopRetailerVolume=cast((@TotalRepurchaseVolume*2/100) as numeric(18,0))  
 SET @ActualTopRetailerVolumeInRs=@ActualTopRetailerVolume*10  
    
 --SET @ActualRewardsVolume=cast((@TotalVolume*2/100) as numeric(18,0))  
 --SET @ActualRewardsVolumeInRs=@ActualRewardsVolume*10  
 
 SET @ActualBonanzaVolume=cast((@TotalVolume*2/100) as numeric(18,0))  
 SET @ActualBonanzaVolumeInRs=@ActualBonanzaVolume*10  
 
 SET @ActualExpenseVolume=cast((@TotalVolume*20/100) as numeric(18,0))  
 SET @ActualExpenseVolumeInRs=@ActualExpenseVolume*10  
   
 
 DECLARE @ClaimedRewardBV FLOAT = 0, @RewardsBudgetAmt FLOAT = 0, @RewardsPaidAmt FLOAT = 0
 SELECT	@ClaimedRewardBV = SUM(ClaimedRewardBV), @RewardsBudgetAmt = SUM(ApproveRewardBudget), @RewardsPaidAmt = SUM(AmtUsedForRewards)
 FROM	RewardClaimedMst 
 Where	IsActive = 1 AND IsApproved = 1
		AND cast(ApprovalDate as date) >= (case when @FromDate is null then cast(ApprovalDate as date) else cast(@FromDate as date) end)
		AND cast(ApprovalDate as date) <= (case when @ToDate is null then cast(ApprovalDate as date) else cast(@ToDate as date) end)
 
 SET @ActualRewardsVolume = cast((ISNULL(@ClaimedRewardBV, 0) * 4 / 100) as numeric(18,0))
 SET @ActualRewardsVolumeInRs = ISNULL(@RewardsBudgetAmt, 0)

 Select @TotalVolume as TotalBusinessVolume,  
   @TotalDirectIncomePay as TotalDirectIncomePay,@TotalPointsIncomePay as TotalMatchingIncomePay,  
   @TotalVoltIncomePay as TotalVoltIncomePay,@TotalLeaderIncomePay as TotalLeaderIncomePay,  
   @TotalUplineSupportIncomePay as TotalUplineSupportIncomePay,  
   @ActualDirectVolume as ActualDirectVolume,  
   @ActualDirectVolumeInRs as ActualDirectVolumeInRs,  
   @ActualMatchingVolume as ActualMatchingVolume,  
   @ActualMatchingVolumeInRs as ActualMatchingVolumeInRs,  
   @ActualLeaderCreateVolume as ActualLeaderCreateVolume,  
   @ActualLeaderCreateVolumeInRs as ActualLeaderCreateVolumeInRs,  
   @ActualUplineSupportVolume as ActualUplineSupportVolume,  
   @ActualUplineSupportVolumeInRs as ActualUplineSupportVolumeInRs,  
   @ActualVoltVolume as ActualVoltVolume,  
   @ActualVoltVolumeInRs as ActualVoltVolumeInRs,  
   @ActualExpenseVolume as ActualExpenseVolume,  
   @ActualExpenseVolumeInRs as ActualExpenseVolumeInRs,  
   @ActualCompanyTurnOverVolume as ActualCompanyTurnOverVolume,  
   @ActualCompanyTurnOverVolumeInRs as ActualCompanyTurnOverVolumeInRs,  
   @ActualTopRetailerVolume as ActualTopRetailerVolume,  
   @ActualTopRetailerVolumeInRs as ActualTopRetailerVolumeInRs,  
   @ActualBonanzaVolume as ActualBonanzaVolume,  
   @ActualBonanzaVolumeInRs as ActualBonanzaVolumeInRs,  
   @TotalDirectVolume as TotalDirectBV,  
   @TotalRepurchaseVolume as TotalRepurchaseBV,  
   @ActualSilverVolume as ActualSilverVolume,  
   @ActualSilverVolumeInRs as ActualSilverVolumeInRs,
   ISNULL(@ActualRewardsVolume, 0) as ActualRewardsVolume,  
   ISNULL(@ActualRewardsVolumeInRs, 0) as ActualRewardsVolumeInRs,  
   ISNULL(@RewardsPaidAmt, 0) AS RewardsPaidAmt
     
END

