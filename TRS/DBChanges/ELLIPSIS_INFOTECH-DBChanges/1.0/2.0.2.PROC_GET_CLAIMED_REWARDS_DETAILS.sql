IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'PROC_GET_CLAIMED_REWARDS_DETAILS')
BEGIN
	DROP PROCEDURE PROC_GET_CLAIMED_REWARDS_DETAILS
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Harshad Chauhan>
-- Create date: <15 Mar 2021>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PROC_GET_CLAIMED_REWARDS_DETAILS
	-- Add the parameters for the stored procedure here
	@Year INT
	, @Month INT
	, @Type VARCHAR(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Declare @FirstDateOfYear DATETIME, @LastDateOfYear DATETIME, @FirstDateOfMonth DATETIME, @LastDateOfMonth DATETIME
	-- You can change @year to any year you desire
	--SELECT @year = 2019
	--SET @Month=6
	
	SELECT @FirstDateOfYear = DATEADD(yyyy, @Year - 1900, 0)
	SELECT @LastDateOfYear = DATEADD(yyyy, @Year - 1900 + 1, 0)
	SET @FirstDateOfMonth=cast(cast(@year as nvarchar(max))+'-'+cast(@Month as nvarchar(max))+'-01' as date)
	SET @LastDateOfMonth=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FirstDateOfMonth)+1,0))

	SELECT	ROW_NUMBER() OVER(ORDER BY rcmst.RewardClaimedMstId ASC) SrNo 
			, CONVERT(varchar, rcmst.ClaimedDate, 103) AS ClaimedDate, CONVERT(varchar, rcmst.ApprovalDate, 103) AS ResponseDate
			, UserMst.MobileNo AS ContactDetails, UserMst.EmailId, rcmst.ClaimedRewardBV
			, rcmst.ApproveRewardBudget, rcmst.AmtUsedForRewards, rcmst.ReferenceNo, rcmst.ClaimApprovalNote
	FROM	RewardClaimedMst AS rcmst
			INNER JOIN UserMst ON UserMst.UserId = rcmst.UserId

	WHERE	rcmst.IsActive = 1 
			AND ((@Type = 'all' AND IsApproved IS NOT NULL)
				--OR (@Type = 'Pending' AND IsApproved IS NULL)
				OR (@Type = 'Approve' AND IsApproved = 1)
				OR (@Type = 'DisApprove' AND IsApproved = 0)
			)
			AND (CAST(rcmst.ClaimedDate AS DATE) BETWEEN @FirstDateOfMonth AND @LastDateOfMonth)
	ORDER BY rcmst.ClaimedDate ASC
END
GO
