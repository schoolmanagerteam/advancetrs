IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'PROC_GET_REWARDS_APPROVAL_DETAILS')
BEGIN
	DROP PROCEDURE PROC_GET_REWARDS_APPROVAL_DETAILS
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Harshad Chauhan>
-- Create date: <15 Mar 2021>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PROC_GET_REWARDS_APPROVAL_DETAILS
	-- Add the parameters for the stored procedure here
	@Year INT
	, @Month INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Declare @FirstDateOfYear DATETIME, @LastDateOfYear DATETIME, @FirstDateOfMonth DATETIME, @LastDateOfMonth DATETIME
	-- You can change @year to any year you desire
	--SELECT @year = 2019
	--SET @Month=6
	
	SELECT @FirstDateOfYear = DATEADD(yyyy, @Year - 1900, 0)
	SELECT @LastDateOfYear = DATEADD(yyyy, @Year - 1900 + 1, 0)
	SET @FirstDateOfMonth=cast(cast(@year as nvarchar(max))+'-'+cast(@Month as nvarchar(max))+'-01' as date)
	SET @LastDateOfMonth=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FirstDateOfMonth)+1,0))

	SELECT	rcmst.RewardClaimedMstId, ROW_NUMBER() OVER(ORDER BY rcmst.RewardClaimedMstId ASC) SrNo 
			, CONVERT(varchar, rcmst.ClaimedDate, 103) AS ClaimedDate, UserMst.RefferalCode IdNo, UserMst.MobileNo AS ContactDetails, UserMst.EmailId
			, rcmst.MatchingBV, rcmst.ClaimedRewardBV, (rcmst.MatchingBV - rcmst.ClaimedRewardBV) AS PendingRewardBV
	FROM	RewardClaimedMst AS rcmst
			INNER JOIN UserMst ON UserMst.UserId = rcmst.UserId

	WHERE	rcmst.IsActive = 1 AND IsApproved IS NULL
			AND CAST(rcmst.ClaimedDate AS DATE) BETWEEN @FirstDateOfMonth AND @LastDateOfMonth
	ORDER BY rcmst.ClaimedDate ASC
END
GO
