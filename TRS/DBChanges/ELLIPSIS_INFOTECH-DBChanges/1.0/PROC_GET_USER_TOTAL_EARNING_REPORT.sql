/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]    Script Date: 22-03-2021 14:35:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]  
(  
@UserId int  
)  
AS  
BEGIN  
 Declare @date date=getdate()  
 Declare @i int=6  
 Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20),Income float, MatchingInCome FLOAT)  
 --Select @date,MONTH(@date),YEAR(@date)  
 While @i>=1  
 BEGIN  
    
  Declare @FromDate datetime=DATEADD(mm, DATEDIFF(mm, 0, @date), 0)  
  Declare @ToDate datetime=DATEADD(ms, -3, DATEADD(mm, DATEDIFF(m, 0, @date) + 1, 0))  
    
  insert into @MonthYear  
  Select	@i as Id, MONTH(@date) as MONTH, YEAR(@date) as YEAR, LEFT(DATENAME(month, @date), 3) + '-' +  DATENAME(year,  @date) as MonthYear
			,Income, MatchingIncome
  from	dbo.[FN_GET_USER_TOTAL_EARNING_DATE_RANGE](@UserId,@FromDate,@ToDate)  
    
  set @date=DATEADD(M,-1,@date)  
  SET @i=@i-1  
 END  
   
 Select * from @MonthYear   
 order by [Year] desc,[Month] desc  
END  
  

