
/****** Object:  Table [dbo].[RewardClaimedMst]    Script Date: 13-03-2021 13:24:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RewardClaimedMst]') AND type in (N'U'))
	DROP TABLE [dbo].[RewardClaimedMst]


CREATE TABLE [dbo].[RewardClaimedMst](
	[RewardClaimedMstId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[MatchingBV] [float] NOT NULL,
	[ClaimedRewardBV] [float] NOT NULL,
	[ClaimedDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ApprovalDate] [datetime] NULL,
	[IsApproved] [bit] NULL,
	[ReferenceNo] [nvarchar](50) NULL,
	[PrimaryReward] [nvarchar](500) NULL,
	[SecondaryReward] [nvarchar](500) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ClaimApprovalNote] [nvarchar](1000) NULL,
	[ApproveRewardBudget] [float] NULL,
	[AmtUsedForRewards] [float] NULL,
 CONSTRAINT [PK_RewardClaimedMst] PRIMARY KEY CLUSTERED 
(
	[RewardClaimedMstId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[RewardClaimedMst]  WITH CHECK ADD  CONSTRAINT [FK_RewardClaimedMst_UserMst] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])

