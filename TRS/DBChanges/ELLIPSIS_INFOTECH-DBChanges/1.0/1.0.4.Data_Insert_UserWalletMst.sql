DECLARE @FromDate DATE = NULL, @ToDate DATE = NULL
DECLARE @UserId int, @LeftMatchingBV FLOAT = 0, @RightMatchingBV FLOAT = 0, @CommonMatchingBVRmn FLOAT = 0
-------------------------------------------------------------------------------------------------------------
DECLARE UserWalletCursor CURSOR FOR

	SELECT UserMst.UserId FROM UserMst WITH(NOLOCK) WHERE IsActive = 1 and ISNULL(IsBlocked, 0) = 0
	AND UserMst.UserId NOT IN (SELECT UserWalletMst.UserId FROM UserWalletMst(NOLOCK))

OPEN UserWalletCursor;
FETCH NEXT FROM UserWalletCursor INTO @UserId

WHILE (@@FETCH_STATUS = 0)
BEGIN

----------------------------------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT 1 FROM UserWalletMst WITH(NOLOCK) WHERE UserId = @UserId)
	BEGIN

		SET @LeftMatchingBV = ISNULL((
			SELECT SUM(LeftBV) FROM (
				Select * from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,@FromDate,@ToDate)
			) AS TotalLeftBV
		), 0)

		SET @RightMatchingBV = ISNULL((
			SELECT SUM(RightBV) FROM (
				Select * from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,@FromDate,@ToDate)
			) AS TotalLeftBV
		), 0)

		SET @CommonMatchingBVRmn = (CASE WHEN @LeftMatchingBV >= @RightMatchingBV THEN @RightMatchingBV ELSE @LeftMatchingBV END)

		INSERT INTO UserWalletMst (UserId, MatchingBV, TotalMatchingBV)
		VALUES(@UserId, CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)), CAST(@CommonMatchingBVRmn AS DECIMAL(18, 2)))
	END
	-------------------------------------------------------------------------------------------------------------

	FETCH NEXT FROM UserWalletCursor INTO @UserId
END

CLOSE UserWalletCursor
DEALLOCATE UserWalletCursor
-------------------------------------------------------------------------------------------------------------