/****** Object:  StoredProcedure [dbo].[PROC_GET_SALES_ORDER_DETAILS]    Script Date: 22-03-2021 11:28:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[PROC_GET_SALES_ORDER_DETAILS]    
(    
	@UserPurchaseHdrId int=0    
  , @FromDate DATE = NULL
  , @ToDate DATE = NULL
)    
AS    
BEGIN    
 --Declare @UserPurchaseHdrId int=0    

	DECLARE @AdminStateName NVARCHAR(500) = ''
	SET @AdminStateName = (
		select	TOP 1 (CASE WHEN ISNULL(sm.StateName, '') = '' THEN pc.[State] ElSE sm.StateName END) AS StateName
		from	dbo.UserMst 
				LEFT JOIN CityMst ON CityMst.CityId = dbo.UserMst.CityId
				LEFT JOIN StateMst as sm on sm.StateId = CityMst.StateId
				LEFT JOIN Pincodes(Nolock) as pc on pc.Pincode = dbo.UserMst.PinCode
		where	(username = 'sadmin' and RefferalUserId = 0)
	)
    
	Select	um.FullName, um.[Address] as [Address], um.GSTNo, EmailId, MobileNo, uph.OrderNo
			, um.PinCode
			, ISNULL(cm.CityName, '') AS CityName
			, ISNULL(sm.StateName, '') AS StateName
			, uph.OrderDate,
			pm.ProductName, pcm.CategoryName, upd.Qty as Qty,
			upd.ProductPrice, upd.Amount, uph.TotalOrderAmount,
			cast((case when uph.UserPurchaseHdrId = oh.UserPurchaseHdrId then 0 else 1 end) as bit) as IsRepurchase,
			um.RefferalCode, ISNULL(IsAyurvedic,0) AS IsAyurvedic,
			(case when uph.IsRepurchaseOrder=0 then upd.DirectPurchaseVolume else upd.RepurchaseMatchingVolume end) as BusinessVolume,
			uph.Notes, @AdminStateName AS AdminStateName
			
	from	UserPurchaseHdr(Nolock) as uph     
			INNER JOIN UserPurchaseDtl(Nolock) upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId     
			INNER JOIN ProductMst(Nolock) as pm on upd.ProductId=pm.ProductId     
			INNER JOIN ProductCategoryMst(Nolock) as pcm on pm.ProductCategoryId=pcm.ProductCategoryId    
			INNER JOIN UserMst(Nolock) as um on uph.UserId=um.UserId    
			LEFT JOIN UserAddressDtl(NoLock) AS uad ON uad.UserAddressDtlId = uph.UserAddressDtlId
			LEFT JOIN CityMst(Nolock) as cm on cm.CityId = uad.CityId
			LEFT JOIN StateMst(Nolock) as sm on sm.StateId = cm.StateId
			--LEFT JOIN (
			--	SELECT	DISTINCT CAST(Tmppc.Pincode AS NVARCHAR(100)) AS Pincode, Tmppc.City, Tmppc.[State] 
			--	FROM	Pincodes(Nolock) AS Tmppc
			--	WHERE	CAST(Tmppc.Pincode AS NVARCHAR(100)) IN (
			--				SELECT Tmpum.Pincode FROM UserMst(Nolock) as Tmpum 
			--				INNER JOIN UserPurchaseHdr(Nolock) as Tmpuph ON Tmpuph.UserId = Tmpum.UserId
			--				WHERE ISNULL(Tmpum.PinCode, '') != ''
			--	) 
			--) as pc on pc.Pincode = um.PinCode
			LEFT JOIN dbo.[fn_GetUserFirstOrderId](default) as oh on uph.UserPurchaseHdrId=oh.UserPurchaseHdrId    

	Where	(uph.UserPurchaseHdrId=case when @UserPurchaseHdrId=0 then uph.UserPurchaseHdrId else @UserPurchaseHdrId end)
			AND (@FromDate IS NULL OR cast(uph.OrderDate as date) >= CAST(@FromDate AS DATE))
			AND (@ToDate IS NULL OR cast(uph.OrderDate as date) <= CAST(@ToDate AS DATE))

END  