IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'PROC_GET_USER_ADDRESS_LST')
BEGIN
	DROP PROCEDURE PROC_GET_USER_ADDRESS_LST
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Harshad Chauhan>
-- Create date: <22 Mar 2021>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PROC_GET_USER_ADDRESS_LST
	-- Add the parameters for the stored procedure here
	@UserId INT
  , @IsMasterAddress BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select	DISTINCT 
			ISNULL((	uad.Address
				+ ', ' + (CASE WHEN ISNULL(cm.CityName, '') = '' THEN pc.[City] ElSE cm.CityName END)
				+ ', ' + (CASE WHEN ISNULL(sm.StateName, '') = '' THEN pc.[State] ElSE sm.StateName END)
				+ ' ' + uad.PinCode
			), '') AS UserAddressLst
			, uad.UserAddressDtlId
			, ISNULL(IsMasterAddress, 0) AS IsMasterAddress
			, ISNULL(IsDefaultAddress, 0) AS IsDefaultAddress
	from	UserAddressDtl AS uad
			LEFT JOIN CityMst AS cm ON cm.CityId = uad.CityId
			LEFT JOIN StateMst as sm on sm.StateId = cm.StateId
			LEFT JOIN Pincodes(Nolock) as pc on pc.Pincode = uad.PinCode
	where	uad.UserId = @UserId
			AND (@IsMasterAddress IS NULL OR uad.IsMasterAddress = @IsMasterAddress)
			AND ISNULL(sm.StateId, 0) != 0
END
GO