/****** Object:  StoredProcedure [dbo].[PROC_GET_TOP_PURCHASE_PRODUCTS]    Script Date: 24-03-2021 13:30:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[PROC_GET_TOP_PURCHASE_PRODUCTS]  
(  
@UserId int  
, @FromDate DATE = NULL
, @ToDate DATE = NULL
)  
AS  
BEGIN  
 SET NOCOUNT ON;  

	Select UserId, sum(TotalAmount) As TotalAmount from (  
		Select	(CASE WHEN uph.UserId = @UserId THEN @UserId ELSE -1 END) AS UserId
				, (SUM(ISNULL(upd.DirectMatchingVolume, 0)) + SUM(ISNULL(upd.RepurchaseMatchingVolume, 0))) as TotalAmount	--, cast(SUM(ISNULL(Amount, 0)) as bigint) as TotalAmount
				--, pm.ProductId, pm.ProductName
		from	UserPurchaseHdr(Nolock) as uph
				INNER JOIN UserPurchaseDtl(Nolock) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId  
				--INNER JOIN ProductMst(Nolock) as pm on upd.productId=pm.ProductId  
		where	(UserId=@UserId 
					OR UserId IN (SELECT UserMst.UserId  FROM UserMst WHERE UserMst.RefferalUserId = @UserId)
				)
				AND (@FromDate IS NULL OR CAST(uph.OrderDate AS DATE) >= CAST(@FromDate AS DATE))
				AND (@ToDate IS NULL OR CAST(uph.OrderDate AS DATE) <= CAST(@ToDate AS DATE))
		group by  uph.UserId--, pm.ProductId,pm.ProductName  
	) as purchase  
	GROUP BY UserId
	--order by TotalAmount desc  
  
END

