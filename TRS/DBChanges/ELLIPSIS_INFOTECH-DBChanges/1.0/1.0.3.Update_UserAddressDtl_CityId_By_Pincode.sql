UPDATE UserAddressDtl
SET CityId = Final.CityMst_CityId
FROM (

	SELECT *, ROW_NUMBER() OVER(PARTITION BY UserId, PinCode ORDER BY PinCode) AS RowNum
	FROM (
		select	DISTINCT 
				UserAddressDtl.* 
				, Pincodes.City
				, CityMst.CityId AS CityMst_CityId
		from	UserAddressDtl
		INNER JOIN (
			SELECT * FROM (
				SELECT ROW_NUMBER() OVER(PARTITION BY PinCode ORDER BY PinCode) AS RowNum, * FROM (
					SELECT DISTINCT Pincode, City FROM Pincodes WHERE ISNULL(City, '') <> '' --ORDER BY Pincode
				) AS Final
			) AS Final2
			WHERE RowNum = 1
		) AS Pincodes ON CAST(Pincodes.Pincode AS NVARCHAR) = UserAddressDtl.PinCode
		INNER JOIN CityMst ON CityMst.CityName = Pincodes.City
		where	ISNULL(UserAddressDtl.CityId, 0) = 0 AND UserAddressDtl.IsMasterAddress = 1 AND ISNULL(UserAddressDtl.PinCode, '') != ''
	) AS Final

) AS Final
WHERE UserAddressDtl.UserAddressDtlId = Final.UserAddressDtlId