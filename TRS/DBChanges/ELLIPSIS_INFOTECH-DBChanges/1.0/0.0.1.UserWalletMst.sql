
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_UserWalletMst]') AND type = 'K')
	ALTER TABLE [dbo].[UserWalletMst] DROP CONSTRAINT [PK_UserWalletMst]

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_UserWalletMst_CommonMatchingBVRmn]') AND type = 'D')
	ALTER TABLE [dbo].[UserWalletMst] DROP CONSTRAINT [DF_UserWalletMst_CommonMatchingBVRmn]

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_UserWalletMst_TotalMatchingBV]') AND type = 'D')
	ALTER TABLE [dbo].[UserWalletMst] DROP CONSTRAINT [DF_UserWalletMst_TotalMatchingBV]

/****** Object:  Table [dbo].[UserWalletMst]    Script Date: 12-03-2021 12:36:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserWalletMst]') AND type in (N'U'))
	DROP TABLE [dbo].[UserWalletMst]


CREATE TABLE [dbo].[UserWalletMst](
	[UserWalletMstId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[MatchingBV] [float] NOT NULL,
	[TotalMatchingBV] [float] NOT NULL,
 CONSTRAINT [PK_UserWalletMst] PRIMARY KEY CLUSTERED 
(
	[UserWalletMstId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[UserWalletMst] ADD  CONSTRAINT [DF_UserWalletMst_TotalMatchingBV]  DEFAULT ((0)) FOR [TotalMatchingBV]

ALTER TABLE [dbo].[UserWalletMst] ADD  CONSTRAINT [DF_UserWalletMst_CommonMatchingBVRmn]  DEFAULT ((0)) FOR [MatchingBV]