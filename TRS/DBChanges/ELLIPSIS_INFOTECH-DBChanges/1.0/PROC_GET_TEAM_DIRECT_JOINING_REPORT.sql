/****** Object:  StoredProcedure [dbo].[PROC_GET_TEAM_DIRECT_JOINING_REPORT]    Script Date: 17-03-2021 17:43:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PROC_GET_TEAM_DIRECT_JOINING_REPORT]  
(  
	@UserId int  
)  
AS  
BEGIN  
 --Declare @date date=getdate()  
 --Declare @i int=6  
 --Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20))  
 ----Select @date,MONTH(@date),YEAR(@date)  
 --While @i>=1  
 --BEGIN  
 -- insert into @MonthYear  
 -- Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + '-' +  DATENAME(year,  @date)  
 -- set @date=DATEADD(M,-1,@date)  
 -- SET @i=@i-1  
 --END  
  
 --Select Id,mm.Month as [Month],mm.Year as [Year],mm.MonthYear,Count(UserId) as RegistrationCnt from @MonthYear as mm  
 --LEFT JOIN (Select * from UserMst where RefferalUserId=@UserId) as um on mm.Month=RegMonth and mm.Year=RegYear  
 --group by mm.Month,mm.Year,mm.MonthYear,Id  
   
 Declare @date date=(Select RegistrationDate from UserMst where UserId=@UserId)  
 Declare @ToDate date=getdate()  
 Declare @i int=1  
 Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20))  
 --Select @date,MONTH(@date),YEAR(@date)  
 --While @ToDate>=@date  
 IF @ToDate>DATEADD(M,6,@date)  
 BEGIN  
  SET @i=6  
  While @i>0  
  BEGIN  
   insert into @MonthYear  
   Select @i,MONTH(@ToDate),YEAR(@ToDate),LEFT(DATENAME(month, @ToDate), 3) + '-' +  DATENAME(year,  @ToDate)  
   --set @date=DATEADD(M,-1,@date)  
   SET @i=@i-1  
   SET @ToDate=DATEADD(M,-1,@ToDate)  
  END  
 END  
 ELSE  
 BEGIN  
  While @i<=6  
  BEGIN  
   insert into @MonthYear  
   Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + '-' +  DATENAME(year,  @date)  
   --set @date=DATEADD(M,-1,@date)  
   SET @i=@i+1  
   SET @date=DATEADD(M,1,@date)  
  END  
 END  
   
	SELECT	Id, mm.Month AS [Month], mm.Year AS [Year], mm.MonthYear,
			ISNULL(SUM(CASE WHEN ISNULL(IsRegistrationActivated, 0) = 1 THEN 1 END), 0) AS ActiveRegistrationCnt,
			ISNULL(SUM(CASE WHEN ISNULL(IsRegistrationActivated, 0) = 0 THEN 1 END), 0) AS InActiveRegistrationCnt
	FROM	@MonthYear AS mm
			LEFT JOIN (
				SELECT * FROM UserMst WHERE RefferalUserId = @UserId
				UNION
				select * from UserMst where RefferalUserId IN (
					Select RefferelTbl.UserId from UserMst AS RefferelTbl where RefferelTbl.RefferalUserId = @UserId
				)

			) AS um ON mm.Month = RegMonth 
					AND mm.Year = RegYear
	GROUP BY mm.Month, mm.Year, mm.MonthYear, Id
END

