
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_UserAddressDtl_IsDefaultAddress]') AND type = 'D')
	ALTER TABLE [dbo].[UserAddressDtl] DROP CONSTRAINT [DF_UserAddressDtl_IsDefaultAddress]

/****** Object:  Table [dbo].[UserAddressDtl]    Script Date: 22-03-2021 17:20:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAddressDtl]') AND type in (N'U'))
	DROP TABLE [dbo].[UserAddressDtl]

CREATE TABLE [dbo].[UserAddressDtl](
	[UserAddressDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Address] [nvarchar](1000) NULL,
	[CityId] [numeric](18, 0) NULL,
	[PinCode] [nvarchar](20) NULL,
	[IsMasterAddress] BIT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[IsActive] [bit] NULL,
	[IsDefaultAddress] [bit] NOT NULL,
 CONSTRAINT [PK_UserAddressDtl] PRIMARY KEY CLUSTERED 
(
	[UserAddressDtlId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[UserAddressDtl] ADD  CONSTRAINT [DF_UserAddressDtl_IsDefaultAddress]  DEFAULT ((0)) FOR [IsDefaultAddress]