IF NOT EXISTS (SELECT	1
			   FROM		INFORMATION_SCHEMA.COLUMNS
			   WHERE	TABLE_NAME = 'UserPurchaseHdr'
						AND COLUMN_NAME = 'UserAddressDtlId')
BEGIN
	BEGIN TRANSACTION
			ALTER TABLE UserPurchaseHdr
			ADD UserAddressDtlId BIGINT DEFAULT(0)
	COMMIT TRANSACTION

	BEGIN TRANSACTION
		EXEC ('
		UPDATE UserPurchaseHdr
		SET UserPurchaseHdr.UserAddressDtlId = Final.UserAddressDtlId
		FROM (
				SELECT	uph.UserPurchaseHdrId, uad.UserAddressDtlId
				from	UserPurchaseHdr(Nolock) as uph
						INNER JOIN UserAddressDtl(Nolock) AS uad ON uad.UserId = uph.UserId AND uad.IsMasterAddress = 1
		) AS Final

		WHERE UserPurchaseHdr.UserPurchaseHdrId = Final.UserPurchaseHdrId
		')
	COMMIT TRANSACTION
END