IF EXISTS(select TOP 1 UserId from dbo.UserMst where (username = 'sadmin' and RefferalUserId = 0) AND ISNULL(CityId, 0) = 0)
BEGIN
	DECLARE @AdminUserId INT = 0
	SET @AdminUserId = ( select TOP 1 UserId from dbo.UserMst where (username = 'sadmin' and RefferalUserId = 0))

	DECLARE @StateId INT = 0;
	SET @StateId = (select TOP 1 StateId from StateMst where StateName like '%gujarat%' and IsActive = 1)

	DECLARE @CityId INT = 0;
	SET @CityId = (select TOP 1 CityId from CityMst where StateId = @StateId and IsActive = 1 AND CityName like '%AHMEDABAD%')

	--select @StateId, @CityId
	UPDATE UserMst set CityId = @CityId where UserId = @AdminUserId
END