﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TRSImplementation;

namespace TRS.Models
{
    public class PaymentDtlEnt : ResponseMsg
    {
        public string Hash { get; set; }
        public string MerchantKey { get; set; }
        public string TxnId { get; set; }
        public string BOLT_KIT_ASP { get; set; }
        public string MerchantSalt { get; set; }
        public string FirstName { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string SuccessURL { get; set; }
        public string FailureURL { get; set; }
        public string ProductInfo { get; set; }
        public string OrderAmount { get; set; }
        public string RegistrationPoints { get; set; }
        public bool IsRegistrationActivated { get; set; }
        public string WalletDebitDtlId { get; set; }
        public double codamt { get; set; }
        public bool RedirectFrmAdmin { get; set; }
        public int OrderUserId { get; set; }
    }

    public class TempRegistration : TempUserMst
    {
        public TempRegistration()
        {
        }

        public string RefferalCode { get; set; }
        public string CityName { get; set; }
        public string Product { get; set; }
        public string Position { get; set; }
        public bool IsActivate { get; set; }
        public bool IsFreeAccount { get; set; }
    }

    public class UserProfileEnt : UserMst
    {
        public string CityName { get; set; }
        public string DateOfBirth { get; set; }
        public string StateId { get; set; }
    }

    //public class FranchiseeUserProfileEnt : FranchiseeMst
    //{
    //    public string CityName { get; set; }
    //}
    public class SelectedProduct
    {
        public SelectedProduct()
        {
        }

        public int ProductId { get; set; }
        public int Qty { get; set; }
        public float Price { get; set; }
        public int ProductVariantId { get; set; }
    }

    //public class clsLogin
    //{
    //    public clsLogin()
    //    {
    //    }
    //    public int UserId { get; set; }
    //    public string FullName { get; set; }
    //    public string FirstName { get; set; }
    //    public string LastName { get; set; }
    //    public string RefferalCode { get; set; }
    //    public string EmailId { get; set; }
    //    public string MobileNo { get; set; }
    //    public string UserName { get; set; }
    //}

    public class PieChartEnt
    {
        public string value { get; set; }
        public string color { get; set; }
        public string highlight { get; set; }
        public string label { get; set; }
    }

    public class ColorHighlightColorEnt
    {
        public string color { get; set; }
        public string highlight { get; set; }
    }

    public class DashboardEnt
    {
        public DashboardEnt()
        {
            listPieChartEnt = new List<PieChartEnt>();
        }

        public List<PieChartEnt> listPieChartEnt { get; set; }
    }

    public class MemberReportEnt
    {
        public MemberReportEnt()
        {
            //listTotalEarningByTeam = new List<PROC_GET_TOTAL_EARNING_BY_INDIVIDUAL_TEAM_MEMBER_Result>();
            //listProductPurchaseByTeam = new List<PROC_GET_PRODUCT_PURCHASE_BY_INDIVIDUAL_TEAM_MEMBER_Result>();
        }

        //public List<PROC_GET_TOTAL_EARNING_BY_INDIVIDUAL_TEAM_MEMBER_Result> listTotalEarningByTeam { get; set; }
        //public List<PROC_GET_PRODUCT_PURCHASE_BY_INDIVIDUAL_TEAM_MEMBER_Result> listProductPurchaseByTeam { get; set; }
    }

    /// <summary>
    /// Class that encapsulates most common parameters sent by DataTables plugin
    /// </summary>
    public class JQueryDataTableParamModel
    {
        /// <summary>
        /// Request sequence number sent by DataTable, same value must be returned in response
        /// </summary>
        public object[] aoData { get; set; }

        /// <summary>
        /// Request sequence number sent by DataTable, same value must be returned in response
        /// </summary>
        public string sEcho { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>
        private string _sSearch { get; set; }

        public string sSearch { get { return _sSearch; } set { _sSearch = value == null ? null : value.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]"); } }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int iDisplayLength { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int iSortingCols { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumns { get; set; }
    }

    public partial class CartProductMst : ProductMst
    {
        public int Qty { get; set; }
    }

    public partial class CartProducts
    {
        public int ProductId { get; set; }
        public int Qty { get; set; }
        public int ProductVariantId { get; set; }
    }

    public class AdminDashboardEnt
    {
        public List<PROC_GET_USER_ORDER_HDR_Result> listTodayOrders { get; set; }
        public List<UserMst> listUserMst { get; set; }
        // public List<PROC_GET_ALL_CASHBACK_APPLICABLE_USERS_Result> listCashbackApplicableUser { get; set; }
        //public List<PROC_GET_USER_POINT_PURCHASE_ORDER_HDR_Result> listPointPurchaseOrders { get; set; }
    }

    public class UserWalletEnt
    {
        //public List<PROC_GET_USER_WALLET_DETAILS_Result> listCashWallet { get; set; }
        //public List<PROC_GET_USER_POINTS_DETAILS_Result> listPointsWallet { get; set; }
    }

    public class SelectedProductsListEnt
    {
        public string ProductName { get; set; }
        public string Qty { get; set; }
        public string ProductPrice { get; set; }
        public string Amount { get; set; }
        public string ProductVariantName { get; set; }
        public string ImagePath { get; set; }
        public Int32 ProductId { get; set; }
    }

    public class ProductVariantListEnt
    {
        public string VariantName { get; set; }
        public string ProductId { get; set; }
        public string ProductVariantId { get; set; }
    }

    public class BusinessCategoryListEnt
    {
        public string CategoryName { get; set; }
        public string BusinessToolsCategoryId { get; set; }
    }

    public class IncomeUserEnt
    {
        public int SrNo { get; set; }
        public string RefferalCode { get; set; }
        public string FullName { get; set; }
        public decimal IncomeAmt { get; set; }
        public double? BV { get; set; }
    }

    public class MatchingIncomeDtl
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string WeekName { get; set; }
        public string MonthYear { get; set; }
        public int WeekNo { get; set; }
        public decimal RightIncome { get; set; }
        public decimal LeftIncome { get; set; }
        public decimal MatchingIncome { get; set; }
        public decimal CarryForwardIncome { get; set; }
        public double LeftBV { get; set; }
        public double RightBV { get; set; }
        public double MatchingBV { get; set; }
        public double CarryForwardBV { get; set; }
        public string CarryForwardBVSide { get; set; }
    }

    public class MonthNames
    {
        public string MonthName { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string MonthYear { get; set; }
    }

    public class WeekNames
    {
        public string WeekName { get; set; }
        public int WeekNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class UserListEnt
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PanNo { get; set; }
        public string BankName { get; set; }
        public string BankAccNo { get; set; }
        public string BankIFSC { get; set; }
        public string BankUserName { get; set; }
        public bool IsRegistrationActivated { get; set; }
        public string RefferalCode { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RegistrationDate { get; set; }
        public string MobileNo { get; set; }
        public bool IsBlocked { get; set; }
        public int UserId { get; set; }
        public string RefferalByCode { get; set; }
        public string ActivateDate { get; set; }
        public double? RightBV { get; set; }
        public double? LeftBV { get; set; }
        public bool IsKYCVerified { get; set; }
        public string PANImage { get; set; }
        public bool IsPANVerified { get; set; }
        public string BankDocument { get; set; }
        public bool IsBankDocumentVerified { get; set; }
        public string KYCImage { get; set; }
    }

    public class AdminWalletDtlEnt : PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result
    {
        public string PayoutDate { get; set; }
    }

    public class ExpenseDtlEnt : PROC_GET_ALL_EXPENSE_DETAILS_Result
    {
        public string sExpenseDate { get; set; }
    }

    public class ProfitReport
    {
        public PROC_GET_ADMIN_PROFITE_DETAILS_Result Profit { get; set; }
        public PROC_GET_ADMIN_VOLUME_DETAILS_Result Volume { get; set; }
    }

    public class UserMatchingBVDtl
    {
        public int UserId { get; set; }
        public double MatchingBV { get; set; }
    }

    public class MemberListEnt : PROC_GET_ALL_MEMBER_LIST_Result
    {
        public double? RightBV { get; set; }
        public double? LeftBV { get; set; }
    }

    public class PROC_GET_UPLINE_MONTHLY_GOLD_USERS_INCOME_Result
    {
        public int UserId { get; set; }
        public decimal MatchingIncome { get; set; }
        public decimal LeftIncome { get; set; }
        public decimal RightIncome { get; set; }
        public string MMYY { get; set; }
        public string RefferalCode { get; set; }
        public string FullName { get; set; }
        public bool IsGoldMember { get; set; }
    }

    public class PROC_GET_LCBPOOL_MONTHLY_INCOME_ResultMdl
    {
        public long LCBPoolIncomeId { get; set; }
        public int UserId { get; set; }
        public string LCBPoolUser { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string MMYY { get; set; }
        public decimal MatchingBV { get; set; }
        public decimal LCBBonus { get; set; }
        public string Level_1 { get; set; }
        public int? Level_1_UserId { get; set; }
        public decimal? Level_1_LCBIncome { get; set; }
        public decimal? Level_1_MatchingBV { get; set; }
        public string Level_2 { get; set; }
        public int? Level_2_UserId { get; set; }
        public decimal? Level_2_LCBIncome { get; set; }
        public decimal? Level_2_MatchingBV { get; set; }
        public string Level_3 { get; set; }
        public int? Level_3_UserId { get; set; }
        public decimal? Level_3_LCBIncome { get; set; }
        public decimal? Level_3_MatchingBV { get; set; }
        public string Level_4 { get; set; }
        public int? Level_4_UserId { get; set; }
        public decimal? Level_4_LCBIncome { get; set; }
        public decimal? Level_4_MatchingBV { get; set; }
        public string Level_5 { get; set; }
        public int? Level_5_UserId { get; set; }
        public decimal? Level_5_LCBIncome { get; set; }
        public decimal? Level_5_MatchingBV { get; set; }
        public string Level_6 { get; set; }
        public int? Level_6_UserId { get; set; }
        public decimal? Level_6_LCBIncome { get; set; }
        public decimal? Level_6_MatchingBV { get; set; }
        public string Level_7 { get; set; }
        public int? Level_7_UserId { get; set; }
        public decimal? Level_7_LCBIncome { get; set; }
        public decimal? Level_7_MatchingBV { get; set; }
    }
    public class DashboardSalesExpenseProfiteMdl
    {
        public decimal TotalSales { get; set; }
        public int ActiveMemberCnt { get; set; }
        public int InActiveMemberCnt { get; set; }
        public decimal ThisMonthDirectSales { get; set; }
        public decimal ThisMonthRepurchaseSales { get; set; }
        public decimal ThisMonthSales { get; set; }
        public decimal ThisWeekSales { get; set; }
        public decimal ThisWeekDirectSales { get; set; }
        public decimal ThisWeekRepurchaseSales { get; set; }
        public decimal ThisWeekExpense { get; set; }
        public decimal ThisMonthExpense { get; set; }
        public decimal ThisMonthProfite { get; set; }
        public decimal ThisWeekProfite { get; set; }
        public decimal TotalProfit { get; set; }
        public int ThisWeekBV { get; set; }
        public int ThisMonthBV { get; set; }
        public int ThisWeekDirectBV { get; set; }
        public int ThisWeekRepurchaseBV { get; set; }
    }
}