﻿function FillAutoComplete(element, sourceList, hdElement) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.Value,
                    value: item.Id,
                    id: item.Id
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 2,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
            }
        }
    })
    .focus(function () {
        $(this).autocomplete("search");
    });
}

function copyReferralCodeForShare(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(WebsiteURL + "/Authentication/Authenticate/Signup?rc=" + $(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    Materialize.toast('Copied!', 2000)
}