﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TRSImplementation;
using log4net;

namespace TRS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();

            clsBase objclsBase = new clsBase();
            var listConfig = objclsBase.db.ConfigMsts.ToList();
            var Version = listConfig.Where(i => i.Key.ToUpper() == ("Version").ToUpper()).FirstOrDefault().Value;
            var BOLT_URL = listConfig.Where(i => i.Key.ToString().ToUpper() == ("BOLTURL").ToUpper()).FirstOrDefault().Value;
            Version = GetVersion(Version);
            objclsBase.db.ConfigMsts.Where(i => i.Key.ToUpper() == ("Version").ToUpper()).FirstOrDefault().Value = Version;
            objclsBase.db.SaveChanges();
            var WebsiteURL = listConfig.Where(i => i.Key.ToUpper() == ("WebsiteURL").ToUpper()).FirstOrDefault().Value;


            Application[clsImplementationMessage.Version] = Version;
            Application[clsImplementationMessage.BOLT_URL] = BOLT_URL;
            Application[clsImplementationMessage.WebsiteURL] = WebsiteURL;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }

        public string GetVersion(string currentVersion)
        {
            List<string> listcurrentVersion = currentVersion.Split('.').ToList();
            if (listcurrentVersion[3] == "999")
            {
                listcurrentVersion[2] = (Convert.ToInt32(listcurrentVersion[2]) + 1).ToString();
                listcurrentVersion[3] = "000";
            }
            else
            {
                listcurrentVersion[3] = (Convert.ToInt32(listcurrentVersion[3]) + 1).ToString().PadLeft(3, '0');
            }

            if (listcurrentVersion[2] == "999")
            {
                listcurrentVersion[1] = (Convert.ToInt32(listcurrentVersion[1]) + 1).ToString();
                listcurrentVersion[2] = "0";
            }

            if (listcurrentVersion[1] == "999")
            {
                listcurrentVersion[0] = (Convert.ToInt32(listcurrentVersion[0]) + 1).ToString();
                listcurrentVersion[1] = "0";
            }

            return string.Join(".", listcurrentVersion);
        }
    }
}
