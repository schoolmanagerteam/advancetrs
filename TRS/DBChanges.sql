/****** Object:  Table [dbo].[SMSSendDtl]    Script Date: 05/19/2019 13:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SMSSendDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SMSSendDtl](
	[SMSSendId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SMSText] [nvarchar](500) NOT NULL,
	[MobileNo] [numeric](18, 0) NOT NULL,
	[SendDate] [smalldatetime] NOT NULL,
	[SendSuccess] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[SMSResponseId] [nvarchar](50) NULL,
	[SMSSendType] [nvarchar](100) NULL,
 CONSTRAINT [PK_SMSSendDtl_SMSSendId] PRIMARY KEY CLUSTERED 
(
	[SMSSendId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsBlocked'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE UserMst ADD IsBlocked bit

END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='TDSPercWithoutPAN')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'TDSPercWithoutPAN','20',1,1,GETDATE(),1,GETDATE()
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='TDSPercWithPAN')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'TDSPercWithPAN','5',1,1,GETDATE(),1,GETDATE()
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='AdminChargePerc')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'AdminChargePerc','5',1,1,GETDATE(),1,GETDATE()
END
GO

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DirectIncomePay'
      AND Object_ID = Object_ID(N'WalletDebitDtl'))
BEGIN
    -- Column Not Exists
    ALTER TABLE WalletDebitDtl ADD DirectIncomePay float

END
GO

IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'PointsIncomePay'
      AND Object_ID = Object_ID(N'WalletDebitDtl'))
BEGIN
    -- Column Not Exists
    ALTER TABLE WalletDebitDtl ADD PointsIncomePay float

END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'VoltIncomePay'
      AND Object_ID = Object_ID(N'WalletDebitDtl'))
BEGIN
    -- Column Not Exists
    ALTER TABLE WalletDebitDtl add VoltIncomePay float

END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DirectPurchasePerc'
      AND Object_ID = Object_ID(N'ProductMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE ProductMst add DirectPurchasePerc float

END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='MinRegistrationPoints')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'MinRegistrationPoints','500',1,1,GETDATE(),1,GETDATE()
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='DirectMatchingPercGym')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'DirectMatchingPercGym','1.6',1,1,GETDATE(),1,GETDATE()
END
GO

GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DirectMatchingPerc'
      AND Object_ID = Object_ID(N'ProductMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE ProductMst add DirectMatchingPerc float

END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'RepurchaseMatchingPerc'
      AND Object_ID = Object_ID(N'ProductMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE ProductMst add RepurchaseMatchingPerc float

END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DirectPurchaseAmt'
      AND Object_ID = Object_ID(N'ProductMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE ProductMst add DirectPurchaseAmt float

END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DirectMatchingAmt'
      AND Object_ID = Object_ID(N'ProductMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE ProductMst add DirectMatchingAmt float

END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'RepurchaseMatchingAmt'
      AND Object_ID = Object_ID(N'ProductMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE ProductMst add RepurchaseMatchingAmt float

END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'KYCImage'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
    ALTER TABLE UserMst add KYCImage nvarchar(max)
END
/****** Object:  Table [dbo].[OrderStatusMst]    Script Date: 07/17/2019 21:40:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrderStatusMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrderStatusMst](
	[OrderStatusId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_OrderStatusMst_OrderStatusId] PRIMARY KEY CLUSTERED 
(
	[OrderStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'NomineeName'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
    ALTER TABLE UserMst add NomineeName nvarchar(max)
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsGoldMember'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
    ALTER TABLE UserMst add IsGoldMember bit
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsSilverMember'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
    ALTER TABLE UserMst add IsSilverMember bit
END
GO
/****** Object:  Table [dbo].[ExpenseTypeMst]    Script Date: 22/07/2019 20:15:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpenseTypeMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExpenseTypeMst](
	[ExpenseTypeId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ExpenseTypeName] [nvarchar](250) NOT NULL,	
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ExpenseTypeMst_ExpenseTypeId] PRIMARY KEY CLUSTERED 
(
	[ExpenseTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ExpenseMst]    Script Date: 22/07/2019 20:15:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpenseMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExpenseMst](
	[ExpenseId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ExpenseTypeId] [numeric](18, 0) NOT NULL,	
	[ExpenseDate] datetime NOT NULL,
	[Amount] float NOT NULL,
	[Description] nvarchar(max),
	[PaidBy] int ,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ExpenseMst_ExpenseId] PRIMARY KEY CLUSTERED 
(
	ExpenseId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].ExpenseMst  WITH CHECK ADD FOREIGN KEY(ExpenseTypeId)
REFERENCES [dbo].[ExpenseTypeMst] (ExpenseTypeId)

END
GO
/****** Object:  Table [dbo].[LeaderAndUplinePointsCreditDtl]    Script Date: 07/27/2019 16:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LeaderAndUplinePointsCreditDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LeaderAndUplinePointsCreditDtl](
	[LeaderAndUplinePointsCreditDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FromCreditUserId] [int] NOT NULL,
	[CreditPerc] [float] NOT NULL,
	[CreditDate] [datetime] NOT NULL,
	[CreditPoints] [numeric](18, 2) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsLeaderPoint] bit NOT NULL,
 CONSTRAINT [PK_LeaderAndUplinePointsCreditDtl_LeaderAndUplinePointsCreditDtlId] PRIMARY KEY CLUSTERED 
(
	[LeaderAndUplinePointsCreditDtlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].[LeaderAndUplinePointsCreditDtl]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='RequiredMemberForGold')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'RequiredMemberForGold','150',1,1,GETDATE(),1,GETDATE()
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='RequiredMemberForSilver')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'RequiredMemberForSilver','50',1,1,GETDATE(),1,GETDATE()
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'GoldMemberDate'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
    ALTER TABLE UserMst add GoldMemberDate datetime
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SilverMemberDate'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
    ALTER TABLE UserMst add SilverMemberDate datetime
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='GoldMemberPerc')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'GoldMemberPerc','100',1,1,GETDATE(),1,GETDATE()
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='SilverMemberPerc')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'SilverMemberPerc','10',1,1,GETDATE(),1,GETDATE()
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'LeaderIncomePay'
      AND Object_ID = Object_ID(N'WalletDebitDtl'))
BEGIN
    -- Column Not Exists
    ALTER TABLE WalletDebitDtl ADD LeaderIncomePay float
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'UplineSupportIncomePay'
      AND Object_ID = Object_ID(N'WalletDebitDtl'))
BEGIN
    -- Column Not Exists
    ALTER TABLE WalletDebitDtl ADD UplineSupportIncomePay float
END
GO
/****** Object:  Table [dbo].[LeaderAndUplinePointsCreditDtl]    Script Date: 07/27/2019 16:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MatchingPointsCreditDtl]') AND type in (N'U'))
BEGIN
/****** Object:  Table [dbo].[MatchingPointsCreditDtl]    Script Date: 08/03/2019 19:31:40 ******/
CREATE TABLE [dbo].[MatchingPointsCreditDtl](
	[MatchingPointsCreditDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[CreditDate] [datetime] NULL,
	LeftPoints [numeric](18, 0) NULL,
	RightPoints [numeric](18, 0) NULL,
	MatchingPoints [numeric](18, 0) NULL,
	CarryForwardPoints [numeric](18, 0) NULL,
	CarryForwardSide nvarchar(max) NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_MatchingPointsCreditDtl_MatchingPointsCreditDtlId] PRIMARY KEY CLUSTERED 
(
	MatchingPointsCreditDtlId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].MatchingPointsCreditDtl  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])
END
GO
/****** Object:  Table [dbo].[ExpenseTypeMst]    Script Date: 08/08/2019 21:45:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpensePaidByMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExpensePaidByMst](
	[ExpensePaidById] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ExpensePaidBy] [nvarchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ExpensePaidByMst_ExpensePaidById] PRIMARY KEY CLUSTERED 
(
	ExpensePaidById ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS(select 1 from ExpensePaidByMst where ExpensePaidBy='Cash')
BEGIN
	insert into ExpensePaidByMst values('Cash',1,1,GETDATE(),1,GETDATE())
END
IF NOT EXISTS(select 1 from ExpensePaidByMst where ExpensePaidBy='Cheque')
BEGIN
	insert into ExpensePaidByMst values('Cheque',1,1,GETDATE(),1,GETDATE())
END
IF NOT EXISTS(select 1 from ExpensePaidByMst where ExpensePaidBy='Wallet')
BEGIN
	insert into ExpensePaidByMst values('Wallet',1,1,GETDATE(),1,GETDATE())
END
IF NOT EXISTS(select 1 from ExpensePaidByMst where ExpensePaidBy='UPI')
BEGIN
	insert into ExpensePaidByMst values('UPI',1,1,GETDATE(),1,GETDATE())
END
GO
/****** Object:  Table [dbo].[ExpensePaidToMst]    Script Date: 13/08/2019 21:45:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpensePaidToMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExpensePaidToMst](
	[ExpensePaidToId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ExpensePaidTo] [nvarchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ExpensePaidToMst_ExpensePaidToId] PRIMARY KEY CLUSTERED 
(
	ExpensePaidToId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ExpensePaidToId'
      AND Object_ID = Object_ID(N'ExpenseMst'))
BEGIN
    ALTER TABLE ExpenseMst add ExpensePaidToId int
END
GO
IF NOT EXISTS (Select 1 from OrderStatusMst Where Name='Order Confirm')
BEGIN
	insert into OrderStatusMst values('Order Confirm',1,1,GETDATE(),1,GETDATE())
END
GO
Update UserPurchaseHdr set OrderStatusId=5 where OrderStatusId is null
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsRepurchaseOrder'
      AND Object_ID = Object_ID(N'UserPurchaseHdr'))
BEGIN
    ALTER TABLE UserPurchaseHdr add IsRepurchaseOrder bit
END
GO
--Update uph set 
--	uph.IsRepurchaseOrder=case when toh.UserPurchaseHdrId is null then 1 else 0 end
--from UserPurchaseHdr as uph
--LEFT JOIN(
--Select * from (
--	Select Row_Number() over(partition by UserId order by UserPurchaseHdrId) as RowNo,UserPurchaseHdrId,UserId
--	from UserPurchaseHdr
--) as oh 
--Where oh.RowNo=1
--) as toh on uph.UserPurchaseHdrId=toh.UserPurchaseHdrId
--Where uph.IsRepurchaseOrder is null
--GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='AdminConfirmationRequired')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'AdminConfirmationRequired','true',1,1,GETDATE(),1,GETDATE()
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='MultiOrderSystem')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'MultiOrderSystem','false',1,1,GETDATE(),1,GETDATE()
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='AllowCODAsFirstOrder')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 'AllowCODAsFirstOrder','false',1,1,GETDATE(),1,GETDATE()
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'RegularPrice'
      AND Object_ID = Object_ID(N'UserPurchaseDtl'))
BEGIN
    ALTER TABLE UserPurchaseDtl add RegularPrice float
END
GO
/****** Object:  Table [dbo].[BusinessToolsCategoryMst]    Script Date: 09/22/2019 13:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BusinessToolsCategoryMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BusinessToolsCategoryMst](
	[BusinessToolsCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_BusinessToolsCategoryMst_BusinessToolsCategoryId] PRIMARY KEY CLUSTERED 
(
	[BusinessToolsCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[BusinessToolsMst]    Script Date: 09/22/2019 13:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BusinessToolsMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BusinessToolsMst](
	[BusinessToolsId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessToolsCategoryId] [int] NOT NULL,
	[BusinessToolsName] [nvarchar](max) NOT NULL,
	[VideoURL] [nvarchar](max) NOT NULL,
	[ImageName] [nvarchar](max) NOT NULL,
	[PdfName] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_BusinessToolsMst_BusinessToolsId] PRIMARY KEY CLUSTERED 
(
	BusinessToolsId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

ALTER TABLE BusinessToolsMst
ADD FOREIGN KEY (BusinessToolsCategoryId) REFERENCES BusinessToolsCategoryMst(BusinessToolsCategoryId);
GO
/****** Object:  Table [dbo].[NotificationMst]    Script Date: 09/22/2019 13:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NotificationMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NotificationMst](
	[NotificationId] [int] IDENTITY(1,1) NOT NULL,
	[NotificationTypeId] [int] NOT NULL,
	UserId int NOT NULL,
	[Notification] [nvarchar](max) NOT NULL,
	[URL] [nvarchar](max) NULL,	
	[IsRead] [bit] NULL,
	[ReadDate] Datetime NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_NotificationMst_NotificationId] PRIMARY KEY CLUSTERED 
(
	NotificationId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

ALTER TABLE NotificationMst
ADD FOREIGN KEY (UserId) REFERENCES UserMst(UserId);
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DeliveryAddress'
      AND Object_ID = Object_ID(N'UserPurchaseHdr'))
BEGIN
    ALTER TABLE UserPurchaseHdr Add DeliveryAddress nvarchar(max)
END
GO
ALTER TABLE BusinessToolsMst ALTER COLUMN VideoURL nvarchar(max)
ALTER TABLE BusinessToolsMst ALTER COLUMN ImageName nvarchar(max)
ALTER TABLE BusinessToolsMst ALTER COLUMN PdfName nvarchar(max)
GO
/****** Object:  Table [dbo].[QuerySupportMst]    Script Date: 09/25/2019 22:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuerySupportMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuerySupportMst](
	[QuerySupportId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,	
	[Subject] [nvarchar](max) NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[QueryStatus] int NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,	
 CONSTRAINT [PK_QuerySupportMst_QuerySupportId] PRIMARY KEY CLUSTERED 
(
	[QuerySupportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[QuerySupportMst]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])

END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Attachment'
      AND Object_ID = Object_ID(N'QuerySupportMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE QuerySupportMst Add Attachment nvarchar(max)
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Response'
      AND Object_ID = Object_ID(N'QuerySupportMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE QuerySupportMst Add Response nvarchar(max)
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ResponseDate'
      AND Object_ID = Object_ID(N'QuerySupportMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE QuerySupportMst Add ResponseDate nvarchar(max)
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DirectPurchaseVolume'
      AND Object_ID = Object_ID(N'UserPurchaseDtl'))
BEGIN
    -- Column Not Exists
    ALTER TABLE UserPurchaseDtl Add DirectPurchaseVolume float
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DirectMatchingVolume'
      AND Object_ID = Object_ID(N'UserPurchaseDtl'))
BEGIN
    -- Column Not Exists
    ALTER TABLE UserPurchaseDtl Add DirectMatchingVolume float
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'RepurchaseMatchingVolume'
      AND Object_ID = Object_ID(N'UserPurchaseDtl'))
BEGIN
    -- Column Not Exists
    ALTER TABLE UserPurchaseDtl Add RepurchaseMatchingVolume float
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Language'
      AND Object_ID = Object_ID(N'BusinessToolsMst'))
BEGIN
    -- Column Not Exists
    ALTER TABLE BusinessToolsMst Add Language nvarchar(max)
END
GO
Update BusinessToolsMst set Language='E' Where Language is null
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsEditable'
      AND Object_ID = Object_ID(N'ConfigMst'))
BEGIN
	ALTER TABLE ConfigMst Add IsEditable bit
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Description'
      AND Object_ID = Object_ID(N'ConfigMst'))
BEGIN
	ALTER TABLE ConfigMst Add Description nvarchar(max)
END
GO
Update ConfigMst set IsEditable=1 Where [key] in ('BonanzaOfferCnt','MinRegistrationPoints','RequiredMemberForGold',
									'RequiredMemberForSilver','AllowCODAsFirstOrder','NoOfMemberForCompanyShare',
									'MinRegistrationPoints')
GO
Update ConfigMst set Description='Member Required For Bonanza Applicable' Where [Key]='BonanzaOfferCnt'
Update ConfigMst set Description='Minimun Registration Points' Where [Key]='MinRegistrationPoints'
Update ConfigMst set Description='No Of Member Pair For Gold Member' Where [Key]='RequiredMemberForGold'
Update ConfigMst set Description='No Of Member Pair For Silver Member' Where [Key]='RequiredMemberForSilver'
Update ConfigMst set Description='Allow First Order As COD' Where [Key]='AllowCODAsFirstOrder'
Update ConfigMst set Description='Pay Company Share For Join Member' Where [Key]='NoOfMemberForCompanyShare'
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'ActivateDate'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst ADD ActivateDate date
END
GO
Update um set um.ActivateDate=uph.OrderDate
from dbo.fn_GetUserFirstOrderId(0) as om
INNER JOIN USerMst as um on om.UserId=um.UserId
INNER JOIN UserPurchaseHdr as uph on om.UserPurchaseHdrId=uph.UserPurchaseHdrId
Where um.ActivateDate is null
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='AdvertisementHeader')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'AdvertisementHeader','',1,1,GETDATE(),1,GETDATE(),1,'Header For Display Advertisement'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='AdvertisementValue')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'AdvertisementValue','',1,1,GETDATE(),1,GETDATE(),1,'Display Advertisement Text'
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'BankingName'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst Add BankingName nvarchar(max)
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='VoltIncomeBV')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'VoltIncomeBV','1000',1,1,GETDATE(),1,GETDATE(),1,'Volt Income Calculate On Direct Purchase BV'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='GoldIncomeBV')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'GoldIncomeBV','75000',1,1,GETDATE(),1,GETDATE(),1,'Matching Purchase BV For Gold Member'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='SilverIncomeBV')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'SilverIncomeBV','25000',1,1,GETDATE(),1,GETDATE(),1,'Matching Purchase BV For Silver Member'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='VoltIncomeAmount')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'VoltIncomeAmount','200',1,1,GETDATE(),1,GETDATE(),1,'Give Volt Income Amount To User on Define BV'
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'NoOfVolts'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst Add NoOfVolts float
END
GO
Update Configmst set IsEditable=0 Where [Key] in ('RequiredMemberForGold','RequiredMemberForSilver')
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'RefferenceNo'
      AND Object_ID = Object_ID(N'ExpenseMst'))
BEGIN
	ALTER TABLE ExpenseMst Add RefferenceNo nvarchar(max)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TopRetailerDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TopRetailerDtl](
	[TopRetailerDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	Month int NOT NULL,
	Year int NOT NULL,
	[SalesAmount] [float] NULL,
	[PaidAmount] [float] NULL,
	[PaidDate] datetime NULL,
	IsPaid bit NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
CONSTRAINT [PK_TopRetailerDtl_TopRetailerDtlId] PRIMARY KEY CLUSTERED 
(
	TopRetailerDtlId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[TopRetailerDtl]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BonanzaRewardDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BonanzaRewardDtl](
	[BonanzaRewardDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	Month int NOT NULL,
	Year int NOT NULL,
	[MonthBV] [float] NULL,
	[PaidAmount] [float] NULL,
	[PaidDate] datetime NULL,
	IsPaid bit NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
CONSTRAINT [PK_BonanzaRewardDtl_BonanzaRewardDtlId] PRIMARY KEY CLUSTERED 
(
	BonanzaRewardDtlId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[BonanzaRewardDtl]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsPrimeMember'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst ADD IsPrimeMember bit
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'PrimeMemberDate'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst ADD PrimeMemberDate datetime
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsDimondMember'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst ADD IsDimondMember bit
END
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DimondMemberDate'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst ADD DimondMemberDate datetime
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='PrimeIncomeBV')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'PrimeIncomeBV','10000',1,1,GETDATE(),1,GETDATE(),1,'Matching Purchase BV For Prime Associate Member'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='DiamondIncomeBV')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'DiamondIncomeBV','250000',1,1,GETDATE(),1,GETDATE(),1,'Matching Purchase BV For Diamond Member'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='PrimeMemberPerc')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'PrimeMemberPerc','10',1,1,GETDATE(),1,GETDATE(),0,'Prime Member Percentage'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='DiamondMemberPerc')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'DiamondMemberPerc','2',1,1,GETDATE(),1,GETDATE(),0,'Diamond Member Percentage'
END
GO
Update UserMst set IsPrimeMember=1,PrimeMemberDate=SilverMemberDate,IsSilverMember=null,SilverMemberDate=null
Where IsSilverMember=1 and IsPrimeMember is null
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiamondAndSilverPointsCreditDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DiamondAndSilverPointsCreditDtl](
	[DiamondAndSilverPointsCreditDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FromCreditUserId] [int] NULL,
	[CreditPerc] [float] NOT NULL,
	[CreditDate] [datetime] NOT NULL,
	[CreditPoints] [numeric](18, 2) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsDiamondPoint] [bit] NOT NULL,
 CONSTRAINT [PK_DiamondAndSilverPointsCreditDtl_DiamondAndSilverPointsCreditDtlId] PRIMARY KEY CLUSTERED 
(
	DiamondAndSilverPointsCreditDtlId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].DiamondAndSilverPointsCreditDtl  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'SilverIncomePay'
      AND Object_ID = Object_ID(N'WalletDebitDtl'))
BEGIN
	ALTER TABLE WalletDebitDtl ADD SilverIncomePay float
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='AllowToPurchaseBelowMinRegistrationPoints')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'AllowToPurchaseBelowMinRegistrationPoints','true',1,1,GETDATE(),1,GETDATE(),0,'Allow User to purchase below min registration when place order for registration.'
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'IsRegistrationMsgSend'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst ADD IsRegistrationMsgSend bit
END
GO
Update UserMst set IsRegistrationMsgSend=1 Where CreatedDate<=getdate() and IsRegistrationActivated=1
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'RegistrationPoints'
      AND Object_ID = Object_ID(N'UserMst'))
BEGIN
	ALTER TABLE UserMst ADD RegistrationPoints float
END
GO
/****** Object:  Table [dbo].[SMSSendDtl]    Script Date: 05/19/2019 13:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FranchiseeMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FranchiseeMst](
	[FranchiseeId] [int] IDENTITY(1,1) NOT NULL,
	[FranchiseeName] [nvarchar](max) NOT NULL,
	[FirstName] [nvarchar](125) NOT NULL,
	[MiddleName] [nvarchar](125) NULL,
	[LastName] [nvarchar](125) NOT NULL,
	[FullName] [nvarchar](500) NOT NULL,
	[EmailId] [nvarchar](100) NOT NULL,
	[MobileNo] [numeric](18, 0) NULL,
	[RegistrationDate] [date] NULL,
	[Address] [nvarchar](500) NULL,
	[CityId] [numeric](18, 0) NULL,
	[PANNo] [nvarchar](20) NULL,
	[GSTNo] [nvarchar](100) NULL,
	[ProfileImage] [nvarchar](500) NULL,
	[BankName] [nvarchar](250) NULL,
	[BankAccNo] [nvarchar](30) NULL,
	[BranchName] [nvarchar](100) NULL,
	[ISFCCode] [nvarchar](20) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [smalldatetime] NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[AlternativeMobileNo] [numeric](18, 0) NULL,
	[PinCode] [nvarchar](6) NULL,
	[Percentage] [float] NULL,
 CONSTRAINT [PK_FranchiseeMst_FranchiseeId] PRIMARY KEY CLUSTERED 
(
	[FranchiseeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

Insert into FranchiseeMst(FranchiseeName,FirstName,MiddleName,LastName,FullName,EmailId,MobileNo,RegistrationDate,
						Address,CityId,PANNo,GSTNo,ProfileImage,BankName,BankAccNo,BranchName,ISFCCode,IsActive
						,CreatedDate,ModifiedDate,AlternativeMobileNo,PinCode,Percentage,UserName,[Password]) 
			values('Maxener Wellness','Maxener','Wellness','Franchisee','Maxener Wellness Franchisee','',0,GETDATE()
					,'',null,null,null,null,null,null,null,null,1,GETDATE(),GETDATE(),0,null,10,'Maxener','Maxener')
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'UserName'
      AND Object_ID = Object_ID(N'FranchiseeMst'))
BEGIN
	ALTER TABLE FranchiseeMst ADD UserName nvarchar(100)
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Password'
      AND Object_ID = Object_ID(N'FranchiseeMst'))
BEGIN
	ALTER TABLE FranchiseeMst ADD Password nvarchar(100)
END
GO
/****** Object:  Table [dbo].[SMSSendDtl]    Script Date: 05/19/2019 13:31:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FranchiseeWalletCreditDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FranchiseeWalletCreditDtl](
	[FranchiseeWalletCreditDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[FranchiseeId] [int] NOT NULL,
	[UserPurchaseHdrId] [bigint] NOT NULL,
	[CreditDate] [datetime] NOT NULL,
	[Percentage] float NOT NULL,
	[CreditAmount] float NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FranchiseeWalletCreditDtl_FranchiseeWalletCreditDtlId] PRIMARY KEY CLUSTERED 
(
	[FranchiseeWalletCreditDtlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].[FranchiseeWalletCreditDtl]  WITH CHECK ADD FOREIGN KEY([FranchiseeId])
REFERENCES [dbo].[FranchiseeMst] ([FranchiseeId])
ALTER TABLE [dbo].[FranchiseeWalletCreditDtl]  WITH CHECK ADD FOREIGN KEY([UserPurchaseHdrId])
REFERENCES [dbo].[UserPurchaseHdr] ([UserPurchaseHdrId])
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'UTRRefNo'
      AND Object_ID = Object_ID(N'BonanzaRewardDtl'))
BEGIN
	ALTER TABLE BonanzaRewardDtl ADD UTRRefNo nvarchar(100)
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='AdvertisementVideoURL')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'AdvertisementVideoURL','',1,1,GETDATE(),1,GETDATE(),1,'Video URL For Display Video Advertisement'
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'OrderReceivedById'
      AND Object_ID = Object_ID(N'UserPurchaseHdr'))
BEGIN
	ALTER TABLE UserPurchaseHdr ADD OrderReceivedById int
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrderReceivedByMst]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrderReceivedByMst](
	[OrderReceivedById] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OrderReceivedBy] [nvarchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_OrderReceivedByMst_OrderReceivedById] PRIMARY KEY CLUSTERED 
(
	[OrderReceivedById] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT 1 from OrderReceivedByMst Where OrderReceivedBy='Razor Pay')
BEGIN
	Insert into OrderReceivedByMst values('Razor Pay',1,1,GETDATE(),1,GETDATE())
END
IF NOT EXISTS(SELECT 1 from OrderReceivedByMst Where OrderReceivedBy='Paytm')
BEGIN
Insert into OrderReceivedByMst values('Paytm',1,1,GETDATE(),1,GETDATE())
END
IF NOT EXISTS(SELECT 1 from OrderReceivedByMst Where OrderReceivedBy='NEFT')
BEGIN
Insert into OrderReceivedByMst values('NEFT',1,1,GETDATE(),1,GETDATE())
END
IF NOT EXISTS(SELECT 1 from OrderReceivedByMst Where OrderReceivedBy='Phone Pay')
BEGIN
Insert into OrderReceivedByMst values('Phone Pay',1,1,GETDATE(),1,GETDATE())
END
IF NOT EXISTS(SELECT 1 from OrderReceivedByMst Where OrderReceivedBy='UPI')
BEGIN
Insert into OrderReceivedByMst values('UPI',1,1,GETDATE(),1,GETDATE())
END
IF NOT EXISTS(SELECT 1 from OrderReceivedByMst Where OrderReceivedBy='Cash')
BEGIN
Insert into OrderReceivedByMst values('Cash',1,1,GETDATE(),1,GETDATE())
END
GO

Declare @RazorpayId int
Select @RazorpayId=OrderReceivedById from OrderReceivedByMst where OrderReceivedBy='Razor Pay'
Update UserPurchaseHdr 
SET OrderReceivedById=@RazorpayId
WHERE RazorpayOrderId<>'' and RazorpayPaymentId<>'' and RazorpaySignature<>'' and OrderReceivedById is null
GO

Update ConfigMst set [Value]='http://api.ask4sms.com/sms/1/text/query' Where [Key]='SMSURL'
Update ConfigMst set [Value]='MAXENER' Where [Key]='SMSUserName'
Update ConfigMst set [Value]='Maxener@54321' Where [Key]='SMSPassword'
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='NoOfSameMobileNoAllow')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'NoOfSameMobileNoAllow','2',1,1,GETDATE(),1,GETDATE(),1,'Allow No. Of Same Mobile No to Allow Registration'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='CODAmount')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'CODAmount','200',1,1,GETDATE(),1,GETDATE(),1,'Amount For COD'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='ShopNowAdvertise')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'ShopNowAdvertise','',1,1,GETDATE(),1,GETDATE(),1,'Advertise Display While User Shop Product'
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MilesCreditDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MilesCreditDtl](
	[MilesCreditDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[UserIdFrom] [bigint] NOT NULL,
	[CreditDate] [datetime] NOT NULL,
	[CreditAmount] [numeric](18, 2) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_MilesCreditDtlId_MilesCreditDtlId] PRIMARY KEY CLUSTERED 
(
	[MilesCreditDtlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[MilesCreditDtl]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MilesDebitDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MilesDebitDtl](
	[MilesDebitDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[UserPurchaseHdrId] bigint NOT NULL,
	[DebitDate] [datetime] NOT NULL,
	[DebitAmount] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_MilesDebitDtl_MilesDebitDtlId] PRIMARY KEY CLUSTERED 
(
	[MilesDebitDtlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[MilesDebitDtl]  WITH CHECK ADD  CONSTRAINT [FK_MilesDebitDtl_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])

ALTER TABLE [dbo].[MilesDebitDtl]  WITH CHECK ADD  CONSTRAINT [FK_MilesDebitDtl_UserPurchaseHdrId] FOREIGN KEY([UserPurchaseHdrId])
REFERENCES [dbo].[UserPurchaseHdr] ([UserPurchaseHdrId])
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='RefernceMilesAmount')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'RefernceMilesAmount','10',1,1,GETDATE(),1,GETDATE(),1,'Miles Amount On User Reference'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='MaxMilesUsePercentage')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'MaxMilesUsePercentage','10',1,1,GETDATE(),1,GETDATE(),1,'Maximun Miles Can Be used At Once Time Order'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='OfficeUserUserName')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'OfficeUserUserName','office',1,1,GETDATE(),1,GETDATE(),1,'Office User UserName'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='OfficeUserPassword')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'OfficeUserPassword','office@123',1,1,GETDATE(),1,GETDATE(),1,'Office User Password'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='AllowToUseMilesAmtInDirectPurchase')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'AllowToUseMilesAmtInDirectPurchase','false',1,1,GETDATE(),1,GETDATE(),1,'Is Allow To Use Miles Amount In Direct Purchase'
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'DiamondIncomePay'
      AND Object_ID = Object_ID(N'WalletDebitDtl'))
BEGIN
	ALTER TABLE WalletDebitDtl ADD DiamondIncomePay float
END
IF Not EXISTS(Select 1 from ConfigMst where [Key]='DefaultLevelLoadInGeneology')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'DefaultLevelLoadInGeneology','4',1,1,GETDATE(),1,GETDATE(),0,'Default Level Load While Genology Load'
END
IF Not EXISTS(Select 1 from ConfigMst where [Key]='ChildLevelLoadInGeneology')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'ChildLevelLoadInGeneology','6',1,1,GETDATE(),1,GETDATE(),0,'Child Level Load While Click On Any Child Geneology'
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompanyTurnOverRewardDtl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CompanyTurnOverRewardDtl](
	[CompanyTurnOverRewardDtlId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[MonthBV] [float] NULL,
	[PaidAmount] [float] NULL,
	[PaidDate] [datetime] NULL,
	[IsPaid] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [numeric](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [numeric](18, 0) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[UTRRefNo] [nvarchar](100) NULL,
 CONSTRAINT [PK_CompanyTurnOverRewardDtl_CompanyTurnOverRewardDtlId] PRIMARY KEY CLUSTERED 
(
	[CompanyTurnOverRewardDtlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[CompanyTurnOverRewardDtl]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMst] ([UserId])

END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='CompanyTurnOverUserBV')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'CompanyTurnOverUserBV','10000',1,1,GETDATE(),1,GETDATE(),1,'User BV For Company Turn Over Qualification'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='BonanzaUserBV')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'BonanzaUserBV','5000',1,1,GETDATE(),1,GETDATE(),1,'User BV For Bonanza Qualification'
END
GO
IF Not EXISTS(Select 1 from ConfigMst where [Key]='BonanzaUserBVPreviousMonths')
BEGIN
	INSERT INTO ConfigMst([Key],[Value],IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsEditable,[Description])
	SELECT 'BonanzaUserBVPreviousMonths','5000',1,1,GETDATE(),1,GETDATE(),1,'User BV Of Previous Months For Bonanza Qualification'
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'BonanzaUserForQualification'
      AND Object_ID = Object_ID(N'BonanzaRewardDtl'))
BEGIN
	ALTER TABLE BonanzaRewardDtl ADD BonanzaUserForQualification float
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'PreviousMonthBV'
      AND Object_ID = Object_ID(N'BonanzaRewardDtl'))
BEGIN
	ALTER TABLE BonanzaRewardDtl ADD PreviousMonthBV float
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Previous2ndMonthBV'
      AND Object_ID = Object_ID(N'BonanzaRewardDtl'))
BEGIN
	ALTER TABLE BonanzaRewardDtl ADD Previous2ndMonthBV float
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Previous3rdMonthBV'
      AND Object_ID = Object_ID(N'BonanzaRewardDtl'))
BEGIN
	ALTER TABLE BonanzaRewardDtl ADD Previous3rdMonthBV float
END
GO
IF NOT EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'CurrentMonthBV'
      AND Object_ID = Object_ID(N'BonanzaRewardDtl'))
BEGIN
	ALTER TABLE BonanzaRewardDtl ADD CurrentMonthBV float
END



------------------------------------------------------All Procedure Start--------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_TOTAL_BV]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_TOTAL_BV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_TOTAL_BV]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_ORDER_CONFIRM_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_ORDER_CONFIRM_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INS_ORDER_CONFIRM_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_GOLD_SILVER_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_GOLD_SILVER_INCOME_OF_USER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INSERT_GOLD_SILVER_INCOME_OF_USER]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_BONANZA_REPORT_BY_BV]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_BONANZA_REPORT_BY_BV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_BONANZA_REPORT_BY_BV]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_MATCHING_INCOME]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_MATCHING_INCOME]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INSERT_MATCHING_INCOME]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_MATCHING_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_MATCHING_INCOME_OF_USER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INSERT_MATCHING_INCOME_OF_USER]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_USER_WALLET_CREDIT_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_USER_WALLET_CREDIT_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INS_USER_WALLET_CREDIT_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_LEADER_CREATE_INCOME_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_LEADER_CREATE_INCOME_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_ADMIN_LEADER_CREATE_INCOME_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_UPLINE_SUPPORT_INCOME_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_UPLINE_SUPPORT_INCOME_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_ADMIN_UPLINE_SUPPORT_INCOME_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_TOP_BONANZA_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_TOP_BONANZA_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_TOP_BONANZA_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_LEFT_SIDE_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_LEFT_SIDE_POINTS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_LEFT_SIDE_POINTS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_RIGHT_SIDE_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_RIGHT_SIDE_POINTS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_RIGHT_SIDE_POINTS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_MATCHING_BV]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_MATCHING_BV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_MATCHING_BV]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_MATCHING_INCOME]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_MATCHING_INCOME]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_MATCHING_INCOME]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_TOTAL_MATCHING_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_TOTAL_MATCHING_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_TOTAL_MATCHING_POINTS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEFT_SIDE_DIRECT_BV]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEFT_SIDE_DIRECT_BV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_LEFT_SIDE_DIRECT_BV]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEFT_SIDE_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEFT_SIDE_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_LEFT_SIDE_POINTS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_RIGHT_SIDE_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_TOTAL_EARNING_DATE_RANGE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_TOTAL_EARNING_DATE_RANGE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_TOTAL_EARNING_DATE_RANGE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_ADMIN_PENDING_PAYOUT_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_ADMIN_PENDING_PAYOUT_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_ADMIN_PENDING_PAYOUT_DETAILS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_RIGHT_SIDE_DIRECT_BV]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_RIGHT_SIDE_DIRECT_BV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_RIGHT_SIDE_DIRECT_BV]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_PROFITE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_PROFITE_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ADMIN_PROFITE_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_PROFITE_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_PROFITE_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ADMIN_PROFITE_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_HIERARCHY_TEAM_MEMBERS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_HIERARCHY_TEAM_MEMBERS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_HIERARCHY_TEAM_MEMBERS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_USER_HIERARCHY]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_USER_HIERARCHY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_USER_HIERARCHY]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_PRODUCT_VARIANT_LIST]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_PRODUCT_VARIANT_LIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_PRODUCT_VARIANT_LIST]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_REPURCHASE_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_REPURCHASE_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_REPURCHASE_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_SALES_ORDER_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_SALES_ORDER_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_SALES_ORDER_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ORDER_INVOICE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ORDER_INVOICE_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ORDER_INVOICE_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_ORDER_DTL]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_ORDER_DTL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_ORDER_DTL]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_USER_COMPANY_SHARE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_USER_COMPANY_SHARE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INS_USER_COMPANY_SHARE]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_USER_POINTS_ON_PURCHASE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_USER_POINTS_ON_PURCHASE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INS_USER_POINTS_ON_PURCHASE]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_LEFT_SIDE_EARNING]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_LEFT_SIDE_EARNING]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_LEFT_SIDE_EARNING]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_PROFILE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_PROFILE_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_PROFILE_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_GOLD_INCOME_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_GOLD_INCOME_AMOUNT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_GOLD_INCOME_AMOUNT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_SILVER_INCOME_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_SILVER_INCOME_AMOUNT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_SILVER_INCOME_AMOUNT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_USER_MILES_DETAILS_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_USER_MILES_DETAILS_FOR_ADMIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_USER_MILES_DETAILS_FOR_ADMIN]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_USER_MILES_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_USER_MILES_FOR_ADMIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_USER_MILES_FOR_ADMIN]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_USER_MILES_LEDGER]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_USER_MILES_LEDGER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_USER_MILES_LEDGER]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_COMPANY_SHARE_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_COMPANY_SHARE_AMOUNT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_COMPANY_SHARE_AMOUNT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_DIAMOND_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_DIAMOND_INCOME_OF_USER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INSERT_DIAMOND_INCOME_OF_USER]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_GOLD_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_GOLD_INCOME_OF_USER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INSERT_GOLD_INCOME_OF_USER]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_SILVER_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_SILVER_INCOME_OF_USER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INSERT_SILVER_INCOME_OF_USER]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_USER_NOTIFICATION]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_USER_NOTIFICATION]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INS_USER_NOTIFICATION]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_WALLET_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_WALLET_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_WALLET_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_ALL_MILES_AMOUNT_FROM_DATE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_ALL_MILES_AMOUNT_FROM_DATE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INS_ALL_MILES_AMOUNT_FROM_DATE]
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_ALL_PARENT_USER_NOTIFICATION]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_ALL_PARENT_USER_NOTIFICATION]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_INS_ALL_PARENT_USER_NOTIFICATION]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_ORDER_HDR]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_ORDER_HDR]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_ORDER_HDR]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_TOTAL_BUSINESS_VOLUME]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_TOTAL_BUSINESS_VOLUME]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_TOTAL_BUSINESS_VOLUME]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_ORDER_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_ORDER_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_USER_ORDER_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_TOP_PURCHASE_PRODUCTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_TOP_PURCHASE_PRODUCTS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_TOP_PURCHASE_PRODUCTS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_TOP_RETAILER_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_TOP_RETAILER_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_TOP_RETAILER_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_PRODUCT_LIST]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_PRODUCT_LIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_PRODUCT_LIST]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_NOTIFICATION_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_NOTIFICATION_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_NOTIFICATION_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_QUERY_SUPPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_QUERY_SUPPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_QUERY_SUPPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_MEMBER_LIST]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_MEMBER_LIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_MEMBER_LIST]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_PARENT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_PARENT_USER_TREE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_PARENT_USER_TREE]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_TOTAL_DIRECT_BV]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_TOTAL_DIRECT_BV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_TOTAL_DIRECT_BV]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_VOLUME_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_VOLUME_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ADMIN_VOLUME_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_WALLET_DEBIT_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_WALLET_DEBIT_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ADMIN_WALLET_DEBIT_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_EXPENSE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_EXPENSE_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_EXPENSE_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_BV_DETAILS_DASHBOARD]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_BV_DETAILS_DASHBOARD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ADMIN_BV_DETAILS_DASHBOARD]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_EXPENSE_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_EXPENSE_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ADMIN_EXPENSE_REPORT]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ADMIN_PROFITE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ADMIN_PROFITE_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ADMIN_PROFITE_DETAILS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_CHILD_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_CHILD_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_CHILD_USER_TREE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_LEFT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_LEFT_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_LEFT_USER_TREE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_PARENT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_PARENT_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_PARENT_USER_TREE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_PARENT_USER_TREE_By_LEVEL]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_PARENT_USER_TREE_By_LEVEL]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_PARENT_USER_TREE_By_LEVEL]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_RIGHT_SIDED_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_RIGHT_SIDED_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_RIGHT_SIDED_USER_TREE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_RIGHT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_RIGHT_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_RIGHT_USER_TREE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_TOTAL_MATCHING_LEADER_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_TOTAL_MATCHING_LEADER_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_TOTAL_MATCHING_LEADER_POINTS]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_TOTAL_MATCHING_UPLINE_SUPPORT_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_TOTAL_MATCHING_UPLINE_SUPPORT_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_GET_ALL_TOTAL_MATCHING_UPLINE_SUPPORT_POINTS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_MEMBER_JOINING_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_MEMBER_JOINING_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ADMIN_MEMBER_JOINING_REPORT]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetUserFirstOrderId]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetUserFirstOrderId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetUserFirstOrderId]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_LEFT_PARENT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_LEFT_PARENT_USER_TREE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_LEFT_PARENT_USER_TREE]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_LEFT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_LEFT_USER_TREE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_LEFT_USER_TREE]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_RIGHT_PARENT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_RIGHT_PARENT_USER_TREE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_RIGHT_PARENT_USER_TREE]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_RIGHT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_RIGHT_USER_TREE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_RIGHT_USER_TREE]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_USER_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_USER_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_ALL_USER_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_REFFERAL_USER_COUNT_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_REFFERAL_USER_COUNT_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_REFFERAL_USER_COUNT_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_FIRST_LAST_DATE_OF_WEEK]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_FIRST_LAST_DATE_OF_WEEK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_FIRST_LAST_DATE_OF_WEEK]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_FRANCHISEE_LIST]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_FRANCHISEE_LIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_FRANCHISEE_LIST]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_MONTH_WISE_WEEK_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_MONTH_WISE_WEEK_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_MONTH_WISE_WEEK_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_TEAM_DIRECT_JOINING_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_TEAM_DIRECT_JOINING_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_TEAM_DIRECT_JOINING_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_BONANZA_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_BONANZA_REPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_BONANZA_REPORT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_MONTH_WISE_ORDER_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_MONTH_WISE_ORDER_AMOUNT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_RPT_GET_MONTH_WISE_ORDER_AMOUNT]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_SERVER_DATE]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_SERVER_DATE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PROC_GET_SERVER_DATE]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_SPLIT]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_SPLIT]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FN_SPLIT]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFirstLastDateOfMonthByDate]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFirstLastDateOfMonthByDate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetFirstLastDateOfMonthByDate]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFirstLastDateOfWeekByDate]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFirstLastDateOfWeekByDate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetFirstLastDateOfWeekByDate]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthWiseWeeks]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetMonthWiseWeeks]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetMonthWiseWeeks]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetServerDate]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetServerDate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetServerDate]
GO
/****** Object:  StoredProcedure [dbo].[sp_generate_inserts]    Script Date: 07/15/2020 17:17:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_generate_inserts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_generate_inserts]
GO
/****** Object:  StoredProcedure [dbo].[sp_generate_inserts]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_generate_inserts]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROC [dbo].[sp_generate_inserts]
(
	@table_name varchar(776),  		-- The table/view for which the INSERT statements will be generated using the existing data
	@target_table varchar(776) = NULL, 	-- Use this parameter to specify a different table name into which the data will be inserted
	@include_column_list bit = 1,		-- Use this parameter to include/ommit column list in the generated INSERT statement
	@from varchar(800) = NULL, 		-- Use this parameter to filter the rows based on a filter condition (using WHERE)
	@include_timestamp bit = 0, 		-- Specify 1 for this parameter, if you want to include the TIMESTAMP/ROWVERSION column''s data in the INSERT statement
	@debug_mode bit = 0,			-- If @debug_mode is set to 1, the SQL statements constructed by this procedure will be printed for later examination
	@owner varchar(64) = NULL,		-- Use this parameter if you are not the owner of the table
	@ommit_images bit = 0,			-- Use this parameter to generate INSERT statements by omitting the ''image'' columns
	@ommit_identity bit = 0,		-- Use this parameter to ommit the identity columns
	@top int = NULL,			-- Use this parameter to generate INSERT statements only for the TOP n rows
	@cols_to_include varchar(8000) = NULL,	-- List of columns to be included in the INSERT statement
	@cols_to_exclude varchar(8000) = NULL,	-- List of columns to be excluded from the INSERT statement
	@disable_constraints bit = 0,		-- When 1, disables foreign key constraints and enables them after the INSERT statements
	@ommit_computed_cols bit = 0		-- When 1, computed columns will not be included in the INSERT statement
	
)
AS
BEGIN

/***********************************************************************************************************
Procedure:	sp_generate_inserts  (Build 22) 
		(Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.)
                                          
Purpose:	To generate INSERT statements from existing data. 
		These INSERTS can be executed to regenerate the data at some other location.
		This procedure is also useful to create a database setup, where in you can 
		script your data along with your table definitions.

Written by:	Narayana Vyas Kondreddi
	        http://vyaskn.tripod.com

Acknowledgements:
		Divya Kalra	-- For beta testing
		Mark Charsley	-- For reporting a problem with scripting uniqueidentifier columns with NULL values
		Artur Zeygman	-- For helping me simplify a bit of code for handling non-dbo owned tables
		Joris Laperre   -- For reporting a regression bug in handling text/ntext columns

Tested on: 	SQL Server 7.0 and SQL Server 2000

Date created:	January 17th 2001 21:52 GMT

Date modified:	May 1st 2002 19:50 GMT

Email: 		vyaskn@hotmail.com

NOTE:		This procedure may not work with tables with too many columns.
		Results can be unpredictable with huge text columns or SQL Server 2000''s sql_variant data types
		Whenever possible, Use @include_column_list parameter to ommit column list in the INSERT statement, for better results
		IMPORTANT: This procedure is not tested with internation data (Extended characters or Unicode). If needed
		you might want to convert the datatypes of character variables in this procedure to their respective unicode counterparts
		like nchar and nvarchar
		

Example 1:	To generate INSERT statements for table ''titles'':
		
		EXEC sp_generate_inserts ''titles''

Example 2: 	To ommit the column list in the INSERT statement: (Column list is included by default)
		IMPORTANT: If you have too many columns, you are advised to ommit column list, as shown below,
		to avoid erroneous results
		
		EXEC sp_generate_inserts ''titles'', @include_column_list = 0

Example 3:	To generate INSERT statements for ''titlesCopy'' table from ''titles'' table:

		EXEC sp_generate_inserts ''titles'', ''titlesCopy''

Example 4:	To generate INSERT statements for ''titles'' table for only those titles 
		which contain the word ''Computer'' in them:
		NOTE: Do not complicate the FROM or WHERE clause here. It''s assumed that you are good with T-SQL if you are using this parameter

		EXEC sp_generate_inserts ''titles'', @from = "from titles where title like ''%Computer%''"

Example 5: 	To specify that you want to include TIMESTAMP column''s data as well in the INSERT statement:
		(By default TIMESTAMP column''s data is not scripted)

		EXEC sp_generate_inserts ''titles'', @include_timestamp = 1

Example 6:	To print the debug information:
  
		EXEC sp_generate_inserts ''titles'', @debug_mode = 1

Example 7: 	If you are not the owner of the table, use @owner parameter to specify the owner name
		To use this option, you must have SELECT permissions on that table

		EXEC sp_generate_inserts Nickstable, @owner = ''Nick''

Example 8: 	To generate INSERT statements for the rest of the columns excluding images
		When using this otion, DO NOT set @include_column_list parameter to 0.

		EXEC sp_generate_inserts imgtable, @ommit_images = 1

Example 9: 	To generate INSERT statements excluding (ommiting) IDENTITY columns:
		(By default IDENTITY columns are included in the INSERT statement)

		EXEC sp_generate_inserts mytable, @ommit_identity = 1

Example 10: 	To generate INSERT statements for the TOP 10 rows in the table:
		
		EXEC sp_generate_inserts mytable, @top = 10

Example 11: 	To generate INSERT statements with only those columns you want:
		
		EXEC sp_generate_inserts titles, @cols_to_include = "''title'',''title_id'',''au_id''"

Example 12: 	To generate INSERT statements by omitting certain columns:
		
		EXEC sp_generate_inserts titles, @cols_to_exclude = "''title'',''title_id'',''au_id''"

Example 13:	To avoid checking the foreign key constraints while loading data with INSERT statements:
		
		EXEC sp_generate_inserts titles, @disable_constraints = 1

Example 14: 	To exclude computed columns from the INSERT statement:
		EXEC sp_generate_inserts MyTable, @ommit_computed_cols = 1
***********************************************************************************************************/

SET NOCOUNT ON

--Making sure user only uses either @cols_to_include or @cols_to_exclude
IF ((@cols_to_include IS NOT NULL) AND (@cols_to_exclude IS NOT NULL))
	BEGIN
		RAISERROR(''Use either @cols_to_include or @cols_to_exclude. Do not use both the parameters at once'',16,1)
		RETURN -1 --Failure. Reason: Both @cols_to_include and @cols_to_exclude parameters are specified
	END

--Making sure the @cols_to_include and @cols_to_exclude parameters are receiving values in proper format
IF ((@cols_to_include IS NOT NULL) AND (PATINDEX(''''''%'''''',@cols_to_include) = 0))
	BEGIN
		RAISERROR(''Invalid use of @cols_to_include property'',16,1)
		PRINT ''Specify column names surrounded by single quotes and separated by commas''
		PRINT ''Eg: EXEC sp_generate_inserts titles, @cols_to_include = "''''title_id'''',''''title''''"''
		RETURN -1 --Failure. Reason: Invalid use of @cols_to_include property
	END

IF ((@cols_to_exclude IS NOT NULL) AND (PATINDEX(''''''%'''''',@cols_to_exclude) = 0))
	BEGIN
		RAISERROR(''Invalid use of @cols_to_exclude property'',16,1)
		PRINT ''Specify column names surrounded by single quotes and separated by commas''
		PRINT ''Eg: EXEC sp_generate_inserts titles, @cols_to_exclude = "''''title_id'''',''''title''''"''
		RETURN -1 --Failure. Reason: Invalid use of @cols_to_exclude property
	END


--Checking to see if the database name is specified along wih the table name
--Your database context should be local to the table for which you want to generate INSERT statements
--specifying the database name is not allowed
IF (PARSENAME(@table_name,3)) IS NOT NULL
	BEGIN
		RAISERROR(''Do not specify the database name. Be in the required database and just specify the table name.'',16,1)
		RETURN -1 --Failure. Reason: Database name is specified along with the table name, which is not allowed
	END

--Checking for the existence of ''user table'' or ''view''
--This procedure is not written to work on system tables
--To script the data in system tables, just create a view on the system tables and script the view instead

IF @owner IS NULL
	BEGIN
		IF ((OBJECT_ID(@table_name,''U'') IS NULL) AND (OBJECT_ID(@table_name,''V'') IS NULL)) 
			BEGIN
				RAISERROR(''User table or view not found.'',16,1)
				PRINT ''You may see this error, if you are not the owner of this table or view. In that case use @owner parameter to specify the owner name.''
				PRINT ''Make sure you have SELECT permission on that table or view.''
				RETURN -1 --Failure. Reason: There is no user table or view with this name
			END
	END
ELSE
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @table_name AND (TABLE_TYPE = ''BASE TABLE'' OR TABLE_TYPE = ''VIEW'') AND TABLE_SCHEMA = @owner)
			BEGIN
				RAISERROR(''User table or view not found.'',16,1)
				PRINT ''You may see this error, if you are not the owner of this table. In that case use @owner parameter to specify the owner name.''
				PRINT ''Make sure you have SELECT permission on that table or view.''
				RETURN -1 --Failure. Reason: There is no user table or view with this name		
			END
	END

--Variable declarations
DECLARE		@Column_ID int, 		
		@Column_List varchar(8000), 
		@Column_Name varchar(128), 
		@Start_Insert varchar(786), 
		@Data_Type varchar(128), 
		@Actual_Values varchar(8000),	--This is the string that will be finally executed to generate INSERT statements
		@IDN varchar(128)		--Will contain the IDENTITY column''s name in the table

--Variable Initialization
SET @IDN = ''''
SET @Column_ID = 0
SET @Column_Name = ''''
SET @Column_List = ''''
SET @Actual_Values = ''''

IF @owner IS NULL 
	BEGIN
		SET @Start_Insert = ''INSERT INTO '' + ''['' + RTRIM(COALESCE(@target_table,@table_name)) + '']'' 
	END
ELSE
	BEGIN
		SET @Start_Insert = ''INSERT '' + ''['' + LTRIM(RTRIM(@owner)) + ''].'' + ''['' + RTRIM(COALESCE(@target_table,@table_name)) + '']'' 		
	END


--To get the first column''s ID

SELECT	@Column_ID = MIN(ORDINAL_POSITION) 	
FROM	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
WHERE 	TABLE_NAME = @table_name AND
(@owner IS NULL OR TABLE_SCHEMA = @owner)



--Loop through all the columns of the table, to get the column names and their data types
WHILE @Column_ID IS NOT NULL
	BEGIN
		SELECT 	@Column_Name = QUOTENAME(COLUMN_NAME), 
		@Data_Type = DATA_TYPE 
		FROM 	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
		WHERE 	ORDINAL_POSITION = @Column_ID AND 
		TABLE_NAME = @table_name AND
		(@owner IS NULL OR TABLE_SCHEMA = @owner)



		IF @cols_to_include IS NOT NULL --Selecting only user specified columns
		BEGIN
			IF CHARINDEX( '''''''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''''''',@cols_to_include) = 0 
			BEGIN
				GOTO SKIP_LOOP
			END
		END

		IF @cols_to_exclude IS NOT NULL --Selecting only user specified columns
		BEGIN
			IF CHARINDEX( '''''''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''''''',@cols_to_exclude) <> 0 
			BEGIN
				GOTO SKIP_LOOP
			END
		END

		--Making sure to output SET IDENTITY_INSERT ON/OFF in case the table has an IDENTITY column
		IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + ''.'' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),''IsIdentity'')) = 1 
		BEGIN
			IF @ommit_identity = 0 --Determing whether to include or exclude the IDENTITY column
				SET @IDN = @Column_Name
			ELSE
				GOTO SKIP_LOOP			
		END
		
		--Making sure whether to output computed columns or not
		IF @ommit_computed_cols = 1
		BEGIN
			IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + ''.'' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),''IsComputed'')) = 1 
			BEGIN
				GOTO SKIP_LOOP					
			END
		END
		
		--Tables with columns of IMAGE data type are not supported for obvious reasons
		IF(@Data_Type in (''image''))
			BEGIN
				IF (@ommit_images = 0)
					BEGIN
						RAISERROR(''Tables with image columns are not supported.'',16,1)
						PRINT ''Use @ommit_images = 1 parameter to generate INSERTs for the rest of the columns.''
						PRINT ''DO NOT ommit Column List in the INSERT statements. If you ommit column list using @include_column_list=0, the generated INSERTs will fail.''
						RETURN -1 --Failure. Reason: There is a column with image data type
					END
				ELSE
					BEGIN
					GOTO SKIP_LOOP
					END
			END

		--Determining the data type of the column and depending on the data type, the VALUES part of
		--the INSERT statement is generated. Care is taken to handle columns with NULL values. Also
		--making sure, not to lose any data from flot, real, money, smallmomey, datetime columns
		SET @Actual_Values = @Actual_Values  +
		CASE 
			WHEN @Data_Type IN (''char'',''varchar'',''nchar'',''nvarchar'') 
				THEN 
					''COALESCE('''''''''''''''' + REPLACE(RTRIM('' + @Column_Name + ''),'''''''''''''''','''''''''''''''''''''''')+'''''''''''''''',''''NULL'''')''
			WHEN @Data_Type IN (''datetime'',''smalldatetime'') 
				THEN 
					''COALESCE('''''''''''''''' + RTRIM(CONVERT(char,'' + @Column_Name + '',109))+'''''''''''''''',''''NULL'''')''
			WHEN @Data_Type IN (''uniqueidentifier'') 
				THEN  
					''COALESCE('''''''''''''''' + REPLACE(CONVERT(char(255),RTRIM('' + @Column_Name + '')),'''''''''''''''','''''''''''''''''''''''')+'''''''''''''''',''''NULL'''')''
			WHEN @Data_Type IN (''text'',''ntext'') 
				THEN  
					''COALESCE('''''''''''''''' + REPLACE(CONVERT(char(8000),'' + @Column_Name + ''),'''''''''''''''','''''''''''''''''''''''')+'''''''''''''''',''''NULL'''')''					
			WHEN @Data_Type IN (''binary'',''varbinary'') 
				THEN  
					''COALESCE(RTRIM(CONVERT(char,'' + ''CONVERT(int,'' + @Column_Name + ''))),''''NULL'''')''  
			WHEN @Data_Type IN (''timestamp'',''rowversion'') 
				THEN  
					CASE 
						WHEN @include_timestamp = 0 
							THEN 
								''''''DEFAULT'''''' 
							ELSE 
								''COALESCE(RTRIM(CONVERT(char,'' + ''CONVERT(int,'' + @Column_Name + ''))),''''NULL'''')''  
					END
			WHEN @Data_Type IN (''float'',''real'',''money'',''smallmoney'')
				THEN
					''COALESCE(LTRIM(RTRIM('' + ''CONVERT(char, '' +  @Column_Name  + '',2)'' + '')),''''NULL'''')'' 
			ELSE 
				''COALESCE(LTRIM(RTRIM('' + ''CONVERT(char, '' +  @Column_Name  + '')'' + '')),''''NULL'''')'' 
		END   + ''+'' +  '''''','''''' + '' + ''
		
		--Generating the column list for the INSERT statement
		SET @Column_List = @Column_List +  @Column_Name + '',''	

		SKIP_LOOP: --The label used in GOTO

		SELECT 	@Column_ID = MIN(ORDINAL_POSITION) 
		FROM 	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
		WHERE 	TABLE_NAME = @table_name AND 
		ORDINAL_POSITION > @Column_ID AND
		(@owner IS NULL OR TABLE_SCHEMA = @owner)


	--Loop ends here!
	END

--To get rid of the extra characters that got concatenated during the last run through the loop
SET @Column_List = LEFT(@Column_List,len(@Column_List) - 1)
SET @Actual_Values = LEFT(@Actual_Values,len(@Actual_Values) - 6)

IF LTRIM(@Column_List) = '''' 
	BEGIN
		RAISERROR(''No columns to select. There should at least be one column to generate the output'',16,1)
		RETURN -1 --Failure. Reason: Looks like all the columns are ommitted using the @cols_to_exclude parameter
	END

--Forming the final string that will be executed, to output the INSERT statements
IF (@include_column_list <> 0)
	BEGIN
		SET @Actual_Values = 
			''SELECT '' +  
			CASE WHEN @top IS NULL OR @top < 0 THEN '''' ELSE '' TOP '' + LTRIM(STR(@top)) + '' '' END + 
			'''''''' + RTRIM(@Start_Insert) + 
			'' ''''+'' + ''''''('' + RTRIM(@Column_List) +  ''''''+'' + '''''')'''''' + 
			'' +''''VALUES(''''+ '' +  @Actual_Values  + ''+'''')'''''' + '' '' + 
			COALESCE(@from,'' FROM '' + CASE WHEN @owner IS NULL THEN '''' ELSE ''['' + LTRIM(RTRIM(@owner)) + ''].'' END + ''['' + rtrim(@table_name) + '']'' + ''(NOLOCK)'')
	END
ELSE IF (@include_column_list = 0)
	BEGIN
		SET @Actual_Values = 
			''SELECT '' + 
			CASE WHEN @top IS NULL OR @top < 0 THEN '''' ELSE '' TOP '' + LTRIM(STR(@top)) + '' '' END + 
			'''''''' + RTRIM(@Start_Insert) + 
			'' '''' +''''VALUES(''''+ '' +  @Actual_Values + ''+'''')'''''' + '' '' + 
			COALESCE(@from,'' FROM '' + CASE WHEN @owner IS NULL THEN '''' ELSE ''['' + LTRIM(RTRIM(@owner)) + ''].'' END + ''['' + rtrim(@table_name) + '']'' + ''(NOLOCK)'')
	END	

--Determining whether to ouput any debug information
IF @debug_mode =1
	BEGIN
		PRINT ''/*****START OF DEBUG INFORMATION*****''
		PRINT ''Beginning of the INSERT statement:''
		PRINT @Start_Insert
		PRINT ''''
		PRINT ''The column list:''
		PRINT @Column_List
		PRINT ''''
		PRINT ''The SELECT statement executed to generate the INSERTs''
		PRINT @Actual_Values
		PRINT ''''
		PRINT ''*****END OF DEBUG INFORMATION*****/''
		PRINT ''''
	END
		
PRINT ''--INSERTs generated by ''''sp_generate_inserts'''' stored procedure written by Vyas''
PRINT ''--Build number: 22''
PRINT ''--Problems/Suggestions? Contact Vyas @ vyaskn@hotmail.com''
PRINT ''--http://vyaskn.tripod.com''
PRINT ''''
PRINT ''SET NOCOUNT ON''
PRINT ''''


--Determining whether to print IDENTITY_INSERT or not
IF (@IDN <> '''')
	BEGIN
		PRINT ''SET IDENTITY_INSERT '' + QUOTENAME(COALESCE(@owner,USER_NAME())) + ''.'' + QUOTENAME(@table_name) + '' ON''
		PRINT ''GO''
		PRINT ''''
	END


IF @disable_constraints = 1 AND (OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + ''.'' + @table_name, ''U'') IS NOT NULL)
	BEGIN
		IF @owner IS NULL
			BEGIN
				SELECT 	''ALTER TABLE '' + QUOTENAME(COALESCE(@target_table, @table_name)) + '' NOCHECK CONSTRAINT ALL'' AS ''--Code to disable constraints temporarily''
			END
		ELSE
			BEGIN
				SELECT 	''ALTER TABLE '' + QUOTENAME(@owner) + ''.'' + QUOTENAME(COALESCE(@target_table, @table_name)) + '' NOCHECK CONSTRAINT ALL'' AS ''--Code to disable constraints temporarily''
			END

		PRINT ''GO''
	END

PRINT ''''
PRINT ''PRINT ''''Inserting values into '' + ''['' + RTRIM(COALESCE(@target_table,@table_name)) + '']'' + ''''''''


--All the hard work pays off here!!! You''ll get your INSERT statements, when the next line executes!
EXEC (@Actual_Values)

PRINT ''PRINT ''''Done''''''
PRINT ''''


IF @disable_constraints = 1 AND (OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + ''.'' + @table_name, ''U'') IS NOT NULL)
	BEGIN
		IF @owner IS NULL
			BEGIN
				SELECT 	''ALTER TABLE '' + QUOTENAME(COALESCE(@target_table, @table_name)) + '' CHECK CONSTRAINT ALL''  AS ''--Code to enable the previously disabled constraints''
			END
		ELSE
			BEGIN
				SELECT 	''ALTER TABLE '' + QUOTENAME(@owner) + ''.'' + QUOTENAME(COALESCE(@target_table, @table_name)) + '' CHECK CONSTRAINT ALL'' AS ''--Code to enable the previously disabled constraints''
			END

		PRINT ''GO''
	END

PRINT ''''
IF (@IDN <> '''')
	BEGIN
		PRINT ''SET IDENTITY_INSERT '' + QUOTENAME(COALESCE(@owner,USER_NAME())) + ''.'' + QUOTENAME(@table_name) + '' OFF''
		PRINT ''GO''
	END

PRINT ''SET NOCOUNT OFF''


SET NOCOUNT OFF
RETURN 0 --Success. We are done!
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetServerDate]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetServerDate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetServerDate]()
RETURNS datetime
AS 
-- Returns the stock level for the product.
BEGIN
    Declare @ServerDate as datetime
     select @ServerDate = CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GetUTCDate() ),''+05:30''))

    RETURN @ServerDate ;
END;' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthWiseWeeks]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetMonthWiseWeeks]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [fn_GetFirstLastDateOfWeekByDate](dbo.fn_GetServerDate())
CREATE Function [dbo].[fn_GetMonthWiseWeeks]
(
@Year int,
@Month int
)
RETURNS @MonthWeeks table (WeekNo int,StartDate datetime,EndDate datetime) 
AS
BEGIN

	Declare
	@FirstDateOfYear DATETIME,
	@LastDateOfYear DATETIME,
	@FirstDateOfMonth DATETIME,
	@LastDateOfMonth DATETIME
	-- You can change @year to any year you desire
	--SELECT @year = 2019
	--SET @Month=6
	SELECT @FirstDateOfYear = DATEADD(yyyy, @Year - 1900, 0)
	SELECT @LastDateOfYear = DATEADD(yyyy, @Year - 1900 + 1, 0)
	SET @FirstDateOfMonth=cast(cast(@year as nvarchar(max))+''-''+cast(@Month as nvarchar(max))+''-01'' as date)
	SET @LastDateOfMonth=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FirstDateOfMonth)+1,0))

	Declare @Weeks table (RowNo int,WeekDate datetime,[DayName] nvarchar(max),IsFirstDayOfMonth bit)

	Declare @WeekDateRange table(FromDate datetime,ToDate Datetime)

	-- Creating Query to Prepare Year Data
	;WITH cte AS (
	SELECT 1 AS DayID,
	@FirstDateOfYear AS FromDate,
	DATENAME(dw, @FirstDateOfYear) AS Dayname,
	case when DATEADD(month, DATEDIFF(month, 0, @FirstDateOfYear), 0)=@FirstDateOfYear then 1 else 0 end as IsFirstDayOfMonth
	UNION ALL
	SELECT cte.DayID + 1 AS DayID,
	DATEADD(d, 1 ,cte.FromDate),
	DATENAME(dw, DATEADD(d, 1 ,cte.FromDate)) AS Dayname,
	case when DATEADD(month, DATEDIFF(month, 0, DATEADD(d, 1 ,cte.FromDate)), 0)=DATEADD(d, 1 ,cte.FromDate) then 1 else 0 end as IsFirstDayOfMonth
	FROM cte
	WHERE DATEADD(d,1,cte.FromDate) < @LastDateOfYear
	)
	insert into @Weeks
	SELECT ROW_NUMBER() over( order by FromDate) as RowNo,FromDate AS Date, Dayname,IsFirstDayOfMonth
	FROM CTE
	WHERE MONTH(FromDate)=@Month and (DayName LIKE ''Sunday'' or IsFirstDayOfMonth=1 )
	/*
	WHERE DayName IN (''Saturday,Sunday'') -- For Weekend
	WHERE DayName NOT IN (''Saturday'',''Sunday'') -- For Weekday
	WHERE DayName LIKE ''Monday'' -- For Monday
	WHERE DayName LIKE ''Sunday''-- For Sunday
	*/
	OPTION (MaxRecursion 370)

	--Select * from @Weeks


	;WITH CTE AS (
	SELECT
	rownum = RowNo,
	WeekDate
	FROM @Weeks p
	)
	insert into @MonthWeeks
	SELECT
	CTE.rownum as WeekNo,
	CTE.WeekDate as FromDate,
	--nex.WeekDate NextValue,
	case when CTE.rownum=1 AND CTE.WeekDate<>@FirstDateOfMonth then DATEADD(D,-1,nex.WeekDate) 
		When nex.WeekDate IS null then @LastDateOfMonth
		else DATEADD(S,59,DATEADD(MI,59,DATEADD(HH,23,DATEADD(D,-1,nex.WeekDate))))  end as ToDate
	FROM CTE
	LEFT JOIN CTE nex ON nex.rownum = CTE.rownum + 1

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFirstLastDateOfWeekByDate]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFirstLastDateOfWeekByDate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [fn_GetFirstLastDateOfWeekByDate](dbo.fn_GetServerDate())
CREATE Function [dbo].[fn_GetFirstLastDateOfWeekByDate]
(
@date datetime
)
RETURNS @WeekDtl table (StartDate datetime,EndDate datetime) 
AS
BEGIN
	insert into @WeekDtl
	SELECT DATEADD(DAY, 1- DATEPART(DW, CAST(@date as date)), CAST(@date as date)),DATEADD(DAY, 7- DATEPART(DW, CAST(@date as datetime)), CAST(@date as datetime))
	
	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFirstLastDateOfMonthByDate]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFirstLastDateOfMonthByDate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
--Select * from [fn_GetFirstLastDateOfMonthByDate](dbo.fn_GetServerDate())
CREATE Function [dbo].[fn_GetFirstLastDateOfMonthByDate]
(
@date datetime
)
RETURNS @MonthDtl table (StartDate datetime,EndDate datetime) 
AS
BEGIN
	insert into @MonthDtl
	SELECT      
	DATEADD(MONTH, DATEDIFF(MONTH, 0, @date) , 0)   as startMonth,
	DATEADD(SECOND, -1, DATEADD(MONTH, 1,  DATEADD(MONTH, DATEDIFF(MONTH, 0, @date) , 0) ) ) as endMonth
	RETURN
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_SPLIT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_SPLIT]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[FN_SPLIT](@String nvarchar(max), @Delimiter char(1))
RETURNS @Results TABLE (ID nvarchar(max))
AS
BEGIN
SET @String=REPLACE(@String,CHAR(13)+CHAR(10),'''')	---Remove Enter In String
SET @String=REPLACE(@String,CHAR(9),'''')	---Remove Tab In String
SET @String=REPLACE(@String,'' '','''')	---Remove Extra Blank string In Strin
SET @String=ltrim(RTRIM(@String))	---Remove First and last Blank String
DECLARE @INDEX INT
DECLARE @SLICE nvarchar(4000)
-- HAVE TO SET TO 1 SO IT DOESNT EQUAL Z
--     ERO FIRST TIME IN LOOP
SELECT @INDEX = 1
WHILE @INDEX !=0
BEGIN
-- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
SELECT @INDEX = CHARINDEX(@Delimiter,@STRING)
-- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
IF @INDEX !=0
SELECT @SLICE = LEFT(@STRING,@INDEX - 1)
ELSE
SELECT @SLICE = @STRING
-- PUT THE ITEM INTO THE RESULTS SET
INSERT INTO @Results(ID) VALUES(LTRIM(RTRIM(@SLICE)))
-- CHOP THE ITEM REMOVED OFF THE MAIN STRING
SELECT @STRING = RIGHT(@STRING,LEN(@STRING) - @INDEX)
-- BREAK OUT IF WE ARE DONE
IF LEN(@STRING) = 0 BREAK
END
RETURN
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_SERVER_DATE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_SERVER_DATE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_SERVER_DATE]
AS 
-- Returns the stock level for the product.
BEGIN
    select  CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GetUTCDate() ),''+05:30'')) as ServerDate

END;' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_MONTH_WISE_ORDER_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_MONTH_WISE_ORDER_AMOUNT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_RPT_GET_MONTH_WISE_ORDER_AMOUNT]
AS
BEGIN
	Declare @date date=getdate()
	Declare @i int=6
	Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20))
	--Select @date,MONTH(@date),YEAR(@date)
	While @i>=1
	BEGIN
		insert into @MonthYear
		Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date)
		set @date=DATEADD(M,-1,@date)
		SET @i=@i-1
	END
	
	--Select * from @MonthYear
	
	Select Id,mm.[Month] as [Month],mm.[Year] as [Year],mm.MonthYear,ISNULL(SUM(uph.TotalOrderAmount),0) as TotalOrderAmount
	from @MonthYear mm	
	LEFT JOIN (SELECT uph.UserPurchaseHdrId,OrderDate,TotalOrderAmount 
	FROM UserPurchaseHdr as uph  
	INNER JOIN UserMst as um on uph.UserId=um.UserId) as uph on Month(OrderDate)=mm.[Month] and Year(OrderDate)=mm.[Year] 	
	group by Id,mm.[Month],mm.[Year],mm.MonthYear
	order by [Year] desc,[Month] desc
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_BONANZA_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_BONANZA_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[PROC_RPT_GET_BONANZA_REPORT]
(
@UserId int,
@Month int,
@Year int
)
AS
BEGIN
	--Declare @UserId int=1
	--Declare @Month int=4
	--Declare @Year int=2019

	Select	ROW_NUMBER() over(order by RegistrationDate desc)SrNo,
			ISNULL(case when Position=''L'' then RefferalCode+''-''+FullName end,'''') as LeftPosition,
			ISNULL(case when Position=''R'' then RefferalCode+''-''+FullName end,'''') as RightPosition,
			RegistrationDate,ISNULL(IsRegistrationActivated,0)IsRegistrationActivated
	from UserMst where RefferalUserId=@UserId and RegMonth=@Month and RegYear=@Year
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_TEAM_DIRECT_JOINING_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_TEAM_DIRECT_JOINING_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_TEAM_DIRECT_JOINING_REPORT]
(
@UserId int
)
AS
BEGIN
	--Declare @date date=getdate()
	--Declare @i int=6
	--Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20))
	----Select @date,MONTH(@date),YEAR(@date)
	--While @i>=1
	--BEGIN
	--	insert into @MonthYear
	--	Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date)
	--	set @date=DATEADD(M,-1,@date)
	--	SET @i=@i-1
	--END

	--Select Id,mm.Month as [Month],mm.Year as [Year],mm.MonthYear,Count(UserId) as RegistrationCnt from @MonthYear as mm
	--LEFT JOIN (Select * from UserMst where RefferalUserId=@UserId) as um on mm.Month=RegMonth and mm.Year=RegYear
	--group by mm.Month,mm.Year,mm.MonthYear,Id
	
	Declare @date date=(Select RegistrationDate from UserMst where UserId=@UserId)
	Declare @ToDate date=getdate()
	Declare @i int=1
	Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20))
	--Select @date,MONTH(@date),YEAR(@date)
	--While @ToDate>=@date
	IF @ToDate>DATEADD(M,6,@date)
	BEGIN
		SET @i=6
		While @i>0
		BEGIN
			insert into @MonthYear
			Select @i,MONTH(@ToDate),YEAR(@ToDate),LEFT(DATENAME(month, @ToDate), 3) + ''-'' +  DATENAME(year,  @ToDate)
			--set @date=DATEADD(M,-1,@date)
			SET @i=@i-1
			SET @ToDate=DATEADD(M,-1,@ToDate)
		END
	END
	ELSE
	BEGIN
		While @i<=6
		BEGIN
			insert into @MonthYear
			Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date)
			--set @date=DATEADD(M,-1,@date)
			SET @i=@i+1
			SET @date=DATEADD(M,1,@date)
		END
	END
	--IF NOT EXISTS(Select 1 from @MonthYear where [Month]=MONTH(@ToDate) AND  [Year]=YEAR(@ToDate))
	--BEGIN
	--	insert into @MonthYear
	--	Select @i,MONTH(@ToDate),YEAR(@ToDate),LEFT(DATENAME(month, @ToDate), 3) + ''-'' +  DATENAME(year,  @ToDate)
	--END
	
	Select Id,mm.Month as [Month],mm.Year as [Year],mm.MonthYear,
			ISNULL(SUM(case when ISNULL(IsRegistrationActivated,0)=1 then 1 end),0) as ActiveRegistrationCnt,
			ISNULL(SUM(case when ISNULL(IsRegistrationActivated,0)=0 then 1 end),0) as InActiveRegistrationCnt
	from @MonthYear as mm
	LEFT JOIN (Select * from UserMst 
				where RefferalUserId=@UserId 
				--and ISNULL(IsRegistrationActivated,0)=1
			  ) as um on mm.Month=RegMonth and mm.Year=RegYear
	group by mm.Month,mm.Year,mm.MonthYear,Id
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_MONTH_WISE_WEEK_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_MONTH_WISE_WEEK_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_MONTH_WISE_WEEK_DETAILS]
(
@Year int,
@Month int,
@WeekNo int=0
)
AS
BEGIN
	SELECT * 
	FROM dbo.fn_GetMonthWiseWeeks(@Year,@Month)
	Where WeekNo=case when @WeekNo is null or @WeekNo=0 then WeekNo else @WeekNo end
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_FRANCHISEE_LIST]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_FRANCHISEE_LIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_FRANCHISEE_LIST]
(
@FranchiseeId int=0
)
AS
BEGIN
	SELECT * 
	FROM FranchiseeMst
	WHERE FranchiseeId=CASE WHEN @FranchiseeId=0 then FranchiseeId ELSE @FranchiseeId END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_FIRST_LAST_DATE_OF_WEEK]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_FIRST_LAST_DATE_OF_WEEK]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_FIRST_LAST_DATE_OF_WEEK]
AS
BEGIN
	Select * from [fn_GetFirstLastDateOfWeekByDate](dbo.fn_GetServerDate())
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_REFFERAL_USER_COUNT_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_REFFERAL_USER_COUNT_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_REFFERAL_USER_COUNT_REPORT]
(
@Month int=0,
@Year int=0
)
AS
BEGIN
	SELECT * FROM (
	Select um.UserId,um.RefferalCode,um.UserName,um.FullName,um.MobileNo,um.EmailId,COUNT(rum.UserId) as RefferalCnt 
	from UserMst as um
	INNER JOIN UserMst as rum on um.UserId=rum.RefferalUserId
	Where um.IsRegistrationActivated=1 and rum.IsRegistrationActivated=1
		AND Month(rum.ActivateDate)=case when @Month=0 then  Month(rum.ActivateDate) else @Month end
		AND Year(rum.ActivateDate)=case when @Year=0 then  Year(rum.ActivateDate) else @Year end
	group by um.UserId,um.RefferalCode,um.UserName,um.FullName,um.MobileNo,um.EmailId
	) as users
	ORDER BY RefferalCnt desc
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT]
(
@UserId int=0,
@Month int=0,
@Year int=0
)
AS
BEGIN
	Select um.UserId as RefferalByUserId,rum.UserId,rum.RefferalCode,rum.UserName,rum.FullName,rum.MobileNo,rum.EmailId,
			cast(rum.ActivateDate as date)ActivateDate,convert(varchar(20),rum.ActivateDate,105) as ActivationDate
	from UserMst as um
	INNER JOIN UserMst as rum on um.UserId=rum.RefferalUserId
	Where um.IsRegistrationActivated=1 and rum.IsRegistrationActivated=1
		AND um.UserId=case when @UserId=0 then um.UserId else @UserId end	
		AND Month(rum.ActivateDate)=case when @Month=0 then  Month(rum.ActivateDate) else @Month end
		AND Year(rum.ActivateDate)=case when @Year=0 then  Year(rum.ActivateDate) else @Year end
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_USER_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_USER_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_ALL_USER_DETAILS]
(
@UserId int
)
AS
BEGIN
	Select um.*,
		rum.RefferalCode as RefferalByCode,rum.FullName as RefferalByName,
		 CONVERT(varchar, um.ActivateDate, 103) as ActivationDate
	from UserMst as um
	INNER JOIN UserMst as rum on um.RefferalUserId=rum.UserId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_RIGHT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_RIGHT_USER_TREE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ALL_RIGHT_USER_TREE]
(
@UserId int,
@IncludeSelf bit=0
)
AS
BEGIN

Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);

WITH EXPL (UserId, RightUserId, FullName,UserLevel) AS
     (
      SELECT ROOT.UserId, ROOT.RightUserId, ROOT.FullName,1 as UserLevel
      FROM   UserMst   ROOT
      WHERE  ROOT.UserId = @UserId

      UNION ALL

      SELECT CHILD.UserId, CHILD.RightUserId, CHILD.FullName,PARENT.UserLevel + 1
      FROM   EXPL PARENT, UserMst CHILD
      WHERE  PARENT.RightUserId = CHILD.UserId
     )
insert into @tbl
SELECT   DISTINCT UserId, RightUserId, FullName,UserLevel
FROM     EXPL
ORDER BY UserId, RightUserId
OPTION (maxrecursion 0);

IF @IncludeSelf=0
BEGIN
	Select * from @tbl where UserId<>@UserId
END
ELSE
BEGIN
	Select * from @tbl
END

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_RIGHT_PARENT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_RIGHT_PARENT_USER_TREE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'--[PROC_GET_ALL_RIGHT_PARENT_USER_TREE] 12
CREATE PROCEDURE [dbo].[PROC_GET_ALL_RIGHT_PARENT_USER_TREE]
(
@UserId int,
@IncludeSelf bit=0
)
AS
BEGIN

Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);

WITH EXPL (UserId, ChildUserId, FullName,UserLevel) AS
     (
      SELECT ROOT.UserId, ROOT.RightUserId, ROOT.FullName,1 as UserLevel
      FROM   UserMst   ROOT
      WHERE  ROOT.UserId = @UserId

      UNION ALL

      SELECT CHILD.UserId, CHILD.RightUserId, CHILD.FullName,PARENT.UserLevel + 1
      FROM   EXPL PARENT, UserMst CHILD
      WHERE  PARENT.UserId = CHILD.RightUserId
     )

insert into @tbl
SELECT   DISTINCT UserId, ChildUserId, FullName,UserLevel
FROM     EXPL
ORDER BY UserId, ChildUserId
OPTION (maxrecursion 0);

IF @IncludeSelf=0
BEGIN
	Select * from @tbl where UserId<>@UserId
END
ELSE
BEGIN
	Select * from @tbl
END

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_LEFT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_LEFT_USER_TREE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ALL_LEFT_USER_TREE]
(
@UserId int,
@IncludeSelf bit=0
)
AS
BEGIN

Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);

WITH EXPL (UserId, LeftUserId, FullName,UserLevel) AS
     (
      SELECT ROOT.UserId, ROOT.LeftUserId, ROOT.FullName,1 as UserLevel
      FROM   UserMst   ROOT
      WHERE  ROOT.UserId = @UserId

      UNION ALL

      SELECT CHILD.UserId, CHILD.LeftUserId, CHILD.FullName,PARENT.UserLevel + 1
      FROM   EXPL PARENT, UserMst CHILD
      WHERE  PARENT.LeftUserId = CHILD.UserId
     )

insert into @tbl
SELECT   DISTINCT UserId, LeftUserId, FullName,UserLevel
FROM     EXPL
ORDER BY UserId, LeftUserId
OPTION (maxrecursion 0);
IF @IncludeSelf=0
BEGIN
	Select * from @tbl where UserId<>@UserId
END
ELSE
BEGIN
	Select * from @tbl
END

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_LEFT_PARENT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_LEFT_PARENT_USER_TREE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'--[PROC_GET_ALL_LEFT_PARENT_USER_TREE] 14,1
CREATE PROCEDURE [dbo].[PROC_GET_ALL_LEFT_PARENT_USER_TREE]
(
@UserId int,
@IncludeSelf bit=0
)
AS
BEGIN

Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);

WITH EXPL (UserId, ChildUserId, FullName,UserLevel) AS
     (
      SELECT ROOT.UserId, ROOT.LeftUserId, ROOT.FullName,1 as UserLevel
      FROM   UserMst   ROOT
      WHERE  ROOT.UserId = @UserId

      UNION ALL

      SELECT CHILD.UserId, CHILD.LeftUserId, CHILD.FullName,PARENT.UserLevel + 1
      FROM   EXPL PARENT, UserMst CHILD
      WHERE  PARENT.UserId = CHILD.LeftUserId
     )

insert into @tbl
SELECT   DISTINCT UserId, ChildUserId, FullName,UserLevel
FROM     EXPL
ORDER BY UserId, ChildUserId
OPTION (maxrecursion 0);

IF @IncludeSelf=0
BEGIN
	Select * from @tbl where UserId<>@UserId
END
ELSE
BEGIN
	Select * from @tbl
END

END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetUserFirstOrderId]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetUserFirstOrderId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
--Select * from [fn_GetUserFirstOrderId](default)
CREATE Function [dbo].[fn_GetUserFirstOrderId]
(
@UserId int=0
)
RETURNS @OrderDtl table (RowNo int,UserPurchaseHdrId int,UserId int) 
AS
BEGIN
	insert into @OrderDtl
	Select * from (
		Select Row_Number() over(partition by UserId order by UserPurchaseHdrId) as RowNo,UserPurchaseHdrId,UserId
		from UserPurchaseHdr
		Where UserId=case when @UserId =0 then UserId else @UserId end
	) as oh 
	Where oh.RowNo=1
			
	RETURN
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_MEMBER_JOINING_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_MEMBER_JOINING_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ADMIN_MEMBER_JOINING_REPORT]
AS
BEGIN
	Declare @date date=getdate()
	Declare @i int=6
	Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20))
	--Select @date,MONTH(@date),YEAR(@date)
	While @i>=1
	BEGIN
		insert into @MonthYear
		Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date)
		set @date=DATEADD(M,-1,@date)
		SET @i=@i-1
	END
--IF NOT EXISTS(Select 1 from @MonthYear where [Month]=MONTH(@date) AND  [Year]=YEAR(@date))
--BEGIN
--	insert into @MonthYear
--	Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date)
--END

Select Id,mm.Month as [Month],mm.Year as [Year],mm.MonthYear,
	Count(case when ISNULL(IsRegistrationActivated,0)=0 then UserId end) as InActiveRegistrationCnt ,
	Count(case when ISNULL(IsRegistrationActivated,0)=1 then UserId end) as ActiveRegistrationCnt 
from @MonthYear as mm
LEFT JOIN UserMst as um on mm.Month=MONTH(ISNULL(ActivateDate,RegistrationDate)) and mm.Year=YEAR(ISNULL(ActivateDate,RegistrationDate))
group by mm.Month,mm.Year,mm.MonthYear,Id
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_TOTAL_MATCHING_UPLINE_SUPPORT_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_TOTAL_MATCHING_UPLINE_SUPPORT_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_ALL_TOTAL_MATCHING_UPLINE_SUPPORT_POINTS](0)
CREATE Function [dbo].[FN_GET_ALL_TOTAL_MATCHING_UPLINE_SUPPORT_POINTS]
(
@UserId int=0
)
RETURNS @tblUserPoints table(UserId int,FinalUplineSupportPoints numeric(18,0))
AS
BEGIN

	Declare @tblUser table(RowNo int,UserId int)
	insert into @tblUser
	Select ROW_NUMBER() over(order by UserId) as RowNo,UserId 
	from UserMst 
	where RefferalUserId<>0
		AND UserId=case when @UserId=0 then UserId else @UserId end
		
	--Declare @tblUserPoints table(UserId int,FinalPoints numeric(18,0))

	Declare @MaxRowNo int,@i int=1,@TempUserId int
	Select @MaxRowNo=MAX(RowNo) from @tblUser
	While @i<=@MaxRowNo
	BEGIN
		Select @TempUserId=UserId from @tblUser where RowNo=@i
		
		Declare @TotalRightPoints numeric(18,2)=0, @TotalLeftPoints numeric(18,2)=0,@FinalPoints numeric(18,2)=0
		
		Select @TotalLeftPoints=ISNULL(SUM(LeftPoints),0) from dbo.FN_GET_USER_LEFT_SIDE_UPLINE_SUPPORT_POINTS(@TempUserId)
		Select @TotalRightPoints=ISNULL(SUM(RightPoints),0) from dbo.FN_GET_USER_RIGHT_SIDE_UPLINE_SUPPORT_POINTS(@TempUserId)
		
		SET @TotalLeftPoints=ISNULL(@TotalLeftPoints,0)
		SET @TotalRightPoints=ISNULL(@TotalRightPoints,0)
		SET @FinalPoints=case when @TotalLeftPoints>=@TotalRightPoints then @TotalRightPoints else @TotalLeftPoints end
		
		insert into @tblUserPoints
		Select @TempUserId,@FinalPoints
		SET @i=@i+1
	END

	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_TOTAL_MATCHING_LEADER_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_TOTAL_MATCHING_LEADER_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_ALL_TOTAL_MATCHING_LEADER_POINTS](0)
CREATE Function [dbo].[FN_GET_ALL_TOTAL_MATCHING_LEADER_POINTS]
(
@UserId int=0
)
RETURNS @tblUserPoints table(UserId int,FinalLeaderPoints numeric(18,0))
AS
BEGIN

	Declare @tblUser table(RowNo int,UserId int)
	insert into @tblUser
	Select ROW_NUMBER() over(order by UserId) as RowNo,UserId 
	from UserMst 
	where RefferalUserId<>0
		AND UserId=case when @UserId=0 then UserId else @UserId end
		
	--Declare @tblUserPoints table(UserId int,FinalPoints numeric(18,0))

	Declare @MaxRowNo int,@i int=1,@TempUserId int
	Select @MaxRowNo=MAX(RowNo) from @tblUser
	While @i<=@MaxRowNo
	BEGIN
		Select @TempUserId=UserId from @tblUser where RowNo=@i
		
		Declare @TotalRightPoints numeric(18,2)=0, @TotalLeftPoints numeric(18,2)=0,@FinalPoints numeric(18,2)=0
		
		Select @TotalLeftPoints=ISNULL(SUM(LeftPoints),0) from dbo.FN_GET_USER_LEFT_SIDE_LEADER_POINTS(@TempUserId)
		Select @TotalRightPoints=ISNULL(SUM(RightPoints),0) from dbo.FN_GET_USER_RIGHT_SIDE_LEADER_POINTS(@TempUserId)
		
		SET @TotalLeftPoints=ISNULL(@TotalLeftPoints,0)
		SET @TotalRightPoints=ISNULL(@TotalRightPoints,0)
		SET @FinalPoints=case when @TotalLeftPoints>=@TotalRightPoints then @TotalRightPoints else @TotalLeftPoints end
		
		insert into @tblUserPoints
		Select @TempUserId,@FinalPoints
		SET @i=@i+1
	END

	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_RIGHT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_RIGHT_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
--Select * from [FN_GET_ALL_RIGHT_USER_TREE](1,0)
CREATE Function [dbo].[FN_GET_ALL_RIGHT_USER_TREE]
(
@UserId int,
@IncludeSelf bit=0
)
RETURNS @Usertbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int)
AS
BEGIN

	Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);

	WITH EXPL (UserId, RightUserId, FullName,UserLevel) AS
		 (
		  SELECT ROOT.UserId, ROOT.RightUserId, ROOT.FullName,1 as UserLevel
		  FROM   UserMst   ROOT
		  WHERE  ROOT.UserId = @UserId

		  UNION ALL

		  SELECT CHILD.UserId, CHILD.RightUserId, CHILD.FullName,PARENT.UserLevel + 1
		  FROM   EXPL PARENT, UserMst CHILD
		  WHERE  PARENT.RightUserId = CHILD.UserId
		 )
	insert into @tbl
	SELECT   DISTINCT UserId, RightUserId, FullName,UserLevel
	FROM     EXPL
	ORDER BY UserId, RightUserId
	OPTION (maxrecursion 0);

	IF @IncludeSelf=0
	BEGIN
		insert into @Usertbl
		Select * from @tbl where UserId<>@UserId
	END
	ELSE
	BEGIN
		insert into @Usertbl
		Select * from @tbl
	END

	RETURN
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_RIGHT_SIDED_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_RIGHT_SIDED_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_ALL_RIGHT_SIDED_USER_TREE](1,1)
CREATE Function [dbo].[FN_GET_ALL_RIGHT_SIDED_USER_TREE]
(
@UserId int,
@IncludeSelf bit=1
)
RETURNS @Usertbl table(LeftUserId int, RightUserId int,UserId int,FullName nvarchar(max),RefferalCode nvarchar(max), UserLevel int,IsRegistered bit,RegDate datetime)
AS
BEGIN

	Declare @tbl table(LeftUserId int, RightUserId int,UserId int,FullName nvarchar(max),RefferalCode nvarchar(max), UserLevel int,IsRegistrationActivated bit,RegistrationDate datetime);

	--Declare @UserId int=1
	Declare @RightUserId int
	Select @RightUserId=RightUserId from UserMst where userId = @UserId

	;WITH DirectReports(LeftUserId, RightUserId,UserId,FullName,RefferalCode, UserLevel,IsRegistrationActivated,RegistrationDate) AS   
	(  
		SELECT LeftUserId, RightUserId, UserId,FullName,RefferalCode, 1 AS UserLevel ,IsRegistrationActivated ,RegistrationDate
		FROM dbo.UserMst   
		WHERE UserId =@RightUserId
		UNION ALL  
		SELECT e.LeftUserId, e.RightUserId, e.UserId,e.FullName,e.RefferalCode, UserLevel + 1,e.IsRegistrationActivated,e.RegistrationDate  
		FROM dbo.UserMst AS e  
			INNER JOIN DirectReports AS d  
			ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
	)  
	insert into @tbl
	SELECT LeftUserId, RightUserId,UserId,FullName,RefferalCode, UserLevel,ISNULL(IsRegistrationActivated,0)IsRegistered,RegistrationDate
	FROM DirectReports  
	OPTION (maxrecursion 0);
	
	IF @IncludeSelf=0
	BEGIN
		insert into @Usertbl
		Select * from @tbl
	END
	ELSE
	BEGIN
		insert into @Usertbl
		
		Select LeftUserId, RightUserId,UserId,FullName,RefferalCode,0,ISNULL(IsRegistrationActivated,0)IsRegistered,RegistrationDate 
		from UserMst where UserId=@UserId
		UNION ALL
		Select * from @tbl
	END

	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_PARENT_USER_TREE_By_LEVEL]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_PARENT_USER_TREE_By_LEVEL]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_ALL_PARENT_USER_TREE_By_LEVEL](8,1)
CREATE Function [dbo].[FN_GET_ALL_PARENT_USER_TREE_By_LEVEL]
(
@UserId int,
@IncludeSelf bit=1
)
RETURNS @Usertbl table(UserId int, FullName nvarchar(max),RefferalCode nvarchar(max),Position char(10),UserLevel int,IsGoldMember bit,IsDiamondMember bit)
AS
BEGIN
	Declare @Level int=0
	Declare @ActualUserId int=@UserId
	Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);

	--Declare @UserId int=8
	Declare @LoopBreak bit=0
	Declare @UserList table(UserId int,UserLevel int)	
	
	While @LoopBreak=0
	BEGIN
		
		insert into @UserList values(@UserId,@Level)
		
		Declare @Position char(1)
		
		Select @Position=Position from UserMst where UserId=@UserId
		IF @Position=''L''
		BEGIN
			Select @UserId=UserId from UserMst where LeftUserId=@UserId
		END
		ELSE IF @Position=''R''
		BEGIN
			Select @UserId=UserId from UserMst where RightUserId=@UserId
		END 
		ELSE
			SET @LoopBreak=1
		
		SET @Level=@Level+1
	END

	
	IF @IncludeSelf=0
	BEGIN
		insert into @Usertbl
		Select ul.UserId,um.FullName,um.RefferalCode,um.Position,UserLevel,
			ISNULL(um.IsGoldMember,0)IsGoldMember,ISNULL(um.IsDimondMember,0) IsDiamondMember
		from @UserList as ul
		INNER JOIN UserMst as um on ul.UserId=um.UserId
		where ul.UserId<>@ActualUserId
	END
	ELSE
	BEGIN
		insert into @Usertbl
		Select ul.UserId,um.FullName,um.RefferalCode,um.Position,UserLevel,
			ISNULL(um.IsGoldMember,0)IsGoldMember,ISNULL(um.IsDimondMember,0) IsDiamondMember
		from @UserList as ul
		INNER JOIN UserMst as um on ul.UserId=um.UserId
	END

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_PARENT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_PARENT_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

--Select * from [FN_GET_ALL_PARENT_USER_TREE](8,1)
CREATE Function [dbo].[FN_GET_ALL_PARENT_USER_TREE]
(
@UserId int,
@IncludeSelf bit=1
)
RETURNS @Usertbl table(UserId int, FullName nvarchar(max),RefferalCode nvarchar(max),Position char(10))
AS
BEGIN
	Declare @ActualUserId int=@UserId
	Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);

	--Declare @UserId int=8
	Declare @LoopBreak bit=0
	Declare @UserList table(UserId int)

	While @LoopBreak=0
	BEGIN
		insert into @UserList values(@UserId)
		
		Declare @Position char(1)
		Select @Position=Position from UserMst where UserId=@UserId
		IF @Position=''L''
		BEGIN
			Select @UserId=UserId from UserMst where LeftUserId=@UserId
		END
		ELSE IF @Position=''R''
		BEGIN
			Select @UserId=UserId from UserMst where RightUserId=@UserId
		END 
		ELSE
			SET @LoopBreak=1
	END

	
	IF @IncludeSelf=0
	BEGIN
		insert into @Usertbl
		Select ul.UserId,um.FullName,um.RefferalCode,um.Position
		from @UserList as ul
		INNER JOIN UserMst as um on ul.UserId=um.UserId
		where ul.UserId<>@ActualUserId
	END
	ELSE
	BEGIN
		insert into @Usertbl
		Select ul.UserId,um.FullName,um.RefferalCode,um.Position
		from @UserList as ul
		INNER JOIN UserMst as um on ul.UserId=um.UserId
	END

	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_LEFT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_LEFT_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
--Select * from [FN_GET_ALL_LEFT_USER_TREE](1,0)
CREATE Function [dbo].[FN_GET_ALL_LEFT_USER_TREE]
(
@UserId int,
@IncludeSelf bit=0
)
RETURNS @Usertbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int)
AS
BEGIN

	Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);

	WITH EXPL (UserId, LeftUserId, FullName,UserLevel) AS
		 (
		  SELECT ROOT.UserId, ROOT.LeftUserId, ROOT.FullName,1 as UserLevel
		  FROM   UserMst   ROOT
		  WHERE  ROOT.UserId = @UserId

		  UNION ALL

		  SELECT CHILD.UserId, CHILD.LeftUserId, CHILD.FullName,PARENT.UserLevel + 1
		  FROM   EXPL PARENT, UserMst CHILD
		  WHERE  PARENT.LeftUserId = CHILD.UserId
		 )

	insert into @tbl
	SELECT   DISTINCT UserId, LeftUserId, FullName,UserLevel
	FROM     EXPL
	ORDER BY UserId, LeftUserId
	OPTION (maxrecursion 0);
	
	IF @IncludeSelf=0
	BEGIN
		insert into @Usertbl
		Select * from @tbl where UserId<>@UserId
	END
	ELSE
	BEGIN
		insert into @Usertbl
		Select * from @tbl
	END

	RETURN
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_ALL_LEFT_SIDED_USER_TREE](1,0)
CREATE Function [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]
(
@UserId int,
@IncludeSelf bit=1
)
RETURNS @Usertbl table(LeftUserId int, RightUserId int,UserId int,FullName nvarchar(max),RefferalCode nvarchar(max), UserLevel int,IsRegistered bit,RegDate datetime)
AS
BEGIN

	Declare @tbl table(LeftUserId int, RightUserId int,UserId int,FullName nvarchar(max),RefferalCode nvarchar(max), UserLevel int,IsRegistrationActivated bit,RegistrationDate datetime);

	--Declare @UserId int=1
	Declare @LeftUserId int
	Select @LeftUserId=LeftUserId from UserMst where userId = @UserId

	;WITH DirectReports(LeftUserId, RightUserId,UserId,FullName,RefferalCode, UserLevel,IsRegistrationActivated,RegistrationDate) AS   
	(  
		SELECT LeftUserId, RightUserId, UserId,FullName,RefferalCode, 1 AS UserLevel,IsRegistrationActivated,RegistrationDate
		FROM dbo.UserMst   
		WHERE UserId =@LeftUserId
		UNION ALL  
		SELECT e.LeftUserId, e.RightUserId, e.UserId,e.FullName,e.RefferalCode, UserLevel + 1,e.IsRegistrationActivated ,e.RegistrationDate
		FROM dbo.UserMst AS e  
			INNER JOIN DirectReports AS d  
			ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
	)  
	insert into @tbl
	SELECT LeftUserId, RightUserId,UserId,FullName,RefferalCode, UserLevel,ISNULL(IsRegistrationActivated,0)IsRegistered,RegistrationDate
	FROM DirectReports  
	OPTION (maxrecursion 0);
	
	IF @IncludeSelf=0
	BEGIN
		insert into @Usertbl
		Select * from @tbl
	END
	ELSE
	BEGIN
		insert into @Usertbl
		
		Select LeftUserId, RightUserId,UserId,FullName,RefferalCode,0,ISNULL(IsRegistrationActivated,0)IsRegistered,RegistrationDate
		from UserMst where UserId=@UserId
		UNION ALL
		Select * from @tbl
	END

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE]
(
@IsLeaderPoint bit,
@UserId int=0,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(UserId numeric(18,0), CreditPoints numeric(18,0))
AS
BEGIN

	insert into @CreditDtl
	SELECT UserId,SUM(CreditPoints) as CreditPoints
	FROM LeaderAndUplinePointsCreditDtl 
	Where UserId=(case when @UserId=0 then UserId else @UserId end)
			and IsLeaderPoint=@IsLeaderPoint
			AND cast(CreditDate as date)>=case when @FromDate is null then cast(CreditDate as date) else @FromDate end
			AND cast(CreditDate as date)<=case when @ToDate is null then cast(CreditDate as date) else @ToDate end
	group by UserId

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS](1,default,default,default)
--Select * from [FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS](1,2,default,default)
CREATE Function [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS]
(
@IsLeaderPoint bit,
@UserId int=0,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(UserId numeric(18,0), CreditPoints numeric(18,0))
AS
BEGIN

	insert into @CreditDtl
	SELECT UserId,SUM(CreditPoints) as CreditPoints
	FROM LeaderAndUplinePointsCreditDtl 
	Where UserId=(case when @UserId=0 then UserId else @UserId end)
			and IsLeaderPoint=@IsLeaderPoint
			AND(@Fromdate is null or CreditDate>=@FromDate )
			AND(@ToDate is null or CreditDate<=@ToDate )
	group by UserId

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE](1,default)
--Select * from [FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE](1,2)
CREATE Function [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE]
(
@IsLeaderPoint bit,
@UserId int=0
)
RETURNS @CreditDtl table(UserId numeric(18,0), CreditDate datetime, CreditPoints numeric(18,0))
AS
BEGIN

	insert into @CreditDtl
	SELECT UserId,CreditDate,SUM(CreditPoints) as CreditPoints
	FROM LeaderAndUplinePointsCreditDtl 
	Where UserId=(case when @UserId=0 then UserId else @UserId end)
			and IsLeaderPoint=@IsLeaderPoint
	group by UserId,CreditDate

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](1,default)
--Select * from [FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](1,2)
CREATE Function [dbo].[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS]
(
@IsLeaderPoint bit,
@UserId int=0
)
RETURNS @CreditDtl table(UserId numeric(18,0), CreditDate datetime, CreditPoints numeric(18,0),FromCreditRefferalCode nvarchar(max),FromCreditUserName nvarchar(max))
AS
BEGIN

	insert into @CreditDtl
	SELECT lupcd.UserId,lupcd.CreditDate,lupcd.CreditPoints,um.RefferalCode as FromCreditRefferalCode,um.FullName as FromCreditUserName
	FROM LeaderAndUplinePointsCreditDtl as lupcd 
	INNER JOIN UserMst as um on lupcd.FromCreditUserId=um.UserId
	Where lupcd.UserId=(case when @UserId=0 then lupcd.UserId else @UserId end)
			and IsLeaderPoint=@IsLeaderPoint

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS](1,default,default,default)
--Select * from [FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS](1,2)
CREATE Function [dbo].[FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]
(
@IsDiamondPoint bit,
@UserId int=0,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(SrNo int,UserId numeric(18,0), CreditDate datetime, CreditPoints numeric(18,0),FromCreditRefferalCode nvarchar(max),FromCreditUserName nvarchar(max))
AS
BEGIN

	insert into @CreditDtl
	SELECT ROW_NUMBER() over(Order by dspcd.UserId,dspcd.CreditDate)
			,dspcd.UserId,dspcd.CreditDate,dspcd.CreditPoints,um.RefferalCode as FromCreditRefferalCode,um.FullName as FromCreditUserName
	FROM DiamondAndSilverPointsCreditDtl as dspcd 
	INNER JOIN UserMst as um on dspcd.FromCreditUserId=um.UserId
	Where dspcd.UserId=(case when @UserId=0 then dspcd.UserId else @UserId end)
			and IsDiamondPoint=@IsDiamondPoint
			AND (dspcd.CreditDate>=@FromDate or @FromDate is null) and (dspcd.CreditDate<=@ToDate or @ToDate is null)

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--SELECT * FROM [FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS] (1,default,default,default)
--SELECT * FROM [FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS] (0,default,default,default)
CREATE Function [dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS]
(
@IsDiamondPoint bit,
@UserId int=0,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(UserId numeric(18,0), CreditDate datetime, CreditPoints numeric(18,0),FromCreditRefferalCode nvarchar(max),FromCreditUserName nvarchar(max))
AS
BEGIN

	insert into @CreditDtl
	SELECT lupcd.UserId,lupcd.CreditDate,lupcd.CreditPoints,um.RefferalCode as FromCreditRefferalCode,um.FullName as FromCreditUserName
	--FROM LeaderAndUplinePointsCreditDtl as lupcd 
	--INNER JOIN UserMst as um on lupcd.FromCreditUserId=um.UserId
	--Where lupcd.UserId=(case when @UserId=0 then lupcd.UserId else @UserId end)
	--		and IsLeaderPoint=@IsLeaderPoint			
	FROM DiamondAndSilverPointsCreditDtl as lupcd 
	INNER JOIN UserMst as um on lupcd.FromCreditUserId=um.UserId
	Where lupcd.UserId=(case when @UserId=0 then lupcd.UserId else @UserId end)
			and IsDiamondPoint=@IsDiamondPoint
			AND(@Fromdate is null or cast(CreditDate as date)>=@FromDate )
			AND(@ToDate is null or cast(CreditDate as date)<=@ToDate )

	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS](1,default)
--Select * from [FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS](1,2)
CREATE Function [dbo].[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS]
(
@IsDiamondPoint bit,
@UserId int=0,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(UserId numeric(18,0), CreditPoints numeric(18,0))
AS
BEGIN

	insert into @CreditDtl
	SELECT UserId,SUM(CreditPoints) as CreditPoints
	FROM DiamondAndSilverPointsCreditDtl 
	Where UserId=(case when @UserId=0 then UserId else @UserId end)
			and IsDiamondPoint=@IsDiamondPoint
			AND(@Fromdate is null or CreditDate>=@FromDate )
			AND(@ToDate is null or CreditDate<=@ToDate )
	group by UserId

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_CHILD_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_CHILD_USER_TREE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FN_GET_ALL_CHILD_USER_TREE]
(
@UserId int,
@IncludeSelf bit=0
)
RETURNS @Usertbl table(UserId int,  FullName nvarchar(max),UserLevel int)
AS
BEGIN

	insert into @Usertbl
	Select UserId,FullName,UserLevel from FN_GET_ALL_LEFT_SIDED_USER_TREE(@UserId,@IncludeSelf)
	UNION
	
	Select UserId,FullName,UserLevel from FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,@IncludeSelf)
	
	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ADMIN_PROFITE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ADMIN_PROFITE_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from dbo.[FN_GET_ADMIN_PROFITE_DETAILS](''2019-07-01'',''2019-07-31'')
CREATE Function [dbo].[FN_GET_ADMIN_PROFITE_DETAILS]
(
@FromDate datetime=null,
@ToDate datetime=null
)
RETURNS @ProfiteDtl table(	TotalSales float,TotalCosting float,GrossProfite float,
							TotalTDS float,TotalAdminCharge float,TotalPayout float,
							TotalExpense float,Profite float,PendingPayout float,
							NetProfite float,TotalVolume float,
							TotalDirectIncomePay float,TotalMatchingIncomePay float,
							TotalVoltIncomePay float,TotalLeaderIncomePay float,
							TotalUplineSupportIncomePay float,TotalBonanzaRewardsPay float,
							TotalTopRetailerPay float,TotalSilverIncomePay float)
AS
BEGIN

	Declare @TotalSales float,@TotalCosting float,@GrossProfite float,
			@TotalTDS float,@TotalAdminCharge float,@TotalPayout float,@TotalExpense float,
			@Profite float,@PendingPayout float,@NetProfite float,@TotalVolume float,
			@TotalDirectIncomePay float,@TotalPointsIncomePay float,@TotalVoltIncomePay float,
			@TotalLeaderIncomePay float,@TotalUplineSupportIncomePay float,@TotalSilverIncomePay float,
			@TotalBonanzaRewardsPay float,@TotalTopRetailerPay float

	Select @TotalSales=ISNULL(SUM(Amount),0),@TotalCosting=ISNULL(SUM(Qty*RegularPrice),0),
		   @TotalVolume=ISNULL(SUM(ISNULL(DirectPurchaseVolume,0)),0)+ISNULL(SUM(ISNULL(RepurchaseMatchingVolume,0)),0)
	from UserPurchaseHdr as uph
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	Where cast(OrderDate as date)>=case when @FromDate is null then cast(OrderDate as date) else cast(@FromDate as date) end 
	  and cast(OrderDate as date)<=case when @ToDate is null then cast(OrderDate as date) else cast(@ToDate as date) end 

	SET @GrossProfite=ISNULL(@TotalSales,0)-ISNULL(@TotalCosting,0)
	SET @TotalVolume=ISNULL(@TotalVolume,0)


	Select @TotalTDS=ISNULL(SUM(TDSAmount),0),@TotalAdminCharge=ISNULL(SUM(AdminCharge),0),@TotalPayout=ISNULL(SUM(NetPayable),0),
		@TotalDirectIncomePay=ISNULL(SUM(DirectIncomePay),0),@TotalPointsIncomePay=ISNULL(SUM(PointsIncomePay),0),@TotalVoltIncomePay=ISNULL(SUM(VoltIncomePay),0),
		@TotalLeaderIncomePay=ISNULL(SUM(LeaderIncomePay),0),@TotalUplineSupportIncomePay=ISNULL(SUM(UplineSupportIncomePay),0),
		@TotalSilverIncomePay=ISNULL(SUM(SilverIncomePay),0)	 	 
	from WalletDebitDtl
	where cast(DebitDate as date)>=case when @FromDate is null then cast(DebitDate as date) else cast(@FromDate as date) end 
	  and cast(DebitDate as date)<=case when @ToDate is null then cast(DebitDate as date) else cast(@ToDate as date) end 
	
	Select @TotalExpense=ISNULL(SUM(Amount),0) from ExpenseMst
	Where cast(ExpenseDate as date)>=case when @FromDate is null then cast(ExpenseDate as date) else cast(@FromDate as date) end 
	  and cast(ExpenseDate as date)<=case when @ToDate is null then cast(ExpenseDate as date) else cast(@ToDate as date) end 

	Select @TotalBonanzaRewardsPay=ISNULL(SUM(PaidAmount),0) from BonanzaRewardDtl
	Where cast(PaidDate as date)>=case when @FromDate is null then cast(PaidDate as date) else cast(@FromDate as date) end 
	  and cast(PaidDate as date)<=case when @ToDate is null then cast(PaidDate as date) else cast(@ToDate as date) end 
	  
	Select @TotalTopRetailerPay=ISNULL(SUM(PaidAmount),0) from TopRetailerDtl
	Where cast(PaidDate as date)>=case when @FromDate is null then cast(PaidDate as date) else cast(@FromDate as date) end 
	  and cast(PaidDate as date)<=case when @ToDate is null then cast(PaidDate as date) else cast(@ToDate as date) end 

	SET @Profite=@GrossProfite-(@TotalTDS+@TotalAdminCharge+@TotalPayout+@TotalExpense+@TotalBonanzaRewardsPay+@TotalTopRetailerPay)

	--SELECT @PendingPayout=ISNULL(SUM(NetPayable),0)
	--FROM	dbo.FN_ADMIN_PENDING_PAYOUT_DETAILS(0)
	SET @PendingPayout=ISNULL(@PendingPayout,0)

	SET @NetProfite=@Profite-@PendingPayout
	
	insert into @ProfiteDtl
	Select	@TotalSales as TotalSales,@TotalCosting as TotalCosting,@GrossProfite as GrossProfite,
			@TotalTDS as TotalTDS,@TotalAdminCharge as TotalAdminCharge,@TotalPayout as TotalPayout,
			@TotalExpense as TotalExpense,@Profite as Profite,@PendingPayout as PendingPayout,
			@NetProfite as NetProfite,@TotalVolume as TotalVolume,
			@TotalDirectIncomePay as TotalDirectIncomePay,@TotalPointsIncomePay as TotalMatchingIncomePay,
			@TotalVoltIncomePay as TotalVoltIncomePay,@TotalLeaderIncomePay as TotalLeaderIncomePay,
			@TotalUplineSupportIncomePay as TotalUplineSupportIncomePay,
			@TotalBonanzaRewardsPay as TotalBonanzaRewardsPay,
			@TotalTopRetailerPay as TotalTopRetailerPay,@TotalSilverIncomePay as TotalSilverIncomePay
			
	RETURN
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_EXPENSE_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_EXPENSE_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ADMIN_EXPENSE_REPORT]
AS
BEGIN
	Declare @date date=getdate()
	Declare @i int=6
	Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20))
	--Select @date,MONTH(@date),YEAR(@date)
	While @i>=1
	BEGIN
		insert into @MonthYear
		Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date)
		set @date=DATEADD(M,-1,@date)
		SET @i=@i-1
	END
--IF NOT EXISTS(Select 1 from @MonthYear where [Month]=MONTH(@date) AND  [Year]=YEAR(@date))
--BEGIN
--	insert into @MonthYear
--	Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date)
--END

Select Id,mm.Month as [Month],mm.Year as [Year],mm.MonthYear,ISNULL(SUM(Amount),0) MonthExpense
from @MonthYear as mm
LEFT JOIN ExpenseMst as um on mm.Month=MONTH(ExpenseDate) and mm.Year=Year(ExpenseDate)
group by mm.Month,mm.Year,mm.MonthYear,Id
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_BV_DETAILS_DASHBOARD]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_BV_DETAILS_DASHBOARD]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ADMIN_BV_DETAILS_DASHBOARD]
AS
BEGIN
	Declare @FromDate datetime
	Declare @ToDate datetime
	Declare @MonthFromDate datetime
	Declare @MonthToDate datetime
	Select @FromDate=StartDate,@ToDate=EndDate from dbo.fn_GetFirstLastDateOfWeekByDate(dbo.fn_GetServerDate())
	Select @MonthFromDate=StartDate,@MonthToDate=EndDate from dbo.fn_GetFirstLastDateOfMonthByDate(dbo.fn_GetServerDate())
		
	SELECT	ISNULL(SUM(case when uph.OrderDate>=@FromDate and uph.OrderDate<=@ToDate then ISNULL(DirectPurchaseVolume,0) end),0)+
			ISNULL(SUM(case when uph.OrderDate>=@FromDate and uph.OrderDate<=@ToDate then ISNULL(RepurchaseMatchingVolume,0) end),0) as WeekBV,
			ISNULL(SUM(ISNULL(DirectPurchaseVolume,0)+ISNULL(RepurchaseMatchingVolume,0)),0) as MonthBV,
			ISNULL(SUM(case when uph.OrderDate>=@FromDate and uph.OrderDate<=@ToDate then ISNULL(DirectPurchaseVolume,0) end),0) as WeekDirectBV,
			ISNULL(SUM(case when uph.OrderDate>=@FromDate and uph.OrderDate<=@ToDate then ISNULL(RepurchaseMatchingVolume,0) end),0) as WeekRepurchaseBV
	FROM UserPurchaseHdr as uph
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	Where cast(OrderDate as DATE)>=@MonthFromDate and  cast(OrderDate as DATE)<=@MonthToDate
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_EXPENSE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_EXPENSE_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_ALL_EXPENSE_DETAILS]
(
@ExpenseId numeric(18,0)=0
)
AS
BEGIN
	Select em.*,etm.ExpenseTypeName,
			epm.ExpensePaidBy as ExpensePaidBy,eptm.ExpensePaidTo as ExpensePaidTo
	from ExpenseMst as em
	INNER JOIN ExpenseTypeMst as etm on em.ExpenseTypeId=etm.ExpenseTypeId
	INNER JOIN ExpensePaidByMst as epm on em.PaidBy=epm.ExpensePaidById
	LEFT JOIN ExpensePaidToMst as eptm on em.ExpensePaidToId=eptm.ExpensePaidToId
	Where em.ExpenseId=case when @ExpenseId=0 then em.ExpenseId else @ExpenseId end
		AND em.IsActive=1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT]
(
@BusinessToolsId int=0
)
AS
BEGIN
	Select pm.*,pcm.CategoryName from BusinessToolsMst as pm
	INNER JOIN BusinessToolsCategoryMst as pcm on pm.BusinessToolsCategoryId=pcm.BusinessToolsCategoryId
	Where BusinessToolsId=case when @BusinessToolsId=0 then BusinessToolsId else @BusinessToolsId end
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_WALLET_DEBIT_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_WALLET_DEBIT_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_ADMIN_WALLET_DEBIT_REPORT]
(
@UserId int=0
)
AS
BEGIN
	Select wd.*,um.RefferalCode,um.FirstName,um.LastName,um.MiddleName,um.FullName,um.RegistrationDate,
			BankName,BankAccNo, CONVERT(varchar(11),DebitDate,103) as PayoutDate,
			um.MobileNo,um.EmailId,PANNo
	from WalletDebitDtl as wd
	INNER JOIN UserMst as um on wd.UserId=um.UserId
	order by DebitDate desc
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_VOLUME_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_VOLUME_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ADMIN_VOLUME_DETAILS]
( 
@FromDate datetime,
@ToDate datetime
)
AS
BEGIN
	--Declare @FromDate datetime=''2019-09-01''
	--Declare @ToDate datetime=''2019-09-15''

	Declare @TotalTDS float,@TotalAdminCharge float,
			@Profite float,@PendingPayout float,@NetProfite float,@TotalVolume float,
			@TotalDirectVolume float,@TotalRepurchaseVolume float,
			@TotalDirectIncomePay float,@TotalPointsIncomePay float,@TotalVoltIncomePay float,
			@TotalLeaderIncomePay float,@TotalUplineSupportIncomePay float,@TotalSilverIncomePay float

	Select  @TotalVolume=ISNULL(SUM(ISNULL(DirectPurchaseVolume,0)),0)+ISNULL(SUM(ISNULL(RepurchaseMatchingVolume,0)),0),
			@TotalDirectVolume=ISNULL(SUM(ISNULL(DirectPurchaseVolume,0)),0),
			@TotalRepurchaseVolume=ISNULL(SUM(ISNULL(RepurchaseMatchingVolume,0)),0)
	from UserPurchaseHdr as uph
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	Where cast(OrderDate as date)>=cast(@FromDate as date) and cast(OrderDate as date)<=cast(@ToDate as date)

	SET @TotalVolume=ISNULL(@TotalVolume,0)


	Select @TotalTDS=ISNULL(SUM(TDSAmount),0),@TotalAdminCharge=ISNULL(SUM(AdminCharge),0),
		@TotalDirectIncomePay=ISNULL(SUM(DirectIncomePay),0),@TotalPointsIncomePay=ISNULL(SUM(PointsIncomePay),0),@TotalVoltIncomePay=ISNULL(SUM(VoltIncomePay),0),
		@TotalLeaderIncomePay=ISNULL(SUM(LeaderIncomePay),0),@TotalUplineSupportIncomePay=ISNULL(SUM(UplineSupportIncomePay),0),
		@TotalSilverIncomePay=ISNULL(SUM(SilverIncomePay),0)	 
	from WalletDebitDtl
	where cast(DebitDate as date)>=cast(@FromDate as date) and cast(DebitDate as date)<=cast(@ToDate as date)	
	
	Declare @ActualDirectVolume float,@ActualDirectVolumeInRs float,
			@ActualMatchingVolume float,@ActualMatchingVolumeInRs float,
			@ActualLeaderCreateVolume float,@ActualLeaderCreateVolumeInRs float,
			@ActualUplineSupportVolume float,@ActualUplineSupportVolumeInRs float,
			@ActualVoltVolume float,@ActualVoltVolumeInRs float,
			@ActualExpenseVolume float,@ActualExpenseVolumeInRs float,
			@ActualCompanyTurnOverVolume float,@ActualCompanyTurnOverVolumeInRs float,
			@ActualTopRetailerVolume float,@ActualTopRetailerVolumeInRs float,
			@ActualRewardsVolume float,@ActualRewardsVolumeInRs float,
			@ActualBonanzaVolume float,@ActualBonanzaVolumeInRs float,
			@ActualSilverVolume float,@ActualSilverVolumeInRs float
			
	SET @ActualDirectVolume=cast((@TotalVolume*10/100) as numeric(18,0))
	SET @ActualDirectVolumeInRs=@ActualDirectVolume*10
	
	SET @ActualMatchingVolume=cast((@TotalVolume*32/100) as numeric(18,0))
	SET @ActualMatchingVolumeInRs=@ActualMatchingVolume*10
	
	SET @ActualLeaderCreateVolume=cast((@TotalVolume*8/100) as numeric(18,0))
	SET @ActualLeaderCreateVolumeInRs=@ActualLeaderCreateVolume*10
	
	SET @ActualUplineSupportVolume=cast((@TotalVolume*0.8/100) as numeric(18,0))
	SET @ActualUplineSupportVolumeInRs=@ActualUplineSupportVolume*10
	
	SET @ActualSilverVolume=cast((@TotalVolume*0.8/100) as numeric(18,0))
	SET @ActualSilverVolumeInRs=@ActualSilverVolume*10
	
	SET @ActualVoltVolume=cast((@TotalDirectVolume*2/100) as numeric(18,0))
	SET @ActualVoltVolumeInRs=@ActualVoltVolume*10
	
	SET @ActualCompanyTurnOverVolume=cast((@TotalVolume*2/100) as numeric(18,0))
	SET @ActualCompanyTurnOverVolumeInRs=@ActualCompanyTurnOverVolume*10
		
	SET @ActualTopRetailerVolume=cast((@TotalRepurchaseVolume*2/100) as numeric(18,0))
	SET @ActualTopRetailerVolumeInRs=@ActualTopRetailerVolume*10
		
	SET @ActualRewardsVolume=cast((@TotalVolume*2/100) as numeric(18,0))
	SET @ActualRewardsVolumeInRs=@ActualRewardsVolume*10
	
	SET @ActualBonanzaVolume=cast((@TotalVolume*2/100) as numeric(18,0))
	SET @ActualBonanzaVolumeInRs=@ActualBonanzaVolume*10
	
	SET @ActualExpenseVolume=cast((@TotalVolume*20/100) as numeric(18,0))
	SET @ActualExpenseVolumeInRs=@ActualExpenseVolume*10
		
	
	Select	@TotalVolume as TotalBusinessVolume,
			@TotalDirectIncomePay as TotalDirectIncomePay,@TotalPointsIncomePay as TotalMatchingIncomePay,
			@TotalVoltIncomePay as TotalVoltIncomePay,@TotalLeaderIncomePay as TotalLeaderIncomePay,
			@TotalUplineSupportIncomePay as TotalUplineSupportIncomePay,
			@ActualDirectVolume as ActualDirectVolume,
			@ActualDirectVolumeInRs as ActualDirectVolumeInRs,
			@ActualMatchingVolume as ActualMatchingVolume,
			@ActualMatchingVolumeInRs as ActualMatchingVolumeInRs,
			@ActualLeaderCreateVolume as ActualLeaderCreateVolume,
			@ActualLeaderCreateVolumeInRs as ActualLeaderCreateVolumeInRs,
			@ActualUplineSupportVolume as ActualUplineSupportVolume,
			@ActualUplineSupportVolumeInRs as ActualUplineSupportVolumeInRs,
			@ActualVoltVolume as ActualVoltVolume,
			@ActualVoltVolumeInRs as ActualVoltVolumeInRs,
			@ActualExpenseVolume as ActualExpenseVolume,
			@ActualExpenseVolumeInRs as ActualExpenseVolumeInRs,
			@ActualCompanyTurnOverVolume as ActualCompanyTurnOverVolume,
			@ActualCompanyTurnOverVolumeInRs as ActualCompanyTurnOverVolumeInRs,
			@ActualTopRetailerVolume as ActualTopRetailerVolume,
			@ActualTopRetailerVolumeInRs as ActualTopRetailerVolumeInRs,
			@ActualRewardsVolume as ActualRewardsVolume,
			@ActualRewardsVolumeInRs as ActualRewardsVolumeInRs,
			@ActualBonanzaVolume as ActualBonanzaVolume,
			@ActualBonanzaVolumeInRs as ActualBonanzaVolumeInRs,
			@TotalDirectVolume as TotalDirectBV,
			@TotalRepurchaseVolume as TotalRepurchaseBV,
			@ActualSilverVolume as ActualSilverVolume,
			@ActualSilverVolumeInRs as ActualSilverVolumeInRs
			
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_TOTAL_DIRECT_BV]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_TOTAL_DIRECT_BV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_TOTAL_DIRECT_BV](52,default,default)
CREATE Function [dbo].[FN_GET_USER_TOTAL_DIRECT_BV]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
RETURNS @CreditDtl table(CreditDate date, FullName nvarchar(max),RefferalCode nvarchar(max),TotalBV float)
AS
BEGIN

	insert into @CreditDtl
	Select uph.orderDate as CreditDate,um.FullName,um.RefferalCode,SUM(ISNULL(DirectPurchaseVolume,0)) TotalBV
	from		UserMst as um
	INNER JOIN	UserPurchaseHdr as uph on um.UserId=uph.UserId	AND	uph.IsRepurchaseOrder=0
	INNER JOIN	UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	Where RefferalUserId=@UserId
		AND	(@FromDate is null or uph.OrderDate>=@FromDate)
		AND	(@ToDate is null or uph.OrderDate<=@ToDate)
		group by uph.orderDate,um.FullName,um.RefferalCode

	RETURN
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT]
(
@ProductId int
)
AS
BEGIN
	Select pm.*,pcm.CategoryName from ProductMst as pm
	INNER JOIN ProductCategoryMst as pcm on pm.ProductCategoryId=pcm.ProductCategoryId
	Where ProductId=case when @ProductId=0 then ProductId else @ProductId end
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_PARENT_USER_TREE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_PARENT_USER_TREE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ALL_PARENT_USER_TREE]
(
@UserId int,
@IncludeSelf bit=1
)
AS
BEGIN

	SELECT * FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@UserId,@IncludeSelf)

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_MEMBER_LIST]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_MEMBER_LIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_ALL_MEMBER_LIST]
(
@UserId numeric(18,0)
)
AS
BEGIN
--Declare @UserId numeric(18,0)=1

Select	lum.UserId,um.FirstName as Name,um.RefferalCode,lum.IsRegistered,lum.Position,
		rum.FirstName as RefferalName,rum.RefferalCode as RefferalRefferalCode,
		um.RegMonth,um.RegYear
from (
Select *,''R'' as Position from FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,0)
union all 
Select *,''L'' as Position from FN_GET_ALL_LEFT_SIDED_USER_TREE(@UserId,0)
) as lum
INNER JOIN UserMst as um on lum.UserId=um.userId
INNER JOIN UserMst as rum on um.RefferalUserId=rum.userId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_QUERY_SUPPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_QUERY_SUPPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_QUERY_SUPPORT]
(
@UserId int=0
)
AS
BEGIN
	SELECT qsm.*,case when QueryStatus=1 then ''Pending'' when  QueryStatus=2 then ''Resolve'' end as QueryStatusName,
			convert(varchar, qsm.CreatedDate, 103)CreatedOn,ResponseDate as ResponseOn,
			um.RefferalCode,um.FirstName,um.FullName,um.LastName,um.MobileNo
	FROM QuerySupportMst as qsm
	INNER JOIN UserMst as um  on qsm.UserId=um.UserId
	Where qsm.UserId=case when @UserId =0 then qsm.UserId else @UserId end
		AND qsm.IsActive=1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_NOTIFICATION_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_NOTIFICATION_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_NOTIFICATION_DETAILS]
(
@UserId int
)
AS
BEGIN
	SELECT * 
	FROM NOTIFICATIONMST
	WHERE UserId=@UserId and ISNULL(IsRead,0)=0
	Order by NotificationId desc
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_PRODUCT_LIST]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_PRODUCT_LIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_PRODUCT_LIST]
AS
BEGIN
	Select pm.*,pcm.CategoryName
	from ProductMst as pm
	INNER JOIN ProductCategoryMst as pcm on pm.ProductCategoryId=pcm.ProductCategoryId
	where pm.IsActive=1
	Order by pm.ProductCategoryId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_TOP_RETAILER_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_TOP_RETAILER_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_TOP_RETAILER_DETAILS]
(
@Month int,
@Year int
)
AS
BEGIN
	SET FMTONLY OFF
	Select Month(OrderDate) as [Month],Year(OrderDate) as [Year],UserId,SUM(upd.Amount) as Amount,
					SUM(ISNULL(RepurchaseMatchingVolume,0)) as BusinessVolume,
					SUM(ISNULL(RepurchaseMatchingVolume,0)) as SelfBV,cast(0 as float) as RefferalBV
	into #TempBV
	from UserPurchaseHdr as uph
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	Where Month(OrderDate)=case when @Month is null or @Month=0 then Month(OrderDate) else @Month end 
		and Year(OrderDate)=case when @Month is null or @Year=0 then Year(OrderDate) else @Year end 
		AND uph.IsRepurchaseOrder=1
	group by UserId,Month(OrderDate),Year(OrderDate)

	Select * 
	into #FinalBV
	from #TempBV
	Where 1=0

	Declare @UserId int
	DECLARE db_user CURSOR FOR 
	SELECT UserId 
	FROM UserMst
	OPEN db_user  
	FETCH NEXT FROM db_user INTO @UserId

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
			insert into #FinalBV
			Select Month,Year,@UserId,SUM(Amount),SUM(BusinessVolume),SUM(SelfBV)SelfBV,SUM(RefferalBV)RefferalBV from (
				Select Month,Year,UserId,Amount,BusinessVolume,SelfBV,RefferalBV from #TempBV Where UserId=@UserId
				UNION ALL
				Select tb.Month,tb.Year,tb.UserId,tb.Amount,tb.BusinessVolume,0 as SelfBV,tb.BusinessVolume as RefferalBV				
				from #TempBV as tb
				INNER JOIN UserMst as um on tb.UserId=um.UserId
				Where um.RefferalUserId=@UserId
			) as final
			group by Month,Year
			
		  FETCH NEXT FROM db_user INTO @UserId 
	END 

	CLOSE db_user  
	DEALLOCATE db_user 


	--Select  *from #FinalBV
	

	Select retailer.*,ISNULL(TopRetailerDtlId,0)TopRetailerDtlId,ISNULL(PaidAmount,0)PaidAmount,
			convert(varchar,PaidDate,103)PaidDate,ISNULL(IsPaid,0)IsPaid 
	from(
		Select orderdtl.*,ROW_NUMBER() over(partition by Month,Year order by BusinessVolume desc) as RowNo,
			um.FullName,um.FirstName,um.LastName,um.MiddleName,um.MobileNo,um.EmailId,um.RefferalCode,
			DOB,Address,ProfileImage,IsGoldMember,IsSilverMember,RegistrationDate
		from #FinalBV as orderdtl
		INNER JOIN UserMst as um on orderdtl.UserId=um.UserId
	) as retailer
	LEFT JOIN TopRetailerDtl as trd on retailer.UserId=trd.UserId and  retailer.[Month]=trd.[Month] and  retailer.[Year]=trd.[Year]
	--Where RowNo<=10
	Order by retailer.Year desc,retailer.Month desc,RowNo
	
	drop table #TempBV
	drop table #FinalBV

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_TOP_PURCHASE_PRODUCTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_TOP_PURCHASE_PRODUCTS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_TOP_PURCHASE_PRODUCTS]
(
@UserId int
)
AS
BEGIN
	SET NOCOUNT ON;
	Select top 5 * from (
	Select pm.ProductId,pm.ProductName,cast(ISNULL(SUM(Amount),0) as bigint) as TotalAmount from UserPurchaseHdr as uph
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN ProductMst as pm on upd.productId=pm.ProductId
	where UserId=@UserId
	group by  pm.ProductId,pm.ProductName
	) as purchase
	order by TotalAmount desc

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_ORDER_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_ORDER_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_USER_ORDER_DETAILS]
(
@UserId int=0,
@FranchiseeId int=0
)
AS
BEGIN
	IF @FranchiseeId is null
	BEGIN
		SET @FranchiseeId=0
	END	
	--Declare @UserId int=9
	Select uph.UserPurchaseHdrId,uph.OrderNo,uph.TotalOrderAmount,uph.OrderDate,uph.FranchiseeId,
			uph.OrderStatusId,
			--fm.FranchiseeName ,fm.MobileNo as FranchiseeMobileNo,osm.Name as StatusName,
			'''' as FranchiseeName,null as FranchiseeMobileNo,osm.Name as StatusName,
			um.FullName as CustomerName,um.MobileNo as CustomerMobileNo,
			case when len(ltrim(rtrim((ISNULL(uph.DeliveryAddress,'''')))))>0 then uph.DeliveryAddress else um.Address end as CustomerAddress,
			uph.CourierTrackingNo,ISNULL(IsRepurchaseOrder,1)IsRepurchaseOrder,
			ISNULL(BusinessVolume,0)BusinessVolume,um.RefferalCode,CONVERT(varchar(20),uph.OrderDate,103) as OdrDate,
			CASE WHEN ISNULL(IsRepurchaseOrder,1)=1 then ''Repurchase'' ELSE ''Direct'' END as OrderType,
			uph.OrderReceivedById,orbm.OrderReceivedBy,RazorpayOrderId,RazorpayPaymentId,RazorpaySignature,
			updd.Qty,updd.Amount,pm.ProductName
	from UserPurchaseHdr as uph  
	INNER JOIN (
			SELECT UserPurchaseHdrId,SUM(ISNULL(DirectPurchaseVolume,0)+ISNULL(RepurchaseMatchingVolume,0)) as BusinessVolume 
			FROM UserPurchaseDtl
			group by UserPurchaseHdrId
	) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN UserPurchaseDtl as updd on uph.UserPurchaseHdrId=updd.UserPurchaseHdrId
	INNER JOIN ProductMst as pm on updd.ProductId=pm.ProductId
	INNER JOIN UserMst as um on uph.UserId=um.UserId
	LEFT JOIN OrderStatusMst as osm on uph.OrderStatusId=osm.OrderStatusId
	LEFT JOIN FranchiseeMst as fm on uph.FranchiseeId=fm.FranchiseeId
	LEFT JOIN OrderReceivedByMst as orbm on uph.OrderReceivedById=orbm.OrderReceivedById
	where uph.UserId=case when @UserId=0 then uph.UserId else @UserId end 
		and ISNULL(uph.FranchiseeId,0)=case when @FranchiseeId=0 then ISNULL(uph.FranchiseeId,0) else @FranchiseeId end
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_TOTAL_BUSINESS_VOLUME]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_TOTAL_BUSINESS_VOLUME]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_USER_TOTAL_BUSINESS_VOLUME]
(
@UserId int=0
)
AS
BEGIN
	--Declare @UserId int=0

	Select SUM(ISNULL(DirectPurchaseVolume,0)+ISNULL(RepurchaseMatchingVolume,0)) as TotalBV,uph.UserId 
	from UserPurchaseHdr as uph
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	Where uph.UserId=case when @UserId =0 then uph.UserId else @UserId end
	group by uph.UserId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_ORDER_HDR]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_ORDER_HDR]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_USER_ORDER_HDR]
(
@UserId int=0,
@FranchiseeId int=0
)
AS
BEGIN
	IF @FranchiseeId is null
	BEGIN
		SET @FranchiseeId=0
	END	
	--Declare @UserId int=9
	Select uph.UserPurchaseHdrId,uph.OrderNo,uph.TotalOrderAmount,uph.OrderDate,uph.FranchiseeId,
			uph.OrderStatusId,
			--fm.FranchiseeName ,fm.MobileNo as FranchiseeMobileNo,osm.Name as StatusName,
			'''' as FranchiseeName,null as FranchiseeMobileNo,osm.Name as StatusName,
			um.FullName as CustomerName,um.MobileNo as CustomerMobileNo,
			case when len(ltrim(rtrim((ISNULL(uph.DeliveryAddress,'''')))))>0 then uph.DeliveryAddress else um.Address end as CustomerAddress,
			uph.CourierTrackingNo,ISNULL(IsRepurchaseOrder,1)IsRepurchaseOrder,
			ISNULL(BusinessVolume,0)BusinessVolume,um.RefferalCode,CONVERT(varchar(20),uph.OrderDate,103) as OdrDate,
			CASE WHEN ISNULL(IsRepurchaseOrder,1)=1 then ''Repurchase'' ELSE ''Direct'' END as OrderType,
			uph.OrderReceivedById,orbm.OrderReceivedBy,RazorpayOrderId,RazorpayPaymentId,RazorpaySignature
	from UserPurchaseHdr as uph  
	INNER JOIN (
			SELECT UserPurchaseHdrId,SUM(ISNULL(DirectPurchaseVolume,0)+ISNULL(RepurchaseMatchingVolume,0)) as BusinessVolume 
			FROM UserPurchaseDtl
			group by UserPurchaseHdrId
	) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN UserMst as um on uph.UserId=um.UserId
	LEFT JOIN OrderStatusMst as osm on uph.OrderStatusId=osm.OrderStatusId
	LEFT JOIN FranchiseeMst as fm on uph.FranchiseeId=fm.FranchiseeId
	LEFT JOIN OrderReceivedByMst as orbm on uph.OrderReceivedById=orbm.OrderReceivedById
	where uph.UserId=case when @UserId=0 then uph.UserId else @UserId end 
		and ISNULL(uph.FranchiseeId,0)=case when @FranchiseeId=0 then ISNULL(uph.FranchiseeId,0) else @FranchiseeId end
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_ALL_PARENT_USER_NOTIFICATION]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_ALL_PARENT_USER_NOTIFICATION]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INS_ALL_PARENT_USER_NOTIFICATION]
(
@UserId bigint,
@UserPurchaseHdrId bigint,
@NotificationTypeId int,	/* 1:Information		2:Action	*/
@Msg nvarchar(max),
@ActionBy int,
@Url nvarchar(max)=null
)
AS
BEGIN
	Declare @walletType int /* 1:Direct		2:Repurchase	3:FranchiseeReferral  */
	Select @WalletType=case when ISNULL(IsRepurchaseOrder,1)=1 then 2 ELSE 1 END  	
	from UserPurchaseHdr with(nolock)
	Where UserPurchaseHdrId=@UserPurchaseHdrId

	SELECT * 
	into #tmpParents
	FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@UserId,1)
	
	IF @walletType=1	--Direct
	BEGIN
		insert into NotificationMst(NotificationTypeId,UserId,Notification,URL,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT @NotificationTypeId as NotificationTypeId,UserId, @Msg as [Notification],@Url as URL,
				1 as IsActive,@ActionBy as CreatedBy,dbo.fn_GetServerDate() as CreatedDate
				,@ActionBy as ModifiedBy,dbo.fn_GetServerDate() as ModifiedDate		
		FROM #tmpParents
		WHERE UserId<>@UserId
	END
	ELSE
	BEGIN	
		insert into NotificationMst(NotificationTypeId,UserId,Notification,URL,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT @NotificationTypeId as NotificationTypeId,UserId, @Msg as [Notification],@Url as URL,
				1 as IsActive,@ActionBy as CreatedBy,dbo.fn_GetServerDate() as CreatedDate
				,@ActionBy as ModifiedBy,dbo.fn_GetServerDate() as ModifiedDate		
		FROM #tmpParents
	END
	
	DROP TABLE #tmpParents
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_ALL_MILES_AMOUNT_FROM_DATE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_ALL_MILES_AMOUNT_FROM_DATE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INS_ALL_MILES_AMOUNT_FROM_DATE]
(
@FromDate datetime
)
AS
BEGIN
	Declare @MilesAmount float
	SELECT @MilesAmount=Value FROM CONFIGMST Where [Key]=''RefernceMilesAmount''
	
	--truncate table MilesCreditDtl
	
	insert into MilesCreditDtl(UserId,UserIdFrom,CreditDate,CreditAmount,IsActive,CreatedBy,CreatedDate)
	Select um.RefferalUserId as UserId,um.UserId as UserIdFrom,um.RegistrationDate as CreditDate,@MilesAmount as CreditAmount
			,1 as IsActive, um.UserId as CreatedBy,um.CreatedDate as CreatedDate
	from UserMst as um
	LEFT JOIN MilesCreditDtl as mcd on um.RefferalUserId=mcd.UserId and um.UserId=mcd.UserIdFrom
	Where RegistrationDate>=@FromDate
	and mcd.MilesCreditDtlId is null
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_WALLET_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_WALLET_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_USER_WALLET_DETAILS]
(
@UserId bigint,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	Select wcd.WalletCreditDtlId,wcd.CreditDate,wcd.CreditAmount,wcd.CreditType ,uph.OrderNo,uph.TotalOrderAmount,um.FullName,um.RefferalCode
	from WalletCreditDtl  as wcd
	INNER JOIN UserPurchaseHdr as uph on wcd.UserPurchaseHdrId=uph.UserPurchaseHdrId
	INNER JOIN UserMst as um on uph.UserId=um.UserId
	where wcd.UserId=@UserId
		AND (wcd.CreditDate>=@FromDate or @FromDate is null)
		AND (wcd.CreditDate<=@ToDate or @ToDate is null)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_USER_NOTIFICATION]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_USER_NOTIFICATION]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INS_USER_NOTIFICATION]
(
@UserId bigint,
@UserPurchaseHdrId bigint,
@NotificationTypeId int,	/* 1:Information		2:Action	*/
@Msg nvarchar(max),
@ActionBy int,
@Url nvarchar(max)=null
)
AS
BEGIN
	Declare @walletType int /* 1:Direct		2:Repurchase	3:FranchiseeReferral  */
	Select @WalletType=case when ISNULL(IsRepurchaseOrder,1)=1 then 2 ELSE 1 END  	
	from UserPurchaseHdr with(nolock)
	Where UserPurchaseHdrId=@UserPurchaseHdrId

	SELECT * 
	into #tmpParents
	FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@UserId,1)
	
	IF @walletType=1	--Direct
	BEGIN
		insert into NotificationMst(NotificationTypeId,UserId,Notification,URL,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT @NotificationTypeId as NotificationTypeId,UserId, @Msg as [Notification],@Url as URL,
				1 as IsActive,@ActionBy as CreatedBy,dbo.fn_GetServerDate() as CreatedDate
				,@ActionBy as ModifiedBy,dbo.fn_GetServerDate() as ModifiedDate		
		FROM #tmpParents
		WHERE UserId<>@UserId
	END
	ELSE
	BEGIN	
		insert into NotificationMst(NotificationTypeId,UserId,Notification,URL,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT @NotificationTypeId as NotificationTypeId,UserId, @Msg as [Notification],@Url as URL,
				1 as IsActive,@ActionBy as CreatedBy,dbo.fn_GetServerDate() as CreatedDate
				,@ActionBy as ModifiedBy,dbo.fn_GetServerDate() as ModifiedDate		
		FROM #tmpParents
	END
	
	DROP TABLE #tmpParents
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_SILVER_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_SILVER_INCOME_OF_USER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INSERT_SILVER_INCOME_OF_USER]
(
@ActionedBy int,
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @ActionedBy int=1
	--Declare @FromDate datetime
	--Declare @ToDate datetime

	Declare @TempUserId int
	Declare @TempChildUserId int
	Declare @TempSilverMemberDate datetime
	Declare @LastCalculateDate datetime
	Declare @TempFromDate datetime
	Declare @Today datetime=dbo.fn_GetServerDate()
	Declare @TotalLeftPoint numeric(18,0)
	Declare @TotalRightPoint numeric(18,0)	
	Declare @MatchingPoint numeric(18,0)				
	Declare @SilverMemberPerc float


	--EXEC PROC_INSERT_MATCHING_INCOME_OF_USER

	SELECT @LastCalculateDate=CAST(Value as datetime) FROM ConfigMst where [Key]=''LastCalculateDate''

	IF NOT EXISTS(SELECT 1 FROM ConfigMst Where [Key]=''SilverMemberPerc'')
	BEGIN
		SELECT @SilverMemberPerc=Value FROM ConfigMst Where [Key]=''SilverMemberPerc''
	END
	ELSE
	BEGIN
		SET @SilverMemberPerc=10
	END

	If @FromDate is null
	BEGIN
		SET @FromDate=CAST(@Today AS DATE)
	END
	If @ToDate is null
	BEGIN
		SET @ToDate=CAST(@Today AS DATE)
	END
	
	Declare @tblSilverMembers table(UserId int,SilverMemberDate datetime)

	Declare @SilverMemberCnt int=0

Declare @GoldUserId int
DECLARE db_cursorgold CURSOR FOR 
Select UserId from UserMst
Where IsDimondMember=1

OPEN db_cursorgold  
FETCH NEXT FROM db_cursorgold INTO @GoldUserId

WHILE @@FETCH_STATUS = 0  
BEGIN  
	
	Delete from @tblSilverMembers
	
	insert into @tblSilverMembers
	Select um.UserId,SilverMemberDate 
	from UserMst as um
	INNER JOIN (
		Select UserId from FN_GET_ALL_LEFT_SIDED_USER_TREE(@GoldUserId,0)
		union
		Select UserId from FN_GET_ALL_RIGHT_SIDED_USER_TREE(@GoldUserId,0)
	) as childs on um.UserId=childs.UserId
	Where 
		IsSilverMember=1
		--AND um.UserId=case when @UserId=0 then um.UserId else @UserId end
				 
	
	Select * 
	into #MatchingIncome
	from MatchingPointsCreditDtl
	Where UserId=@GoldUserId
	
	
	DECLARE db_cursor CURSOR FOR 
	Select UserId,SilverMemberDate 
	from @tblSilverMembers as um
	Where UserId=case when @UserId=0 then UserId else @UserId end

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @TempUserId ,@TempSilverMemberDate

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		
		IF  @LastCalculateDate is null
		BEGIN
			SET @TempFromDate=cast(@TempSilverMemberDate as date)
		END
		ELSE
		BEGIN
			SET @TempFromDate=DATEADD(d,-1,@LastCalculateDate)
		END
		
		SET @TempFromDate=cast(@TempFromDate as DATE)
		SET @ToDate=cast(@ToDate as DATE)
		
		WHILE @TempFromDate<=@ToDate
		BEGIN
			
			Declare @GoldIncome numeric(18,0)
			SET @GoldIncome=ISNULL(@GoldIncome,0)
			
			SELECT @GoldIncome=SUM(MatchingPoints) 
			from #MatchingIncome as mpcd
			Where UserId=@GoldUserId AND mpcd.CreditDate=@TempFromDate
			SET @GoldIncome=ISNULL(@GoldIncome,0)
			
			Select @SilverMemberCnt=COUNT(distinct UserId) from @tblSilverMembers Where cast(SilverMemberDate as date)<=cast(@TempFromDate as date)
			--Select @SilverMemberCnt,@GoldUserId,@TempFromDate
			IF ISNULL(@SilverMemberCnt,0)>0
			BEGIN
				SET @GoldIncome=cast((@GoldIncome/@SilverMemberCnt) as numeric(18,0))
			END
			ELSE
			BEGIN
				SET @GoldIncome=0			
			END
			
			IF EXISTS(SELECT 1 from DiamondAndSilverPointsCreditDtl Where UserId=@TempUserId AND CreditDate=@TempFromDate and FromCreditUserId=@GoldUserId and IsDiamondPoint=0)
			BEGIN
				UPDATE DiamondAndSilverPointsCreditDtl
				SET CreditPerc=@SilverMemberPerc,
					CreditDate=@TempFromDate,
					CreditPoints=cast((@SilverMemberPerc*@GoldIncome/100) as numeric(18,0)),
					IsActive=1,
					CreatedBy=@ActionedBy,
					CreatedDate=dbo.fn_GetServerDate(),
					IsDiamondPoint=0
				WHERE UserId=@TempUserId AND CreditDate=@TempFromDate and FromCreditUserId=@GoldUserId and IsDiamondPoint=0
			END
			ELSE
			BEGIN
				IF @GoldIncome>0
				BEGIN
					INSERT INTO DiamondAndSilverPointsCreditDtl(UserId
															   ,FromCreditUserId
															   ,CreditPerc
															   ,CreditDate
															   ,CreditPoints
															   ,IsActive
															   ,CreatedBy
															   ,CreatedDate
															   ,IsDiamondPoint)
					SELECT @TempUserId as UserId,
						@GoldUserId as FromCreditUserId,
						@SilverMemberPerc as CreditPerc,
						@TempFromDate as CreditDate,
						cast((@SilverMemberPerc*@GoldIncome/100) as numeric(18,0)) as CreditPoints,
						1 as IsActive,
						@ActionedBy as CreatedBy,
						dbo.fn_GetServerDate() as CreatedDate,
						cast(0 as bit) as IsDiamondPoint
						
					INSERT INTO NotificationMst (NotificationTypeId,UserId,Notification,URL,IsRead,ReadDate,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
					values(1,@TempUserId,''You Got New Silver Income In Your Wallet Check Now'',null,null,null,1,1,dbo.fn_GetServerDate(),1,dbo.fn_GetServerDate())
				END
			END						
			
			SET @TempFromDate=@TempFromDate+1
		END

	FETCH NEXT FROM db_cursor INTO @TempUserId ,@TempSilverMemberDate 
	END
	CLOSE db_cursor  
	DEALLOCATE db_cursor 
	
	Drop Table #MatchingIncome
	
FETCH NEXT FROM db_cursorgold INTO @GoldUserId
END
CLOSE db_cursorgold  
DEALLOCATE db_cursorgold 	
	
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER]
(
@ActionedBy int,
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @ActionedBy int=1
	--Declare @FromDate datetime
	--Declare @ToDate datetime

	Declare @TempUserId int
	Declare @TempChildUserId int
	Declare @TempPrimeMemberDate datetime
	Declare @LastCalculateDate datetime
	Declare @TempFromDate datetime
	Declare @Today datetime=dbo.fn_GetServerDate()
	Declare @TotalLeftPoint numeric(18,0)
	Declare @TotalRightPoint numeric(18,0)	
	Declare @MatchingPoint numeric(18,0)				
	Declare @PrimeMemberPerc float


	--EXEC PROC_INSERT_MATCHING_INCOME_OF_USER

	SELECT @LastCalculateDate=CAST(Value as datetime) FROM ConfigMst where [Key]=''LastCalculateDate''

	IF NOT EXISTS(SELECT 1 FROM ConfigMst Where [Key]=''PrimeMemberPerc'')
	BEGIN
		SELECT @PrimeMemberPerc=Value FROM ConfigMst Where [Key]=''PrimeMemberPerc''
	END
	ELSE
	BEGIN
		SET @PrimeMemberPerc=10
	END

	If @FromDate is null
	BEGIN
		SET @FromDate=CAST(@Today AS DATE)
	END
	If @ToDate is null
	BEGIN
		SET @ToDate=CAST(@Today AS DATE)
	END
	
	Declare @tblPrimeMembers table(UserId int,PrimeMemberDate datetime)

	Declare @PrimeMemberCnt int=0

Declare @PrimeUserId int
Declare @GoldUserId int
DECLARE db_cursorprime CURSOR FOR 
Select UserId,PrimeMemberDate from UserMst
Where IsPrimeMember=1 and ISNULL(IsSilverMember,0)=0

OPEN db_cursorprime  
FETCH NEXT FROM db_cursorprime INTO @PrimeUserId,@TempPrimeMemberDate

WHILE @@FETCH_STATUS = 0  
BEGIN  

--SELECT @PrimeUserId
SET @GoldUserId=null
Select top 1 @GoldUserId=UserId from [dbo].[FN_GET_ALL_PARENT_USER_TREE_BY_LEVEL](@PrimeUserId,0) Where IsGoldMember=1 order by UserLevel

IF ISNULL(@GoldUserId,0)>0
BEGIN
	Delete from @tblPrimeMembers
	
	insert into @tblPrimeMembers
	Select um.UserId,PrimeMemberDate 
	from UserMst as um
	INNER JOIN (
		Select UserId from FN_GET_ALL_LEFT_SIDED_USER_TREE(@GoldUserId,0)
		union
		Select UserId from FN_GET_ALL_RIGHT_SIDED_USER_TREE(@GoldUserId,0)
	) as childs on um.UserId=childs.UserId
	Where 
		--RefferalUserId=@GoldUserId	AND 
		IsPrimeMember=1 AND 
		um.UserId=case when @UserId=0 then um.UserId else @UserId end
	
	IF  @LastCalculateDate is null
	BEGIN
		SET @TempFromDate=cast(@TempPrimeMemberDate as date)
	END
	ELSE
	BEGIN
		SET @TempFromDate=@LastCalculateDate
	END
	
	IF cast(@TempFromDate as date)<cast(@TempPrimeMemberDate as date)
	BEGIN
		SET @TempFromDate=cast(@TempPrimeMemberDate as date)
	END
	
	SET @TempFromDate=cast(@TempFromDate as DATE)
	SET @ToDate=cast(@ToDate as DATE)
	
	WHILE @TempFromDate<=@ToDate
	BEGIN
		
		Declare @GoldIncome numeric(18,0)
		--SET @GoldIncome=ISNULL(@GoldIncome,0)			
		--SELECT @GoldIncome=SUM(CreditPoints) 
		--from LeaderAndUplinePointsCreditDtl as mpcd
		--Where UserId=@GoldUserId AND mpcd.CreditDate=@TempFromDate and IsLeaderPoint=1
		
		SELECT @GoldIncome=SUM(MatchingPoints)
		FROM MatchingPointsCreditDtl
		Where UserId=@GoldUserId AND CreditDate=@TempFromDate
		
		SET @GoldIncome=ISNULL(@GoldIncome,0)
		
		Select @PrimeMemberCnt=COUNT(distinct UserId) from @tblPrimeMembers Where cast(PrimeMemberDate as date)<=cast(@TempFromDate as date)
		--Select @PrimeMemberCnt,@GoldUserId,@TempFromDate
		IF ISNULL(@PrimeMemberCnt,0)>0
		BEGIN
			SET @GoldIncome=cast((@GoldIncome/@PrimeMemberCnt) as numeric(18,0))
		END
		ELSE
		BEGIN
			SET @GoldIncome=0			
		END
		
		IF EXISTS(SELECT 1 from LeaderAndUplinePointsCreditDtl Where UserId=@PrimeUserId AND CreditDate=@TempFromDate and FromCreditUserId=@GoldUserId and IsLeaderPoint=0)
		BEGIN
			UPDATE LeaderAndUplinePointsCreditDtl
			SET CreditPerc=@PrimeMemberPerc,
				CreditDate=@TempFromDate,
				CreditPoints=cast((@PrimeMemberPerc*@GoldIncome/100) as numeric(18,0)),
				IsActive=1,
				CreatedBy=@ActionedBy,
				CreatedDate=dbo.fn_GetServerDate(),
				IsLeaderPoint=0
			WHERE UserId=@PrimeUserId AND CreditDate=@TempFromDate and FromCreditUserId=@GoldUserId and IsLeaderPoint=0
		END
		ELSE
		BEGIN
			IF @GoldIncome>0
			BEGIN
				INSERT INTO LeaderAndUplinePointsCreditDtl(UserId
														   ,FromCreditUserId
														   ,CreditPerc
														   ,CreditDate
														   ,CreditPoints
														   ,IsActive
														   ,CreatedBy
														   ,CreatedDate
														   ,IsLeaderPoint)
				SELECT @PrimeUserId as UserId,
					@GoldUserId as FromCreditUserId,
					@PrimeMemberPerc as CreditPerc,
					@TempFromDate as CreditDate,
					cast((@PrimeMemberPerc*@GoldIncome/100) as numeric(18,0)) as CreditPoints,
					1 as IsActive,
					@ActionedBy as CreatedBy,
					dbo.fn_GetServerDate() as CreatedDate,
					cast(0 as bit) as IsLeaderPoint
					
				INSERT INTO NotificationMst (NotificationTypeId,UserId,Notification,URL,IsRead,ReadDate,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				values(1,@PrimeUserId,''You Got New Upline Support Income In Your Wallet Check Now'',null,null,null,1,1,dbo.fn_GetServerDate(),1,dbo.fn_GetServerDate())
			END
		END						
		
		SET @TempFromDate=@TempFromDate+1
	END
		
END

FETCH NEXT FROM db_cursorprime INTO @PrimeUserId,@TempPrimeMemberDate
END
CLOSE db_cursorprime  
DEALLOCATE db_cursorprime	
	
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_GOLD_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_GOLD_INCOME_OF_USER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INSERT_GOLD_INCOME_OF_USER]
(
@ActionedBy int,
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @ActionedBy int=1
	--Declare @FromDate datetime
	--Declare @ToDate datetime

	Declare @TempUserId int
	Declare @TempChildUserId int
	Declare @TempGoldMemberDate datetime
	Declare @LastCalculateDate datetime
	Declare @TempFromDate datetime
	Declare @Today datetime=dbo.fn_GetServerDate()
	Declare @TotalLeftPoint numeric(18,0)
	Declare @TotalRightPoint numeric(18,0)	
	Declare @MatchingPoint numeric(18,0)				
	Declare @GoldMemberPerc float

	--EXEC PROC_INSERT_MATCHING_INCOME_OF_USER
	
	SELECT @LastCalculateDate=CAST(Value as datetime) FROM ConfigMst where [Key]=''LastCalculateDate''

	IF NOT EXISTS(SELECT 1 FROM ConfigMst Where [Key]=''GoldMemberPerc'')
	BEGIN
		SELECT @GoldMemberPerc=Value FROM ConfigMst Where [Key]=''GoldMemberPerc''
	END
	ELSE
	BEGIN
		SET @GoldMemberPerc=100
	END

	If @FromDate is null
	BEGIN
		SET @FromDate=CAST(@Today AS DATE)
	END
	If @ToDate is null
	BEGIN
		SET @ToDate=CAST(@Today AS DATE)
	END

	DECLARE db_cursor CURSOR FOR 
	Select UserId,GoldMemberDate from UserMst
	Where IsGoldMember=1 AND ISNULL(IsDimondMember,0)=0
		AND UserId=case when @UserId=0 then UserId else @UserId end

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @TempUserId ,@TempGoldMemberDate 

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		
		IF  @LastCalculateDate is null
		BEGIN
			SET @TempFromDate=cast(@TempGoldMemberDate as date)
		END
		ELSE
		BEGIN
			SET @TempFromDate=@LastCalculateDate
		END
		
		SET @TempFromDate=cast(@TempFromDate as DATE)
		SET @ToDate=cast(@ToDate as DATE)
		
		WHILE @TempFromDate<=@ToDate
		BEGIN
			
			UPDATE	lupcd
				SET CreditPerc=@GoldMemberPerc,
					CreditDate=@TempFromDate,
					CreditPoints=cast((@GoldMemberPerc*mpcd.MatchingPoints/100) as numeric(18,0)),
					IsActive=1,
					CreatedBy=@ActionedBy,
					CreatedDate=dbo.fn_GetServerDate(),
					IsLeaderPoint=1
			FROM UserMst as um
			INNER JOIN MatchingPointsCreditDtl as mpcd on um.UserId=mpcd.UserId
			INNER JOIN LeaderAndUplinePointsCreditDtl as lupcd on um.UserId = lupcd.FromCreditUserId 
																and lupcd.CreditDate=@TempFromDate and IsLeaderPoint=1
			Where RefferalUserId=@TempUserId and mpcd.CreditDate=@TempFromDate
			
			IF EXISTS(Select 1 from UserMst as um
						INNER JOIN MatchingPointsCreditDtl as mpcd on um.UserId=mpcd.UserId
						LEFT JOIN LeaderAndUplinePointsCreditDtl as lupcd on mpcd.UserId = lupcd.FromCreditUserId 
																and lupcd.CreditDate=@TempFromDate and IsLeaderPoint=1
						Where  RefferalUserId=@TempUserId and mpcd.CreditDate=@TempFromDate
								AND mpcd.MatchingPoints>0 and lupcd.FromCreditUserId is null)
			BEGIN						
				INSERT INTO NotificationMst(NotificationTypeId,UserId,Notification,URL,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsRead,ReadDate) 
				values(1,@TempUserId,''You Got New Leader Income In Your Wallet Check Now'',null,1,0,dbo.fn_GetServerDate(),0,dbo.fn_GetServerDate(),null,null)
			END
							
			insert into LeaderAndUplinePointsCreditDtl
			SELECT	@TempUserId as UserId,
					um.UserId as FromCreditUserId,
					@GoldMemberPerc as CreditPerc,
					@TempFromDate as CreditDate,
					cast((@GoldMemberPerc*mpcd.MatchingPoints/100) as numeric(18,0)) as CreditPoints,
					1 as IsActive,
					@ActionedBy as CreatedBy,
					dbo.fn_GetServerDate() as CreatedDate,
					1 as IsLeaderPoint
			FROM UserMst as um
			INNER JOIN MatchingPointsCreditDtl as mpcd on um.UserId=mpcd.UserId
			LEFT JOIN LeaderAndUplinePointsCreditDtl as lupcd on um.UserId = lupcd.FromCreditUserId 
																and lupcd.CreditDate=@TempFromDate and IsLeaderPoint=1
			Where RefferalUserId=@TempUserId and mpcd.CreditDate=@TempFromDate
					AND mpcd.MatchingPoints>0 and lupcd.FromCreditUserId is null
			
								
			SET @TempFromDate=@TempFromDate+1
		END

	FETCH NEXT FROM db_cursor INTO @TempUserId ,@TempGoldMemberDate 
	END
	CLOSE db_cursor  
	DEALLOCATE db_cursor 
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_DIAMOND_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_DIAMOND_INCOME_OF_USER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INSERT_DIAMOND_INCOME_OF_USER]
(
@ActionedBy int,
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @ActionedBy int=1
	--Declare @FromDate datetime
	--Declare @ToDate datetime

	Declare @TempUserId int
	Declare @TempChildUserId int
	Declare @TempDimondMemberDate datetime
	Declare @LastCalculateDate datetime
	Declare @TempFromDate datetime
	Declare @Today datetime=dbo.fn_GetServerDate()
	Declare @TotalLeftPoint numeric(18,0)
	Declare @TotalRightPoint numeric(18,0)	
	Declare @MatchingPoint numeric(18,0)				
	Declare @DiamondMemberPerc float

	--EXEC PROC_INSERT_MATCHING_INCOME_OF_USER
	
	--SELECT @LastCalculateDate=CAST(Value as datetime) FROM ConfigMst where [Key]=''LastCalculateDate''

	IF NOT EXISTS(SELECT 1 FROM ConfigMst Where [Key]=''DiamondMemberPerc'')
	BEGIN
		SELECT @DiamondMemberPerc=Value FROM ConfigMst Where [Key]=''DiamondMemberPerc''
	END
	ELSE
	BEGIN
		SET @DiamondMemberPerc=100
	END

	If @FromDate is null
	BEGIN
		SET @FromDate=CAST(@Today AS DATE)
	END
	If @ToDate is null
	BEGIN
		SET @ToDate=CAST(@Today AS DATE)
	END

	DECLARE db_cursor CURSOR FOR 
	Select UserId,DimondMemberDate from UserMst
	Where IsDimondMember=1
		AND UserId=case when @UserId=0 then UserId else @UserId end

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @TempUserId ,@TempDimondMemberDate 

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		
		IF  @LastCalculateDate is null
		BEGIN
			SET @TempFromDate=cast(@TempDimondMemberDate as date)
		END
		ELSE
		BEGIN
			SET @TempFromDate=@LastCalculateDate
		END
		
		SET @TempFromDate=cast(@TempFromDate as DATE)
		SET @ToDate=cast(@ToDate as DATE)
		
		WHILE @TempFromDate<=@ToDate
		BEGIN
			
			UPDATE	lupcd
				SET CreditPerc=@DiamondMemberPerc,
					CreditDate=@TempFromDate,
					CreditPoints=cast((@DiamondMemberPerc*mpcd.MatchingPoints/100) as numeric(18,0)),
					IsActive=1,
					CreatedBy=@ActionedBy,
					CreatedDate=dbo.fn_GetServerDate(),
					IsDiamondPoint=1
			FROM UserMst as um
			INNER JOIN MatchingPointsCreditDtl as mpcd on um.UserId=mpcd.UserId
			INNER JOIN DiamondAndSilverPointsCreditDtl as lupcd on um.UserId = lupcd.FromCreditUserId 
																and lupcd.CreditDate=@TempFromDate and IsDiamondPoint=1
			Where RefferalUserId=@TempUserId and mpcd.CreditDate=@TempFromDate
			
			IF EXISTS(Select 1 from UserMst as um
						INNER JOIN MatchingPointsCreditDtl as mpcd on um.UserId=mpcd.UserId
						LEFT JOIN DiamondAndSilverPointsCreditDtl as lupcd on mpcd.UserId = lupcd.FromCreditUserId 
																and lupcd.CreditDate=@TempFromDate and IsDiamondPoint=1
						Where  RefferalUserId=@TempUserId and mpcd.CreditDate=@TempFromDate
								AND mpcd.MatchingPoints>0 and lupcd.FromCreditUserId is null)
			BEGIN						
				INSERT INTO NotificationMst(NotificationTypeId,UserId,Notification,URL,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsRead,ReadDate) 
				values(1,@TempUserId,''You Got New Diamond Income In Your Wallet Check Now'',null,1,0,dbo.fn_GetServerDate(),0,dbo.fn_GetServerDate(),null,null)
			END
							
			insert into DiamondAndSilverPointsCreditDtl
			SELECT	@TempUserId as UserId,
					um.UserId as FromCreditUserId,
					@DiamondMemberPerc as CreditPerc,
					@TempFromDate as CreditDate,
					cast((@DiamondMemberPerc*mpcd.MatchingPoints/100) as numeric(18,0)) as CreditPoints,
					1 as IsActive,
					@ActionedBy as CreatedBy,
					dbo.fn_GetServerDate() as CreatedDate,
					1 as IsDiamondPoint
			FROM UserMst as um
			INNER JOIN MatchingPointsCreditDtl as mpcd on um.UserId=mpcd.UserId
			LEFT JOIN DiamondAndSilverPointsCreditDtl as lupcd on um.UserId = lupcd.FromCreditUserId 
																and lupcd.CreditDate=@TempFromDate and IsDiamondPoint=1
			Where RefferalUserId=@TempUserId and mpcd.CreditDate=@TempFromDate
					AND mpcd.MatchingPoints>0 and lupcd.FromCreditUserId is null
			
								
			SET @TempFromDate=@TempFromDate+1
		END

	FETCH NEXT FROM db_cursor INTO @TempUserId ,@TempDimondMemberDate 
	END
	CLOSE db_cursor  
	DEALLOCATE db_cursor 
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'--exec PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT ''2019-09-20'',''2020-02-26''
--exec PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT ''2019-09-20'',''2019-12-26''
CREATE Procedure [dbo].[PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT]
(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	--Declare @FromDate date=''2019-09-20''
	--Declare @ToDate date=''2020-12-26''
	--Declare @UserId int=0
	
	Select * from (
	Select um.UserId,um.UserName,um.RefferalCode,um.FullName,
			COUNT(case when (cast(cscd.CreditDate as date)>=@FromDate or @FromDate IS null) and  (cast(cscd.CreditDate as date)<=@ToDate or @ToDate IS null) then CompanyShareCreditDtlId end) as NoOfVolts,
			COUNT(CompanyShareCreditDtlId) as TotalNoOfVolts,
			SUM(case when (cast(cscd.CreditDate as date)>=@FromDate or @FromDate IS null) and  (cast(cscd.CreditDate as date)<=@ToDate or @ToDate IS null) then CreditAmt end) as VoltIncome,
			SUM(CreditAmt) as TotalVoltIncome
	from UserMst as um
	INNER JOIN CompanyShareCreditDtl as cscd on um.UserId=cscd.UserId
	group by um.UserId,um.UserName,um.RefferalCode,um.FullName
	) as volt
	order by NoOfVolts desc
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN]
(
@FromCreditUserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN

	SELECT um.UserId,um.RefferalCode,um.FullName,CreditPoints as CreditPoints, CONVERT(varchar(26),CreditDate,105) strCreditDate,CreditDate,IncomeType
	FROM (
		SELECT distinct UserId,SUM(CreditPoints) as CreditPoints,CreditDate,''Prime'' as IncomeType
		FROM LeaderAndUplinePointsCreditDtl as lupc
		Where IsLeaderPoint=0
			and FromCreditUserId=@FromCreditUserId
			and ((cast(lupc.CreditDate as date)>=@FromDate) or (@FromDate is null))
			and ((cast(lupc.CreditDate as date)<=@ToDate) or (@ToDate is null))
		group by UserId,CreditDate

		union all

		SELECT distinct UserId,SUM(CreditPoints) as CreditPoints,CreditDate,''Silver'' as IncomeType
		FROM DiamondAndSilverPointsCreditDtl as lupc
		Where IsDiamondPoint=0
			and FromCreditUserId=@FromCreditUserId
			and ((cast(lupc.CreditDate as date)>=@FromDate) or (@FromDate is null))
			and ((cast(lupc.CreditDate as date)<=@ToDate) or (@ToDate is null))
		group by UserId,CreditDate
	) as p
	INNER JOIN UserMst as um on p.UserId=um.UserId
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @UserId int=14
	--Declare @FromDate datetime
	--Declare @ToDate datetime
	Select Row_Number() over(order by CreditDate desc) as SrNo,CreditDate,um.RefferalCode,um.FullName,uph.TotalOrderAmount,wcd.CreditAmount
	from WalletCreditDtl as wcd
	INNER JOIN UserPurchaseHdr as uph on wcd.UserPurchaseHdrId=uph.UserPurchaseHdrId
								AND (wcd.CreditDate>=@FromDate or @FromDate is null) and (wcd.CreditDate<=@ToDate or @ToDate is null)
	INNER JOIN UserMst as um on uph.UserId=um.UserId
	Where wcd.UserId=@UserId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_COMPANY_SHARE_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_COMPANY_SHARE_AMOUNT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/* 
exec PROC_RPT_GET_COMPANY_SHARE_AMOUNT 0
exec PROC_RPT_GET_COMPANY_SHARE_AMOUNT 54 
*/
CREATE Procedure [dbo].[PROC_RPT_GET_COMPANY_SHARE_AMOUNT]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @UserId int=14
	--Declare @FromDate datetime
	--Declare @ToDate datetime
	Select Row_Number() over(order by cast(CreditDate as date) desc) as SrNo,cast(CreditDate as date) CreditDate,
				um.RefferalCode,um.FullName,uph.TotalOrderAmount,wcd.CreditAmt as ShareAmount,
				convert(varchar,cast(CreditDate as date),103) as VoltIncomeDate,
				wcd.UserId,wum.RefferalCode as UserCode,wum.FullName as UserName,
				CompanyShareCreditDtlId
	from CompanyShareCreditDtl as wcd
	INNER JOIN UserPurchaseHdr as uph on wcd.UserPurchaseHdrId=uph.UserPurchaseHdrId
								AND (cast(wcd.CreditDate as DATE)>=@FromDate or @FromDate is null) 
								and (cast(wcd.CreditDate as DATE)<=@ToDate or @ToDate is null)
	INNER JOIN UserMst as um on uph.UserId=um.UserId
	INNER JOIN UserMst as wum on wcd.UserId=wum.UserId
	Where wcd.UserId=case when @UserId=0 then wcd.UserId else @UserId end
	
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_USER_MILES_LEDGER]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_USER_MILES_LEDGER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_RPT_GET_USER_MILES_LEDGER]
(
@UserId int=0
)
AS
BEGIN
	
	;WITH cte
	AS
	(
	   SELECT UserId,UserIdFrom,TransactionDate,UserPurchaseHdrId,CreatedDate,CreditAmount,DebitAmount, SUM(CreditAmount) CreditAmountTotal, SUM(DebitAmount) DebitAmountTotal
	   FROM (
				SELECT UserId,UserIdFrom,CreditDate as TransactionDate,CreditAmount as CreditAmount,0 as DebitAmount,CreatedDate,0 as UserPurchaseHdrId 
				FROM MilesCreditDtl Where UserId=case when ISNULL(@UserId,0)=0 then UserId ELSE @UserId END

				union

				SELECT UserId,0 as UserIdFrom,DebitDate as TransactionDate,0 as CreditAmount,DebitAmount as DebitAmount,CreatedDate,UserPurchaseHdrId 
				FROM MilesDebitDtl Where UserId=case when ISNULL(@UserId,0)=0 then UserId ELSE @UserId END
			
	   )as Miles 
	   gROUP BY UserId,UserIdFrom,TransactionDate,UserPurchaseHdrId,CreatedDate,CreditAmount,DebitAmount
	  
	), cteRanked AS
	(
	   SELECT CreditAmountTotal,DebitAmountTotal, UserId,UserIdFrom,TransactionDate,UserPurchaseHdrId,CreatedDate,CreditAmount,DebitAmount, ROW_NUMBER() OVER(Partition by UserId ORDER BY UserId,CreatedDate) rownum
	   FROM cte
	) 
	SELECT  c1.UserId,UserIdFrom,TransactionDate,c1.UserPurchaseHdrId,c1.CreatedDate,CreditAmount,DebitAmount,
			(SELECT SUM(CreditAmountTotal) - SUM(DebitAmountTotal) 
			 FROM cteRanked c2 
			 WHERE c2.rownum <= c1.rownum and c2.UserId=c1.UserId) AS BalanceAmount,
			 RefferalCode,FullName,OrderNo,
			 ROW_NUMBER() over(Partition by c1.UserId order by c1.CreatedDate) as RowNo
	FROM cteRanked c1
	LEFT JOIN (SELECT UserId,RefferalCode,FullName FROM UserMst) as um on c1.UserIdFrom=um.UserId
	LEFT JOIN (SELECT UserPurchaseHdrId,OrderNo FROM UserPurchaseHdr) as uph on c1.UserPurchaseHdrId=uph.UserPurchaseHdrId
	
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_USER_MILES_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_USER_MILES_FOR_ADMIN]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_RPT_GET_USER_MILES_FOR_ADMIN]
(
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN

	SELECT MilesCr.UserId,RefferalCode,FullName,MobileNo,MilesCr.CreditAmount,MilesCr.TotalCreditAmount,
		ISNULL(MilesDr.DebitAmount,0)DebitAmount,ISNULL(MilesDr.TotalDebitAmount,0)TotalDebitAmount, 
		ISNULL(MilesCr.TotalCreditAmount,0)-ISNULL(MilesDr.TotalDebitAmount,0) as FinalBalanceAmount
	FROM (
		SELECT UserId,cast(SUM(CreditAmount) as numeric(18,0)) as TotalCreditAmount,
				cast(SUM(case when (@FromDate is null or CreditDate>=@FromDate) and (@ToDate is null or CreditDate<=@ToDate) 
							then CreditAmount 
							ELSE 0 END) as numeric(18,0)) as CreditAmount
		FROM MilesCreditDtl 
		Where UserId=case when ISNULL(@UserId,0)=0 then UserId ELSE @UserId END
		GROUP BY UserId
	) as MilesCr
	INNER JOIN (SELECT UserId,RefferalCode,FullName,MobileNo 
				FROM UserMst) as um on MilesCr.UserId=um.UserId
	LEFT JOIN (
		SELECT UserId,cast(SUM(DebitAmount) as numeric(18,0)) as TotalDebitAmount,
				cast(SUM(case when (@FromDate is null or DebitDate>=@FromDate) and (@ToDate is null or DebitDate<=@ToDate) 
							then DebitAmount 
							ELSE 0 END) as numeric(18,0)) as DebitAmount
		FROM MilesDebitDtl 
		Where UserId=case when ISNULL(@UserId,0)=0 then UserId ELSE @UserId END
		GROUP BY UserId
	) MilesDr on MilesCr.UserId=MilesDr.UserId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_USER_MILES_DETAILS_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_USER_MILES_DETAILS_FOR_ADMIN]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_RPT_GET_USER_MILES_DETAILS_FOR_ADMIN]
(
@Type int, /*1:Credit		2:Debit*/
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN

	IF @Type=1
	BEGIN
		SELECT FullName as CreditDebitDetails,cast(CreditAmount as numeric(18,0)) as Amount,
				Convert(nvarchar(26),CreditDate,103) as TransactionDate,mcd.CreatedDate
		FROM MilesCreditDtl as mcd
		INNER JOIN(SELECT UserId,RefferalCode+''-''+FullName as FullName FROM UserMst) as um on mcd.UserIdFrom=um.UserId
		Where mcd.UserId=case when ISNULL(@UserId,0)=0 then mcd.UserId ELSE @UserId END
			AND (@FromDate is null or CreditDate>=@FromDate) 
			AND (@ToDate is null or CreditDate<=@ToDate)
		order by mcd.CreatedDate
	END
	ELSE IF @Type=2
	BEGIN
		SELECT cast(OrderNo as nvarchar(max)) as CreditDebitDetails,cast(DebitAmount as numeric(18,0)) as Amount,
				Convert(nvarchar(26),DebitDate,103) as TransactionDate,mdd.CreatedDate
		FROM MilesDebitDtl mdd
		INNER JOIN UserPurchaseHdr as uph on mdd.UserPurchaseHdrId=uph.UserPurchaseHdrId
		Where mdd.UserId=case when ISNULL(@UserId,0)=0 then mdd.UserId ELSE @UserId END
			AND (@FromDate is null or DebitDate>=@FromDate) 
			AND (@ToDate is null or DebitDate<=@ToDate)	
		order by mdd.CreatedDate
	END

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @UserId int=14
	--Declare @FromDate datetime
	--Declare @ToDate datetime
	Select Row_Number() over(order by CreditDate desc) as SrNo,lupcd.*,um.FullName,um.RefferalCode
	from dbo.[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](0,@UserId) as lupcd
	INNER JOIN UserMst as um on lupcd.UserId=um.UserId
	Where (lupcd.CreditDate>=@FromDate or @FromDate is null) and (lupcd.CreditDate<=@ToDate or @ToDate is null)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_SILVER_INCOME_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_SILVER_INCOME_AMOUNT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[PROC_RPT_GET_SILVER_INCOME_AMOUNT]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @UserId int=14
	--Declare @FromDate datetime
	--Declare @ToDate datetime
	Select Row_Number() over(order by CreditDate desc) as SrNo,lupcd.*
	from dbo.[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](0,@UserId) as lupcd
	Where (lupcd.CreditDate>=@FromDate or @FromDate is null) and (lupcd.CreditDate<=@ToDate or @ToDate is null)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
--Declare @UserId int=15,
--@FromDate datetime=null,
--@ToDate datetime=null

	--Declare @RightUsers table(UserId int, LeftUserId int, FullName nvarchar(max),UserLevel int)
	--insert into @RightUsers
	--exec PROC_GET_ALL_RIGHT_USER_TREE @UserId,1

	Select SUM(CreditPoints)CreditPoints,cast(pcd.CreditDate as date)CreditDate,um.RefferalCode,
			um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount as OrderAmount,
			uph.UserPurchaseHdrId,
			SUM(ISNULL(ISNULL(DirectPurchaseVolume,DirectMatchingVolume),0)+ISNULL(RepurchaseMatchingVolume,0))RightBV
	from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,1) as lu
	INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId
								AND (uph.OrderDate>=@FromDate or @FromDate is null)
								AND (uph.OrderDate<=@ToDate or @ToDate is null)
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on uph.UserId=um.UserId and ((pcd.UserId=pcd.CreatedBy and um.Position=''L'') or (pcd.UserId<>pcd.CreatedBy))
	Where pcd.UserId=@UserId
	group by cast(pcd.CreditDate as date),um.RefferalCode,um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount,uph.UserPurchaseHdrId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN]
(
@UserId int,
@IsLeaderIncome bit,
@FromDate date=null,
@ToDate date=null
)
AS
BEGIN

	IF @IsLeaderIncome=1
	BEGIN
		Select	CreditDate,SUM(CreditPoints) as CreditPoints,FromCreditRefferalCode,FromCreditUserName, CONVERT(varchar(26),CreditDate,105) strCreditDate
		from dbo.[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](1,@UserId) as lupcd	
		Where (@FromDate is null or CreditDate>=@FromDate)
			AND (@ToDate is null or CreditDate<=@ToDate)
		group by CreditDate,FromCreditRefferalCode,FromCreditUserName
	END
	ELSE
	BEGIN
		Select CreditDate,SUM(CreditPoints) as CreditPoints,FromCreditRefferalCode,FromCreditUserName, CONVERT(varchar(26),CreditDate,105) strCreditDate
		from dbo.[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS](1,@UserId,@FromDate,@ToDate) as lupcd
		group by CreditDate,FromCreditRefferalCode,FromCreditUserName
	END
	
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_GOLD_INCOME_AMOUNT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_GOLD_INCOME_AMOUNT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_RPT_GET_GOLD_INCOME_AMOUNT]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	--Declare @UserId int=14
	--Declare @FromDate datetime
	--Declare @ToDate datetime
	Select Row_Number() over(order by CreditDate desc) as SrNo,lupcd.*,um.FullName,um.RefferalCode
	from dbo.[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](1,@UserId) as lupcd
	INNER JOIN UserMst as um on lupcd.UserId=um.UserId
	Where (lupcd.CreditDate>=@FromDate or @FromDate is null) and (lupcd.CreditDate<=@ToDate or @ToDate is null)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_PROFILE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_PROFILE_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_USER_PROFILE_DETAILS]
(
@UserId bigint
)
AS
BEGIN
--Declare @UserId bigint=1

Declare @TotalMembers int
Select @TotalMembers=Count(UserId) from UserMst where RefferalUserId=@UserId and IsActive=1

--Declare @TotalOrders int,@TotalOrderAmount float
--Select @TotalOrders=Count(UserPurchaseHdrId),@TotalOrderAmount=SUM(TotalOrderAmount) from UserPurchaseHdr where UserId=@UserId and IsActive=1

--Declare @CreditAmount float
--Select @CreditAmount=SUM(CreditAmount) from WalletCreditDtl where UserId=@UserId and IsActive=1

Select um.UserId,um.RefferalCode,um.FirstName,um.MiddleName,um.LastName,um.FullName,um.UserName,um.EmailId,um.MobileNo,
		um.DOB,um.Address,um.CityId,um.PANNo,um.AdharNo,um.BankName,um.BankAccNo,um.BranchName,um.ISFCCode,
		um.ProfileImage,um.RegistrationDate,cm.CityName,rum.FullName as ReferralName,
		um.Gender,um.AlternativeMobileNo,um.PinCode,um.GSTNo,um.TradeName,
		@TotalMembers as TotalMembers,um.KYCImage,um.NomineeName,ISNULL(um.BankingName,'''')BankingName
		--,@TotalOrders as TotalOrders,@TotalOrderAmount as TotalOrderAmount,@CreditAmount as WalletAmount
from UserMst as um
LEFT JOIN CityMst as cm on um.CityId=cm.CityId
LEFT JOIN UserMst as rum on um.RefferalUserId=rum.RefferalUserId
where um.UserId=@UserId
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_LEFT_SIDE_EARNING]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_LEFT_SIDE_EARNING]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_RPT_GET_LEFT_SIDE_EARNING]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
--Declare @UserId int=15,
--@FromDate datetime=null,
--@ToDate datetime=null

	--Declare @LeftUsers table(UserId int, LeftUserId int, FullName nvarchar(max),UserLevel int)
	--insert into @LeftUsers
	--exec PROC_GET_ALL_LEFT_USER_TREE @UserId,1

	Select SUM(CreditPoints)CreditPoints,cast(pcd.CreditDate as date)CreditDate,um.RefferalCode,
			um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount as OrderAmount,
			uph.UserPurchaseHdrId,
			SUM(ISNULL(ISNULL(DirectPurchaseVolume,DirectMatchingVolume),0)+ISNULL(RepurchaseMatchingVolume,0))LeftBV
	from dbo.FN_GET_ALL_LEFT_SIDED_USER_TREE(@UserId,1) as lu
	INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId
								AND (uph.OrderDate>=@FromDate or @FromDate is null)
								AND (uph.OrderDate<=@ToDate or @ToDate is null)
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on uph.UserId=um.UserId and ((pcd.UserId=pcd.CreatedBy and um.Position=''R'') or (pcd.UserId<>pcd.CreatedBy))
	Where pcd.UserId=@UserId
	group by cast(pcd.CreditDate as date),um.RefferalCode,um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount,uph.UserPurchaseHdrId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT]
(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	--Declare @FromDate date=''2019-09-20''
	--Declare @ToDate date=''2020-12-26''
	--Declare @UserId int=0

	Select um.UserId,um.UserName,um.RefferalCode,um.FullName,
			ISNULL(COUNT(case when (cast(rum.ActivateDate as date) >= @FromDate or @FromDate is null) and (cast(rum.ActivateDate as date)<=@ToDate or @ToDate is null) then rum.UserId end),0)  as NoOfDirect,
			ISNULL(COUNT(rum.UserId),0)  as TotalNoOfDirect,
			ISNULL(dbv.DirectBV,0)DirectBV,
			ISNULL(dbv.TotalDirectBV,0)TotalDirectBV,
			ISNULL(DirectAmount,0)DirectAmount,
			ISNULL(TotalDirectIncome,0)TotalDirectIncome
	from UserMst as um
	LEFT JOIN UserMst rum on um.UserId=rum.RefferalUserId
	LEFT JOIN (
		Select pcd.UserId,
				SUM(case when (cast(pcd.CreditDate as date) >= @FromDate or @FromDate is null)  and  (cast(pcd.CreditDate as date) <= @ToDate or @ToDate is null) then DirectPurchaseVolume end) as DirectBV ,
				SUM(DirectPurchaseVolume) as TotalDirectBV 
		from PointsCreditDtl as pcd
		INNER JOIN UserPurchaseDtl as upd on pcd.UserPurchaseDtlId=upd.UserPurchaseDtlId
		INNER JOIN UserPurchaseHdr as uph on upd.UserPurchaseHdrId=uph.UserPurchaseHdrId
		Where IsRepurchaseOrder=0
		group by pcd.UserId
	) as dbv on um.UserId=dbv.UserId
	LEFT JOIN(
	Select wcd.UserId,
			SUM(case when (cast(wcd.CreditDate as date) >= @FromDate or @FromDate is null) and (cast(wcd.CreditDate as date)<=@ToDate or @ToDate is null) then CreditAmount end) as DirectAmount,
			SUM(CreditAmount) as TotalDirectIncome 
	from WalletCreditDtl as wcd
	group by wcd.UserId
	) as dincome on um.UserId=dincome.UserId
	Where rum.IsRegistrationActivated=1  --and cast(rum.ActivateDate as date) between @FromDate and @ToDate
	And um.UserId=case when @UserId=0 then um.UserId else @UserId end
	group by um.UserId,um.UserName,um.RefferalCode,um.FullName,ISNULL(dbv.DirectBV,0),ISNULL(dbv.TotalDirectBV,0),
			ISNULL(DirectAmount,0),ISNULL(TotalDirectIncome,0)
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS]
(
@UserId numeric(18,0)=0,
@IsUptoToday bit=1
)
AS
BEGIN

--SELECT *
--FROM	dbo.FN_ADMIN_PENDING_PAYOUT_DETAILS(@UserId)
--order by NetPayable desc
SET FMTONLY OFF

Declare @ToDate date=getdate()
IF @IsUptoToday=0
BEGIN
	Select @ToDate=EndDate from dbo.fn_GetFirstLastDateOfWeekByDate(DATEADD(d,-7,GETDATE()))
END

Declare @PaymentDtl table(UserId bigint,RefferalCode nvarchar(20),FullName nvarchar(max),PANNo nvarchar(50),BankAccNo nvarchar(50),ISFCCode nvarchar(50)
							,DirectIncome numeric(18,0),RePurchaseIncome numeric(18,0),VoltIncome numeric(18,0)
							,TotalPointsIncome numeric(18,0),TotalPayout numeric(18,0),TotalIncome numeric(18,0)
							,DirectIncomePay numeric(18,0),PointsIncomePay numeric(18,0)
							,TDSPerc numeric(18,2),TDS numeric(18,0),AdminChargePerc numeric(18,2),AdminCharge numeric(18,0)
							,NetPayable numeric(18,0),VoltIncomePay numeric(18,0),LeaderIncome numeric(18,0)
							,UplineSupportIncome numeric(18,0),SilverIncome numeric(18,0),DiamondIncome numeric(18,0),LeaderPointsPay numeric(18,0)
							,UplineSupportPointsPay numeric(18,0),SilverPointsPay numeric(18,0),DiamondPointsPay numeric(18,0),IsRegistrationActivated bit,
							MobileNo numeric(18,0),EmailId nvarchar(max))
	
	--insert into 	@PaymentDtl		(UserId,RefferalCode,FullName,PANNo,BankAccNo,ISFCCode,
	--					DirectIncome,RePurchaseIncome,VoltIncome,
	--						TotalPointsIncome,TotalPayout,DirectIncomePay,PointsIncomePay,VoltIncomePay,
	--						LeaderIncome,UplineSupportIncome,SilverIncome,LeaderPointsPay,UplineSupportPointsPay,SilverPointsPay,
	--						IsRegistrationActivated,MobileNo,EmailId)		
	Select um.UserId,um.RefferalCode,um.FullName,um.PANNo,um.BankAccNo,um.ISFCCode,
			--ISNULL(purchase.TotalPurchase,0)TotalPurchase,
			ISNULL(DirectIncome,0)DirectIncome,	ISNULL(RePurchaseIncome,0)RePurchaseIncome,ISNULL(VoltIncome,0)VoltIncome,
			ISNULL(FinalPoints,0)TotalPointsIncome,ISNULL(TotalPayout,0)TotalPayout,
			ISNULL(DirectIncomePay,0)DirectIncomePay,ISNULL(PointsIncomePay,0)PointsIncomePay,
			ISNULL(VoltIncomePay,0)VoltIncomePay,ISNULL(leader.CreditPoints,0)FinalLeaderPoints,
			ISNULL(uplinesupport.CreditPoints,0)FinalUplineSupportPoints,
			ISNULL(silver.CreditPoints,0)FinalSilverPoints,ISNULL(diamond.CreditPoints,0)FinalDiamondPoints,
			ISNULL(LeaderIncomePay,0)LeaderIncomePay,ISNULL(UplineSupportIncomePay,0)UplineSupportIncomePay,
			ISNULL(SilverIncomePay,0) SilverPointsPay,ISNULL(DiamondIncomePay,0) DiamondIncomePay,
			ISNULL(IsRegistrationActivated,0) IsRegistrationActivated,MobileNo,EmailId
	INTO #PaymentDtl
	from UserMst as um
	LEFT JOIN (
	Select UserId,SUM(case when CreditType=1 then CreditAmount end) as DirectIncome 
					,SUM(case when CreditType=2 then CreditAmount end) as RePurchaseIncome
					,SUM(case when CreditType=3 then CreditAmount end) as FranchiseeIncome
		from	WalletCreditDtl 
		WHERE cast(CreditDate as date)<=@Todate
		group by UserId
	) as direct on um.UserId=direct.UserId	
	LEFT JOIN (
	Select UserId,SUM(CreditAmt) as VoltIncome
	from	CompanyShareCreditDtl
		WHERE cast(CreditDate as date)<=@Todate
		group by UserId
		) as volt on um.UserId=volt.UserId
	LEFT JOIN(
		Select UserId,SUM(DebitAmount) as TotalPayout,SUM(DirectIncomePay)DirectIncomePay,SUM(VoltIncomePay) as VoltIncomePay
				,SUM(PointsIncomePay)PointsIncomePay,SUM(LeaderIncomePay)LeaderIncomePay,SUM(UplineSupportIncomePay)UplineSupportIncomePay
				,SUM(SilverIncomePay)SilverIncomePay,SUM(DiamondIncomePay)DiamondIncomePay
		from	WalletDebitDtl
		--WHERE cast(DebitDate as date)<=@Todate
		group by UserId
	) as debit on um.UserId=debit.UserId
	--LEFT JOIN dbo.FN_GET_ALL_TOTAL_MATCHING_POINTS(@UserId) as points on um.UserId=points.UserId
	LEFT JOIN(Select UserId,SUM(MatchingPoints)FinalPoints 
			  from MatchingPointsCreditDtl			  
				WHERE cast(CreditDate as date)<=@Todate
			  group by UserId) as points on um.UserId=points.UserId
	LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(1,@UserId,default,@Todate) as leader on um.UserId=leader.UserId
	LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(0,@UserId,default,@Todate) as uplinesupport on um.UserId=uplinesupport.UserId
	LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(0,@UserId,default,@Todate) as silver on um.UserId=silver.UserId
	LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(1,@UserId,default,@Todate) as diamond on um.UserId=diamond.UserId
	Where um.UserId=case when @UserId=0 then um.UserId else @UserId end
			--AND ISNULL(IsRegistrationActivated,0)=1

	Declare @TDSPercWithoutPAN decimal
	Declare @TDSPercWithPAN decimal
	Declare @AdminChargePerc decimal
	Select @TDSPercWithoutPAN=Value from ConfigMst where [Key]=''TDSPercWithoutPAN''
	Select @TDSPercWithPAN=Value from ConfigMst where [Key]=''TDSPercWithPAN''
	Select @AdminChargePerc=Value from ConfigMst where [Key]=''AdminChargePerc''
	SET @TDSPercWithoutPAN=ISNULL(@TDSPercWithoutPAN,20)
	SET @TDSPercWithPAN=ISNULL(@TDSPercWithPAN,5)
	SET @AdminChargePerc=ISNULL(@AdminChargePerc,5)

	insert into @PaymentDtl(UserId,RefferalCode,FullName,PANNo,BankAccNo,ISFCCode,
						DirectIncome,RePurchaseIncome,VoltIncome,
							TotalPointsIncome,TotalPayout,DirectIncomePay,PointsIncomePay,VoltIncomePay,
							LeaderIncome,UplineSupportIncome,SilverIncome,DiamondIncome,
							LeaderPointsPay,UplineSupportPointsPay,SilverPointsPay,DiamondPointsPay,
							IsRegistrationActivated,MobileNo,EmailId)
	Select * from #PaymentDtl
	
	Update pd 
	set 
		 TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome+LeaderIncome+UplineSupportIncome+SilverIncome+DiamondIncome),0)
		 --TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome),0)
		,TDSPerc=case when len(PANNo)>0 then @TDSPercWithPAN else @TDSPercWithoutPAN end
		,AdminChargePerc=@AdminChargePerc
	from @PaymentDtl as pd


	--SELECT *
	Update pd set TDS=((TotalIncome-TotalPayout)*TDSPerc/100),AdminCharge=((TotalIncome-TotalPayout)*AdminChargePerc/100),
			NetPayable=TotalIncome-TotalPayout
	FROM	@PaymentDtl as pd

	Select *,(TotalIncome-TotalPayout-TDS-AdminCharge) as FinalPayoutAmt from @PaymentDtl
	order by NetPayable desc
	
	Drop Table #PaymentDtl

END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_USER_POINTS_ON_PURCHASE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_USER_POINTS_ON_PURCHASE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INS_USER_POINTS_ON_PURCHASE]
(
@UserId bigint,
@UserPurchaseHdrId bigint
)
AS
BEGIN
	Declare @walletType int /* 1:Direct		2:Repurchase	3:FranchiseeReferral  */
	--Declare @UserId bigint=52
	--Declare @UserPurchaseHdrId bigint=134

	Select @WalletType=case when ISNULL(IsRepurchaseOrder,1)=1 then 2 ELSE 1 END  	
	from UserPurchaseHdr with(nolock)
	Where UserPurchaseHdrId=@UserPurchaseHdrId

	SELECT * 
	into #tmpParents
	FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@UserId,1)

	IF @walletType=1	--Direct
	BEGIN
		SELECT upd.UserPurchaseDtlId,DirectMatchingPerc as MatchingPerc,DirectMatchingAmt,Qty,
				(DirectMatchingAmt*Qty) as DirectMatchingVolume,
				cast(((DirectMatchingAmt*DirectMatchingPerc*Qty)/100) as numeric(18,0)) as CreditAmount
		into #tmpDirectBVDetails
		FROM UserPurchaseHdr as uph with(nolock)
		INNER JOIN UserPurchaseDtl as upd with(nolock) on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
		INNER JOIN ProductMst as pm with(nolock) on upd.ProductId=pm.ProductId
		Where uph.UserPurchaseHdrId=@UserPurchaseHdrId
		
		insert into PointsCreditDtl(UserId,UserPurchaseDtlId,CreditPerc,CreditDate,CreditPoints,IsActive,CreatedBy,CreatedDate)
		Select 
		tp.UserId ,bv.UserPurchaseDtlId,bv.MatchingPerc as CreditPerc,dbo.fn_GetServerDate() as CreditDate,
		bv.CreditAmount as CreditPoints,1 as IsActive,@UserId as CreatedBy,dbo.fn_GetServerDate() as CreatedDate
		from #tmpDirectBVDetails as bv
		cross join (SELECT * FROM #tmpParents Where UserId<>@UserId) as tp
		
		Update upd set upd.DirectMatchingVolume=bv.DirectMatchingVolume
		from UserPurchaseDtl as upd  with(nolock)
		INNER JOIN #tmpDirectBVDetails as bv on upd.UserPurchaseDtlId=bv.UserPurchaseDtlId
		
		drop table #tmpDirectBVDetails
	END
	ELSE IF @walletType=2	--Repurchase
	BEGIN
		SELECT upd.UserPurchaseDtlId,RepurchaseMatchingPerc as MatchingPerc,RepurchaseMatchingAmt,Qty,
				(RepurchaseMatchingAmt*Qty) as RepurchaseMatchingVolume,
				cast(((RepurchaseMatchingAmt*RepurchaseMatchingPerc*Qty)/100) as numeric(18,0)) as CreditAmount
		into #tmpRepurchaseBVDetails
		FROM UserPurchaseHdr as uph with(nolock)
		INNER JOIN UserPurchaseDtl as upd with(nolock) on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
		INNER JOIN ProductMst as pm with(nolock) on upd.ProductId=pm.ProductId
		Where uph.UserPurchaseHdrId=@UserPurchaseHdrId
		
		
		insert into PointsCreditDtl(UserId,UserPurchaseDtlId,CreditPerc,CreditDate,CreditPoints,IsActive,CreatedBy,CreatedDate)
		Select 
		tp.UserId ,bv.UserPurchaseDtlId,bv.MatchingPerc as CreditPerc,dbo.fn_GetServerDate() as CreditDate,
		bv.CreditAmount as CreditPoints,1 as IsActive,@UserId as CreatedBy,dbo.fn_GetServerDate() as CreatedDate
		from #tmpRepurchaseBVDetails as bv
		cross join #tmpParents as tp
		
		Update upd set upd.RepurchaseMatchingVolume=bv.RepurchaseMatchingVolume
		from UserPurchaseDtl as upd  with(nolock)
		INNER JOIN #tmpRepurchaseBVDetails as bv on upd.UserPurchaseDtlId=bv.UserPurchaseDtlId
		
		drop table #tmpRepurchaseBVDetails
	END

	drop table #tmpParents
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_USER_COMPANY_SHARE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_USER_COMPANY_SHARE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INS_USER_COMPANY_SHARE]
(
@UserPurchaseHdrId bigint,
@RefferalUserId bigint,
@ActionBy int
)
AS
BEGIN
	--Declare
	--@UserPurchaseHdrId bigint,
	--@RefferalUserId bigint,
	--@ActionBy int

	SELECT * 
	into #tmpConfig
	FROM ConfigMst with(nolock)
	WHERE [Key] in (''VoltIncomeBV'',''VoltIncomeAmount'',''CompanySharePerc'')

	DECLARE @VoltIncomeBV float
	DECLARE @VoltIncomeAmount float
	DECLARE @CompanySharePerc float
	Declare @DirectBV float 
	DECLARE @StartDate datetime
	DECLARE @EndDate datetime
	DECLARE @NoOfVolts int

	SELECT @VoltIncomeBV=[Value] FROM #tmpConfig Where [Key]=''VoltIncomeBV''
	SELECT @VoltIncomeAmount=[Value] FROM #tmpConfig Where [Key]=''VoltIncomeAmount''
	SELECT @CompanySharePerc=[Value] FROM #tmpConfig Where [Key]=''CompanySharePerc''

	Select @StartDate=StartDate,@EndDate=EndDate from [fn_GetFirstLastDateOfWeekByDate](dbo.fn_GetServerDate())

	SELECT @DirectBV=SUM(TotalBV) FROM DBO.FN_GET_USER_TOTAL_DIRECT_BV(@RefferalUserId,@StartDate,@EndDate)

	SET @DirectBV=ISNULL(@DirectBV,0)
	SET @VoltIncomeBV=ISNULL(@VoltIncomeBV,0)
	SET @VoltIncomeAmount=ISNULL(@VoltIncomeAmount,0)
	SET @CompanySharePerc=ISNULL(@CompanySharePerc,0)

	SELECT @NoOfVolts=ISNULL(NoOfVolts,1) 
	FROM USERMST  with(nolock)
	WHERE UserId =@RefferalUserId

	DROP TABLE #tmpConfig


END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_ORDER_DTL]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_ORDER_DTL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_USER_ORDER_DTL]
(
@UserPurchaseHdrId bigint
)
AS
BEGIN
	--Declare @UserId int=9
	Select uph.UserPurchaseHdrId,uph.UserId,uph.OrderNo,uph.TotalOrderAmount,uph.OrderDate,uph.FranchiseeId,uph.OrderStatusId,
		upd.UserPurchaseDtlId,upd.Amount,upd.ProductPrice,upd.Qty,pm.ProductName,
		ISNULL(upd.ProductVariantId,0)ProductVariantId,ISNULL(pvm.VariantName,'''')VariantName
	from UserPurchaseHdr as uph 
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN ProductMst as pm on upd.ProductId=pm.ProductId
	LEFT JOIN ProductVariantMst as pvm on upd.ProductVariantId=pvm.ProductVariantId
	where uph.UserPurchaseHdrId=@UserPurchaseHdrId

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ORDER_INVOICE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ORDER_INVOICE_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_ORDER_INVOICE_DETAILS]
(
@UserPurchaseHdrId int=0
)
AS
BEGIN
	--Declare @UserPurchaseHdrId int=0


	Select	um.FullName,um.[Address] as [Address],um.GSTNo,EmailId,MobileNo,uph.OrderNo,uph.OrderDate,
			pm.ProductName,pcm.CategoryName,upd.Qty,
			upd.ProductPrice,upd.Amount,cm.CityName,uph.TotalOrderAmount,
			cast(case when uph.UserPurchaseHdrId=oh.UserPurchaseHdrId  then 0 else 1 end as bit) as IsRepurchase,
			um.RefferalCode,ISNULL(IsAyurvedic,0)IsAyurvedic
	from	UserPurchaseHdr	as	uph	
	INNER JOIN	UserPurchaseDtl	upd	on	uph.UserPurchaseHdrId=upd.UserPurchaseHdrId	
	INNER JOIN	ProductMst as pm	on	upd.ProductId=pm.ProductId	
	INNER JOIN	ProductCategoryMst as pcm	on	pm.ProductCategoryId=pcm.ProductCategoryId
	INNER JOIN	UserMst	as	um	on	uph.UserId=um.UserId
	LEFT JOIN	CityMst	as	cm	on	um.CityId=cm.CityId
	LEFT JOIN (
				Select * from (
			Select Row_Number() over(partition by UserId order by UserPurchaseHdrId) as RowNo,UserPurchaseHdrId,UserId
			from UserPurchaseHdr
			) as oh 
			Where oh.RowNo=1
			) as oh on uph.UserPurchaseHdrId=oh.UserPurchaseHdrId
	Where	uph.UserPurchaseHdrId=case when @UserPurchaseHdrId=0 then uph.UserPurchaseHdrId else @UserPurchaseHdrId end
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_SALES_ORDER_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_SALES_ORDER_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_SALES_ORDER_DETAILS]
(
@UserPurchaseHdrId int=0
)
AS
BEGIN
	--Declare @UserPurchaseHdrId int=0


	Select	um.FullName,um.[Address] as [Address],um.GSTNo,EmailId,MobileNo,uph.OrderNo,uph.OrderDate,
			pm.ProductName,pcm.CategoryName,upd.Qty as Qty,
			upd.ProductPrice,upd.Amount,cm.CityName,uph.TotalOrderAmount,
			cast(case when uph.UserPurchaseHdrId=oh.UserPurchaseHdrId  then 0 else 1 end as bit) as IsRepurchase,
			um.RefferalCode,ISNULL(IsAyurvedic,0)IsAyurvedic,
			case when uph.IsRepurchaseOrder=0 then upd.DirectPurchaseVolume else upd.RepurchaseMatchingVolume end as BusinessVolume
	from	UserPurchaseHdr	as	uph	
	INNER JOIN	UserPurchaseDtl	upd	on	uph.UserPurchaseHdrId=upd.UserPurchaseHdrId	
	INNER JOIN	ProductMst as pm	on	upd.ProductId=pm.ProductId	
	INNER JOIN	ProductCategoryMst as pcm	on	pm.ProductCategoryId=pcm.ProductCategoryId
	INNER JOIN	UserMst	as	um	on	uph.UserId=um.UserId
	LEFT JOIN	CityMst	as	cm	on	um.CityId=cm.CityId
	LEFT JOIN dbo.[fn_GetUserFirstOrderId](default) as oh on uph.UserPurchaseHdrId=oh.UserPurchaseHdrId
	Where	uph.UserPurchaseHdrId=case when @UserPurchaseHdrId=0 then uph.UserPurchaseHdrId else @UserPurchaseHdrId end
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'--Drop Procedure PROC_GET_RPT_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS
CREATE PROCEDURE [dbo].[PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS]
(
@IsDiamondPoint bit=0,
@UserId int=0,
@FromDate date=null,
@ToDate date=null
)
AS
BEGIN
--Declare @UserId int=0
--Declare @FromDate date=null
--Declare @ToDate date=null

Select a.*,um.FullName,um.RefferalCode 
from dbo.FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS(@IsDiamondPoint,@UserId,@FromDate,@ToDate) as a
INNER JOIN UserMst as um on a.UserId=um.UserId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_REPURCHASE_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_REPURCHASE_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_REPURCHASE_REPORT]
(
@UserId int
)
AS
BEGIN
		
	Declare @date date=(Select RegistrationDate from UserMst where UserId=@UserId)
	Declare @ToDate date=getdate()
	Declare @i int=1
	Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20))
	--Select @date,MONTH(@date),YEAR(@date)
	--While @ToDate>=@date
	While @i<=6
	BEGIN
		insert into @MonthYear
		Select @i,MONTH(@date),YEAR(@date),LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date)
		--set @date=DATEADD(M,-1,@date)
		SET @i=@i+1
		SET @date=DATEADD(M,1,@date)
	END
	
	Select Id,mm.Month as [Month],mm.Year as [Year],mm.MonthYear,ISNULL(CreditAmount,0) as CreditAmount from @MonthYear as mm
	LEFT JOIN(Select CreditMonth,CreditYear,case when LeftPoint>=RightPoint then RightPoint else LeftPoint end as CreditAmount
				from (
					Select Month(CreditDate) as CreditMonth,Year(CreditDate) As CreditYear,
							ISNULL(SUM(case when um.Position=''L'' then CreditPoints end),0) as LeftPoint ,
							ISNULL(SUM(case when um.Position=''R'' then CreditPoints end),0) as RightPoint 
					from PointsCreditDtl as pcd
					INNER JOIN UserPurchaseDtl as upd on pcd.UserPurchaseDtlId=upd.UserPurchaseDtlId
					INNER JOIN UserPurchaseHdr as uph on upd.UserPurchaseHdrId=uph.UserPurchaseHdrId
					INNER JOIN UserMst as um on uph.UserId=um.UserId
					where pcd.UserId=@UserId
					group by Month(CreditDate),Year(CreditDate)
				) as finalCredit
	)finalCredit on [Month]=CreditMonth and [Year]=CreditYear
	
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_PRODUCT_VARIANT_LIST]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_PRODUCT_VARIANT_LIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_PRODUCT_VARIANT_LIST]
(
@ProductId int=0,
@ProductVariantId int=0
)
AS
BEGIN
	Select pvm.*,pm.ProductName 
	from ProductVariantMst as pvm
	INNER JOIN ProductMst as pm on pvm.ProductId=pm.ProductId
	Where pvm.ProductId=case when @ProductId=0 then pvm.ProductId else @ProductId end 
		AND pvm.ProductVariantId=case when @ProductVariantId=0 then pvm.ProductVariantId else @ProductVariantId end
		--And pvm.IsActive=1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_USER_HIERARCHY]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_USER_HIERARCHY]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_ALL_USER_HIERARCHY]
(
@UserId int,
@IncludeSelf bit=0
)
AS
BEGIN
	
	SET FMTONLY OFF
	Select UserId,FirstName,FullName,RefferalUserId,RefferalCode,MobileNo,
						Position,IsRegistrationActivated,ProfileImage,LeftUserId,RightUserId
	into #tmpUsers
	FROM UserMst 
	--FROM UserMst(nolock) 
	SELECT * FROM (
	Select um.UserId,um.FirstName,um.FullName,um.RefferalUserId,um.RefferalCode,um.MobileNo,
			case when um.Position=''L'' then ''Left'' else ''Right'' end as PositionName,
			case when um.Position=''L'' then 0 else 1 end as PositionOrder,
			ISNULL(um.IsRegistrationActivated,0) as IsActive,pum.UserId as ParentUserId,UserLevel,
			um.ProfileImage,rum.FirstName as RefferedByUser,rum.FullName as RefferedByUserFullName,
			ISNULL(cast((case when um.LeftUserId is null and um.RightUserId is null then 0 else 1 end) as bit),1) as HasChilds,
			um.Position,um.LeftUserId,um.RightUserId,cast(0 as float) as BV
	from FN_GET_ALL_CHILD_USER_TREE(@UserId,@IncludeSelf) as ac
	INNER JOIN #tmpUsers as um on ac.UserId=um.UserId
	LEFT JOIN #tmpUsers as pum on ((ac.UserId=pum.LeftUserId) or (ac.UserId=pum.RightUserId))
	LEFT JOIN #tmpUsers as rum on um.RefferalUserId=rum.UserId
	) as Final
	order by UserLevel,PositionOrder
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]
(
@UserId int
)
AS
BEGIN
	SELECT um.*,cm.CityName--,pm.PlanName
	FROM UserMst as um
	LEFT JOIN CityMst as cm on um.CityId=cm.CityId
	Where um.RefferalUserId=@UserId and um.IsActive=1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_HIERARCHY_TEAM_MEMBERS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ALL_HIERARCHY_TEAM_MEMBERS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ALL_HIERARCHY_TEAM_MEMBERS]
(
@UserId int,
@IncludeSelf bit=0
)
AS
BEGIN
	SET FMTONLY ON
	
	Select UserId,FirstName,FullName,RefferalUserId,RefferalCode,MobileNo,
						Position,IsRegistrationActivated,ProfileImage,LeftUserId, RightUserId
	into #tempUser
	FROM UserMst with(nolock) 

	Select um.UserId,um.FirstName as UserFirstName,um.FullName as UserFullName,um.RefferalUserId,um.RefferalCode,um.MobileNo,
			case when um.Position=''L'' then ''Left'' else ''Right'' end as Position,
			ISNULL(um.IsRegistrationActivated,0) as IsActive,ISNULL(pum.UserId,0) as ParentUserId,UserLevel,
			um.ProfileImage,rum.FirstName as RefferedByUser,rum.FullName as RefferedByUserFullName
	from FN_GET_ALL_CHILD_USER_TREE(@UserId,@IncludeSelf) as ac
	INNER JOIN #tempUser as um on ac.UserId=um.UserId
	LEFT JOIN #tempUser as pum on ((ac.UserId=pum.LeftUserId) or (ac.UserId=pum.RightUserId))
	LEFT JOIN #tempUser as rum on um.RefferalUserId=rum.UserId
	order by UserLevel

	--DROP TABLE #tempUser
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_PROFITE_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_PROFITE_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_ADMIN_PROFITE_REPORT]
AS
BEGIN
	Declare @date date=getdate()
	Declare @i int=6
	Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20),Profite float)
	--Select @date,MONTH(@date),YEAR(@date)
	While @i>=1
	BEGIN
		
		Declare @FromDate datetime=DATEADD(mm, DATEDIFF(mm, 0, @date), 0)
		Declare @ToDate datetime=DATEADD(ms, -3, DATEADD(mm, DATEDIFF(m, 0, @date) + 1, 0))
		
		insert into @MonthYear
		Select @i as Id,MONTH(@date) as MONTH,YEAR(@date) as YEAR,LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date) as MonthYear,Profite 
		from dbo.[FN_GET_ADMIN_PROFITE_DETAILS](@FromDate,@ToDate)
		
		set @date=DATEADD(M,-1,@date)
		SET @i=@i-1
	END
	
	Select * from @MonthYear	
	order by [Year] desc,[Month] desc
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ADMIN_PROFITE_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_ADMIN_PROFITE_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'--exec [PROC_GET_ADMIN_PROFITE_DETAILS] ''2019-07-01'',''2019-07-31''
CREATE PROCEDURE [dbo].[PROC_GET_ADMIN_PROFITE_DETAILS]
(
@FromDate datetime,
@ToDate datetime
)
AS
BEGIN
	--Declare @FromDate datetime=''2019-09-01''
	--Declare @ToDate datetime=''2019-09-15''

	Select * from dbo.[FN_GET_ADMIN_PROFITE_DETAILS](@FromDate,@ToDate)
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_RIGHT_SIDE_DIRECT_BV]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_RIGHT_SIDE_DIRECT_BV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_RIGHT_SIDE_DIRECT_BV](52)
CREATE Function [dbo].[FN_GET_USER_RIGHT_SIDE_DIRECT_BV]
(
@UserId int
)
RETURNS @CreditDtl table(CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max),RightBV float)
AS
BEGIN

	insert into @CreditDtl
	Select	pcd.CreditDate,ru.FullName,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position=''L'' 
					then (case when uph.IsRepurchaseOrder=0 then DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=0 then DirectPurchaseVolume end) end)RightBV
			--SUM(CreditPoints) as RightPoints,pcd.CreditDate,ru.FullName,um.RefferalCode
	from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,default) as ru
	INNER JOIN UserPurchaseHdr as uph on ru.UserId=uph.UserId and uph.IsRepurchaseOrder=0
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on ru.UserId=um.UserId
	Where pcd.UserId=@UserId
	group by pcd.CreditDate,ru.FullName,um.RefferalCode

	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_ADMIN_PENDING_PAYOUT_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_ADMIN_PENDING_PAYOUT_DETAILS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from dbo.[FN_ADMIN_PENDING_PAYOUT_DETAILS](0,default)
CREATE Function [dbo].[FN_ADMIN_PENDING_PAYOUT_DETAILS]
(
@UserId numeric(18,0)=0,
@IsUptoToday bit=1
)
RETURNS @PaymentDtl table(UserId bigint,RefferalCode nvarchar(20),FullName nvarchar(max),PANNo nvarchar(50),BankAccNo nvarchar(50),ISFCCode nvarchar(50)
							,DirectIncome numeric(18,0),RePurchaseIncome numeric(18,0),VoltIncome numeric(18,0)
							,TotalPointsIncome numeric(18,0),TotalPayout numeric(18,0),TotalIncome numeric(18,0)
							,DirectIncomePay numeric(18,0),PointsIncomePay numeric(18,0)
							,TDSPerc numeric(18,2),TDS numeric(18,0),AdminChargePerc numeric(18,2),AdminCharge numeric(18,0)
							,NetPayable numeric(18,0),VoltIncomePay numeric(18,0),LeaderIncome numeric(18,0)
							,UplineSupportIncome numeric(18,0),SilverIncome numeric(18,0),LeaderPointsPay numeric(18,0)
							,UplineSupportPointsPay numeric(18,0),SilverPointsPay numeric(18,0),IsRegistrationActivated bit,
							MobileNo numeric(18,0),EmailId nvarchar(max),FinalPayoutAmt numeric(18,0))
AS
BEGIN
Declare @ToDate date=getdate()
IF @IsUptoToday=0
BEGIN
	Select @ToDate=EndDate from dbo.fn_GetFirstLastDateOfWeekByDate(DATEADD(d,-7,GETDATE()))
END
	insert into @PaymentDtl(UserId,RefferalCode,FullName,PANNo,BankAccNo,ISFCCode,
						DirectIncome,RePurchaseIncome,VoltIncome,
							TotalPointsIncome,TotalPayout,DirectIncomePay,PointsIncomePay,VoltIncomePay,
							LeaderIncome,UplineSupportIncome,SilverIncome,LeaderPointsPay,UplineSupportPointsPay,SilverPointsPay,
							IsRegistrationActivated,MobileNo,EmailId)
	Select um.UserId,um.RefferalCode,um.FullName,um.PANNo,um.BankAccNo,um.ISFCCode,
			ISNULL(DirectIncome,0)DirectIncome,	ISNULL(RePurchaseIncome,0)RePurchaseIncome,ISNULL(VoltIncome,0)VoltIncome,
			ISNULL(FinalPoints,0)TotalPointsIncome,ISNULL(TotalPayout,0)TotalPayout,
			ISNULL(DirectIncomePay,0)DirectIncomePay,ISNULL(PointsIncomePay,0)PointsIncomePay,
			ISNULL(VoltIncomePay,0)VoltIncomePay,ISNULL(leader.CreditPoints,0)FinalLeaderPoints,
			ISNULL(uplinesupport.CreditPoints,0)FinalUplineSupportPoints,
			ISNULL(silver.CreditPoints,0)FinalSilverPoints,
			ISNULL(LeaderIncomePay,0)LeaderIncomePay,ISNULL(UplineSupportIncomePay,0)UplineSupportIncomePay,
			ISNULL(SilverIncomePay,0) SilverPointsPay,
			ISNULL(IsRegistrationActivated,0) IsRegistrationActivated,MobileNo,EmailId
	from UserMst as um
	LEFT JOIN (
	Select UserId,SUM(case when CreditType=1 then CreditAmount end) as DirectIncome 
					,SUM(case when CreditType=2 then CreditAmount end) as RePurchaseIncome
					,SUM(case when CreditType=3 then CreditAmount end) as FranchiseeIncome
	from	WalletCreditDtl
		WHERE cast(CreditDate as date)<=@Todate
		group by UserId
		) as direct on um.UserId=direct.UserId	
	LEFT JOIN (
	Select UserId,SUM(CreditAmt) as VoltIncome
	from	CompanyShareCreditDtl	
		WHERE cast(CreditDate as date)<=@Todate
	group by UserId
		) as volt on um.UserId=volt.UserId
	LEFT JOIN(
		Select UserId,SUM(DebitAmount) as TotalPayout,SUM(DirectIncomePay)DirectIncomePay,SUM(VoltIncomePay) as VoltIncomePay
				,SUM(PointsIncomePay)PointsIncomePay,SUM(LeaderIncomePay)LeaderIncomePay,SUM(UplineSupportIncomePay)UplineSupportIncomePay
				,SUM(SilverIncomePay)SilverIncomePay
		from	WalletDebitDtl	
		--WHERE cast(DebitDate as date)<=@Todate
	 group by UserId
	) as debit on um.UserId=debit.UserId
	--LEFT JOIN dbo.FN_GET_ALL_TOTAL_MATCHING_POINTS(@UserId) as points on um.UserId=points.UserId
	LEFT JOIN(Select UserId,SUM(MatchingPoints)FinalPoints 
			  from MatchingPointsCreditDtl	
			  WHERE cast(CreditDate as date)<=@Todate	
			  group by UserId) as points on um.UserId=points.UserId
	LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(1,@UserId,default,@Todate) as leader on um.UserId=leader.UserId
	LEFT JOIN dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(0,@UserId,default,@Todate) as uplinesupport on um.UserId=uplinesupport.UserId
	LEFT JOIN dbo.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(0,@UserId,default,@Todate) as silver on um.UserId=silver.UserId
	Where um.UserId=case when @UserId=0 then um.UserId else @UserId end
			--AND ISNULL(IsRegistrationActivated,0)=1

	Declare @TDSPercWithoutPAN decimal
	Declare @TDSPercWithPAN decimal
	Declare @AdminChargePerc decimal
	Select @TDSPercWithoutPAN=Value from ConfigMst where [Key]=''TDSPercWithoutPAN''
	Select @TDSPercWithPAN=Value from ConfigMst where [Key]=''TDSPercWithPAN''
	Select @AdminChargePerc=Value from ConfigMst where [Key]=''AdminChargePerc''
	SET @TDSPercWithoutPAN=ISNULL(@TDSPercWithoutPAN,20)
	SET @TDSPercWithPAN=ISNULL(@TDSPercWithPAN,5)
	SET @AdminChargePerc=ISNULL(@AdminChargePerc,5)

	Update pd 
	set 
		 TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome+LeaderIncome+UplineSupportIncome+SilverIncome),0)
		 --TotalIncome=ISNULL((DirectIncome+RePurchaseIncome+TotalPointsIncome+VoltIncome),0)
		,TDSPerc=case when len(PANNo)>0 then @TDSPercWithPAN else @TDSPercWithoutPAN end
		,AdminChargePerc=@AdminChargePerc
	from @PaymentDtl as pd


	--SELECT *
	Update pd set TDS=((TotalIncome-TotalPayout)*TDSPerc/100),AdminCharge=((TotalIncome-TotalPayout)*AdminChargePerc/100),
			NetPayable=TotalIncome-TotalPayout
	FROM	@PaymentDtl as pd
	
	Update  pd set FinalPayoutAmt=(TotalIncome-TotalPayout-TDS-AdminCharge)
	FROM	@PaymentDtl as pd
	
	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_TOTAL_EARNING_DATE_RANGE]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_TOTAL_EARNING_DATE_RANGE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FN_GET_USER_TOTAL_EARNING_DATE_RANGE]
(
@UserId int=0,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(UserId numeric(18,0), Income numeric(18,0))
AS
BEGIN

	insert into @CreditDtl
	Select a.UserId,SUM(Income)as Income from (
	
	Select UserId,CreditPoints as Income from dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE(1,@UserId,@FromDate,@ToDate)
	
	Union ALL
	
	Select UserId,CreditPoints as Income from dbo.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS_DATE_RANGE(0,@UserId,@FromDate,@ToDate)
	
	Union ALL
	
	Select @UserId as UserId,SUM(CreditAmount) as Income 
	from WalletCreditDtl 
	Where UserId=@UserId 
		AND cast(CreditDate as date)>=case when @FromDate is null then cast(CreditDate as date) else @FromDate end
		AND cast(CreditDate as date)<=case when @ToDate is null then cast(CreditDate as date) else @ToDate end 
		and IsActive=1  	
	group by UserId
		
	Union ALL
	
	Select @UserId as UserId,SUM(MatchingPoints) as Income 
	from MatchingPointsCreditDtl 
	Where UserId=@UserId 
		AND cast(CreditDate as date)>=case when @FromDate is null then cast(CreditDate as date) else @FromDate end
		AND cast(CreditDate as date)<=case when @ToDate is null then cast(CreditDate as date) else @ToDate end 
	group by UserId
		
	UNION ALL
	
	Select UserId,SUM(CreditAmt) as Income from CompanyShareCreditDtl 
	Where UserId=@UserId 
		AND cast(CreditDate as date)>=case when @FromDate is null then cast(CreditDate as date) else @FromDate end
		AND cast(CreditDate as date)<=case when @ToDate is null then cast(CreditDate as date) else @ToDate end  	
	group by UserId
	) as a
	
	group by UserId

	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_RIGHT_SIDE_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_RIGHT_SIDE_POINTS](1,''2020-02-15'',default)
--Select * from [FN_GET_USER_RIGHT_SIDE_POINTS](1,default,default)
CREATE Function [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS]
(
@UserId int,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(RightPoints numeric(18,0), CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max),RightBV float)
AS
BEGIN

	insert into @CreditDtl
	Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position=''L'' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as RightPoints,
			pcd.CreditDate,ru.FullName,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position=''L'' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end)RightBV
			--SUM(CreditPoints) as RightPoints,pcd.CreditDate,ru.FullName,um.RefferalCode
	from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,default) as ru
	--INNER JOIN UserPurchaseHdr as uph on ru.UserId=uph.UserId
	INNER JOIN UserPurchaseHdr as uph on ru.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on ru.UserId=um.UserId
	Where pcd.UserId=@UserId
			and ((cast(pcd.CreditDate as date)>=@FromDate) or (@FromDate is null))
			and ((cast(pcd.CreditDate as date)<=@ToDate) or (@ToDate is null))
	group by pcd.CreditDate,ru.FullName,um.RefferalCode

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEFT_SIDE_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEFT_SIDE_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_LEFT_SIDE_POINTS](1,''2020-02-15'',default)
--Select * from [FN_GET_USER_LEFT_SIDE_POINTS](1,default,default)
CREATE Function [dbo].[FN_GET_USER_LEFT_SIDE_POINTS]
(
@UserId int,
@FromDate date=null,
@ToDate date=null
)
RETURNS @CreditDtl table(LeftPoints numeric(18,2), CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max),LeftBV float)
AS
BEGIN

	insert into @CreditDtl
	Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position=''R'' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as LeftPoints,
			pcd.CreditDate,lu.FullName,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position=''R'' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV
			 
			--SUM(CreditPoints) as LeftPoints,pcd.CreditDate,lu.FullName,um.RefferalCode
	from [FN_GET_ALL_LEFT_SIDED_USER_TREE](@UserId,default) as lu
	--INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId
	INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on lu.UserId=um.UserId
	Where pcd.UserId=@UserId
			and ((cast(pcd.CreditDate as date)>=@FromDate) or (@FromDate is null))
			and ((cast(pcd.CreditDate as date)<=@ToDate) or (@ToDate is null))
	group by pcd.CreditDate,lu.FullName,um.RefferalCode

	RETURN
END' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEFT_SIDE_DIRECT_BV]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_LEFT_SIDE_DIRECT_BV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_LEFT_SIDE_DIRECT_BV](52)
CREATE Function [dbo].[FN_GET_USER_LEFT_SIDE_DIRECT_BV]
(
@UserId int
)
RETURNS @CreditDtl table(CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max),LeftBV float)
AS
BEGIN

	insert into @CreditDtl
	Select	pcd.CreditDate,lu.FullName,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position=''R'' 
					then (case when uph.IsRepurchaseOrder=0 then DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=0 then DirectPurchaseVolume end) end) as LeftBV
			 
			--SUM(CreditPoints) as LeftPoints,pcd.CreditDate,lu.FullName,um.RefferalCode
	from [FN_GET_ALL_LEFT_SIDED_USER_TREE](@UserId,default) as lu
	INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId and uph.IsRepurchaseOrder=0
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on lu.UserId=um.UserId
	Where pcd.UserId=@UserId
	group by pcd.CreditDate,lu.FullName,um.RefferalCode

	RETURN
END


' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_TOTAL_MATCHING_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_ALL_TOTAL_MATCHING_POINTS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_ALL_TOTAL_MATCHING_POINTS](0)
CREATE Function [dbo].[FN_GET_ALL_TOTAL_MATCHING_POINTS]
(
@UserId int=0
)
RETURNS @tblUserPoints table(UserId int,FinalPoints numeric(18,0))
AS
BEGIN

	Declare @tblUser table(RowNo int,UserId int)
	insert into @tblUser
	Select ROW_NUMBER() over(order by UserId) as RowNo,UserId 
	from UserMst 
	where RefferalUserId<>0
		AND UserId=case when @UserId=0 then UserId else @UserId end
		
	--Declare @tblUserPoints table(UserId int,FinalPoints numeric(18,0))

	Declare @MaxRowNo int,@i int=1,@TempUserId int
	Select @MaxRowNo=MAX(RowNo) from @tblUser
	While @i<=@MaxRowNo
	BEGIN
		Select @TempUserId=UserId from @tblUser where RowNo=@i
		
		Declare @TotalRightPoints numeric(18,2)=0, @TotalLeftPoints numeric(18,2)=0,@FinalPoints numeric(18,2)=0
		
		Select @TotalLeftPoints=ISNULL(SUM(LeftPoints),0) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@TempUserId,default,default)
		Select @TotalRightPoints=ISNULL(SUM(RightPoints),0) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@TempUserId,default,default)
		
		SET @TotalLeftPoints=ISNULL(@TotalLeftPoints,0)
		SET @TotalRightPoints=ISNULL(@TotalRightPoints,0)
		SET @FinalPoints=case when @TotalLeftPoints>=@TotalRightPoints then @TotalRightPoints else @TotalLeftPoints end
		
		insert into @tblUserPoints
		Select @TempUserId,@FinalPoints
		SET @i=@i+1
	END

	RETURN
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_MATCHING_INCOME]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_MATCHING_INCOME]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_MATCHING_BV](1,''2020-02-15'',default)
--Select * from [FN_GET_USER_MATCHING_BV](1,default,default)
CREATE Function [dbo].[FN_GET_USER_MATCHING_INCOME]
(
@UserId int,
@FromDate date=null,
@ToDate date=null
)
RETURNS float
AS
BEGIN
	
	Declare @MatchingPoints float
	Declare @RightPoints float
	Declare @LeftPoints float
	
	Declare @CarryForwardPoints float
	Declare @CarryForwardSide char(10)
	
	SELECT top 1 @CarryForwardPoints=CarryForwardPoints,@CarryForwardSide=CarryForwardSide FROM MatchingPointsCreditDtl Where UserId=@UserId and CreditDate<@FromDate order by CreditDate desc
	
	Select @RightPoints=SUM(RightPoints) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,@FromDate,@ToDate)
	Select @LeftPoints=SUM(LeftPoints) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,@FromDate,@ToDate)
	
	SET @LeftPoints=ISNULL(@LeftPoints,0)
	SET @RightPoints=ISNULL(@RightPoints,0)
	IF @CarryForwardPoints is not null
	BEGIN
		IF @CarryForwardSide=''L''
		BEGIN
			SET @LeftPoints=@LeftPoints+@CarryForwardPoints
		END
		ELSE IF @CarryForwardSide=''R''
		BEGIN
			SET @RightPoints=@RightPoints+@CarryForwardPoints
		END
	END
	
	SET @MatchingPoints=(case when @LeftPoints>=@RightPoints then @RightPoints ELSE @LeftPoints END)

	RETURN @MatchingPoints
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_MATCHING_BV]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_GET_USER_MATCHING_BV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'--Select * from [FN_GET_USER_MATCHING_BV](1,''2020-02-15'',default)
--Select * from [FN_GET_USER_MATCHING_BV](1,default,default)
CREATE Function [dbo].[FN_GET_USER_MATCHING_BV]
(
@UserId int,
@FromDate date=null,
@ToDate date=null
)
RETURNS float
AS
BEGIN
	
	Declare @MatchingBV float
	Declare @RightBV float
	Declare @LeftBV float
	Declare @PreviousRightBV float
	Declare @PreviousLeftBV float
	Declare @CarryForward float
	Declare @CarryForwardSide char(10)
	
	Select @PreviousRightBV=SUM(RightBV) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,null,DATEADD(d,-1,@FromDate))
	Select @PreviousLeftBV=SUM(LeftBV) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,null,DATEADD(d,-1,@FromDate))
		
	Select @RightBV=SUM(RightBV) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,@FromDate,@ToDate)
	Select @LeftBV=SUM(LeftBV) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@UserId,@FromDate,@ToDate)
	
	SELECT	@CarryForward=case when ISNULL(@PreviousRightBV,0)>ISNULL(@PreviousLeftBV,0) then ISNULL(@PreviousRightBV,0)-ISNULL(@PreviousLeftBV,0)
				ELSE ISNULL(@PreviousLeftBV,0)-ISNULL(@PreviousRightBV,0) END,
			@CarryForwardSide=case when ISNULL(@PreviousRightBV,0)>ISNULL(@PreviousLeftBV,0) then ''R''
				ELSE ''L'' END
		
	IF @CarryForwardSide=''L''
	BEGIN
		SET @LeftBV=ISNULL(@LeftBV,0) + ISNULL(@CarryForward,0)
	END
	ELSE IF @CarryForwardSide=''R''
	BEGIN
		SET @RightBV=ISNULL(@RightBV,0) + ISNULL(@CarryForward,0)
	END
	
	SET @MatchingBV=(case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END)

	RETURN ISNULL(@MatchingBV,0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH]
(
@Month int,
@Year int
)
AS
BEGIN
--Declare @Month int=6
--Declare @Year int=2020

SET FMTONLY OFF

Declare @Date datetime=cast((cast(@Year as nvarchar(10))+''-''+cast(@Month as nvarchar(10))+''-01'') as date)

Declare @CompanyTurnOverUserBV float

SELECT @CompanyTurnOverUserBV=value FROM ConfigMst Where [Key]=''CompanyTurnOverUserBV''
--SET @CompanyTurnOverUserBV=5000

Declare @FromDate datetime
Declare @ToDate datetime

Select @FromDate=StartDate,@ToDate=EndDate from dbo.fn_GetFirstLastDateOfMonthByDate(@Date)

--SELECT * 
--into #Previous3rdMonth
--FROM (
--	SELECT *,dbo.FN_GET_USER_MATCHING_BV(UserId,DATEADD(M,-3,@FromDate),DATEADD(M,-3,@ToDate)) as Previous3rdMonthBV 
--	FROM(
--		SELECT UserId
--		FROM UserMst Where (IsDimondMember=1)
--	) as u
--) as final
--Where Previous3rdMonthBV>=@CompanyTurnOverUserBV

--SELECT * 
--into #Previous2rdMonth
--FROM (
--	SELECT UserId,Previous3rdMonthBV,dbo.FN_GET_USER_MATCHING_BV(UserId,DATEADD(M,-2,@FromDate),DATEADD(M,-2,@ToDate)) as Previous2rdMonthBV
--	FROM #Previous3rdMonth
--) as final
--Where Previous2rdMonthBV>=@CompanyTurnOverUserBV


--SELECT * 
--into #PreviousMonth
--FROM (
--	SELECT UserId,Previous3rdMonthBV,Previous2rdMonthBV,dbo.FN_GET_USER_MATCHING_BV(UserId,DATEADD(M,-1,@FromDate),DATEADD(M,-1,@ToDate)) as PreviousMonthBV
--	FROM #Previous2rdMonth
--) as final
--Where PreviousMonthBV>=@CompanyTurnOverUserBV


SELECT final.*,cast(0 as float) as Previous3rdMonthBV,cast(0 as float) as Previous2rdMonthBV,cast(0 as float) as PreviousMonthBV,
		CASE WHEN ISNULL(CurrentMonthLeftBV,0)>=ISNULL(CurrentMonthRightBV,0)
			THEN ISNULL(CurrentMonthRightBV,0)
			ELSE ISNULL(CurrentMonthLeftBV,0)
		END as CurrentMonthBV,
		cast(0 as bigint) as Amount
FROM (
SELECT *,
	(Select SUM(LeftBV) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(UserId,@FromDate,@ToDate)) as CurrentMonthLeftBV,
	(Select SUM(RightBV) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(UserId,@FromDate,@ToDate)) as CurrentMonthRightBV
FROM (SELECT UserId,RefferalCode,FullName FROM UserMst Where IsDimondMember=1) as a
) as final
Where	CurrentMonthLeftBV>=@CompanyTurnOverUserBV AND 
		CurrentMonthRightBV>=@CompanyTurnOverUserBV

--DROP TABLE #Previous3rdMonth
--DROP TABLE #Previous2rdMonth
--DROP TABLE #PreviousMonth
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_RIGHT_SIDE_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_RIGHT_SIDE_POINTS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_USER_RIGHT_SIDE_POINTS]
(
@UserId int,
@FromDate date=null,
@ToDate date=null
)
AS
BEGIN
--Declare @UserId int=15


--Declare @RightUsers table(UserId int, LeftUserId int, FullName nvarchar(max),UserLevel int)
--insert into @RightUsers
--exec PROC_GET_ALL_RIGHT_USER_TREE @UserId

--Select SUM(CreditPoints) as RightPoints,pcd.CreditDate,ru.FullName,um.RefferalCode
--from @RightUsers as ru
--INNER JOIN UserPurchaseHdr as uph on ru.UserId=uph.UserId
--INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
--INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
--INNER JOIN UserMst as um on ru.UserId=um.UserId
--Where pcd.UserId=@UserId
--group by pcd.CreditDate,ru.FullName,um.RefferalCode

Select * from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,@FromDate,@ToDate)

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_LEFT_SIDE_POINTS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_LEFT_SIDE_POINTS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_USER_LEFT_SIDE_POINTS]
(
@UserId int,
@FromDate date=null,
@ToDate date=null
)
AS
BEGIN
--Declare @UserId int=15


--Declare @LeftUsers table(UserId int, LeftUserId int, FullName nvarchar(max),UserLevel int)
--insert into @LeftUsers
--exec PROC_GET_ALL_LEFT_USER_TREE @UserId

--Select SUM(CreditPoints) as LeftPoints,pcd.CreditDate,lu.FullName,um.RefferalCode
--from [FN_GET_ALL_LEFT_USER_TREE](@UserId,default) as lu
--INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId
--INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
--INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
--INNER JOIN UserMst as um on lu.UserId=um.UserId
--Where pcd.UserId=@UserId
--group by pcd.CreditDate,lu.FullName,um.RefferalCode

Select * from [FN_GET_USER_LEFT_SIDE_POINTS](@UserId,@FromDate,@ToDate)

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_TOP_BONANZA_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_TOP_BONANZA_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_TOP_BONANZA_DETAILS]
(
@Month int=0,
@Year int=0
)
AS
BEGIN


Declare @TempDate date=cast( (cast(@Year as nvarchar(max))+''-''+cast(@Month as nvarchar(max))+''-01'') as date)
Declare @PreviousMonth int=case when @Month=4 and @Year=2020 then Month(DATEADD(M,-1,@TempDate)) else @Month end
Declare @PreviousYear int=case when @Month=4 and @Year=2020 then Year(DATEADD(M,-1,@TempDate)) else @Year end
	

Declare @BonanzaOfferCnt int
Select @BonanzaOfferCnt=Value from ConfigMst Where [Key]=''BONANZAOFFERCNT''
Declare @UserId int
Declare @TempBonanza table(UserId int,Month int,Year int,LeftBV bigint,RightBV bigint,MatchingBV bigint)

insert into @TempBonanza
Select cast(0 as int) as UserId,cast(0 as int) as Month,cast(0 as int) as Year,cast(0 as bigint) as LeftBV,cast(0 as bigint) as RightBV,0 as MatchingBV
--into #TempBonanza

DECLARE db_user CURSOR FOR 
SELECT UserId 
FROM UserMst
OPEN db_user  
FETCH NEXT FROM db_user INTO @UserId

WHILE @@FETCH_STATUS = 0  
BEGIN  
		--insert into #TempBonanza
		insert into @TempBonanza
		Select UserId,@Month,@Year,SUM(LeftBV)LeftBV,SUM(RightBV)RightBV,
			case When  SUM(LeftBV)>=SUM(RightBV) then SUM(RightBV) ELSE SUM(LeftBV) end as MatchingBV
		from (
			Select @UserId as UserId,* from(
			Select SUM(LeftBV)LeftBV,0 as RightBV 
			from [FN_GET_USER_LEFT_SIDE_POINTS](@UserId,default,default) 
			WHERE Month(CreditDate) =@Month and Year(CreditDate)=@Year
			or (Month(CreditDate) =@PreviousMonth and Year(CreditDate)=@PreviousYear)
			
			UNION 
			
			Select 0 asLeftBV,SUM(RightBV )RightBV 
			from [FN_GET_USER_RIGHT_SIDE_POINTS](@UserId,default,default)
			WHERE Month(CreditDate) =@Month and Year(CreditDate)=@Year
			or (Month(CreditDate) =@PreviousMonth and Year(CreditDate)=@PreviousYear)
			) as totalbv
		) as final
		group by UserId
	
      FETCH NEXT FROM db_user INTO @UserId 
END 

CLOSE db_user  
DEALLOCATE db_user 
--delete from #TempBonanza Where UserId=0
delete from @TempBonanza Where UserId=0
Select top 10 tb.UserId,tb.MatchingBV,tb.RightBV,tb.Month,tb.Year,tb.LeftBV,um.FullName,um.FirstName,um.LastName,
			um.MiddleName,um.MobileNo,um.EmailId,um.RefferalCode,
			DOB,Address,ProfileImage,IsGoldMember,IsSilverMember,RegistrationDate,
			ISNULL(BonanzaRewardDtlId,0)BonanzaRewardDtlId,ISNULL(PaidAmount,0)PaidAmount,
			convert(varchar,PaidDate,103)PaidDate,ISNULL(IsPaid,0)IsPaid,ISNULL(brd.UTRRefNo,'''')UTRRefNo  
--from #TempBonanza as tb
from @TempBonanza as tb
INNER JOIN UserMst as um on tb.UserId=um.UserId
LEFT JOIN BonanzaRewardDtl as brd on tb.UserId=brd.UserId and  tb.[Month]=brd.[Month] and  tb.[Year]=brd.[Year]
Where MatchingBV>=@BonanzaOfferCnt
order by MatchingBV desc
--drop table #TempBonanza
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_USER_TOTAL_EARNING_REPORT]
(
@UserId int
)
AS
BEGIN
	Declare @date date=getdate()
	Declare @i int=6
	Declare @MonthYear table(Id int,Month int,Year int,MonthYear nvarchar(20),Income float)
	--Select @date,MONTH(@date),YEAR(@date)
	While @i>=1
	BEGIN
		
		Declare @FromDate datetime=DATEADD(mm, DATEDIFF(mm, 0, @date), 0)
		Declare @ToDate datetime=DATEADD(ms, -3, DATEADD(mm, DATEDIFF(m, 0, @date) + 1, 0))
		
		insert into @MonthYear
		Select @i as Id,MONTH(@date) as MONTH,YEAR(@date) as YEAR,LEFT(DATENAME(month, @date), 3) + ''-'' +  DATENAME(year,  @date) as MonthYear,Income 
		from dbo.[FN_GET_USER_TOTAL_EARNING_DATE_RANGE](@UserId,@FromDate,@ToDate)
		
		set @date=DATEADD(M,-1,@date)
		SET @i=@i-1
	END
	
	Select * from @MonthYear	
	order by [Year] desc,[Month] desc
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_UPLINE_SUPPORT_INCOME_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_UPLINE_SUPPORT_INCOME_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'--exec PROC_RPT_GET_ADMIN_UPLINE_SUPPORT_INCOME_REPORT ''2019-09-20'',''2020-02-26''
--exec PROC_RPT_GET_ADMIN_UPLINE_SUPPORT_INCOME_REPORT ''2019-09-20'',''2019-12-26''
CREATE Procedure [dbo].[PROC_RPT_GET_ADMIN_UPLINE_SUPPORT_INCOME_REPORT]
(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	SET FMTONLY OFF
	
	--Declare @FromDate date=''2019-09-20''
	--Declare @ToDate date=''2020-12-26''
	--Declare @UserId int=0
	
	Declare @TempUserId int

	Select	um.UserId,um.UserName,um.RefferalCode,um.FullName,
			SUM(case when (lupcd.CreditDate>=@FromDate or @FromDate is null) and (lupcd.CreditDate<=@ToDate or @ToDate is null) then CreditPoints end) as UplineSupportIncome,
			SUM(CreditPoints) as TotalUplineSupportIncome,0 as MatchingBV,0 as TotalMatchingBV
	into #tmpUsers
	from dbo.[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](0,@UserId) as lupcd	
	INNER JOIN UserMst as um on lupcd.UserId=um.UserId
	group by um.UserId,um.UserName,um.RefferalCode,um.FullName
	
	DECLARE db_leader_match_bv CURSOR FOR 
	SELECT UserId 
	FROM #tmpUsers

	OPEN db_leader_match_bv  
	FETCH NEXT FROM db_leader_match_bv INTO @TempUserId  

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
			Declare @TotalLeftIncome float
			Declare @TotalLeftBV float
			Declare @LeftIncome float
			Declare @LeftBV float
			Declare @TotalRightIncome float
			Declare @TotalRightBV float
			Declare @RightIncome float
			Declare @RightBV float
			
			Select	--@TotalLeftIncome=SUM(LeftPoints),
					@TotalLeftBV=SUM(LeftBV), 		
					--@LeftIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftPoints end),
					@LeftBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftBV end)
			from FN_GET_USER_LEFT_SIDE_POINTS(@TempUserId,default,default)
			
			Select	--@TotalRightIncome=SUM(RightPoints),
					@TotalRightBV=SUM(RightBV), 		
					--@RightIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightPoints end),
					@RightBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightBV end)		
			from FN_GET_USER_RIGHT_SIDE_POINTS(@TempUserId,default,default)
			
			--Declare @FinalIncome float=case when ISNULL(@LeftIncome,0)>=ISNULL(@RightIncome,0) then ISNULL(@RightIncome,0) ELSE ISNULL(@LeftIncome,0) END
			Declare @FinalBV float=case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END
			--Declare @FinalTotalIncome float=case when ISNULL(@TotalLeftIncome,0)>=ISNULL(@TotalRightIncome,0) then ISNULL(@TotalRightIncome,0) ELSE ISNULL(@TotalLeftIncome,0) END
			Declare @FinalTotalBV float=case when ISNULL(@TotalLeftBV,0)>=ISNULL(@TotalRightBV,0) then ISNULL(@TotalRightBV,0) ELSE ISNULL(@TotalLeftBV,0) END
			
			Update #tmpUsers set MatchingBV=@FinalBV,TotalMatchingBV=@FinalTotalBV Where UserId=@TempUserId
			
		  FETCH NEXT FROM db_leader_match_bv INTO @TempUserId 
	END 

	CLOSE db_leader_match_bv  
	DEALLOCATE db_leader_match_bv 
	
	Select * from #tmpUsers
	
	drop table #tmpUsers
	
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'--exec PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT ''2019-09-20'',''2020-02-26''
--exec PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT ''2019-09-20'',''2019-12-26''
CREATE Procedure [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT]
(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	--Declare @FromDate date=''2019-09-20''
	--Declare @ToDate date=''2020-12-26''
	--Declare @UserId int=0
		SET FMTONLY OFF

	Select 0 as UserId,cast('''' as nvarchar(max)) as UserName,cast('''' as nvarchar(max)) as RefferalCode,cast('''' as nvarchar(max)) as FullName,
	cast(0 as float) as Income,cast(0 as float) as BV,cast(0 as float) as TotalIncome,cast(0 as float) as TotalBV
	into #tmpMatchingDtl
	
	delete from #tmpMatchingDtl Where UserId=0
	Declare @TempUserId int

	DECLARE db_match_bv CURSOR FOR 
	Select UserId 
	from UserMst 
	Where UserId=case when @UserId=0 then UserId else @UserId end
	OPEN db_match_bv  
	FETCH NEXT FROM db_match_bv INTO @TempUserId  

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		
			Declare @TotalLeftIncome float
			Declare @TotalLeftBV float
			Declare @LeftIncome float
			Declare @LeftBV float
			Declare @TotalRightIncome float
			Declare @TotalRightBV float
			Declare @RightIncome float
			Declare @RightBV float
			
			Select	@TotalLeftIncome=SUM(LeftPoints),
					@TotalLeftBV=SUM(LeftBV), 		
					@LeftIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftPoints end),
					@LeftBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftBV end)
			from FN_GET_USER_LEFT_SIDE_POINTS(@TempUserId,default,default)
			
			Select	@TotalRightIncome=SUM(RightPoints),
					@TotalRightBV=SUM(RightBV), 		
					@RightIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightPoints end),
					@RightBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightBV end)		
			from FN_GET_USER_RIGHT_SIDE_POINTS(@TempUserId,default,default)
			
			Declare @FinalIncome float=case when ISNULL(@LeftIncome,0)>=ISNULL(@RightIncome,0) then ISNULL(@RightIncome,0) ELSE ISNULL(@LeftIncome,0) END
			Declare @FinalBV float=case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END
			Declare @FinalTotalIncome float=case when ISNULL(@TotalLeftIncome,0)>=ISNULL(@TotalRightIncome,0) then ISNULL(@TotalRightIncome,0) ELSE ISNULL(@TotalLeftIncome,0) END
			Declare @FinalTotalBV float=case when ISNULL(@TotalLeftBV,0)>=ISNULL(@TotalRightBV,0) then ISNULL(@TotalRightBV,0) ELSE ISNULL(@TotalLeftBV,0) END
			
				insert into #tmpMatchingDtl
				Select @TempUserId as UserId,um.UserName,um.RefferalCode,um.FullName,
						@FinalIncome as Income,@FinalBV as BV,@FinalTotalIncome as TotalIncome,@FinalTotalBV as TotalBV
				From UserMst as um
				Where UserId=@TempUserId
			
		  FETCH NEXT FROM db_match_bv INTO @TempUserId 
	END 

	CLOSE db_match_bv  
	DEALLOCATE db_match_bv 

	Select UserId,UserName,RefferalCode,FullName,cast(t.Income as numeric(18,0))Income,cast(t.BV as numeric(18,0))BV,
			cast(t.TotalIncome as numeric(18,0))TotalIncome,cast(t.TotalBV as numeric(18,0))TotalBV 
	from #tmpMatchingDtl as t
	drop table #tmpMatchingDtl
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_LEADER_CREATE_INCOME_REPORT]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_ADMIN_LEADER_CREATE_INCOME_REPORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'--exec PROC_RPT_GET_ADMIN_LEADER_CREATE_INCOME_REPORT ''2019-09-20'',''2020-02-26''
--exec PROC_RPT_GET_ADMIN_LEADER_CREATE_INCOME_REPORT ''2019-09-20'',''2019-12-26''
CREATE Procedure [dbo].[PROC_RPT_GET_ADMIN_LEADER_CREATE_INCOME_REPORT]
(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	SET FMTONLY OFF
	
	--Declare @FromDate date=''2019-09-20''
	--Declare @ToDate date=''2020-12-26''
	--Declare @UserId int=0
	
	Declare @TempUserId int

	Select	um.UserId,um.UserName,um.RefferalCode,um.FullName,
			SUM(case when (lupcd.CreditDate>=@FromDate or @FromDate is null) and (lupcd.CreditDate<=@ToDate or @ToDate is null) then CreditPoints end) as LeaderIncome,
			SUM(CreditPoints) as TotalLeaderIncome,0 as MatchingBV,0 as TotalMatchingBV
	into #tmpUsers
	from dbo.[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](1,@UserId) as lupcd	
	INNER JOIN UserMst as um on lupcd.UserId=um.UserId
	group by um.UserId,um.UserName,um.RefferalCode,um.FullName
	
	DECLARE db_leader_match_bv CURSOR FOR 
	SELECT UserId 
	FROM #tmpUsers

	OPEN db_leader_match_bv  
	FETCH NEXT FROM db_leader_match_bv INTO @TempUserId  

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
			Declare @TotalLeftIncome float
			Declare @TotalLeftBV float
			Declare @LeftIncome float
			Declare @LeftBV float
			Declare @TotalRightIncome float
			Declare @TotalRightBV float
			Declare @RightIncome float
			Declare @RightBV float
			
			Select	--@TotalLeftIncome=SUM(LeftPoints),
					@TotalLeftBV=SUM(LeftBV), 		
					--@LeftIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftPoints end),
					@LeftBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftBV end)
			from FN_GET_USER_LEFT_SIDE_POINTS(@TempUserId,default,default)
			
			Select	--@TotalRightIncome=SUM(RightPoints),
					@TotalRightBV=SUM(RightBV), 		
					--@RightIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightPoints end),
					@RightBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightBV end)		
			from FN_GET_USER_RIGHT_SIDE_POINTS(@TempUserId,default,default)
			
			--Declare @FinalIncome float=case when ISNULL(@LeftIncome,0)>=ISNULL(@RightIncome,0) then ISNULL(@RightIncome,0) ELSE ISNULL(@LeftIncome,0) END
			Declare @FinalBV float=case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END
			--Declare @FinalTotalIncome float=case when ISNULL(@TotalLeftIncome,0)>=ISNULL(@TotalRightIncome,0) then ISNULL(@TotalRightIncome,0) ELSE ISNULL(@TotalLeftIncome,0) END
			Declare @FinalTotalBV float=case when ISNULL(@TotalLeftBV,0)>=ISNULL(@TotalRightBV,0) then ISNULL(@TotalRightBV,0) ELSE ISNULL(@TotalLeftBV,0) END
			
			Update #tmpUsers set MatchingBV=@FinalBV,TotalMatchingBV=@FinalTotalBV Where UserId=@TempUserId
			
		  FETCH NEXT FROM db_leader_match_bv INTO @TempUserId 
	END 

	CLOSE db_leader_match_bv  
	DEALLOCATE db_leader_match_bv 
	
	Select * from #tmpUsers
	
	drop table #tmpUsers
	
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_USER_WALLET_CREDIT_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_USER_WALLET_CREDIT_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INS_USER_WALLET_CREDIT_DETAILS]
(
@UserId bigint,
@UserPurchaseHdrId bigint,
@RefferalUserId bigint,
@ActionBy int
)
AS
BEGIN
	--Declare
	--@UserId bigint,
	--@UserPurchaseHdrId bigint,
	--@RefferalUserId bigint,
	--@ActionBy int
	
	Declare @WalletType int /* 1:Direct		2:Repurchase	3:FranchiseeReferral  */
	--Declare @UserId bigint=52
	--Declare @UserPurchaseHdrId bigint=134

	Select @WalletType=case when ISNULL(IsRepurchaseOrder,1)=1 then 2 ELSE 1 END  	
	from UserPurchaseHdr with(nolock)
	Where UserPurchaseHdrId=@UserPurchaseHdrId

	SELECT upd.UserPurchaseDtlId,DirectPurchasePerc as MatchingPerc,DirectPurchaseAmt,Qty,
			(DirectPurchaseAmt*Qty) as DirectPurchaseVolume,
			cast(((DirectPurchaseAmt*DirectPurchasePerc*Qty)/100) as numeric(18,0)) as CreditAmount
	into #tmpDirectBVDetails
	FROM UserPurchaseHdr as uph with(nolock)
	INNER JOIN UserPurchaseDtl as upd with(nolock) on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN ProductMst as pm with(nolock) on upd.ProductId=pm.ProductId
	Where uph.UserPurchaseHdrId=@UserPurchaseHdrId
	
	insert into WalletCreditDtl(UserId,UserPurchaseHdrId,CreditDate,CreditType,CreditAmount,IsActive,CreatedBy,CreatedDate)
	SELECT @RefferalUserId as UserId,@UserPurchaseHdrId,dbo.fn_GetServerDate() as CreditDate,@WalletType as CreditType,
			(SELECT SUM(CreditAmount) FROM #tmpDirectBVDetails) as CreditAmount,1 as IsActive,
			@ActionBy as CreatedBy,dbo.fn_GetServerDate() as CreatedDate
	
	Update upd 
	SET upd.DirectPurchaseVolume=bv.DirectPurchaseVolume
	FROM UserPurchaseDtl as upd with(nolock)
	INNER JOIN #tmpDirectBVDetails as bv on upd.UserPurchaseDtlId=bv.UserPurchaseDtlId
	
	IF @WalletType=1	--Direct
	BEGIN
		exec PROC_INS_USER_COMPANY_SHARE @UserPurchaseHdrId,@RefferalUserId,@ActionBy
	END
	
	insert into NotificationMst(NotificationTypeId,UserId,Notification,URL,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 1 as NotificationTypeId,@RefferalUserId as UserId,''You Got New Income In Your Wallet Check Now'' as [Notification],
			null as URL,1 as IsActive,@ActionBy as CreatedBy,dbo.fn_GetServerDate() as CreatedDate,
			@ActionBy as ModifiedBy,dbo.fn_GetServerDate() as ModifiedDate

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_MATCHING_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_MATCHING_INCOME_OF_USER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INSERT_MATCHING_INCOME_OF_USER]
(
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null,
@FromRowNo int=null,
@ToRowNo int=null
)
AS
BEGIN

IF @FromRowNo=0
BEGIN
	SET @FromRowNo=null
END
IF @ToRowNo=0
BEGIN
	SET @ToRowNo=null
END

Declare @TempUserId int
Declare @TempRegistrationDate datetime
Declare @LastCalculateDate datetime
Declare @TempFromDate datetime
Declare @Today datetime=dbo.fn_GetServerDate()
Declare @TotalLeftPoint numeric(18,0)
Declare @TotalRightPoint numeric(18,0)	
Declare @MatchingPoint numeric(18,0)

If @FromDate is null
BEGIN
	SET @FromDate=CAST(@Today AS DATE)
END
If @ToDate is null
BEGIN
	SET @ToDate=CAST(@Today AS DATE)
END

Declare @tblLeftSidePoints table(LeftPoints numeric(18,2),CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max))
Declare @tblRightSidePoints table(RightPoints numeric(18,2),CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max))

DECLARE db_cursor1 CURSOR FOR 
SELECT UserId,RegistrationDate FROM(
SELECT UserId,RegistrationDate,ROW_NUMBER() over(order by UserId asc) as RowNo FROM UserMst
Where UserId=case when @UserId=0 then UserId else @UserId end
) as u
WHERE RowNo>=(case when @FromRowNo is null then RowNo else @FromRowNo end)
	AND RowNo<=(case when @ToRowNo is null then RowNo else @ToRowNo end)
OPEN db_cursor1  
FETCH NEXT FROM db_cursor1 INTO @TempUserId,@TempRegistrationDate
WHILE @@FETCH_STATUS = 0  
BEGIN  	
	Select top 1 @LastCalculateDate=CreditDate
	from MatchingPointsCreditDtl 
	where UserId=@TempUserId
	order by MatchingPointsCreditDtlId desc
	
	IF  @LastCalculateDate is null
	BEGIN
		SET @TempFromDate=@TempRegistrationDate
	END
	ELSE
	BEGIN
		SET @TempFromDate=DATEADD(d,-1,@LastCalculateDate)
	END	
	
	SET @TempFromDate=cast(@TempFromDate as DATE)
	SET @ToDate=cast(@ToDate as DATE)
  
	--SELECT @TempChildUserId
	Declare @LastCarryForwardAmt numeric(18,0)
	Declare @LastCarryForwardPosition nvarchar(10)
	SET @LastCarryForwardAmt=null
	SET @LastCarryForwardPosition=null
	
	Select top 1 @LastCarryForwardAmt=CarryForwardPoints, @LastCarryForwardPosition=CarryForwardSide
	from MatchingPointsCreditDtl 
	where UserId=@TempUserId 
		AND CreditDate<@TempFromDate
	order by MatchingPointsCreditDtlId desc
	
	SET @LastCarryForwardAmt=ISNULL(@LastCarryForwardAmt,0)
	
	insert into @tblLeftSidePoints
	SELECT LeftPoints,CreditDate,FullName,RefferalCode from dbo.FN_GET_USER_LEFT_SIDE_POINTS(@TempUserId,@TempFromDate,@ToDate)
	
	insert into @tblRightSidePoints
	SELECT RightPoints,CreditDate,FullName,RefferalCode from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@TempUserId,@TempFromDate,@ToDate)
	
	WHILE @TempFromDate<=@ToDate
	BEGIN
		--Select @TempUserId ,@TempFromDate
		SET @TotalLeftPoint=0
		SET @TotalRightPoint=0
		SET @MatchingPoint=0
		
		SELECT @TotalLeftPoint = SUM(LeftPoints) FROM @tblLeftSidePoints where cast(CreditDate as DATE)=cast(@TempFromDate as DATE)
		SELECT @TotalRightPoint = SUM(RightPoints) FROM @tblRightSidePoints where cast(CreditDate as DATE)=cast(@TempFromDate as DATE)
		
		SET @TotalLeftPoint=ISNULL(@TotalLeftPoint,0)
		SET @TotalRightPoint=ISNULL(@TotalRightPoint,0)
		IF @LastCarryForwardPosition is not null
		BEGIN
			IF @LastCarryForwardPosition=''L''
			BEGIN
				SET @TotalLeftPoint=@TotalLeftPoint+@LastCarryForwardAmt
			END
			ELSE
			BEGIN
				SET @TotalRightPoint=@TotalRightPoint+@LastCarryForwardAmt
			END
		END
		
		IF @TotalLeftPoint>=@TotalRightPoint
		BEGIN
			SET @MatchingPoint=@TotalRightPoint
			SET @LastCarryForwardAmt=@TotalLeftPoint-@TotalRightPoint
			SET @LastCarryForwardPosition=''L''
		END
		ELSE
		BEGIN
			SET @MatchingPoint=@TotalLeftPoint
			SET @LastCarryForwardAmt=@TotalRightPoint-@TotalLeftPoint
			SET @LastCarryForwardPosition=''R''
		END
		
		IF NOT EXISTS(SELECT 1 from MatchingPointsCreditDtl where UserId=@TempUserId and CreditDate=@TempFromDate)
		BEGIN
			insert into MatchingPointsCreditDtl
			SELECT	@TempUserId as UserId,
					@TempFromDate as CreditDate,
					@TotalLeftPoint as LeftPoints,
					@TotalRightPoint as RightPoints,
					@MatchingPoint as MatchingPoints,
					@LastCarryForwardAmt as CarryForwardPoints,
					@LastCarryForwardPosition as CarryForwardSide,
					dbo.fn_GetServerDate() as CreatedDate
		END
		ELSE
		BEGIN
			UPDATE MatchingPointsCreditDtl
			SET	LeftPoints			=@TotalLeftPoint,
				RightPoints			=@TotalRightPoint  ,
				MatchingPoints		=@MatchingPoint  ,
				CarryForwardPoints	=@LastCarryForwardAmt  ,
				CarryForwardSide	=@LastCarryForwardPosition  ,
				CreatedDate			=dbo.fn_GetServerDate()
			Where  UserId=@TempUserId and CreditDate=@TempFromDate
		END
		SET @TempFromDate=@TempFromDate+1
	END
	
	DELETE FROM @tblLeftSidePoints
	DELETE FROM @tblRightSidePoints
	
	FETCH NEXT FROM db_cursor1 INTO @TempUserId,@TempRegistrationDate
END 

CLOSE db_cursor1  
DEALLOCATE db_cursor1 
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_MATCHING_INCOME]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_MATCHING_INCOME]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INSERT_MATCHING_INCOME]
(
@UserId int=0
)
AS
BEGIN
	Declare @TempUserId int
	Declare @MaxUserCnt int
	Declare @Cnt int=1
	Declare @Today datetime= cast(dbo.fn_GetServerDate() as date)

	Declare @tblUsers table(UserId int,RowNo int)
	insert into @tblUsers
	Select UserId,ROW_NUMBER() over(order by UserId)as RowNo 
	from UserMst 
	Where UserId=case when @UserId is null or @UserId=0 then UserId else @UserId END

	Select @MaxUserCnt=MAX(RowNo) from @tblUsers
	WHILE @Cnt<=@MaxUserCnt
	BEGIN
		SELECT @TempUserId=UserId FROM @tblUsers WHERE RowNo=@Cnt 
		
		insert into MatchingPointsCreditDtl
		SELECT tt.UserId,ISNULL(ri.CreditDate,li.CreditDate)CreditDate,ISNULL(li.LeftPoints,0)LeftPoints,ISNULL(ri.RightPoints,0)RightPoints,			
				case when ISNULL(li.LeftPoints,0)>=ISNULL(ri.RightPoints,0) then ISNULL(ri.RightPoints,0) else ISNULL(li.LeftPoints,0) end as MatchingPoints,
				case when ISNULL(li.LeftPoints,0)>=ISNULL(ri.RightPoints,0) 
					then ISNULL(li.LeftPoints,0)-ISNULL(ri.RightPoints,0) 
					else ISNULL(ri.RightPoints,0)-ISNULL(li.LeftPoints,0)
				end as CarryForwardPoints,
				case when ISNULL(li.LeftPoints,0)>=ISNULL(ri.RightPoints,0) 
					then ''L''
					else ''R''
				end as CarryForwardSide,dbo.fn_GetServerDate()
		FROM @tblUsers as tt	
		LEFT JOIN (	
			Select @TempUserId as UserId,SUM(LeftPoints) as LeftPoints,cast(CreditDate as date)CreditDate 
			from FN_GET_USER_LEFT_SIDE_POINTS(@TempUserId,default,default) 
			WHERE cast(CreditDate as date)<>@Today
			Group by cast(CreditDate as date)
		) as li on li.UserId=tt.UserId	
		LEFT JOIN (	
			Select @TempUserId as UserId,SUM(RightPoints) as RightPoints,cast(CreditDate as date)CreditDate 
			from FN_GET_USER_RIGHT_SIDE_POINTS(@TempUserId,default,default) 
			WHERE cast(CreditDate as date)<>@Today
			Group by cast(CreditDate as date)
		) as ri on ri.UserId=tt.UserId and li.UserId=tt.UserId  and li.CreditDate=ri.CreditDate
		LEFT JOIN MatchingPointsCreditDtl as mpc on  tt.UserId=mpc.UserId and ISNULL(ri.CreditDate,li.CreditDate)=mpc.CreditDate
		WHERE tt.UserId=@TempUserId and ISNULL(ri.CreditDate,li.CreditDate) is not null --(li.CreditDate is not null or ri.CreditDate is not null)
			AND mpc.CreditDate is null
			
		SET @Cnt=@Cnt+1
	END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN]
(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	SET FMTONLY OFF
	
	--Declare @FromDate date=''2019-09-20''
	--Declare @ToDate date=''2020-12-26''
	--Declare @UserId int=0
	
	Declare @TempUserId int

	SELECT * 
	into #tmpUsers
	FROM
	(
	Select	um.UserId,um.UserName,um.RefferalCode,um.FullName,
			ISNULL(SUM(case when (lupcd.CreditDate>=@FromDate or @FromDate is null) and (lupcd.CreditDate<=@ToDate or @ToDate is null) then CreditPoints end),0) as LeaderIncome,
			SUM(CreditPoints) as TotalLeaderIncome,0 as MatchingBV,0 as TotalMatchingBV,''Leader'' IncomeType	
	from dbo.[FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_ALL_DETAILS](1,@UserId) as lupcd	
	INNER JOIN UserMst as um on lupcd.UserId=um.UserId
	group by um.UserId,um.UserName,um.RefferalCode,um.FullName
	
	UNION
	
	Select	um.UserId,um.UserName,um.RefferalCode,um.FullName,
			ISNULL(SUM(case when (lupcd.CreditDate>=@FromDate or @FromDate is null) and (lupcd.CreditDate<=@ToDate or @ToDate is null) then CreditPoints end),0) as LeaderIncome,
			SUM(CreditPoints) as TotalLeaderIncome,0 as MatchingBV,0 as TotalMatchingBV,''Diamond'' IncomeType	
	from dbo.[FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS_ALL_DETAILS](1,@UserId,null,null) as lupcd	
	INNER JOIN UserMst as um on lupcd.UserId=um.UserId
	group by um.UserId,um.UserName,um.RefferalCode,um.FullName
	) as a
	
	DECLARE db_leader_match_bv CURSOR FOR 
	SELECT UserId 
	FROM #tmpUsers

	OPEN db_leader_match_bv  
	FETCH NEXT FROM db_leader_match_bv INTO @TempUserId  

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
			Declare @TotalLeftIncome float
			Declare @TotalLeftBV float
			Declare @LeftIncome float
			Declare @LeftBV float
			Declare @TotalRightIncome float
			Declare @TotalRightBV float
			Declare @RightIncome float
			Declare @RightBV float
			
			Select	@TotalLeftBV=SUM(LeftBV),
					@LeftBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftBV end)
			from FN_GET_USER_LEFT_SIDE_POINTS(@TempUserId,default,default)
			
			Select	@TotalRightBV=SUM(RightBV), 		
					@RightBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightBV end)		
			from FN_GET_USER_RIGHT_SIDE_POINTS(@TempUserId,default,default)
			
			Declare @FinalBV float=case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END
			Declare @FinalTotalBV float=case when ISNULL(@TotalLeftBV,0)>=ISNULL(@TotalRightBV,0) then ISNULL(@TotalRightBV,0) ELSE ISNULL(@TotalLeftBV,0) END
			
			Update #tmpUsers set MatchingBV=@FinalBV,TotalMatchingBV=@FinalTotalBV Where UserId=@TempUserId
			
		  FETCH NEXT FROM db_leader_match_bv INTO @TempUserId 
	END 

	CLOSE db_leader_match_bv  
	DEALLOCATE db_leader_match_bv 
	
	Select * from #tmpUsers
	
	drop table #tmpUsers
	
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_BONANZA_REPORT_BY_BV]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_BONANZA_REPORT_BY_BV]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[PROC_RPT_GET_BONANZA_REPORT_BY_BV] --52,4,2020
(
@UserId int,
@Month int,
@Year int
)
AS
BEGIN
	--Declare @UserId int=1
	--Declare @Month int=4
	--Declare @Year int=2019
	
	Declare @TempDate date=cast( (cast(@Year as nvarchar(max))+''-''+cast(@Month as nvarchar(max))+''-01'') as date)
	Declare @PreviousMonth int=case when @Month=4 and @Year=2020 then Month(DATEADD(M,-1,@TempDate)) else @Month end
	Declare @PreviousYear int=case when @Month=4 and @Year=2020 then Year(DATEADD(M,-1,@TempDate)) else @Year end

	
	Select ROW_NUMBER() over(order by CreditDate) as SrNo,* from(
	Select cast(CreditDate as date)CreditDate,FullName,RefferalCode,SUM(LeftBV)LeftBV,0 as RightBV 
	from [FN_GET_USER_LEFT_SIDE_POINTS](@UserId,default,default) 
	WHERE (Month(CreditDate) =@Month and Year(CreditDate)=@Year)
			or (Month(CreditDate) =@PreviousMonth and Year(CreditDate)=@PreviousYear)
	group by cast(CreditDate as date),FullName,RefferalCode
	
	UNION 
	
	Select cast(CreditDate as date)CreditDate,FullName,RefferalCode,0 asLeftBV,SUM(RightBV )RightBV 
	from [FN_GET_USER_RIGHT_SIDE_POINTS](@UserId,default,default)
	WHERE Month(CreditDate) =@Month and Year(CreditDate)=@Year
			or (Month(CreditDate) =@PreviousMonth and Year(CreditDate)=@PreviousYear)
	group by cast(CreditDate as date),FullName,RefferalCode
	) as totalbv
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN]
(
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
		
	Declare @PrimeMemberPerc float

	SELECT @PrimeMemberPerc=[Value] FROM ConfigMst Where [Key]=''PrimeMemberPerc''

	SELECT *,cast(((MatchingBV*@PrimeMemberPerc)/100) as bigint) as BV FROM(
		SELECT um.UserId,um.RefferalCode,um.FullName,dbo.FN_GET_USER_MATCHING_INCOME(t.UserId,@FromDate,@ToDate) as MatchingBV
		FROM (
			SELECT distinct FromCreditUserId as UserId
			FROM LeaderAndUplinePointsCreditDtl as lupc
			Where IsLeaderPoint=0
				and ((cast(lupc.CreditDate as date)>=@FromDate) or (@FromDate is null))
				and ((cast(lupc.CreditDate as date)<=@ToDate) or (@ToDate is null))

			UNION 
			
			SELECT distinct FromCreditUserId as UserId
			FROM DiamondAndSilverPointsCreditDtl as dspc
			Where IsDiamondPoint=0
				and ((cast(dspc.CreditDate as date)>=@FromDate) or (@FromDate is null))
				and ((cast(dspc.CreditDate as date)<=@ToDate) or (@ToDate is null))
		) as t
		INNER JOIN UserMst as um on t.UserId=um.UserId
	) as a

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INSERT_GOLD_SILVER_INCOME_OF_USER]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INSERT_GOLD_SILVER_INCOME_OF_USER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INSERT_GOLD_SILVER_INCOME_OF_USER]
(
@ActionedBy int,
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN
	EXEC PROC_INSERT_MATCHING_INCOME_OF_USER
	
	exec PROC_INSERT_GOLD_INCOME_OF_USER @ActionedBy,@UserId,@FromDate,@ToDate
	
	exec PROC_INSERT_SILVER_INCOME_OF_USER @ActionedBy,@UserId,@FromDate,@ToDate
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_INS_ORDER_CONFIRM_DETAILS]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_INS_ORDER_CONFIRM_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_INS_ORDER_CONFIRM_DETAILS]
(
@UserPurchaseHdrId bigint,
@ActionBy int,
@Msg nvarchar(max)=''''
)
AS
BEGIN
	--Declare
	--@UserPurchaseHdrId bigint,
	--@ActionBy int,
	--@Msg nvarchar(max)=''''


	Declare @UserId bigint
	Declare @RefferalUserId bigint
	Declare @RefferalRefferalUserId bigint
	Declare @IsRepurchaseOrder bit
	Declare @walletType int /* 1:Direct		2:Repurchase	3:FranchiseeReferral  */

	Select * 
	into #tblUserPurchaseHdr
	from UserPurchaseHdr
	Where @UserPurchaseHdrId=@UserPurchaseHdrId

	IF EXISTS(SELECT 1 FROM #tblUserPurchaseHdr)
	BEGIN
		IF (SELECT OrderStatusId FROM #tblUserPurchaseHdr )	=5
		BEGIN
			SET @Msg=''Order is already confirm!''
		END
		ELSE IF (SELECT ISNULL(OrderStatusId,1) FROM #tblUserPurchaseHdr)=1
		BEGIN
			Update UserPurchaseHdr
			SET OrderStatusId=5
			Where @UserPurchaseHdrId=@UserPurchaseHdrId
			
			SELECT top 1 @UserId=um.UserId,@RefferalUserId=um.RefferalUserId,@IsRepurchaseOrder=IsRepurchaseOrder,
					@WalletType=case when ISNULL(IsRepurchaseOrder,1)=1 then 2 ELSE 1 END  	
			FROM UserMst as um
			INNER JOIN #tblUserPurchaseHdr as uph on um.UserId=uph.UserId
			
			SELECT top 1 @RefferalRefferalUserId=um.RefferalUserId 
			FROM UserMst as um
			WHERE UserId=@RefferalUserId
					
			exec PROC_INS_USER_POINTS_ON_PURCHASE @UserId,@UserPurchaseHdrId
			
			exec PROC_INS_USER_NOTIFICATION @UserId,@UserPurchaseHdrId,1,@Msg,@ActionBy
			
			IF @IsRepurchaseOrder=0
			BEGIN
				Update UserMst
				SET IsRegistrationActivated=1,
					ActivateDate=dbo.fn_GetServerDate()
				WHERE UserId=@UserId
				
				IF @RefferalRefferalUserId<>0
				BEGIN
					exec PROC_INS_USER_WALLET_CREDIT_DETAILS @UserId,@UserPurchaseHdrId,@RefferalUserId,@ActionBy
				END
			END
		END
	END

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_TOTAL_BV]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_USER_TOTAL_BV]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[PROC_GET_USER_TOTAL_BV]
(
@UserId nvarchar(max)
)
AS
BEGIN
--Declare @UserId nvarchar(max)=''1,50,51,52,53,54,55,66,67,68,69,70''
	SELECT u.ID,cast(dbo.FN_GET_USER_MATCHING_BV(u.ID,null,null) as numeric(18,0)) as TotalBV 
	FROM [dbo].[FN_SPLIT](@UserId,'','') as u
	INNER JOIN USERMst as um on u.ID=um.UserId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH]    Script Date: 07/15/2020 17:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH]
(
@Month int,
@Year int
)
AS
BEGIN
--Declare @Month int=6
--Declare @Year int=2020

SET FMTONLY OFF

Declare @Date datetime=cast((cast(@Year as nvarchar(10))+''-''+cast(@Month as nvarchar(10))+''-01'') as date)

Declare @BonanzaOfferCnt float
Declare @BonanzaUserBVPreviousMonths float

SELECT @BonanzaOfferCnt=value FROM ConfigMst Where [Key]=''BonanzaUserBV''
SELECT @BonanzaUserBVPreviousMonths=value FROM ConfigMst Where [Key]=''BonanzaUserBVPreviousMonths''
--SET @BonanzaOfferCnt=5000

Declare @FromDate datetime
Declare @ToDate datetime

Select @FromDate=StartDate,@ToDate=EndDate from dbo.fn_GetFirstLastDateOfMonthByDate(@Date)

SELECT * 
into #Previous3rdMonth
FROM (
	SELECT *,dbo.FN_GET_USER_MATCHING_BV(UserId,DATEADD(M,-3,@FromDate),DATEADD(M,-3,@ToDate)) as Previous3rdMonthBV 
	FROM(
		SELECT UserId
		FROM UserMst Where (IsSilverMember=1 or IsGoldMember=1 or IsDimondMember=1)
	) as u
) as final
Where Previous3rdMonthBV>=@BonanzaUserBVPreviousMonths

SELECT * 
into #Previous2rdMonth
FROM (
	SELECT UserId,Previous3rdMonthBV,dbo.FN_GET_USER_MATCHING_BV(UserId,DATEADD(M,-2,@FromDate),DATEADD(M,-2,@ToDate)) as Previous2rdMonthBV
	FROM #Previous3rdMonth
) as final
Where Previous2rdMonthBV>=@BonanzaUserBVPreviousMonths


SELECT * 
into #PreviousMonth
FROM (
	SELECT UserId,Previous3rdMonthBV,Previous2rdMonthBV,dbo.FN_GET_USER_MATCHING_BV(UserId,DATEADD(M,-1,@FromDate),DATEADD(M,-1,@ToDate)) as PreviousMonthBV
	FROM #Previous2rdMonth
) as final
Where PreviousMonthBV>=@BonanzaUserBVPreviousMonths


SELECT final.*,um.RefferalCode,um.FullName,
		dbo.FN_GET_USER_MATCHING_BV(final.UserId,@FromDate,@ToDate) as CurrentMonthBV,
		cast(0 as bigint) as Amount
FROM (
SELECT *,
	(Select SUM(LeftBV) from dbo.FN_GET_USER_LEFT_SIDE_POINTS(UserId,@FromDate,@ToDate)) as CurrentMonthLeftBV,
	(Select SUM(RightBV) from dbo.FN_GET_USER_RIGHT_SIDE_POINTS(UserId,@FromDate,@ToDate)) as CurrentMonthRightBV
FROM #PreviousMonth
) as final
INNER JOIN UserMst as um on final.UserId=um.UserId
Where	CurrentMonthLeftBV>=@BonanzaOfferCnt AND 
		CurrentMonthRightBV>=@BonanzaOfferCnt

DROP TABLE #Previous3rdMonth
DROP TABLE #Previous2rdMonth
DROP TABLE #PreviousMonth
END' 
END
GO
