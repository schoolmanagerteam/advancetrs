﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRSImplementation;
using Elmah;
using TRS.Models;
using System.Text;
using System.Data;
using System.IO;
using OfficeOpenXml;
using TRS.Areas.Admin.Controllers;
using System.IO.Packaging;
using System.Globalization;

namespace TRS.Areas.Member.Controllers
{
    //[NoCache]
    public class TeamsController : clsBase
    {
        private const string UploadFilePath = "~/PayoutExcel/";
        private string WebsiteURL = string.Empty;
        private List<UserMst> listUserMst = null;
        private List<OrganizationDtl> listOrganizationDtl = null;

        // GET: Member/Teams
        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            DateTime now = (DateTime)db.PROC_GET_SERVER_DATE().FirstOrDefault();
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = now;
            ViewBag.StartDate = startDate.ToString("dd/MM/yyyy");
            ViewBag.EndDate = endDate.ToString("dd/MM/yyyy");

            return View();
        }

        public JsonResult GetDirectRefferal(string StartDate, string EndDate, string UserCode = "", int BVCondition = 0, int BVValue = 0, int UserStatus = -1)
        {
            List<PROC_GET_ALL_TEAM_MEMBER_LIST_Result> listUserMst = new List<PROC_GET_ALL_TEAM_MEMBER_LIST_Result>();
            //var DtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //var DtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var DtStartDate = objManager.ConvertDDMMYYToMMDDYY(StartDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
            var DtEndDate = objManager.ConvertDDMMYYToMMDDYY(EndDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

            try
            {
                listUserMst = db.PROC_GET_ALL_TEAM_MEMBER_LIST(objclsLogin.UserId, false, 0).Where(p => string.IsNullOrEmpty(UserCode) || p.RefferalCode == UserCode)
                                                                                  .Where(p => UserStatus == -1 || p.IsRegistrationActivated == ((UserStatus == 1) ? true : false))
                                                                                  .Where(p => BVValue == 0 || ((BVCondition == 1 && p.TotalBV > BVValue) || (BVCondition == 2 && p.TotalBV < BVValue)))
                                                                                  .ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listUserMst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBVDetail(int userId, string StartDate, string EndDate)
        {
            OrganizationUserDtl obj = new OrganizationUserDtl();
            try
            {
                var DtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var DtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var listRIGHTUsers = db.FN_GET_ALL_RIGHT_SIDED_USER_TREE(userId, false).ToList();
                var listLEFTUsers = db.FN_GET_ALL_LEFT_SIDED_USER_TREE(userId, false).ToList();
                var RightUserCnt = listRIGHTUsers.Count(i => i.IsRegistered == true);
                var LeftUserCnt = listLEFTUsers.Count(i => i.IsRegistered == true);
                var InActiveRightUserCnt = listRIGHTUsers.Count(i => i.IsRegistered == false);
                var InActiveLeftUserCnt = listLEFTUsers.Count(i => i.IsRegistered == false);

                obj.TotalDirectActive = db.UserMsts.Count(i => i.RefferalUserId == userId && (i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value));
                obj.TotalDirectInActive = db.UserMsts.Count(i => i.RefferalUserId == userId && (!i.IsRegistrationActivated.HasValue || (i.IsRegistrationActivated.HasValue && !i.IsRegistrationActivated.Value)));

                obj.TotalLeftActiveTeams = LeftUserCnt;
                obj.TotalLeftInActiveTeams = InActiveLeftUserCnt;
                obj.TotalRightActiveTeams = RightUserCnt;
                obj.TotalRightInActiveTeams = InActiveRightUserCnt;

                var objLeftPoints = db.PROC_GET_USER_LEFT_SIDE_POINTS(userId, null, null).ToList();
                var objRightPoints = db.PROC_GET_USER_RIGHT_SIDE_POINTS(userId, null, null).ToList();

                var ServerDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();

                obj.TotalLeftBV = Convert.ToInt64(objLeftPoints.Sum(i => i.LeftBV) ?? 0);
                obj.TotalRightBV = Convert.ToInt64(objRightPoints.Sum(i => i.RightBV) ?? 0);
                obj.TotalMonthLeftBV = Convert.ToInt64(objLeftPoints.Where(i => i.CreditDate.Value.Month == ServerDate.Value.Month && i.CreditDate.Value.Year == ServerDate.Value.Year).Sum(i => i.LeftBV) ?? 0);
                obj.TotalMonthRightBV = Convert.ToInt64(objRightPoints.Where(i => i.CreditDate.Value.Month == ServerDate.Value.Month && i.CreditDate.Value.Year == ServerDate.Value.Year).Sum(i => i.RightBV) ?? 0);

                obj.TotalLeftBVByDateRange = Convert.ToInt64(objLeftPoints.Where(i => i.CreditDate.Value.Date >= DtStartDate && i.CreditDate.Value.Date <= DtEndDate).Sum(i => i.LeftBV) ?? 0);
                obj.TotalRightBVByDateRange = Convert.ToInt64(objRightPoints.Where(i => i.CreditDate.Value.Date >= DtStartDate && i.CreditDate.Value.Date <= DtEndDate).Sum(i => i.RightBV) ?? 0);

                obj.DtStartDate = StartDate;
                obj.DtEndDate = EndDate;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetChildDirectRefferal(int userId)
        {
            List<PROC_GET_ALL_TEAM_MEMBER_LIST_Result> listUserMst = new List<PROC_GET_ALL_TEAM_MEMBER_LIST_Result>();
            try
            {
                listUserMst = db.PROC_GET_ALL_TEAM_MEMBER_LIST(userId, true, 0).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetChildDirectRefferal", listUserMst);
        }

        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Team()
        {
            WebsiteURL = db.ConfigMsts.Where(i => i.Key == "WebsiteURL").FirstOrDefault().Value;
            listUserMst = db.UserMsts.ToList();
            listOrganizationDtl = new List<OrganizationDtl>();

            var objParentUser = listUserMst.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
            listOrganizationDtl.Add(new OrganizationDtl
            {
                UserId = objParentUser.UserId,
                Name = objParentUser.FirstName,
                ParentUserId = 0,
                RefferalCode = objParentUser.RefferalCode,
                ImagePath = GetProfileImagePath(objParentUser.ProfileImage),
                RefferalUserId = objParentUser.RefferalUserId,
                RefferalName = GetDirectRefferedByUser(objParentUser),
                MobileNo = GetUserMobileNo(objParentUser),
                Position = GetUserPosition(objParentUser),
                IsActive = GetUserIsActive(objParentUser),
                //RightBV = objManager.GetRightBV(objParentUser.UserId),
                //LeftBV = objManager.GetLeftBV(objParentUser.UserId),
            });

            GetChildNodes(objParentUser);

            ViewBag.OrgDtl = listOrganizationDtl;
            return View();
        }

        //public double? GetRightBV(int UserId)
        //{
        //    double? BV = 0;
        //    try
        //    {
        //        var objRightPoints = db.PROC_GET_USER_RIGHT_SIDE_POINTS(UserId).ToList();
        //        BV = objRightPoints.Sum(i => i.RightBV);
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    return BV??0;
        //}
        //public double? GetLeftBV(int UserId)
        //{
        //    double? BV = 0;
        //    try
        //    {
        //        var objLeftPoints = db.PROC_GET_USER_LEFT_SIDE_POINTS(UserId).ToList();
        //        BV = objLeftPoints.Sum(i => i.LeftBV);
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    return BV??0;
        //}
        public void GetChildNodes(UserMst objUser)
        {
            try
            {
                if (listUserMst.Any(i => i.LeftUserId.HasValue && i.LeftUserId.Value != 0 && i.UserId == objUser.UserId))
                {
                    var objChildUser = listUserMst.Where(i => i.UserId == objUser.LeftUserId).FirstOrDefault();
                    listOrganizationDtl.Add(
                        new OrganizationDtl
                        {
                            UserId = objChildUser.UserId,
                            Name = objChildUser.FirstName,
                            ParentUserId = objUser.UserId,
                            RefferalCode = objChildUser.RefferalCode,
                            ImagePath = GetProfileImagePath(objChildUser.ProfileImage),
                            RefferalUserId = objChildUser.RefferalUserId,
                            RefferalName = GetDirectRefferedByUser(objChildUser),
                            MobileNo = GetUserMobileNo(objChildUser),
                            Position = GetUserPosition(objChildUser),
                            IsActive = GetUserIsActive(objChildUser),
                            //RightBV = objManager.GetRightBV(objChildUser.UserId),
                            //LeftBV = objManager.GetLeftBV(objChildUser.UserId),
                        });
                    GetChildNodes(objChildUser);
                }
                if (listUserMst.Any(i => i.RightUserId.HasValue && i.RightUserId.Value != 0 && i.UserId == objUser.UserId))
                {
                    var objChildUser = listUserMst.Where(i => i.UserId == objUser.RightUserId).FirstOrDefault();
                    listOrganizationDtl.Add(
                        new OrganizationDtl
                        {
                            UserId = objChildUser.UserId,
                            Name = objChildUser.FirstName,
                            ParentUserId = objUser.UserId,
                            RefferalCode = objChildUser.RefferalCode,
                            ImagePath = GetProfileImagePath(objChildUser.ProfileImage),
                            RefferalUserId = objChildUser.RefferalUserId,
                            RefferalName = GetDirectRefferedByUser(objChildUser),
                            MobileNo = GetUserMobileNo(objChildUser),
                            Position = GetUserPosition(objChildUser),
                            IsActive = GetUserIsActive(objChildUser),
                            //RightBV = objManager.GetRightBV(objChildUser.UserId),
                            //LeftBV = objManager.GetLeftBV(objChildUser.UserId),
                        });
                    GetChildNodes(objChildUser);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public string GetProfileImagePath(string Image)
        {
            var ProfileImage = "";
            if (!string.IsNullOrEmpty(Image))
            {
                ProfileImage = WebsiteURL + "/Images/ProfileImages/" + Image;
            }
            else
            {
                ProfileImage = WebsiteURL + "/Content/images/avatar/avatar-7.png";
            }
            return ProfileImage;
        }

        public string GetDirectRefferedByUser(UserMst objUser)
        {
            string RefferalName = "";
            var objRefferalUser = listUserMst.Where(i => i.UserId == objUser.RefferalUserId).FirstOrDefault();
            if (objRefferalUser != null)
            {
                RefferalName = objRefferalUser.FirstName;
            }
            return RefferalName;
        }

        public string GetUserMobileNo(UserMst objUser)
        {
            string MobileNo = "";
            MobileNo = objUser.MobileNo.HasValue ? objUser.MobileNo.ToString() : "";
            return MobileNo;
        }

        public string GetUserIsActive(UserMst objUser)
        {
            string IsActive = "";
            IsActive = objUser.IsRegistrationActivated.HasValue && objUser.IsRegistrationActivated.Value ? "Yes" : "No";
            return IsActive;
        }

        public string GetUserPosition(UserMst objUser)
        {
            string Position = "";
            if (!string.IsNullOrEmpty(objUser.Position))
            {
                Position = objUser.Position == "L" ? "Left" : "Right";
            }
            return Position;
        }

        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult MemberList()
        {
            var CurrentDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
            var CurrentYear = CurrentDate.Year;
            ViewBag.CurrentYear = CurrentDate.Year;
            ViewBag.CurrentMonth = CurrentDate.Month;
            List<SelectListItem> listYears = new List<SelectListItem>();
            while (CurrentYear >= 2019)
            {
                listYears.Add(new SelectListItem { Text = CurrentYear.ToString(), Value = CurrentYear.ToString() });
                CurrentYear--;
            }
            ViewBag.listYears = listYears;
            return View();
        }

        public JsonResult GetMemberList(int UserActiveType, string DisplayType, int Month, int Year, string UserCode)
        {
            var listUserMst = new List<PROC_GET_ALL_MEMBER_LIST_Result>();
            List<MemberListEnt> listMemberListEnt = new List<MemberListEnt>();
            try
            {
                listUserMst = db.PROC_GET_ALL_MEMBER_LIST(objclsLogin.UserId, Month, Year, DisplayType, UserCode, UserActiveType).ToList();

                //if (string.IsNullOrEmpty(UserCode) || string.IsNullOrWhiteSpace(UserCode))
                //{
                //    if (DisplayType == "A")
                //    {
                //        listUserMst = db.PROC_GET_ALL_MEMBER_LIST(objclsLogin.UserId).Where(i => (i.RegMonth == Month && i.RegYear == Year)).ToList();
                //    }
                //    else if (DisplayType == "L")
                //    {
                //        listUserMst = db.PROC_GET_ALL_MEMBER_LIST(objclsLogin.UserId).Where(i => i.Position == "L" && (i.RegMonth == Month && i.RegYear == Year)).ToList();
                //    }
                //    else if (DisplayType == "R")
                //    {
                //        listUserMst = db.PROC_GET_ALL_MEMBER_LIST(objclsLogin.UserId).Where(i => i.Position == "R" && (i.RegMonth == Month && i.RegYear == Year)).ToList();
                //    }
                //}
                //else
                //{
                //    listUserMst = db.PROC_GET_ALL_MEMBER_LIST(objclsLogin.UserId).Where(i => i.RefferalCode.Trim() == UserCode.Trim()).ToList();
                //}

                //if (UserActiveType != -1)
                //{
                //    if(UserActiveType == 1)
                //        listUserMst = listUserMst.Where(p => p.IsRegistered == true).ToList();
                //    else
                //        listUserMst = listUserMst.Where(p => p.IsRegistered == false).ToList();
                //}

                foreach (var item in listUserMst)
                {
                    listMemberListEnt.Add(new MemberListEnt
                    {
                        IsRegistered = item.IsRegistered,
                        //LeftBV= objManager.GetLeftBV(item.UserId.Value),
                        //RightBV = objManager.GetRightBV(item.UserId.Value),
                        LeftBV = 0,
                        RightBV = 0,
                        Name = item.Name,
                        Position = item.Position,
                        RefferalCode = item.RefferalCode,
                        RefferalName = item.RefferalName,
                        RefferalRefferalCode = item.RefferalRefferalCode,
                        UserId = item.UserId
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            //return PartialView("_GetMemberList", listMemberListEnt);
            return Json(listMemberListEnt, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult MyGeneology()
        {
            List<string> listConfigs = new List<string>();
            listConfigs.Add(ConfigKeys.DefaultLevelLoadInGeneology);
            listConfigs.Add(ConfigKeys.ChildLevelLoadInGeneology);

            var listConfigDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();
            ViewBag.DefaultLevelLoadInGeneology = objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.DefaultLevelLoadInGeneology);
            ViewBag.ChildLevelLoadInGeneology = objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.ChildLevelLoadInGeneology);
            //var lst = db.PROC_GET_ALL_USER_HIERARCHY(objclsLogin.UserId, true).ToList();
            ViewBag.UserId = objclsLogin.UserId;

            return View();
        }

        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public PartialViewResult GetHierarchyTeamList(int? UserId, int UserLevel, string UserCode = "")
        {
            var HIERARCHYUserId = 0;
            List<PROC_GET_ALL_USER_HIERARCHY_Result> listChilds = null;
            if (UserId.HasValue) { HIERARCHYUserId = UserId.Value; }
            else if (!string.IsNullOrEmpty(UserCode))
            {
                var objUserMst = db.UserMsts.FirstOrDefault(i => i.RefferalCode == UserCode);
                HIERARCHYUserId = objUserMst != null ? objUserMst.UserId : 0;
            }
            else { HIERARCHYUserId = objclsLogin.UserId; }
            if (UserId.HasValue)
            {
                listChilds = db.PROC_GET_ALL_USER_HIERARCHY(HIERARCHYUserId, true).Where(i => i.UserLevel <= UserLevel).ToList();
            }

            ViewBag.UserId = HIERARCHYUserId;
            if (listChilds != null && listChilds.Count > 0)
            {
                StringBuilder strUserId = new StringBuilder();
                var arUser = listChilds.Select(i => i.UserId).ToArray();
                foreach (var item in arUser)
                {
                    if (strUserId.Length == 0)
                    {
                        strUserId.Append(item);
                    }
                    else
                    {
                        strUserId.Append("," + item);
                    }
                }

                var listUserBV = db.PROC_GET_USER_TOTAL_BV(strUserId.ToString()).ToList();

                listChilds.ForEach(i =>
                {
                    var objUserBV = listUserBV.FirstOrDefault(x => x.ID == i.UserId.ToString());
                    i.BV = objUserBV != null ? Convert.ToDouble(objUserBV.TotalBV) : 0;
                });

                ViewBag.MaxLevel = listChilds.Max(i => i.UserLevel);
            }
            else
            {
                ViewBag.MaxLevel = 1;
            }

            return PartialView("_Geneology", listChilds);
        }

        public JsonResult CheckSearchUserIsExistInHierarchy(string UserCode)
        {
            var obj = db.PROC_GET_ALL_USER_HIERARCHY(objclsLogin.UserId, true).FirstOrDefault(i => i.RefferalCode == UserCode);
            var IsExist = false;
            var UserId = 0;
            if (obj != null)
            {
                IsExist = true;
                UserId = obj.UserId;
            }
            return Json(new { UserId = UserId, IsExist = IsExist });
        }

        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public PartialViewResult GeneologyUserDetails(int? UserId)
        {
            OrganizationUserDtl obj = new OrganizationUserDtl();

            var listRIGHTUsers = db.FN_GET_ALL_RIGHT_SIDED_USER_TREE(UserId, false).ToList();
            var listLEFTUsers = db.FN_GET_ALL_LEFT_SIDED_USER_TREE(UserId, false).ToList();
            var RightUserCnt = listRIGHTUsers.Count(i => i.IsRegistered == true);
            var LeftUserCnt = listLEFTUsers.Count(i => i.IsRegistered == true);
            var InActiveRightUserCnt = listRIGHTUsers.Count(i => i.IsRegistered == false);
            var InActiveLeftUserCnt = listLEFTUsers.Count(i => i.IsRegistered == false);

            obj.TotalDirectActive = db.UserMsts.Count(i => i.RefferalUserId == UserId && (i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value));
            obj.TotalDirectInActive = db.UserMsts.Count(i => i.RefferalUserId == UserId && (!i.IsRegistrationActivated.HasValue || (i.IsRegistrationActivated.HasValue && !i.IsRegistrationActivated.Value)));

            obj.TotalLeftActiveTeams = LeftUserCnt;
            obj.TotalLeftInActiveTeams = InActiveLeftUserCnt;
            obj.TotalRightActiveTeams = RightUserCnt;
            obj.TotalRightInActiveTeams = InActiveRightUserCnt;

            var objLeftPoints = db.PROC_GET_USER_LEFT_SIDE_POINTS(UserId, null, null).ToList();
            var objRightPoints = db.PROC_GET_USER_RIGHT_SIDE_POINTS(UserId, null, null).ToList();

            var ServerDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();

            obj.TotalLeftBV = Convert.ToInt64(objLeftPoints.Sum(i => i.LeftBV) ?? 0);
            obj.TotalRightBV = Convert.ToInt64(objRightPoints.Sum(i => i.RightBV) ?? 0);
            obj.TotalMonthLeftBV = Convert.ToInt64(objLeftPoints.Where(i => i.CreditDate.Value.Month == ServerDate.Value.Month && i.CreditDate.Value.Year == ServerDate.Value.Year).Sum(i => i.LeftBV) ?? 0);
            obj.TotalMonthRightBV = Convert.ToInt64(objRightPoints.Where(i => i.CreditDate.Value.Month == ServerDate.Value.Month && i.CreditDate.Value.Year == ServerDate.Value.Year).Sum(i => i.RightBV) ?? 0);

            return PartialView("_GeneologyUserDetails", obj);
        }

        public ActionResult MyTeam()
        {
            return View();
        }

        public class OrganizationDtl
        {
            public int UserId { get; set; }
            public int ParentUserId { get; set; }
            public string Name { get; set; }
            public string RefferalCode { get; set; }
            public string ImagePath { get; set; }
            public int RefferalUserId { get; set; }
            public string RefferalName { get; set; }
            public string MobileNo { get; set; }
            public string Position { get; set; }
            public string IsActive { get; set; }
            public double? RightBV { get; set; }
            public double? LeftBV { get; set; }
            public int UserLevel { get; set; }
        }

        //public class MemberListEnt
        //{
        //    public string Name { get; set; }
        //    public string RefferalCode { get; set; }
        //    public string RefferalName { get; set; }
        //    public string IsActive { get; set; }
        //}

        public class OrganizationUserDtl
        {
            public Int64 TotalRightBV { get; set; }
            public Int64 TotalLeftBV { get; set; }
            public Int64 TotalMonthRightBV { get; set; }
            public Int64 TotalMonthLeftBV { get; set; }
            public int TotalLeftActiveTeams { get; set; }
            public int TotalRightActiveTeams { get; set; }
            public int TotalLeftInActiveTeams { get; set; }
            public int TotalRightInActiveTeams { get; set; }
            public int TotalDirectActive { get; set; }
            public int TotalDirectInActive { get; set; }
            public Int64 TotalLeftBVByDateRange { get; set; }
            public Int64 TotalRightBVByDateRange { get; set; }

            public string DtStartDate { get; set; }
            public string DtEndDate { get; set; }
        }

        public ActionResult ExportDirectRefferal(string StartDate, string EndDate, string UserCode = "", int BVCondition = 0, int BVValue = 0, int UserStatus = -1)
        {
            var DtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var DtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (!Directory.Exists(Server.MapPath(UploadFilePath)))
                Directory.CreateDirectory(Server.MapPath(UploadFilePath));

            string FName = string.Format("DirectRefferal_{0}.xlsx", DateTime.Now.Ticks);
            string FnamePath = Server.MapPath(UploadFilePath) + FName;

            FileInfo fInfor = new FileInfo(FnamePath);

            List<PROC_GET_ALL_TEAM_MEMBER_LIST_Result> listUserMst = new List<PROC_GET_ALL_TEAM_MEMBER_LIST_Result>();
            listUserMst = db.PROC_GET_ALL_TEAM_MEMBER_LIST(objclsLogin.UserId, false, 0).Where(p => string.IsNullOrEmpty(UserCode) || p.RefferalCode == UserCode)
                                                                                              .Where(p => UserStatus == -1 || p.IsRegistrationActivated == ((UserStatus == 1) ? true : false))
                                                                                              .Where(p => BVValue == 0 || ((BVCondition == 1 && p.TotalBV > BVValue) || (BVCondition == 2 && p.TotalBV < BVValue)))
                                                                                              .ToList();

            ExcelPackage excel = Utilities.GetExcelPackageForExport(listUserMst, new List<string> { "UserId", "RefferalUserId", "TotalBV", "TotalLeftBV", "TotalRightBV" }, "DirectRefferal", false);

            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", string.Format("attachment;filename=DirectRefferal_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            return View();
        }

        [HttpGet]
        [DeleteFileAttribute] //Action Filter, it will auto delete the file after download,
        public ActionResult Download(string file)
        {
            string fullPath = Server.MapPath(file);

            return File(fullPath, "application/vnd.ms-excel", Path.GetFileName(fullPath));
        }
    }
}