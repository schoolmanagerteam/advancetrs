﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Member.Controllers
{
    //[NoCache]
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class OrdersController : clsBase
    {
        // GET: Member/Orders
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult Index()
        {
            List<PROC_GET_USER_ORDER_HDR_Result> listOrders = new List<PROC_GET_USER_ORDER_HDR_Result>();
            try
            {
                listOrders = db.PROC_GET_USER_ORDER_HDR(objclsLogin.UserId, 0).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listOrders);
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult LoadProductDetails(int OrderPurchaseHdrId)
        {
            var listOrderDtl = db.PROC_GET_USER_ORDER_DTL(OrderPurchaseHdrId).ToList();
            if (listOrderDtl.Count > 0)
            {
                if (objclsLogin.UserType == UserType.Franchisee)
                {
                    if (listOrderDtl.FirstOrDefault().FranchiseeId != null && listOrderDtl.FirstOrDefault().FranchiseeId == objclsLogin.FranchiseeId)
                    {
                        return PartialView("~/Views/Shared/_ProductPurchaseDetails.cshtml", listOrderDtl);
                    }
                    else
                    {
                        return Json("You Are Not Authorized To View Order Details!");
                    }
                }
                else if (objclsLogin.UserType == UserType.Member)
                {
                    //if (listOrderDtl.FirstOrDefault().UserId == objclsLogin.UserId)
                    //{
                        return PartialView("~/Views/Shared/_ProductPurchaseDetails.cshtml", listOrderDtl);
                    //}
                    //else
                    //{
                    //    return Json("You Are Not Authorized To View Order Details!");
                    //}
                }
                else if (objclsLogin.UserType == UserType.Admin || objclsLogin.UserType == UserType.Office)
                {
                    return PartialView("~/Views/Shared/_ProductPurchaseDetails.cshtml", listOrderDtl);
                }
                else
                {
                    return Json("You Are Not Authorized To View Order Details!");
                }
            }
            else
            {
                return Json("No Order Details Found!");
            }
        }

        //[OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        //public ActionResult LoadPointProductDetails(int PointsPurchaseHdrId)
        //{
        //    var listOrderDtl = db.PROC_GET_USER_POINT_PURCHASE_ORDER_DTL(PointsPurchaseHdrId).ToList();
        //    if (listOrderDtl.Count > 0)
        //    {
        //        if (objclsLogin.UserType == UserType.Franchisee)
        //        {
        //            if (listOrderDtl.FirstOrDefault().FranchiseeId != null && listOrderDtl.FirstOrDefault().FranchiseeId == objclsLogin.FranchiseeId)
        //            {
        //                return PartialView("~/Views/Shared/_PointProductPurchaseDetails.cshtml", listOrderDtl);
        //            }
        //            else
        //            {
        //                return Json("You Are Not Authorized To View Order Details!");
        //            }
        //        }
        //        else if (objclsLogin.UserType == UserType.Member)
        //        {
        //            if (listOrderDtl.FirstOrDefault().UserId == objclsLogin.UserId)
        //            {
        //                return PartialView("~/Views/Shared/_PointProductPurchaseDetails.cshtml", listOrderDtl);
        //            }
        //            else
        //            {
        //                return Json("You Are Not Authorized To View Order Details!");
        //            }
        //        }
        //        else if (objclsLogin.UserType == UserType.Admin)
        //        {
        //            return PartialView("~/Views/Shared/_PointProductPurchaseDetails.cshtml", listOrderDtl);
        //        }
        //        else
        //        {
        //            return Json("You Are Not Authorized To View Order Details!");
        //        }
        //    }
        //    else
        //    {
        //        return Json("No Order Details Found!");
        //    }
        //}
    }
}