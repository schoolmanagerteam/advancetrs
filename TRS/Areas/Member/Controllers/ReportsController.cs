﻿using Elmah;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TRS.Models;
using TRSImplementation;

namespace TRS.Areas.Member.Controllers
{
    public class ReportsController : clsBase
    {
        private const string UploadFilePath = "~/PayoutExcel/";

        // GET: Member/Reports
        [SessionExpireFilter]
        public ActionResult DirectIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeDirectIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT_Result> listDirectAmount = new List<PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_DIRECT_REFFERAL_AMOUNT(objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeDirectIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult VoltIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeVoltIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_COMPANY_SHARE_AMOUNT_Result> listDirectAmount = new List<PROC_RPT_GET_COMPANY_SHARE_AMOUNT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_COMPANY_SHARE_AMOUNT(objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeVoltIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult BonanzaDtl()
        {
            var listMonthNames = GetUserMonthYear();
            return View(listMonthNames);
        }

        public PartialViewResult GetMonthWiseBonanzaDtl(string MonthYear)
        {
            List<PROC_RPT_GET_BONANZA_REPORT_BY_BV_Result> listBonanzaDtl = new List<PROC_RPT_GET_BONANZA_REPORT_BY_BV_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);

                ViewBag.BonanzaOfferCnt = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.BonanzaOfferCnt).FirstOrDefault().Value;
                listBonanzaDtl = db.PROC_RPT_GET_BONANZA_REPORT_BY_BV(objclsLogin.UserId, Convert.ToInt32(Month), Convert.ToInt32(Year)).ToList();
                //ViewBag.MemberCnt = listBonanzaDtl.Count;
                var LeftBV = listBonanzaDtl.Sum(i => i.LeftBV);
                var RightBV = listBonanzaDtl.Sum(i => i.RightBV);
                ViewBag.MemberCnt = (LeftBV.HasValue ? LeftBV.Value : 0) >= (RightBV.HasValue ? RightBV.Value : 0)
                                    ? (RightBV.HasValue ? RightBV.Value : 0)
                                    : (LeftBV.HasValue ? LeftBV.Value : 0);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseBonanzaDtl", listBonanzaDtl);
        }

        [SessionExpireFilter]
        public ActionResult RightIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeRightIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_RIGHT_SIDE_EARNING_Result> listDirectAmount = new List<PROC_RPT_GET_RIGHT_SIDE_EARNING_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_RIGHT_SIDE_EARNING(objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeRightIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult LeftIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeLeftIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_LEFT_SIDE_EARNING_Result> listDirectAmount = new List<PROC_RPT_GET_LEFT_SIDE_EARNING_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_LEFT_SIDE_EARNING(objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeLeftIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult MatchingIncome()
        {
            var listMonthNames = GetUserMonthYear();
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public ActionResult GetMonthWeekDtl(string MonthYear)
        {
            List<WeekNames> listWeekNames = new List<WeekNames>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, 0).Select(i => new WeekNames { FromDate = i.StartDate.Value, ToDate = i.EndDate.Value, WeekName = i.StartDate.Value.ToString("dd/MM/yyyy") + " to " + i.EndDate.Value.ToString("dd/MM/yyyy"), WeekNo = i.WeekNo.Value }).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listWeekNames, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public PartialViewResult GetMonthWiseMatchingIncome(string MonthYear, int WeekNo)
        {
            List<MatchingIncomeDtl> listMatchingIncomeDtl = new List<MatchingIncomeDtl>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, WeekNo).ToList();
                var StartDate = listWeekNames.FirstOrDefault().StartDate;
                var EndDate = listWeekNames.LastOrDefault().EndDate;
                var listLeftIncome = db.PROC_GET_USER_LEFT_SIDE_POINTS(objclsLogin.UserId, null, null).ToList();
                var listRightIncome = db.PROC_GET_USER_RIGHT_SIDE_POINTS(objclsLogin.UserId, null, null).ToList();
                var TotalLeftBV = listLeftIncome.Sum(i => i.LeftBV);
                var TotalRightBV = listRightIncome.Sum(i => i.RightBV);
                var Today = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

                var TotalLeftBVUptoPrevMonth = listLeftIncome.Where(i => i.CreditDate < StartDate).Sum(i => i.LeftBV);
                var TotalRightBVUptoPrevMonth = listRightIncome.Where(i => i.CreditDate < StartDate).Sum(i => i.RightBV);
                var CarryForwardBVPrevMonth = ((TotalLeftBVUptoPrevMonth.HasValue ? TotalLeftBVUptoPrevMonth.Value : 0) >= (TotalRightBVUptoPrevMonth.HasValue ? TotalRightBVUptoPrevMonth.Value : 0)
                                            ? (TotalLeftBVUptoPrevMonth.HasValue ? TotalLeftBVUptoPrevMonth.Value : 0) - (TotalRightBVUptoPrevMonth.HasValue ? TotalRightBVUptoPrevMonth.Value : 0)
                                            : (TotalRightBVUptoPrevMonth.HasValue ? TotalRightBVUptoPrevMonth.Value : 0) - (TotalLeftBVUptoPrevMonth.HasValue ? TotalLeftBVUptoPrevMonth.Value : 0));
                var CarryForwardBVSidePrevMonth = (TotalLeftBVUptoPrevMonth.HasValue ? TotalLeftBVUptoPrevMonth.Value : 0) >= (TotalRightBVUptoPrevMonth.HasValue ? TotalRightBVUptoPrevMonth.Value : 0)
                                             ? "L"
                                             : "R";

                ViewBag.TotalLeftBV = (TotalLeftBV.HasValue ? TotalLeftBV.Value : 0);
                ViewBag.TotalRightBV = (TotalRightBV.HasValue ? TotalRightBV.Value : 0);
                ViewBag.TotalMatchingBV = ((TotalLeftBV.HasValue ? TotalLeftBV.Value : 0) >= (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                                            ? (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                                            : (TotalLeftBV.HasValue ? TotalLeftBV.Value : 0));
                ViewBag.CarryForwardBV = ((TotalLeftBV.HasValue ? TotalLeftBV.Value : 0) >= (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                                            ? (TotalLeftBV.HasValue ? TotalLeftBV.Value : 0) - (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                                            : (TotalRightBV.HasValue ? TotalRightBV.Value : 0) - (TotalLeftBV.HasValue ? TotalLeftBV.Value : 0));

                ViewBag.CarryForwardBVSide = (TotalLeftBV.HasValue ? TotalLeftBV.Value : 0) >= (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                                            ? "Left"
                                            : "Right";
                foreach (var week in listWeekNames)
                {
                    var CurrentWeekLeftPoints = listLeftIncome.Where(i => i.CreditDate >= week.StartDate && i.CreditDate <= week.EndDate).ToList().Sum(i => i.LeftPoints);
                    var CurrentWeekRightPoints = listRightIncome.Where(i => i.CreditDate >= week.StartDate && i.CreditDate <= week.EndDate).ToList().Sum(i => i.RightPoints);
                    var MatchingIncome = ((CurrentWeekLeftPoints.HasValue ? CurrentWeekLeftPoints.Value : 0) >= (CurrentWeekRightPoints.HasValue ? CurrentWeekRightPoints.Value : 0)
                                            ? (CurrentWeekRightPoints.HasValue ? CurrentWeekRightPoints.Value : 0)
                                            : (CurrentWeekLeftPoints.HasValue ? CurrentWeekLeftPoints.Value : 0));

                    var CurrentWeekLeftBV = listLeftIncome.Where(i => i.CreditDate >= week.StartDate && i.CreditDate <= week.EndDate).ToList().Sum(i => i.LeftBV);
                    var CurrentWeekRightBV = listRightIncome.Where(i => i.CreditDate >= week.StartDate && i.CreditDate <= week.EndDate).ToList().Sum(i => i.RightBV);

                    if (CarryForwardBVSidePrevMonth == "L")
                    {
                        CurrentWeekLeftBV += CarryForwardBVPrevMonth;
                    }
                    else
                    {
                        CurrentWeekRightBV += CarryForwardBVPrevMonth;
                    }

                    var CurrentWeekMatchingBV = ((CurrentWeekLeftBV.HasValue ? CurrentWeekLeftBV.Value : 0) >= (CurrentWeekRightBV.HasValue ? CurrentWeekRightBV.Value : 0)
                                            ? (CurrentWeekRightBV.HasValue ? CurrentWeekRightBV.Value : 0)
                                            : (CurrentWeekLeftBV.HasValue ? CurrentWeekLeftBV.Value : 0));

                    var CurrentWeekCarryForwardBV = ((CurrentWeekLeftBV.HasValue ? CurrentWeekLeftBV.Value : 0) >= (CurrentWeekRightBV.HasValue ? CurrentWeekRightBV.Value : 0)
                                            ? (CurrentWeekLeftBV.HasValue ? CurrentWeekLeftBV.Value : 0) - (CurrentWeekRightBV.HasValue ? CurrentWeekRightBV.Value : 0)
                                            : (CurrentWeekRightBV.HasValue ? CurrentWeekRightBV.Value : 0) - (CurrentWeekLeftBV.HasValue ? CurrentWeekLeftBV.Value : 0));

                    CarryForwardBVPrevMonth = CurrentWeekCarryForwardBV;
                    CarryForwardBVSidePrevMonth = (CurrentWeekLeftBV.HasValue ? CurrentWeekLeftBV.Value : 0) >= (CurrentWeekRightBV.HasValue ? CurrentWeekRightBV.Value : 0)
                                            ? "L"
                                            : "R";

                    listMatchingIncomeDtl.Add(new MatchingIncomeDtl
                    {
                        FromDate = week.StartDate.Value,
                        LeftIncome = Today >= week.StartDate.Value ? Convert.ToInt64(CurrentWeekLeftPoints.HasValue ? CurrentWeekLeftPoints.Value : 0) : 0,
                        MatchingIncome = Convert.ToInt64(CurrentWeekMatchingBV * 80 / 100),
                        RightIncome = Today >= week.StartDate.Value ? Convert.ToInt64(CurrentWeekRightPoints.HasValue ? CurrentWeekRightPoints.Value : 0) : 0,
                        ToDate = week.EndDate.Value,
                        WeekName = week.StartDate.Value.ToString("dd/MM/yyyy") + " to " + week.EndDate.Value.ToString("dd/MM/yyyy"),
                        WeekNo = week.WeekNo.Value,
                        MonthYear = MonthYear,
                        LeftBV = Today >= week.StartDate.Value ? Convert.ToInt64(CurrentWeekLeftBV.HasValue ? CurrentWeekLeftBV.Value : 0) : 0,
                        RightBV = Today >= week.StartDate.Value ? Convert.ToInt64(CurrentWeekRightBV.HasValue ? CurrentWeekRightBV.Value : 0) : 0,
                        MatchingBV = Today >= week.StartDate.Value ? Convert.ToInt64(CurrentWeekMatchingBV) : 0,
                        CarryForwardBV = Today >= week.StartDate.Value ? Convert.ToInt64(CurrentWeekCarryForwardBV) : 0,
                        CarryForwardBVSide = (CarryForwardBVSidePrevMonth == "L" ? " Left" : " Right")
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseMatchingIncome", listMatchingIncomeDtl);
        }

        public PartialViewResult GetIncomeByUser(string MonthYear, int WeekNo, string Position)
        {
            List<IncomeUserEnt> listIncomeUserEnt = new List<IncomeUserEnt>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, WeekNo).ToList();
                var StartDate = listWeekNames.FirstOrDefault().StartDate;
                var EndDate = listWeekNames.LastOrDefault().EndDate;
                int SrNo = 1;
                if (Position == "L")
                {
                    var listLeftIncome = db.PROC_GET_USER_LEFT_SIDE_POINTS(objclsLogin.UserId, null, null).Where(i => i.CreditDate >= StartDate && i.CreditDate <= EndDate).ToList();
                    listLeftIncome.ForEach(i =>
                    {
                        listIncomeUserEnt.Add(new IncomeUserEnt
                        {
                            FullName = i.FullName,
                            IncomeAmt = i.LeftPoints.HasValue ? i.LeftPoints.Value : 0,
                            RefferalCode = i.RefferalCode,
                            SrNo = SrNo,
                            BV = i.LeftBV
                        });
                        SrNo++;
                    });
                }
                else
                {
                    var listRightIncome = db.PROC_GET_USER_RIGHT_SIDE_POINTS(objclsLogin.UserId, null, null).Where(i => i.CreditDate >= StartDate && i.CreditDate <= EndDate).ToList();
                    listRightIncome.ForEach(i =>
                    {
                        listIncomeUserEnt.Add(new IncomeUserEnt
                        {
                            FullName = i.FullName,
                            IncomeAmt = i.RightPoints.HasValue ? i.RightPoints.Value : 0,
                            RefferalCode = i.RefferalCode,
                            SrNo = SrNo,
                            BV = i.RightBV
                        });
                        SrNo++;
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetIncomeByUser", listIncomeUserEnt);
        }

        [SessionExpireFilter]
        public ActionResult PayoutDtl()
        {
            var objPayoutDtl = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(objclsLogin.UserId, true).FirstOrDefault();
            ViewBag.TotalPayout = objPayoutDtl.TotalPayout;
            ViewBag.PendingPayout = objPayoutDtl.NetPayable;
            var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
            ViewBag.PreviousDate = serverDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetMonthWisePayoutDtl(string FromDate, string ToDate)
        {
            List<WalletDebitDtl> listWalletDebitDtls = new List<WalletDebitDtl>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listWalletDebitDtls = db.WalletDebitDtls.Where(i => i.DebitDate >= dtFromDate && i.DebitDate <= dtToDate && i.UserId == objclsLogin.UserId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWisePayoutDtl", listWalletDebitDtls);
        }

        public ActionResult ExportGetMonthWisePayoutDtl(string FromDate, string ToDate)
        {
            List<WalletDebitDtl> listWalletDebitDtls = new List<WalletDebitDtl>();
            List<WalletDebitDtlExtra> listWalletDebitDtlsExtra = new List<WalletDebitDtlExtra>();

            var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
            var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

            if (!Directory.Exists(Server.MapPath(UploadFilePath)))
                Directory.CreateDirectory(Server.MapPath(UploadFilePath));

            string FName = string.Format("PayoutDtl_{0}.xlsx", DateTime.Now.Ticks);
            string FnamePath = Server.MapPath(UploadFilePath) + FName;

            FileInfo fInfor = new FileInfo(FnamePath);

            listWalletDebitDtls = db.WalletDebitDtls.Where(i => i.DebitDate >= dtFromDate && i.DebitDate <= dtToDate && i.UserId == objclsLogin.UserId).ToList();

            foreach (var obj in listWalletDebitDtls)
            {
                listWalletDebitDtlsExtra.Add(new WalletDebitDtlExtra
                {
                    DirectIncome = obj.DirectIncomePay
                    ,
                    MatchingIncome = obj.PointsIncomePay
                    ,
                    OtherIncome = (obj.SilverIncomePay + obj.LeaderIncomePay + obj.UplineSupportIncomePay + obj.UplineMonthlyPay + obj.RetailRewardMonthlyPay)
                    ,
                    DebitAmount = obj.DebitAmount
                    ,
                    VoltIncome = obj.VoltIncomePay
                    ,
                    TDSAmount = obj.TDSAmount
                    ,
                    TDSPerc = obj.TDSPerc
                    ,
                    AdminCharge = obj.AdminCharge
                    ,
                    AdminPerc = obj.AdminPerc
                    ,
                    NetPayable = obj.NetPayable
                    ,
                    DebitDate = obj.DebitDate
                    ,
                    UTRRefNo = obj.UTRRefNo
                });
            }

            ExcelPackage excel = Utilities.GetExcelPackageForExport(listWalletDebitDtlsExtra, new List<string> { "UplineSupportIncomePay", "LeaderIncomePay", "SilverIncomePay", "UplineMonthlyPay", "RetailRewardMonthlyPay" }, "PayoutDtl", false);

            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", string.Format("attachment;filename=PayoutDtl_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult LeaderIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeLeaderIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_GOLD_INCOME_AMOUNT_Result> listDirectAmount = new List<PROC_RPT_GET_GOLD_INCOME_AMOUNT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_GOLD_INCOME_AMOUNT(objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeLeaderIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult UplineSupportIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeUplineSupportIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT_Result> listDirectAmount = new List<PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_UPLINE_SUPPORT_INCOME_AMOUNT(objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeUplineSupportIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult SilverIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeSilverIncome(string FromDate, string ToDate)
        {
            List<FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result> listDirectAmount = new List<FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS(false, objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeSilverIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult DiamondIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeDiamondIncome(string FromDate, string ToDate)
        {
            List<FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result> listDirectAmount = new List<FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.FN_GET_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS(true, objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeDiamondIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult Rewards()
        {
            var CompleteTotalBV = objManager.GetMatchingBVValues(objclsLogin.UserId, null, null); //db.PROC_GET_USER_TOTAL_BUSINESS_VOLUME(objclsLogin.UserId).FirstOrDefault().TotalBV;

            double TotalBV = 0;

            if (CompleteTotalBV > 0)
            {
                var objUserWalletMst = db.UserWalletMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                if (objUserWalletMst != null)
                {
                    if (CompleteTotalBV != objUserWalletMst.TotalMatchingBV && CompleteTotalBV > objUserWalletMst.TotalMatchingBV)
                    {
                        double NewMatchingBV = CompleteTotalBV - objUserWalletMst.TotalMatchingBV;

                        objUserWalletMst.TotalMatchingBV = objUserWalletMst.TotalMatchingBV + NewMatchingBV;
                        objUserWalletMst.MatchingBV = objUserWalletMst.MatchingBV + NewMatchingBV;

                        db.SaveChanges();
                    }

                    TotalBV = Convert.ToDouble(objUserWalletMst.MatchingBV);
                }
                else
                {
                    UserWalletMst objUserWalletMsts = new UserWalletMst();
                    objUserWalletMsts.UserId = objclsLogin.UserId;
                    objUserWalletMsts.TotalMatchingBV = CompleteTotalBV;
                    objUserWalletMsts.MatchingBV = CompleteTotalBV;

                    db.UserWalletMsts.Add(objUserWalletMsts);
                    db.SaveChanges();

                    TotalBV = Convert.ToDouble(CompleteTotalBV);
                }
            }

            ViewBag.TotalBV = Math.Round(TotalBV, 0);
            ViewBag.Rank = 0;
            ViewBag.UserId = objclsLogin.UserId;
            if (TotalBV > 25000000)
            {
                ViewBag.Rank = 10;
            }
            else if (TotalBV > 10000000)
            {
                ViewBag.Rank = 9;
            }
            else if (TotalBV > 5000000)
            {
                ViewBag.Rank = 8;
            }
            else if (TotalBV > 2500000)
            {
                ViewBag.Rank = 7;
            }
            else if (TotalBV > 1000000)
            {
                ViewBag.Rank = 6;
            }
            else if (TotalBV > 500000)
            {
                ViewBag.Rank = 5;
            }
            else if (TotalBV > 250000)
            {
                ViewBag.Rank = 4;
            }
            else if (TotalBV > 75000)
            {
                ViewBag.Rank = 3;
            }
            else if (TotalBV > 25000)
            {
                ViewBag.Rank = 2;
            }
            else if (TotalBV > 10000)
            {
                ViewBag.Rank = 1;
            }

            ViewBag.QualifyBV = 0;

            return View();
        }

        public List<MonthNames> GetUserMonthYear()
        {
            var objUserMst = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
            var listMonthNames = new List<MonthNames>();
            DateTime target = new DateTime(objUserMst.RegYear.Value, objUserMst.RegMonth.Value, 1);
            DateTime Currentdt = DateTime.Now;

            while (target < Currentdt)
            {
                listMonthNames.Add(new MonthNames()
                {
                    Month = Currentdt.Month,
                    Year = Currentdt.Year,
                    MonthYear = Currentdt.Month.ToString() + "#" + Currentdt.Year.ToString(),
                    MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Currentdt.Month) + "-" + Currentdt.Year.ToString()
                });
                //DO SOMETHING WITH TARGET.MONTH AND TARGET.YEAR
                Currentdt = Currentdt.AddMonths(-1);
            }
            return listMonthNames;
        }

        [SessionExpireFilter]
        public JsonResult InsertRewardClaim(Int32 UserId, string ClaimedBV, string PrimaryReward, string SecondaryReward)
        {
            ResponseMsg response = new ResponseMsg();

            try
            {
                var objUserWalletMst = db.UserWalletMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                if (objUserWalletMst != null)
                {
                    RewardClaimedMst rewardClaimedMst = new RewardClaimedMst();
                    rewardClaimedMst.UserId = Convert.ToInt32(UserId);

                    double dblMatchingBV = objUserWalletMst.MatchingBV;
                    rewardClaimedMst.MatchingBV = Math.Round(dblMatchingBV, 0);

                    rewardClaimedMst.ClaimedRewardBV = Convert.ToDouble(ClaimedBV);
                    rewardClaimedMst.ClaimedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    rewardClaimedMst.IsActive = true;
                    rewardClaimedMst.PrimaryReward = PrimaryReward;
                    rewardClaimedMst.SecondaryReward = SecondaryReward;
                    rewardClaimedMst.CreatedBy = Convert.ToInt32(UserId);
                    rewardClaimedMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

                    db.RewardClaimedMsts.Add(rewardClaimedMst);

                    objUserWalletMst.MatchingBV = (Math.Round(dblMatchingBV, 0) - Convert.ToDouble(ClaimedBV));

                    if (objUserWalletMst.MatchingBV < 0)
                    {
                        response.Key = false;
                        response.Msg = "Matching BV not enough for claim reward.";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }

                    db.SaveChanges();
                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "User reward detaild not found.";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult ClaimStatement()
        {
            var ClaimRewardDtls = db.RewardClaimedMsts.Where(i => i.UserId == objclsLogin.UserId);
            if (ClaimRewardDtls.Any())
            {
                ClaimRewardDtls = ClaimRewardDtls.ToList().OrderByDescending(i => i.ClaimedDate).AsQueryable();
            }

            return View(ClaimRewardDtls);
        }

        #region Miles Ledger

        [SessionExpireFilter]
        public ActionResult MilesDetails()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeMilesLedger(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_USER_MILES_LEDGER_Result> listDirectAmount = new List<PROC_RPT_GET_USER_MILES_LEDGER_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                ViewBag.TotalCreditAmount = db.PROC_RPT_GET_USER_MILES_LEDGER(objclsLogin.UserId).Sum(i => i.CreditAmount);
                ViewBag.TotalDebitAmount = db.PROC_RPT_GET_USER_MILES_LEDGER(objclsLogin.UserId).Sum(i => i.DebitAmount);
                var objFinalBalance = db.PROC_RPT_GET_USER_MILES_LEDGER(objclsLogin.UserId).LastOrDefault();
                ViewBag.FinalBalance = objFinalBalance != null ? objFinalBalance.BalanceAmount.Value : 0;
                listDirectAmount = db.PROC_RPT_GET_USER_MILES_LEDGER(objclsLogin.UserId).Where(i => i.TransactionDate.Date >= dtFromDate.Date && i.TransactionDate.Date <= dtToDate.Date).OrderByDescending(x => x.TransactionDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeMilesDetails", listDirectAmount);
        }

        #endregion Miles Ledger

        #region "Upline Monthly Income"

        [SessionExpireFilter]
        public ActionResult UplineMonthlyIncome()
        {
            try
            {
                var today = DateTime.Today;
                var month = new DateTime(today.Year, today.Month, 1);
                var first = month.AddMonths(-1);
                var last = month.AddDays(-1);
                ViewBag.StartDate = first.ToString("dd/MM/yyyy");
                ViewBag.EndDate = last.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public JsonResult GetDateRangeUplineMonthly(string StartDate, string EndDate)
        {
            List<PROC_GET_UPLINE_MONTHLY_INCOME_MEMBER_Result> lstUplineMonthlyIncome = new List<PROC_GET_UPLINE_MONTHLY_INCOME_MEMBER_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(StartDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(EndDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                lstUplineMonthlyIncome = db.PROC_GET_UPLINE_MONTHLY_INCOME_MEMBER(dtFromDate, dtToDate, objclsLogin.UserId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstUplineMonthlyIncome, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetDateRangeUplineMonthlyExport(string MonthYear, int QualifiedType)
        {
            List<PROC_GET_UPLINE_MONTHLY_INCOME_Result> lstUplineMonthlyIncome = new List<PROC_GET_UPLINE_MONTHLY_INCOME_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                bool QualifiedTypeBool = (QualifiedType == Convert.ToInt16("1"));

                lstUplineMonthlyIncome = db.PROC_GET_UPLINE_MONTHLY_INCOME((byte)Month, (short)Year, null, string.Empty, QualifiedTypeBool).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(lstUplineMonthlyIncome, new List<string> { "UserId", "Month", "Year", "UplineBugetIncome", "TotalMatchingIncome", "TotalBVQualifier" }, "UplineMonthlyIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineMonthlyIncome_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lstUplineMonthlyIncome = new List<PROC_GET_UPLINE_MONTHLY_INCOME_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        #endregion "Upline Monthly Income"

        #region "TopRetailer"

        public ActionResult TopRetailer()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public PartialViewResult GetMonthWiseTopRetailerDetails(string MonthYear)
        {
            List<PROC_GET_TOP_RETAILER_DETAILS_Result> listRetailerDtl = new List<PROC_GET_TOP_RETAILER_DETAILS_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                listRetailerDtl = db.PROC_GET_TOP_RETAILER_DETAILS(Month, Year, objclsLogin.UserId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseTopRetailerDetails", listRetailerDtl);
        }

        public ActionResult GetMonthWiseTopRetailerDetailsExport(string MonthYear)
        {
            List<PROC_GET_TOP_RETAILER_DETAILS_Result> listRetailerDtl = new List<PROC_GET_TOP_RETAILER_DETAILS_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                listRetailerDtl = db.PROC_GET_TOP_RETAILER_DETAILS(Month, Year, objclsLogin.UserId).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listRetailerDtl, new List<string> { "UserId" }, "TopRetailerReport", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=TopRetailerReport_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        #endregion "TopRetailer"

        #region PayoutMoreDtl

        public ActionResult PayoutMoreDtl()
        {
            var objPayoutDtl = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(objclsLogin.UserId, true).FirstOrDefault();
            ViewBag.TotalPayout = objPayoutDtl.TotalPayout;
            ViewBag.PendingPayout = objPayoutDtl.NetPayable;
            var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
            ViewBag.PreviousDate = serverDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetMonthWisePayoutMoreDtl(string FromDate, string ToDate)
        {
            List<WalletDebitDtl> listWalletDebitDtls = new List<WalletDebitDtl>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listWalletDebitDtls = db.WalletDebitDtls.Where(i => i.DebitDate >= dtFromDate && i.DebitDate <= dtToDate && i.UserId == objclsLogin.UserId).ToList();                
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWisePayoutMoreDtl", listWalletDebitDtls);
        }

        public ActionResult ExportGetMonthWisePayoutMoreDtl(string FromDate, string ToDate)
        {
            List<WalletDebitDtl> listWalletDebitDtls = new List<WalletDebitDtl>();
            List<WalletDebitDtlExtra> listWalletDebitDtlsExtra = new List<WalletDebitDtlExtra>();

            var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
            var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

            if (!Directory.Exists(Server.MapPath(UploadFilePath)))
                Directory.CreateDirectory(Server.MapPath(UploadFilePath));

            string FName = string.Format("PayoutDtl_{0}.xlsx", DateTime.Now.Ticks);
            string FnamePath = Server.MapPath(UploadFilePath) + FName;

            FileInfo fInfor = new FileInfo(FnamePath);

            listWalletDebitDtls = db.WalletDebitDtls.Where(i => i.DebitDate >= dtFromDate && i.DebitDate <= dtToDate && i.UserId == objclsLogin.UserId).ToList();

            foreach (var obj in listWalletDebitDtls)
            {
                listWalletDebitDtlsExtra.Add(new WalletDebitDtlExtra
                {
                    DirectIncome = obj.DirectIncomePay
                    ,
                    MatchingIncome = obj.PointsIncomePay
                    ,
                    SilverIncomePay = obj.SilverIncomePay,
                    LeaderIncomePay = obj.LeaderIncomePay,
                    UplineSupportIncomePay = obj.UplineSupportIncomePay,
                    DebitAmount = obj.DebitAmount
                    ,
                    UplineMonthlyPay = obj.UplineMonthlyPay,
                    RetailRewardMonthlyPay = obj.RetailRewardMonthlyPay,
                    VoltIncome = obj.VoltIncomePay
                    ,
                    TDSAmount = obj.TDSAmount
                    ,
                    TDSPerc = obj.TDSPerc
                    ,
                    AdminCharge = obj.AdminCharge
                    ,
                    AdminPerc = obj.AdminPerc
                    ,
                    NetPayable = obj.NetPayable
                    ,
                    DebitDate = obj.DebitDate
                    ,
                    UTRRefNo = obj.UTRRefNo
                });
            }

            ExcelPackage excel = Utilities.GetExcelPackageForExport(listWalletDebitDtlsExtra, new List<string> { "OtherIncome" }, "PayoutDtl", false);

            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", string.Format("attachment;filename=PayoutDtl_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            return View();
        }

        #endregion PayoutMoreDtl

        #region Company Turn Over Rewards

        [SessionExpireFilter]
        public ActionResult CTOReward()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public PartialViewResult GetMonthWiseCTODetails(string MonthYear)
        {
            List<BonanzaQualifiersEnt> listRetailerDtl = new List<BonanzaQualifiersEnt>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var CurrentDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                DateTime Date = new DateTime(Year, Month, 1);
                var MonthDuration = db.fn_GetFirstLastDateOfMonthByDate(Date).FirstOrDefault();
                if (CurrentDate > MonthDuration.EndDate)
                {
                    if (db.CompanyTurnOverRewardDtls.Any(i => i.Month == Month && i.Year == Year))
                    {
                        listRetailerDtl = objManager.GetCompanyTurnOverQualifierUsers(Month, Year);
                    }
                    else
                    {
                        listRetailerDtl = objManager.GetCompanyTurnOverQualifierUsers(Month, Year);

                        foreach (var item in listRetailerDtl)
                        {
                            CompanyTurnOverRewardDtl _objCompanyTurnOverRewardDtl = new CompanyTurnOverRewardDtl();
                            _objCompanyTurnOverRewardDtl.CreatedBy = 0;
                            _objCompanyTurnOverRewardDtl.CreatedDate = CurrentDate;
                            _objCompanyTurnOverRewardDtl.IsActive = true;
                            _objCompanyTurnOverRewardDtl.IsPaid = false;
                            _objCompanyTurnOverRewardDtl.Month = Month;
                            _objCompanyTurnOverRewardDtl.MonthBV = item.CurrentMonthBV;
                            _objCompanyTurnOverRewardDtl.PaidAmount = item.Amount;
                            _objCompanyTurnOverRewardDtl.ModifiedBy = 0;
                            _objCompanyTurnOverRewardDtl.ModifiedDate = CurrentDate;
                            ////_objCompanyTurnOverRewardDtl.PaidDate = null;
                            _objCompanyTurnOverRewardDtl.UserId = item.UserId;
                            _objCompanyTurnOverRewardDtl.UTRRefNo = string.Empty;
                            _objCompanyTurnOverRewardDtl.Year = Year;
                            db.CompanyTurnOverRewardDtls.Add(_objCompanyTurnOverRewardDtl);
                        }
                        db.SaveChanges();
                    }

                    var listBonanza = db.CompanyTurnOverRewardDtls.Where(i => i.Month == Month && i.Year == Year).ToList();
                    listRetailerDtl.ForEach(i =>
                    {
                        if (listBonanza.Any(x => x.UserId == i.UserId))
                        {
                            var obj = listBonanza.FirstOrDefault(x => x.UserId == i.UserId);
                            i.UTRRefNo = obj.UTRRefNo;
                            i.PaidAmount = obj.PaidAmount;
                            i.PaidDate = obj.PaidDate.HasValue ? obj.PaidDate.Value.ToString("dd-MM-yyyy") : string.Empty;
                            i.IsPaid = obj.IsPaid;
                            i.CompanyTurnOverRewardDtlId = obj.CompanyTurnOverRewardDtlId;
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            listRetailerDtl = listRetailerDtl.Where(p => p.UserId == objclsLogin.UserId).ToList();
            return PartialView("_GetMonthWiseCTODetails", listRetailerDtl);
        }

        #endregion Company Turn Over Rewards

        #region "LCB Pool Monthly Income"

        [SessionExpireFilter]
        public ActionResult LCBPoolMonthlyIncome()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            try
            {
                var result = (from recordset in db.LcbPoolIncomes
                              select new
                              {
                                  recordset.Year,
                                  recordset.Month
                              }).Distinct().ToList();

                //List<LcbPoolIncome> uplineIncome = db.LcbPoolIncomes.ToList();
                foreach (var upline in result)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = upline.Month,
                        Year = upline.Year,
                        MonthYear = upline.Month.ToString() + "#" + upline.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(upline.Month) + "-" + upline.Year.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        public JsonResult GetLCBPoolMonthlyIncome(string MonthYear)
        {
            List<PROC_GET_LCBPOOL_MONTHLY_INCOME_Member_Result> lstUplineMonthlyIncome = new List<PROC_GET_LCBPOOL_MONTHLY_INCOME_Member_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);

                lstUplineMonthlyIncome = db.PROC_GET_LCBPOOL_MONTHLY_INCOME_Member((byte)Month, (short)Year, objclsLogin.UserId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstUplineMonthlyIncome, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetLCBPoolMonthlyIncomeExport(string MonthYear)
        {
            List<PROC_GET_LCBPOOL_MONTHLY_INCOME_Member_Result> lstUplineMonthlyIncome = new List<PROC_GET_LCBPOOL_MONTHLY_INCOME_Member_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);

                lstUplineMonthlyIncome = db.PROC_GET_LCBPOOL_MONTHLY_INCOME_Member((byte)Month, (short)Year, objclsLogin.UserId).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(lstUplineMonthlyIncome, new List<string> { "DirectRefferalUserId", "LCBPoolIncomeId", "UserId", "Month", "Year" }, "UplineMonthlyIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineMonthlyIncome_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lstUplineMonthlyIncome = new List<PROC_GET_LCBPOOL_MONTHLY_INCOME_Member_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        #endregion "LCB Pool Monthly Income"

        #region "Retail Reward Monthly Income"

        [SessionExpireFilter]
        public ActionResult RetailRewardMonthlyIncome()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            try
            {
                List<RetailRewardIncomeSummary> uplineIncome = db.RetailRewardIncomeSummaries.ToList();
                foreach (var upline in uplineIncome)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = upline.Month,
                        Year = upline.Year,
                        MonthYear = upline.Month.ToString() + "#" + upline.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(upline.Month) + "-" + upline.Year.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        public JsonResult GetRetailRewardMonthly(string MonthYear)
        {
            List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result> lstUplineMonthlyIncome = new List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);

                lstUplineMonthlyIncome = db.PROC_GET_RETAIL_REWARD_MONTHLY_INCOME((byte)Month, (short)Year, objclsLogin.UserId, string.Empty, null).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstUplineMonthlyIncome, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetRetailRewardMonthlyExport(string MonthYear)
        {
            List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result> lstUplineMonthlyIncome = new List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);

                lstUplineMonthlyIncome = db.PROC_GET_RETAIL_REWARD_MONTHLY_INCOME((byte)Month, (short)Year, objclsLogin.UserId, string.Empty, null).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(lstUplineMonthlyIncome, new List<string> { "UserId", "Month", "Year", "RetailRewardIncomeSummaryId", "RetailRewardIncomeDetailId" }, "UplineMonthlyIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineMonthlyIncome_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lstUplineMonthlyIncome = new List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        #endregion "Retail Reward Monthly Income"
    }
}