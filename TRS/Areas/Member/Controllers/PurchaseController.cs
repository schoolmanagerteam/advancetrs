﻿using Elmah;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRS.Models;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Member.Controllers
{
    //[NoCache]
    public class PurchaseController : clsBase
    {
        // GET: Member/Purchase
        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            //return Redirect("https://maxenerwellness.com/shop/");
            clsLogin objclsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
            List<PROC_GET_PRODUCT_LIST_Result> listProductMst = new List<PROC_GET_PRODUCT_LIST_Result>();
            listProductMst = db.PROC_GET_PRODUCT_LIST().ToList();
            if (!objclsLogin.IsRegistrationActivated)
            {
                //listProductMst = listProductMst.Where(i => (i.IsAyurvedic.HasValue && i.IsAyurvedic.Value == true) || (i.DirectPurchasePerc.HasValue)).ToList();
                listProductMst = listProductMst.Where(i => i.JoinningProductType == (int)JoinningProductType.ForInActiveRegistrationUser
                                                        || i.JoinningProductType == (int)JoinningProductType.ForAllActiveUser).ToList();
            }
            else
                listProductMst = listProductMst.Where(i => i.JoinningProductType == (int)JoinningProductType.ForActiveRegistrationUser
                                                        || i.JoinningProductType == (int)JoinningProductType.ForAllActiveUser).ToList();

            ViewBag.ProductVariants = db.ProductVariantMsts.Where(p => p.IsActive == true).Select(i => new ProductVariantListEnt { VariantName = i.VariantName, ProductVariantId = i.ProductVariantId.ToString(), ProductId = i.ProductId.ToString() }).ToList();

            ViewBag.IsRegistrationActivated = objclsLogin.IsRegistrationActivated;

            List<string> listConfigs = new List<string>();
            listConfigs.Add(ConfigKeys.AllowCODAsFirstOrder);
            listConfigs.Add(ConfigKeys.ShopNowAdvertise);
            var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

            var AllowCODAsFirstOrder = false;
            var ShopNowAdvertise = string.Empty;
            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowCODAsFirstOrder))
            {
                try
                {
                    AllowCODAsFirstOrder = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AllowCODAsFirstOrder).Value);
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.ShopNowAdvertise))
            {
                try
                {
                    ShopNowAdvertise = listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.ShopNowAdvertise).Value;
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            ViewBag.AllowCODAsFirstOrder = AllowCODAsFirstOrder;

            if (objclsLogin != null && objclsLogin.RedirectFrmAdmin)
                ViewBag.AllowCODAsFirstOrder = !(objclsLogin.RedirectFrmAdmin);

            ViewBag.ShopNowAdvertise = ShopNowAdvertise;

            ////////////////////////////////////////////////////////////

            ViewBag.UserAddressList = db.PROC_GET_USER_ADDRESS_LST(objclsLogin.UserId, null).ToList();
            ViewBag.UserDefaultAddressDtlId = db.PROC_GET_USER_ADDRESS_LST(objclsLogin.UserId, null).ToList().Where(x => x.IsDefaultAddress == true).Select(x => x.UserAddressDtlId).FirstOrDefault();

            ViewBag.Cities = db.CityMsts.OrderBy(i => i.CityName).Select(i => new AutoCompleteEnt { key = i.CityId.ToString(), val = i.CityName.Replace(" ", "_").Replace("#", "_xxx_") }).ToList();

            ViewBag.StateTbl = db.StateMsts.OrderBy(x => x.StateName).Select(i => new AutoCompleteEnt { key = i.StateId.ToString(), val = i.StateName.Replace(" ", "_").Replace("#", "_xxx_") }).ToList();

            ViewBag.RedirectFrmAdmin = objclsLogin.RedirectFrmAdmin;
            ////////////////////////////////////////////////////////////

            return View(listProductMst);
        }

        [SessionExpireFilter]
        public JsonResult IsCityNameExists(string CityName)
        {
            ResponseMsg response = new ResponseMsg();

            try
            {
                if (!string.IsNullOrEmpty(CityName))
                {
                    var objCity = db.CityMsts.Where(i => i.CityName.Trim() == CityName.Replace("_xxx_", "#").Replace("_", " "));
                    if (objCity.Any())
                    {
                        response.Key = true;
                        response.Id = Convert.ToInt32(objCity.FirstOrDefault().StateId);
                    }
                    else
                        response.Key = false;
                }
                else
                {
                    response.Key = true;
                    response.Id = 0;
                }
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult InsertUserAddress(string Address, string CityName, string StateId, string PinCode, bool MakeDefaultAddress)
        {
            ResponseMsg response = new ResponseMsg();
            decimal CityId = 0;
            try
            {
                if (!string.IsNullOrEmpty(CityName))
                {
                    var objCity = db.CityMsts.Where(i => i.CityName.Trim() == CityName.Replace("_xxx_", "#").Replace("_", " ")).FirstOrDefault();
                    if (objCity != null && objCity.CityId != 0)
                    {
                        CityId = objCity.CityId;
                    }
                    else
                    {
                        using (var context = new TRSEntities(3600))
                        {
                            //var ObjStateMst = context.StateMsts.Where(x=>x.StateName.Trim() == StateName.Replace("_xxx_", "#").Replace("_", " ")).FirstOrDefault();
                            CityMst ObjCityMst = new CityMst();
                            ObjCityMst.CityName = CityName;
                            ObjCityMst.StateId = Convert.ToInt32(StateId);
                            ObjCityMst.IsActive = true;
                            ObjCityMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            ObjCityMst.CreatedBy = objclsLogin.UserId;
                            ObjCityMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            ObjCityMst.ModifiedBy = objclsLogin.UserId;

                            context.CityMsts.Add(ObjCityMst);

                            context.SaveChanges();

                            CityId = ObjCityMst.CityId;
                        }
                    }
                }

                UserAddressDtl useraddresslst = new UserAddressDtl();
                useraddresslst.UserId = objclsLogin.UserId;
                useraddresslst.Address = !string.IsNullOrEmpty(Address) ? Address.Trim() : null;
                useraddresslst.CityId = CityId;
                useraddresslst.PinCode = !string.IsNullOrEmpty(PinCode) ? PinCode.Trim() : null;
                useraddresslst.IsMasterAddress = false;
                useraddresslst.CreatedOn = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                useraddresslst.CreatedBy = objclsLogin.UserId;
                useraddresslst.IsActive = true;
                useraddresslst.IsDefaultAddress = MakeDefaultAddress;

                if (MakeDefaultAddress)
                {
                    var varuseraddresslst = db.UserAddressDtls.Where(x => x.UserId == objclsLogin.UserId);
                    if (varuseraddresslst.Any())
                    {
                        var objuseraddresslst = db.UserAddressDtls.Where(x => x.UserId == objclsLogin.UserId).ToList();
                        objuseraddresslst.ForEach(a => a.IsDefaultAddress = false);
                    }
                }

                db.UserAddressDtls.Add(useraddresslst);

                db.SaveChanges();
                response.Key = true;
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RefillUserAddressList()
        {
            List<PROC_GET_USER_ADDRESS_LST_Result> UserAddressList = new List<PROC_GET_USER_ADDRESS_LST_Result>();

            try
            {
                UserAddressList = db.PROC_GET_USER_ADDRESS_LST(objclsLogin.UserId, null).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(new { UserAddressList = UserAddressList }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Cart(string pinfo)
        {
            List<CartProductMst> listCartProductMst = new List<CartProductMst>();
            try
            {
                pinfo = pinfo.Replace(" ", "+");
                var SplitProductInfo = objManager.Decrypt(pinfo).Split(',');
                var listProducts = db.ProductMsts.ToList();
                foreach (var products in SplitProductInfo)
                {
                    var productInfo = products.Split('_');
                    var prodid = Convert.ToInt32(productInfo[0]);
                    var qty = Convert.ToInt32(productInfo[1]);
                    var pvid = 0;
                    if (productInfo.Length > 2)
                    {
                        pvid = Convert.ToInt32(productInfo[2]);
                    }
                    var objProduct = listProducts.Where(i => i.ProductId == prodid).FirstOrDefault();
                    listCartProductMst.Add(new CartProductMst
                    {
                        ProductId = objProduct.ProductId,
                        ImagePath = objProduct.ImagePath,
                        LongDescription = objProduct.LongDescription,
                        ProductName = objProduct.ProductName,
                        RegularPrice = objProduct.RegularPrice,
                        SalePrice = objProduct.SalePrice,
                        ShortDesc = objProduct.ShortDesc,
                        SKU = objProduct.SKU,
                        Stock = objProduct.Stock,
                        Qty = qty,
                        ProductCategoryId = pvid
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listCartProductMst);
        }

        [SessionExpireFilter]
        public JsonResult GetPaymentDtl(List<SelectedProduct> productinfo, bool IsCOD, string deliveryAddress)
        {
            Utilities.WriteLog("Start GetPaymentDtl");
            PaymentDtlEnt response = new PaymentDtlEnt();
            response.RedirectFrmAdmin = objclsLogin.RedirectFrmAdmin;
            response.OrderUserId = objclsLogin.UserId;
            double OrdDeliveryCharge = 0;
            try
            {
                List<string> listConfigs = new List<string>();
                listConfigs.Add(ConfigKeys.MultiOrderSystem);
                listConfigs.Add(ConfigKeys.AllowCODAsFirstOrder);
                listConfigs.Add(ConfigKeys.CODAmount);
                listConfigs.Add(ConfigKeys.MaxMilesUsePercentage);
                listConfigs.Add(ConfigKeys.AllowToUseMilesAmtInDirectPurchase);
                listConfigs.Add(ConfigKeys.MinimumOrderAmountForDeliveryChargeApply);
                listConfigs.Add(ConfigKeys.OrderDeliveryCharge);

                var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                var objUserMst = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                var AllowCODAsFirstOrder = false;
                var MultiOrderSystem = true;
                double CODAmount = 0;
                double MaxMilesUsePercentage = 0;
                var AllowToUseMilesAmtInDirectPurchase = false;
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MultiOrderSystem))
                {
                    try
                    {
                        MultiOrderSystem = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MultiOrderSystem).Value);
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.CODAmount))
                {
                    try
                    {
                        CODAmount = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.CODAmount).Value);
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowCODAsFirstOrder))
                {
                    try
                    {
                        AllowCODAsFirstOrder = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AllowCODAsFirstOrder).Value);
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MaxMilesUsePercentage))
                {
                    try
                    {
                        MaxMilesUsePercentage = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MaxMilesUsePercentage).Value);
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowToUseMilesAmtInDirectPurchase))
                {
                    try
                    {
                        AllowToUseMilesAmtInDirectPurchase = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AllowToUseMilesAmtInDirectPurchase).Value);
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

                double MinmumOrderAmountForDeliveryChargeApply = 0;
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MinimumOrderAmountForDeliveryChargeApply))
                {
                    try
                    {
                        MinmumOrderAmountForDeliveryChargeApply = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MinimumOrderAmountForDeliveryChargeApply).Value);
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

                double OrderDeliveryCharge = 0;
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.OrderDeliveryCharge))
                {
                    try
                    {
                        OrderDeliveryCharge = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OrderDeliveryCharge).Value);
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

                if (!MultiOrderSystem)
                {
                    var OrderGenerateStatusId = clsImplementationEnum.OrderStatus.OrderGenerate.GetHashCode();
                    if (db.UserPurchaseHdrs.Any(i => i.UserId == objclsLogin.UserId &&
                                                (!i.OrderStatusId.HasValue ||
                                                i.OrderStatusId.Value == OrderGenerateStatusId)))
                    {
                        response.Key = false;
                        response.Msg = "There is already one order placed. kindly request to first confirm that order then place other one!";
                        response.IsInfo = true;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                {
                    var objFirstOrder = db.UserPurchaseHdrs.FirstOrDefault(i => i.UserId == objclsLogin.UserId);
                    if (objFirstOrder != null && (!objFirstOrder.OrderStatusId.HasValue ||
                        (objFirstOrder.OrderStatusId.HasValue && objFirstOrder.OrderStatusId == clsImplementationEnum.OrderStatus.OrderGenerate.GetHashCode())))
                    {
                        response.Key = false;
                        response.Msg = "There is already one order placed. kindly request to first confirm that order then place other one!";
                        response.IsInfo = true;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }

                //clsLogin objclsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                if (productinfo != null && productinfo.Count > 0)
                {
                    var pinfo = "";
                    var productsId = productinfo.Select(i => i.ProductId);
                    var listProducts = db.ProductMsts.Where(i => productsId.Contains(i.ProductId));
                    double amount = 0;

                    List<string> listConfigKeys = new List<string>();
                    listConfigKeys.Add(ConfigKeys.MinRegistrationPoints);
                    listConfigKeys.Add(ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints);

                    var listConfigDtl = db.ConfigMsts.Where(i => listConfigKeys.Contains(i.Key.ToUpper())).ToList();
                    double MinRegistrationPoints = 0;
                    bool AllowToPurchaseBelowMinRegistrationPoints = false;
                    if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints))
                    {
                        MinRegistrationPoints = Convert.ToDouble(listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints).FirstOrDefault().Value);
                    }
                    if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints))
                    {
                        AllowToPurchaseBelowMinRegistrationPoints = Convert.ToBoolean(listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints).FirstOrDefault().Value);
                    }

                    double CreditAmount = 0;
                    foreach (var item in listProducts)
                    {
                        var SelectedProdInfo = productinfo.Where(i => i.ProductId == item.ProductId).FirstOrDefault();
                        if (pinfo.Length == 0)
                        {
                            pinfo = item.ProductId + "_" + SelectedProdInfo.Qty + "_" + SelectedProdInfo.ProductVariantId;
                        }
                        else
                        {
                            pinfo += "," + item.ProductId + "_" + SelectedProdInfo.Qty + "_" + SelectedProdInfo.ProductVariantId;
                        }
                        amount += (SelectedProdInfo.Qty * item.SalePrice.Value);

                        if (!objclsLogin.IsRegistrationActivated)
                        {
                            objManager.CalculateWalletCreditAmountRegistration(item, SelectedProdInfo.Qty, ref CreditAmount, WalletCreditType.Direct);
                        }
                        else
                        {
                            objManager.CalculateWalletCreditAmountRegistration(item, SelectedProdInfo.Qty, ref CreditAmount, WalletCreditType.Repurchase);
                        }
                    }

                    if (!IsCOD && !objclsLogin.RedirectFrmAdmin)
                    {
                        if (MinmumOrderAmountForDeliveryChargeApply > amount)
                        {
                            amount = (amount + OrderDeliveryCharge);
                            OrdDeliveryCharge = OrderDeliveryCharge;
                        }

                    }

                    if (!objclsLogin.IsRegistrationActivated)
                    {
                        //var RegistrationAmt = Convert.ToDouble(db.ConfigMsts.Where(i => i.Key == "RegistrationAmt").FirstOrDefault().Value);
                        //if (amount < RegistrationAmt)
                        //{
                        //    response.Key = false;
                        //    response.Msg = "Please Purchase Atleast " + RegistrationAmt.ToString() + "Rs. Amount Order!";
                        //    response.IsInfo = true;
                        //    return Json(response, JsonRequestBehavior.AllowGet);
                        //}
                        //else if (amount > RegistrationAmt)
                        //{
                        //    response.Key = false;
                        //    response.Msg = "You Must Have To Purchase " + RegistrationAmt.ToString() + "Rs. Amount Order!";
                        //    response.IsInfo = true;
                        //    return Json(response, JsonRequestBehavior.AllowGet);
                        //}
                        //if (!AllowToPurchaseBelowMinRegistrationPoints)
                        //{
                        //    if (CreditAmount < MinRegistrationPoints)
                        //    {
                        //        response.Key = false;
                        //        response.Msg = "You Must Have To Purchase Atleast " + MinRegistrationPoints.ToString() + " Points Order!";
                        //        response.IsInfo = true;
                        //        return Json(response, JsonRequestBehavior.AllowGet);
                        //    }
                        //}
                    }

                    var objTempProductWalletDebitDtl = objManager.InsertTempProductWalletDebitDtl(objUserMst.UserId, (decimal)amount);
                    double productamount = Convert.ToDouble((Int64)objTempProductWalletDebitDtl.ProductWalletAmount);
                    amount = amount - productamount;

                    pinfo = objManager.Encrypt(pinfo);
                    if (!IsCOD)
                    {
                        double MaximunMilesCanBeUse = 0;
                        MaximunMilesCanBeUse = Convert.ToInt64((amount * MaxMilesUsePercentage) / 100);
                        decimal MilesAmount = 0;
                        if (!objclsLogin.RedirectFrmAdmin)
                        {
                            if (objclsLogin.IsRegistrationActivated || (AllowToUseMilesAmtInDirectPurchase && !objclsLogin.IsRegistrationActivated))
                            {
                                //MilesAmount = objManager.GetUserMilesAmount(objclsLogin.UserId);
                                var objMilesAmount = db.UserMilesDtls.Where(x => x.UserId == objclsLogin.UserId);
                                if (objMilesAmount.Any())
                                {
                                    double dblMilesAmount = db.UserMilesDtls.Where(x => x.UserId == objclsLogin.UserId).Select(x => (x.TotalCreditMiles - x.TotalDebitMiles)).FirstOrDefault();
                                    MilesAmount = Math.Floor(Convert.ToDecimal(dblMilesAmount));
                                }
                            }
                            if ((double)MilesAmount > MaximunMilesCanBeUse)
                            {
                                MilesAmount = (decimal)MaximunMilesCanBeUse;
                                amount = amount - MaximunMilesCanBeUse;
                            }
                            else
                            {
                                amount = amount - (double)MilesAmount;
                            } 
                        }

                        #region RazorPay Code

                        Random r = new Random();
                        string txnid = "Txn" + r.Next(100, 9999);

                        Dictionary<string, object> input = new Dictionary<string, object>();
                        input.Add("amount", (amount * 100).ToString()); // this amount should be same as transaction amount
                        input.Add("currency", "INR");
                        input.Add("receipt", txnid);
                        input.Add("notes", new { pinfo = pinfo, tempid = objTempProductWalletDebitDtl.TempProductWalletDebitDtlId.ToString(), amount = amount, milesamt = MilesAmount, OrdDelChrg = OrdDeliveryCharge });
                        input.Add("payment_capture", 1);

                        var KeyDetails = objManager.GetPaymentKeyDetails();
                        RazorpayClient client = new RazorpayClient(KeyDetails.PaymentKey, KeyDetails.PaymentSecret);
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        Order order = client.Order.Create(input);

                        response.MerchantKey = KeyDetails.PaymentKey;
                        response.TxnId = order["id"].ToString();
                        response.FirstName = objUserMst.FirstName;
                        response.EmailId = objUserMst.EmailId;
                        response.MobileNo = objUserMst.MobileNo.HasValue ? objUserMst.MobileNo.ToString() : "";
                        response.ProductInfo = pinfo;
                        response.RegistrationPoints = CreditAmount.ToString();
                        response.IsRegistrationActivated = objclsLogin.IsRegistrationActivated;
                        response.Key = true;

                        #endregion RazorPay Code
                    }
                    else
                    {
                        var MilesAmount = 0;
                        if (!AllowCODAsFirstOrder && !db.UserPurchaseHdrs.Any(i => i.UserId == objclsLogin.UserId))
                        {
                            response.Key = false;
                            response.Msg = "You Can Not COD Order For First Order!";
                            response.IsInfo = true;
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }
                        if (CODAmount > 0)
                        {
                            Random r = new Random();
                            string txnid = "Txn" + r.Next(100, 9999);

                            Dictionary<string, object> input = new Dictionary<string, object>();
                            input.Add("amount", (CODAmount * 100).ToString()); // this amount should be same as transaction amount
                            input.Add("currency", "INR");
                            input.Add("receipt", txnid);
                            input.Add("notes", new { pinfo = pinfo, tempid = objTempProductWalletDebitDtl.TempProductWalletDebitDtlId.ToString(), amount = CODAmount.ToString(), milesamt = MilesAmount, OrdDelChrg = OrdDeliveryCharge });
                            input.Add("payment_capture", 1);

                            var KeyDetails = objManager.GetPaymentKeyDetails();
                            RazorpayClient client = new RazorpayClient(KeyDetails.PaymentKey, KeyDetails.PaymentSecret);
                            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                            Order order = client.Order.Create(input);

                            response.MerchantKey = KeyDetails.PaymentKey;
                            response.TxnId = order["id"].ToString();
                            response.FirstName = objUserMst.FirstName;
                            response.EmailId = objUserMst.EmailId;
                            response.MobileNo = objUserMst.MobileNo.HasValue ? objUserMst.MobileNo.ToString() : "";
                        }
                        else
                        {
                            response.TxnId = objTempProductWalletDebitDtl.TempProductWalletDebitDtlId.ToString();
                            response.WalletDebitDtlId = objTempProductWalletDebitDtl.TempProductWalletDebitDtlId.ToString();
                        }
                        response.Key = true;
                        response.IsRegistrationActivated = objclsLogin.IsRegistrationActivated;
                        response.ProductInfo = pinfo;
                        response.RegistrationPoints = CreditAmount.ToString();
                        response.codamt = CODAmount;
                    }
                    response.RedirectFrmAdmin = objclsLogin.RedirectFrmAdmin;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "Please Select Atleast One Product For Purchase!";
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                Utilities.WriteLog("Start SaveTempOrder");

                if (!response.RedirectFrmAdmin)
                    SaveTempOrder(response, IsCOD, deliveryAddress, OrdDeliveryCharge);

                Utilities.WriteLog("End SaveTempOrder");
                    
            }
            Utilities.WriteLog("End GetPaymentDtl");
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        protected void SaveTempOrder(PaymentDtlEnt response, bool IsCOD, string deliveryAddress, double OrdDeliveryCharge)
        {
            var KeyDetails = objManager.GetPaymentKeyDetails();
            RazorpayClient client = new RazorpayClient(KeyDetails.PaymentKey, KeyDetails.PaymentSecret);

            Order order = client.Order.Fetch(response.TxnId);
            var pinfo = order["notes"].pinfo;
            var tempid = order["notes"].tempid;
            var amount = order["notes"].amount;
            var milesamt = order["notes"].milesamt;
            double MilesAmount = milesamt != null && !string.IsNullOrEmpty(Convert.ToString(milesamt)) ? Convert.ToDouble(milesamt) : 0;

            Log.Info(string.Format("TempOrder Detail: RazorpayOrderId: {0}, pinfo: {1}, IsCOD:{2}", response.TxnId, pinfo, IsCOD));

            PaymentResponse_OrderTemp(pinfo.ToString(), tempid.ToString(), response.TxnId, "", "", deliveryAddress, IsCOD, MilesAmount, "", "", "", "", OrdDeliveryCharge);
        }

        public static void WriteLog(string strLog)
        {
            Utilities.WriteLog(strLog);
            //StreamWriter log;
            //FileStream fileStream = null;
            //DirectoryInfo logDirInfo = null;
            //FileInfo logFileInfo;

            //string msgFormat = string.Format("{0}-{1}", DateTime.Now.ToLongTimeString(), strLog);
            //string logFilePath = @"F:\\Project_Temp_Log\\";
            //logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            //logFileInfo = new FileInfo(logFilePath);
            //logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            //if (!logDirInfo.Exists) logDirInfo.Create();
            //if (!logFileInfo.Exists)
            //{
            //    fileStream = logFileInfo.Create();
            //}
            //else
            //{
            //    fileStream = new FileStream(logFilePath, FileMode.Append);
            //}
            //log = new StreamWriter(fileStream);
            //log.WriteLine(msgFormat);
            //log.Close();
        }

        public ActionResult PaymentSuccess(string order_id, string payment_id, string signature, bool IsCOD, string pinformation, string tid, string DeliveryAddress, string wid, string codamt, string OrderNote, string RazorpayOrderId, string RazorpayPaymentId, string RazorpaySignature, long UserAddressDtlId)
        {
            WriteLog("1_PaymentSuccess");
            ResponseMsg response = new ResponseMsg();
            bool disableOrderPayment = objclsLogin.RedirectFrmAdmin;
            try
            {
                if (disableOrderPayment || (!IsCOD || (!string.IsNullOrEmpty(codamt) && Convert.ToDouble(codamt) > 0)))
                {
                    WriteLog("1.1_PaymentSuccess");
                    var KeyDetails = objManager.GetPaymentKeyDetails();
                    WriteLog("1.2_PaymentSuccess");
                    RazorpayClient client = new RazorpayClient(KeyDetails.PaymentKey, KeyDetails.PaymentSecret);
                    WriteLog("1.3_PaymentSuccess");
                    Dictionary<string, string> attributes = new Dictionary<string, string>();

                    attributes.Add("razorpay_payment_id", payment_id);
                    attributes.Add("razorpay_order_id", order_id);
                    attributes.Add("razorpay_signature", signature);

                    if (!disableOrderPayment)
                        Utils.verifyPaymentSignature(attributes);
                    WriteLog("1.4_PaymentSuccess");
                    //Refund refund = new Razorpay.Api.Payment((string) paymentId).Refund();
                    Order order = client.Order.Fetch(order_id);
                    Razorpay.Api.Payment payment = null;
                    if (!disableOrderPayment)
                        payment = client.Payment.Fetch(payment_id);

                    WriteLog("1.5_PaymentSuccess");
                    var pinfo = order["notes"].pinfo;
                    var tempid = order["notes"].tempid;
                    var amount = order["notes"].amount;
                    var milesamt = order["notes"].milesamt;
                    var OrdDelChrg = order["notes"].OrdDelChrg;

                    double MilesAmount = milesamt != null && !string.IsNullOrEmpty(Convert.ToString(milesamt)) ? Convert.ToDouble(milesamt) : 0;
                    double OrdDeliveryCharge = (!string.IsNullOrEmpty(Convert.ToString(OrdDelChrg)) ? Convert.ToDouble(OrdDelChrg) : 0);

                    //Dictionary<string, object> options = new Dictionary<string, object>();
                    //options.Add("amount",Convert.ToDouble(amount));
                    //Payment paymentCaptured = payment.Capture(options);

                    WriteLog("2_PaymentSuccess");

                    Log.Info(string.Format("PaymentSuccess Detail: RazorpayOrderId: {0}, pinfo: {1}, IsCOD:{2}", order_id, pinfo, IsCOD));

                    if (payment != null && payment["status"] == "captured")
                    {
                        WriteLog("3_PaymentSuccess");
                        response.Key = PaymentResponse(pinfo.ToString(), tempid.ToString(), order_id, payment_id, signature, DeliveryAddress, UserAddressDtlId, IsCOD, MilesAmount, OrderNote, RazorpayOrderId, RazorpayPaymentId, RazorpaySignature, OrdDeliveryCharge);
                        WriteLog("4_PaymentSuccess");
                    }
                    else
                    {
                        WriteLog("5_PaymentSuccess");
                        response.Key = PaymentResponse(pinfo.ToString(), tempid.ToString(), "", "", "", DeliveryAddress, UserAddressDtlId, IsCOD, MilesAmount, OrderNote, RazorpayOrderId, RazorpayPaymentId, RazorpaySignature, OrdDeliveryCharge);
                        WriteLog("6_PaymentSuccess");
                    }
                }
                else
                {
                    //NOTE : remain for pass OrdDeliveryCharge 
                    response.Key = PaymentResponse(pinformation.ToString(), wid, "", "", "", DeliveryAddress, UserAddressDtlId, IsCOD, 0, OrderNote, RazorpayOrderId, RazorpayPaymentId, RazorpaySignature, 0);
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult PaymentResponse(string pinfo, string tempid)
        public bool PaymentResponse(string pinfo, string tempid, string OrderId, string PaymentId, string Signature, string DeliveryAddress, long UserAddressDtlId, bool IsCOD, double MilesAmt, string OrderNote, string RazorpayOrderId, string RazorpayPaymentId, string RazorpaySignature, double OrdDeliveryCharge)
        {
            WriteLog("1_");
            //ResponseMsg response = new ResponseMsg();
            bool Status = false;
            Int64 UserPurchaseHdrId = 0;
            double CreditAmount = 0;
            decimal OrderReceivedById = 0;
            bool OrderPlaceByAdmin = objclsLogin.RedirectFrmAdmin;
            try
            {
                using (var context = new TRSEntities(3600))
                {
                    WriteLog("2_");
                    DateTime dt_CurrentDatetime = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    var isPointsCreditdtlGenerateWhilePlaceOrder = false;
                    //context.Database.Log = Console.Write;
                    clsLogin objclsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            if (OrderPlaceByAdmin)
                                OrderId = RazorpayOrderId;

                            List<string> listConfigs = new List<string>();
                            listConfigs.Add(ConfigKeys.AdminConfirmationRequired);
                            listConfigs.Add(ConfigKeys.MinRegistrationPoints);
                            listConfigs.Add(ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints);
                            listConfigs.Add(ConfigKeys.ORDERGENBYADMINSTOPSMS);
                            listConfigs.Add(ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER);

                            var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                            WriteLog("3_");

                            var AdminConfirmationRequired = false;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AdminConfirmationRequired))
                            {
                                try
                                {
                                    AdminConfirmationRequired = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdminConfirmationRequired).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }
                            double MinRegistrationPoints = 0;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints))
                            {
                                try
                                {
                                    MinRegistrationPoints = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            bool AllowToPurchaseBelowMinRegistrationPoints = false;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints))
                            {
                                try
                                {
                                    AllowToPurchaseBelowMinRegistrationPoints = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            bool ORDER_GEN_BYADMIN_STOPSMS = true;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.ORDERGENBYADMINSTOPSMS))
                            {
                                try
                                {
                                    ORDER_GEN_BYADMIN_STOPSMS = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.ORDERGENBYADMINSTOPSMS).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER))
                            {
                                try
                                {
                                    isPointsCreditdtlGenerateWhilePlaceOrder = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            if (!string.IsNullOrEmpty(OrderId) && !string.IsNullOrEmpty(PaymentId) && !string.IsNullOrEmpty(Signature) && !IsCOD)
                            {
                                AdminConfirmationRequired = false;
                            }

                            if (OrderPlaceByAdmin && !IsCOD)
                                AdminConfirmationRequired = false;

                            var OrderAlreadyExist = false;
                            if (!string.IsNullOrEmpty(OrderId) && db.UserPurchaseHdrs.Any(i => i.RazorpayOrderId == OrderId))
                            {
                                OrderAlreadyExist = true;
                            }
                            if (!OrderAlreadyExist)
                            {
                                WriteLog("4_");

                                var objOrderReceivedBy = db.OrderReceivedByMsts.FirstOrDefault(i => i.OrderReceivedBy.ToUpper() == ("Razor Pay").ToUpper());
                                OrderReceivedById = objOrderReceivedBy != null ? objOrderReceivedBy.OrderReceivedById : 0;
                                var objUserMst = context.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                                var objReferralUser = context.UserMsts.Where(i => i.UserId == objUserMst.RefferalUserId).FirstOrDefault();

                                bool IsUserRegistrationActivated = (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value);

                                WriteLog("5_");

                                var TempProductWalletDebitDtlId = Convert.ToInt64(tempid);
                                var objTempProductWalletDebitDtl = context.TempProductWalletDebitDtls.Where(i => i.TempProductWalletDebitDtlId == TempProductWalletDebitDtlId).FirstOrDefault();

                                #region Purchase Orders

                                double TotalAmount = 0;

                                if (!OrderPlaceByAdmin)
                                {
                                    UserPurchaseHdrTemp userPurchaseHdrTemp = db.UserPurchaseHdrTemps.Where(p => p.RazorpayOrderId == OrderId).FirstOrDefault();
                                    if (userPurchaseHdrTemp != null)
                                    {
                                        List<UserPurchaseDtlTemp> userPurchaseDtlTemp = db.UserPurchaseDtlTemps.Where(p => p.UserPurchaseHdrId == userPurchaseHdrTemp.UserPurchaseHdrId).ToList();
                                        if (userPurchaseDtlTemp.Any())
                                        {
                                            var pinfo_MainTbl = pinfo;
                                            pinfo = "";
                                            foreach (var item in userPurchaseDtlTemp)
                                            {
                                                if (pinfo.Length == 0)
                                                    pinfo = item.ProductId + "_" + item.Qty + "_" + item.ProductVariantId;
                                                else
                                                    pinfo += "," + item.ProductId + "_" + item.Qty + "_" + item.ProductVariantId;
                                            }
                                            pinfo = objManager.Encrypt(pinfo);

                                            if (pinfo_MainTbl != pinfo)
                                                Log.Error(string.Format("pinfo is not matched for RazorPayOrderId: {0}, pinfo_TempTbl: {1}, pinfo_MainTbl: {2}", OrderId, pinfo, pinfo_MainTbl));
                                        }
                                    }
                                }

                                var SplitProductInfo = objManager.Decrypt(pinfo).Split(',');
                                var objUserPurchaseHdr = new UserPurchaseHdr();
                                var listUserPurchaseDtl = new List<UserPurchaseDtl>();

                                WriteLog("6_");

                                foreach (var products in SplitProductInfo)
                                {
                                    var productInfo = products.Split('_');
                                    var prodid = Convert.ToInt32(productInfo[0]);
                                    var qty = Convert.ToInt32(productInfo[1]);
                                    var pvid = 0;
                                    if (productInfo.Length > 2)
                                    {
                                        pvid = Convert.ToInt32(productInfo[2]);
                                    }
                                    var objProductMst = context.ProductMsts.Where(i => i.ProductId == prodid).FirstOrDefault();

                                    objProductMst.Stock = objProductMst.Stock.HasValue ? objProductMst.Stock.Value - qty : -qty;

                                    TotalAmount += (qty * objProductMst.SalePrice.Value);
                                    var objUserPurchaseDtl = new UserPurchaseDtl();
                                    objUserPurchaseDtl.Amount = (qty * objProductMst.SalePrice.Value);
                                    objUserPurchaseDtl.CreatedBy = objclsLogin.UserId;
                                    objUserPurchaseDtl.CreatedDate = dt_CurrentDatetime;
                                    objUserPurchaseDtl.IsActive = true;
                                    objUserPurchaseDtl.ProductId = prodid;
                                    objUserPurchaseDtl.ProductPrice = objProductMst.SalePrice.Value;
                                    objUserPurchaseDtl.RegularPrice = objProductMst.RegularPrice.Value;
                                    objUserPurchaseDtl.Qty = qty;
                                    objUserPurchaseDtl.ProductVariantId = pvid;

                                    #region Points

                                    if (!AdminConfirmationRequired)
                                    {
                                        if (OrderReceivedById > 0)
                                        {
                                            objUserPurchaseHdr.OrderReceivedById = Convert.ToInt32(OrderReceivedById);
                                        }
                                        if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                        {
                                            WriteLog("6_1.1");
                                            objManager.InsertUserPointsOnPurchase(context, objUserPurchaseDtl, objclsLogin.UserId, objUserMst.RefferalUserId, WalletCreditType.Direct, isPointsCreditdtlGenerateWhilePlaceOrder);
                                            WriteLog("6_1.2");
                                            objManager.CalculateWalletCreditAmountRegistration(objProductMst, qty, ref CreditAmount, WalletCreditType.Direct);
                                            WriteLog("6_1.3");
                                        }
                                        else
                                        {
                                            WriteLog("6_1.1");
                                            objManager.InsertUserPointsOnPurchase(context, objUserPurchaseDtl, objclsLogin.UserId, objUserMst.RefferalUserId, WalletCreditType.Repurchase, isPointsCreditdtlGenerateWhilePlaceOrder);
                                            WriteLog("6_1.2");
                                        }
                                    }

                                    #endregion Points

                                    objUserPurchaseHdr.UserPurchaseDtls.Add(objUserPurchaseDtl);

                                    WriteLog("7_");
                                }

                                if (!AdminConfirmationRequired && isPointsCreditdtlGenerateWhilePlaceOrder)
                                {
                                    WriteLog("8_");
                                    db.PROC_INSERT_ALL_PARENT_USER_NOTIFICATIONMST(objclsLogin.UserId, NotificationType.Information.GetHashCode(), true);
                                    WriteLog("9_");
                                }

                                objUserPurchaseHdr.IsCODOrder = IsCOD;
                                objUserPurchaseHdr.Notes = OrderNote;
                                objUserPurchaseHdr.IsOrderPlaceByAdmin = objclsLogin.RedirectFrmAdmin;
                                //objUserPurchaseHdr.OrderNo = Convert.ToInt64(Convert.ToInt64(objManager.GenerateOrderNo()));
                                objUserPurchaseHdr.CreatedBy = objclsLogin.UserId;
                                objUserPurchaseHdr.CreatedDate = dt_CurrentDatetime;
                                objUserPurchaseHdr.IsActive = true;
                                objUserPurchaseHdr.OrderDate = dt_CurrentDatetime;
                                objUserPurchaseHdr.TotalOrderAmount = (TotalAmount - MilesAmt) + OrdDeliveryCharge;
                                objUserPurchaseHdr.UserId = objclsLogin.UserId;
                                if ((bool)!objUserPurchaseHdr.IsOrderPlaceByAdmin)
                                {
                                    objUserPurchaseHdr.RazorpayOrderId = OrderId;
                                    objUserPurchaseHdr.RazorpayPaymentId = PaymentId;
                                    objUserPurchaseHdr.RazorpaySignature = Signature;
                                }
                                else
                                {
                                    objUserPurchaseHdr.RazorpayOrderId = RazorpayOrderId;
                                    objUserPurchaseHdr.RazorpayPaymentId = RazorpayPaymentId;
                                    objUserPurchaseHdr.RazorpaySignature = RazorpaySignature;
                                }
                                if (!string.IsNullOrEmpty(DeliveryAddress) && !string.IsNullOrWhiteSpace(DeliveryAddress))
                                {
                                    objUserPurchaseHdr.DeliveryAddress = DeliveryAddress;
                                    objUserPurchaseHdr.UserAddressDtlId = UserAddressDtlId;
                                }
                                if (AdminConfirmationRequired)
                                {
                                    objUserPurchaseHdr.OrderStatusId = OrderStatus.OrderGenerate.GetHashCode();
                                }
                                else
                                {
                                    objUserPurchaseHdr.OrderStatusId = OrderStatus.OrderConfirm.GetHashCode();
                                }
                                if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                {
                                    objUserPurchaseHdr.IsRepurchaseOrder = false;
                                }
                                else
                                {
                                    objUserPurchaseHdr.IsRepurchaseOrder = true;
                                }
                                if (MilesAmt > 0)
                                {
                                    var objMilesDebitDtl = new MilesDebitDtl();
                                    objMilesDebitDtl.CreatedBy = objUserPurchaseHdr.CreatedBy;
                                    objMilesDebitDtl.CreatedDate = objUserPurchaseHdr.CreatedDate;
                                    objMilesDebitDtl.DebitAmount = Math.Round(MilesAmt, 2);
                                    objMilesDebitDtl.DebitDate = objUserPurchaseHdr.OrderDate;
                                    objMilesDebitDtl.IsActive = true;
                                    objMilesDebitDtl.UserId = objUserPurchaseHdr.UserId;
                                    objUserPurchaseHdr.MilesDebitDtls.Add(objMilesDebitDtl);

                                    List<MilesCreditDtl> UserCreditMiles = context.MilesCreditDtls.Where(x => x.UserId == objUserPurchaseHdr.UserId && (x.CreditAmount - x.UsedCreditAmount) > 0).OrderBy(x => x.MilesCreditDtlId).ToList();
                                    decimal MileDebitRemain = Convert.ToDecimal(MilesAmt);
                                    foreach (var item in UserCreditMiles)
                                    {
                                        if (MileDebitRemain > 0)
                                        {
                                            decimal RemainForUsed = (item.CreditAmount - item.UsedCreditAmount);
                                            if (RemainForUsed < MileDebitRemain)
                                            {
                                                item.UsedCreditAmount = (item.UsedCreditAmount + RemainForUsed);
                                                MileDebitRemain = MileDebitRemain - RemainForUsed;
                                            }
                                            else
                                            {
                                                item.UsedCreditAmount = (item.UsedCreditAmount + MileDebitRemain);
                                                MileDebitRemain = 0;
                                            }
                                        }

                                        if (MileDebitRemain == 0)
                                            break;
                                    }

                                    var objUserMiles = context.UserMilesDtls.Where(x => x.UserId == objUserPurchaseHdr.UserId).FirstOrDefault();
                                    objUserMiles.TotalDebitMiles = objUserMiles.TotalDebitMiles + MilesAmt;
                                }

                                objUserPurchaseHdr.DeliveryCharge = OrdDeliveryCharge;
                                context.UserPurchaseHdrs.Add(objUserPurchaseHdr);

                                WriteLog("11_");

                                #endregion Purchase Orders

                                if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                {
                                    WriteLog("12_");
                                    if (!AdminConfirmationRequired)
                                    {
                                        //Multiple purchase id activation
                                        var RegistrationPoints = objUserMst.RegistrationPoints.HasValue ? objUserMst.RegistrationPoints.Value : 0;
                                        RegistrationPoints += CreditAmount;

                                        if (RegistrationPoints >= MinRegistrationPoints || AllowToPurchaseBelowMinRegistrationPoints)
                                        {
                                            objUserMst.IsRegistrationActivated = true;
                                            objUserMst.ActivateDate = dt_CurrentDatetime;

                                            var objLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                                            objLogin.IsRegistrationActivated = true;
                                        }
                                        objUserMst.RegistrationPoints = RegistrationPoints;

                                        #region Wallet Credit

                                        if (objReferralUser.RefferalUserId != 0)
                                        {
                                            WriteLog("13_");
                                            objManager.InsertWalletCredit(context, objUserPurchaseHdr, objclsLogin.UserId, objUserMst.RefferalUserId, WalletCreditType.Direct);
                                            WriteLog("14_");
                                        }

                                        #endregion Wallet Credit
                                    }
                                    
                                    if (!objUserMst.IsRegistrationMsgSend.HasValue || (objUserMst.IsRegistrationMsgSend.HasValue && !objUserMst.IsRegistrationMsgSend.Value))
                                    {
                                        if (!objclsLogin.RedirectFrmAdmin || !ORDER_GEN_BYADMIN_STOPSMS)
                                        {
                                            if (objUserMst.MobileNo.HasValue)
                                            {
                                                //var Msg = "Congratulation " + objUserMst.FullName + " . You have been successful registered to maxener wellness - financial reward plan .Your user id - " + objUserMst.UserName + " ,Password - " + objUserMst.Password + "(do not share your pass word) to any other person.Complete your profile by loging in - " + HttpContext.Application[clsImplementationMessage.WebsiteURL].ToString();

                                                var Msg = string.Format("Congratulation {0} . You have been successful registered to Maxener Wellness .Your user id - {1} ,Passcode - {2} (do not share your pass word) to any other person.Complete your profile by loging in - https://www.maxenerwellness.co.in Team Maxener Wellness"
                                                        , objUserMst.FullName, objUserMst.UserName, objUserMst.Password);

                                                objManager.SendSMS(objUserMst.MobileNo, "1107160293313158853", Msg, "New Registration");
                                                objUserMst.IsRegistrationMsgSend = true;
                                            }
                                            if (objReferralUser.MobileNo.HasValue)
                                            {
                                                var Msg = "Congratulations, " + objUserMst.FullName + " successfully registered under your " + (objUserMst.Position == "L" ? "left" : "right") + " side. Team Maxener Wellness";
                                                objManager.SendSMS(objReferralUser.MobileNo, string.Empty, Msg, "Team Joining");
                                                objUserMst.IsRegistrationMsgSend = true;
                                            }
                                        }
                                    }
                                }
                                WriteLog("15_");

                                context.SaveChanges();
                                transaction.Commit();
                                UserPurchaseHdrId = objUserPurchaseHdr.UserPurchaseHdrId;
                                WriteLog("16_");

                                //
                                if (db.UserPurchaseHdrTemps.Any(p => p.RazorpayOrderId == objUserPurchaseHdr.RazorpayOrderId))
                                {
                                    UserPurchaseHdrTemp UserPurchaseHdrTempObj = context.UserPurchaseHdrTemps.FirstOrDefault(p => p.RazorpayOrderId == objUserPurchaseHdr.RazorpayOrderId);
                                    UserPurchaseHdrTempObj.IsSuccess = UserPurchaseHdrTempObj.IsProcess = true;
                                    UserPurchaseHdrTempObj.Notes = string.Format("Order placed successfully. Ref UserPurchaseHdrId: {0}", UserPurchaseHdrId);
                                    context.SaveChanges();
                                }

                                #region "13-09-2020"

                                if (isPointsCreditdtlGenerateWhilePlaceOrder && !AdminConfirmationRequired && objUserPurchaseHdr.UserPurchaseDtls != null && objUserPurchaseHdr.UserPurchaseDtls.Any())
                                {
                                    foreach (var userPurchaseDtl in objUserPurchaseHdr.UserPurchaseDtls)
                                    {
                                        var objProductMst = context.ProductMsts.Where(i => i.ProductId == userPurchaseDtl.ProductId).FirstOrDefault();

                                        if (IsUserRegistrationActivated)//Direct
                                        {
                                            var DirectMatchingPerc = Convert.ToDouble(objProductMst.DirectMatchingPerc);

                                            double DirectMatching = 0;
                                            DirectMatching = (Int64)(Convert.ToDouble((objProductMst.DirectMatchingAmt.Value * objProductMst.DirectMatchingPerc) / 100));
                                            var CreditAmount_f = (decimal)(decimal.Round((decimal)(DirectMatching * userPurchaseDtl.Qty), 0, MidpointRounding.AwayFromZero));

                                            db.PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL(objclsLogin.UserId, userPurchaseDtl.UserPurchaseDtlId, DirectMatchingPerc, CreditAmount_f, false);
                                        }
                                        else //Repurchase
                                        {
                                            var MatchingPerc = Convert.ToDouble(objProductMst.RepurchaseMatchingPerc);
                                            double RepurchaseMatching = 0;
                                            RepurchaseMatching = (Int64)(Convert.ToDouble((objProductMst.RepurchaseMatchingAmt.Value * objProductMst.RepurchaseMatchingPerc) / 100));
                                            var CreditAmount_f = (decimal)(decimal.Round((decimal)(RepurchaseMatching * userPurchaseDtl.Qty), 0, MidpointRounding.AwayFromZero));

                                            db.PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL(objclsLogin.UserId, userPurchaseDtl.UserPurchaseDtlId, MatchingPerc, CreditAmount_f, true);
                                        }
                                    }
                                }

                                #endregion "13-09-2020"

                                WriteLog("16_A");

                                if (!objclsLogin.RedirectFrmAdmin || !ORDER_GEN_BYADMIN_STOPSMS)
                                {
                                    try
                                    {
                                        //var OrderMsg = "Dear  " + objUserMst.FirstName + " : your order is to be received by order number: " + objUserPurchaseHdr.OrderNo + ", it will be deliver soon. Team Maxener";
                                        var OrderMsg = string.Format("Dear {0} : Your order have been successfully placed . Your order id number is : {1}, Your tracking deatils will be sent within next 48 hours . Team Maxener Wellness"
                                                                    , objUserMst.FirstName, objUserPurchaseHdr.OrderNo);
                                        objManager.SendSMS(objUserMst.MobileNo, "1107160293366271719", OrderMsg, "New Order");
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorSignal.FromCurrentContext().Raise(ex);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            ErrorSignal.FromCurrentContext().Raise(ex);
                            //return RedirectToAction("Index", "Purchase", new { area = "Member" });
                            return false;
                        }
                    }

                    #region Old Gold & Silver Member Logic

                    //if (IsRepurchase)
                    //{
                    //    try
                    //    {
                    //        var listParentMemberDtl = db.PROC_GET_ALL_PARENT_USER_TREE(objclsLogin.UserId, false).ToList().Select(i => new ParentMemberDtl { FullName = i.FullName, UserId = i.UserId.Value }).ToList();

                    //        var objUserMst = context.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                    //        var objReferralUser = context.UserMsts.Where(i => i.UserId == objUserMst.RefferalUserId).FirstOrDefault();

                    //        List<string> listConfigs = new List<string>();
                    //        listConfigs.Add(ConfigKeys.RequiredMemberForGold);
                    //        listConfigs.Add(ConfigKeys.RequiredMemberForSilver);

                    //        var listConfigDtl = context.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();
                    //        int RequiredMemberForGold = 150;
                    //        int RequiredMemberForSilver = 150;
                    //        if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.RequiredMemberForGold))
                    //        {
                    //            RequiredMemberForGold = Convert.ToInt32(listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.RequiredMemberForGold).FirstOrDefault().Value);
                    //        }
                    //        if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.RequiredMemberForSilver))
                    //        {
                    //            RequiredMemberForSilver = Convert.ToInt32(listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.RequiredMemberForSilver).FirstOrDefault().Value);
                    //        }
                    //        foreach (var item in listParentMemberDtl)
                    //        {
                    //            var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                    //            if (!objParentUserMst.IsGoldMember.HasValue || !objParentUserMst.IsGoldMember.Value)
                    //            {
                    //                var listRIGHTUsersCnt = context.FN_GET_ALL_RIGHT_SIDED_USER_TREE(objParentUserMst.UserId, false).Where(i => i.IsRegistered == true).Count();
                    //                var listLEFTUsersCnt = context.FN_GET_ALL_LEFT_SIDED_USER_TREE(objParentUserMst.UserId, false).Where(i => i.IsRegistered == true).Count();
                    //                if (listRIGHTUsersCnt >= RequiredMemberForGold && listLEFTUsersCnt >= RequiredMemberForGold)
                    //                {
                    //                    objParentUserMst.IsGoldMember = true;
                    //                    objParentUserMst.GoldMemberDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                    //                }
                    //            }
                    //        }
                    //        context.SaveChanges();

                    //        foreach (var item in listParentMemberDtl)
                    //        {
                    //            var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                    //            if (!objParentUserMst.IsSilverMember.HasValue || !objParentUserMst.IsSilverMember.Value)
                    //            {
                    //                var listRIGHTUsersCnt = context.FN_GET_ALL_RIGHT_SIDED_USER_TREE(item.UserId, false).Where(i => i.IsRegistered == true).Count();
                    //                var listLEFTUsersCnt = context.FN_GET_ALL_LEFT_SIDED_USER_TREE(item.UserId, false).Where(i => i.IsRegistered == true).Count();
                    //                var listParentParentMemberDtl = db.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).ToList().Select(i => i.UserId.Value).ToArray();

                    //                if (listRIGHTUsersCnt >= RequiredMemberForSilver
                    //                    && listLEFTUsersCnt >= RequiredMemberForSilver
                    //                    && db.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsGoldMember == true))
                    //                {
                    //                    objParentUserMst.IsSilverMember = true;
                    //                    objParentUserMst.SilverMemberDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                    //                }
                    //            }
                    //        }
                    //        context.SaveChanges();

                    //        //if (UserPurchaseHdrId > 0)
                    //        //{
                    //        //    var listParentUserId = listParentMemberDtl.Select(i => i.UserId).ToList();
                    //        //    objManager.InsertLedgerAndUplinePointsCreditDtl(objclsLogin.UserId, UserPurchaseHdrId, objReferralUser.UserId, listParentUserId);
                    //        //}
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        ErrorSignal.FromCurrentContext().Raise(ex);
                    //    }
                    //}

                    #endregion Old Gold & Silver Member Logic

                    //if (IsRepurchase)
                    //{
                    try
                    {
                        //var listParentMemberDtl = db.PROC_GET_ALL_PARENT_USER_TREE(objclsLogin.UserId, true).ToList().Select(i => new ParentMemberDtl { FullName = i.FullName, UserId = i.UserId.Value }).ToList();

                        if (isPointsCreditdtlGenerateWhilePlaceOrder)
                        {
                            var listParentMemberDtl = context.UserMsts.Where(i => i.UserId == objclsLogin.UserId).Select(i => new ParentMemberDtl { FullName = i.FullName, UserId = i.UserId }).ToList();
                            var objUserMst = context.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                            var objReferralUser = context.UserMsts.Where(i => i.UserId == objUserMst.RefferalUserId).FirstOrDefault();

                            List<string> listConfigKeys = new List<string>();
                            listConfigKeys.Add(ConfigKeys.GoldIncomeBV);
                            listConfigKeys.Add(ConfigKeys.SilverIncomeBV);
                            listConfigKeys.Add(ConfigKeys.PrimeIncomeBV);
                            listConfigKeys.Add(ConfigKeys.DiamondIncomeBV);

                            var listConfig = objManager.GetConfigValues(listConfigKeys);
                            double GoldIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.GoldIncomeBV));
                            double SilverIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.SilverIncomeBV));
                            double PrimeIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.PrimeIncomeBV));
                            double DiamondIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.DiamondIncomeBV));
                            WriteLog("17_");
                            List<UserMatchingBVDtl> listUserMatchingBVDtl = new List<UserMatchingBVDtl>();
                            foreach (var item in listParentMemberDtl)
                            {
                                var MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });

                                var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                                if (!objParentUserMst.IsGoldMember.HasValue || !objParentUserMst.IsGoldMember.Value)
                                {
                                    if (MatchingBV >= GoldIncomeBV)
                                    {
                                        objParentUserMst.IsGoldMember = true;
                                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)GoldIncomeBV).FirstOrDefault();
                                        objParentUserMst.GoldMemberDate = dt;
                                    }
                                }
                                if (!objParentUserMst.IsDimondMember.HasValue || !objParentUserMst.IsDimondMember.Value)
                                {
                                    if (MatchingBV >= DiamondIncomeBV)
                                    {
                                        objParentUserMst.IsDimondMember = true;
                                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)DiamondIncomeBV).FirstOrDefault();
                                        objParentUserMst.DimondMemberDate = dt;
                                    }
                                }
                                WriteLog("18_");
                            }
                            WriteLog("19_");

                            foreach (var item in listParentMemberDtl)
                            {
                                var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                                if (!objParentUserMst.IsPrimeMember.HasValue || !objParentUserMst.IsPrimeMember.Value)
                                {
                                    double MatchingBV = 0;
                                    if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                                    {
                                        MatchingBV = listUserMatchingBVDtl.Where(i => i.UserId == item.UserId).FirstOrDefault().MatchingBV;
                                    }
                                    else
                                    {
                                        MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                        listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });
                                    }
                                    var listParentParentMemberDtl = db.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).ToList().Select(i => i.UserId.Value).ToArray();

                                    if (MatchingBV >= PrimeIncomeBV
                                        && db.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsGoldMember == true))
                                    {
                                        objParentUserMst.IsPrimeMember = true;
                                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)PrimeIncomeBV).FirstOrDefault();
                                        objParentUserMst.PrimeMemberDate = dt;
                                    }
                                }

                                if (!objParentUserMst.IsSilverMember.HasValue || !objParentUserMst.IsSilverMember.Value)
                                {
                                    double MatchingBV = 0;
                                    if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                                    {
                                        MatchingBV = listUserMatchingBVDtl.Where(i => i.UserId == item.UserId).FirstOrDefault().MatchingBV;
                                    }
                                    else
                                    {
                                        MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                    }
                                    var listParentParentMemberDtl = db.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).ToList().Select(i => i.UserId.Value).ToArray();

                                    if (MatchingBV >= SilverIncomeBV
                                        && db.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsDimondMember == true))
                                    {
                                        objParentUserMst.IsSilverMember = true;
                                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)SilverIncomeBV).FirstOrDefault();
                                        objParentUserMst.SilverMemberDate = dt;
                                    }
                                }
                                WriteLog("19_");
                            }
                        }

                        context.SaveChanges();
                        WriteLog("20_");
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                    //}
                }
                Status = true;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            //return RedirectToAction("Index", "Dashboard", new { area = "Member" });
            return Status;
        }

        public bool PaymentResponse_OrderTemp(string pinfo, string tempid, string OrderId, string PaymentId, string Signature, string DeliveryAddress, bool IsCOD, double MilesAmt, string OrderNote, string RazorpayOrderId, string RazorpayPaymentId, string RazorpaySignature, double OrdDeliveryCharge)
        {
            WriteLog("1_");
            bool Status = false;
            Int64 UserPurchaseHdrId = 0;
            double CreditAmount = 0;
            decimal OrderReceivedById = 0;
            bool OrderPlaceByAdmin = objclsLogin.RedirectFrmAdmin;
            try
            {
                using (var context = new TRSEntities(3600))
                {
                    WriteLog("2_");
                    DateTime dt_CurrentDatetime = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    var isPointsCreditdtlGenerateWhilePlaceOrder = false;
                    //context.Database.Log = Console.Write;
                    clsLogin objclsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            List<string> listConfigs = new List<string>();
                            listConfigs.Add(ConfigKeys.AdminConfirmationRequired);
                            listConfigs.Add(ConfigKeys.MinRegistrationPoints);
                            listConfigs.Add(ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints);
                            listConfigs.Add(ConfigKeys.ORDERGENBYADMINSTOPSMS);
                            listConfigs.Add(ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER);

                            var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                            WriteLog("3_");

                            var AdminConfirmationRequired = false;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AdminConfirmationRequired))
                            {
                                try
                                {
                                    AdminConfirmationRequired = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdminConfirmationRequired).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }
                            double MinRegistrationPoints = 0;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints))
                            {
                                try
                                {
                                    MinRegistrationPoints = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            bool AllowToPurchaseBelowMinRegistrationPoints = false;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints))
                            {
                                try
                                {
                                    AllowToPurchaseBelowMinRegistrationPoints = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            bool ORDER_GEN_BYADMIN_STOPSMS = true;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.ORDERGENBYADMINSTOPSMS))
                            {
                                try
                                {
                                    ORDER_GEN_BYADMIN_STOPSMS = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.ORDERGENBYADMINSTOPSMS).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER))
                            {
                                try
                                {
                                    isPointsCreditdtlGenerateWhilePlaceOrder = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            if (!IsCOD)
                                AdminConfirmationRequired = false;

                            var OrderAlreadyExist = false;
                            if (!string.IsNullOrEmpty(OrderId) && db.UserPurchaseHdrTemps.Any(i => i.RazorpayOrderId == OrderId))
                            {
                                OrderAlreadyExist = true;
                            }
                            if (!OrderAlreadyExist)
                            {
                                WriteLog("4_");

                                var objOrderReceivedBy = db.OrderReceivedByMsts.FirstOrDefault(i => i.OrderReceivedBy.ToUpper() == ("Razor Pay").ToUpper());
                                OrderReceivedById = objOrderReceivedBy != null ? objOrderReceivedBy.OrderReceivedById : 0;
                                var objUserMst = context.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                                var objReferralUser = context.UserMsts.Where(i => i.UserId == objUserMst.RefferalUserId).FirstOrDefault();

                                bool IsUserRegistrationActivated = (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value);

                                WriteLog("5_");

                                var TempProductWalletDebitDtlId = Convert.ToInt64(tempid);
                                var objTempProductWalletDebitDtl = context.TempProductWalletDebitDtls.Where(i => i.TempProductWalletDebitDtlId == TempProductWalletDebitDtlId).FirstOrDefault();

                                #region Purchase Orders

                                double TotalAmount = 0;
                                //var SplitProductInfo = pinfo.Split(',');
                                var SplitProductInfo = objManager.Decrypt(pinfo).Split(',');
                                var objUserPurchaseHdr = new UserPurchaseHdrTemp();
                                var listUserPurchaseDtl = new List<UserPurchaseDtlTemp>();

                                WriteLog("6_");

                                foreach (var products in SplitProductInfo)
                                {
                                    double MatchingPerc = 0;
                                    double DirectMatchingPerc = 0;
                                    decimal CreditAmount_POINTSCREDITDTL = 0;
                                    var productInfo = products.Split('_');
                                    var prodid = Convert.ToInt32(productInfo[0]);
                                    var qty = Convert.ToInt32(productInfo[1]);
                                    var pvid = 0;
                                    if (productInfo.Length > 2)
                                    {
                                        pvid = Convert.ToInt32(productInfo[2]);
                                    }
                                    var objProductMst = context.ProductMsts.Where(i => i.ProductId == prodid).FirstOrDefault();

                                    objProductMst.Stock = objProductMst.Stock.HasValue ? objProductMst.Stock.Value - qty : -qty;

                                    TotalAmount += (qty * objProductMst.SalePrice.Value);
                                    var objUserPurchaseDtl = new UserPurchaseDtlTemp();
                                    objUserPurchaseDtl.Amount = (qty * objProductMst.SalePrice.Value);
                                    objUserPurchaseDtl.CreatedBy = objclsLogin.UserId;
                                    objUserPurchaseDtl.CreatedDate = dt_CurrentDatetime;
                                    objUserPurchaseDtl.IsActive = true;
                                    objUserPurchaseDtl.ProductId = prodid;
                                    objUserPurchaseDtl.ProductPrice = objProductMst.SalePrice.Value;
                                    objUserPurchaseDtl.RegularPrice = objProductMst.RegularPrice.Value;
                                    objUserPurchaseDtl.Qty = qty;
                                    objUserPurchaseDtl.ProductVariantId = pvid;

                                    #region Points

                                    if (!AdminConfirmationRequired)
                                    {
                                        if (OrderReceivedById > 0)
                                        {
                                            objUserPurchaseHdr.OrderReceivedById = Convert.ToInt32(OrderReceivedById);
                                        }
                                        if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                        {
                                            WriteLog("6_1.1");
                                            objManager.InsertUserPointsOnPurchase_Temp(context, objUserPurchaseDtl, WalletCreditType.Direct);
                                            WriteLog("6_1.2");
                                            objManager.CalculateWalletCreditAmountRegistration(objProductMst, qty, ref CreditAmount, WalletCreditType.Direct);
                                            WriteLog("6_1.3");
                                        }
                                        else
                                        {
                                            WriteLog("6_1.1");
                                            objManager.InsertUserPointsOnPurchase_Temp(context, objUserPurchaseDtl, WalletCreditType.Repurchase);
                                            WriteLog("6_1.2");
                                        }
                                    }

                                    #endregion Points

                                    objUserPurchaseHdr.UserPurchaseDtlTemps.Add(objUserPurchaseDtl);

                                    WriteLog("7_");
                                }

                                objUserPurchaseHdr.IsCODOrder = IsCOD;
                                objUserPurchaseHdr.Notes = OrderNote;
                                objUserPurchaseHdr.IsOrderPlaceByAdmin = objclsLogin.RedirectFrmAdmin;
                                //objUserPurchaseHdr.OrderNo = Convert.ToInt64(Convert.ToInt64(objManager.GenerateOrderNo(true)));
                                objUserPurchaseHdr.CreatedBy = objclsLogin.UserId;
                                objUserPurchaseHdr.CreatedDate = dt_CurrentDatetime;
                                objUserPurchaseHdr.IsActive = true;
                                objUserPurchaseHdr.OrderDate = dt_CurrentDatetime;
                                objUserPurchaseHdr.TotalOrderAmount = (TotalAmount - MilesAmt) + OrdDeliveryCharge;
                                objUserPurchaseHdr.UserId = objclsLogin.UserId;
                                if ((bool)!objUserPurchaseHdr.IsOrderPlaceByAdmin)
                                {
                                    objUserPurchaseHdr.RazorpayOrderId = OrderId;
                                    objUserPurchaseHdr.RazorpayPaymentId = PaymentId;
                                    objUserPurchaseHdr.RazorpaySignature = Signature;
                                }
                                else
                                {
                                    objUserPurchaseHdr.RazorpayOrderId = RazorpayOrderId;
                                    objUserPurchaseHdr.RazorpayPaymentId = RazorpayPaymentId;
                                    objUserPurchaseHdr.RazorpaySignature = RazorpaySignature;
                                }
                                if (!string.IsNullOrEmpty(DeliveryAddress) && !string.IsNullOrWhiteSpace(DeliveryAddress))
                                {
                                    objUserPurchaseHdr.DeliveryAddress = DeliveryAddress;
                                }
                                if (AdminConfirmationRequired)
                                {
                                    objUserPurchaseHdr.OrderStatusId = OrderStatus.OrderGenerate.GetHashCode();
                                }
                                else
                                {
                                    objUserPurchaseHdr.OrderStatusId = OrderStatus.OrderConfirm.GetHashCode();
                                }
                                if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                {
                                    objUserPurchaseHdr.IsRepurchaseOrder = false;
                                }
                                else
                                {
                                    objUserPurchaseHdr.IsRepurchaseOrder = true;
                                }

                                objUserPurchaseHdr.DeliveryCharge = OrdDeliveryCharge;

                                context.UserPurchaseHdrTemps.Add(objUserPurchaseHdr);

                                WriteLog("11_");

                                #endregion Purchase Orders

                                WriteLog("15_");

                                context.SaveChanges();
                                transaction.Commit();
                                UserPurchaseHdrId = objUserPurchaseHdr.UserPurchaseHdrId;

                                WriteLog("16_A");
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            ErrorSignal.FromCurrentContext().Raise(ex);
                            //return RedirectToAction("Index", "Purchase", new { area = "Member" });
                            return false;
                        }
                    }
                }
                Status = true;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            //return RedirectToAction("Index", "Dashboard", new { area = "Member" });
            return Status;
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2").ToLower());
            }
            return result.ToString();
        }

        //No Set as SessionExpired Filter becase use from authenticate view
        public JsonResult AddProductToCart(List<SelectedProduct> productinfo)
        {
            AddCartResponse response = new AddCartResponse();
            var pinfo = "";
            var ProductCnt = 0;
            try
            {
                double CreditAmount = 0;

                if (productinfo != null && productinfo.Count > 0)
                {
                    var productsId = productinfo.Select(i => i.ProductId);
                    var listProducts = db.ProductMsts.Where(i => productsId.Contains(i.ProductId));
                    foreach (var item in listProducts)
                    {
                        var SelectedProdInfo = productinfo.Where(i => i.ProductId == item.ProductId).FirstOrDefault();
                        if (pinfo.Length == 0)
                        {
                            pinfo = item.ProductId + "_" + SelectedProdInfo.Qty + "_" + SelectedProdInfo.ProductVariantId;
                        }
                        else
                        {
                            pinfo += "," + item.ProductId + "_" + SelectedProdInfo.Qty + "_" + SelectedProdInfo.ProductVariantId;
                        }

                        if (!objclsLogin.IsRegistrationActivated)
                        {
                            objManager.CalculateWalletCreditAmountRegistration(item, SelectedProdInfo.Qty, ref CreditAmount, WalletCreditType.Direct);
                        }
                        else
                        {
                            objManager.CalculateWalletCreditAmountRegistration(item, SelectedProdInfo.Qty, ref CreditAmount, WalletCreditType.Repurchase);
                        }
                    }
                    if (pinfo != "")
                    {
                        pinfo = objManager.Encrypt(pinfo);
                        ProductCnt = listProducts.Count();
                    }
                }
                response.Key = true;
                response.Msg = pinfo;
                response.Id = ProductCnt;
                response.RegistrationPoints = CreditAmount.ToString();
                response.IsRegistrationActivated = objclsLogin.IsRegistrationActivated;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCartCnt(string cart)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var SplitProductInfo = objManager.Decrypt(cart).Split(',');
                response.Key = true;
                response.Msg = SplitProductInfo.Count().ToString();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCartProductList(string cart)
        {
            List<CartProducts> listCartProducts = new List<CartProducts>();
            try
            {
                cart = cart.Replace(" ", "+");
                var SplitProductInfo = objManager.Decrypt(cart).Split(',');
                foreach (var products in SplitProductInfo)
                {
                    var productInfo = products.Split('_');
                    var prodid = Convert.ToInt32(productInfo[0]);
                    var qty = Convert.ToInt32(productInfo[1]);
                    var pvid = 0;
                    if (productInfo.Length > 2)
                    {
                        pvid = Convert.ToInt32(productInfo[2]);
                    }
                    listCartProducts.Add(new CartProducts
                    {
                        ProductId = prodid,
                        Qty = qty,
                        ProductVariantId = pvid
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listCartProducts, JsonRequestBehavior.AllowGet);
        }

        //#region Points Purchase
        //[SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        //public ActionResult Points()
        //{
        //    ViewBag.TotalPoint = objManager.GetFinalUserPoints(objclsLogin.UserId);
        //    var listProducts = db.PackageMsts.Where(i => i.IsActive == true).ToList();
        //    return View(listProducts);
        //}
        //[SessionExpireFilter]
        //public JsonResult SavePointsPurchase(List<SelectedProduct> productinfo)
        //{
        //    PaymentDtlEnt response = new PaymentDtlEnt();
        //    try
        //    {
        //        //clsLogin objclsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
        //        if (productinfo != null && productinfo.Count > 0)
        //        {
        //            var pinfo = "";
        //            var PackageIds = productinfo.Select(i => i.ProductId);
        //            var listPackages = db.PackageMsts.Where(i => PackageIds.Contains(i.PackageId));
        //            double amount = 0;
        //            foreach (var item in listPackages)
        //            {
        //                var SelectedProdInfo = productinfo.Where(i => i.ProductId == item.PackageId).FirstOrDefault();
        //                if (pinfo.Length == 0)
        //                {
        //                    pinfo = item.PackageId + "_" + SelectedProdInfo.Qty;
        //                }
        //                else
        //                {
        //                    pinfo += "," + item.PackageId + "_" + SelectedProdInfo.Qty;
        //                }
        //                amount += (SelectedProdInfo.Qty * item.PackagePrice.Value);
        //            }
        //            var TotalPoint = objManager.GetFinalUserPoints(objclsLogin.UserId);
        //            if (TotalPoint < amount)
        //            {
        //                response.Key = false;
        //                response.IsInfo = true;
        //                response.Msg = "You Can Not Purchase More Than " + TotalPoint.ToString() + " Points!";
        //                return Json(response, JsonRequestBehavior.AllowGet);
        //            }
        //            else
        //            {
        //                pinfo = objManager.Encrypt(pinfo);
        //                var Status = objManager.InsertPointsPurchase(objclsLogin.UserId, pinfo);
        //                if (Status)
        //                {
        //                    response.Key = true;
        //                    response.Msg = "Points Purchase Successfully!";
        //                }
        //                else
        //                {
        //                    response.Key = false;
        //                    response.Msg = "Something Went Wrong! Try After Some Time!";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            response.Key = false;
        //            response.IsInfo = true;
        //            response.Msg = "Please Select Atleast One Product For Purchase!";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Key = false;
        //        ErrorSignal.FromCurrentContext().Raise(ex);
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}
        //#endregion
    }
}