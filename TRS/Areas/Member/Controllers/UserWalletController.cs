﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRS.Models;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Member.Controllers
{
    //[NoCache]
    public class UserWalletController : clsBase
    {
        // GET: Member/UserWallet/Index        
        [SessionExpireFilter,OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            UserWalletEnt ent = new UserWalletEnt();            
            try
            {
                int Direct = Convert.ToInt32(WalletCreditType.Direct);
                int Repurchase = Convert.ToInt32(WalletCreditType.Repurchase);
                ent.listCashWallet= db.PROC_GET_USER_WALLET_DETAILS(objclsLogin.UserId,null,null).ToList();
                ent.listPointsWallet = db.PROC_GET_USER_POINTS_DETAILS(objclsLogin.UserId).ToList();
                double TotalWallet = (double)ent.listCashWallet.Sum(i => i.CreditAmount);
                double CreditPoints = (double)ent.listPointsWallet.Sum(i => i.CreditPoints);
                double DebitPoints = (double)ent.listPointsWallet.Sum(i => i.DebitPoints);
                ViewBag.TotalPoints =(Int64)( CreditPoints - DebitPoints);
                ViewBag.TotalWallet = TotalWallet;
                ViewBag.Direct = ent.listCashWallet.Where(i => i.CreditType == Direct).Sum(i => i.CreditAmount);
                ViewBag.Repurchase = ent.listCashWallet.Where(i => i.CreditType == Repurchase).Sum(i => i.CreditAmount);

                var PlanId = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault().PlanId;
                var objPlan = db.PlanMsts.Where(i => i.PlanId == PlanId).FirstOrDefault();
                ViewBag.ProductWallet = objManager.GetFinalProductWalletAmountForUser(objclsLogin.UserId);
                ent.listCashWallet = ent.listCashWallet.Where(i => i.CreditType == Direct).ToList();
                var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
                ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
                ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(ent);
        }

        public PartialViewResult GetDateRangeDirechCreditDetails(string FromDate, string ToDate)
        {
            UserWalletEnt ent = new UserWalletEnt();
            try
            {
                int Direct = Convert.ToInt32(WalletCreditType.Direct);
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                ent.listCashWallet = db.PROC_GET_USER_WALLET_DETAILS(objclsLogin.UserId, dtFromDate, dtToDate).Where(i =>i.CreditType == Direct).OrderByDescending(i=>i.WalletCreditDtlId).ToList();                
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeDirectCreditDetails", ent);
        }
        // GET: Member/UserWallet/Repurchase        
        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Repurchase()
        {
            UserWalletEnt ent = new UserWalletEnt();
            try
            {
                int Repurchase = Convert.ToInt32(WalletCreditType.Repurchase);
                ent.listCashWallet = db.PROC_GET_USER_WALLET_DETAILS(objclsLogin.UserId,null,null).Where(i => i.CreditType == Repurchase).ToList();                
                double TotalWallet = (double)ent.listCashWallet.Sum(i => i.CreditAmount);                
                ViewBag.TotalWallet = TotalWallet;
                var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
                ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
                ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(ent);
        }

        public PartialViewResult GetDateRangeRepurchaseCreditDetails(string FromDate, string ToDate)
        {
            UserWalletEnt ent = new UserWalletEnt();
            try
            {
                int Repurchase = Convert.ToInt32(WalletCreditType.Repurchase);
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                ent.listCashWallet = db.PROC_GET_USER_WALLET_DETAILS(objclsLogin.UserId, dtFromDate, dtToDate).Where(i => i.CreditType == Repurchase).OrderByDescending(i => i.WalletCreditDtlId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeRepurchaseCreditDetails", ent);
        }
        // GET: Member/UserWallet/Points       
        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Points()
        {
            UserWalletEnt ent = new UserWalletEnt();
            try
            {
                ent.listPointsWallet = db.PROC_GET_USER_POINTS_DETAILS(objclsLogin.UserId).ToList();                
                double CreditPoints = (double)ent.listPointsWallet.Sum(i => i.CreditPoints);
                double DebitPoints = (double)ent.listPointsWallet.Sum(i => i.DebitPoints);
                ViewBag.TotalPoints = (Int64)(CreditPoints - DebitPoints);
                var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
                ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
                ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(ent);
        }
        public PartialViewResult GetDateRangePointsDetails(string FromDate, string ToDate)
        {
            UserWalletEnt ent = new UserWalletEnt();
            try
            {
                int Repurchase = Convert.ToInt32(WalletCreditType.Repurchase);
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                ent.listPointsWallet = db.PROC_GET_USER_POINTS_DETAILS(objclsLogin.UserId).Where(i => i.CreditDebitDate >= dtFromDate && i.CreditDebitDate <= dtToDate).OrderByDescending(i => i.CreditDebitDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangePointsDetails", ent);
        }
        // GET: Member/UserWallet/Points       
        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ProductWallet()
        {
            List<PROC_GET_USER_PRODUCT_WALLET_DETAILS_Result> listPROC_GET_USER_PRODUCT_WALLET_DETAILS = new List<PROC_GET_USER_PRODUCT_WALLET_DETAILS_Result>();
            try
            {
                listPROC_GET_USER_PRODUCT_WALLET_DETAILS = db.PROC_GET_USER_PRODUCT_WALLET_DETAILS(objclsLogin.UserId).ToList();
                double CreditAmount = (double)listPROC_GET_USER_PRODUCT_WALLET_DETAILS.Sum(i => i.CreditAmount);
                double DebitAmount = (double)listPROC_GET_USER_PRODUCT_WALLET_DETAILS.Sum(i => i.DebitAmount);
                ViewBag.TotalProductWallet = (Int64)(CreditAmount - DebitAmount);
                var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
                ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
                ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listPROC_GET_USER_PRODUCT_WALLET_DETAILS);
        }
        public PartialViewResult GetDateRangeProductWalletDetails(string FromDate, string ToDate)
        {
            List<PROC_GET_USER_PRODUCT_WALLET_DETAILS_Result> listPROC_GET_USER_PRODUCT_WALLET_DETAILS = new List<PROC_GET_USER_PRODUCT_WALLET_DETAILS_Result>();
            try
            {
                int Repurchase = Convert.ToInt32(WalletCreditType.Repurchase);
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                listPROC_GET_USER_PRODUCT_WALLET_DETAILS = db.PROC_GET_USER_PRODUCT_WALLET_DETAILS(objclsLogin.UserId).Where(i => i.CreditDebitDate >= dtFromDate && i.CreditDebitDate <= dtToDate).OrderByDescending(i => i.CreditDebitDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeProductWalletDetails", listPROC_GET_USER_PRODUCT_WALLET_DETAILS);
        }
    }
}