﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TRSImplementation;
using TRS.Models;
using System.Web.UI;
using Elmah;
using System.Web.Helpers;
using DocumentFormat.OpenXml.Drawing;
using System.IO;

namespace TRS.Areas.Member.Controllers
{
    //[NoCache]
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class UserProfileController : clsBase
    {
        // GET: Member/UserProfile
        [SessionExpireFilter]
        public ActionResult Index()
        {
            PROC_GET_USER_PROFILE_DETAILS_Result objProfile = new PROC_GET_USER_PROFILE_DETAILS_Result();
            try
            {
                objProfile = db.PROC_GET_USER_PROFILE_DETAILS(objclsLogin.UserId).FirstOrDefault();
                if (string.IsNullOrEmpty(objProfile.ProfileImage))
                    objProfile.ProfileImage = "/Content/images/gallary/10.png";
                else
                    objProfile.ProfileImage = "/Images/ProfileImages/" + objProfile.ProfileImage + "?id=" + objManager.GetTimespam();

                if (!string.IsNullOrEmpty(objProfile.KYCImage))
                    objProfile.KYCImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objclsLogin.RefferalCode}/" + objProfile.KYCImage + "?id=" + objManager.GetTimespam();

                if (!string.IsNullOrEmpty(objProfile.KYCBackImage))
                    objProfile.KYCBackImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objclsLogin.RefferalCode}/" + objProfile.KYCBackImage + "?id=" + objManager.GetTimespam();

                if (!string.IsNullOrEmpty(objProfile.PANImage))
                    objProfile.PANImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objclsLogin.RefferalCode}/" + objProfile.PANImage + "?id=" + objManager.GetTimespam();

                if (!string.IsNullOrEmpty(objProfile.BankDocument))
                    objProfile.BankDocument = Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objclsLogin.RefferalCode}/" + objProfile.BankDocument + "?id=" + objManager.GetTimespam();

                ViewBag.Cities = db.CityMsts.OrderBy(i => i.CityName).Select(i => new AutoCompleteEnt { key = i.CityId.ToString(), val = i.CityName.Replace(" ", "_").Replace("#", "_xxx_") }).ToList();
                if (objProfile.DOB.HasValue)
                {
                    ViewBag.DOB = objProfile.DOB.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    ViewBag.DOB = "";
                }

                ViewBag.StateTbl = db.StateMsts.OrderBy(x => x.StateName).Select(i => new AutoCompleteEnt { key = i.StateId.ToString(), val = i.StateName.Replace(" ", "_").Replace("#", "_xxx_") }).ToList();
                if (objProfile.CityId != 0)
                {
                    var ObjCity = db.CityMsts.Where(x => x.CityId == objProfile.CityId).FirstOrDefault();
                    if (ObjCity != null)
                        ViewBag.StateId = ObjCity.StateId;
                    else
                        ViewBag.StateId = 0;
                }
                else
                    ViewBag.StateId = 0;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(objProfile);
        }

        [SessionExpireFilter]
        public JsonResult UpdateUserProfile(UserProfileEnt user)
        {
            ResponseMsg response = new ResponseMsg();
            decimal CityId = 0;
            try
            {
                if (!objManager.CheckUserNameExist(user.UserName, objclsLogin.UserId) && !objManager.CheckEmailIdExist(user.EmailId, objclsLogin.UserId))
                {
                    if (!string.IsNullOrEmpty(user.CityName))
                    {
                        var objCity = db.CityMsts.Where(i => i.CityName.Trim() == user.CityName.Replace("_xxx_", "#").Replace("_", " ")).FirstOrDefault();
                        if (objCity != null)
                        {
                            CityId = objCity.CityId;
                        }
                        else
                        {
                            if (user.StateId != "0")
                            {
                                using (var context = new TRSEntities(3600))
                                {
                                    //var ObjStateMst = context.StateMsts.Where(x=>x.StateName.Trim() == StateName.Replace("_xxx_", "#").Replace("_", " ")).FirstOrDefault();
                                    CityMst ObjCityMst = new CityMst();
                                    ObjCityMst.CityName = user.CityName.Replace("_xxx_", "#").Replace("_", " ");
                                    ObjCityMst.StateId = Convert.ToInt32(user.StateId);
                                    ObjCityMst.IsActive = true;
                                    ObjCityMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                                    ObjCityMst.CreatedBy = objclsLogin.UserId;
                                    ObjCityMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                                    ObjCityMst.ModifiedBy = objclsLogin.UserId;

                                    context.CityMsts.Add(ObjCityMst);

                                    context.SaveChanges();

                                    CityId = ObjCityMst.CityId;
                                }
                            }
                        }
                    }

                    var objUserMst = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                    objUserMst.Address = !string.IsNullOrEmpty(user.Address) ? user.Address.Trim() : null;
                    objUserMst.AdharNo = !string.IsNullOrEmpty(user.AdharNo) ? user.AdharNo.Trim() : null;
                    objUserMst.BankAccNo = !string.IsNullOrEmpty(user.BankAccNo) ? user.BankAccNo.Trim() : null;
                    objUserMst.BankName = !string.IsNullOrEmpty(user.BankName) ? user.BankName.Trim() : null;
                    objUserMst.BranchName = !string.IsNullOrEmpty(user.BranchName) ? user.BranchName.Trim() : null;
                    objUserMst.FirstName = !string.IsNullOrEmpty(user.FirstName) ? user.FirstName.Trim() : null;
                    objUserMst.ISFCCode = !string.IsNullOrEmpty(user.ISFCCode) ? user.ISFCCode.Trim() : null;
                    objUserMst.BankingName = !string.IsNullOrEmpty(user.BankingName) ? user.BankingName.Trim() : null;
                    objUserMst.LastName = !string.IsNullOrEmpty(user.LastName) ? user.LastName.Trim() : null;
                    objUserMst.MiddleName = !string.IsNullOrEmpty(user.MiddleName) ? user.MiddleName.Trim() : null;
                    objUserMst.MobileNo = user.MobileNo;
                    objUserMst.PANNo = !string.IsNullOrEmpty(user.PANNo) ? user.PANNo.Trim() : null;
                    objUserMst.FullName = user.FirstName + " " + user.MiddleName + " " + user.LastName;
                    objUserMst.CityId = CityId;
                    objUserMst.Gender = user.Gender;
                    objUserMst.AlternativeMobileNo = user.AlternativeMobileNo;
                    objUserMst.PinCode = !string.IsNullOrEmpty(user.PinCode) ? user.PinCode.Trim() : null;
                    objUserMst.GSTNo = !string.IsNullOrEmpty(user.GSTNo) ? user.GSTNo.Trim() : null;
                    objUserMst.TradeName = !string.IsNullOrEmpty(user.TradeName) ? user.TradeName.Trim() : null;
                    objUserMst.NomineeName = !string.IsNullOrEmpty(user.NomineeName) ? user.NomineeName.Trim() : null;
                    objUserMst.UserName = !string.IsNullOrEmpty(user.UserName) ? user.UserName.Trim() : null;
                    objUserMst.EmailId = !string.IsNullOrEmpty(user.EmailId) ? user.EmailId.Trim() : null;
                    if (!string.IsNullOrEmpty(user.DateOfBirth))
                    {
                        objUserMst.DOB = objManager.ConvertDDMMYYToMMDDYY(user.DateOfBirth, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                    }
                    else
                    {
                        objUserMst.DOB = null;
                    }

                    var objUserAddressLst = db.UserAddressDtls.Where(x => x.UserId == objclsLogin.UserId && x.IsMasterAddress == true);
                    if (objUserAddressLst.Any())
                    {
                        var varUserAddressLst = db.UserAddressDtls.Where(x => x.UserId == objclsLogin.UserId && x.IsMasterAddress == true).FirstOrDefault();

                        varUserAddressLst.Address = !string.IsNullOrEmpty(user.Address) ? user.Address.Trim() : null;
                        varUserAddressLst.CityId = CityId;
                        varUserAddressLst.PinCode = !string.IsNullOrEmpty(user.PinCode) ? user.PinCode.Trim() : null;
                        varUserAddressLst.ModifiedOn = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        varUserAddressLst.ModifiedBy = objclsLogin.UserId;
                    }
                    else
                    {
                        UserAddressDtl useraddresslst = new UserAddressDtl();
                        useraddresslst.UserId = objclsLogin.UserId;
                        useraddresslst.Address = !string.IsNullOrEmpty(user.Address) ? user.Address.Trim() : null;
                        useraddresslst.CityId = CityId;
                        useraddresslst.PinCode = !string.IsNullOrEmpty(user.PinCode) ? user.PinCode.Trim() : null;
                        useraddresslst.IsMasterAddress = true;
                        useraddresslst.CreatedOn = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        useraddresslst.CreatedBy = objclsLogin.UserId;
                        useraddresslst.IsActive = true;

                        db.UserAddressDtls.Add(useraddresslst);
                    }

                    var objLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                    objLogin.IsProfileCompleted = !string.IsNullOrEmpty(objUserMst.Address) ? true : false;
                    db.SaveChanges();
                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "User Name/EmailId Already Exist!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult UpdateUserProfileImage()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var UserId = objclsLogin.UserId;
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string FileName = "";
                    var pic = System.Web.HttpContext.Current.Request.Files["fuProfileImage"];
                    if (pic.ContentLength > 0)
                    {
                        if (!System.IO.Directory.Exists(Server.MapPath("/Images/ProfileImages/")))
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("/Images/ProfileImages/"));
                        }
                        FileName = UserId + ("." + pic.FileName.Split('.').Last());
                        var _comPath = Server.MapPath("/Images/ProfileImages/") + FileName;
                        if (System.IO.File.Exists(_comPath))
                        {
                            System.IO.File.Delete(_comPath);
                        }
                        var path = _comPath;
                        //pic.SaveAs(path);

                        WebImage img = new WebImage(pic.InputStream);
                        if (img.Height > img.Width)
                        {
                            img.Resize(400, 600, true, true);
                            img.Save(path);
                        }
                        else
                        {
                            img.Resize(600, 400, true, true);
                            img.Save(path);
                        }

                        var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                        objUserMst.ProfileImage = FileName;
                        db.SaveChanges();
                        response.Msg = "/Images/ProfileImages/" + FileName + "?id=" + objManager.GetTimespam();
                        response.Key = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult UpdateKYCImage()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var UserId = objclsLogin.UserId;
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string KYCImage = null;
                    string KYCBackImage = null;

                    var front = System.Web.HttpContext.Current.Request.Files["fupKYCImageFront"];
                    var back = System.Web.HttpContext.Current.Request.Files["fupKYCImageBack"];

                    var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();

                    if (front != null)
                    {
                        if (System.IO.Path.GetExtension(front.FileName.ToLower()) == ".pdf")
                            KYCImage = UploadPDF(front);
                        else
                            KYCImage = UploadImage(front);

                        objUserMst.KYCImage = KYCImage;
                    }

                    if (back != null)
                    {
                        if (System.IO.Path.GetExtension(back.FileName.ToLower()) == ".pdf")
                            KYCBackImage = UploadPDF(back);
                        else
                            KYCBackImage = UploadImage(back);

                        objUserMst.KYCBackImage = KYCBackImage;
                    }

                    objUserMst.KYCRejectComment = null;
                    if (objclsLogin.RedirectFrmAdmin)
                        objUserMst.IsKYCVerified = true;
                    db.SaveChanges();
                    response.Msg = $"/Documents/{objclsLogin.RefferalCode}/" + KYCImage + "?id=" + objManager.GetTimespam();

                    if (!string.IsNullOrEmpty(objUserMst.KYCBackImage))
                        response.Msg += $",/Documents/{objclsLogin.RefferalCode}/" + KYCBackImage + "?id=" + objManager.GetTimespam();

                    response.Key = true;
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult UpdatePANImage()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var UserId = objclsLogin.UserId;
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string FileName = "";
                    var pic = System.Web.HttpContext.Current.Request.Files["fupPANImage"];
                    if (pic.ContentLength > 0)
                    {
                        var fileDir = Server.MapPath($"/Documents/{objclsLogin.RefferalCode}/");
                        FileName = $"{Guid.NewGuid().ToString()}{System.IO.Path.GetExtension(pic.FileName)}";
                        if (!System.IO.Directory.Exists(fileDir))
                            System.IO.Directory.CreateDirectory(fileDir);

                        var _comPath = fileDir + FileName;

                        if (System.IO.File.Exists(_comPath))
                            System.IO.File.Delete(_comPath);

                        WebImage img = new WebImage(pic.InputStream);
                        if (img.Height > img.Width)
                        {
                            img.Resize(400, 600, true, true);
                            img.Save(_comPath);
                        }
                        else
                        {
                            img.Resize(600, 400, true, true);
                            img.Save(_comPath);
                        }

                        var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                        objUserMst.PANImage = FileName;
                        objUserMst.PANRejectComment = null;
                        if (objclsLogin.RedirectFrmAdmin)
                            objUserMst.IsPANVerified = true;
                        db.SaveChanges();
                        response.Msg = $"/Documents/{objclsLogin.RefferalCode}/" + FileName + "?id=" + objManager.GetTimespam();
                        response.Key = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult UpdateBankDocument()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var UserId = objclsLogin.UserId;
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string FileName = "";
                    var pic = System.Web.HttpContext.Current.Request.Files["fupBankDocument"];
                    if (pic.ContentLength > 0)
                    {
                        var fileDir = Server.MapPath($"/Documents/{objclsLogin.RefferalCode}/");
                        FileName = $"{Guid.NewGuid().ToString()}{System.IO.Path.GetExtension(pic.FileName)}";

                        if (!System.IO.Directory.Exists(fileDir))
                            System.IO.Directory.CreateDirectory(fileDir);

                        var _comPath = fileDir + FileName;
                        if (System.IO.File.Exists(_comPath))
                            System.IO.File.Delete(_comPath);

                        WebImage img = new WebImage(pic.InputStream);
                        if (img.Height > img.Width)
                        {
                            img.Resize(400, 600, true, true);
                            img.Save(_comPath);
                        }
                        else
                        {
                            img.Resize(600, 400, true, true);
                            img.Save(_comPath);
                        }

                        var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                        objUserMst.BankDocument = FileName;
                        objUserMst.BankDocumentRejectComment = null;
                        if (objclsLogin.RedirectFrmAdmin)
                            objUserMst.IsBankDocumentVerified = true;
                        db.SaveChanges();
                        response.Msg = $"/Documents/{objclsLogin.RefferalCode}/" + FileName + "?id=" + objManager.GetTimespam();
                        response.Key = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult GetUserProfileImage()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objUserMst = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();

                if (string.IsNullOrEmpty(objUserMst.ProfileImage))
                {
                    response.Msg = "/Content/images/avatar/avatar-7.png";
                }
                else
                {
                    response.Msg = "/Images/ProfileImages/" + objUserMst.ProfileImage + "?id=" + objManager.GetTimespam();
                }
            }
            catch (Exception ex)
            {
                response.Msg = "/Content/images/avatar/avatar-7.png";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult ClearUserProfileImage()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objUserMst = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                objUserMst.ProfileImage = null;
                if (string.IsNullOrEmpty(objUserMst.ProfileImage))
                {
                    response.Msg = "/Content/images/avatar/avatar-7.png";
                }
                else
                {
                    response.Msg = "/Images/ProfileImages/" + objUserMst.ProfileImage + "?id=" + objManager.GetTimespam();
                }
                db.SaveChanges();
                response.Key = true;
            }
            catch (Exception ex)
            {
                response.Msg = "/Content/images/avatar/avatar-7.png";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult ChangePassword(string oldpassword, string newpassword)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objUserMst = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                if (objUserMst.Password != oldpassword)
                {
                    response.Key = false;
                    response.Msg = "Wrong old password!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                objUserMst.Password = newpassword.Trim();
                db.SaveChanges();
                response.Key = true;
                response.Msg = "password update successfully!";
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something went wrong!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private string UploadImage(HttpPostedFile file)
        {
            string FileName = "";
            if (file != null && file.ContentLength > 0)
            {
                var fileDir = Server.MapPath($"/Documents/{objclsLogin.RefferalCode}/");
                FileName = $"{Guid.NewGuid()}{System.IO.Path.GetExtension(file.FileName)}";
                if (!System.IO.Directory.Exists(fileDir))
                {
                    System.IO.Directory.CreateDirectory(fileDir);
                }

                var _comPath = fileDir + FileName;
                if (System.IO.File.Exists(_comPath))
                {
                    System.IO.File.Delete(_comPath);
                }
                var path = _comPath;

                WebImage img = new WebImage(file.InputStream);
                if (img.Height > img.Width)
                {
                    img.Resize(400, 600, true, true);
                    img.Save(path);
                }
                else
                {
                    img.Resize(600, 400, true, true);
                    img.Save(path);
                }
            }
            return FileName;
        }

        private string UploadPDF(HttpPostedFile file)
        {
            string FileName = "";
            if (file != null && file.ContentLength > 0)
            {
                var fileDir = Server.MapPath($"/Documents/{objclsLogin.RefferalCode}/");
                FileName = $"{Guid.NewGuid()}{System.IO.Path.GetExtension(file.FileName)}";
                if (!System.IO.Directory.Exists(fileDir))
                {
                    System.IO.Directory.CreateDirectory(fileDir);
                }

                var _comPath = fileDir + FileName;
                if (System.IO.File.Exists(_comPath))
                {
                    System.IO.File.Delete(_comPath);
                }
                var path = _comPath;
                file.SaveAs(path);
            }
            return FileName;
        }

        [SessionExpireFilter]
        public JsonResult ValidateKYC(string panNo, string userName)
        {
            bool isValid = true;

            try
            {
                var userData = db.UserMsts.Where(u => u.PANNo == panNo && u.UserName != userName).ToList();
                if (userData != null && userData.Count > 0)
                {
                    isValid = false;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(isValid);
        }
    }
}