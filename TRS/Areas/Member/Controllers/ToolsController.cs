﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TRS.Models;
using TRSImplementation;

namespace TRS.Areas.Member.Controllers
{
    public class ToolsController : clsBase
    {
        // GET: Member/Tools
        [SessionExpireFilter]
        public ActionResult ProductVideo()
        {
            List<PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT_Result> listProductMst = new List<PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT_Result>();
            listProductMst = db.PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT(0).Where(i=>i.Language=="E" && i.IsActive==true).ToList();                        
            return View(listProductMst);
        }
        [SessionExpireFilter]
        public ActionResult ProductVideoHindi()
        {
            List<PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT_Result> listProductMst = new List<PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT_Result>();
            listProductMst = db.PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT(0).Where(i => i.Language == "H" && i.IsActive == true).ToList();
            return View(listProductMst);
        }
    }
}