﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRS.Models;
using TRSImplementation;

namespace TRS.Areas.Member.Controllers
{
    //[NoCache]
    public class DashboardController : clsBase
    {
        // GET: Member/Dashboard/Index
        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            DashboardEnt dashboard = new DashboardEnt();

            try
            {
                var objJoiningMember = db.PROC_GET_TEAM_DIRECT_JOINING_REPORT(objclsLogin.UserId).ToList();
                ViewBag.JoinMonths = objJoiningMember.OrderBy(i => i.Id).Select(i => i.MonthYear).ToArray();
                ViewBag.TotalMembers = db.UserMsts.Where(i => i.RefferalUserId == objclsLogin.UserId && i.IsActive == true).Count();
                ViewBag.InActiveRegistrationCntMonthsValue = objJoiningMember.OrderBy(i => i.Id).Select(i => i.InActiveRegistrationCnt).ToArray();
                ViewBag.ActiveRegistrationCntMonthsValue = objJoiningMember.OrderBy(i => i.Id).Select(i => i.ActiveRegistrationCnt).ToArray();

                var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();

                var objLeftPoints = db.PROC_GET_USER_LEFT_SIDE_POINTS(objclsLogin.UserId, null, null).ToList();
                var objRightPoints = db.PROC_GET_USER_RIGHT_SIDE_POINTS(objclsLogin.UserId, null, null).ToList();
                var objTotalLeaderPoints = db.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(true, objclsLogin.UserId, null, null).FirstOrDefault();
                var objTotalUplineSupportPoints = db.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(false, objclsLogin.UserId, null, null).FirstOrDefault();

                var objTotalLeaderPointsWeek = db.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(true, objclsLogin.UserId, objWeekDatas.StartDate, objWeekDatas.EndDate).FirstOrDefault();
                var objTotalUplineSupportPointsWeek = db.FN_GET_USER_LEADER_UPLINE_SUPPORT_TOTAL_POINTS(false, objclsLogin.UserId, objWeekDatas.StartDate, objWeekDatas.EndDate).FirstOrDefault();

                var objTotalDiamondPoints = db.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(true, objclsLogin.UserId, null, null).FirstOrDefault();
                var objTotalSilverPoints = db.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(false, objclsLogin.UserId, null, null).FirstOrDefault();

                var objTotalDiamondPointsWeek = db.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(true, objclsLogin.UserId, objWeekDatas.StartDate, objWeekDatas.EndDate).FirstOrDefault();
                var objTotalSilverPointsWeek = db.FN_GET_USER_DIAMOND_AND_SILVER_TOTAL_POINTS(false, objclsLogin.UserId, objWeekDatas.StartDate, objWeekDatas.EndDate).FirstOrDefault();

                var TotalLeaderIncome = objTotalLeaderPoints != null ? objTotalLeaderPoints.CreditPoints : 0;
                var TotalUplineSupportIncome = objTotalUplineSupportPoints != null ? objTotalUplineSupportPoints.CreditPoints : 0;
                var TotalLeaderIncomeWeek = objTotalLeaderPointsWeek != null ? objTotalLeaderPointsWeek.CreditPoints : 0;
                var TotalUplineSupportIncomeWeek = objTotalUplineSupportPointsWeek != null ? objTotalUplineSupportPointsWeek.CreditPoints : 0;

                var TotalDiamondIncome = objTotalDiamondPoints != null ? objTotalDiamondPoints.CreditPoints : 0;
                var TotalSilverIncome = objTotalSilverPoints != null ? objTotalSilverPoints.CreditPoints : 0;
                var TotalDiamondIncomeWeek = objTotalDiamondPointsWeek != null ? objTotalDiamondPointsWeek.CreditPoints : 0;
                var TotalSilverIncomeWeek = objTotalSilverPointsWeek != null ? objTotalSilverPointsWeek.CreditPoints : 0;

                ViewBag.LeaderIncome = (TotalLeaderIncomeWeek.HasValue ? TotalLeaderIncomeWeek.Value : 0) +
                                        (TotalDiamondIncomeWeek.HasValue ? TotalDiamondIncomeWeek.Value : 0);
                ViewBag.UplineSupportIncome = (TotalUplineSupportIncomeWeek.HasValue ? TotalUplineSupportIncomeWeek.Value : 0) +
                                        (TotalSilverIncomeWeek.HasValue ? TotalSilverIncomeWeek.Value : 0);

                var TotalLeftIncome = objLeftPoints.Sum(i => i.LeftPoints);
                var TotalRightIncome = objRightPoints.Sum(i => i.RightPoints);
                var TotalRightBV = objRightPoints.Sum(i => i.RightBV);
                var TotalLeftBV = objLeftPoints.Sum(i => i.LeftBV);
                ViewBag.TotalLeftBV = objLeftPoints.Sum(i => i.LeftBV);
                ViewBag.TotalRightBV = objRightPoints.Sum(i => i.RightBV);
                var TotalLeftIncomeForWalletAmt = TotalLeftIncome;
                var TotalRightIncomeForWalletAmt = TotalRightIncome;

                var TotalLeftIncomeBeforeCurrentWeek = objLeftPoints.Where(i => i.CreditDate < objWeekDatas.StartDate).ToList().Sum(i => i.LeftPoints);
                var TotalRightIncomeBeforeCurrentWeek = objRightPoints.Where(i => i.CreditDate < objWeekDatas.StartDate).ToList().Sum(i => i.RightPoints);

                var TotalLeftIncomeBeforeCurrentWeekBV = objLeftPoints.Where(i => i.CreditDate < objWeekDatas.StartDate).ToList().Sum(i => i.LeftBV);
                var TotalRightIncomeBeforeCurrentWeekBV = objRightPoints.Where(i => i.CreditDate < objWeekDatas.StartDate).ToList().Sum(i => i.RightBV);

                var CarryForwardIncome = ((TotalLeftIncomeBeforeCurrentWeek.HasValue ? TotalLeftIncomeBeforeCurrentWeek.Value : 0) >= (TotalRightIncomeBeforeCurrentWeek.HasValue ? TotalRightIncomeBeforeCurrentWeek.Value : 0)
                                            ? (TotalLeftIncomeBeforeCurrentWeek.HasValue ? TotalLeftIncomeBeforeCurrentWeek.Value : 0) - (TotalRightIncomeBeforeCurrentWeek.HasValue ? TotalRightIncomeBeforeCurrentWeek.Value : 0)
                                            : (TotalRightIncomeBeforeCurrentWeek.HasValue ? TotalRightIncomeBeforeCurrentWeek.Value : 0) - (TotalLeftIncomeBeforeCurrentWeek.HasValue ? TotalLeftIncomeBeforeCurrentWeek.Value : 0));
                var CarryForwardIncomeBV = ((TotalLeftIncomeBeforeCurrentWeekBV.HasValue ? TotalLeftIncomeBeforeCurrentWeekBV.Value : 0) >= (TotalRightIncomeBeforeCurrentWeekBV.HasValue ? TotalRightIncomeBeforeCurrentWeekBV.Value : 0)
                                            ? (TotalLeftIncomeBeforeCurrentWeekBV.HasValue ? TotalLeftIncomeBeforeCurrentWeekBV.Value : 0) - (TotalRightIncomeBeforeCurrentWeekBV.HasValue ? TotalRightIncomeBeforeCurrentWeekBV.Value : 0)
                                            : (TotalRightIncomeBeforeCurrentWeekBV.HasValue ? TotalRightIncomeBeforeCurrentWeekBV.Value : 0) - (TotalLeftIncomeBeforeCurrentWeekBV.HasValue ? TotalLeftIncomeBeforeCurrentWeekBV.Value : 0));

                var TotalMatchingBV = ((TotalLeftBV.HasValue ? TotalLeftBV.Value : 0) >= (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                                            ? (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                                            : (TotalLeftBV.HasValue ? TotalLeftBV.Value : 0));

                var CarryForwardIncomeSide = (TotalLeftIncome.HasValue ? TotalLeftIncome.Value : 0) >= (TotalRightIncome.HasValue ? TotalRightIncome.Value : 0)
                                            ? "L"
                                            : "R";

                var CarryForwardBVSide = (ViewBag.TotalLeftBV) >= (ViewBag.TotalRightBV)
                                            ? "L"
                                            : "R";

                //var objRepurchase = db.PROC_GET_REPURCHASE_REPORT(objclsLogin.UserId).ToList();
                //ViewBag.RepurchaseMonths = objRepurchase.OrderBy(i => i.Id).Select(i => i.MonthYear).ToArray();
                //ViewBag.RepurchaseMonthsValue = objRepurchase.OrderBy(i => i.Id).Select(i => i.CreditAmount).ToArray();

                //var objTotalIncome = db.PROC_GET_USER_TOTAL_EARNING_REPORT(objclsLogin.UserId).ToList();
                //ViewBag.RepurchaseMonths = objTotalIncome.OrderBy(i => i.Id).Select(i => i.MonthYear).ToArray();
                //ViewBag.RepurchaseMonthsValue = objTotalIncome.OrderBy(i => i.Id).Select(i => i.Income).ToArray();
                //ViewBag.MatchingIncomeMonthsValue = objTotalIncome.OrderBy(i => i.Id).Select(i => i.MatchingIncome).ToArray();
                ViewBag.RepurchaseMonths = 0;
                ViewBag.RepurchaseMonthsValue = 0;
                ViewBag.MatchingIncomeMonthsValue = 0;

                var CurrentWeekLeftPoints = objLeftPoints.Where(i => i.CreditDate >= objWeekDatas.StartDate && i.CreditDate <= objWeekDatas.EndDate).ToList().Sum(i => i.LeftPoints);
                var CurrentWeekRightPoints = objRightPoints.Where(i => i.CreditDate >= objWeekDatas.StartDate && i.CreditDate <= objWeekDatas.EndDate).ToList().Sum(i => i.RightPoints);
                var CurrentWeekLeftBV = objLeftPoints.Where(i => i.CreditDate >= objWeekDatas.StartDate && i.CreditDate <= objWeekDatas.EndDate).ToList().Sum(i => i.LeftBV);
                var CurrentWeekRightBV = objRightPoints.Where(i => i.CreditDate >= objWeekDatas.StartDate && i.CreditDate <= objWeekDatas.EndDate).ToList().Sum(i => i.RightBV);
                if (CarryForwardIncomeSide.ToUpper() == "L")
                {
                    CurrentWeekLeftBV = CurrentWeekLeftBV + CarryForwardIncomeBV;
                }
                else if (CarryForwardIncomeSide.ToUpper() == "R")
                {
                    CurrentWeekRightBV = CurrentWeekRightBV + CarryForwardIncomeBV;
                }
                ViewBag.LeftBV = Convert.ToInt64(CurrentWeekLeftBV);
                ViewBag.RightBV = Convert.ToInt64(CurrentWeekRightBV);
                //var TotalLeftPoints = objLeftPoints.Sum(i => i.LeftPoints);
                //var TotalRightPoints = objRightPoints.Sum(i => i.RightPoints);
                int DirectType = Convert.ToInt32(clsImplementationEnum.WalletCreditType.Direct);
                var objWalletCredit = db.WalletCreditDtls.Where(i => i.UserId == objclsLogin.UserId && i.IsActive == true).ToList();
                var objTotalDirectAmt = objWalletCredit.Where(i => i.CreditType == DirectType && i.CreditDate >= objWeekDatas.StartDate && i.CreditDate <= objWeekDatas.EndDate).Sum(i => i.CreditAmount);

                ViewBag.TotalDirectAmt = objWalletCredit.Sum(i => i.CreditAmount);
                var CompanyShareAmt = db.CompanyShareCreditDtls.Where(i => i.CreditDate >= objWeekDatas.StartDate && i.CreditDate <= objWeekDatas.EndDate && i.UserId == objclsLogin.UserId).ToList().Sum(i => i.CreditAmt);
                var TotalCompanyShareAmt = db.CompanyShareCreditDtls.Where(i => i.UserId == objclsLogin.UserId).ToList().Sum(i => i.CreditAmt);

                //var objPayoutDtl = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(objclsLogin.UserId, true).FirstOrDefault();
                ////ViewBag.WalletAmt = objPayoutDtl.TotalPayout;
                //decimal? temp = objPayoutDtl.TotalPayout;

                //ViewBag.WalletAmt = (Int64)(objWalletCredit.Sum(i => i.CreditAmount)
                //                            + (Int64)((((TotalLeftIncome.HasValue ? TotalLeftIncome.Value : 0) >= (TotalRightIncome.HasValue ? TotalRightIncome.Value : 0) ? (TotalRightIncome.HasValue ? TotalRightIncome.Value : 0) : (TotalLeftIncome.HasValue ? TotalLeftIncome.Value : 0))))
                //                            + TotalCompanyShareAmt
                //                            + TotalLeaderIncome
                //                            + TotalUplineSupportIncome
                //                            + TotalDiamondIncome
                //                            + TotalSilverIncome);

                ViewBag.WalletAmt = (Int64)(objWalletCredit.Sum(i => i.CreditAmount)
                                            + (Int64)((((TotalLeftIncome.HasValue ? TotalLeftIncome.Value : 0) >= (TotalRightIncome.HasValue ? TotalRightIncome.Value : 0) ? (TotalRightIncome.HasValue ? TotalRightIncome.Value : 0) : (TotalLeftIncome.HasValue ? TotalLeftIncome.Value : 0))))
                                            + TotalCompanyShareAmt
                                            + TotalLeaderIncome
                                            + TotalUplineSupportIncome
                                            + TotalDiamondIncome
                                            + TotalSilverIncome);

                ViewBag.CompanyShareAmt = (Int64)CompanyShareAmt;

                ViewBag.DirectAmount = (Int64)objTotalDirectAmt;

                //

                var registeredLActiveUsersCount = db.PROC_GET_ALL_LEFT_SIDED_USER_TREE_COUNT(objclsLogin.UserId, false, true).FirstOrDefault();
                var registeredLInActiveUsersCount = db.PROC_GET_ALL_LEFT_SIDED_USER_TREE_COUNT(objclsLogin.UserId, false, false).FirstOrDefault();

                var registeredRActiveUsersCount = db.PROC_GET_ALL_RIGHT_SIDED_USER_TREE_COUNT(objclsLogin.UserId, false, true).FirstOrDefault();
                var registeredRInActiveUsersCount = db.PROC_GET_ALL_RIGHT_SIDED_USER_TREE_COUNT(objclsLogin.UserId, false, false).FirstOrDefault();

                //

                //var listRIGHTUsers = db.FN_GET_ALL_RIGHT_SIDED_USER_TREE(objclsLogin.UserId, false).ToList();
                //var listLEFTUsers = db.FN_GET_ALL_LEFT_SIDED_USER_TREE(objclsLogin.UserId, false).ToList();
                //var RightUserCnt = listRIGHTUsers.Count(i => i.IsRegistered == true);
                //var LeftUserCnt = listLEFTUsers.Count(i => i.IsRegistered == true);
                //var InActiveRightUserCnt = listRIGHTUsers.Count(i => i.IsRegistered == false);
                //var InActiveLeftUserCnt = listLEFTUsers.Count(i => i.IsRegistered == false);
                ViewBag.RightUserCnt = registeredRActiveUsersCount;
                ViewBag.LeftUserCnt = registeredLActiveUsersCount;
                ViewBag.InActiveRightUserCnt = registeredRInActiveUsersCount;
                ViewBag.InActiveLeftUserCnt = registeredLInActiveUsersCount;
                ViewBag.TeamPairing = registeredRActiveUsersCount >= registeredLActiveUsersCount ? registeredLActiveUsersCount : registeredRActiveUsersCount;

                decimal LeftMetchingIncome = 0; decimal RightMetchingIncome = 0;
                if (CarryForwardIncomeSide.ToUpper() == "L")
                {
                    ViewBag.LeftPoints = CarryForwardIncome;
                    LeftMetchingIncome = (CurrentWeekLeftPoints.HasValue ? CurrentWeekLeftPoints.Value : 0) + CarryForwardIncome;
                }
                else
                {
                    LeftMetchingIncome = (CurrentWeekLeftPoints.HasValue ? CurrentWeekLeftPoints.Value : 0);
                }
                if (CarryForwardIncomeSide.ToUpper() == "R")
                {
                    ViewBag.RightPoints = CarryForwardIncome;
                    RightMetchingIncome = (CurrentWeekRightPoints.HasValue ? CurrentWeekRightPoints.Value : 0) + CarryForwardIncome;
                }
                else
                {
                    RightMetchingIncome = (CurrentWeekRightPoints.HasValue ? CurrentWeekRightPoints.Value : 0);
                }
                var MatchingIncome = (Int64)(LeftMetchingIncome >= RightMetchingIncome ? RightMetchingIncome : LeftMetchingIncome);
                ViewBag.TotalEligibleIncome = (Int64)(objTotalDirectAmt + MatchingIncome + CompanyShareAmt + TotalLeaderIncomeWeek
                                            + TotalUplineSupportIncomeWeek
                                            + (TotalDiamondIncomeWeek.HasValue ? TotalDiamondIncomeWeek.Value : 0)
                                            + (TotalSilverIncomeWeek.HasValue ? TotalSilverIncomeWeek.Value : 0));
                ViewBag.MatchingIncome = MatchingIncome;
                var listDirectRefferal = db.UserMsts.Where(i => i.RefferalUserId == objclsLogin.UserId).ToList();
                ViewBag.TotalDirectRefferal = listDirectRefferal.Where(i => i.IsRegistrationActivated == true).ToList().Count;
                var CurrentDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                ViewBag.BonanzaOfferCnt = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.BonanzaOfferCnt).FirstOrDefault().Value;
                var CurrentMonth = CurrentDate.Month;
                var CurrentYear = CurrentDate.Year;
                var PreviousMonth = CurrentMonth;
                if (CurrentMonth == 4 && CurrentYear == 2020)
                {
                    PreviousMonth = CurrentDate.AddMonths(-1).Month;
                }
                var PreviousYear = CurrentYear;
                if (CurrentMonth == 4 && CurrentYear == 2020)
                {
                    PreviousYear = CurrentDate.AddMonths(-1).Year;
                }
                var CurrentMonthLeftBVForBonanza = objLeftPoints.Where(i => (i.CreditDate.Value.Month == CurrentMonth && i.CreditDate.Value.Year == CurrentYear) || (i.CreditDate.Value.Month == PreviousMonth && i.CreditDate.Value.Year == PreviousYear)).Sum(i => i.LeftBV);
                var CurrentMonthRightBVForBonanza = objRightPoints.Where(i => (i.CreditDate.Value.Month == CurrentMonth && i.CreditDate.Value.Year == CurrentYear) || (i.CreditDate.Value.Month == PreviousMonth && i.CreditDate.Value.Year == PreviousYear)).Sum(i => i.RightBV);
                ViewBag.BonanzaMemberLeftCnt = CurrentMonthLeftBVForBonanza;//listLEFTUsers.Where(i => i.RegDate.Value.Month == CurrentMonth && i.RegDate.Value.Year == CurrentYear).Count();
                ViewBag.BonanzaMemberRightCnt = CurrentMonthRightBVForBonanza;// listRIGHTUsers.Where(i => i.RegDate.Value.Month == CurrentMonth && i.RegDate.Value.Year == CurrentYear).Count();
                ViewBag.InActiveMember = listDirectRefferal.Where(i => (!i.IsRegistrationActivated.HasValue || (i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value == false))).ToList().Count;

                var objUserMst = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                ViewBag.IsFastActMilesEarned = objUserMst.IsFastActMilesEarned;
                if (objUserMst.IsFastActMilesEarned == 1 || string.IsNullOrEmpty(Convert.ToString(objUserMst.RegistrationDate)))
                    ViewBag.MilesEarnCD = Convert.ToDateTime("01 Jan 2021").ToString("dd/MMM/yyyy HH:mm");
                else
                    ViewBag.MilesEarnCD = Convert.ToDateTime(objUserMst.RegistrationDate).AddDays(7).ToString("dd/MMM/yyyy HH:mm");

                if (string.IsNullOrEmpty(objUserMst.ProfileImage))
                {
                    ViewBag.ProfileImage = "/Content/images/avatar/avatar-7.png";
                }
                else
                {
                    ViewBag.ProfileImage = "/Images/ProfileImages/" + objUserMst.ProfileImage + "?id=" + objManager.GetTimespam();
                }
                if (objUserMst.IsRegistrationActivated.HasValue && objUserMst.IsRegistrationActivated.Value)
                {
                    ViewBag.IsActiveImage = "/Content/images/misch/Active.png";
                }
                else
                {
                    ViewBag.IsActiveImage = "/Content/images/misch/Inactive.png";
                }
                ViewBag.IsActive = objUserMst.IsRegistrationActivated.HasValue && objUserMst.IsRegistrationActivated.Value;
                ViewBag.IsGoldMember = objUserMst.IsGoldMember.HasValue && objUserMst.IsGoldMember.Value;
                ViewBag.IsSilverMember = objUserMst.IsSilverMember.HasValue && objUserMst.IsSilverMember.Value;
                ViewBag.IsDimondMember = objUserMst.IsDimondMember.HasValue && objUserMst.IsDimondMember.Value;
                ViewBag.IsPrimeMember = objUserMst.IsPrimeMember.HasValue && objUserMst.IsPrimeMember.Value;
                var objCityMst = db.CityMsts.Where(i => i.CityId == objUserMst.CityId).FirstOrDefault();
                ViewBag.CityName = (objCityMst != null ? objCityMst.CityName : "") + "-" + objUserMst.PinCode;
                ViewBag.BankAccNo = objUserMst.BankAccNo;
                ViewBag.IFSCCode = objUserMst.ISFCCode;

                var objRefferalUser = db.UserMsts.Where(i => i.UserId == objUserMst.RefferalUserId).FirstOrDefault();
                if (objRefferalUser != null)
                {
                    ViewBag.RefferalUserName = "Ref. By: " + objRefferalUser.RefferalCode + "-" + objRefferalUser.FirstName;
                }
                else
                {
                    ViewBag.RefferalUserName = "";
                }

                List<string> listConfigs = new List<string>();
                listConfigs.Add(ConfigKeys.AdvertisementHeader);
                listConfigs.Add(ConfigKeys.AdvertisementValue);
                listConfigs.Add(ConfigKeys.AdvertisementVideoURL);
                listConfigs.Add(ConfigKeys.GoldIncomeBV);
                listConfigs.Add(ConfigKeys.SilverIncomeBV);
                listConfigs.Add(ConfigKeys.PrimeIncomeBV);
                listConfigs.Add(ConfigKeys.DiamondIncomeBV);
                listConfigs.Add(ConfigKeys.VoltIncomeAmount);
                listConfigs.Add(ConfigKeys.SuperGoldIncomeBV);

                var listConfigDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();
                string AdvertisementHeader = string.Empty;
                string AdvertisementValue = string.Empty;
                string AdvertisementVideoURL = string.Empty;
                if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AdvertisementHeader))
                {
                    AdvertisementHeader = listConfigDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdvertisementHeader).Value.Trim();
                }
                if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AdvertisementValue))
                {
                    AdvertisementValue = listConfigDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdvertisementValue).Value.Trim();
                }
                if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AdvertisementValue))
                {
                    AdvertisementVideoURL = listConfigDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdvertisementVideoURL).Value.Trim();
                }

                double SuperGoldIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.SuperGoldIncomeBV));
                double GoldIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.GoldIncomeBV));
                double SilverIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.SilverIncomeBV));
                double PrimeIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.PrimeIncomeBV));
                double DiamondIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.DiamondIncomeBV));

                var userMst = db.UserMsts.Where(p => p.UserId == objclsLogin.UserId).FirstOrDefault();

                if (Convert.ToDouble(TotalMatchingBV) >= DiamondIncomeBV)
                {
                    ViewBag.UserBVStatus = clsImplementationEnum.UserBVStatus.Diamond.GetHashCode();

                    if ((!userMst.IsDimondMember.HasValue || !userMst.IsDimondMember.Value))
                    {
                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)DiamondIncomeBV).FirstOrDefault();

                        userMst.IsPrimeMember = userMst.IsSilverMember = userMst.IsGoldMember = new bool?(false);
                        userMst.IsDimondMember = new bool?(true);
                        userMst.DimondMemberDate = dt;
                    }
                }
                else if (Convert.ToDouble(TotalMatchingBV) >= GoldIncomeBV)
                {
                    ViewBag.UserBVStatus = clsImplementationEnum.UserBVStatus.Gold.GetHashCode();

                    if ((!userMst.IsGoldMember.HasValue || !userMst.IsGoldMember.Value))
                    {
                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)GoldIncomeBV).FirstOrDefault();

                        userMst.IsPrimeMember = userMst.IsSilverMember = userMst.IsDimondMember = new bool?(false);
                        userMst.IsGoldMember = new bool?(true);
                        userMst.GoldMemberDate = dt;
                    }
                }
                else if (Convert.ToDouble(TotalMatchingBV) >= SilverIncomeBV)
                {
                    ViewBag.UserBVStatus = clsImplementationEnum.UserBVStatus.Silver.GetHashCode();

                    if ((!userMst.IsSilverMember.HasValue || !userMst.IsSilverMember.Value))
                    {
                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)SilverIncomeBV).FirstOrDefault();

                        userMst.IsPrimeMember = userMst.IsGoldMember = userMst.IsDimondMember = new bool?(false);
                        userMst.IsSilverMember = new bool?(true);
                        userMst.SilverMemberDate = dt;
                    }
                }
                else if (Convert.ToDouble(TotalMatchingBV) >= PrimeIncomeBV)
                {
                    ViewBag.UserBVStatus = clsImplementationEnum.UserBVStatus.Prime.GetHashCode();

                    if ((!userMst.IsPrimeMember.HasValue || !userMst.IsPrimeMember.Value))
                    {
                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)PrimeIncomeBV).FirstOrDefault();

                        userMst.IsSilverMember = userMst.IsGoldMember = userMst.IsDimondMember = new bool?(false);
                        userMst.IsPrimeMember = new bool?(true);
                        userMst.PrimeMemberDate = dt;
                    }
                }
                else
                {
                    ViewBag.UserBVStatus = 0;
                }

                if (Convert.ToDouble(TotalMatchingBV) >= SuperGoldIncomeBV)
                {
                    if (!userMst.IsDimondMember.HasValue || !userMst.IsDimondMember.Value)
                    {
                        var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)SuperGoldIncomeBV).FirstOrDefault();
                        userMst.IsSuperGold = true;
                        userMst.SuperGoldMemberDate = dt;
                    }
                }

                db.SaveChanges();

                ViewBag.IsRegistrationActivate = objclsLogin.IsRegistrationActivated ? "true" : "false";
                ViewBag.AdvertisementHeader = AdvertisementHeader;
                ViewBag.AdvertisementValue = AdvertisementValue;
                ViewBag.AdvertisementVideoURL = AdvertisementVideoURL;

                var objVolumeDtl = db.PROC_GET_ADMIN_VOLUME_DETAILS(objWeekDatas.StartDate, objWeekDatas.EndDate).FirstOrDefault();
                var WeekBudget = objVolumeDtl.ActualVoltVolumeInRs.HasValue ? objVolumeDtl.ActualVoltVolumeInRs.Value : 0;
                var listVoltIncome = db.PROC_RPT_GET_COMPANY_SHARE_AMOUNT(0, objWeekDatas.StartDate, objWeekDatas.EndDate).ToList();
                if (WeekBudget > 0)
                {
                    if (listVoltIncome.Count > 0)
                    {
                        decimal TotalVolts = listVoltIncome.Sum(x => x.VoltMiles);
                        ViewBag.VoltEnd = (Int64)(WeekBudget / Convert.ToInt64(TotalVolts));
                    }
                    else
                    {
                        ViewBag.VoltEnd = WeekBudget;
                    }
                }
                else
                {
                    ViewBag.VoltEnd = WeekBudget;
                }
                ViewBag.VoltStart = objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.VoltIncomeAmount);

                //ViewBag.TotalMilesAmt = objManager.GetUserMilesCreditAmount(objclsLogin.UserId);
                //ViewBag.CurrentMilesAmt = objManager.GetUserMilesAmount(objclsLogin.UserId);

                decimal MilesAmount = 0;
                var objMilesAmount = db.UserMilesDtls.Where(x => x.UserId == objclsLogin.UserId);
                if (objMilesAmount.Any())
                {
                    var objUserMilesDtl = objMilesAmount.FirstOrDefault();
                    MilesAmount = Convert.ToDecimal((objUserMilesDtl.TotalCreditMiles - objUserMilesDtl.TotalDebitMiles));

                    ViewBag.TotalMilesAmt = objUserMilesDtl.TotalCreditMiles;
                }
                else
                    ViewBag.TotalMilesAmt = 0;

                ViewBag.CurrentMilesAmt = MilesAmount;

                ViewBag.listFinancialYear = GetLastFiveFinancialYear();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(dashboard);
        }

        private List<string> GetLastFiveFinancialYear()
        {
            List<string> LstFinancialYear = new List<string>();

            try
            {
                DateTime curDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

                int CurrentYear = curDate.Year;
                int PreviousYear = (curDate.Year - 1);
                int NextYear = (curDate.Year + 1);
                string CurFinYear = "";
                int CurFinYear1 = CurrentYear;
                int CurFinYear2;

                if (curDate.Month > 3)
                {
                    CurFinYear = CurrentYear + "-" + NextYear;
                    CurFinYear2 = CurrentYear;
                    CurFinYear1 = NextYear;
                }
                else
                {
                    CurFinYear = PreviousYear + "-" + CurrentYear;
                    CurFinYear2 = PreviousYear;
                    CurFinYear1 = CurrentYear;
                }

                LstFinancialYear.Add(CurFinYear);

                int i = 1;
                while (i <= 4)
                {
                    LstFinancialYear.Add((CurFinYear2 - i) + "-" + (CurFinYear1 - i));
                    i++;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return LstFinancialYear;
        }

        public ActionResult GetProductPieChartList(string FY_Year)
        {
            string FromDate = "01/04/" + Convert.ToString(FY_Year.Split('-')[0]);
            string ToDate = "31/03/" + Convert.ToString(FY_Year.Split('-')[1]);

            var DtStartDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var DtEndDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var listTopProducts = db.PROC_GET_TOP_PURCHASE_PRODUCTS(objclsLogin.UserId, DtStartDate, DtEndDate).ToList();

            DashboardEnt dashboard = new DashboardEnt();
            try
            {
                var listColorHighlightColorEnt = GetColorHighlightColorList();
                var cnt = 0;
                listTopProducts.ForEach(i =>
                {
                    dashboard.listPieChartEnt.Add(new PieChartEnt
                    {
                        color = listColorHighlightColorEnt[cnt].color,
                        highlight = listColorHighlightColorEnt[cnt].highlight,
                        //label = i.ProductName,
                        value = i.TotalAmount.Value.ToString()
                    });
                    cnt = cnt + 1;
                });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(dashboard.listPieChartEnt, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                ExpireAllCookies();
                Session[clsImplementationMessage.LoginInfo] = null;
                Session.Abandon();
                response.Key = true;
                //return RedirectToAction("Index", "Authenticate", new { area = "Authentication" });
            }
            catch (Exception ex)
            {
                response.Key = false;
                throw ex;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private void ExpireAllCookies()
        {
            int cookieCount = HttpContext.Request.Cookies.Count;
            for (var i = 0; i < cookieCount; i++)
            {
                var cookie = HttpContext.Request.Cookies[i];
                if (cookie != null)
                {
                    var expiredCookie = new HttpCookie(cookie.Name)
                    {
                        Expires = DateTime.Now.AddDays(-1),
                        Domain = cookie.Domain
                    };
                    HttpContext.Response.Cookies.Add(expiredCookie); // overwrite it
                }
            }

            // clear cookies server side
            HttpContext.Request.Cookies.Clear();
        }

        public List<ColorHighlightColorEnt> GetColorHighlightColorList()
        {
            List<ColorHighlightColorEnt> listColorHighlightColorEnt = new List<ColorHighlightColorEnt>();
            try
            {
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#FF5252", highlight = "#E53935" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#00BFA5", highlight = "#00897b" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#FF6E40", highlight = "#F4511E" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#f9b47e", highlight = "#f3974f" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#F1A5FC", highlight = "#E760FB" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#e1f8a4", highlight = "#c7f943" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#A0C4F9", highlight = "#649FF6" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#D1A0F9", highlight = "#B560FB" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#F2D7D5", highlight = "#CD6155" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#D7BDE2", highlight = "#C39BD3" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#ABB2B9", highlight = "#566573" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#A9CCE3", highlight = "#5499C7" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#A3E4D7", highlight = "#48C9B0" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#F9E79F", highlight = "#F4D03F" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#EDBB99", highlight = "#DC7633" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#D5DBDB", highlight = "#AAB7B8" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#A3E4D7", highlight = "#5499C7" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#ffb3ff", highlight = "#ff80ff" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#b3ffff", highlight = "#80ffff" });
                listColorHighlightColorEnt.Add(new ColorHighlightColorEnt { color = "#b3ecff", highlight = "#80dfff" });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return listColorHighlightColorEnt;
        }

        [SessionExpireFilter]
        public PartialViewResult GetSocialSharing()
        {
            var IsRightShareVisible = true;
            var IsLeftShareVisible = true;
            var objUserMst = db.UserMsts.Find(objclsLogin.UserId);
            //if (!db.UserMsts.Any(i => i.RefferalUserId == objclsLogin.UserId))
            //{
            //    if (!string.IsNullOrEmpty(objUserMst.Position) && objUserMst.Position == "L")
            //    {
            //        IsLeftShareVisible = true;
            //        IsRightShareVisible = false;
            //    }
            //    if (!string.IsNullOrEmpty(objUserMst.Position) && objUserMst.Position == "R")
            //    {
            //        IsLeftShareVisible = false;
            //        IsRightShareVisible = true;
            //    }
            //}
            //else
            //{
            IsLeftShareVisible = true;
            IsRightShareVisible = true;
            //}
            ViewBag.IsLeftShareVisible = IsLeftShareVisible;
            ViewBag.IsRightShareVisible = IsRightShareVisible;
            ViewBag.ReferralCodeLeft = Server.HtmlDecode(HttpContext.Application[clsImplementationMessage.WebsiteURL].ToString() + "/Authentication/Authenticate/Signup?rc%3D" + objclsLogin.RefferalCode + "%26p%3DL");// %3D means =, %26p means &
            ViewBag.ReferralCodeRight = Server.HtmlDecode(HttpContext.Application[clsImplementationMessage.WebsiteURL].ToString() + "/Authentication/Authenticate/Signup?rc%3D" + objclsLogin.RefferalCode + "%26p%3DR");// %3D means =, %26p means &
            return PartialView("~/Views/Shared/_SocialSharing.cshtml");
        }

        // GET: Member/Dashboard/Reports
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult Reports()
        {
            MemberReportEnt model = new MemberReportEnt();
            try
            {
                //model.listTotalEarningByTeam = db.PROC_GET_TOTAL_EARNING_BY_INDIVIDUAL_TEAM_MEMBER(objclsLogin.UserId).OrderByDescending(i => i.TotalIncome).Take(10).ToList();
                //model.listProductPurchaseByTeam = db.PROC_GET_PRODUCT_PURCHASE_BY_INDIVIDUAL_TEAM_MEMBER(objclsLogin.UserId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(model);
        }

        public JsonResult ReadNotification(int NotificationId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objNotification = db.NotificationMsts.Where(i => i.NotificationId == NotificationId).FirstOrDefault();
                objNotification.IsRead = true;
                objNotification.ReadDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                db.SaveChanges();
                response.Key = true;
            }
            catch (Exception ex)
            {
                response.Key = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}