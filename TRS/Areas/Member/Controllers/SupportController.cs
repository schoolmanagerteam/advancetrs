﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Member.Controllers
{
    public class SupportController : clsBase
    {
        // GET: Member/Support
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.listQuerySupport = db.PROC_GET_QUERY_SUPPORT(objclsLogin.UserId).OrderByDescending(i=>i.CreatedDate).ToList();
            return View();
        }

        public JsonResult SubmitQuery(QuerySupportMst objQuery)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                response.Key = true;
                if (string.IsNullOrEmpty(objQuery.Subject) || string.IsNullOrWhiteSpace(objQuery.Subject))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Subject!";
                }
                if (string.IsNullOrEmpty(objQuery.Comment) || string.IsNullOrWhiteSpace(objQuery.Comment))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Comment!";
                }
                if (!response.Key)
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                QuerySupportMst objQuerySupport = new QuerySupportMst();
                objQuerySupport.Comment = objQuery.Comment;
                objQuerySupport.CreatedBy = objclsLogin.UserId;
                objQuerySupport.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                objQuerySupport.IsActive = true;
                objQuerySupport.ModifiedBy = objclsLogin.UserId;
                objQuerySupport.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                objQuerySupport.Subject= objQuery.Subject;
                objQuerySupport.UserId = objclsLogin.UserId;
                objQuerySupport.QueryStatus = QuerySupportStatus.Pending.GetHashCode();
                db.QuerySupportMsts.Add(objQuerySupport);
                db.SaveChanges();
                response.Key = true;                
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}