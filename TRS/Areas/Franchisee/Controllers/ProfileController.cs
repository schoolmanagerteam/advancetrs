﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRS.Models;
using TRSImplementation;

namespace TRS.Areas.Franchisee.Controllers
{
    //[NoCache]
    public class ProfileController : clsBase
    {
        // GET: Franchisee/Profile        
        [SessionExpireFilter,OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            //PROC_GET_USER_PROFILE_DETAILS_FOR_FRANCHISEE_Result objProfile = null;
            //try
            //{
            //    objProfile = db.PROC_GET_USER_PROFILE_DETAILS_FOR_FRANCHISEE(objclsLogin.UserId).FirstOrDefault();
            //    if (string.IsNullOrEmpty(objProfile.ProfileImage))
            //    {
            //        objProfile.ProfileImage = "/Content/images/gallary/10.png";
            //    }
            //    else
            //    {
            //        objProfile.ProfileImage = "/Images/ProfileImages/" + objProfile.ProfileImage;
            //    }

            //    ViewBag.Cities = db.CityMsts.OrderBy(i => i.CityName).Select(i => new AutoCompleteEnt { key = i.CityId.ToString(), val = i.CityName.Replace(" ", "_").Replace("#", "_xxx_") }).ToList();

            //}
            //catch (Exception ex)
            //{
            //    ErrorSignal.FromCurrentContext().Raise(ex);
            //}
            return View();
        }

        //[SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        //public JsonResult UpdateUserProfile(FranchiseeUserProfileEnt user)
        //{
        //    ResponseMsg response = new ResponseMsg();
        //    decimal CityId = 0;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(user.CityName))
        //        {
        //            var objCity = db.CityMsts.Where(i => i.CityName.Trim() == user.CityName.Replace("_xxx_", "#").Replace("_", " ")).FirstOrDefault();
        //            if (objCity != null) { CityId = objCity.CityId; }
        //        }
        //        var objUserMst = db.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
        //        var objFranchiseeMst = db.FranchiseeMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
        //        objUserMst.Address = user.Address;                
        //        objUserMst.BankAccNo = user.BankAccNo;
        //        objUserMst.BankName = user.BankName;
        //        objUserMst.BranchName = user.BranchName;
        //        objUserMst.FirstName = user.FirstName;
        //        objUserMst.ISFCCode = user.ISFCCode;
        //        objUserMst.LastName = user.LastName;
        //        objUserMst.MiddleName = user.MiddleName;
        //        objUserMst.MobileNo = user.MobileNo;
        //        objUserMst.PANNo = user.PANNo;
        //        objUserMst.FullName = user.FirstName + " " + user.MiddleName + " " + user.LastName;
        //        objUserMst.CityId = CityId;

        //        objFranchiseeMst.Address = user.Address;
        //        objFranchiseeMst.BankAccNo = user.BankAccNo;
        //        objFranchiseeMst.BankName = user.BankName;
        //        objFranchiseeMst.BranchName = user.BranchName;
        //        objFranchiseeMst.FirstName = user.FirstName;
        //        objFranchiseeMst.ISFCCode = user.ISFCCode;
        //        objFranchiseeMst.LastName = user.LastName;
        //        objFranchiseeMst.MiddleName = user.MiddleName;
        //        objFranchiseeMst.MobileNo = user.MobileNo;
        //        objFranchiseeMst.PANNo = user.PANNo;
        //        objFranchiseeMst.GSTNo = user.GSTNo;
        //        objFranchiseeMst.FullName = user.FirstName + " " + user.MiddleName + " " + user.LastName;
        //        objFranchiseeMst.CityId = CityId;

        //        db.SaveChanges();
        //        response.Key = true;
        //    }           
        //    catch (Exception ex)
        //    {
        //        response.Msg = ex.Message;
        //        response.Key = false;
        //        ErrorSignal.FromCurrentContext().Raise(ex);
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}
    }
}