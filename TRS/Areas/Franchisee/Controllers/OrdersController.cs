﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRS.Models;
using TRSImplementation;

namespace TRS.Areas.Franchisee.Controllers
{
    //[NoCache]
    public class OrdersController : clsBase
    {
        // GET: Franchisee/Orders        
        [SessionExpireFilter,OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }
        [SessionExpireFilter]
        public PartialViewResult GetDateRangeOrderDetails(int OrderStatusId, string MonthYear, int WeekNo, int OrderType)
        {
            List<PROC_GET_USER_ORDER_HDR_Result> listOrder = new List<PROC_GET_USER_ORDER_HDR_Result>();
            try
            {

                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, WeekNo).ToList();
                var StartDate = listWeekNames.FirstOrDefault().StartDate;
                var EndDate = listWeekNames.LastOrDefault().EndDate;
                listOrder = db.PROC_GET_USER_ORDER_HDR(0, objclsLogin.UserId).Where(i => i.OrderDate >= StartDate &&
                                                                   i.OrderDate <= EndDate &&
                                                                   i.OrderStatusId == (OrderStatusId == 0 ? i.OrderStatusId : OrderStatusId) &&
                                                                   i.IsRepurchaseOrder == (OrderType == -1 ? i.IsRepurchaseOrder : (OrderType == 1 ? true : false)))
                                                            .OrderByDescending(i => i.UserPurchaseHdrId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeOrderDetails", listOrder);
        }
        //[SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        //public ActionResult CompleteOrderDetails(Int64 UserPurchaseHdrId,string CourierTrackingNo)
        //{
        //    ResponseMsg response = new ResponseMsg();
        //    try
        //    {
        //        using (var context = new TRSEntities())
        //        {
        //            using (DbContextTransaction transaction = context.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    var objUserPurchaseHdr = context.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).FirstOrDefault();
        //                    if (objUserPurchaseHdr.FranchiseeId.HasValue)
        //                    {
        //                        if (objUserPurchaseHdr.FranchiseeId.Value != objclsLogin.FranchiseeId)
        //                        {
        //                            response.Msg = "This Order Not Assign To You!";
        //                            response.Key = false;
        //                            return Json(response, JsonRequestBehavior.AllowGet);
        //                        }

        //                        objManager.InsertWalletCreditForFranchisee(context, objUserPurchaseHdr, objclsLogin.UserId, CourierTrackingNo);
        //                        context.SaveChanges();
        //                        transaction.Commit();
        //                        response.Key = true;
        //                        response.Msg = "Order Completed Successfully!";
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    response.Key = false;
        //                    response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
        //                    transaction.Rollback();
        //                    ErrorSignal.FromCurrentContext().Raise(ex);
        //                }
        //            }
        //        }
        //    }           
        //    catch (Exception ex)
        //    {
        //        response.Key = false;
        //        response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
        //        ErrorSignal.FromCurrentContext().Raise(ex);
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}
    }
}