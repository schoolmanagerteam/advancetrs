﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRSImplementation;

namespace TRS.Areas.Franchisee.Controllers
{
    //[NoCache]
    public class DashboardController : clsBase
    {
        // GET: Franchisee/Dashboard
        [SessionExpireFilter]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }
    }
}