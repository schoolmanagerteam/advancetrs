﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using TRSImplementation;

namespace TRS.Areas.Admin.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class ProductController : clsBase
    {
        // GET: Admin/Product
        [SessionExpireFilter]
        public ActionResult Index()
        {
            var objProductDtl = new PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT_Result();
            ViewBag.listProductMst = db.PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT(0).ToList();
            ViewBag.listProductCategory = db.ProductCategoryMsts.Select(i => new SelectListItem { Text = i.CategoryName, Value = i.ProductCategoryId.ToString() }).ToList();

            List<SelectListItem> objJoinningProductTypes = new List<SelectListItem>();
            objJoinningProductTypes.Add(new SelectListItem { Text = "For All Users", Value = "1" });
            objJoinningProductTypes.Add(new SelectListItem { Text = "For Active Users", Value = "2" });
            objJoinningProductTypes.Add(new SelectListItem { Text = "For InActive Users", Value = "3" });
            ViewBag.listJoinningProductTypes = objJoinningProductTypes;

            return View(objProductDtl);
        }

        public JsonResult GetProductDetails(int ProductId)
        {
            var objProductDtl = db.PROC_GET_ALL_PRODUCT_LIST_FOR_VIEW_EDIT(ProductId).FirstOrDefault();
            return Json(objProductDtl, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertUpdateProduct()
        {
            ResponseMsg response = new ResponseMsg();
            bool isError = false;
            bool Status = false;
            try
            {
                response.Key = true;
                string ProductId = System.Web.HttpContext.Current.Request.Form["ProductId"];
                string ProductName = System.Web.HttpContext.Current.Request.Form["ProductName"];
                string ProductCategoryId = System.Web.HttpContext.Current.Request.Form["ProductCategoryId"];
                string ShortDesc = System.Web.HttpContext.Current.Request.Form["ShortDesc"];
                string LongDescription = System.Web.HttpContext.Current.Request.Form["LongDescription"];
                string SalePrice = System.Web.HttpContext.Current.Request.Form["SalePrice"];
                string RegularPrice = System.Web.HttpContext.Current.Request.Form["RegularPrice"];
                string FrontCover = System.Web.HttpContext.Current.Request.Form["FrontCover"];
                string BackCover = System.Web.HttpContext.Current.Request.Form["BackCover"];
                string ProductVideoURL = System.Web.HttpContext.Current.Request.Form["ProductVideoURL"];
                string Stock = System.Web.HttpContext.Current.Request.Form["Stock"];
                string DirectPurchaseAmt = System.Web.HttpContext.Current.Request.Form["DirectPurchaseAmt"];
                string DirectPurchasePerc = System.Web.HttpContext.Current.Request.Form["DirectPurchasePerc"];
                string DirectMatchingAmt = System.Web.HttpContext.Current.Request.Form["DirectMatchingAmt"];
                string DirectMatchingPerc = System.Web.HttpContext.Current.Request.Form["DirectMatchingPerc"];
                string RepurchaseMatchingAmt = System.Web.HttpContext.Current.Request.Form["RepurchaseMatchingAmt"];
                string RepurchaseMatchingPerc = System.Web.HttpContext.Current.Request.Form["RepurchaseMatchingPerc"];
                string WeightGM = System.Web.HttpContext.Current.Request.Form["WeightGM"];
                string JoinningProductType = System.Web.HttpContext.Current.Request.Form["JoinningProductType"];

                int id = !string.IsNullOrEmpty(ProductId) ? Convert.ToInt32(ProductId) : 0;
                double DirectPurchaseAmount = !string.IsNullOrEmpty(DirectPurchaseAmt) ? Convert.ToDouble(DirectPurchaseAmt) : 0;
                double DirectPurchasePercentage = !string.IsNullOrEmpty(DirectPurchasePerc) ? Convert.ToDouble(DirectPurchasePerc) : 0;
                double DirectMatchingAmount = !string.IsNullOrEmpty(DirectMatchingAmt) ? Convert.ToDouble(DirectMatchingAmt) : 0;
                double DirectMatchingPercentage = !string.IsNullOrEmpty(DirectMatchingPerc) ? Convert.ToDouble(DirectMatchingPerc) : 0;
                double RepurchaseMatchingAmount = !string.IsNullOrEmpty(RepurchaseMatchingAmt) ? Convert.ToDouble(RepurchaseMatchingAmt) : 0;
                double RepurchaseMatchingPercentage = !string.IsNullOrEmpty(RepurchaseMatchingPerc) ? Convert.ToDouble(RepurchaseMatchingPerc) : 0;
                int CurrentStock = !string.IsNullOrEmpty(Stock) ? Convert.ToInt32(Stock) : 0;
                if (string.IsNullOrEmpty(ProductName))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Product Name!";
                }
                else if (string.IsNullOrEmpty(ProductCategoryId))
                {
                    response.Key = false;
                    response.Msg = "Please Select Product Category!";
                }
                else if (string.IsNullOrEmpty(SalePrice))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Sales Price!";
                }
                else if (string.IsNullOrEmpty(RegularPrice))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Regular Price!";
                }
                else if (string.IsNullOrEmpty(DirectPurchaseAmt))
                {
                    response.Key = false;
                    response.Msg = "Please enter direct purchase amount!";
                }
                else if (string.IsNullOrEmpty(DirectPurchasePerc))
                {
                    response.Key = false;
                    response.Msg = "Please enter direct purchase amount percentage!";
                }
                else if (string.IsNullOrEmpty(DirectMatchingAmt))
                {
                    response.Key = false;
                    response.Msg = "Please enter direct matching amount!";
                }
                else if (string.IsNullOrEmpty(DirectMatchingPerc))
                {
                    response.Key = false;
                    response.Msg = "Please enter direct matching amount percentage!";
                }
                else if (string.IsNullOrEmpty(RepurchaseMatchingAmt))
                {
                    response.Key = false;
                    response.Msg = "Please enter re-purchase matching amount!";
                }
                else if (string.IsNullOrEmpty(RepurchaseMatchingPerc))
                {
                    response.Key = false;
                    response.Msg = "Please enter re-purchase matching amount percentage!";
                }

                if (!response.Key)
                {
                    isError = true;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    if (System.Web.HttpContext.Current.Request.Files["FrontCover"] != null)
                    {
                        var pic = System.Web.HttpContext.Current.Request.Files["FrontCover"];
                        if (pic.ContentLength > 0)
                        {
                            if (!System.IO.Directory.Exists(Server.MapPath("/Images/ProductImages/")))
                            {
                                System.IO.Directory.CreateDirectory(Server.MapPath("/Images/ProductImages/"));
                            }
                            var _comPath = Server.MapPath("/Images/ProductImages/") + FrontCover;
                            var path = _comPath;
                            //pic.SaveAs(path);

                            WebImage img = new WebImage(pic.InputStream);
                            if (img.Height > img.Width)
                            {
                                img.Resize(400, 600, true, true);
                                img.Save(path);
                            }
                            else
                            {
                                img.Resize(600, 400, true, true);
                                img.Save(path);
                            }
                            FrontCover = "/Images/ProductImages/" + FrontCover;
                        }
                    }
                    else
                    {
                        if (FrontCover.Length > 0)
                        {
                            FrontCover = "/Images/ProductImages/" + FrontCover;
                        }
                    }
                    if (System.Web.HttpContext.Current.Request.Files["BackCover"] != null)
                    {
                        var picBackCover = System.Web.HttpContext.Current.Request.Files["BackCover"];
                        if (picBackCover.ContentLength > 0)
                        {
                            if (!System.IO.Directory.Exists(Server.MapPath("/Images/ProductImages/")))
                            {
                                System.IO.Directory.CreateDirectory(Server.MapPath("/Images/ProductImages/"));
                            }
                            var _comPath = Server.MapPath("/Images/ProductImages/") + BackCover;
                            var path = _comPath;
                            //picBackCover.SaveAs(path);

                            WebImage img = new WebImage(picBackCover.InputStream);
                            if (img.Height > img.Width)
                            {
                                img.Resize(400, 600, true, true);
                                img.Save(path);
                            }
                            else
                            {
                                img.Resize(600, 400, true, true);
                                img.Save(path);
                            }

                            BackCover = "/Images/ProductImages/" + BackCover;
                        }
                    }
                    else
                    {
                        if (BackCover.Length > 0)
                        {
                            BackCover = "/Images/ProductImages/" + BackCover;
                        }
                    }
                }
                else if (id == 0)
                {
                    FrontCover = string.Empty;
                    BackCover = string.Empty;
                }
                else if (id > 0)
                {
                    if (FrontCover.Length > 0)
                    {
                        FrontCover = "/Images/ProductImages/" + FrontCover;
                    }
                    if (BackCover.Length > 0)
                    {
                        BackCover = "/Images/ProductImages/" + BackCover;
                    }
                }

                ProductMst objProductMst = new ProductMst();
                if (id == 0)
                {
                    objProductMst = new ProductMst();
                }
                else
                {
                    objProductMst = db.ProductMsts.Where(i => i.ProductId == id).FirstOrDefault();
                }
                objProductMst.ProductName = ProductName;
                objProductMst.ProductCategoryId = Convert.ToInt32(ProductCategoryId);
                objProductMst.SalePrice = Convert.ToDouble(SalePrice);
                objProductMst.RegularPrice = Convert.ToDouble(RegularPrice);
                objProductMst.ShortDesc = ShortDesc;
                objProductMst.LongDescription = LongDescription;
                objProductMst.ImagePath = FrontCover;
                objProductMst.BackCoverImagePath = BackCover;
                objProductMst.ProductVideoURL = ProductVideoURL;
                objProductMst.Stock = CurrentStock;
                objProductMst.DirectPurchaseAmt = DirectPurchaseAmount;
                objProductMst.DirectPurchasePerc = DirectPurchasePercentage != 100 ? 100 : DirectPurchasePercentage;
                objProductMst.DirectMatchingAmt = DirectPurchaseAmount;//DirectMatchingAmount;
                objProductMst.DirectMatchingPerc = DirectPurchasePercentage != 80 ? 80 : DirectMatchingPercentage;
                objProductMst.RepurchaseMatchingAmt = RepurchaseMatchingAmount;
                objProductMst.RepurchaseMatchingPerc = DirectPurchasePercentage != 80 ? 80 : RepurchaseMatchingPercentage;
                objProductMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                objProductMst.WeightGM = !string.IsNullOrEmpty(WeightGM) ? Convert.ToDecimal(WeightGM) : 0;

                if (!string.IsNullOrEmpty(JoinningProductType))
                    objProductMst.JoinningProductType = Convert.ToByte(JoinningProductType);
                else
                    objProductMst.JoinningProductType = null;

                if (objProductMst.ProductCategoryId == 1)//Is Ayurvedic product
                {
                    objProductMst.IsAyurvedic = true;
                }
                else
                {
                    objProductMst.IsAyurvedic = false;
                }
                if (id == 0)
                {
                    objProductMst.IsActive = true;
                    objProductMst.CreatedBy = 0;
                    objProductMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    objProductMst.ModifiedBy = 0;
                    db.ProductMsts.Add(objProductMst);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActiveInActiveProduct(int ProductId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objProductMst = db.ProductMsts.Where(i => i.ProductId == ProductId).FirstOrDefault();
                if (objProductMst != null)
                {
                    if (objProductMst.IsActive)
                    {
                        objProductMst.IsActive = false;
                        response.Msg = "Product inactive successfully!";
                    }
                    else
                    {
                        objProductMst.IsActive = true;
                        response.Msg = "Product activate successfully!";
                    }
                    objProductMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    db.SaveChanges();

                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "Product does not exist!";
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult BusinessTool()
        {
            var objProductDtl = new PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT_Result();
            ViewBag.listProductMst = db.PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT(0).ToList();
            ViewBag.listProductCategory = db.BusinessToolsCategoryMsts.Select(i => new SelectListItem { Text = i.CategoryName, Value = i.BusinessToolsCategoryId.ToString() }).ToList();
            return View(objProductDtl);
        }

        public JsonResult GetBusinessToolDetails(int BusinessToolsId)
        {
            var objProductDtl = db.PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT(BusinessToolsId).FirstOrDefault();
            return Json(objProductDtl, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertUpdateBusinessTool()
        {
            ResponseMsg response = new ResponseMsg();
            bool isError = false;
            bool Status = false;
            try
            {
                response.Key = true;
                string BusinessToolsId = System.Web.HttpContext.Current.Request.Form["BusinessToolsId"];
                string BusinessToolsName = System.Web.HttpContext.Current.Request.Form["BusinessToolsName"];
                string BusinessToolsCategoryId = System.Web.HttpContext.Current.Request.Form["BusinessToolsCategoryId"];
                string Language = System.Web.HttpContext.Current.Request.Form["Language"];
                string VideoURL = System.Web.HttpContext.Current.Request.Form["VideoURL"];
                string ImageName = System.Web.HttpContext.Current.Request.Form["ImageName"];
                string PdfFileName = System.Web.HttpContext.Current.Request.Form["PdfFileName"];

                int id = !string.IsNullOrEmpty(BusinessToolsId) ? Convert.ToInt32(BusinessToolsId) : 0;
                if (string.IsNullOrEmpty(BusinessToolsName))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Name!";
                }
                else if (string.IsNullOrEmpty(BusinessToolsCategoryId))
                {
                    response.Key = false;
                    response.Msg = "Please Select Category!";
                }
                else if (string.IsNullOrEmpty(Language))
                {
                    response.Key = false;
                    response.Msg = "Please Select Language!";
                }

                if (!response.Key)
                {
                    isError = true;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    if (System.Web.HttpContext.Current.Request.Files["Image"] != null)
                    {
                        var pic = System.Web.HttpContext.Current.Request.Files["Image"];
                        if (pic.ContentLength > 0)
                        {
                            if (!System.IO.Directory.Exists(Server.MapPath("/Images/BusinessTools/")))
                            {
                                System.IO.Directory.CreateDirectory(Server.MapPath("/Images/BusinessTools/"));
                            }
                            var _comPath = Server.MapPath("/Images/BusinessTools/") + ImageName;
                            var path = _comPath;
                            //pic.SaveAs(path);

                            WebImage img = new WebImage(pic.InputStream);
                            if (img.Height > img.Width)
                            {
                                img.Resize(400, 600, true, true);
                                img.Save(path);
                            }
                            else
                            {
                                img.Resize(600, 400, true, true);
                                img.Save(path);
                            }
                            ImageName = "/Images/BusinessTools/" + ImageName;
                        }
                    }
                    else
                    {
                        if (ImageName.Length > 0)
                        {
                            ImageName = "/Images/BusinessTools/" + ImageName;
                        }
                    }

                    if (System.Web.HttpContext.Current.Request.Files["PdfFile"] != null)
                    {
                        var pic = System.Web.HttpContext.Current.Request.Files["PdfFile"];
                        if (pic.ContentLength > 0)
                        {
                            if (!System.IO.Directory.Exists(Server.MapPath("/Images/BusinessTools/")))
                            {
                                System.IO.Directory.CreateDirectory(Server.MapPath("/Images/BusinessTools/"));
                            }
                            var _comPath = Server.MapPath("/Images/BusinessTools/") + PdfFileName;
                            var path = _comPath;
                            pic.SaveAs(path);
                            PdfFileName = "/Images/BusinessTools/" + PdfFileName;
                        }
                    }
                    else
                    {
                        if (PdfFileName.Length > 0)
                        {
                            PdfFileName = "/Images/BusinessTools/" + PdfFileName;
                        }
                    }
                }
                else if (id == 0)
                {
                    ImageName = string.Empty;
                    PdfFileName = string.Empty;
                }
                else if (id > 0)
                {
                    if (ImageName.Length > 0)
                    {
                        ImageName = "/Images/BusinessTools/" + ImageName;
                    }
                    if (PdfFileName.Length > 0)
                    {
                        PdfFileName = "/Images/BusinessTools/" + PdfFileName;
                    }
                }

                BusinessToolsMst objProductMst = new BusinessToolsMst();
                if (id == 0)
                {
                    objProductMst = new BusinessToolsMst();
                }
                else
                {
                    objProductMst = db.BusinessToolsMsts.Where(i => i.BusinessToolsId == id).FirstOrDefault();
                }
                objProductMst.BusinessToolsName = BusinessToolsName;
                objProductMst.BusinessToolsCategoryId = Convert.ToInt32(BusinessToolsCategoryId);
                objProductMst.VideoURL = VideoURL;
                objProductMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                objProductMst.ImageName = ImageName;
                objProductMst.PdfName = PdfFileName;
                objProductMst.Language = Language;
                if (id == 0)
                {
                    objProductMst.IsActive = true;
                    objProductMst.CreatedBy = 0;
                    objProductMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    objProductMst.ModifiedBy = 0;
                    db.BusinessToolsMsts.Add(objProductMst);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActiveInActiveBusinessTool(int BusinessToolsId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objBusinessToolsMst = db.BusinessToolsMsts.Where(i => i.BusinessToolsId == BusinessToolsId).FirstOrDefault();
                if (objBusinessToolsMst != null)
                {
                    if (objBusinessToolsMst.IsActive)
                    {
                        objBusinessToolsMst.IsActive = false;
                        response.Msg = "Business Tool Detail inactive successfully!";
                    }
                    else
                    {
                        objBusinessToolsMst.IsActive = true;
                        response.Msg = "Business Tool Detail activate successfully!";
                    }
                    objBusinessToolsMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    db.SaveChanges();

                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "Business Tool Detail does not exist!";
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddBusinessToolsCategory(string Name)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                response.Key = true;
                if (string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name))
                {
                    response.IsInfo = true;
                    response.Key = false;
                    response.Msg = "Please Enter Category Name!";
                }
                if (response.Key)
                {
                    if (!db.BusinessToolsCategoryMsts.Any(i => i.CategoryName.ToLower() == Name.ToLower()))
                    {
                        var objBusinessToolsCategoryMst = new BusinessToolsCategoryMst();
                        objBusinessToolsCategoryMst.CreatedBy = 0;
                        objBusinessToolsCategoryMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objBusinessToolsCategoryMst.ModifiedBy = 0;
                        objBusinessToolsCategoryMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objBusinessToolsCategoryMst.CategoryName = Name.Trim();
                        objBusinessToolsCategoryMst.IsActive = true;
                        db.BusinessToolsCategoryMsts.Add(objBusinessToolsCategoryMst);
                        db.SaveChanges();
                        response.Key = true;
                        response.Msg = "Category Added Successfully!";
                        response.Id = (int)objBusinessToolsCategoryMst.BusinessToolsCategoryId;
                    }
                    else
                    {
                        response.IsInfo = true;
                        response.Key = false;
                        response.Msg = "Category Name Already Exist!";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult Variant()
        {
            var listProduct = db.ProductMsts.Where(i => i.IsActive == true).Select(i => new SelectListItem { Text = i.ProductName, Value = i.ProductId.ToString() }).ToList();
            ViewBag.listProduct = listProduct;
            ViewBag.listVariant = db.PROC_GET_PRODUCT_VARIANT_LIST(0, 0).ToList();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertUpdateProductVariant()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                response.Key = true;
                string ProductId = System.Web.HttpContext.Current.Request.Form["ProductId"];
                string VariantName = System.Web.HttpContext.Current.Request.Form["VariantName"];
                string ProductVariantId = System.Web.HttpContext.Current.Request.Form["ProductVariantId"];

                int id = !string.IsNullOrEmpty(ProductVariantId) ? Convert.ToInt32(ProductVariantId) : 0;
                if (string.IsNullOrEmpty(VariantName))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Variant Name!";
                }
                else if (string.IsNullOrEmpty(ProductId) || ProductId == "0")
                {
                    response.Key = false;
                    response.Msg = "Please Select Product!";
                }

                if (!response.Key)
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                ProductVariantMst objProductVariantMst = new ProductVariantMst();
                if (id == 0)
                {
                    objProductVariantMst = new ProductVariantMst();
                }
                else
                {
                    objProductVariantMst = db.ProductVariantMsts.Where(i => i.ProductVariantId == id).FirstOrDefault();
                }
                objProductVariantMst.VariantName = VariantName;
                objProductVariantMst.ProductId = Convert.ToInt32(ProductId);
                objProductVariantMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                if (id == 0)
                {
                    objProductVariantMst.IsActive = true;
                    objProductVariantMst.CreatedBy = 0;
                    objProductVariantMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    objProductVariantMst.ModifiedBy = 0;
                    db.ProductVariantMsts.Add(objProductVariantMst);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductVariantList()
        {
            var listVariant = db.PROC_GET_PRODUCT_VARIANT_LIST(0, 0).ToList();
            return Json(listVariant, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteProductVariant(int ProductVariantId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objProductVariantMst = db.ProductVariantMsts.Where(i => i.ProductVariantId == ProductVariantId).FirstOrDefault();
                if (objProductVariantMst != null)
                {
                    if (objProductVariantMst.IsActive)
                    {
                        objProductVariantMst.IsActive = false;
                        response.Msg = "Product Variant inactive successfully!";
                    }
                    else
                    {
                        objProductVariantMst.IsActive = true;
                        response.Msg = "Product Variant activate successfully!";
                    }
                    objProductVariantMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    db.SaveChanges();

                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "Product Variant detail does not exist!";
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}