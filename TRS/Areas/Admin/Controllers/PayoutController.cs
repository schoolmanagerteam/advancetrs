﻿using Elmah;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.UI;
using TRSImplementation;
using System.Data;
using OfficeOpenXml;
using DocumentFormat.OpenXml.Office2013.Drawing.Chart;
using System.ComponentModel;
using Microsoft.SqlServer.Management.Smo;
using System.Web;
using DocumentFormat.OpenXml.Wordprocessing;

namespace TRS.Areas.Admin.Controllers
{
    public class PayoutController : clsBase
    {
        private const string UploadFilePath = "~/PayoutExcel/";
        private const string PayoutExcelTemplatePath = "~/PayoutExcel/PayoutExcelTemplate/";

        // GET: Admin/Payout
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult Index()
        {
            var Today = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
            var objLastWeek = db.fn_GetFirstLastDateOfWeekByDate(Today.AddDays(-7)).FirstOrDefault();
            //var objCurrentWeek = db.fn_GetFirstLastDateOfWeekByDate(Today).FirstOrDefault();
            var IsUptoDate = false;
            if (Convert.ToDateTime(Today.AddDays(-2).ToString("yyyy/MM/dd")) <= Convert.ToDateTime(objLastWeek.EndDate.Value.ToString("yyyy/MM/dd")))
            {
                IsUptoDate = false;
            }
            else
            {
                IsUptoDate = true;
            }
            //var listPendingPayout = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(0, IsUptoDate).ToList();
            ViewBag.IsUptoDate = IsUptoDate.ToString().ToLower();
            ViewBag.IsPayoutSMSSend = false.ToString().ToLower();
            return View();
        }

        public PartialViewResult GetPayoutList(bool IsUptoDate)
        {
            var listPendingPayout = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(0, IsUptoDate).Where(i => i.FinalPayoutAmt > 0).ToList();
            return PartialView("_GetPayoutList", listPendingPayout);
        }

        public PartialViewResult GetPayoutDetails(int UserId, bool IsUptoDate)
        {
            PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_Result objPayoutDtl = new PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_Result();
            var objUser = db.UserMsts.Where(i => i.UserId == UserId).Select(x => new { x.FullName, x.BankingName }).FirstOrDefault();
            try
            {
                ViewBag.PayoutDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value.ToString("dd/MM/yyyy");

                ViewBag.UserFullName = objUser.BankingName ?? objUser.FullName ?? "";
                objPayoutDtl = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(UserId, IsUptoDate).FirstOrDefault();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetPayoutDetails", objPayoutDtl);
        }

        public JsonResult GetAllPayourDetails(bool IsUptoDate, int KYCApprovedfilter)
        {
            var listPendingPayout = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(0, IsUptoDate).Where(i => i.FinalPayoutAmt > 0).ToList();
            if (KYCApprovedfilter > 0)
            {
                string strKYCApprovedfilter = (KYCApprovedfilter == 1) ? "YES" : "NO";
                listPendingPayout = listPendingPayout.Where(p => p.KYCApproved == strKYCApprovedfilter).ToList();
            }
            return Json(listPendingPayout, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PayAmountToUser(int UserId, string UTRRefNo, bool IsUptoDate, bool IsPayoutSMSSend)
        {
            ResponseMsg response = null;
            try
            {
                var objPayoutDtl = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(UserId, IsUptoDate).FirstOrDefault();
                var DirectIncome = objPayoutDtl.DirectIncome.HasValue ? objPayoutDtl.DirectIncome.Value : 0;
                var TotalPointsIncome = objPayoutDtl.TotalPointsIncome.HasValue ? objPayoutDtl.TotalPointsIncome.Value : 0;
                var TotalVoltIncome = objPayoutDtl.VoltIncome.HasValue ? objPayoutDtl.VoltIncome.Value : 0;
                var TotalLeaderIncome = objPayoutDtl.LeaderIncome.HasValue ? objPayoutDtl.LeaderIncome.Value : 0;
                var TotalUplineSupportIncome = objPayoutDtl.UplineSupportIncome.HasValue ? objPayoutDtl.UplineSupportIncome.Value : 0;
                var TotalSilverIncome = objPayoutDtl.SilverIncome.HasValue ? objPayoutDtl.SilverIncome.Value : 0;
                var TotalDiamondIncome = objPayoutDtl.DiamondIncome.HasValue ? objPayoutDtl.DiamondIncome.Value : 0;

                var DirectIncomePay = objPayoutDtl.DirectIncomePay.HasValue ? objPayoutDtl.DirectIncomePay.Value : 0;
                var PointsIncomePay = objPayoutDtl.PointsIncomePay.HasValue ? objPayoutDtl.PointsIncomePay.Value : 0;
                var VoltIncomePay = objPayoutDtl.VoltIncomePay.HasValue ? objPayoutDtl.VoltIncomePay.Value : 0;
                var LeaderPointsPay = objPayoutDtl.LeaderPointsPay.HasValue ? objPayoutDtl.LeaderPointsPay.Value : 0;
                var UplineSupportPointsPay = objPayoutDtl.UplineSupportPointsPay.HasValue ? objPayoutDtl.UplineSupportPointsPay.Value : 0;

                var RetailRewardMonthlyPay = objPayoutDtl.RetailRewardMonthlyPay.HasValue ? objPayoutDtl.RetailRewardMonthlyPay.Value : 0;
                var UplineMonthlyPay = objPayoutDtl.UplineMonthlyPay.HasValue ? objPayoutDtl.UplineMonthlyPay.Value : 0;

                var SilverPointsPay = objPayoutDtl.SilverPointsPay.HasValue ? objPayoutDtl.SilverPointsPay.Value : 0;
                var DiamondPointsPay = objPayoutDtl.DiamondPointsPay.HasValue ? objPayoutDtl.DiamondPointsPay.Value : 0;

                var TotalIncome = objPayoutDtl.TotalIncome.HasValue ? objPayoutDtl.TotalIncome.Value : 0;
                var TotalPayout = objPayoutDtl.TotalPayout.HasValue ? objPayoutDtl.TotalPayout.Value : 0;
                var TDS = objPayoutDtl.TDS.HasValue ? objPayoutDtl.TDS.Value : 0;
                var AdminCharge = objPayoutDtl.AdminCharge.HasValue ? objPayoutDtl.AdminCharge.Value : 0;

                var PayoutDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value.ToString("dd/MM/yyyy");
                var FinalDirectIncome = DirectIncome - DirectIncomePay;
                var FinalPointsIncome = TotalPointsIncome - PointsIncomePay;
                var FinalVoltIncome = TotalVoltIncome - VoltIncomePay;
                var FinalLeaderIncome = TotalLeaderIncome - LeaderPointsPay;
                var FinalUplineSupportIncome = TotalUplineSupportIncome - UplineSupportPointsPay;
                var FinalSilverIncome = TotalSilverIncome - SilverPointsPay;
                var FinalDiamondIncome = TotalDiamondIncome - DiamondPointsPay;
                //var NetPayable = TotalIncome - TotalPayout - TDS - AdminCharge;
                var objPayoutToUser = PayoutToUser(UserId, FinalDirectIncome, FinalPointsIncome, UTRRefNo, FinalVoltIncome, FinalLeaderIncome, FinalUplineSupportIncome, PayoutDate, FinalSilverIncome, FinalDiamondIncome, UplineMonthlyPay, RetailRewardMonthlyPay, IsUptoDate, IsPayoutSMSSend, false, objPayoutDtl);
                response = (ResponseMsg)objPayoutToUser.Data;
            }
            catch (Exception ex)
            {
                response = new ResponseMsg();
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// PayAmountProcess
        /// </summary>
        /// <param name="listUTR"></param>
        /// <param name="IsUptoDate"></param>
        /// <returns></returns>
        protected ResponseMsg PayAmountProcess(List<UserUTRNo> listUTR, bool IsUptoDate, bool IsPayoutSMSSend)
        {
            ResponseMsg response = null;
            if (listUTR == null || listUTR.Count == 0)
            {
                response = new ResponseMsg();
                response.Key = false;
                response.IsInfo = true;
                response.Msg = "Please enter atleast one user UTR No!";
            }
            else
            {
                foreach (var item in listUTR)
                {
                    var objPayoutDtl = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(item.UserId, IsUptoDate).FirstOrDefault();
                    var DirectIncome = objPayoutDtl.DirectIncome.HasValue ? objPayoutDtl.DirectIncome.Value : 0;
                    var TotalPointsIncome = objPayoutDtl.TotalPointsIncome.HasValue ? objPayoutDtl.TotalPointsIncome.Value : 0;
                    var TotalVoltIncome = objPayoutDtl.VoltIncome.HasValue ? objPayoutDtl.VoltIncome.Value : 0;
                    var TotalLeaderIncome = objPayoutDtl.LeaderIncome.HasValue ? objPayoutDtl.LeaderIncome.Value : 0;
                    var TotalUplineSupportIncome = objPayoutDtl.UplineSupportIncome.HasValue ? objPayoutDtl.UplineSupportIncome.Value : 0;
                    var TotalSilverIncome = objPayoutDtl.SilverIncome.HasValue ? objPayoutDtl.SilverIncome.Value : 0;
                    var TotalDiamondIncome = objPayoutDtl.DiamondIncome.HasValue ? objPayoutDtl.DiamondIncome.Value : 0;

                    var DirectIncomePay = objPayoutDtl.DirectIncomePay.HasValue ? objPayoutDtl.DirectIncomePay.Value : 0;
                    var PointsIncomePay = objPayoutDtl.PointsIncomePay.HasValue ? objPayoutDtl.PointsIncomePay.Value : 0;
                    var VoltIncomePay = objPayoutDtl.VoltIncomePay.HasValue ? objPayoutDtl.VoltIncomePay.Value : 0;
                    var LeaderPointsPay = objPayoutDtl.LeaderPointsPay.HasValue ? objPayoutDtl.LeaderPointsPay.Value : 0;

                    var UplineSupportPointsPay = objPayoutDtl.UplineSupportPointsPay.HasValue ? objPayoutDtl.UplineSupportPointsPay.Value : 0;
                    var RetailRewardMonthlyPay = objPayoutDtl.RetailRewardMonthlyPay.HasValue ? objPayoutDtl.RetailRewardMonthlyPay.Value : 0;

                    var SilverPointsPay = objPayoutDtl.SilverPointsPay.HasValue ? objPayoutDtl.SilverPointsPay.Value : 0;
                    var DiamondPointsPay = objPayoutDtl.DiamondPointsPay.HasValue ? objPayoutDtl.DiamondPointsPay.Value : 0;

                    var PayoutDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value.ToString("dd/MM/yyyy");
                    var FinalDirectIncome = DirectIncome - DirectIncomePay;
                    var FinalPointsIncome = TotalPointsIncome - PointsIncomePay;
                    var FinalVoltIncome = TotalVoltIncome - VoltIncomePay;
                    var FinalLeaderIncome = TotalLeaderIncome - LeaderPointsPay;
                    var FinalUplineSupportIncome = TotalUplineSupportIncome - UplineSupportPointsPay;
                    var FinalSilverIncome = TotalSilverIncome - SilverPointsPay;
                    var FinalDiamondIncome = TotalDiamondIncome - DiamondPointsPay;
                    var objPayoutToUser = PayoutToUser(item.UserId, FinalDirectIncome, FinalPointsIncome, item.UTRNo, FinalVoltIncome, FinalLeaderIncome, FinalUplineSupportIncome, PayoutDate, FinalSilverIncome, FinalDiamondIncome, UplineSupportPointsPay, RetailRewardMonthlyPay, IsUptoDate, IsPayoutSMSSend, false, objPayoutDtl);
                    //response = (ResponseMsg)objPayoutToUser.Data;
                }
                response = new ResponseMsg();
                response.Key = true;
                response.Msg = "Payout Successfully!";
            }
            return response;
        }

        public ActionResult PayAmountToAllUser(List<UserUTRNo> listUTR, bool IsUptoDate, bool IsPayoutSMSSend)
        {
            ResponseMsg response = null;
            try
            {
                response = PayAmountProcess(listUTR, IsUptoDate, IsPayoutSMSSend);
            }
            catch (Exception ex)
            {
                response = new ResponseMsg();
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PayoutToUser(int UserId, decimal DirectIncome, decimal TotalPointsIncome, string UTRRefNo, decimal TotalVoltIncome, decimal TotalLeaderIncome, decimal TotalUplineSupportIncome, string PayoutDate, decimal TotalSilverIncome, decimal TotalDiamondIncome, decimal TotalUplineMonthlyPay, decimal TotalRetailRewardMonthlyPay, bool IsUptoDate, bool IsPayoutSMSSend, bool? ReGetPayoutDtl = true, PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_Result objPayout = null)
        {
            ResponseMsg response = new ResponseMsg();
            PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_Result objPayoutDtl = null;
            try
            {
                if (!ReGetPayoutDtl.HasValue || (ReGetPayoutDtl.HasValue && ReGetPayoutDtl.Value))
                {
                    objPayoutDtl = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(UserId, IsUptoDate).FirstOrDefault();
                }
                else
                {
                    objPayoutDtl = objPayout;
                }

                if (!objPayoutDtl.DirectIncome.HasValue || (objPayoutDtl.DirectIncome.HasValue && DirectIncome > objPayoutDtl.DirectIncome.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay Direct Income Less Than Or Equal To " + objPayoutDtl.DirectIncome.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (!objPayoutDtl.TotalPointsIncome.HasValue || (objPayoutDtl.TotalPointsIncome.HasValue && TotalPointsIncome > objPayoutDtl.TotalPointsIncome.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay Points Income Less Than Or Equal To " + objPayoutDtl.TotalPointsIncome.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (!objPayoutDtl.VoltIncome.HasValue || (objPayoutDtl.VoltIncome.HasValue && TotalVoltIncome > objPayoutDtl.VoltIncome.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay Volt Income Less Than Or Equal To " + objPayoutDtl.VoltIncome.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (!objPayoutDtl.LeaderIncome.HasValue || (objPayoutDtl.LeaderIncome.HasValue && TotalLeaderIncome > objPayoutDtl.LeaderIncome.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay Leader Income Less Than Or Equal To " + objPayoutDtl.LeaderIncome.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (!objPayoutDtl.UplineSupportIncome.HasValue || (objPayoutDtl.UplineSupportIncome.HasValue && TotalUplineSupportIncome > objPayoutDtl.UplineSupportIncome.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay Upline Support Income Less Than Or Equal To " + objPayoutDtl.UplineSupportIncome.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (!objPayoutDtl.SilverIncome.HasValue || (objPayoutDtl.SilverIncome.HasValue && TotalSilverIncome > objPayoutDtl.SilverIncome.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay Silver Income Less Than Or Equal To " + objPayoutDtl.SilverIncome.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (!objPayoutDtl.DiamondIncome.HasValue || (objPayoutDtl.DiamondIncome.HasValue && TotalDiamondIncome > objPayoutDtl.DiamondIncome.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay Diamond Income Less Than Or Equal To " + objPayoutDtl.DiamondIncome.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (!objPayoutDtl.UplineMonthlyPay.HasValue || (objPayoutDtl.UplineMonthlyPay.HasValue && TotalUplineMonthlyPay > objPayoutDtl.UplineMonthlyPay.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay UplineMonthly Income Less Than Or Equal To " + objPayoutDtl.UplineMonthlyPay.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                if (!objPayoutDtl.RetailRewardMonthlyPay.HasValue || (objPayoutDtl.RetailRewardMonthlyPay.HasValue && TotalRetailRewardMonthlyPay > objPayoutDtl.RetailRewardMonthlyPay.Value))
                {
                    response.Key = false;
                    response.Msg = "Please Pay RetailRewardMonthly Income Less Than Or Equal To " + objPayoutDtl.RetailRewardMonthlyPay.Value;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(PayoutDate))
                {
                    response.Key = false;
                    response.Msg = "Please Select Payout Date!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                double NetPayable = 0;
                var dtPayoutDate = objManager.ConvertDDMMYYToMMDDYY(PayoutDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                if (dtPayoutDate > db.PROC_GET_SERVER_DATE().FirstOrDefault().Value)
                {
                    response.Key = false;
                    response.Msg = "Please Select Payout Date Less Than Today Date!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                response.Key = objManager.InsertWalletDebitDtl(objclsLogin.UserId, UserId, (double)DirectIncome, (double)TotalPointsIncome, UTRRefNo, (double)TotalVoltIncome, (double)TotalLeaderIncome, (double)TotalUplineSupportIncome, (double)TotalSilverIncome, (double)TotalDiamondIncome, (double)TotalUplineMonthlyPay, (double)TotalRetailRewardMonthlyPay, ref NetPayable, dtPayoutDate);
                if (response.Key)
                {
                    try
                    {
                        var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                        if (objUserMst.MobileNo.HasValue && IsPayoutSMSSend)
                        {
                            //var Msg = "Congratulation, We Have Credited Amount: " + NetPayable.ToString() + " In Your Bank: " + objUserMst.BankName + " & Account No: " + objUserMst.BankAccNo + ", Team Maxener";
                            var Msg = string.Format("Congratulation, {0} We Have Credited Amount: {1} In Your Bank utr ref no is - {2} . Team Maxener Wellness."
                                                   , objUserMst.FullName, NetPayable.ToString(), UTRRefNo);
                            objManager.SendSMS(objUserMst.MobileNo, "1107160293461969769", Msg, "Payout");
                        }

                        var NotificationMsg = "You Just Received Payout Amount : " + NetPayable.ToString() + " , Check Your Bank Statement";
                        var objNotificationRefferalUser = new NotificationMst();
                        objNotificationRefferalUser.CreatedBy = 0;
                        objNotificationRefferalUser.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objNotificationRefferalUser.IsActive = true;
                        objNotificationRefferalUser.ModifiedBy = 0;
                        objNotificationRefferalUser.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objNotificationRefferalUser.Notification = NotificationMsg;
                        objNotificationRefferalUser.NotificationTypeId = clsImplementationEnum.NotificationType.Information.GetHashCode();
                        objNotificationRefferalUser.UserId = objUserMst.UserId;
                        db.NotificationMsts.Add(objNotificationRefferalUser);
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                    response.Msg = "Payout Successfully!";
                }
                else
                {
                    response.Msg = "Unsuccessfully Attempts!";
                }
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DownloadPayoutExcel(bool IsUptoDate)
        {
            if (!Directory.Exists(Server.MapPath(UploadFilePath)))
            {
                Directory.CreateDirectory(Server.MapPath(UploadFilePath));
            }
            using (ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook())
            {
                var ws = workbook.Worksheets.Add("Payout Details");
                var listPendingPayout = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(0, IsUptoDate).Where(i => i.FinalPayoutAmt > 0 && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).ToList();
                PropertyInfo[] propInfos = typeof(PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_Result).GetProperties();
                for (int l = 0; l < propInfos.Count(); l++)
                {
                    ws.Cell(1, l + 1).Value = propInfos[l].Name;
                    ws.Cell(1, l + 1).Style.Font.Bold = true;
                    ws.Cell(1, l + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                }
                for (int k = 0; k < listPendingPayout.Count; k++)
                {
                    var a = listPendingPayout[k];
                    for (int i = 0; i < propInfos.Count(); i++)
                    {
                        ws.Cell(k + 2, i + 1).Value = a.GetType().GetProperty(propInfos[i].Name).GetValue(a, null);
                        ws.Cell(k + 2, i + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                    }
                }

                string FileName = UploadFilePath + "/Payout_" + DateTime.Now.Ticks + ".xlsx";
                workbook.SaveAs(Server.MapPath(FileName));
                return Json(new { filename = FileName }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DownloadBankPayoutExcel(bool IsUptoDate)
        {
            if (!Directory.Exists(Server.MapPath(UploadFilePath)))
            {
                Directory.CreateDirectory(Server.MapPath(UploadFilePath));
            }
            using (ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook())
            {
                var ws = workbook.Worksheets.Add("Sheet1");
                var listPendingPayout = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(0, IsUptoDate).Where(i => i.FinalPayoutAmt > 0 && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).ToList();
                List<string> listColName = new List<string>();
                listColName.Add("Transaction_Ref_No");
                listColName.Add("Amount");
                listColName.Add("Value_Date");
                listColName.Add("Branch_Code");
                listColName.Add("Sender_Account_Type");
                listColName.Add("Remitter_Account_No");
                listColName.Add("Remitter_Name");
                listColName.Add("IFSC_Code");
                listColName.Add("Debit_Account");
                listColName.Add("Beneficiary_Account_type");
                listColName.Add("Bank_Account_Number");
                listColName.Add("Beneficiary_Name");
                listColName.Add("Remittance_Details");
                listColName.Add("Debit_Account_System");
                listColName.Add("Originator_Of_Remmittance");
                listColName.Add("EMAILMOBILENO");
                for (int l = 0; l < listColName.Count; l++)
                {
                    ws.Cell(1, l + 1).Value = listColName[l];
                    ws.Cell(1, l + 1).Style.Font.Bold = true;
                    ws.Cell(1, l + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                }
                for (int k = 0; k < listPendingPayout.Count; k++)
                {
                    var a = listPendingPayout[k];
                    for (int i = 0; i < listColName.Count; i++)
                    {
                        if (listColName[i].ToLower() == "Amount".ToLower()
                            || listColName[i].ToLower() == "IFSC_Code".ToLower()
                            || listColName[i].ToLower() == "Bank_Account_Number".ToLower()
                            || listColName[i].ToLower() == "Beneficiary_Name".ToLower()
                            || listColName[i].ToLower() == "EMAILMOBILENO".ToLower())
                        {
                            if (listColName[i].ToLower() == "Amount".ToLower())
                            {
                                ws.Cell(k + 2, i + 1).Value = a.FinalPayoutAmt;
                                ws.Cell(k + 2, i + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                            }
                            else if (listColName[i].ToLower() == "IFSC_Code".ToLower())
                            {
                                ws.Cell(k + 2, i + 1).Value = a.ISFCCode;
                                ws.Cell(k + 2, i + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                            }
                            else if (listColName[i].ToLower() == "Bank_Account_Number".ToLower())
                            {
                                ws.Cell(k + 2, i + 1).Value = a.BankAccNo;
                                ws.Cell(k + 2, i + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                            }
                            else if (listColName[i].ToLower() == "Beneficiary_Name".ToLower())
                            {
                                ws.Cell(k + 2, i + 1).Value = a.FullName;
                                ws.Cell(k + 2, i + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                            }
                            else if (listColName[i].ToLower() == "EMAILMOBILENO".ToLower())
                            {
                                ws.Cell(k + 2, i + 1).Value = a.MobileNo.HasValue ? a.MobileNo.Value.ToString() : "";
                                ws.Cell(k + 2, i + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                            }
                        }
                        else
                        {
                            ws.Cell(k + 2, i + 1).Value = "";
                            ws.Cell(k + 2, i + 1).DataType = ClosedXML.Excel.XLDataType.Text;
                        }
                    }
                }

                string FileName = UploadFilePath + "/Payout_Bank_" + DateTime.Now.Ticks + ".xlsx";
                workbook.SaveAs(Server.MapPath(FileName));
                return Json(new { filename = FileName }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// ToDataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public DataTable ToDataTable<T>(IList<T> data)// T is any generic type
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                //table.Columns.Add(prop.Name, prop.PropertyType);
                table.Columns.Add(prop.Name);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public JsonResult DownloadRBIBankPayoutExcel(bool IsUptoDate)
        {
            if (!Directory.Exists(Server.MapPath(UploadFilePath)))
                Directory.CreateDirectory(Server.MapPath(UploadFilePath));

            string filePath = Server.MapPath(PayoutExcelTemplatePath) + "PayoutExcelTemplate.xlsx";
            FileInfo fInforSource = new FileInfo(filePath);

            string FName = string.Format("Payout_Bank_{0}.xlsx", DateTime.Now.Ticks);
            string FnamePath = Server.MapPath(UploadFilePath) + FName;

            fInforSource.CopyTo(FnamePath);
            FileInfo fInfor = new FileInfo(FnamePath);

            List<PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_BANKFORMAT_Result> listPendingPayouts = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_BANKFORMAT(0, IsUptoDate).ToList();

            var listPendingPayout_HDFC = listPendingPayouts.Where(i => i.IsHDFCPayOut == true).ToList();
            listPendingPayout_HDFC.ForEach(i => { i.IFSCCode = ""; i.BeneBankName = ""; i.BranchName = ""; });

            var listPendingPayout_Other = listPendingPayouts.Where(i => i.IsHDFCPayOut == false).ToList();

            DataTable dt_HDFC = new DataTable(); dt_HDFC = (DataTable)ToDataTable(listPendingPayout_HDFC);
            DataTable dt_Other = new DataTable(); dt_Other = (DataTable)ToDataTable(listPendingPayout_Other);

            dt_HDFC.Columns.Remove("IsHDFCPayOut");
            dt_Other.Columns.Remove("IsHDFCPayOut");

            using (var package = new ExcelPackage(fInfor))
            {
                int ExcelTbIndex = 0;
                foreach (ExcelWorksheet worksheet in package.Workbook.Worksheets)
                {
                    int startRow = 6;
                    DataTable dt = null;

                    dt = (ExcelTbIndex == 0) ? dt_Other : dt_HDFC;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            int columnIndx = 0;
                            int totalcolumn = dt.Columns.Count;
                            while (columnIndx < totalcolumn)
                            {
                                worksheet.Cells[startRow, columnIndx + 1].Value = dr[columnIndx];
                                columnIndx++;
                            }
                            startRow++;
                        }
                    }
                    ExcelTbIndex++;
                }
                package.SaveAs(fInfor);
            }
            return Json(new { filename = UploadFilePath + "/" + FName }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [DeleteFileAttribute] //Action Filter, it will auto delete the file after download,
        public ActionResult Download(string file)
        {
            string fullPath = Server.MapPath(file);

            return File(fullPath, "application/vnd.ms-excel", Path.GetFileName(fullPath));
        }

        public class UserUTRNo
        {
            public string UTRNo { get; set; }
            public int UserId { get; set; }
        }

        [HttpPost]
        public ActionResult Upload(FormCollection formCollection)
        {
            ResponseMsg response = null;
            if (Request != null && Request.Files.Count > 0)
            {
                bool IsPayoutSMSSend = Convert.ToBoolean(Request.Form["IsPayoutSMSSend"]);
                bool IsUptoDate = Convert.ToBoolean(Request.Form["IsUptoDate"]);
                HttpPostedFileBase file = Request.Files[0];
                //HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    var usersList = new List<UserUTRNo>();
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        foreach (ExcelWorksheet worksheet in package.Workbook.Worksheets)
                        {
                            var workSheet = worksheet;
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;

                            for (int rowIterator = 6; rowIterator <= noOfRow; rowIterator++)
                            {
                                UserUTRNo user = new UserUTRNo();
                                user.UTRNo = Convert.ToString(workSheet.Cells[rowIterator, 13].Value);
                                string refferalCode = Convert.ToString(workSheet.Cells[rowIterator, 14].Value);
                                if (!string.IsNullOrEmpty(refferalCode) && !string.IsNullOrEmpty(user.UTRNo))
                                {
                                    var usermstObj = db.UserMsts.Where(p => p.RefferalCode == refferalCode).FirstOrDefault();
                                    if (usermstObj != null)
                                    {
                                        user.UserId = usermstObj.UserId;
                                        if (user.UserId > 0 && !string.IsNullOrEmpty(user.UTRNo))
                                            usersList.Add(user);
                                    }
                                }
                            }
                        }
                        response = PayAmountProcess(usersList, IsUptoDate, IsPayoutSMSSend);
                    }
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }

    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();

            //convert the current filter context to file and get the file path
            string filePath = (filterContext.Result as FilePathResult).FileName;

            //delete the file after download
            System.IO.File.Delete(filePath);
        }
    }
}