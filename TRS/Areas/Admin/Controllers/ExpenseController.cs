﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TRS.Models;
using TRSImplementation;

namespace TRS.Areas.Admin.Controllers
{
    public class ExpenseController : clsBase
    {
        // GET: Admin/Expense
        public ActionResult Index()
        {
            var objWeek = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeek.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeek.EndDate.Value.ToString("dd/MM/yyyy");
            //ViewBag.listExpenseMst = db.PROC_GET_ALL_EXPENSE_DETAILS(0).ToList();
            ViewBag.listExpenseType = db.ExpenseTypeMsts.Select(i => new SelectListItem { Text = i.ExpenseTypeName, Value = i.ExpenseTypeId.ToString() }).ToList();
            ViewBag.listExpensePaidBy = db.ExpensePaidByMsts.Select(i => new SelectListItem { Text = i.ExpensePaidBy, Value = i.ExpensePaidById.ToString() }).ToList();
            ViewBag.listExpensePaidTo = db.ExpensePaidToMsts.Select(i => new SelectListItem { Text = i.ExpensePaidTo, Value = i.ExpensePaidToId.ToString() }).ToList();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertUpdateExpense()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                response.Key = true;
                string ExpenseId = System.Web.HttpContext.Current.Request.Form["ExpenseId"];
                string ExpenseDate = System.Web.HttpContext.Current.Request.Form["ExpenseDate"];
                string ExpenseType = System.Web.HttpContext.Current.Request.Form["ExpenseType"];
                string Amount = System.Web.HttpContext.Current.Request.Form["Amount"];
                string PaidBy = System.Web.HttpContext.Current.Request.Form["PaidBy"];
                string ExpensePaidTo = System.Web.HttpContext.Current.Request.Form["ExpensePaidToId"];
                string Description = System.Web.HttpContext.Current.Request.Form["Description"];
                string RefferenceNo = System.Web.HttpContext.Current.Request.Form["RefferenceNo"];

                int id = !string.IsNullOrEmpty(ExpenseId) ? Convert.ToInt32(ExpenseId) : 0;
                double ActualAmount = !string.IsNullOrEmpty(Amount) ? Convert.ToDouble(Amount) : 0;

                if (string.IsNullOrEmpty(ExpenseDate))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Expense Date!";
                }
                else if (string.IsNullOrEmpty(ExpenseType))
                {
                    response.Key = false;
                    response.Msg = "Please Select Expense Type!";
                }
                else if (string.IsNullOrEmpty(Amount))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Expense Amount!";
                }
                var dtExpenseDate = objManager.ConvertDDMMYYToMMDDYY(ExpenseDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                if (dtExpenseDate > db.PROC_GET_SERVER_DATE().FirstOrDefault().Value)
                {
                    response.Key = false;
                    response.Msg = "Expense Date Must Be Less Than Or Equal To Today Date!";
                }
                if (!response.Key)
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                ExpenseMst objExpenseMst = new ExpenseMst();
                if (id == 0)
                {
                    objExpenseMst = new ExpenseMst();
                }
                else
                {
                    objExpenseMst = db.ExpenseMsts.Where(i => i.ExpenseId == id).FirstOrDefault();
                }

                objExpenseMst.Amount = ActualAmount;
                objExpenseMst.ExpenseDate = dtExpenseDate;
                objExpenseMst.Description = Description;
                objExpenseMst.ExpenseTypeId = Convert.ToDecimal(ExpenseType);
                objExpenseMst.ExpensePaidToId = Convert.ToInt32(ExpensePaidTo);
                objExpenseMst.PaidBy = Convert.ToInt32(PaidBy);
                objExpenseMst.RefferenceNo = RefferenceNo;
                if (id == 0)
                {
                    objExpenseMst.IsActive = true;
                    objExpenseMst.CreatedBy = 0;
                    objExpenseMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    objExpenseMst.ModifiedBy = 0;
                    objExpenseMst.ModifiedDate = objExpenseMst.CreatedDate;
                    db.ExpenseMsts.Add(objExpenseMst);
                }
                else
                {
                    objExpenseMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddExpenseCategory(string Name)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                response.Key = true;
                if (string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name))
                {
                    response.IsInfo = true;
                    response.Key = false;
                    response.Msg = "Please Enter Expense Category Name!";
                }
                if (response.Key)
                {
                    if (!db.ExpenseTypeMsts.Any(i => i.ExpenseTypeName.ToLower() == Name.ToLower()))
                    {
                        var objExpenseTypeMst = new ExpenseTypeMst();
                        objExpenseTypeMst.CreatedBy = 0;
                        objExpenseTypeMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objExpenseTypeMst.ModifiedBy = 0;
                        objExpenseTypeMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objExpenseTypeMst.ExpenseTypeName = Name.Trim();
                        objExpenseTypeMst.IsActive = true;
                        db.ExpenseTypeMsts.Add(objExpenseTypeMst);
                        db.SaveChanges();
                        response.Key = true;
                        response.Msg = "Expense Category Added Successfully!";
                        response.Id = (int)objExpenseTypeMst.ExpenseTypeId;
                    }
                    else
                    {
                        response.IsInfo = true;
                        response.Key = false;
                        response.Msg = "Expense Category Already Exist!";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddExpensePaidBy(string Name)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                response.Key = true;
                if (string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name))
                {
                    response.IsInfo = true;
                    response.Key = false;
                    response.Msg = "Please Enter Paid By Name!";
                }
                if (response.Key)
                {
                    if (!db.ExpensePaidByMsts.Any(i => i.ExpensePaidBy.ToLower() == Name.ToLower()))
                    {
                        var objExpensePaidByMst = new ExpensePaidByMst();
                        objExpensePaidByMst.CreatedBy = 0;
                        objExpensePaidByMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objExpensePaidByMst.ModifiedBy = 0;
                        objExpensePaidByMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objExpensePaidByMst.ExpensePaidBy = Name.Trim();
                        objExpensePaidByMst.IsActive = true;
                        db.ExpensePaidByMsts.Add(objExpensePaidByMst);
                        db.SaveChanges();
                        response.Key = true;
                        response.Msg = "Paid By Name Added Successfully!";
                        response.Id = (int)objExpensePaidByMst.ExpensePaidById;
                    }
                    else
                    {
                        response.IsInfo = true;
                        response.Key = false;
                        response.Msg = "Paid By Name Already Exist!";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddExpensePaidTo(string Name)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                response.Key = true;
                if (string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name))
                {
                    response.IsInfo = true;
                    response.Key = false;
                    response.Msg = "Please Enter Paid To Name!";
                }
                if (response.Key)
                {
                    if (!db.ExpensePaidToMsts.Any(i => i.ExpensePaidTo.ToLower() == Name.ToLower()))
                    {
                        var objExpensePaidToMst = new ExpensePaidToMst();
                        objExpensePaidToMst.CreatedBy = 0;
                        objExpensePaidToMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objExpensePaidToMst.ModifiedBy = 0;
                        objExpensePaidToMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objExpensePaidToMst.ExpensePaidTo = Name.Trim();
                        objExpensePaidToMst.IsActive = true;
                        db.ExpensePaidToMsts.Add(objExpensePaidToMst);
                        db.SaveChanges();
                        response.Key = true;
                        response.Msg = "Paid To Name Added Successfully!";
                        response.Id = (int)objExpensePaidToMst.ExpensePaidToId;
                    }
                    else
                    {
                        response.IsInfo = true;
                        response.Key = false;
                        response.Msg = "Paid To Name Already Exist!";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetExpenseDetails(int ExpenseId)
        {
            ExpenseDtlEnt objExpenseDtl = new ExpenseDtlEnt();
            try
            {
                var objExpense = db.PROC_GET_ALL_EXPENSE_DETAILS(ExpenseId).FirstOrDefault();
                objExpenseDtl.Amount = objExpense.Amount;
                objExpenseDtl.Description = objExpense.Description;
                objExpenseDtl.ExpenseId = objExpense.ExpenseId;
                objExpenseDtl.ExpenseTypeId = objExpense.ExpenseTypeId;
                objExpenseDtl.PaidBy = objExpense.PaidBy;
                objExpenseDtl.sExpenseDate = objExpense.ExpenseDate.ToString("dd/MM/yyyy");
                objExpenseDtl.ExpensePaidToId = objExpense.ExpensePaidToId;
                objExpenseDtl.RefferenceNo = objExpense.RefferenceNo;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(objExpenseDtl, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult GetExpenseDetailsList(string fromdate, string todate)
        {
            List<ExpenseDtlEnt> listExpenseListEnt = new List<ExpenseDtlEnt>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(fromdate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(todate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                var listExpense = db.PROC_GET_ALL_EXPENSE_DETAILS(0).Where(i => i.ExpenseDate >= dtFromDate && i.ExpenseDate <= dtToDate).ToList();
                listExpenseListEnt = listExpense.Select(i => new ExpenseDtlEnt
                {
                    Amount = i.Amount,
                    Description = i.Description,
                    sExpenseDate = i.ExpenseDate.ToString("dd/MM/yyyy"),
                    ExpenseId = i.ExpenseId,
                    ExpensePaidBy = i.ExpensePaidBy,
                    ExpensePaidTo = i.ExpensePaidTo,
                    ExpenseTypeName = i.ExpenseTypeName,
                    RefferenceNo = i.RefferenceNo
                }).ToList();
                //ViewBag.listExpenseListEnt = listExpenseListEnt;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetExpenseDetails", listExpenseListEnt);
        }
        public JsonResult DeleteExpense(Int64 ExpenseId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                try
                {
                    var objExpenseMst = db.ExpenseMsts.Where(i => i.ExpenseId == ExpenseId).FirstOrDefault();
                    if (objExpenseMst != null)
                    {
                        db.ExpenseMsts.Remove(objExpenseMst);
                        db.SaveChanges();
                        response.Key = true;
                        response.Msg = "Expense details delete successfully!";

                    }
                    else
                    {
                        response.Key = false;
                        response.IsInfo = true;
                        response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                    }

                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult GetExpenseCategory()
        {
            ExpenseTypeMst objExpenseTypeMst = new ExpenseTypeMst();
            try
            {
                ViewBag.listExpenseTypeMst = db.ExpenseTypeMsts.Select(i => new { ExpenseTypeId = i.ExpenseTypeId, ExpenseTypeName = i.ExpenseTypeName }).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetExpenseCategory", objExpenseTypeMst);
        }
        public JsonResult DeleteExpenseCategory(int ExpenseTypeId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                try
                {
                    var objExpenseMst = db.ExpenseMsts.Where(i => i.ExpenseTypeId == ExpenseTypeId && i.IsActive).FirstOrDefault();
                    if (objExpenseMst != null)
                    {
                        response.Key = false;
                        response.IsInfo = true;
                        response.Msg = "Expense category already used in some expenses!";
                    }
                    else
                    {
                        var objExpenseTypeMst = db.ExpenseTypeMsts.Where(i => i.ExpenseTypeId == ExpenseTypeId).FirstOrDefault();
                        if (objExpenseTypeMst != null)
                        {
                            db.ExpenseTypeMsts.Remove(objExpenseTypeMst);
                            db.SaveChanges();
                            response.Key = true;
                            response.Msg = "Expense category delete successfully!";

                        }
                        else
                        {
                            response.Key = false;
                            response.IsInfo = true;
                            response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
        public PartialViewResult GetExpensePaidTo()
        {
            ExpensePaidToMst objExpensePaidToMst = new ExpensePaidToMst();
            try
            {
                ViewBag.listExpensePaidToMst = db.ExpensePaidToMsts.Select(i => new { ExpensePaidToId = i.ExpensePaidToId, ExpensePaidTo = i.ExpensePaidTo }).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetExpensePaidTo", objExpensePaidToMst);
        }
        public JsonResult DeleteExpensePaidTo(int ExpensePaidToId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                try
                {
                    var objExpenseMst = db.ExpenseMsts.Where(i => i.ExpensePaidToId == ExpensePaidToId && i.IsActive).FirstOrDefault();
                    if (objExpenseMst != null)
                    {
                        response.Key = false;
                        response.IsInfo = true;
                        response.Msg = "Expense paid to name already used in some expenses!";
                    }
                    else
                    {
                        var objExpensePaidToMst = db.ExpensePaidToMsts.Where(i => i.ExpensePaidToId == ExpensePaidToId).FirstOrDefault();
                        if (objExpensePaidToMst != null)
                        {
                            db.ExpensePaidToMsts.Remove(objExpensePaidToMst);
                            db.SaveChanges();
                            response.Key = true;
                            response.Msg = "Expense paid to name delete successfully!";

                        }
                        else
                        {
                            response.Key = false;
                            response.IsInfo = true;
                            response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}