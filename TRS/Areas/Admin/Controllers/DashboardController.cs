﻿using Elmah;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRS.Models;
using TRSImplementation;

namespace TRS.Areas.Admin.Controllers
{
    public class DashboardController : clsBase
    {
        // GET: Admin/Dashboard

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult Index()
        {
            if (((clsLogin)Session[clsImplementationMessage.LoginInfo]).UserType == clsImplementationEnum.UserType.Office)
            {
                return RedirectToAction("Index", "Orders", new { area = "Admin" });
            }
            AdminDashboardEnt admindashboardEnt = new AdminDashboardEnt();
            try
            {
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            //var listTodayOrders = db.PROC_GET_USER_ORDER_HDR(0, 0).ToList();
            return View(admindashboardEnt);
        }

        public JsonResult LoadOrderData(JQueryDataTableParamModel request)
        {
            WriteLog("Start LoadOrderData");
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
            // dc.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
            var listTodayOrders = db.PROC_GET_USER_ORDER_HDR(0, 0).Where(i => i.OrderDate.Year == serverDate.Year && i.OrderDate.Month == serverDate.Month && i.OrderDate.Day == serverDate.Day).ToList();

            recordsTotal = listTodayOrders.Count();
            var data = listTodayOrders.Skip(skip).Take(pageSize).ToList();

            WriteLog("End LoadOrderData");

            return Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
        }

        //public PartialViewResult GetPayoutDetails(int UserId)
        //{
        //    PROC_GET_USER_CASHBACK_AMOUNT_Result objPayoutDtl = new PROC_GET_USER_CASHBACK_AMOUNT_Result();
        //    try
        //    {
        //        objPayoutDtl = db.PROC_GET_USER_CASHBACK_AMOUNT(UserId).FirstOrDefault();
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorSignal.FromCurrentContext().Raise(ex);
        //    }
        //    return PartialView("_GetPayoutDetails", objPayoutDtl);
        //}
        //public ActionResult PayoutToUser(int UserId, string UTRRefNo)
        //{
        //    ResponseMsg response = new ResponseMsg();
        //    try
        //    {
        //        var objPayoutDtl = db.PROC_GET_USER_CASHBACK_AMOUNT(UserId).FirstOrDefault();
        //        response.Key = objManager.InsertCashbackDtl(objclsLogin.UserId, UserId, objPayoutDtl.TotalOrderAmount.Value, UTRRefNo);
        //        if (response.Key)
        //        {
        //            response.Msg = "Payout Successfully!";
        //        }
        //        else
        //        {
        //            response.Msg = "Unsuccessfully Attempts!";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Msg = ex.Message;
        //        ErrorSignal.FromCurrentContext().Raise(ex);
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}

        public PartialViewResult GetDashboardCharts()
        {
            try
            {
                WriteLog("start GetDashboardCharts");
                var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
                WriteLog("1");
                var objJoiningMember = db.PROC_GET_ADMIN_MEMBER_JOINING_REPORT().ToList();
                WriteLog("2");
                ViewBag.JoinMonths = objJoiningMember.OrderBy(i => i.Id).Select(i => i.MonthYear).ToArray();
                ViewBag.InActiveRegistrationCntMonthsValue = objJoiningMember.OrderBy(i => i.Id).Select(i => i.InActiveRegistrationCnt).ToArray();
                ViewBag.ActiveRegistrationCntMonthsValue = objJoiningMember.OrderBy(i => i.Id).Select(i => i.ActiveRegistrationCnt).ToArray();

                WriteLog("3");

                var listExpense = db.PROC_GET_ADMIN_EXPENSE_REPORT().ToList();
                WriteLog("4");
                ViewBag.ExpenseMonths = listExpense.OrderBy(i => i.Id).Select(i => i.MonthYear).ToArray();
                ViewBag.ExpenseMonthsValue = listExpense.OrderBy(i => i.Id).Select(i => i.MonthExpense).ToArray();

                var objTotalOrderAmount = db.PROC_RPT_GET_MONTH_WISE_ORDER_AMOUNT().ToList();
                WriteLog("5");
                ViewBag.OrderMonths = objTotalOrderAmount.OrderBy(i => i.Id).Select(i => i.MonthYear).ToArray();
                ViewBag.OrderMonthsValue = objTotalOrderAmount.OrderBy(i => i.Id).Select(i => i.TotalOrderAmount).ToArray();

                var objTotalProfit = db.PROC_GET_ADMIN_PROFITE_REPORT().ToList();
                WriteLog("6");
                ViewBag.ProfitMonths = objTotalProfit.OrderBy(i => i.Id).Select(i => i.MonthYear).ToArray();
                ViewBag.ProfitMonthsValue = objTotalProfit.OrderBy(i => i.Id).Select(i => i.Profite).ToArray();

                ViewBag.listTopRetailer = new List<PROC_GET_TOP_RETAILER_DETAILS_Result>(); //db.PROC_GET_TOP_RETAILER_DETAILS(objWeekDatas.StartDate.Value.Month, objWeekDatas.StartDate.Value.Year, 0).Take(10).ToList();
                WriteLog("7");
                var OrderStatusId = clsImplementationEnum.OrderStatus.OrderConfirm.GetHashCode();
                ViewBag.listOrders = db.PROC_GET_USER_ORDER_HDR(0, 0).Where(i => i.OrderStatusId == OrderStatusId).OrderByDescending(i => i.UserPurchaseHdrId).Take(10).ToList();
                WriteLog("8");
                ViewBag.listQuerySupport = db.PROC_GET_QUERY_SUPPORT(0).Where(i => i.QueryStatusName == "Pending").OrderBy(i => i.CreatedDate).Take(10).ToList();
                WriteLog("9");

                WriteLog("end GetDashboardCharts");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_DashboardCharts");
        }

        public JsonResult GetTopRetailerDetails()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            List<PROC_GET_TOP_RETAILER_DETAILS_Result> objTopRetailerDetails = db.PROC_GET_TOP_RETAILER_DETAILS(objWeekDatas.StartDate.Value.Month, objWeekDatas.StartDate.Value.Year, 0).Take(10).ToList();
            return Json(new { records = objTopRetailerDetails }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetDashboardSalesExpenseProfite()
        {
            WriteLog("start GetDashboardSalesExpenseProfite");
            DashboardSalesExpenseProfiteMdl dashboardSalesExpenseProfiteMdl = new DashboardSalesExpenseProfiteMdl();
            try
            {
                dashboardSalesExpenseProfiteMdl = db.Database.SqlQuery<DashboardSalesExpenseProfiteMdl>("PROC_GET_DASHBOARD_SALES_EXPENSE_PROFITE").FirstOrDefault();

                #region Old COde...
                //WriteLog("start GetDashboardSalesExpenseProfite");
                //var objTotalSales = db.UserPurchaseHdrs.Sum(i => i.TotalOrderAmount);
                //WriteLog("1");
                //ViewBag.TotalSales = objTotalSales;
                //var listUsers = db.UserMsts.ToList();
                //WriteLog("2");
                //ViewBag.ActiveMemberCnt = listUsers.Where(i => i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).Count();
                //ViewBag.InActiveMemberCnt = listUsers.Where(i => !i.IsRegistrationActivated.HasValue || (i.IsRegistrationActivated.HasValue && !i.IsRegistrationActivated.Value)).Count();
                //WriteLog("3");
                //var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
                //WriteLog("4");
                //var listMonthsOrders = db.UserPurchaseHdrs.Where(i => i.OrderDate.Month == objWeekDatas.StartDate.Value.Month && i.OrderDate.Year == objWeekDatas.StartDate.Value.Year).ToList();
                //WriteLog("5");
                //var listWeekOrders = listMonthsOrders.Where(i => i.OrderDate >= objWeekDatas.StartDate && i.OrderDate <= objWeekDatas.EndDate).ToList();
                //ViewBag.ThisMonthDirectSales = listMonthsOrders.Where(i => !i.IsRepurchaseOrder.HasValue || (i.IsRepurchaseOrder.HasValue && !i.IsRepurchaseOrder.Value)).Sum(i => i.TotalOrderAmount);
                //ViewBag.ThisMonthRepurchaseSales = listMonthsOrders.Where(i => i.IsRepurchaseOrder.HasValue && i.IsRepurchaseOrder.Value).Sum(i => i.TotalOrderAmount);
                //ViewBag.ThisMonthSales = listMonthsOrders.Sum(i => i.TotalOrderAmount);
                //ViewBag.ThisWeekSales = listWeekOrders.Sum(i => i.TotalOrderAmount);
                //ViewBag.ThisWeekDirectSales = listWeekOrders.Where(i => !i.IsRepurchaseOrder.HasValue || (i.IsRepurchaseOrder.HasValue && !i.IsRepurchaseOrder.Value)).Sum(i => i.TotalOrderAmount);
                //ViewBag.ThisWeekRepurchaseSales = listWeekOrders.Where(i => i.IsRepurchaseOrder.HasValue && i.IsRepurchaseOrder.Value).Sum(i => i.TotalOrderAmount);

                //WriteLog("6");

                //var listMonthExpense = db.ExpenseMsts.Where(i => i.ExpenseDate.Month == objWeekDatas.StartDate.Value.Month && i.ExpenseDate.Year == objWeekDatas.StartDate.Value.Year).ToList();

                //WriteLog("7");
                //var listWeekExpense = listMonthExpense.Where(i => i.ExpenseDate >= objWeekDatas.StartDate && i.ExpenseDate <= objWeekDatas.EndDate).ToList();
                //ViewBag.ThisWeekExpense = listWeekExpense.Sum(i => i.Amount);
                //ViewBag.ThisMonthExpense = listMonthExpense.Sum(i => i.Amount);
                ////admindashboardEnt.listUserMst = db.UserMsts.Where(i => i.RefferalUserId != 0 && (!i.IsRegistrationActivated.HasValue || !i.IsRegistrationActivated.Value)).OrderByDescending(i => i.RegistrationDate).ToList();

                //WriteLog("8");
                //var MonthStartDate = new DateTime(objWeekDatas.StartDate.Value.Year, objWeekDatas.StartDate.Value.Month, 1);
                //var MonthEndDate = MonthStartDate.AddMonths(1).AddDays(-1);

                //var objWeekProfite = db.PROC_GET_ADMIN_PROFITE_DETAILS(objWeekDatas.StartDate, objWeekDatas.EndDate).FirstOrDefault();

                //WriteLog("9");

                //var objMonthProfite = db.PROC_GET_ADMIN_PROFITE_DETAILS(MonthStartDate, MonthEndDate).FirstOrDefault();
                //var objTotalProfite = db.PROC_GET_ADMIN_PROFITE_DETAILS(null, null).FirstOrDefault();

                //WriteLog("10");

                //var objBV = db.PROC_GET_ADMIN_BV_DETAILS_DASHBOARD().FirstOrDefault();

                //WriteLog("11");

                //ViewBag.ThisMonthProfite = objMonthProfite.Profite;
                //ViewBag.ThisWeekProfite = objWeekProfite.Profite;
                //ViewBag.TotalProfit = objTotalProfite.Profite;

                //ViewBag.ThisWeekBV = objBV.WeekBV;
                //ViewBag.ThisMonthBV = objBV.MonthBV;
                //ViewBag.ThisWeekDirectBV = objBV.WeekDirectBV;
                //ViewBag.ThisWeekRepurchaseBV = objBV.WeekRepurchaseBV;

                //WriteLog("end GetDashboardSalesExpenseProfite");

                #endregion

            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            WriteLog("end GetDashboardSalesExpenseProfite");
            return PartialView("_DashboardSalesExpenseProfite", dashboardSalesExpenseProfiteMdl);
        }

        public PartialViewResult GetDateRangeOrderDetails(string FromDate, string ToDate)
        {
            AdminDashboardEnt admindashboardEnt = new AdminDashboardEnt();
            try
            {
                WriteLog("start GetDateRangeOrderDetails");
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                admindashboardEnt.listTodayOrders = db.PROC_GET_USER_ORDER_HDR(0, 0).Where(i => i.OrderDate >= dtFromDate && i.OrderDate <= dtToDate).ToList();

                WriteLog("end GetDateRangeOrderDetails");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeOrderDetails", admindashboardEnt);
        }

        public PartialViewResult GetInactiveUserDetails(string Type = "Inactive")
        {
            WriteLog("start GetInactiveUserDetails");
            List<UserMst> listUserMst = new List<UserMst>();
            try
            {
                if (Type.ToLower() == "Inactive".ToLower())
                {
                    listUserMst = db.UserMsts.Where(i => i.RefferalUserId != 0 && (!i.IsRegistrationActivated.HasValue || !i.IsRegistrationActivated.Value)).OrderByDescending(i => i.RegistrationDate).ToList();
                }
                else if (Type.ToLower() == "active".ToLower())
                {
                    listUserMst = db.UserMsts.Where(i => i.RefferalUserId != 0 && (i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value)).OrderByDescending(i => i.RegistrationDate).ToList();
                }
                else if (Type.ToLower() == "all".ToLower())
                {
                    listUserMst = db.UserMsts.Where(i => i.RefferalUserId != 0).OrderByDescending(i => i.RegistrationDate).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            WriteLog("end GetInactiveUserDetails");
            return PartialView("_GetInactiveUserDetails", listUserMst);
        }

        public static void WriteLog(string strLog)
        {
            //StreamWriter log;
            //FileStream fileStream = null;
            //DirectoryInfo logDirInfo = null;
            //FileInfo logFileInfo;

            //string msgFormat = string.Format("{0}-{1}", DateTime.Now.ToLongTimeString(), strLog);
            //string logFilePath = @"E:\\Release\\";
            //logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            //logFileInfo = new FileInfo(logFilePath);
            //logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            //if (!logDirInfo.Exists) logDirInfo.Create();
            //if (!logFileInfo.Exists)
            //{
            //    fileStream = logFileInfo.Create();
            //}
            //else
            //{
            //    fileStream = new FileStream(logFilePath, FileMode.Append);
            //}
            //log = new StreamWriter(fileStream);
            //log.WriteLine(msgFormat);
            //log.Close();
        }
    }
}