﻿using Elmah;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TRS.Models;
using TRSImplementation;
using static TRS.Areas.Member.Controllers.TeamsController;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Admin.Controllers
{
    public class ReportsController : clsBase
    {
        private string WebsiteURL = string.Empty;
        private List<UserMst> listUserMst = null;
        private List<OrganizationDtl> listOrganizationDtl = null;

        // GET: Admin/Reports
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult JoiningProducts()
        {
            List<ProductReportEnt> listProductReportEnt = new List<ProductReportEnt>();
            try
            {
                listProductReportEnt.Add(new ProductReportEnt { Name = "Product", Percentage = 4 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Tax", Percentage = 5 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Direct", Percentage = 10 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Matching", Percentage = 8 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Leader Create Bonus", Percentage = 8 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Upline Support Bonus", Percentage = 1 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Company Share", Percentage = 2 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Company Turnover", Percentage = 1 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Rewards", Percentage = 3 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Bonanza", Percentage = 2 });
                ViewBag.TitleName = "Joining";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View("Index", listProductReportEnt);
        }

        [SessionExpireFilter]
        public ActionResult RepurchaseProducts()
        {
            List<ProductReportEnt> listProductReportEnt = new List<ProductReportEnt>();
            try
            {
                listProductReportEnt.Add(new ProductReportEnt { Name = "Product", Percentage = 8 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Tax", Percentage = 5 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Retailer", Percentage = 13 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Matching", Percentage = 4 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Leader Create Bonus", Percentage = 4 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Upline Support Bonus", Percentage = (decimal)0.5 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Company Share", Percentage = 1 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Company Turnover", Percentage = 1 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Rewards", Percentage = (decimal)1.5 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Bonanza", Percentage = 1 });
                ViewBag.TitleName = "Repurchase";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View("Index", listProductReportEnt);
        }

        [SessionExpireFilter]
        public ActionResult GymProducts()
        {
            List<ProductReportEnt> listProductReportEnt = new List<ProductReportEnt>();
            try
            {
                listProductReportEnt.Add(new ProductReportEnt { Name = "Product", Percentage = 40 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Tax", Percentage = 18 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Direct", Percentage = 58 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Matching", Percentage = 2 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Leader Create Bonus", Percentage = 2 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Upline Support Bonus", Percentage = (decimal)0.25 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Company Share", Percentage = (decimal)0.5 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Company Turnover", Percentage = (decimal)0.5 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Rewards", Percentage = (decimal)0.75 });
                listProductReportEnt.Add(new ProductReportEnt { Name = "Bonanza", Percentage = (decimal)0.5 });
                ViewBag.TitleName = "Gym";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View("Index", listProductReportEnt);
        }

        [SessionExpireFilter]
        public ActionResult Sales()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public ActionResult GetMonthWeekDtl(string MonthYear)
        {
            List<WeekNames> listWeekNames = null;
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);

                listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, 0).Select(i => new WeekNames { FromDate = i.StartDate.Value, ToDate = i.EndDate.Value, WeekName = i.StartDate.Value.ToString("dd/MM/yyyy") + " to " + i.EndDate.Value.ToString("dd/MM/yyyy"), WeekNo = i.WeekNo.Value }).ToList();
            }
            catch (Exception ex)
            {
                listWeekNames = new List<WeekNames>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listWeekNames, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public PartialViewResult GetMonthWiseSalesDetails(string MonthYear, int WeekNo = 0)
        {
            List<SalesReportEnt> listOrder = new List<SalesReportEnt>();
            int? id = 0;
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, WeekNo).ToList();
                var StartDate = listWeekNames.FirstOrDefault().StartDate;
                var EndDate = listWeekNames.LastOrDefault().EndDate;
                var listOrderDtl = db.PROC_GET_SALES_ORDER_DETAILS(id, StartDate, EndDate).ToList();

                foreach (var item in listOrderDtl)
                {
                    var ProductTotalAmount = item.ProductPrice.Value * item.Qty.Value;
                    double ProductMiles = 0;
                    if (item.UsedMiles != 0)
                        ProductMiles = Math.Round((item.UsedMiles / item.OrderTotalQty), 2);

                    item.ProductPrice = Convert.ToDouble((((item.ProductPrice - ProductMiles) / (item.IsAyurvedic ? 105 : 118)) * 100).Value.ToString("#.00"));
                    item.Amount = Convert.ToDouble((item.ProductPrice * item.Qty).Value.ToString("#.00"));
                    var CGSTPer = item.IsAyurvedic ? 2.5 : 9;
                    var SGSTPer = item.IsAyurvedic ? 2.5 : 9;
                    var IGSTPer = item.IsAyurvedic ? 5 : 18;

                    listOrder.Add(new SalesReportEnt
                    {
                        Address = item.Address,
                        Amount = item.Amount,
                        CategoryName = item.CategoryName,
                        CityName = item.CityName,
                        StateName = item.StateName,
                        PinCode = item.PinCode,
                        EmailId = item.EmailId,
                        FullName = item.FullName,
                        GSTNo = item.GSTNo,
                        MobileNo = item.MobileNo,
                        OrderDate = item.OrderDate,
                        OrderNo = item.OrderNo,
                        ProductName = item.ProductName,
                        PaymentId = item.PaymentId,
                        ProductPrice = item.ProductPrice * item.Qty,
                        Qty = item.Qty,
                        TotalOrderAmount = item.TotalOrderAmount,
                        UsedMiles = ProductMiles,
                        ProductTotalAmount = ProductTotalAmount,
                        CGST = ((string.IsNullOrEmpty(item.StateName) || item.StateName.ToLower() == item.AdminStateName.ToLower()) ?
                            (item.Amount.HasValue ? Convert.ToDouble((item.Amount.Value * CGSTPer / 100).ToString("#.00")) : 0)
                            : 0),
                        SGST = ((string.IsNullOrEmpty(item.StateName) || item.StateName.ToLower() == item.AdminStateName.ToLower()) ?
                            (item.Amount.HasValue ? Convert.ToDouble((item.Amount.Value * SGSTPer / 100).ToString("#.00")) : 0)
                            : 0),
                        IGST = (!string.IsNullOrEmpty(item.StateName) && (item.StateName.ToLower() != item.AdminStateName.ToLower()) ?
                            (item.Amount.HasValue ? Convert.ToDouble((item.Amount.Value * IGSTPer / 100).ToString("#.00")) : 0)
                            : 0),
                        RefferalCode = item.RefferalCode,
                        IsRepurchase = item.IsRepurchase,
                        BusinessVolume = item.BusinessVolume,
                        Notes = item.Notes
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseSalesDetails", listOrder);
        }

        public ActionResult GetMonthWiseSalesDetailsExport(string MonthYear, int WeekNo = 0)
        {
            List<SalesReportEnt> listOrder = new List<SalesReportEnt>();
            int? id = 0;
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, WeekNo).ToList();
                var StartDate = listWeekNames.FirstOrDefault().StartDate;
                var EndDate = listWeekNames.LastOrDefault().EndDate;
                var listOrderDtl = db.PROC_GET_SALES_ORDER_DETAILS(id, StartDate, EndDate).ToList();

                foreach (var item in listOrderDtl)
                {
                    var ProductTotalAmount = item.ProductPrice.Value * item.Qty.Value;

                    double ProductMiles = 0;
                    if (item.UsedMiles != 0)
                        ProductMiles = (item.UsedMiles / item.OrderTotalQty);

                    item.ProductPrice = Convert.ToDouble((((item.ProductPrice - ProductMiles) / (item.IsAyurvedic ? 105 : 118)) * 100).Value.ToString("#.00"));

                    item.Amount = Convert.ToDouble((item.ProductPrice * item.Qty).Value.ToString("#.00"));
                    var CGSTPer = item.IsAyurvedic ? 2.5 : 9;
                    var SGSTPer = item.IsAyurvedic ? 2.5 : 9;
                    var IGSTPer = item.IsAyurvedic ? 5 : 18;

                    listOrder.Add(new SalesReportEnt
                    {
                        Address = item.Address,
                        Amount = item.Amount,
                        CategoryName = item.CategoryName,
                        CityName = item.CityName,
                        StateName = item.StateName,
                        PinCode = item.PinCode,
                        EmailId = item.EmailId,
                        FullName = item.FullName,
                        GSTNo = item.GSTNo,
                        MobileNo = item.MobileNo,
                        OrderDate = item.OrderDate,
                        OrderNo = item.OrderNo,
                        PaymentId = item.PaymentId,
                        ProductName = item.ProductName,
                        ProductPrice = item.ProductPrice * item.Qty,
                        Qty = item.Qty,
                        TotalOrderAmount = item.TotalOrderAmount,
                        UsedMiles = ProductMiles,
                        ProductTotalAmount = ProductTotalAmount,
                        CGST = ((string.IsNullOrEmpty(item.StateName) || item.StateName.ToLower() == item.AdminStateName.ToLower()) ?
                            (item.Amount.HasValue ? Convert.ToDouble((item.Amount.Value * CGSTPer / 100).ToString("#.00")) : 0)
                            : 0),
                        SGST = ((string.IsNullOrEmpty(item.StateName) || item.StateName.ToLower() == item.AdminStateName.ToLower()) ?
                            (item.Amount.HasValue ? Convert.ToDouble((item.Amount.Value * SGSTPer / 100).ToString("#.00")) : 0)
                            : 0),
                        IGST = (!string.IsNullOrEmpty(item.StateName) && (item.StateName.ToLower() != item.AdminStateName.ToLower()) ?
                            (item.Amount.HasValue ? Convert.ToDouble((item.Amount.Value * IGSTPer / 100).ToString("#.00")) : 0)
                            : 0),
                        RefferalCode = item.RefferalCode,
                        IsRepurchase = item.IsRepurchase,
                        BusinessVolume = item.BusinessVolume,
                        Notes = item.Notes
                    });
                }

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listOrder, new List<string>(), "SalesReport", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=SalesReport_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Genealogy()
        {
            try
            {
                WebsiteURL = db.ConfigMsts.Where(i => i.Key == "WebsiteURL").FirstOrDefault().Value;
                listUserMst = db.UserMsts.ToList();
                listOrganizationDtl = new List<OrganizationDtl>();
                var AdminUserId = db.UserMsts.Where(i => i.RefferalUserId == 0).FirstOrDefault().UserId;

                var objParentUser = listUserMst.FirstOrDefault(i => i.UserId == AdminUserId);
                listOrganizationDtl.Add(new OrganizationDtl
                {
                    UserId = objParentUser.UserId,
                    Name = objParentUser.FirstName,
                    ParentUserId = 0,
                    RefferalCode = objParentUser.RefferalCode,
                    ImagePath = GetProfileImagePath(objParentUser.ProfileImage),
                    RefferalUserId = objParentUser.RefferalUserId,
                    RefferalName = GetDirectRefferedByUser(objParentUser),
                    MobileNo = GetUserMobileNo(objParentUser),
                    Position = GetUserPosition(objParentUser),
                    IsActive = GetUserIsActive(objParentUser),
                    //RightBV =objManager.GetRightBV(objParentUser.UserId),
                    //LeftBV = objManager.GetLeftBV(objParentUser.UserId),
                });

                GetChildNodes(objParentUser);

                ViewBag.OrgDtl = listOrganizationDtl;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public void GetChildNodes(UserMst objUser)
        {
            if (listUserMst.Any(i => i.LeftUserId.HasValue && i.LeftUserId.Value != 0 && i.UserId == objUser.UserId))
            {
                var objChildUser = listUserMst.FirstOrDefault(i => i.UserId == objUser.LeftUserId);
                listOrganizationDtl.Add(
                    new OrganizationDtl
                    {
                        UserId = objChildUser.UserId,
                        Name = objChildUser.FirstName,
                        ParentUserId = objUser.UserId,
                        RefferalCode = objChildUser.RefferalCode,
                        ImagePath = GetProfileImagePath(objChildUser.ProfileImage),
                        RefferalUserId = objChildUser.RefferalUserId,
                        RefferalName = GetDirectRefferedByUser(objChildUser),
                        MobileNo = GetUserMobileNo(objChildUser),
                        Position = GetUserPosition(objChildUser),
                        IsActive = GetUserIsActive(objChildUser),
                        //RightBV = objManager.GetRightBV(objChildUser.UserId),
                        //LeftBV = objManager.GetLeftBV(objChildUser.UserId),
                    });
                GetChildNodes(objChildUser);
            }
            if (listUserMst.Any(i => i.RightUserId.HasValue && i.RightUserId.Value != 0 && i.UserId == objUser.UserId))
            {
                var objChildUser = listUserMst.FirstOrDefault(i => i.UserId == objUser.RightUserId);
                listOrganizationDtl.Add(
                    new OrganizationDtl
                    {
                        UserId = objChildUser.UserId,
                        Name = objChildUser.FirstName,
                        ParentUserId = objUser.UserId,
                        RefferalCode = objChildUser.RefferalCode,
                        ImagePath = GetProfileImagePath(objChildUser.ProfileImage),
                        RefferalUserId = objChildUser.RefferalUserId,
                        RefferalName = GetDirectRefferedByUser(objChildUser),
                        MobileNo = GetUserMobileNo(objChildUser),
                        Position = GetUserPosition(objChildUser),
                        IsActive = GetUserIsActive(objChildUser),
                        RightBV = objManager.GetRightBV(objChildUser.UserId),
                        LeftBV = objManager.GetLeftBV(objChildUser.UserId),
                    });
                GetChildNodes(objChildUser);
            }
        }

        public string GetProfileImagePath(string Image)
        {
            var ProfileImage = "";
            if (!string.IsNullOrEmpty(Image))
            {
                ProfileImage = WebsiteURL + "/Images/ProfileImages/" + Image;
            }
            else
            {
                ProfileImage = WebsiteURL + "/Content/images/avatar/avatar-7.png";
            }
            return ProfileImage;
        }

        public string GetDirectRefferedByUser(UserMst objUser)
        {
            string RefferalName = "";
            try
            {
                var objRefferalUser = listUserMst.Where(i => i.UserId == objUser.RefferalUserId).FirstOrDefault();
                if (objRefferalUser != null)
                {
                    RefferalName = (!string.IsNullOrEmpty(objRefferalUser.RefferalCode) ? objRefferalUser.RefferalCode : "") + "-" + objRefferalUser.FirstName;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return RefferalName;
        }

        public string GetUserMobileNo(UserMst objUser)
        {
            string MobileNo = "";
            try
            {
                MobileNo = objUser.MobileNo.HasValue ? objUser.MobileNo.ToString() : "";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return MobileNo;
        }

        public string GetUserIsActive(UserMst objUser)
        {
            string IsActive = "";
            try
            {
                IsActive = objUser.IsRegistrationActivated.HasValue && objUser.IsRegistrationActivated.Value ? "Yes" : "No";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return IsActive;
        }

        public string GetUserPosition(UserMst objUser)
        {
            string Position = "";
            try
            {
                if (!string.IsNullOrEmpty(objUser.Position))
                {
                    Position = objUser.Position == "L" ? "Left" : "Right";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Position;
        }

        public ActionResult PayoutDtl()
        {
            try
            {
                DateTime baseDate = (DateTime)db.PROC_GET_SERVER_DATE().FirstOrDefault();
                var yesterday = baseDate.AddDays(-1);
                var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                var thisWeekEnd = thisWeekStart.AddDays(7).AddDays(-1);
                ViewBag.StartDate = thisWeekStart.ToString("dd/MM/yyyy");
                ViewBag.EndDate = thisWeekEnd.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public PartialViewResult GetMonthWisePayoutDtl(string StartDate, string EndDate)
        {
            List<PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result> listWalletDebitDtls = new List<PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result>();
            try
            {
                var DtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var DtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                listWalletDebitDtls = db.PROC_GET_ADMIN_WALLET_DEBIT_REPORT(0, DtStartDate, DtEndDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWisePayoutDtl", listWalletDebitDtls);
        }

        public ActionResult AdminCharges()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                listMonthNames.Add(new MonthNames()
                {
                    Month = 0,
                    Year = 0,
                    MonthYear = 0 + "#" + 0,
                    MonthName = "ALL"
                });
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        public PartialViewResult GetMonthWiseAdminCharges(string StartDate, string EndDate)
        {
            List<PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result> listWalletDebitDtls = new List<PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result>();
            try
            {
                var DtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var DtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                listWalletDebitDtls = db.PROC_GET_ADMIN_WALLET_DEBIT_REPORT(0, DtStartDate, DtEndDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseAdminCharges", listWalletDebitDtls);
        }

        public ActionResult TDS()
        {
            try
            {
                DateTime baseDate = (DateTime)db.PROC_GET_SERVER_DATE().FirstOrDefault();
                var yesterday = baseDate.AddDays(-1);
                var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                var thisWeekEnd = thisWeekStart.AddDays(7).AddDays(-1);
                ViewBag.StartDate = thisWeekStart.ToString("dd/MM/yyyy");
                ViewBag.EndDate = thisWeekEnd.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public PartialViewResult GetMonthWiseTDS(string StartDate, string EndDate)
        {
            List<PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result> listWalletDebitDtls = new List<PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result>();
            try
            {
                //var DtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //var DtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var DtStartDate = objManager.ConvertDDMMYYToMMDDYY(StartDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var DtEndDate = objManager.ConvertDDMMYYToMMDDYY(EndDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                listWalletDebitDtls = db.PROC_GET_ADMIN_WALLET_DEBIT_REPORT(0, DtStartDate, DtEndDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseTDS", listWalletDebitDtls);
        }

        public ActionResult GetMonthWiseTDSExport(string StartDate, string EndDate)
        {
            List<PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result> listWalletDebitDtls = new List<PROC_GET_ADMIN_WALLET_DEBIT_REPORT_Result>();
            try
            {
                //var DtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //var DtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var DtStartDate = objManager.ConvertDDMMYYToMMDDYY(StartDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var DtEndDate = objManager.ConvertDDMMYYToMMDDYY(EndDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                listWalletDebitDtls = db.PROC_GET_ADMIN_WALLET_DEBIT_REPORT(0, DtStartDate, DtEndDate).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listWalletDebitDtls, new List<string> { "UserId", "WalletDebitDtlId", }, "TDSReport", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=TDSReport_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Profite()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeProfiteVolume(string FromDate, string ToDate)
        {
            PROC_GET_ADMIN_PROFITE_DETAILS_Result objPROC_GET_ADMIN_PROFITE_DETAILS = new PROC_GET_ADMIN_PROFITE_DETAILS_Result();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                objPROC_GET_ADMIN_PROFITE_DETAILS = db.PROC_GET_ADMIN_PROFITE_DETAILS(dtFromDate, dtToDate).FirstOrDefault();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeProfite", objPROC_GET_ADMIN_PROFITE_DETAILS);
        }

        public PartialViewResult GetDateRangeProfite(string FromDate, string ToDate)
        {
            ProfitReport objProfitReport = new ProfitReport();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var objPROC_GET_ADMIN_PROFITE_DETAILS = db.PROC_GET_ADMIN_PROFITE_DETAILS(dtFromDate, dtToDate).FirstOrDefault();
                var objPROC_GET_ADMIN_VOLUME_DETAILS = db.PROC_GET_ADMIN_VOLUME_DETAILS(dtFromDate, dtToDate).FirstOrDefault();
                objProfitReport.Profit = objPROC_GET_ADMIN_PROFITE_DETAILS;
                objProfitReport.Volume = objPROC_GET_ADMIN_VOLUME_DETAILS;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeProfite", objProfitReport);
        }

        public PartialViewResult GetDateRangeVolume(string FromDate, string ToDate)
        {
            PROC_GET_ADMIN_VOLUME_DETAILS_Result objPROC_GET_ADMIN_VOLUME_DETAILS = new PROC_GET_ADMIN_VOLUME_DETAILS_Result();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                objPROC_GET_ADMIN_VOLUME_DETAILS = db.PROC_GET_ADMIN_VOLUME_DETAILS(dtFromDate, dtToDate).FirstOrDefault();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeVolume", objPROC_GET_ADMIN_VOLUME_DETAILS);
        }

        public ActionResult TopRetailer()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public PartialViewResult GetMonthWiseTopRetailerDetails(string MonthYear)
        {
            List<PROC_GET_TOP_RETAILER_DETAILS_Result> listRetailerDtl = new List<PROC_GET_TOP_RETAILER_DETAILS_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                listRetailerDtl = db.PROC_GET_TOP_RETAILER_DETAILS(Month, Year, 0).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseTopRetailerDetails", listRetailerDtl);
        }

        public ActionResult GetMonthWiseTopRetailerDetailsExport(string MonthYear)
        {
            List<PROC_GET_TOP_RETAILER_DETAILS_Result> listRetailerDtl = new List<PROC_GET_TOP_RETAILER_DETAILS_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                listRetailerDtl = db.PROC_GET_TOP_RETAILER_DETAILS(Month, Year, 0).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listRetailerDtl, new List<string> { "UserId" }, "TopRetailerReport", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=TopRetailerReport_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }
        
        public ActionResult RefferalUserList()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            List<PROC_GET_REFFERAL_USER_COUNT_REPORT_Result> listUsers = new List<PROC_GET_REFFERAL_USER_COUNT_REPORT_Result>();
            try
            {
                //listUsers = db.PROC_GET_REFFERAL_USER_COUNT_REPORT().ToList();
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                listMonthNames.Add(new MonthNames()
                {
                    Month = 0,
                    Year = 0,
                    MonthYear = 0 + "#" + 0,
                    MonthName = "ALL"
                });
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
                ViewBag.listMonthNames = listMonthNames;
            }
            catch (Exception)
            {

                throw;
            }
            return View(listUsers);
        }

        public JsonResult GetRefferalUserList(string MonthYear)
        {
            List<PROC_GET_REFFERAL_USER_COUNT_REPORT_Result> listRetailerDtl = new List<PROC_GET_REFFERAL_USER_COUNT_REPORT_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                listRetailerDtl = db.PROC_GET_REFFERAL_USER_COUNT_REPORT(Month, Year).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listRetailerDtl, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult RefferalUserListDetails(int UserId, string MonthYear)
        {
            List<PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT_Result> listUsers = new List<PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                listUsers = db.PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT(UserId, Month, Year).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_RefferalUserListDetails", listUsers);
        }


        [SessionExpireFilter]
        public ActionResult LeaderIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public JsonResult GetDateRangeLeaderIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN_Result> listDirectAmount = null;
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN(dtFromDate, dtToDate, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId).ToList();
            }
            catch (Exception ex)
            {
                listDirectAmount = new List<PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listDirectAmount, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDateRangeLeaderIncomeExport(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN_Result> listDirectAmount = null;
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN(dtFromDate, dtToDate, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listDirectAmount, new List<string> { "UserId" }, "LeaderIncomeReport", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=LeaderIncomeReport_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                listDirectAmount = new List<PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_HDR_FOR_ADMIN_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public PartialViewResult GetGoldDiamondIncome(int UserId, string FromDate, string ToDate, bool IsLeaderIncome)
        {
            List<PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN_Result> listDirectAmount = null;
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN(UserId, IsLeaderIncome, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                listDirectAmount = new List<PROC_RPT_GET_LEADER_AND_DIAMOND_INCOME_DETAIL_FOR_ADMIN_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetLeaderDiamondIncomeDetails", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult UplineSupportIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public JsonResult GetDateRangeUplineSupportIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN_Result> listDirectAmount = new List<PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN(dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listDirectAmount, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDateRangeUplineSupportIncomeExport(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN_Result> listDirectAmount = new List<PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_PRIME_AND_SILVER_INCOME_HDR_FOR_ADMIN(dtFromDate, dtToDate).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listDirectAmount, new List<string> { "UserId" }, "UplineSupportIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineSupportIncomeReport_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public PartialViewResult GetPrimeSilverIncome(int UserId, string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN_Result> listDirectAmount = new List<PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_PRIME_AND_SILVER_INCOME_DETAIL_FOR_ADMIN(UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetPrimeSilverIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult SilverIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeSilverIncome(string FromDate, string ToDate)
        {
            List<PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result> listDirectAmount = new List<PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS(false, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeSilverIncome", listDirectAmount);
        }

        public ActionResult GetDateRangeSilverIncomeExport(string FromDate, string ToDate)
        {
            List<PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result> listDirectAmount = new List<PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS(false, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId, dtFromDate, dtToDate).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listDirectAmount, new List<string> { "UserId" }, "UplineSupportIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineSupportIncomeReport_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult DiamondIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public PartialViewResult GetDateRangeDiamondIncome(string FromDate, string ToDate)
        {
            List<PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result> listDirectAmount = new List<PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_GET_RPT_ADMIN_USER_DIAMOND_SILVER_POINTS_ALL_DETAILS(true, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeDiamondIncome", listDirectAmount);
        }

        [SessionExpireFilter]
        public ActionResult DirectRefferalIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public JsonResult GetDateRangeDirectRefferalIncome(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT_Result> listDirectAmount = new List<PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT(dtFromDate, dtToDate, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listDirectAmount, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetDateRangeDirectRefferalIncomeExport(string FromDate, string ToDate)
        {
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                List<PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT_Result> listDirectAmount = db.PROC_RPT_GET_ADMIN_DIRECT_REFFERAL_REPORT(dtFromDate, dtToDate, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listDirectAmount, new List<string> { "UserId", "UserName" }, "DirectRefferalIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=DirectRefferalIncome_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss")));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public PartialViewResult GetDirectRefferalUserList(int UserId, string FromDate, string ToDate)
        {
            List<PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT_Result> listUsers = new List<PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listUsers = db.PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT(UserId, 0, 0).Where(i => i.ActivateDate >= dtFromDate && i.ActivateDate <= dtToDate).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_RefferalUserListDetails", listUsers);
        }

        [SessionExpireFilter]
        public ActionResult MatchingIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public JsonResult GetDateRangeMatchingIncome(string FromDate, string ToDate, string RefferalCode)
        {
            List<PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_Result> listDirectAmount = new List<PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT(dtFromDate, dtToDate, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId, RefferalCode).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listDirectAmount, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetDateRangeMatchingIncomeExport(string FromDate, string ToDate, string RefferalCode)
        {
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                List<PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_Result> listDirectAmount = db.PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT(dtFromDate, dtToDate, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId, RefferalCode).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listDirectAmount, new List<string> { "UserId", "UserName" }, "MatchingIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=MatchingIncome_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss")));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }


        #region "Upline Monthly Income"

        [SessionExpireFilter]
        public ActionResult UplineMonthlyIncome()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            try
            {
                List<UplineIncomeSummary> uplineIncome = db.UplineIncomeSummaries.ToList();
                foreach (var upline in uplineIncome)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = upline.Month,
                        Year = upline.Year,
                        MonthYear = upline.Month.ToString() + "#" + upline.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(upline.Month) + "-" + upline.Year.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        public JsonResult GetDateRangeUplineMonthly(string MonthYear, int QualifiedType)
        {
            List<PROC_GET_UPLINE_MONTHLY_INCOME_Result> lstUplineMonthlyIncome = new List<PROC_GET_UPLINE_MONTHLY_INCOME_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                bool QualifiedTypeBool = (QualifiedType == Convert.ToInt16("1"));

                lstUplineMonthlyIncome = db.PROC_GET_UPLINE_MONTHLY_INCOME((byte)Month, (short)Year, null, string.Empty, QualifiedTypeBool).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstUplineMonthlyIncome, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetDateRangeUplineMonthlyExport(string MonthYear, int QualifiedType)
        {
            List<PROC_GET_UPLINE_MONTHLY_INCOME_Result> lstUplineMonthlyIncome = new List<PROC_GET_UPLINE_MONTHLY_INCOME_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                bool QualifiedTypeBool = (QualifiedType == Convert.ToInt16("1"));

                lstUplineMonthlyIncome = db.PROC_GET_UPLINE_MONTHLY_INCOME((byte)Month, (short)Year, null, string.Empty, QualifiedTypeBool).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(lstUplineMonthlyIncome, new List<string> { "UserId", "Month", "Year", "UplineBugetIncome", "TotalMatchingIncome", "TotalBVQualifier", "UplineIncomeDetailId", "UplineIncomeSummaryId" }, "UplineMonthlyIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineMonthlyIncome_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lstUplineMonthlyIncome = new List<PROC_GET_UPLINE_MONTHLY_INCOME_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public PartialViewResult GetUplineIncomeGoldUserDetails(int UplineIncomeSummaryId)
        {
            List<PROC_GET_UPLINE_MONTHLY_GOLD_USERS_INCOME_Result> UplineIncomeGoldDetail = new List<PROC_GET_UPLINE_MONTHLY_GOLD_USERS_INCOME_Result>();
            try
            {
                object[] xparams = { new SqlParameter("@UplineIncomeSummaryId", UplineIncomeSummaryId) };
                UplineIncomeGoldDetail = db.Database.SqlQuery<PROC_GET_UPLINE_MONTHLY_GOLD_USERS_INCOME_Result>("PROC_GET_UPLINE_MONTHLY_GOLD_USERS_INCOME @UplineIncomeSummaryId", xparams).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetUplineIncomeGoldUserDetails", UplineIncomeGoldDetail);
        }

        public JsonResult UplineIncomeReCalculate(int UplineIncomeSummaryId, int UplineBugetIncomeVal)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                UplineIncomeSummary uplineIncomeSumm = db.UplineIncomeSummaries.Where(p => p.UplineIncomeSummaryId == UplineIncomeSummaryId).FirstOrDefault();
                if (!uplineIncomeSumm.IsLock)
                {
                    uplineIncomeSumm.UplineBugetIncome = UplineBugetIncomeVal;
                    uplineIncomeSumm.BVMultiplier = (uplineIncomeSumm.UplineBugetIncome / uplineIncomeSumm.TotalBVQualifier);
                    uplineIncomeSumm.UpdatedDate = DateTime.Now;

                    List<UplineIncomeDetail> uplineIncomeDetails = db.UplineIncomeDetails.Where(p => p.UplineIncomeSummaryId == UplineIncomeSummaryId && p.IsQualified == true).ToList();
                    foreach (var obj in uplineIncomeDetails)
                    {
                        obj.UplineIncome = obj.MatchingBVInMonth * uplineIncomeSumm.BVMultiplier;
                        obj.UpdatedDate = DateTime.Now;
                    }
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Upline monthly income re-calculate successfully";
                }
                else
                {
                    response.Key = false;
                    response.Msg = "Upline monthly income lock for re-calculate";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.DenyGet);
        }

        #endregion "Upline Monthly Income"

        [SessionExpireFilter]
        public ActionResult VoltIncome()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");
            return View();
        }

        public JsonResult GetVoltIncomeList(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT_Result> listDirectAmount = new List<PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT(dtFromDate, dtToDate, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listDirectAmount, JsonRequestBehavior.DenyGet);
        }

        public PartialViewResult GetDateRangeVoltIncome(int UserId, string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_COMPANY_SHARE_AMOUNT_Result> listDirectAmount = null;
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_COMPANY_SHARE_AMOUNT(UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                listDirectAmount = new List<PROC_RPT_GET_COMPANY_SHARE_AMOUNT_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeVoltIncome", listDirectAmount);
        }

        public ActionResult GetDateRangeVoltIncomeExport(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT_Result> listDirectAmount = new List<PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT(dtFromDate, dtToDate, objclsLogin.IsAdmin ? 0 : objclsLogin.UserId).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listDirectAmount, new List<string> { "UserId" }, "VoltIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=VoltIncome_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                listDirectAmount = new List<PROC_RPT_GET_ADMIN_VOLT_INCOME_REPORT_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        #region Miles Details

        [SessionExpireFilter]
        public ActionResult MilesDetails()
        {
            var objWeekDatas = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeekDatas.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeekDatas.EndDate.Value.ToString("dd/MM/yyyy");

            var TotalCreditAmount = db.UserMilesDtls.Sum(x => x.TotalCreditMiles);
            var TotalDebitAmount = db.UserMilesDtls.Sum(x => x.TotalDebitMiles);
            ViewBag.TotalCreditAmount = TotalCreditAmount;
            ViewBag.TotalDebitAmount = TotalDebitAmount;
            ViewBag.OverAllPending = ((double)TotalCreditAmount - TotalDebitAmount);
            return View();
        }

        public JsonResult GetDateRangeMilesDetails(string FromDate, string ToDate)
        {
            List<PROC_RPT_GET_USER_MILES_FOR_ADMIN_Result> listDirectAmount = null;
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                listDirectAmount = db.PROC_RPT_GET_USER_MILES_FOR_ADMIN(objclsLogin.IsAdmin ? 0 : objclsLogin.UserId, dtFromDate, dtToDate).ToList();
            }
            catch (Exception ex)
            {
                listDirectAmount = new List<PROC_RPT_GET_USER_MILES_FOR_ADMIN_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listDirectAmount, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetMilesDetails(int UserId, string FromDate, string ToDate, int Type)
        {
            List<PROC_RPT_GET_USER_MILES_LEDGER_Result> listDirectAmount = null;
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                ViewBag.Type = Type;
                //listDirectAmount = db.PROC_RPT_GET_USER_MILES_DETAILS_FOR_ADMIN(Type, UserId, dtFromDate, dtToDate).ToList();
                if (Type == 1)
                {
                    listDirectAmount = db.PROC_RPT_GET_USER_MILES_LEDGER(UserId).Where(i => i.TransactionDate.Date >= dtFromDate.Date && i.TransactionDate.Date <= dtToDate.Date && i.MilesStatus.ToLower() == "credit")
                        .Select(x => new PROC_RPT_GET_USER_MILES_LEDGER_Result { FullName = x.TransactionDate.ToString("dd/MMM/yyyy"), BalanceAmount = Convert.ToDouble(x.CreditAmount), RefferalCode = x.FullName }).OrderByDescending(x => x.TransactionDate).ToList();
                }
                else
                {
                    listDirectAmount = db.PROC_RPT_GET_USER_MILES_LEDGER(UserId).Where(i => i.TransactionDate.Date >= dtFromDate.Date && i.TransactionDate.Date <= dtToDate.Date && (i.MilesStatus.ToLower() == "used" || i.MilesStatus.ToLower() == "lapsed"))
                        .Select(x => new PROC_RPT_GET_USER_MILES_LEDGER_Result { FullName = x.TransactionDate.ToString("dd/MMM/yyyy"), BalanceAmount = Convert.ToDouble(x.DebitAmount), RefferalCode = (x.MilesStatus.ToLower() == "lapsed" ? "Lapsed" : Convert.ToString(x.OrderNo)) }).OrderByDescending(x => x.TransactionDate).ToList();
                }
            }
            catch (Exception ex)
            {
                listDirectAmount = new List<PROC_RPT_GET_USER_MILES_LEDGER_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMilesDetails", listDirectAmount);
        }

        #endregion Miles Details

        #region Rewards Claim Approval

        [SessionExpireFilter]
        public ActionResult RewardsApproval()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        public PartialViewResult GetMonthWiseRewardApprovalDetails(string MonthYear)
        {
            List<PROC_GET_REWARDS_APPROVAL_DETAILS_Result> listWalletDebitDtls = new List<PROC_GET_REWARDS_APPROVAL_DETAILS_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var varWalletDebitDtls = db.PROC_GET_REWARDS_APPROVAL_DETAILS(Year, Month);

                if (varWalletDebitDtls.Any())
                {
                    listWalletDebitDtls = db.PROC_GET_REWARDS_APPROVAL_DETAILS(Year, Month).ToList();
                }

                ViewBag.listUserListEnt = listWalletDebitDtls;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseRewardApprovalDetails", listWalletDebitDtls);

        }

        [SessionExpireFilter]
        public JsonResult UpdateClaimRewardStatus(Int64 RewardClaimedMstId, string RefNo, string Notes, string AmtUsdForRewards, string AmtRewardBudget)
        {
            ResponseMsg response = new ResponseMsg();

            try
            {
                var objRewardClaimedMst = db.RewardClaimedMsts.Where(i => i.RewardClaimedMstId == RewardClaimedMstId).FirstOrDefault();
                if (objRewardClaimedMst != null)
                {
                    objRewardClaimedMst.ApprovalDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    objRewardClaimedMst.IsApproved = true;
                    objRewardClaimedMst.ReferenceNo = RefNo;
                    objRewardClaimedMst.ClaimApprovalNote = Notes;
                    objRewardClaimedMst.ApproveRewardBudget = Convert.ToDouble(AmtRewardBudget);
                    objRewardClaimedMst.AmtUsedForRewards = Convert.ToDouble(AmtUsdForRewards);

                    db.SaveChanges();
                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "Record not found.";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult UpdateClaimRewardStatusDis(Int64 RewardClaimedMstId, string Notes)
        {
            ResponseMsg response = new ResponseMsg();

            try
            {
                var objRewardClaimedMst = db.RewardClaimedMsts.Where(i => i.RewardClaimedMstId == RewardClaimedMstId).FirstOrDefault();
                if (objRewardClaimedMst != null)
                {
                    objRewardClaimedMst.ApprovalDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    objRewardClaimedMst.IsApproved = false;
                    objRewardClaimedMst.ReferenceNo = "DisApprove";
                    objRewardClaimedMst.ClaimApprovalNote = Notes;
                    objRewardClaimedMst.ApproveRewardBudget = 0;
                    objRewardClaimedMst.AmtUsedForRewards = 0;

                    var objUserWalletMst = db.UserWalletMsts.Where(i => i.UserId == objRewardClaimedMst.UserId).FirstOrDefault();
                    if (objUserWalletMst != null)
                    {
                        double ClaimedBV = objRewardClaimedMst.ClaimedRewardBV;

                        objUserWalletMst.MatchingBV = (Math.Round(objUserWalletMst.MatchingBV, 2) + Convert.ToDouble(ClaimedBV));
                    }

                    db.SaveChanges();
                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "Record not found.";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult ClaimedRewards()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        public PartialViewResult GetMonthWiseClaimedRewardDetails(string MonthYear, string Type = "all")
        {
            List<PROC_GET_CLAIMED_REWARDS_DETAILS_Result> listWalletDebitDtls = new List<PROC_GET_CLAIMED_REWARDS_DETAILS_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var varWalletDebitDtls = db.PROC_GET_CLAIMED_REWARDS_DETAILS(Year, Month, Type);

                if (varWalletDebitDtls.Any())
                {
                    listWalletDebitDtls = db.PROC_GET_CLAIMED_REWARDS_DETAILS(Year, Month, Type).ToList();
                }

                ViewBag.listUserListEnt = listWalletDebitDtls;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseClaimedRewardDetails", listWalletDebitDtls);

        }

        public ActionResult GetMonthWiseClaimedRewardDetailsExport(string MonthYear, string Type = "all")
        {
            List<PROC_GET_CLAIMED_REWARDS_DETAILS_Result> listWalletDebitDtls = new List<PROC_GET_CLAIMED_REWARDS_DETAILS_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var varWalletDebitDtls = db.PROC_GET_CLAIMED_REWARDS_DETAILS(Year, Month, Type);

                if (varWalletDebitDtls.Any())
                {
                    listWalletDebitDtls = db.PROC_GET_CLAIMED_REWARDS_DETAILS(Year, Month, Type).ToList();
                }

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listWalletDebitDtls, new List<string>(), "ClaimedRewardsSheet", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=ClaimedRewards_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View();
        }

        [SessionExpireFilter]
        public ActionResult RewardFund()
        {
            try
            {
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public PartialViewResult GetRewardFundDetails()
        {
            List<PROC_GET_REWARD_FUNDS_Result> listGetRewardFund = new List<PROC_GET_REWARD_FUNDS_Result>();
            try
            {
                var varGetRewardFund = db.PROC_GET_REWARD_FUNDS();
                decimal? TotalBudgetAmount = 0;
                if (varGetRewardFund.Any())
                {
                    listGetRewardFund = db.PROC_GET_REWARD_FUNDS().ToList();
                    TotalBudgetAmount = listGetRewardFund.Sum(x => x.Budget);
                }

                ViewBag.TotalBudgetAmount = TotalBudgetAmount;
                ViewBag.listGetRewardFund = listGetRewardFund;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetRewardFundDetails", listGetRewardFund);

        }

        public ActionResult GetRewardFundDetailsExport()
        {
            List<PROC_GET_REWARD_FUNDS_Result> listGetRewardFund = new List<PROC_GET_REWARD_FUNDS_Result>();
            try
            {
                var varGetRewardFund = db.PROC_GET_REWARD_FUNDS();
                if (varGetRewardFund.Any())
                {
                    listGetRewardFund = db.PROC_GET_REWARD_FUNDS().ToList();
                }

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listGetRewardFund, new List<string>(), "RewardFundSheet", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=RewardFund_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View();
        }
        
        #endregion Rewards Claim Approval

        #region "LCB Pool Monthly Income"
        
        [SessionExpireFilter]
        public ActionResult LCBPoolMonthlyIncome()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            try
            {
                var result = (from recordset in db.LcbPoolIncomes
                              select new
                              {
                                  recordset.Year
                                  ,
                                  recordset.Month
                              }).Distinct().ToList();

                //List<LcbPoolIncome> uplineIncome = db.LcbPoolIncomes.ToList();
                foreach (var upline in result)
                {
                    listMonthNames.Add(new MonthNames()


                    {
                        Month = upline.Month,
                        Year = upline.Year,
                        MonthYear = upline.Month.ToString() + "#" + upline.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(upline.Month) + "-" + upline.Year.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        public JsonResult GetLCBPoolMonthlyIncome(string MonthYear, int QualifiedType)
        {
            List<PROC_GET_LCBPOOL_MONTHLY_INCOME_ResultMdl> lstUplineMonthlyIncome = new List<PROC_GET_LCBPOOL_MONTHLY_INCOME_ResultMdl>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                bool QualifiedTypeBool = (QualifiedType == Convert.ToInt16("1"));

                object[] xparams = { new SqlParameter("@Month", (byte)Month)
                                    ,new SqlParameter("@Year", (short)Year)
                                    ,new SqlParameter("@UserId", DBNull.Value)
                                    //,new SqlParameter("@RefferalCode", string.Empty)
                                    ,new SqlParameter("@IsQualified", QualifiedTypeBool) };
                lstUplineMonthlyIncome = db.Database.SqlQuery<PROC_GET_LCBPOOL_MONTHLY_INCOME_ResultMdl>("PROC_GET_LCBPOOL_MONTHLY_INCOME @Month, @Year, @UserId, @IsQualified ", xparams).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstUplineMonthlyIncome, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetLCBPoolMonthlyIncomeExport(string MonthYear, int QualifiedType)
        {
            List<PROC_GET_LCBPOOL_MONTHLY_INCOME_ResultMdl> lstUplineMonthlyIncome = new List<PROC_GET_LCBPOOL_MONTHLY_INCOME_ResultMdl>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                bool QualifiedTypeBool = (QualifiedType == Convert.ToInt16("1"));

                object[] xparams = { new SqlParameter("@Month", (byte)Month)
                                    ,new SqlParameter("@Year", (short)Year)
                                    ,new SqlParameter("@UserId", DBNull.Value)
                                    //,new SqlParameter("@RefferalCode", string.Empty)
                                    ,new SqlParameter("@IsQualified", QualifiedTypeBool) };
                lstUplineMonthlyIncome = db.Database.SqlQuery<PROC_GET_LCBPOOL_MONTHLY_INCOME_ResultMdl>("PROC_GET_LCBPOOL_MONTHLY_INCOME @Month, @Year, @UserId, @IsQualified ", xparams).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(lstUplineMonthlyIncome, new List<string> { "LCBPoolIncomeId", "UserId", "Month", "Year" }, "UplineMonthlyIncome", false);
                
                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineMonthlyIncome_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lstUplineMonthlyIncome = new List<PROC_GET_LCBPOOL_MONTHLY_INCOME_ResultMdl>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        #endregion "LCB Pool Monthly Income"
        
        #region "Retail Reward Monthly Income"
        
        [SessionExpireFilter]
        public ActionResult RetailRewardMonthlyIncome()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            try
            {
                List<RetailRewardIncomeSummary> uplineIncome = db.RetailRewardIncomeSummaries.ToList();
                foreach (var upline in uplineIncome)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = upline.Month,
                        Year = upline.Year,
                        MonthYear = upline.Month.ToString() + "#" + upline.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(upline.Month) + "-" + upline.Year.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        public JsonResult GetRetailRewardMonthly(string MonthYear, int QualifiedType)
        {
            List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result> lstUplineMonthlyIncome = new List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                bool QualifiedTypeBool = (QualifiedType == Convert.ToInt16("1"));
                
                lstUplineMonthlyIncome = db.PROC_GET_RETAIL_REWARD_MONTHLY_INCOME((byte)Month, (short)Year, null, string.Empty, QualifiedTypeBool).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstUplineMonthlyIncome, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetRetailRewardMonthlyExport(string MonthYear, int QualifiedType)
        {
            List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result> lstUplineMonthlyIncome = new List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                bool QualifiedTypeBool = (QualifiedType == Convert.ToInt16("1"));
                lstUplineMonthlyIncome = db.PROC_GET_RETAIL_REWARD_MONTHLY_INCOME((byte)Month, (short)Year, null, string.Empty, QualifiedTypeBool).ToList();

                ExcelPackage excel = Utilities.GetExcelPackageForExport(lstUplineMonthlyIncome, new List<string> { "UserId", "Month", "Year", "RetailRewardIncomeSummaryId", "RetailRewardIncomeDetailId" }, "UplineMonthlyIncome", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineMonthlyIncome_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lstUplineMonthlyIncome = new List<PROC_GET_RETAIL_REWARD_MONTHLY_INCOME_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public PartialViewResult GetRetailRewardRepurchaseBVDetails(int RetailRewardIncomeSummaryId)
        {
            List<PROC_GET_RETAIL_REWARD_MONTHLY_REPURCHASEBV_DETAILS_Result> UplineIncomeGoldDetail = new List<PROC_GET_RETAIL_REWARD_MONTHLY_REPURCHASEBV_DETAILS_Result>();
            try
            {
                object[] xparams = { new SqlParameter("@RetailRewardIncomeSummaryId", RetailRewardIncomeSummaryId) };
                UplineIncomeGoldDetail = db.Database.SqlQuery<PROC_GET_RETAIL_REWARD_MONTHLY_REPURCHASEBV_DETAILS_Result>("PROC_GET_RETAIL_REWARD_MONTHLY_REPURCHASEBV_DETAILS @RetailRewardIncomeSummaryId", xparams).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetRetailRewardRepurchaseBVDetails", UplineIncomeGoldDetail);
        }

        public JsonResult RetailRewardIncomeReCalculate(int RetailRewardIncomeSummaryId, int RetailRewardBugetIncome)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                RetailRewardIncomeSummary uplineIncomeSumm = db.RetailRewardIncomeSummaries.Where(p => p.RetailRewardIncomeSummaryId == RetailRewardIncomeSummaryId).FirstOrDefault();
                if (!uplineIncomeSumm.IsLock)
                {
                    List<RetailRewardIncomeDetail> uplineIncomeDetails = db.RetailRewardIncomeDetails.Where(p => p.RetailRewardIncomeSummaryId == RetailRewardIncomeSummaryId && p.IsQualified == true).ToList();
                    decimal totBV = uplineIncomeDetails.ToList().Sum(p => p.TotalBV);

                    uplineIncomeSumm.RetailRewardBugetBV = (RetailRewardBugetIncome * 10 / 100);
                    uplineIncomeSumm.RetailRewardBugetIncome = RetailRewardBugetIncome;
                    uplineIncomeSumm.BVMultiplier = ((decimal)(uplineIncomeSumm.RetailRewardBugetBV / totBV));
                    uplineIncomeSumm.UpdatedDate = DateTime.Now;

                    foreach (var obj in uplineIncomeDetails)
                    {
                        obj.RetailRewardIncome = (obj.TotalBV * uplineIncomeSumm.BVMultiplier) * 10;
                        obj.UpdatedDate = DateTime.Now;
                    }
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Retail reward monthly income re-calculate successfully";
                }
                else
                {
                    response.Key = false;
                    response.Msg = "Retail reward monthly income lock for re-calculate";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.DenyGet);
        }

        #endregion "Retail Reward Monthly Income"

        #region User Miles File upload

        public class UserMiles
        {
            //public string IdNo { get; set; }
            public Int32 UserId { get; set; }
            public double Miles { get; set; }
            public bool IsIns { get; set; }
        }

        [HttpPost]
        public ActionResult MileUpload(FormCollection formCollection)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                if (Request != null && Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    ////HttpPostedFileBase file = Request.Files["UploadedFile"];
                    if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                    {
                        string fileName = file.FileName;
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                        var objUserMiles = new List<UserMiles>();

                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            foreach (ExcelWorksheet worksheet in package.Workbook.Worksheets)
                            {
                                var workSheet = worksheet;

                                if (workSheet != null && worksheet.Dimension != null && worksheet.Dimension.End != null)
                                {
                                    var noOfCol = workSheet.Dimension.End.Column;
                                    var noOfRow = workSheet.Dimension.End.Row;

                                    for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                                    {
                                        UserMiles usermiles = new UserMiles();
                                        string IdNo = Convert.ToString(workSheet.Cells[rowIterator, 1].Value);

                                        if (!string.IsNullOrEmpty(IdNo))
                                        {
                                            Int32 UserId = db.UserMsts.Where(x => x.RefferalCode == IdNo).Select(x => x.UserId).FirstOrDefault();
                                            usermiles.UserId = UserId;

                                            string strExcelMiles = Convert.ToString(workSheet.Cells[rowIterator, 2].Value);
                                            double dblExcelMiles = 0;
                                            if (!double.TryParse(strExcelMiles, out dblExcelMiles))
                                            {
                                                response.Key = false;
                                                response.Msg = "Enter Valid Miles in format";
                                                return Json(response, JsonRequestBehavior.AllowGet);
                                            }

                                            usermiles.Miles = Convert.ToDouble(strExcelMiles);

                                            if (UserId != 0 && usermiles.Miles > 0)
                                            {
                                                usermiles.IsIns = false;
                                                objUserMiles.Add(usermiles);
                                            }
                                            else
                                            {
                                                response.Key = false;
                                                response.IsInfo = true;
                                                response.Msg = "Invalid User Id No!";

                                                return Json(response, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        if (objUserMiles != null && objUserMiles.Count > 0)
                        {
                            var updatedUserMiles = objUserMiles.GroupBy(s => new { s.UserId, s.IsIns })
                                .Select(g => new
                                {
                                    UserId = g.Key.UserId,
                                    IsIns = g.Key.IsIns,
                                    TotalMiles = g.Sum(x => Math.Round(Convert.ToDouble(x.Miles), 2))
                                }).Distinct();

                            foreach (var item in updatedUserMiles)
                            {
                                var IsUserMilesExists = db.UserMilesDtls.Where(x => x.UserId == item.UserId);
                                if (IsUserMilesExists.Any())
                                {
                                    var ExistsUserMiles = db.UserMilesDtls.Where(x => x.UserId == item.UserId).FirstOrDefault();
                                    ExistsUserMiles.TotalCreditMiles = ExistsUserMiles.TotalCreditMiles + item.TotalMiles;
                                }
                                else
                                {
                                    UserMilesDtl tblUserMilesDtl = new UserMilesDtl();
                                    tblUserMilesDtl.UserId = item.UserId;
                                    tblUserMilesDtl.TotalCreditMiles = item.TotalMiles;
                                    db.UserMilesDtls.Add(tblUserMilesDtl);
                                }

                                var objMilesCreditDtl = new MilesCreditDtl();
                                objMilesCreditDtl.CreatedBy = objclsLogin.UserId;
                                objMilesCreditDtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                                objMilesCreditDtl.CreditAmount = (decimal)item.TotalMiles;
                                objMilesCreditDtl.CreditDate = objMilesCreditDtl.CreatedDate;
                                objMilesCreditDtl.IsActive = true;
                                objMilesCreditDtl.UserId = item.UserId;
                                objMilesCreditDtl.UserIdFrom = objclsLogin.UserId;
                                objMilesCreditDtl.InsertType = (int)MilesCreditType.ForFileUpload;
                                db.MilesCreditDtls.Add(objMilesCreditDtl);

                                #region Notofication

                                var MilesUser = db.UserMsts.Where(x => x.UserId == item.UserId).FirstOrDefault();

                                var Notificationuser = item.TotalMiles + " miles has been credited";
                                var objNotificationUser = new NotificationMst();
                                objNotificationUser.CreatedBy = objclsLogin.UserId;
                                objNotificationUser.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                                objNotificationUser.IsActive = true;
                                objNotificationUser.ModifiedBy = objclsLogin.UserId;
                                objNotificationUser.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                                objNotificationUser.Notification = Notificationuser;
                                objNotificationUser.NotificationTypeId = NotificationType.Information.GetHashCode();
                                objNotificationUser.UserId = MilesUser.UserId;
                                db.NotificationMsts.Add(objNotificationUser);

                                #endregion Notofication
                            }

                            db.SaveChanges();

                            response = new ResponseMsg();
                            response.Key = true;
                            response.Msg = "User Miles updated successfully!";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                response = new ResponseMsg();
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }

    public class ProductReportEnt
    {
        public string Name { get; set; }
        public decimal Percentage { get; set; }
        public decimal Amount { get; set; }
    }

    public class SalesReportEnt : PROC_GET_ORDER_INVOICE_DETAILS_Result
    {
        //public double ProductPrice { get; set; }
        public double ProductTotalAmount { get; set; }

        public double CGST { get; set; }
        public double SGST { get; set; }
        public double IGST { get; set; }
        public double? BusinessVolume { get; set; }
        public string StateName { get; set; }
        public string PaymentId { get; set; }
        public double? UsedMiles { get; set; }


    }

    public class RewardApprovalEnt : PROC_GET_REWARDS_APPROVAL_DETAILS_Result
    {
        public Int64? SrNo { get; set; }
        public Int64 RewardClaimedMstId { get; set; }
        public DateTime ClaimedDate { get; set; }
        public string IdNo { get; set; }
        public string ContactDetails { get; set; }
        public string EmailId { get; set; }
        public double MatchingBV { get; set; }
        public double ClaimedRewardBV { get; set; }
        public double PendingRewardBV { get; set; }
    }
}