﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using TRSImplementation;

namespace TRS.Areas.Admin.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class PointsProductController : clsBase
    {
        // GET: Admin/Product
        [SessionExpireFilter]
        public ActionResult Index()
        {
            var objProductDtl = new PackageMst();
            ViewBag.listProductMst = db.PackageMsts.ToList();
            ViewBag.listProductCategory = db.ProductCategoryMsts.Select(i => new SelectListItem { Text = i.CategoryName, Value = i.ProductCategoryId.ToString() }).ToList();
            return View(objProductDtl);
        }

        public JsonResult GetProductDetails(int ProductId)
        {

            var objProductDtl = db.PackageMsts.Where(i => i.PackageId == ProductId).FirstOrDefault();
            return Json(objProductDtl, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertUpdateProduct()
        {
            ResponseMsg response = new ResponseMsg();
            bool isError = false;
            try
            {
                response.Key = true;
                string PackageId = System.Web.HttpContext.Current.Request.Form["PackageId"];
                string PackageName = System.Web.HttpContext.Current.Request.Form["PackageName"];                
                string ShortDesc = System.Web.HttpContext.Current.Request.Form["ShortDesc"];
                string LongDescription = System.Web.HttpContext.Current.Request.Form["LongDescription"];
                string PackagePrice = System.Web.HttpContext.Current.Request.Form["PackagePrice"];                
                string FrontCover = System.Web.HttpContext.Current.Request.Form["FrontCover"];
                string BackCover = System.Web.HttpContext.Current.Request.Form["BackCover"];
                string ProductVideoURL = System.Web.HttpContext.Current.Request.Form["ProductVideoURL"];
                int id = !string.IsNullOrEmpty(PackageId) ? Convert.ToInt32(PackageId) : 0;
                if (string.IsNullOrEmpty(PackageName))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Package Name!";
                }                
                else if (string.IsNullOrEmpty(PackagePrice))
                {
                    response.Key = false;
                    response.Msg = "Please Enter Package Price!";
                }
                
                if (!response.Key)
                {
                    isError = true;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    if (System.Web.HttpContext.Current.Request.Files["FrontCover"] != null)
                    {
                        var pic = System.Web.HttpContext.Current.Request.Files["FrontCover"];
                        if (pic.ContentLength > 0)
                        {
                            if (!System.IO.Directory.Exists(Server.MapPath("/Images/ProductImages/")))
                            {
                                System.IO.Directory.CreateDirectory(Server.MapPath("/Images/ProductImages/"));
                            }
                            var _comPath = Server.MapPath("/Images/ProductImages/") + FrontCover;
                            var path = _comPath;
                            //pic.SaveAs(path);

                            WebImage img = new WebImage(pic.InputStream);
                            if (img.Height > img.Width)
                            {
                                img.Resize(400, 600, true, true);
                                img.Save(path);
                            }
                            else
                            {
                                img.Resize(600, 400, true, true);
                                img.Save(path);
                            }
                            FrontCover = "/Images/ProductImages/" + FrontCover;
                        }
                    }
                    else
                    {
                        if (FrontCover.Length > 0)
                        {
                            FrontCover = "/Images/ProductImages/" + FrontCover;
                        }
                    }
                    if (System.Web.HttpContext.Current.Request.Files["BackCover"] != null)
                    {
                        var picBackCover = System.Web.HttpContext.Current.Request.Files["BackCover"];
                        if (picBackCover.ContentLength > 0)
                        {
                            if (!System.IO.Directory.Exists(Server.MapPath("/Images/ProductImages/")))
                            {
                                System.IO.Directory.CreateDirectory(Server.MapPath("/Images/ProductImages/"));
                            }
                            var _comPath = Server.MapPath("/Images/ProductImages/") + BackCover;
                            var path = _comPath;
                            //picBackCover.SaveAs(path);

                            WebImage img = new WebImage(picBackCover.InputStream);
                            if (img.Height > img.Width)
                            {
                                img.Resize(400, 600, true, true);
                                img.Save(path);
                            }
                            else
                            {
                                img.Resize(600, 400, true, true);
                                img.Save(path);
                            }


                            BackCover = "/Images/ProductImages/" + BackCover;
                        }
                    }
                    else
                    {
                        if (BackCover.Length > 0)
                        {
                            BackCover = "/Images/ProductImages/" + BackCover;
                        }
                    }
                }
                else if (id == 0)
                {
                    FrontCover = string.Empty;
                    BackCover = string.Empty;
                }
                else if (id > 0)
                {
                    if (FrontCover.Length > 0)
                    {
                        FrontCover = "/Images/ProductImages/" + FrontCover;
                    }
                    if (BackCover.Length > 0)
                    {
                        BackCover = "/Images/ProductImages/" + BackCover;
                    }
                }

                PackageMst objProductMst = new PackageMst();
                if (id == 0)
                {
                    objProductMst = new PackageMst();
                }
                else
                {
                    objProductMst = db.PackageMsts.Where(i => i.PackageId == id).FirstOrDefault();
                }
                objProductMst.PackageName = PackageName;                
                objProductMst.PackagePrice = Convert.ToDouble(PackagePrice);                
                objProductMst.ShortDesc = ShortDesc;
                objProductMst.LongDescription = LongDescription;
                objProductMst.ImagePath = FrontCover;
                objProductMst.BackCoverImagePath = BackCover;
                objProductMst.ProductVideoURL = ProductVideoURL;
                objProductMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                if (id == 0)
                {
                    objProductMst.IsActive = true;
                    objProductMst.CreatedBy = 0;
                    objProductMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    objProductMst.ModifiedBy = 0;
                    db.PackageMsts.Add(objProductMst);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActiveInActiveProduct(int ProductId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objProductMst = db.PackageMsts.Where(i => i.PackageId == ProductId).FirstOrDefault();
                if (objProductMst != null)
                {
                    if (objProductMst.IsActive)
                    {
                        objProductMst.IsActive = false;
                        response.Msg = "Package inactive successfully!";
                    }
                    else
                    {
                        objProductMst.IsActive = true;
                        response.Msg = "Package activate successfully!";
                    }
                    objProductMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    db.SaveChanges();

                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "Product does not exist!";
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}