﻿using DocumentFormat.OpenXml.Spreadsheet;
using Elmah;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using TRS.Models;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Admin.Controllers
{
    public class OrdersController : clsBase
    {
        private const string UploadFilePath = "~/PayoutExcel/";

        // GET: Admin/Orders
        public ActionResult Index()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            try
            {
                DateTime baseDate = (DateTime)db.PROC_GET_SERVER_DATE().FirstOrDefault();
                var yesterday = baseDate.AddDays(-1);
                var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                var thisWeekEnd = thisWeekStart.AddDays(7).AddDays(-1);
                ViewBag.StartDate = thisWeekStart.ToString("dd/MM/yyyy");
                ViewBag.EndDate = thisWeekEnd.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public ActionResult GetMonthWeekDtl(string MonthYear)
        {
            List<WeekNames> listWeekNames = null;
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, 0).Select(i => new WeekNames { FromDate = i.StartDate.Value, ToDate = i.EndDate.Value, WeekName = i.StartDate.Value.ToString("dd/MM/yyyy") + " to " + i.EndDate.Value.ToString("dd/MM/yyyy"), WeekNo = i.WeekNo.Value }).ToList();
            }
            catch (Exception ex)
            {
                listWeekNames = new List<WeekNames>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listWeekNames, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public PartialViewResult GetDateRangeOrderDetails(int OrderStatusId, string StartDate, string EndDate, int OrderType)
        {
            List<PROC_GET_USER_ORDER_HDR_Result> listOrder = null;
            try
            {
                var DtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var DtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                listOrder = db.PROC_GET_USER_ORDER_HDR(0, 0).Where(i => i.OrderDate >= DtStartDate &&
                                                                   i.OrderDate <= DtEndDate &&
                                                                   i.OrderStatusId == (OrderStatusId == 0 ? i.OrderStatusId : OrderStatusId) &&
                                                                   i.IsRepurchaseOrder == (OrderType == -1 ? i.IsRepurchaseOrder : (OrderType == 1)))
                                                            .OrderByDescending(i => i.UserPurchaseHdrId).ToList();
                ViewBag.listFranchisee = db.PROC_GET_FRANCHISEE_LIST(0).Select(i => new { FranchiseeName = i.FranchiseeName, FranchiseeId = i.FranchiseeId }).ToList();
                ViewBag.listOrderReceivedBy = db.OrderReceivedByMsts.Select(i => new { OrderReceivedBy = i.OrderReceivedBy, OrderReceivedById = i.OrderReceivedById }).ToList();
            }
            catch (Exception ex)
            {
                listOrder = new List<PROC_GET_USER_ORDER_HDR_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetDateRangeOrderDetails", listOrder);
        }

        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult CompleteOrderDetails(Int64 UserPurchaseHdrId)
        {
            return Json(OrderStatusUpdate(UserPurchaseHdrId, OrderStatus.OrderCompleted), JsonRequestBehavior.AllowGet);
        }

        public ActionResult TrackingOrderDetails(Int64 UserPurchaseHdrId, string CourierTrackingNo, string CourierTrackingCompany)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                using (var context = new TRSEntities(3600))
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var objUserPurchaseHdr = context.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).FirstOrDefault();
                            objUserPurchaseHdr.CourierTrackingNo = CourierTrackingNo;
                            objUserPurchaseHdr.CourierTrackingCompany = CourierTrackingCompany;

                            //Send SMS for Courier TrackingNo Notification...
                            bool isSMSSend = objUserPurchaseHdr.IsSMSSendforCourierTracking ?? false;
                            if (!isSMSSend)
                            {
                                var objUser = db.UserMsts.Where(p => p.UserId == objUserPurchaseHdr.UserId).FirstOrDefault();
                                //string Msg = string.Format("Dear {0} your order no {1} have been dispatched and you can track your order from here {2}.Thanks and regards.Team Maxener wellness.",
                                //                           objUser.FullName, objUserPurchaseHdr.OrderNo, CourierTrackingNo);
                                string Msg = string.Format("Hey, Greetings from Maxener Wellness. Your order {0} has been dispatched your tracking no {1} you can track at {2}."
                                                            , objUserPurchaseHdr.OrderNo, CourierTrackingNo, CourierTrackingCompany);
                                objManager.SendSMS(objUser.MobileNo, "1107160293407273758", Msg, "Courier tracking notification to user");
                                objUserPurchaseHdr.IsSMSSendforCourierTracking = true;
                            }
                            context.SaveChanges();
                            transaction.Commit();
                            response.Key = true;
                            response.Msg = "Tracking Order Successfully!";
                        }
                        catch (Exception ex)
                        {
                            response.Key = false;
                            response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                            transaction.Rollback();
                            ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private DataTable ExecuteStoreQuery(string commandText, params Object[] parameters)
        {
            DataTable retVal = new DataTable();

            db.Database.Connection.Open();
            var con = (SqlConnection)db.Database.Connection;
            var cmd = new SqlCommand(commandText, con);
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (var parameter in parameters)
            {
                cmd.Parameters.Add(parameter);
            }
            using (var rdr = cmd.ExecuteReader())
            {
                retVal.Load(rdr);
            }

            return retVal;
        }

        public ActionResult ExportToExcel(int OrderStatusId, int? OrderType, string StartDate, string EndDate, int? WeekNo)
        {
            var dtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var dtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var startDate = new SqlParameter
            {
                ParameterName = "startDate",
                Value = dtStartDate // "2020/05/01"
            };

            var endDate = new SqlParameter
            {
                ParameterName = "endDate",
                Value = dtEndDate
            };

            var orderStatusId = new SqlParameter
            {
                ParameterName = "orderStatusId",
                Value = OrderStatusId
            };

            var isRepurchaseOrder = new SqlParameter
            {
                ParameterName = "isRepurchaseOrder",
                Value = (OrderType == -1 ? 0 : (OrderType == 1 ? 1 : 2))
            };

            DataTable dt = ExecuteStoreQuery("Temp_GetOrderData_DV", startDate, endDate, orderStatusId, isRepurchaseOrder);
            dt.Columns["Product to be Shipped"].ReadOnly = false;
            dt.Columns["Product to be Shipped"].MaxLength = 5000;
            dt.Columns["Weight"].ReadOnly = false;
            dt.Columns["Weight"].MaxLength = 50;

            dt.Columns["BV"].ReadOnly = false;
            dt.Columns["BV"].MaxLength = 10;

            foreach (DataRow item in dt.Rows)
            {
                var products = item["ProductsDetails"].ToString();
                List<ProductDetails> productDetails = JsonConvert.DeserializeObject<List<ProductDetails>>(products);

                var orderType = item["Order Type"].ToString();
                string shipmentDetails = string.Empty;
                float ORderBV = 0;
                for (int i = 0; i < productDetails.Count; i++)
                {
                    shipmentDetails += string.Format("{5}. ({0}, {1}, {2}, {3}, {4}),{6} ", productDetails[i].ProductName, productDetails[i].WeightGM, productDetails[i].VariantName, productDetails[i].Qty, orderType, (i + 1).ToString(), "\n");
                    ORderBV += productDetails[i].DirectMatchingVolume + productDetails[i].RepurchaseMatchingVolume;
                }
                item["BV"] = ORderBV;
                item["Product to be Shipped"] = shipmentDetails;

                if (orderType.Equals("Repurchase", StringComparison.CurrentCultureIgnoreCase))
                {
                    item["Weight"] = productDetails.Sum(x => x.WeightGM) * 2;
                }
                else
                {
                    item["Weight"] = productDetails.Sum(x => x.WeightGM);
                }
            }
            dt.Columns.Remove("ProductsDetails");
            dt.Columns.Remove("Order Type");

            //var grid = new GridView();
            //grid.DataSource = dt;
            //grid.DataBind();

            var Orders = dt;
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Orders");
            var totalCols = Orders.Columns.Count;
            var totalRows = Orders.Rows.Count;

            for (var col = 1; col <= totalCols; col++)
            {
                workSheet.Cells[1, col].Value = Orders.Columns[col - 1].ColumnName;
            }
            for (var row = 1; row <= totalRows; row++)
            {
                for (var col = 0; col < totalCols; col++)
                {
                    workSheet.Cells[row + 1, col + 1].Value = Orders.Rows[row - 1][col];
                }
            }
            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Orders_Exported.xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }

            //Response.ClearContent();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition", "attachment; filename=Orders Exported" + DateTime.Now.ToString("ddmmyyyy") + ".xls");
            //Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            //Response.Charset = "";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter htw = new HtmlTextWriter(sw);

            //grid.RenderControl(htw);

            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.End();

            return View();
        }

        /// <summary>
        /// SaveOrderNote
        /// </summary>
        /// <param name="UserPurchaseHdrId"></param>
        /// <param name="OrderNotes"></param>
        /// <param name="RazorpayOrderId"></param>
        /// <param name="RazorpayPaymentId"></param>
        /// <param name="RazorpaySignature"></param>
        /// <returns></returns>
        public JsonResult SaveOrderNote(Int64 UserPurchaseHdrId, string OrderNotes, string RazorpayOrderId, string RazorpayPaymentId, string RazorpaySignature)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                using (var context = new TRSEntities(3600))
                {
                    var objUserPurchaseHdr = context.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).FirstOrDefault();
                    if (objUserPurchaseHdr != null)
                    {
                        objUserPurchaseHdr.Notes = OrderNotes;

                        if ((bool)objUserPurchaseHdr.IsOrderPlaceByAdmin)
                        {
                            objUserPurchaseHdr.RazorpayOrderId = RazorpayOrderId;
                            objUserPurchaseHdr.RazorpayPaymentId = RazorpayPaymentId;
                            objUserPurchaseHdr.RazorpaySignature = RazorpaySignature;
                        }
                        context.SaveChanges();
                        response.Key = true;
                        response.Msg = "Order notes saved successfully!";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// OrderStatusUpdate
        /// </summary>
        /// <param name="UserPurchaseHdrId"></param>
        /// <param name="OrderStatusToBe"></param>
        /// <returns></returns>
        protected ResponseMsg OrderStatusUpdate(Int64 UserPurchaseHdrId, OrderStatus OrderStatusToBe)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                using (var context = new TRSEntities(3600))
                {
                    clsLogin objclsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var ServerDate = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            List<string> listConfigs = new List<string>();
                            listConfigs.Add(ConfigKeys.MinRegistrationPoints);
                            listConfigs.Add(ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints);
                            listConfigs.Add(ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER);

                            var listSMSDtl = context.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                            double MinRegistrationPoints = 0;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints))
                            {
                                try
                                {
                                    MinRegistrationPoints = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            bool AllowToPurchaseBelowMinRegistrationPoints = false;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints))
                            {
                                try
                                {
                                    AllowToPurchaseBelowMinRegistrationPoints = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            var isPointsCreditdtlGenerateWhilePlaceOrder = false;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER))
                            {
                                try
                                {
                                    isPointsCreditdtlGenerateWhilePlaceOrder = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER).Value);
                                }
                                catch (Exception ex)
                                {
                                    ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }

                            var objUserPurchaseHdr = context.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).FirstOrDefault();
                            if (objUserPurchaseHdr != null)
                            {
                                if (objUserPurchaseHdr.OrderStatusId.HasValue && objUserPurchaseHdr.OrderStatusId.Value == OrderStatus.OrderConfirm.GetHashCode() && OrderStatusToBe == OrderStatus.OrderConfirm)
                                {
                                    response.Key = false;
                                    response.IsInfo = true;
                                    response.Msg = "Order is already confirm!";
                                }
                                else if (objUserPurchaseHdr.OrderStatusId.HasValue && objUserPurchaseHdr.OrderStatusId.Value == OrderStatus.OrderCompleted.GetHashCode())
                                {
                                    response.Key = false;
                                    response.IsInfo = true;
                                    response.Msg = "Order is already completed!";
                                }
                                else
                                {
                                    int? dbOrderStatusId = objUserPurchaseHdr.OrderStatusId;
                                    objUserPurchaseHdr.OrderStatusId = (int)OrderStatusToBe;
                                    objUserPurchaseHdr.ModifiedDate = ServerDate;
                                    var objUserMst = context.UserMsts.Where(i => i.UserId == objUserPurchaseHdr.UserId).FirstOrDefault();
                                    var objReferralUser = context.UserMsts.Where(i => i.UserId == objUserMst.RefferalUserId).FirstOrDefault();

                                    if (dbOrderStatusId == (int)OrderStatus.OrderGenerate 
                                        && (OrderStatusToBe == OrderStatus.OrderCompleted || OrderStatusToBe == OrderStatus.OrderConfirm))
                                    {
                                        #region Points

                                        if (objUserPurchaseHdr.IsRepurchaseOrder.HasValue)
                                        {
                                            double CreditAmount = 0;

                                            if (objUserPurchaseHdr.IsRepurchaseOrder.Value)
                                            {
                                                foreach (var objUserPurchaseDtl in objUserPurchaseHdr.UserPurchaseDtls)
                                                {
                                                    objManager.InsertUserPointsOnPurchase(context, objUserPurchaseDtl, objUserMst.UserId, objUserMst.RefferalUserId, WalletCreditType.Repurchase, isPointsCreditdtlGenerateWhilePlaceOrder);
                                                }
                                            }
                                            else
                                            {
                                                foreach (var objUserPurchaseDtl in objUserPurchaseHdr.UserPurchaseDtls)
                                                {
                                                    objManager.InsertUserPointsOnPurchase(context, objUserPurchaseDtl, objUserMst.UserId, objUserMst.RefferalUserId, WalletCreditType.Direct, isPointsCreditdtlGenerateWhilePlaceOrder);

                                                    var objProductMst = context.ProductMsts.Where(i => i.ProductId == objUserPurchaseDtl.ProductId).FirstOrDefault();
                                                    objManager.CalculateWalletCreditAmountRegistration(objProductMst, (double)objUserPurchaseDtl.Qty, ref CreditAmount, WalletCreditType.Direct);
                                                }
                                            }

                                            if (isPointsCreditdtlGenerateWhilePlaceOrder)
                                                db.PROC_INSERT_ALL_PARENT_USER_NOTIFICATIONMST(objclsLogin.UserId, NotificationType.Information.GetHashCode(), true);

                                            if (!objUserPurchaseHdr.IsRepurchaseOrder.Value)
                                            {
                                                if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                                {
                                                    var RegistrationPoints = objUserMst.RegistrationPoints.HasValue ? objUserMst.RegistrationPoints.Value : 0;
                                                    RegistrationPoints += CreditAmount;

                                                    if (RegistrationPoints >= MinRegistrationPoints || AllowToPurchaseBelowMinRegistrationPoints)
                                                    {
                                                        objUserMst.IsRegistrationActivated = true;
                                                        objUserMst.ActivateDate = ServerDate;
                                                    }
                                                    objUserMst.RegistrationPoints = RegistrationPoints;
                                                }

                                                #region Wallet Credit

                                                if (objReferralUser.RefferalUserId != 0)
                                                {
                                                    objManager.InsertWalletCredit(context, objUserPurchaseHdr, objUserMst.UserId, objUserMst.RefferalUserId, WalletCreditType.Direct);
                                                }

                                                #endregion Wallet Credit
                                            }
                                        }

                                        #endregion Points    
                                    }

                                    context.SaveChanges();
                                    transaction.Commit();

                                    response.Key = true;
                                    response.Msg = string.Format("Order {0} successfully!", OrderStatusToBe == OrderStatus.OrderConfirm ? "Confirm" : "Completed");
                                }
                            }
                            else
                            {
                                response.Key = false;
                                response.IsInfo = true;
                                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                            }
                            
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return response;
        }

        /// <summary>
        /// ConfirmOrderProcess
        /// </summary>
        /// <param name="UserPurchaseHdrId"></param>
        /// <returns></returns>
        protected ResponseMsg ConfirmOrderProcess(Int64 UserPurchaseHdrId)
        {
            return OrderStatusUpdate(UserPurchaseHdrId, OrderStatus.OrderConfirm);
        }

        public JsonResult ConfirmOrder(Int64 UserPurchaseHdrId)
        {
            return Json(ConfirmOrderProcess(UserPurchaseHdrId), JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult DeleteOrder(Int64 UserPurchaseHdrId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                clsLogin objclsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                if (objclsLogin.UserType != clsImplementationEnum.UserType.Admin && !objclsLogin.RedirectFrmAdmin)
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "You are not authorized User to perform the delete operation for this order.";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                try
                {
                    var objUserPurchaseHdr = db.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).FirstOrDefault();
                    if (objUserPurchaseHdr != null)
                    {
                        if (objUserPurchaseHdr.OrderStatusId.HasValue && objUserPurchaseHdr.OrderStatusId.Value != clsImplementationEnum.OrderStatus.OrderGenerate.GetHashCode())
                        {
                            response.Key = false;
                            response.IsInfo = true;
                            response.Msg = "Order is already confirm!";
                        }
                        else
                        {
                            var listOrderDtl = db.UserPurchaseDtls.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).ToList();
                            db.UserPurchaseDtls.RemoveRange(listOrderDtl);
                            db.UserPurchaseHdrs.Remove(objUserPurchaseHdr);

                            db.SaveChanges();

                            response.Key = true;
                            response.Msg = "Order delete successfully!";
                        }
                    }
                    else
                    {
                        response.Key = false;
                        response.IsInfo = true;
                        response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AssignFranchisee(Int64 UserPurchaseHdrId, int FranchiseeId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                try
                {
                    var objUserPurchaseHdr = db.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).FirstOrDefault();
                    if (objUserPurchaseHdr != null)
                    {
                        objUserPurchaseHdr.FranchiseeId = FranchiseeId;
                        objUserPurchaseHdr.ModifiedBy = 0;
                        objUserPurchaseHdr.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        db.SaveChanges();

                        response.Key = true;
                        response.Msg = "Order Successfully Assign To Franchisee!";
                    }
                    else
                    {
                        response.Key = false;
                        response.IsInfo = true;
                        response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateOrderReceivedBy(Int64 UserPurchaseHdrId, int OrderReceivedById)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                try
                {
                    var objUserPurchaseHdr = db.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).FirstOrDefault();
                    if (objUserPurchaseHdr != null)
                    {
                        objUserPurchaseHdr.OrderReceivedById = OrderReceivedById;
                        objUserPurchaseHdr.ModifiedBy = 0;
                        objUserPurchaseHdr.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        db.SaveChanges();

                        response.Key = true;
                        response.Msg = "Order Details Updated Successfully!";
                    }
                    else
                    {
                        response.Key = false;
                        response.IsInfo = true;
                        response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DownloadOrdersExcel(int OrderStatusId, string StartDate, string EndDate, int OrderType)
        {
            if (!Directory.Exists(Server.MapPath(UploadFilePath)))
            {
                Directory.CreateDirectory(Server.MapPath(UploadFilePath));
            }
            using (ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook())
            {
                var ws = workbook.Worksheets.Add("Orders");
                var dtStartDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dtEndDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var listOrder = db.PROC_GET_USER_ORDER_DETAILS(0, 0).Where(i => i.OrderDate >= dtStartDate &&
                                                                   i.OrderDate <= dtEndDate &&
                                                                   i.OrderStatusId == (OrderStatusId == 0 ? i.OrderStatusId : OrderStatusId) &&
                                                                   i.IsRepurchaseOrder == (OrderType == -1 ? i.IsRepurchaseOrder : (OrderType == 1)))
                                                            .OrderByDescending(i => i.UserPurchaseHdrId).ToList();
                List<string> listColName = new List<string>();
                listColName.Add("Name");
                listColName.Add("User Code");
                listColName.Add("OrderNo");
                listColName.Add("Order Date");
                listColName.Add("Product Name");
                listColName.Add("Qty");
                listColName.Add("Amount");
                listColName.Add("Total Order Amount");
                listColName.Add("Payment Id");
                listColName.Add("BV");
                listColName.Add("Address");
                listColName.Add("Order Type");
                listColName.Add("MobileNo");
                listColName.Add("Franchisee");
                listColName.Add("Order Received By");
                listColName.Add("Status");

                for (int l = 0; l < listColName.Count; l++)
                {
                    ws.Cell(1, l + 1).Value = listColName[l];
                    ws.Cell(1, l + 1).Style.Font.Bold = true;
                }
                for (int k = 0; k < listOrder.Count; k++)
                {
                    var a = listOrder[k];
                    var i = 0;
                    ws.Cell(k + 2, ++i).Value = a.CustomerName;
                    ws.Cell(k + 2, ++i).Value = a.RefferalCode;
                    ws.Cell(k + 2, ++i).Value = a.OrderNo;
                    ws.Cell(k + 2, ++i).Value = a.OdrDate;
                    ws.Cell(k + 2, ++i).Value = a.ProductName;
                    ws.Cell(k + 2, ++i).Value = a.Qty;
                    ws.Cell(k + 2, ++i).Value = a.Amount;
                    ws.Cell(k + 2, ++i).Value = a.TotalOrderAmount;
                    ws.Cell(k + 2, ++i).Value = a.RazorpayPaymentId;
                    ws.Cell(k + 2, ++i).Value = a.BusinessVolume;
                    ws.Cell(k + 2, ++i).Value = a.CustomerAddress;
                    ws.Cell(k + 2, ++i).Value = a.OrderType;
                    ws.Cell(k + 2, ++i).Value = a.CustomerMobileNo;
                    ws.Cell(k + 2, ++i).Value = a.FranchiseeName;
                    ws.Cell(k + 2, ++i).Value = a.OrderReceivedBy;
                    ws.Cell(k + 2, ++i).Value = a.StatusName;
                }

                string FileName = UploadFilePath + "/Orders_" + DateTime.Now.Ticks + ".xlsx";
                workbook.SaveAs(Server.MapPath(FileName));
                return Json(new { filename = FileName }, JsonRequestBehavior.AllowGet);
            }
        }

        internal class ProductDetails
        {
            public string VariantName { get; set; }
            public string ProductName { get; set; }
            public decimal WeightGM { get; set; }
            public int Qty { get; set; }
            public float DirectMatchingVolume { get; set; }
            public float RepurchaseMatchingVolume { get; set; }
        }
        public class OrderCourierTracking
        {
            public Int64 UserPurchaseHdrId { get; set; }
            public string CourierTrackingNo { get; set; }
            public string CourierCompany { get; set; }
        }

        [HttpPost]
        public ActionResult UploadCourierTracking(FormCollection formCollection)
        {
            ResponseMsg response = null;
            if (Request != null && Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                //HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    var usersList = new List<OrderCourierTracking>();

                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        foreach (ExcelWorksheet worksheet in package.Workbook.Worksheets)
                        {
                            var workSheet = worksheet;
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;

                            for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                OrderCourierTracking user = new OrderCourierTracking();

                                string UserPurchaseHdrId = Convert.ToString(workSheet.Cells[rowIterator, 28].Value);
                                string CourierTracking = Convert.ToString(workSheet.Cells[rowIterator, 29].Value);
                                string CourierCompany = Convert.ToString(workSheet.Cells[rowIterator, 30].Value);

                                if (!string.IsNullOrEmpty(UserPurchaseHdrId) && !string.IsNullOrEmpty(CourierTracking))
                                {
                                    user.UserPurchaseHdrId = Convert.ToInt64(UserPurchaseHdrId);
                                    user.CourierTrackingNo = CourierTracking;
                                    user.CourierCompany = CourierCompany;
                                    usersList.Add(user);
                                }
                            }
                            break;
                        }
                        response = UpdateTrackingOrderDetails(usersList);
                    }
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// UpdateTrackingOrderDetails
        /// </summary>
        /// <param name="orderCourierTrackings"></param>
        /// <returns></returns>
        public ResponseMsg UpdateTrackingOrderDetails(List<OrderCourierTracking> orderCourierTrackings)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                using (var context = new TRSEntities(3600))
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            foreach (var orderTracking in orderCourierTrackings)
                            {
                                var objUserPurchaseHdr = context.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == orderTracking.UserPurchaseHdrId).FirstOrDefault();
                                objUserPurchaseHdr.CourierTrackingNo = orderTracking.CourierTrackingNo;
                                objUserPurchaseHdr.CourierTrackingCompany = orderTracking.CourierCompany;

                                //Send SMS for Courier TrackingNo Notification...
                                bool isSMSSend = objUserPurchaseHdr.IsSMSSendforCourierTracking ?? false;
                                if (!isSMSSend)
                                {
                                    var objUser = db.UserMsts.Where(p => p.UserId == objUserPurchaseHdr.UserId).FirstOrDefault();
                                    //string Msg = string.Format("Dear {0} your order no {1} have been dispatched and your tracking number {2} for track your order at currior site {3}.Thanks and regards.Team Maxener wellness.",
                                    //               objUser.FullName, objUserPurchaseHdr.OrderNo, orderTracking.CourierTrackingNo, orderTracking.CourierCompany);

                                    string Msg = string.Format("Hey, Greetings from Maxener Wellness. Your order {0} has been dispatched your tracking no {1} you can track at {2}."
                                                            , objUserPurchaseHdr.OrderNo, orderTracking.CourierTrackingNo, orderTracking.CourierCompany);

                                    objManager.SendSMS(objUser.MobileNo, "1107160293407273758", Msg, "Courier tracking notification to user");
                                    objUserPurchaseHdr.IsSMSSendforCourierTracking = true;
                                }
                                context.SaveChanges();
                            }
                            transaction.Commit();
                            response.Key = true;
                            response.Msg = "Tracking Order Successfully!";
                        }
                        catch (Exception ex)
                        {
                            response.Key = false;
                            response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                            transaction.Rollback();
                            ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return response;
        }


        #region Order Status Update Process

        [HttpPost]
        public ActionResult CODToConfirmProcess(FormCollection formCollection)
        {
            ResponseMsg response = null;
            if (Request != null && Request.Files.Count > 0)
            {
                int OrderStatusProcessType = Convert.ToInt32(Request.Form["OrderStatusProcessType"]);//Note:  1:COD To Confirm   2:Confirm To Complete
                int OrderType = Convert.ToInt32(Request.Form["OrderType"]);//Note:  1:COD 2:Prepaid
                bool? isCodeOrder = (OrderType == 1);
                HttpPostedFileBase file = Request.Files[0];

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        List<string> listOrderNumbers = new List<string>();
                        List<long> UserPurchaseHdrIds = new List<long>();
                        foreach (ExcelWorksheet worksheet in package.Workbook.Worksheets)
                        {
                            var workSheet = worksheet;
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;

                            for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                string OrderNumber = Convert.ToString(workSheet.Cells[rowIterator, 1].Value);
                                if (!string.IsNullOrEmpty(OrderNumber))
                                    listOrderNumbers.Add(OrderNumber);
                            }
                            break;
                        }

                        var listUserPurchaseHdrIds = db.UserPurchaseHdrs.Where(i => listOrderNumbers.Contains(i.OrderNo.ToString()) && i.IsCODOrder == isCodeOrder).Select(p => p.UserPurchaseHdrId).ToList();

                        if (!listUserPurchaseHdrIds.Any())
                        {
                            response = new ResponseMsg { Key = false, Msg = "No order record found from attached excel base on selected order type." };
                        }
                        else
                            response = OrderStatusUpdateProcess(listUserPurchaseHdrIds, OrderStatusProcessType);
                    }
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// CODToConfirmDBProcess
        /// </summary>
        /// <param name="listUserPurchaseHdrIds"></param>
        /// <returns></returns>
        protected ResponseMsg OrderStatusUpdateProcess(List<long> listUserPurchaseHdrIds,int OrderStatusProcessType)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                foreach (var UserPurchaseHdrId in listUserPurchaseHdrIds)
                {
                    if (OrderStatusProcessType == 1)
                        OrderStatusUpdate(UserPurchaseHdrId, OrderStatus.OrderConfirm); //ConfirmOrderProcess(UserPurchaseHdrId);
                    else
                        OrderStatusUpdate(UserPurchaseHdrId, OrderStatus.OrderCompleted); //CompleteOrderProcess(UserPurchaseHdrId);
                }
                response.Key = true;
                response.Msg = "Order Status are updated Successfully!";
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "Something Went Wrong! Please Try Again After Sometime!";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return response;
        }

        #endregion

    }
}