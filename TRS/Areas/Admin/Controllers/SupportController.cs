﻿using Elmah;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using TRS.Models;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Admin.Controllers
{
    public class SupportController : clsBase
    {
        // GET: Admin/Support
        public ActionResult Index()
        {
            var objWeek = db.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
            ViewBag.FromDate = objWeek.StartDate.Value.ToString("dd/MM/yyyy");
            ViewBag.ToDate = objWeek.EndDate.Value.ToString("dd/MM/yyyy");
            ViewBag.listQuerySupport = db.PROC_GET_QUERY_SUPPORT(0).Where(i => i.CreatedDate >= objWeek.StartDate.Value && i.CreatedDate <= objWeek.EndDate.Value).OrderByDescending(i => i.CreatedDate).ToList();
            return View();
        }

        public JsonResult UpdateResponse(string Response, int QuerySupportId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                response.Key = true;
                if (string.IsNullOrEmpty(Response) || string.IsNullOrWhiteSpace(Response))
                {
                    response.IsInfo = true;
                    response.Key = false;
                    response.Msg = "Please Enter Response!";
                }
                if (response.Key)
                {
                    var objQuerySupportMst = db.QuerySupportMsts.Where(i => i.QuerySupportId == QuerySupportId).FirstOrDefault();
                    objQuerySupportMst.Response = Response;
                    objQuerySupportMst.ResponseDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value.ToString("dd/MM/yyyy");
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Reponse Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResolveQuery(int QuerySupportId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objQuerySupportMst = db.QuerySupportMsts.Where(i => i.QuerySupportId == QuerySupportId).FirstOrDefault();
                objQuerySupportMst.QueryStatus = clsImplementationEnum.QuerySupportStatus.Resolve.GetHashCode();
                db.SaveChanges();
                response.Key = true;
                response.Msg = "Query Resolve Successfully!";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuerySupportList(int QueryStatus, string FromDate, string ToDate)
        {
            List<PROC_GET_QUERY_SUPPORT_Result> listPROC_GET_QUERY_SUPPORT = new List<PROC_GET_QUERY_SUPPORT_Result>();
            try
            {
                var dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                var dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());

                if (QueryStatus == 0)
                {
                    listPROC_GET_QUERY_SUPPORT = db.PROC_GET_QUERY_SUPPORT(0).Where(i => i.CreatedDate >= dtFromDate && i.CreatedDate <= dtToDate).OrderByDescending(i => i.CreatedDate).ToList();
                }
                else
                {
                    listPROC_GET_QUERY_SUPPORT = db.PROC_GET_QUERY_SUPPORT(0).Where(i => i.QueryStatus == QueryStatus && i.CreatedDate >= dtFromDate && i.CreatedDate <= dtToDate).OrderByDescending(i => i.CreatedDate).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listPROC_GET_QUERY_SUPPORT, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult Config()
        {
            var listConfigMst = db.ConfigMsts.Where(i => i.IsEditable == true).OrderBy(x => x.Description).ToList();
            return View(listConfigMst);
        }

        public JsonResult UpdateConfigValue(string value, int ConfigId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objConfigMst = db.ConfigMsts.Where(i => i.ConfigId == ConfigId).FirstOrDefault();
                response.Key = true;
                if ((string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value)) && objConfigMst.Key.ToUpper() != ConfigKeys.AdvertisementValue.ToUpper())
                {
                    response.IsInfo = true;
                    response.Key = false;
                    response.Msg = "Please Enter Value!";
                }

                if (objConfigMst.Key == "TDSEffectiveDate")
                {
                    var objTDSEffectiveDate = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSEffectiveDate).FirstOrDefault();
                    DateTime OldTDSEffectiveDate = Convert.ToDateTime(objTDSEffectiveDate.Value);
                    DateTime NewTDSEffectiveDate = Convert.ToDateTime(value);
                    if (OldTDSEffectiveDate.Date != NewTDSEffectiveDate.Date)
                    {
                        if (OldTDSEffectiveDate.Date > NewTDSEffectiveDate.Date)
                        {
                            response.Key = false;
                            response.Msg = "You can not update older TDS effective date!";
                        }
                        else
                        {
                            var objTDSPercWithPAN = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithPAN).FirstOrDefault();
                            double TDSPercWithPAN = Convert.ToDouble(objTDSPercWithPAN.Value);

                            var objTDSPercWithoutPAN = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithoutPAN).FirstOrDefault();
                            double OldTDSPercWithoutPAN = Convert.ToDouble(objTDSPercWithoutPAN.Value);

                            TDSPercHistory objTDSPercHistory = new TDSPercHistory();
                            objTDSPercHistory.PercWithPAN = TDSPercWithPAN;
                            objTDSPercHistory.PercWithoutPAN = OldTDSPercWithoutPAN;
                            objTDSPercHistory.EffectiveDate = OldTDSEffectiveDate.Date;
                            db.TDSPercHistories.Add(objTDSPercHistory);
                        }
                    }
                }
                else if (objConfigMst.Key == "TDSPercWithPAN")
                {
                    var objTDSPercWithPAN = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithPAN).FirstOrDefault();
                    double OldTDSPercWithPAN = Convert.ToDouble(objTDSPercWithPAN.Value);
                    double NewTDSPercWithPAN = Convert.ToDouble(value);
                    if (OldTDSPercWithPAN != NewTDSPercWithPAN)
                    {
                        var objTDSEffectiveDate = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSEffectiveDate).FirstOrDefault();
                        DateTime TDSEffectiveDate = Convert.ToDateTime(objTDSEffectiveDate.Value);

                        var objTDSPercWithoutPAN = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithoutPAN).FirstOrDefault();
                        double OldTDSPercWithoutPAN = Convert.ToDouble(objTDSPercWithoutPAN.Value);

                        TDSPercHistory objTDSPercHistory = new TDSPercHistory();
                        objTDSPercHistory.PercWithPAN = OldTDSPercWithPAN;
                        objTDSPercHistory.PercWithoutPAN = OldTDSPercWithoutPAN;
                        objTDSPercHistory.EffectiveDate = TDSEffectiveDate.Date;
                        db.TDSPercHistories.Add(objTDSPercHistory);
                    }
                }
                else if (objConfigMst.Key == "TDSPercWithoutPAN")
                {
                    var objTDSPercWithoutPAN = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithoutPAN).FirstOrDefault();
                    double OldTDSPercWithoutPAN = Convert.ToDouble(objTDSPercWithoutPAN.Value);
                    double NewTDSPercWithoutPAN = Convert.ToDouble(value);
                    if (OldTDSPercWithoutPAN != NewTDSPercWithoutPAN)
                    {
                        var objTDSEffectiveDate = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSEffectiveDate).FirstOrDefault();
                        DateTime TDSEffectiveDate = Convert.ToDateTime(objTDSEffectiveDate.Value);

                        var objTDSPercWithPAN = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithPAN).FirstOrDefault();
                        double TDSPercWithPAN = Convert.ToDouble(objTDSPercWithPAN.Value);

                        TDSPercHistory objTDSPercHistory = new TDSPercHistory();
                        objTDSPercHistory.PercWithPAN = TDSPercWithPAN;
                        objTDSPercHistory.PercWithoutPAN = OldTDSPercWithoutPAN;
                        objTDSPercHistory.EffectiveDate = TDSEffectiveDate.Date;
                        db.TDSPercHistories.Add(objTDSPercHistory);
                    }
                }

                if (response.Key)
                {
                    if (objConfigMst.Key == "TDSEffectiveDate")
                        objConfigMst.Value = Convert.ToDateTime(value).ToString("dd/MMM/yyyy");
                    else
                        objConfigMst.Value = value;

                    objConfigMst.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Value Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetTDSHistoryDetails()
        {
            List<TDSPercHistory> lstTDSPercHistory = new List<TDSPercHistory>();
            try
            {
                lstTDSPercHistory = db.TDSPercHistories.OrderByDescending(x => x.EffectiveDate).OrderByDescending(x => x.TDSPercHistoryId).ToList();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetTDSHistoryDetails", lstTDSPercHistory);
        }


        [SessionExpireFilter]
        public ActionResult VoltIncome()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View(listMonthNames);

        }

        [SessionExpireFilter]
        public ActionResult GetMonthWeekDtl(string MonthYear)
        {
            List<WeekNames> listWeekNames = null;
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);

                listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, 0).Select(i => new WeekNames { FromDate = i.StartDate.Value, ToDate = i.EndDate.Value, WeekName = i.StartDate.Value.ToString("dd/MM/yyyy") + " to " + i.EndDate.Value.ToString("dd/MM/yyyy"), WeekNo = i.WeekNo.Value }).ToList();
            }
            catch (Exception ex)
            {
                listWeekNames = new List<WeekNames>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listWeekNames, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWeekRangeVoltDetails(string MonthYear, int WeekNo = 0, decimal CompanyWeekBudget = 0, decimal WeekBudget = 0)
        {
            List<PROC_GET_VOLT_EARN_BY_USER_Result> listVoltIncome = new List<PROC_GET_VOLT_EARN_BY_USER_Result>();
            decimal? BVMultiplier = 0;
            Int32? TotalVolts = 0;
            int IsMilesInserted = 0;

            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, WeekNo).ToList();
                var StartDate = listWeekNames.FirstOrDefault().StartDate;
                var EndDate = listWeekNames.LastOrDefault().EndDate;

                if (CompanyWeekBudget == 0)
                {
                    var IsVoltSaved = db.VoltIncomeSummaries.Where(x => x.Month == Month && x.Year == Year && x.WeekNo == WeekNo);
                    if (IsVoltSaved.Any())
                    {
                        var MonthVoltDetails = db.VoltIncomeSummaries.Where(x => x.Month == Month && x.Year == Year && x.WeekNo == WeekNo).FirstOrDefault();
                        WeekBudget = MonthVoltDetails.WeeklyBudget;
                        CompanyWeekBudget = MonthVoltDetails.CompanyWeeklyBudget;
                        IsMilesInserted = 1;
                    }
                    else
                    {
                        var objVolumeDtl = db.PROC_GET_ADMIN_VOLUME_DETAILS(StartDate, EndDate).FirstOrDefault();
                        WeekBudget = objVolumeDtl.ActualVoltVolumeInRs.HasValue ? Convert.ToDecimal(objVolumeDtl.ActualVoltVolumeInRs.Value) : 0;
                        CompanyWeekBudget = WeekBudget;
                    }
                }

                listVoltIncome = db.PROC_GET_VOLT_EARN_BY_USER(0, StartDate, EndDate).ToList();
                TotalVolts = listVoltIncome.Sum(i => i.VoltCount);

                if (TotalVolts > 0)
                    BVMultiplier = Math.Round(Convert.ToDecimal((CompanyWeekBudget / TotalVolts)), 2);
                else
                    BVMultiplier = CompanyWeekBudget;

                for (int i = 0; i < listVoltIncome.Count; i++)
                {
                    listVoltIncome[i].Miles = Math.Round(Convert.ToDecimal(listVoltIncome[i].VoltCount * BVMultiplier), 2);
                    listVoltIncome[i].BVMultiplier = BVMultiplier;
                }

            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { listVoltIncome = listVoltIncome, WeekBudget = WeekBudget, CompanyWeekBudget = CompanyWeekBudget, BVMultiplier = BVMultiplier, IsMilesInserted = IsMilesInserted }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VoltIncomeReCalculate(string MonthYear, int WeekNo, decimal WeekVoltIncome, decimal CompanyWeekVoltIncome, decimal BVMultiplier, List<UserMilesTbl> UserMilesTbl)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, WeekNo).ToList();
                var StartDate = listWeekNames.FirstOrDefault().StartDate;
                var EndDate = listWeekNames.LastOrDefault().EndDate;

                var IsVoltSaved = db.VoltIncomeSummaries.Where(x => x.Month == Month && x.Year == Year && x.WeekNo == WeekNo);
                if (IsVoltSaved.Any())
                {
                    var MonthVoltDetails = db.VoltIncomeSummaries.Where(x => x.Month == Month && x.Year == Year && x.WeekNo == WeekNo).FirstOrDefault();
                    MonthVoltDetails.CompanyWeeklyBudget = CompanyWeekVoltIncome;
                    MonthVoltDetails.WeeklyBudget = WeekVoltIncome;
                    MonthVoltDetails.BvMultiplier = BVMultiplier;
                    MonthVoltDetails.ModifiedBy = objclsLogin.UserId;
                    MonthVoltDetails.ModifiedOn = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                }
                else
                {
                    var MonthVoltDetails = new VoltIncomeSummary();
                    MonthVoltDetails.Month = Convert.ToInt16(Month);
                    MonthVoltDetails.Year = Convert.ToInt16(Year);
                    MonthVoltDetails.WeekNo = Convert.ToInt16(WeekNo);

                    MonthVoltDetails.CompanyWeeklyBudget = CompanyWeekVoltIncome;
                    MonthVoltDetails.WeeklyBudget = WeekVoltIncome;
                    MonthVoltDetails.BvMultiplier = BVMultiplier;
                    MonthVoltDetails.CreatedBy = objclsLogin.UserId;
                    MonthVoltDetails.CreatedOn = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

                    db.VoltIncomeSummaries.Add(MonthVoltDetails);

                    for (int i = 0; i < UserMilesTbl.Count; i++)
                    {
                        var objMilesCreditDtl = new MilesCreditDtl();
                        objMilesCreditDtl.CreatedBy = objclsLogin.UserId;
                        objMilesCreditDtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

                        decimal VoltMiles = Convert.ToDecimal(UserMilesTbl[i].Miles);
                        objMilesCreditDtl.CreditAmount = VoltMiles;

                        objMilesCreditDtl.CreditDate = Convert.ToDateTime(EndDate);//objMilesCreditDtl.CreatedDate;
                        objMilesCreditDtl.IsActive = true;

                        string UserRefferalCode = Convert.ToString(UserMilesTbl[i].UserCode);
                        var objUserDtl = db.UserMsts.Where(x => x.RefferalCode == UserRefferalCode).FirstOrDefault();
                        objMilesCreditDtl.UserId = objUserDtl.UserId;

                        objMilesCreditDtl.UserIdFrom = objclsLogin.UserId;
                        objMilesCreditDtl.InsertType = (int)MilesCreditType.ForVoltIncome;
                        db.MilesCreditDtls.Add(objMilesCreditDtl);

                        var IsUserMilesExists = db.UserMilesDtls.Where(x => x.UserId == objUserDtl.UserId);
                        if (IsUserMilesExists.Any())
                        {
                            var ExistsUserMiles = db.UserMilesDtls.Where(x => x.UserId == objUserDtl.UserId).FirstOrDefault();
                            ExistsUserMiles.TotalCreditMiles = ExistsUserMiles.TotalCreditMiles + Convert.ToDouble(VoltMiles);
                        }
                        else
                        {
                            UserMilesDtl tblUserMilesDtl = new UserMilesDtl();
                            tblUserMilesDtl.UserId = objUserDtl.UserId;
                            tblUserMilesDtl.TotalCreditMiles = Convert.ToDouble(VoltMiles);
                            db.UserMilesDtls.Add(tblUserMilesDtl);
                        }

                        #region Notofication
                        var MilesUser = db.UserMsts.Where(x => x.UserId == objUserDtl.UserId).FirstOrDefault();

                        var Notificationuser = VoltMiles + " miles has been credited";
                        var objNotificationUser = new NotificationMst();
                        objNotificationUser.CreatedBy = objclsLogin.UserId;
                        objNotificationUser.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objNotificationUser.IsActive = true;
                        objNotificationUser.ModifiedBy = objclsLogin.UserId;
                        objNotificationUser.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objNotificationUser.Notification = Notificationuser;
                        objNotificationUser.NotificationTypeId = NotificationType.Information.GetHashCode();
                        objNotificationUser.UserId = MilesUser.UserId;
                        db.NotificationMsts.Add(objNotificationUser);
                        #endregion Notofication
                    }
                }

                db.SaveChanges();
                response.Key = true;
                response.Msg = "Miles re-calculate successfully";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(response, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetWeekRangeVoltDetailsExport(string MonthYear, int WeekNo = 0)
        {
            List<PROC_GET_VOLT_EARN_BY_USER_Result> listVoltIncome = new List<PROC_GET_VOLT_EARN_BY_USER_Result>();
            decimal? BVMultiplier = 0, CompanyWeekBudget = 0;
            Int32? TotalVolts = 0;

            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var listWeekNames = db.PROC_GET_MONTH_WISE_WEEK_DETAILS(Year, Month, WeekNo).ToList();
                var StartDate = listWeekNames.FirstOrDefault().StartDate;
                var EndDate = listWeekNames.LastOrDefault().EndDate;

                var IsVoltSaved = db.VoltIncomeSummaries.Where(x => x.Month == Month && x.Year == Year && x.WeekNo == WeekNo);
                if (IsVoltSaved.Any())
                {
                    var MonthVoltDetails = db.VoltIncomeSummaries.Where(x => x.Month == Month && x.Year == Year && x.WeekNo == WeekNo).FirstOrDefault();
                    CompanyWeekBudget = MonthVoltDetails.CompanyWeeklyBudget;
                }
                else
                {
                    var objVolumeDtl = db.PROC_GET_ADMIN_VOLUME_DETAILS(StartDate, EndDate).FirstOrDefault();
                    CompanyWeekBudget = objVolumeDtl.ActualVoltVolumeInRs.HasValue ? Convert.ToDecimal(objVolumeDtl.ActualVoltVolumeInRs.Value) : 0;
                }


                listVoltIncome = db.PROC_GET_VOLT_EARN_BY_USER(0, StartDate, EndDate).ToList();
                TotalVolts = listVoltIncome.Sum(i => i.VoltCount);

                if (TotalVolts > 0)
                    BVMultiplier = Math.Round(Convert.ToDecimal((CompanyWeekBudget / TotalVolts)), 2);
                else
                    BVMultiplier = CompanyWeekBudget;

                for (int i = 0; i < listVoltIncome.Count; i++)
                {
                    listVoltIncome[i].Miles = Math.Round(Convert.ToDecimal(listVoltIncome[i].VoltCount * BVMultiplier), 2);
                    listVoltIncome[i].BVMultiplier = BVMultiplier;
                }

                ExcelPackage excel = Utilities.GetExcelPackageForExport(listVoltIncome, new List<string>(), "VoltIncomeSheet", false);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", string.Format("attachment;filename=UplineMonthlyIncome_{0}.xlsx", DateTime.Now.Ticks.ToString()));
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                listVoltIncome = new List<PROC_GET_VOLT_EARN_BY_USER_Result>();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public class UserMilesTbl
        {
            public string UserCode { get; set; }
            public decimal Miles { get; set; }
        }

        public JsonResult UpdateVoltIncome(decimal? value, int CompanyShareCreditDtlId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objCompanyShareCreditDtls = db.CompanyShareCreditDtls.Where(i => i.CompanyShareCreditDtlId == CompanyShareCreditDtlId).FirstOrDefault();
                response.Key = true;
                if (response.Key)
                {
                    objCompanyShareCreditDtls.CreditAmt = value.HasValue ? value.Value : 0;
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Charge Up Income Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #region Top Retailer

        [SessionExpireFilter]
        public ActionResult TopRetailer()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public PartialViewResult GetMonthWiseTopRetailerDetails(string MonthYear)
        {
            List<PROC_GET_TOP_RETAILER_DETAILS_Result> listRetailerDtl = new List<PROC_GET_TOP_RETAILER_DETAILS_Result>();
            try
            {
                var IsNewAdded = false;
                var CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                if (db.TopRetailerDtls.Any(i => i.Month == Month && i.Year == Year))
                {
                    listRetailerDtl = db.PROC_GET_TOP_RETAILER_DETAILS(Month, Year, 0).Take(10).ToList();
                    var arrayUserId = listRetailerDtl.Select(i => i.UserId).ToArray();
                    var listTopRetailer = db.TopRetailerDtls.Where(i => i.Month == Month && i.Year == Year && i.IsPaid.HasValue && i.IsPaid.Value).ToList();
                    var listTopRetailerNotPaid = db.TopRetailerDtls.Where(i => i.Month == Month && i.Year == Year && (!i.IsPaid.HasValue || (i.IsPaid.HasValue && !i.IsPaid.Value))).ToList();
                    var cnt = listTopRetailer.Count();
                    var listRemoveTopRetailer = listTopRetailerNotPaid.Where(i => !arrayUserId.Contains(i.UserId)).ToList();
                    if (listRemoveTopRetailer.Count > 0)
                    {
                        IsNewAdded = true;
                        db.TopRetailerDtls.RemoveRange(listRemoveTopRetailer);
                    }
                    listRetailerDtl.ForEach(i =>
                    {
                        if (!listTopRetailer.Any(x => x.UserId == i.UserId) && cnt <= 9)
                        {
                            if (listTopRetailerNotPaid.Any(x => x.UserId == i.UserId))
                            {
                                var objUpdate = listTopRetailerNotPaid.Where(x => x.UserId == i.UserId).FirstOrDefault();
                                objUpdate.SalesAmount = i.Amount;
                            }
                            else
                            {
                                db.TopRetailerDtls.Add(new TopRetailerDtl
                                {
                                    CreatedBy = objclsLogin.UserId,
                                    CreatedDate = CreatedDate.Value,
                                    IsActive = true,
                                    ModifiedBy = objclsLogin.UserId,
                                    ModifiedDate = CreatedDate.Value,
                                    Month = Month,
                                    Year = Year,
                                    PaidAmount = 0,
                                    SalesAmount = i.Amount,
                                    UserId = i.UserId
                                });
                                IsNewAdded = true;
                            }
                            cnt++;
                        }
                    });
                    if (IsNewAdded)
                    {
                        db.SaveChanges();
                        listRetailerDtl = db.PROC_GET_TOP_RETAILER_DETAILS(Month, Year, 0).Take(10).ToList();
                    }
                }
                else
                {
                    listRetailerDtl = db.PROC_GET_TOP_RETAILER_DETAILS(Month, Year, 0).Take(10).ToList();
                    listRetailerDtl.ForEach(i =>
                    {
                        db.TopRetailerDtls.Add(new TopRetailerDtl
                        {
                            CreatedBy = objclsLogin.UserId,
                            CreatedDate = CreatedDate.Value,
                            IsActive = true,
                            ModifiedBy = objclsLogin.UserId,
                            ModifiedDate = CreatedDate.Value,
                            Month = Month,
                            Year = Year,
                            PaidAmount = 0,
                            SalesAmount = i.Amount,
                            UserId = i.UserId
                        });
                    });
                    db.SaveChanges();
                    listRetailerDtl = db.PROC_GET_TOP_RETAILER_DETAILS(Month, Year, 0).Take(10).ToList();
                }
                var MonthStartDate = new DateTime(Year, Month, 1);
                var MonthEndDate = MonthStartDate.AddMonths(1).AddDays(-1);
                var objVolumeDtl = db.PROC_GET_ADMIN_VOLUME_DETAILS(MonthStartDate, MonthEndDate).FirstOrDefault();
                ViewBag.WeekBudget = objVolumeDtl.ActualTopRetailerVolumeInRs.HasValue ? objVolumeDtl.ActualTopRetailerVolumeInRs.Value : 0;
                ViewBag.TotalWeeklyTopRetailer = listRetailerDtl.Sum(i => i.PaidAmount);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseTopRetailerDetails", listRetailerDtl);
        }

        public JsonResult UpdateTopRetailerPaidAmount(double? value, int TopRetailerDtlId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objTopRetailerDtls = db.TopRetailerDtls.Where(i => i.TopRetailerDtlId == TopRetailerDtlId).FirstOrDefault();
                if (objTopRetailerDtls.IsPaid.HasValue && objTopRetailerDtls.IsPaid.Value)
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "You Have Already Paid Amount To Retailer So You Can Not Modify Amount!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                response.Key = true;
                if (response.Key)
                {
                    objTopRetailerDtls.PaidAmount = value.HasValue ? value.Value : 0;
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Amount Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateIsPaidToTopRetailer(double? value, int TopRetailerDtlId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objTopRetailerDtls = db.TopRetailerDtls.Where(i => i.TopRetailerDtlId == TopRetailerDtlId).FirstOrDefault();
                if (objTopRetailerDtls.IsPaid.HasValue && objTopRetailerDtls.IsPaid.Value)
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "You Have Already Paid Amount To Retailer So You Can Not Modify Amount!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                response.Key = true;
                if (response.Key)
                {
                    objTopRetailerDtls.PaidAmount = value.HasValue ? value.Value : 0;
                    objTopRetailerDtls.IsPaid = true;
                    objTopRetailerDtls.PaidDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Amount Paid Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion Top Retailer

        #region Bonanza Rewards

        [SessionExpireFilter]
        public ActionResult BonanzaReward()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public PartialViewResult GetMonthWiseBonanzaDetails(string MonthYear)
        {
            List<BonanzaQualifiersEnt> listRetailerDtl = new List<BonanzaQualifiersEnt>();
            try
            {
                List<string> listConfigs = new List<string>();
                listConfigs.Add(ConfigKeys.BonanzaUserBVPreviousMonths);
                var listConfigDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                double BonanzaUserBVPreviousMonths = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfigDtl, ConfigKeys.BonanzaUserBVPreviousMonths));

                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var CurrentDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                DateTime Date = new DateTime(Year, Month, 1);
                var MonthDuration = db.fn_GetFirstLastDateOfMonthByDate(Date).FirstOrDefault();
                if (CurrentDate > MonthDuration.EndDate)
                {
                    if (db.BonanzaRewardDtls.Any(i => i.Month == Month && i.Year == Year && i.IsPaid.HasValue && i.IsPaid.Value))
                    {
                        var listBonanza = db.BonanzaRewardDtls.Where(i => i.Month == Month && i.Year == Year).ToList();

                        listBonanza.ForEach(i =>
                        {
                            var objUser = db.UserMsts.FirstOrDefault(x => x.UserId == i.UserId);
                            listRetailerDtl.Add(new BonanzaQualifiersEnt
                            {
                                Amount = (Int64)i.PaidAmount,
                                BonanzaRewardDtlId = i.BonanzaRewardDtlId,
                                CurrentMonthBV = i.CurrentMonthBV ?? 0,
                                FullName = objUser.FullName,
                                IsPaid = i.IsPaid,
                                Month = i.Month,
                                PaidAmount = i.PaidAmount,
                                PaidDate = i.PaidDate.HasValue ? Convert.ToString("dd/MM/yyyy") : "",
                                Previous2rdMonthBV = i.Previous2ndMonthBV,
                                Previous3rdMonthBV = i.Previous3rdMonthBV,
                                PreviousMonthBV = i.PreviousMonthBV,
                                RefferalCode = objUser.RefferalCode,
                                UserId = i.UserId,
                                UTRRefNo = i.UTRRefNo,
                                Year = i.Year
                            });
                        });
                    }
                    else
                    {
                        var listBonanza = db.BonanzaRewardDtls.Where(i => i.Month == Month && i.Year == Year).ToList();

                        listRetailerDtl = objManager.GetBonanzaQualifierUsers(Month, Year);

                        foreach (var item in listRetailerDtl)
                        {
                            if (!listBonanza.Any(i => i.UserId == item.UserId))
                            {
                                BonanzaRewardDtl _objBonanzaRewardDtl = new BonanzaRewardDtl();
                                _objBonanzaRewardDtl.CreatedBy = 0;
                                _objBonanzaRewardDtl.CreatedDate = CurrentDate;
                                _objBonanzaRewardDtl.IsActive = true;
                                _objBonanzaRewardDtl.IsPaid = false;
                                _objBonanzaRewardDtl.Month = Month;
                                _objBonanzaRewardDtl.MonthBV = item.CurrentMonthBV;
                                _objBonanzaRewardDtl.PaidAmount = item.Amount;
                                _objBonanzaRewardDtl.ModifiedBy = 0;
                                _objBonanzaRewardDtl.ModifiedDate = CurrentDate;
                                ////_objBonanzaRewardDtl.PaidDate = null;
                                _objBonanzaRewardDtl.UserId = item.UserId;
                                _objBonanzaRewardDtl.UTRRefNo = string.Empty;
                                _objBonanzaRewardDtl.Year = Year;
                                _objBonanzaRewardDtl.BonanzaUserForQualification = BonanzaUserBVPreviousMonths;
                                _objBonanzaRewardDtl.PreviousMonthBV = item.PreviousMonthBV;
                                _objBonanzaRewardDtl.Previous2ndMonthBV = item.Previous2rdMonthBV;
                                _objBonanzaRewardDtl.Previous3rdMonthBV = item.Previous3rdMonthBV;
                                _objBonanzaRewardDtl.CurrentMonthBV = item.CurrentMonthBV;
                                db.BonanzaRewardDtls.Add(_objBonanzaRewardDtl);
                            }
                            else
                            {
                                var objBonanza = listBonanza.FirstOrDefault(x => x.UserId == item.UserId);

                                item.BonanzaRewardDtlId = objBonanza.BonanzaRewardDtlId;
                                item.PaidDate = objBonanza.PaidDate.HasValue ? Convert.ToString("dd/MM/yyyy") : "";
                                item.IsPaid = objBonanza.IsPaid;
                                item.UTRRefNo = objBonanza.UTRRefNo;
                                item.Month = objBonanza.Month;
                                item.Year = objBonanza.Year;

                                objBonanza.PaidAmount = item.PaidAmount;
                                objBonanza.BonanzaUserForQualification = BonanzaUserBVPreviousMonths;
                                objBonanza.PreviousMonthBV = item.PreviousMonthBV;
                                objBonanza.Previous2ndMonthBV = item.Previous2rdMonthBV;
                                objBonanza.Previous3rdMonthBV = item.Previous3rdMonthBV;
                                objBonanza.CurrentMonthBV = item.CurrentMonthBV;
                            }
                        }
                        foreach (var item in listBonanza)
                        {
                            if (!listRetailerDtl.Any(i => i.UserId == item.UserId))
                            {
                                db.BonanzaRewardDtls.Remove(item);
                            }
                        }
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseBonanzaDetails", listRetailerDtl);
        }

        public JsonResult UpdateBonanzaPaidAmount(double? value, string utrrefno, int BonanzaRewardDtlId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objBonanzaRewardDtls = db.BonanzaRewardDtls.Where(i => i.BonanzaRewardDtlId == BonanzaRewardDtlId).FirstOrDefault();
                if (objBonanzaRewardDtls.IsPaid.HasValue && objBonanzaRewardDtls.IsPaid.Value)
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "You Have Already Paid Amount To User So You Can Not Modify Amount!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                response.Key = true;
                if (response.Key)
                {
                    objBonanzaRewardDtls.PaidAmount = value.HasValue ? value.Value : 0;
                    objBonanzaRewardDtls.UTRRefNo = utrrefno;
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Amount Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateIsPaidToBonanza(double? value, int BonanzaRewardDtlId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objBonanzaRewardDtls = db.BonanzaRewardDtls.Where(i => i.BonanzaRewardDtlId == BonanzaRewardDtlId).FirstOrDefault();
                if (objBonanzaRewardDtls.IsPaid.HasValue && objBonanzaRewardDtls.IsPaid.Value)
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "You Have Already Paid Amount To User So You Can Not Modify Amount!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                response.Key = true;
                if (response.Key)
                {
                    objBonanzaRewardDtls.PaidAmount = value.HasValue ? value.Value : 0;
                    objBonanzaRewardDtls.IsPaid = true;
                    objBonanzaRewardDtls.PaidDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Amount Paid Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCalculateBonanza(string MonthYear)
        {
            int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
            int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
            var listRetailerDtl = objManager.GetBonanzaQualifierUsers(Month, Year).FirstOrDefault();
            if (listRetailerDtl != null)
            {
                return Json(new { ActualBonanzaVolume = listRetailerDtl.ActualBonanzaVolume, BVMultiplier = listRetailerDtl.BVMultiplier, AllUsersTotalMonthBV = listRetailerDtl.AllUsersTotalMonthBV });
            }
            else
            {
                return Json(new { ActualBonanzaVolume = 0, BVMultiplier = 0, AllUsersTotalMonthBV = 0 });
            }
        }

        #endregion Bonanza Rewards

        #region Company Turn Over Rewards

        [SessionExpireFilter]
        public ActionResult CTOReward()
        {
            List<MonthNames> listMonthNames = new List<MonthNames>();
            int i = 12;
            try
            {
                var serverDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                while (i >= 0)
                {
                    listMonthNames.Add(new MonthNames()
                    {
                        Month = serverDate.Value.Month,
                        Year = serverDate.Value.Year,
                        MonthYear = serverDate.Value.Month.ToString() + "#" + serverDate.Value.Year.ToString(),
                        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(serverDate.Value.Month) + "-" + serverDate.Value.Year.ToString()
                    });
                    serverDate = serverDate.Value.AddMonths(-1);
                    i--;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(listMonthNames);
        }

        [SessionExpireFilter]
        public PartialViewResult GetMonthWiseCTODetails(string MonthYear)
        {
            List<BonanzaQualifiersEnt> listRetailerDtl = new List<BonanzaQualifiersEnt>();
            try
            {
                int Month = Convert.ToInt32(MonthYear.Split('#')[0]);
                int Year = Convert.ToInt32(MonthYear.Split('#')[1]);
                var CurrentDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                DateTime Date = new DateTime(Year, Month, 1);
                var MonthDuration = db.fn_GetFirstLastDateOfMonthByDate(Date).FirstOrDefault();
                if (CurrentDate > MonthDuration.EndDate)
                {
                    if (db.CompanyTurnOverRewardDtls.Any(i => i.Month == Month && i.Year == Year))
                    {
                        listRetailerDtl = objManager.GetCompanyTurnOverQualifierUsers(Month, Year);
                    }
                    else
                    {
                        listRetailerDtl = objManager.GetCompanyTurnOverQualifierUsers(Month, Year);

                        foreach (var item in listRetailerDtl)
                        {
                            CompanyTurnOverRewardDtl _objCompanyTurnOverRewardDtl = new CompanyTurnOverRewardDtl();
                            _objCompanyTurnOverRewardDtl.CreatedBy = 0;
                            _objCompanyTurnOverRewardDtl.CreatedDate = CurrentDate;
                            _objCompanyTurnOverRewardDtl.IsActive = true;
                            _objCompanyTurnOverRewardDtl.IsPaid = false;
                            _objCompanyTurnOverRewardDtl.Month = Month;
                            _objCompanyTurnOverRewardDtl.MonthBV = item.CurrentMonthBV;
                            _objCompanyTurnOverRewardDtl.PaidAmount = item.Amount;
                            _objCompanyTurnOverRewardDtl.ModifiedBy = 0;
                            _objCompanyTurnOverRewardDtl.ModifiedDate = CurrentDate;
                            ////_objCompanyTurnOverRewardDtl.PaidDate = null;
                            _objCompanyTurnOverRewardDtl.UserId = item.UserId;
                            _objCompanyTurnOverRewardDtl.UTRRefNo = string.Empty;
                            _objCompanyTurnOverRewardDtl.Year = Year;
                            db.CompanyTurnOverRewardDtls.Add(_objCompanyTurnOverRewardDtl);
                        }
                        db.SaveChanges();
                    }

                    var listBonanza = db.CompanyTurnOverRewardDtls.Where(i => i.Month == Month && i.Year == Year).ToList();
                    listRetailerDtl.ForEach(i =>
                    {
                        if (listBonanza.Any(x => x.UserId == i.UserId))
                        {
                            var obj = listBonanza.FirstOrDefault(x => x.UserId == i.UserId);
                            i.UTRRefNo = obj.UTRRefNo;
                            i.PaidAmount = obj.PaidAmount;
                            i.PaidDate = obj.PaidDate.HasValue ? obj.PaidDate.Value.ToString("dd-MM-yyyy") : string.Empty;
                            i.IsPaid = obj.IsPaid;
                            i.CompanyTurnOverRewardDtlId = obj.CompanyTurnOverRewardDtlId;
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetMonthWiseCTODetails", listRetailerDtl);
        }

        public JsonResult UpdateCTOPaidAmount(double? value, string utrrefno, int CompanyTurnOverRewardDtlId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objCompanyTurnOverRewardDtls = db.CompanyTurnOverRewardDtls.Where(i => i.CompanyTurnOverRewardDtlId == CompanyTurnOverRewardDtlId).FirstOrDefault();
                if (objCompanyTurnOverRewardDtls.IsPaid.HasValue && objCompanyTurnOverRewardDtls.IsPaid.Value)
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "You Have Already Paid Amount To User So You Can Not Modify Amount!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                response.Key = true;
                if (response.Key)
                {
                    objCompanyTurnOverRewardDtls.PaidAmount = value.HasValue ? value.Value : 0;
                    objCompanyTurnOverRewardDtls.UTRRefNo = utrrefno;
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Amount Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateIsPaidToCTO(double? value, int CompanyTurnOverRewardDtlId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objCompanyTurnOverRewardDtls = db.CompanyTurnOverRewardDtls.Where(i => i.CompanyTurnOverRewardDtlId == CompanyTurnOverRewardDtlId).FirstOrDefault();
                if (objCompanyTurnOverRewardDtls.IsPaid.HasValue && objCompanyTurnOverRewardDtls.IsPaid.Value)
                {
                    response.Key = false;
                    response.IsInfo = true;
                    response.Msg = "You Have Already Paid Amount To User So You Can Not Modify Amount!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                response.Key = true;
                if (response.Key)
                {
                    objCompanyTurnOverRewardDtls.PaidAmount = value.HasValue ? value.Value : 0;
                    objCompanyTurnOverRewardDtls.IsPaid = true;
                    objCompanyTurnOverRewardDtls.PaidDate = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                    db.SaveChanges();
                    response.Key = true;
                    response.Msg = "Amount Paid Successfully!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.Key = false;
                response.IsInfo = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion Company Turn Over Rewards
    }
}