﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using TRS.Models;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Admin.Controllers
{
    public class UserController : clsBase
    {
        // GET: Admin/User
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult Index()
        {
            var CurrentDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
            var CurrentYear = CurrentDate.Year;
            ViewBag.CurrentYear = CurrentDate.Year;
            ViewBag.CurrentMonth = CurrentDate.Month;
            List<SelectListItem> listYears = new List<SelectListItem>();
            while (CurrentYear >= 2019)
            {
                listYears.Add(new SelectListItem { Text = CurrentYear.ToString(), Value = CurrentYear.ToString() });
                CurrentYear--;
            }
            ViewBag.listYears = listYears;
            //var listUserMst = db.UserMsts.Where(i => i.RefferalUserId != 0).OrderByDescending(i => i.RegistrationDate).ToList();
            //return View(listUserMst);
            ViewBag.IsBVEnable = "false";
            return View();
        }

        // GET: Admin/User/UserBV
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult UserBV()
        {
            ViewBag.IsBVEnable = "true";
            return View("Index");
        }

        // GET: Admin/User/KYC
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult KYC()
        {
            return View();
        }

        [SessionExpireFilter]
        public JsonResult UserLoginBlock(int UserId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                if (db.UserMsts.Any(i => i.UserId == UserId))
                {
                    var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                    if (!objUserMst.IsBlocked.HasValue)
                    {
                        objUserMst.IsBlocked = true;
                    }
                    else if (objUserMst.IsBlocked.Value)
                    {
                        objUserMst.IsBlocked = false;
                    }
                    else if (!objUserMst.IsBlocked.Value)
                    {
                        objUserMst.IsBlocked = true;
                    }
                    db.SaveChanges();
                    response.Msg = "User updated successfully!";
                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "User does not exist!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult UserActiveInActiveRegistration(int UserId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                if (db.UserMsts.Any(i => i.UserId == UserId))
                {
                    var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                    if (!objUserMst.IsRegistrationActivated.HasValue)
                    {
                        objUserMst.IsRegistrationActivated = true;
                    }
                    else if (objUserMst.IsRegistrationActivated.Value)
                    {
                        objUserMst.IsRegistrationActivated = false;
                    }
                    else if (!objUserMst.IsRegistrationActivated.Value)
                    {
                        objUserMst.IsRegistrationActivated = true;
                    }
                    db.SaveChanges();
                    response.Msg = "User updated successfully!";
                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "User does not exist!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult InActiveUsers(int UserId)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                if (db.UserMsts.Any(i => i.UserId == UserId))
                {
                    var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                    if (objUserMst.IsRegistrationActivated.HasValue && objUserMst.IsRegistrationActivated.Value)
                    {
                        response.Msg = "You can not inactive user because it's successfully registered using purchase order!";
                        response.Key = false;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        db.UserMsts.Remove(objUserMst);
                    }
                    db.SaveChanges();
                    response.Msg = "User inactive successfully!";
                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "User does not exist!";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetKYCDetails()
        {
            //var kycPartialView = RenderRazorViewToString(this.ControllerContext, "_" + partialView);
            var kycPartialView1 = RenderRazorViewToString(this.ControllerContext, "_KYCApplied");
            var kycPartialView2 = RenderRazorViewToString(this.ControllerContext, "_KYCConfirmed");
            var kycPartialView3 = RenderRazorViewToString(this.ControllerContext, "_KYCRejected");

            return Json(new { kycPartialView1, kycPartialView2, kycPartialView3 });
        }

        public PartialViewResult GetUserDetails(int Month, int Year, string UserCode, string Type = "all")
        {
            return PartialView("_GetUserDetails");
        }

        public ActionResult GetKYCDetailsByPaging(int PageIndex, int PageSize, string KYCType, string FirstName = null, string MiddleName = null, string LastName = null, string UserId = null, string PANNo = null, string BankName = null, string BankAccNo = null, string BankIFSC = null, string BankingName = null, string MobileNo = null, string RefferalCode = null)
        {
            List<PROC_GET_ALL_USER_DETAILS_Result> listUserMst = new List<PROC_GET_ALL_USER_DETAILS_Result>();
            List<UserListEnt> listUserListEnt = null;

            try
            {
                object[] xparams = {
                new SqlParameter("@Page", PageIndex),
                new SqlParameter("@Size", PageSize),
                new SqlParameter("@KYCType", KYCType),
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@FirstName", FirstName),
                new SqlParameter("@MiddleName", MiddleName),
                new SqlParameter("@LastName", LastName),
                new SqlParameter("@PanNo", PANNo),
                new SqlParameter("@BankName", BankName),
                new SqlParameter("@BankAcc", BankAccNo),
                new SqlParameter("@BankIFSC", BankIFSC),
                new SqlParameter("@BankingName", BankingName),
                new SqlParameter("@MobileNo", MobileNo),
                new SqlParameter("@RefferalCode", RefferalCode),
                new SqlParameter("@TotalCount", System.Data.SqlDbType.BigInt) { Direction = System.Data.ParameterDirection.Output }
            };

                listUserMst = db.Database.SqlQuery<PROC_GET_ALL_USER_DETAILS_Result>("PROC_GET_KYC_DETAILS @Page, @Size, @KYCType, @UserId, @FirstName, @MiddleName, @LastName, @PanNo, @BankName, @BankAcc, @BankIFSC, @BankingName, @MobileNo, @RefferalCode, @TotalCount OUTPUT", xparams).ToList();
                ViewBag.KYCTotalRecords = ((System.Data.SqlClient.SqlParameter)xparams[14]).Value;

                listUserListEnt = listUserMst.Select(i => new UserListEnt
                {
                    FirstName = i.FirstName,
                    MiddleName = i.MiddleName,
                    LastName = i.LastName,
                    PanNo = i.PANNo,
                    BankName = i.BankName,
                    BankIFSC = i.ISFCCode,
                    BankAccNo = i.BankAccNo,
                    BankUserName = i.BankingName,
                    //FullName=i.FullName,
                    IsBlocked = i.IsBlocked.HasValue && i.IsBlocked.Value,
                    IsRegistrationActivated = i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value,
                    MobileNo = i.MobileNo.ToString(),
                    Password = i.Password,
                    RefferalCode = i.RefferalCode,
                    RegistrationDate = i.RegistrationDate.Value.ToString("dd/MM/yyyy"),
                    UserId = i.UserId,
                    UserName = i.UserName,
                    RefferalByCode = i.RefferalByCode + " - " + i.RefferalByName,
                    ActivateDate = i.ActivationDate,
                    IsKYCVerified = i.IsKYCVerified ?? false,
                    IsBankDocumentVerified = i.IsBankDocumentVerified ?? false,
                    IsPANVerified = i.IsPANVerified ?? false,
                    PANImage = !string.IsNullOrWhiteSpace(i.PANImage) ? Utilities.GetMaxenerwellnessUrl() + $"/Documents/{i.RefferalCode}/" + i.PANImage + "?id=" + objManager.GetTimespam() : string.Empty,
                    BankDocument = !string.IsNullOrWhiteSpace(i.BankDocument) ? Utilities.GetMaxenerwellnessUrl() + $"/Documents/{i.RefferalCode}/" + i.BankDocument + "?id=" + objManager.GetTimespam() : string.Empty,
                    KYCImage = !string.IsNullOrWhiteSpace(i.KYCImage) ? Utilities.GetMaxenerwellnessUrl() + $"/Documents/{i.RefferalCode}/" + i.KYCImage + "?id=" + objManager.GetTimespam() : string.Empty,
                }).ToList();
                ViewBag.kycList = listUserListEnt;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { Data = listUserListEnt, Count = ViewBag.KYCTotalRecords });
        }

        public ActionResult GetUserDetailsByPaging(int? Month, int? Year, string UserCode, int PageIndex, int PageSize, string ActivateDate = null, string MobileNo = null, string Password = null, string RefferalByCode = null, string RefferalCode = null, string RegistrationDate = null, string UserName = null, string Type = "all")
        {
            List<PROC_GET_ALL_USER_DETAILS_Result> listUserMst = new List<PROC_GET_ALL_USER_DETAILS_Result>();
            List<UserListEnt> listUserListEnt = null;

            try
            {
                object[] xparams = {
                    new SqlParameter("@Page", PageIndex),
                    new SqlParameter("@Size", PageSize),
                    new SqlParameter("@UserId", 1),
                    new SqlParameter("@Type", Type),
                    new SqlParameter("@Year", Year <= 0 ? (object)DBNull.Value : Year),
                    new SqlParameter("@Month", Month <= 0 || Year <= 0 ? (object)DBNull.Value : Month),
                    new SqlParameter("@ActivateDate", ActivateDate),
                    new SqlParameter("@MobileNo", MobileNo),
                    new SqlParameter("@Password", Password),
                    new SqlParameter("@RefferalByCode", RefferalByCode),
                    new SqlParameter("@RefferalCode", RefferalCode),
                    new SqlParameter("@RegistrationDate", RegistrationDate),
                    new SqlParameter("@UserName", UserName),
                    new SqlParameter("@UserCodeOrMobile", UserCode),
                    new SqlParameter("@TotalCount", System.Data.SqlDbType.BigInt) { Direction = System.Data.ParameterDirection.Output }
                };

                //listUserMst = db.Database.SqlQuery<PROC_GET_ALL_USER_DETAILS_Result>("PROC_GET_ALL_USER_DETAILS_V1 @UserId, @PageIndex, @RowCount, @Type, @Year, @Month, @ActivateDate, @MobileNo, @Password, @RefferalByCode, @RefferalCode, @RegistrationDate, @UserName, @UserCodeOrMobile, @TotalCount OUTPUT", xparams).ToList();
                listUserMst = db.Database.SqlQuery<PROC_GET_ALL_USER_DETAILS_Result>("PROC_GET_ALL_USER_DETAILS_V1 @Page, @Size, @UserId, @Type, @Year, @Month, @ActivateDate, @MobileNo, @Password, @RefferalByCode, @RefferalCode, @RegistrationDate, @UserName, @UserCodeOrMobile, @TotalCount OUTPUT", xparams).ToList();
                ViewBag.TotalRecords = ((System.Data.SqlClient.SqlParameter)xparams[14]).Value;

                listUserListEnt = listUserMst.Select(i => new UserListEnt
                {
                    //FullName=i.FullName,
                    IsBlocked = i.IsBlocked.HasValue && i.IsBlocked.Value,
                    IsRegistrationActivated = i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value,
                    MobileNo = i.MobileNo.ToString(),
                    Password = i.Password,
                    RefferalCode = i.RefferalCode + " - " + i.FullName,
                    RegistrationDate = i.RegistrationDate.Value.ToString("dd/MM/yyyy"),
                    UserId = i.UserId,
                    UserName = i.UserName,
                    RefferalByCode = i.RefferalByCode + " - " + i.RefferalByName,
                    ActivateDate = i.ActivationDate,
                    IsKYCVerified = i.IsKYCVerified ?? false,
                    IsBankDocumentVerified = i.IsBankDocumentVerified ?? false,
                    IsPANVerified = i.IsPANVerified ?? false,
                    PANImage = !string.IsNullOrWhiteSpace(i.PANImage) ? Utilities.GetMaxenerwellnessUrl() + $"/Documents/{i.RefferalCode}/" + i.PANImage + "?id=" + objManager.GetTimespam() : string.Empty,
                    BankDocument = !string.IsNullOrWhiteSpace(i.BankDocument) ? Utilities.GetMaxenerwellnessUrl() + $"/Documents/{i.RefferalCode}/" + i.BankDocument + "?id=" + objManager.GetTimespam() : string.Empty,
                    KYCImage = !string.IsNullOrWhiteSpace(i.KYCImage) ? Utilities.GetMaxenerwellnessUrl() + $"/Documents/{i.RefferalCode}/" + i.KYCImage + "?id=" + objManager.GetTimespam() : string.Empty,
                }).ToList();
                ViewBag.listUserListEnt = listUserListEnt;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { Data = listUserListEnt, Count = ViewBag.TotalRecords });
        }

        public PartialViewResult GetUserDetailsBV(int Month, int Year, string UserCode, string Type = "all")
        {
            List<PROC_GET_ALL_USER_DETAILS_Result> listUserMst = new List<PROC_GET_ALL_USER_DETAILS_Result>();
            List<UserListEnt> listUserListEnt = new List<UserListEnt>();
            try
            {
                if (string.IsNullOrEmpty(UserCode) || string.IsNullOrWhiteSpace(UserCode))
                {
                    if (Type.ToLower() == "Inactive".ToLower())
                    {
                        listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => i.RefferalUserId != 0 && (!i.IsRegistrationActivated.HasValue || !i.IsRegistrationActivated.Value) && (i.RegMonth == Month && i.RegYear == Year)).OrderByDescending(i => i.RegistrationDate).ToList();
                    }
                    else if (Type.ToLower() == "active".ToLower())
                    {
                        listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => i.RefferalUserId != 0 && (i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value) && (i.RegMonth == Month && i.RegYear == Year)).OrderByDescending(i => i.RegistrationDate).ToList();
                    }
                    else if (Type.ToLower() == "all".ToLower())
                    {
                        listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => i.RefferalUserId != 0 && (i.RegMonth == Month && i.RegYear == Year)).OrderByDescending(i => i.RegistrationDate).ToList();
                    }
                    else if (Type.ToLower() == "gold".ToLower())
                    {
                        listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => i.RefferalUserId != 0 && i.IsGoldMember == true && (i.RegMonth == Month && i.RegYear == Year)).OrderByDescending(i => i.RegistrationDate).ToList();
                    }
                    else if (Type.ToLower() == "prime".ToLower())
                    {
                        listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => i.RefferalUserId != 0 && i.IsPrimeMember == true && (i.RegMonth == Month && i.RegYear == Year)).OrderByDescending(i => i.RegistrationDate).ToList();
                    }
                    else if (Type.ToLower() == "silver".ToLower())
                    {
                        listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => i.RefferalUserId != 0 && i.IsSilverMember == true && (i.RegMonth == Month && i.RegYear == Year)).OrderByDescending(i => i.RegistrationDate).ToList();
                    }
                    else if (Type.ToLower() == "diamond".ToLower())
                    {
                        listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => i.RefferalUserId != 0 && i.IsDimondMember == true && (i.RegMonth == Month && i.RegYear == Year)).OrderByDescending(i => i.RegistrationDate).ToList();
                    }
                    else if (Type.ToLower() == "last30".ToLower())
                    {
                        var CurrentDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        var Last30Days = CurrentDate.AddDays(-30);
                        listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => i.RefferalUserId != 0 && i.RegistrationDate >= Last30Days && (!i.IsRegistrationActivated.HasValue || !i.IsRegistrationActivated.Value)).OrderByDescending(i => i.RegistrationDate).ToList();
                    }
                }
                else
                {
                    listUserMst = db.PROC_GET_ALL_USER_DETAILS(0).Where(i => (i.RefferalUserId != 0 && i.RefferalCode.Trim() == UserCode.Trim()) || (i.MobileNo.HasValue && i.MobileNo.Value.ToString() == UserCode.Trim()) || (i.AlternativeMobileNo.HasValue && i.AlternativeMobileNo.Value.ToString() == UserCode.Trim())).OrderByDescending(i => i.RegistrationDate).ToList();
                }
                listUserListEnt = listUserMst.Select(i => new UserListEnt
                {
                    //FullName=i.FullName,
                    IsBlocked = i.IsBlocked.HasValue && i.IsBlocked.Value,
                    IsRegistrationActivated = i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value,
                    MobileNo = i.MobileNo.ToString(),
                    Password = i.Password,
                    RefferalCode = i.RefferalCode + " - " + i.FullName,
                    RegistrationDate = i.RegistrationDate.Value.ToString("dd/MM/yyyy"),
                    UserId = i.UserId,
                    UserName = i.UserName,
                    RefferalByCode = i.RefferalByCode + " - " + i.RefferalByName,
                    ActivateDate = i.ActivationDate,
                    //LeftBV= Convert.ToDouble((Int64)objManager.GetLeftBV(i.UserId)),
                    //RightBV=Convert.ToDouble((Int64)objManager.GetRightBV(i.UserId))
                }).ToList();
                ViewBag.listUserListEnt = listUserListEnt;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetUserDetailsBV", listUserMst);
        }

        public string GetCalculatedBV(int UserId, string Position)
        {
            if (Position == "L")
            {
                return Convert.ToDouble((Int64)objManager.GetLeftBV(UserId)).ToString();
            }
            else
            {
                return Convert.ToDouble((Int64)objManager.GetRightBV(UserId)).ToString();
            }
        }

        public string GetTotalIncome(int UserId)
        {
            return db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(UserId, true).FirstOrDefault().TotalIncome.ToString();
        }

        public PartialViewResult GetUserIncomeDetails(int UserId)
        {
            PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_Result obj = null;
            try
            {
                obj = db.PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS(UserId, true).FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = new PROC_RPT_ADMIN_PENDING_PAYOUT_DETAILS_Result();
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_GetUserIncomeDetails", obj);
        }

        [SessionExpireFilter]
        public JsonResult ApproveKYC(int userId, bool isApproved, string comment)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objUserMst = db.UserMsts.Where(i => i.UserId == userId).FirstOrDefault();
                objUserMst.IsKYCVerified = isApproved;
                objUserMst.KYCRejectComment = isApproved ? null : comment;
                db.SaveChanges();
                response.Key = isApproved;
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult ApprovePAN(int userId, bool isApproved, string comment)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objUserMst = db.UserMsts.Where(i => i.UserId == userId).FirstOrDefault();
                objUserMst.IsPANVerified = isApproved;
                objUserMst.PANRejectComment = isApproved ? null : comment;
                db.SaveChanges();
                response.Key = isApproved;
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult ApproveBankDoc(int userId, bool isApproved, string comment)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var objUserMst = db.UserMsts.Where(i => i.UserId == userId).FirstOrDefault();
                objUserMst.IsBankDocumentVerified = isApproved;
                objUserMst.BankDocumentRejectComment = isApproved ? null : comment;
                db.SaveChanges();
                response.Key = isApproved;
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult UpdateUserDetail(UserProfileEnt user)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                if (db.UserMsts.Any(x => x.UserId == user.UserId))
                {
                    var objUserMst = db.UserMsts.Where(i => i.UserId == user.UserId).First();
                    objUserMst.AdharNo = !string.IsNullOrEmpty(user.AdharNo) ? user.AdharNo.Trim() : objUserMst.AdharNo;
                    objUserMst.BankAccNo = !string.IsNullOrEmpty(user.BankAccNo) ? user.BankAccNo.Trim() : objUserMst.BankAccNo;
                    objUserMst.BankName = !string.IsNullOrEmpty(user.BankName) ? user.BankName.Trim() : objUserMst.BankName;
                    objUserMst.BranchName = !string.IsNullOrEmpty(user.BranchName) ? user.BranchName.Trim() : objUserMst.BranchName;
                    objUserMst.FirstName = !string.IsNullOrEmpty(user.FirstName) ? user.FirstName.Trim() : objUserMst.FirstName;
                    objUserMst.ISFCCode = !string.IsNullOrEmpty(user.ISFCCode) ? user.ISFCCode.Trim() : objUserMst.ISFCCode;
                    objUserMst.BankingName = !string.IsNullOrEmpty(user.BankingName) ? user.BankingName.Trim() : objUserMst.BankingName;
                    objUserMst.LastName = !string.IsNullOrEmpty(user.LastName) ? user.LastName.Trim() : objUserMst.LastName;
                    objUserMst.MiddleName = !string.IsNullOrEmpty(user.MiddleName) ? user.MiddleName.Trim() : objUserMst.MiddleName;
                    objUserMst.PANNo = !string.IsNullOrEmpty(user.PANNo) ? user.PANNo.Trim() : objUserMst.PANNo;
                    objUserMst.FullName = objUserMst.FirstName + " " + objUserMst.MiddleName + " " + objUserMst.LastName;
                    db.SaveChanges();
                    response.Key = true;
                }
                else
                {
                    response.Key = false;
                    response.Msg = "User Not Found";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                response.Msg = ex.Message;
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private bool RemoteFileExistsUsingHTTP(string url)
        {
            Utilities.WriteLog(string.Format("KYCImage url:{0}", url));
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TURE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }


        [SessionExpireFilter]
        public JsonResult GetDocuments(int userId)
        {
            PROC_GET_USER_PROFILE_DETAILS_Result objProfile = new PROC_GET_USER_PROFILE_DETAILS_Result();
            try
            {
                objProfile = db.PROC_GET_USER_PROFILE_DETAILS(userId).FirstOrDefault();

                if (!string.IsNullOrEmpty(objProfile.KYCImage))
                {
                    if (RemoteFileExistsUsingHTTP(Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objProfile.RefferalCode}/" + objProfile.KYCImage))
                    {
                        objProfile.KYCImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objProfile.RefferalCode}/" + objProfile.KYCImage + "?id=" + objManager.GetTimespam();
                    }
                    else if (RemoteFileExistsUsingHTTP(Utilities.GetMaxenerwellnessUrl() + $"/Images/KYCImages/" + objProfile.KYCImage))
                    {
                        objProfile.KYCImage = Utilities.GetMaxenerwellnessUrl() + $"/Images/KYCImages/" + objProfile.KYCImage + "?id=" + objManager.GetTimespam();
                    }
                    else
                        objProfile.KYCImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/no-image-found.jpg?id=" + objManager.GetTimespam();
                }
                else
                    objProfile.KYCImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/no-image-found.jpg?id=" + objManager.GetTimespam();

                if (!string.IsNullOrEmpty(objProfile.KYCBackImage))
                    objProfile.KYCBackImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objProfile.RefferalCode}/" + objProfile.KYCBackImage + "?id=" + objManager.GetTimespam();
                else
                    objProfile.KYCBackImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/no-image-found.jpg?id=" + objManager.GetTimespam();

                if (!string.IsNullOrEmpty(objProfile.PANImage))
                    objProfile.PANImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objProfile.RefferalCode}/" + objProfile.PANImage + "?id=" + objManager.GetTimespam();
                else
                    objProfile.PANImage = Utilities.GetMaxenerwellnessUrl() + $"/Documents/no-image-found.jpg?id=" + objManager.GetTimespam();

                if (!string.IsNullOrEmpty(objProfile.BankDocument))
                    objProfile.BankDocument = Utilities.GetMaxenerwellnessUrl() + $"/Documents/{objProfile.RefferalCode}/" + objProfile.BankDocument + "?id=" + objManager.GetTimespam();
                else
                    objProfile.BankDocument = Utilities.GetMaxenerwellnessUrl() + $"/Documents/no-image-found.jpg?id=" + objManager.GetTimespam();

                objProfile.RefferalCode = objProfile.RefferalCode + " - " + objProfile.FullName;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(objProfile, JsonRequestBehavior.AllowGet);
        }

        #region "User Registration"

        public ActionResult Signup(string p, string rc)
        {
            var TempUserMst = new TempUserMst();
            try
            {
                ViewBag.p = p;
                ViewBag.RefferalCode = rc;
                //var objUserMst = db.UserMsts.Where(i => i.RefferalCode == rc && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).FirstOrDefault();
                var objUserMst = db.UserMsts.Where(i => i.RefferalCode == rc).FirstOrDefault();
                if (string.IsNullOrEmpty(rc) || objUserMst == null)
                {
                    return RedirectToAction("Index", "Authenticate");
                }
                if (!string.IsNullOrEmpty(p))
                {
                    ViewBag.DefaultPosition = p.ToUpper().Trim();
                    ViewBag.IsPositionEnabled = false;
                }
                else
                {
                    ViewBag.DefaultPosition = !string.IsNullOrEmpty(objUserMst.Position) ? objUserMst.Position : "L";
                    ViewBag.IsPositionEnabled = true;
                }
                ViewBag.Cities = db.CityMsts.OrderBy(i => i.CityName).Select(i => new AutoCompleteEnt { key = i.CityId.ToString(), val = i.CityName.Replace(" ", "_").Replace("#", "_xxx_") }).ToList();
            }
            catch (Exception ex)
            {
                ViewBag.DefaultPosition = "L";
                ViewBag.IsPositionEnabled = true;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            //ViewBag.IsPositionEnabled = true;
            return View(TempUserMst);
        }

        // GET: Authentication/Authenticate/Plans
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        //public ActionResult Plans(int id, string refcode)
        public ActionResult Plans(string rc)
        {
            var objUserMst = db.UserMsts.Where(i => i.RefferalCode == rc).FirstOrDefault();
            if (string.IsNullOrEmpty(rc) || objUserMst == null)
            {
                return RedirectToAction("Index", "Authenticate");
            }
            ViewBag.rc = rc;
            return View();
        }

        public ActionResult RegisterUser(TempRegistration user)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var TempUserMst = new UserMst();
                decimal CityId = 0;
                if (user.TempUserId == 0)
                {
                    if (!objManager.CheckUserNameExist(user.UserName) && !objManager.CheckEmailIdExist(user.EmailId))
                    {
                        List<string> listConfigs = new List<string>();
                        listConfigs.Add(ConfigKeys.RefernceMilesAmount);
                        var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                        double RefernceMilesAmount = 0;
                        if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.RefernceMilesAmount))
                        {
                            try
                            {
                                RefernceMilesAmount = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.RefernceMilesAmount).Value);
                            }
                            catch (Exception ex)
                            {
                                ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }

                        //var objRefferalUserMst = db.UserMsts.Where(i => i.RefferalCode == user.RefferalCode && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).FirstOrDefault();
                        var objRefferalUserMst = db.UserMsts.Where(i => i.RefferalCode == user.RefferalCode).FirstOrDefault();
                        if (string.IsNullOrEmpty(user.RefferalCode) || objRefferalUserMst == null)
                        {
                            response.Key = false;
                            response.Msg = "InValid Refferal Code!";
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }

                        if (!string.IsNullOrEmpty(user.CityName))
                        {
                            var objCity = db.CityMsts.Where(i => i.CityName.Trim() == user.CityName.Replace("_xxx_", "#").Replace("_", " ")).FirstOrDefault();
                            if (objCity != null) { CityId = objCity.CityId; }
                        }

                        TempUserMst.FirstName = !string.IsNullOrEmpty(user.FirstName) ? user.FirstName.Trim() : null;
                        TempUserMst.LastName = !string.IsNullOrEmpty(user.LastName) ? user.LastName.Trim() : null;
                        TempUserMst.UserName = !string.IsNullOrEmpty(user.UserName) ? user.UserName.Trim() : null;
                        TempUserMst.Password = !string.IsNullOrEmpty(user.Password) ? user.Password.Trim() : null;
                        TempUserMst.EmailId = !string.IsNullOrEmpty(user.EmailId) ? user.EmailId.Trim() : null;
                        TempUserMst.MobileNo = user.MobileNo;
                        TempUserMst.FullName = (!string.IsNullOrEmpty(user.FirstName) ? user.FirstName.Trim() : null) + " " + (!string.IsNullOrEmpty(user.LastName) ? user.LastName.Trim() : null);
                        TempUserMst.CityId = CityId;
                        TempUserMst.Position = user.Position;
                        //TempUserMst.RefferalCode = objManager.GenerateRefferalCode();

                        //bool CheckRefferalCodeduplicate = db.UserMsts.Any(i => i.RefferalCode == TempUserMst.RefferalCode);
                        //while (CheckRefferalCodeduplicate)
                        //{
                        //    TempUserMst.RefferalCode = objManager.GenerateRefferalCode();
                        //    if (!db.UserMsts.Any(i => i.RefferalCode == TempUserMst.RefferalCode))
                        //        CheckRefferalCodeduplicate = false;
                        //}

                        TempUserMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        TempUserMst.RegMonth = TempUserMst.CreatedDate.Value.Month;
                        TempUserMst.RegYear = TempUserMst.CreatedDate.Value.Year;
                        TempUserMst.IsActive = true;
                        TempUserMst.RegistrationDate = TempUserMst.CreatedDate;
                        TempUserMst.UserType = UserType.Member.GetHashCode();

                        if (!string.IsNullOrEmpty(user.RefferalCode))
                        {
                            if (db.UserMsts.Any(i => i.RefferalCode == user.RefferalCode))
                            {
                                TempUserMst.RefferalUserId = db.UserMsts.Where(i => i.RefferalCode == user.RefferalCode).FirstOrDefault().UserId;
                            }
                            else
                            {
                                response.Key = false;
                                response.Msg = "InValid Refferal Code!";
                                return Json(response, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            TempUserMst.RefferalUserId = db.UserMsts.Where(i => i.RefferalUserId == 0).FirstOrDefault().UserId;
                        }

                        db.UserMsts.Add(TempUserMst);
                        db.SaveChanges();

                        #region Miles Credit Details

                        if (RefernceMilesAmount > 0)
                        {
                            var objMilesCreditDtl = new MilesCreditDtl();
                            objMilesCreditDtl.CreatedBy = TempUserMst.UserId;
                            objMilesCreditDtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objMilesCreditDtl.CreditAmount = (decimal)RefernceMilesAmount;
                            objMilesCreditDtl.CreditDate = objMilesCreditDtl.CreatedDate;
                            objMilesCreditDtl.IsActive = true;
                            objMilesCreditDtl.UserId = TempUserMst.RefferalUserId;
                            objMilesCreditDtl.UserIdFrom = TempUserMst.UserId;
                            objMilesCreditDtl.InsertType = (int)MilesCreditType.ForRegisterUser;
                            db.MilesCreditDtls.Add(objMilesCreditDtl);

                            var IsUserMilesExists = db.UserMilesDtls.Where(x => x.UserId == TempUserMst.RefferalUserId);
                            if (IsUserMilesExists.Any())
                            {
                                var ExistsUserMiles = db.UserMilesDtls.Where(x => x.UserId == TempUserMst.RefferalUserId).FirstOrDefault();
                                ExistsUserMiles.TotalCreditMiles = ExistsUserMiles.TotalCreditMiles + RefernceMilesAmount;
                            }
                            else
                            {
                                UserMilesDtl tblUserMilesDtl = new UserMilesDtl();
                                tblUserMilesDtl.UserId = TempUserMst.RefferalUserId;
                                tblUserMilesDtl.TotalCreditMiles = RefernceMilesAmount;
                                db.UserMilesDtls.Add(tblUserMilesDtl);
                            }

                            db.SaveChanges();
                        }

                        #endregion Miles Credit Details

                        var UserPosition = "";
                        var objRefferalUser = db.UserMsts.Where(i => i.UserId == TempUserMst.RefferalUserId).FirstOrDefault();
                        if (user.Position == "L")
                        {
                            UserPosition = "Left";
                            if (!objRefferalUser.LeftUserId.HasValue)
                            {
                                objRefferalUser.LeftUserId = TempUserMst.UserId;
                            }
                            else
                            {
                                var LastLeftUser = db.PROC_GET_ALL_LEFT_USER_TREE(objRefferalUser.UserId, false).Where(i => i.ChildUserId == null).FirstOrDefault();
                                var objLeftRefferalUser = db.UserMsts.Where(i => i.UserId == LastLeftUser.UserId).FirstOrDefault();
                                objLeftRefferalUser.LeftUserId = TempUserMst.UserId;
                            }
                        }
                        else if (user.Position == "R")
                        {
                            UserPosition = "Right";
                            if (!objRefferalUser.RightUserId.HasValue)
                            {
                                objRefferalUser.RightUserId = TempUserMst.UserId;
                            }
                            else
                            {
                                var LastRightUser = db.PROC_GET_ALL_RIGHT_USER_TREE(objRefferalUser.UserId, false).Where(i => i.ChildUserId == null).FirstOrDefault();
                                var objRightRefferalUser = db.UserMsts.Where(i => i.UserId == LastRightUser.UserId).FirstOrDefault();
                                objRightRefferalUser.RightUserId = TempUserMst.UserId;
                            }
                        }
                        db.SaveChanges();

                        if (!user.IsFreeAccount)
                        {
                            //var login = Login(TempUserMst.UserName, TempUserMst.Password, false);
                            //string json = new JavaScriptSerializer().Serialize(login.Data);
                            ////LoginResponse ResponseMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginResponse>(login.ToString());
                            //JavaScriptSerializer jss = new JavaScriptSerializer();
                            //LoginResponse ResponseMsg = jss.Deserialize<LoginResponse>(json);
                            //if (ResponseMsg.Key)
                            //{
                            //    //return RedirectToAction("Index", "Purchase", new { area = "Member" });
                            //    response.Key = true;
                            //}

                            response.Key = true;
                            if (TempUserMst.MobileNo.HasValue)
                            {
                                //var Msg = "Congratulation " + TempUserMst.FullName + " . You have been successful registered to maxener wellness - financial reward plan .Your user id - " + TempUserMst.UserName + " ,Password - " + TempUserMst.Password + "(do not share your pass word) to any other person.Complete your profile by loging in - " + HttpContext.Application[clsImplementationMessage.WebsiteURL].ToString();
                                var Msg = string.Format("Congratulation {0} . You have been successful registered to Maxener Wellness .Your user id - {1} ,Passcode - {2} (do not share your pass word) to any other person.Complete your profile by loging in - https://www.maxenerwellness.co.in Team Maxener Wellness"
                                                        , TempUserMst.FullName, TempUserMst.UserName, TempUserMst.Password);
                                objManager.SendSMS(TempUserMst.MobileNo, "1107160293313158853", Msg, "New Registration");
                            }

                            #region Notofication

                            var Notificationuser = "Hi, Congratulation Dear " + TempUserMst.FullName + " , Welcome To Team Maxener";
                            var objNotificationUser = new NotificationMst();
                            objNotificationUser.CreatedBy = TempUserMst.UserId;
                            objNotificationUser.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objNotificationUser.IsActive = true;
                            objNotificationUser.ModifiedBy = TempUserMst.UserId;
                            objNotificationUser.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objNotificationUser.Notification = Notificationuser;
                            objNotificationUser.NotificationTypeId = NotificationType.Information.GetHashCode();
                            objNotificationUser.UserId = TempUserMst.UserId;
                            db.NotificationMsts.Add(objNotificationUser);

                            var NotificationRefferaluser = "Congratulation " + objRefferalUser.FirstName + ", " + TempUserMst.FullName + " Joined Your " + UserPosition + " Side Team Member Check Now!";
                            var objNotificationRefferalUser = new NotificationMst();
                            objNotificationRefferalUser.CreatedBy = TempUserMst.UserId;
                            objNotificationRefferalUser.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objNotificationRefferalUser.IsActive = true;
                            objNotificationRefferalUser.ModifiedBy = TempUserMst.UserId;
                            objNotificationRefferalUser.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objNotificationRefferalUser.Notification = NotificationRefferaluser;
                            objNotificationRefferalUser.NotificationTypeId = NotificationType.Action.GetHashCode();
                            objNotificationRefferalUser.UserId = objRefferalUser.UserId;
                            objNotificationRefferalUser.URL = "/Member/Teams/Team";
                            db.NotificationMsts.Add(objNotificationRefferalUser);

                            db.SaveChanges();

                            #endregion Notofication
                        }
                    }
                    else
                    {
                        response.Key = false;
                        response.Msg = "User Name/EmailId Already Exist!";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (!CheckUserNameExistForTemp(user.UserName, user.TempUserId) && !CheckEmailIdExistForTemp(user.EmailId, user.TempUserId))
                    {
                        if (!string.IsNullOrEmpty(user.CityName))
                        {
                            var objCity = db.CityMsts.Where(i => i.CityName.Trim() == user.CityName.Replace("_xxx_", "#").Replace("_", " ")).FirstOrDefault();
                            if (objCity != null) { CityId = objCity.CityId; }
                        }

                        TempUserMst = db.UserMsts.Where(i => i.UserId == user.TempUserId).FirstOrDefault();
                        TempUserMst.FirstName = user.FirstName;
                        TempUserMst.LastName = user.LastName;
                        TempUserMst.UserName = user.UserName;
                        TempUserMst.Password = user.Password;
                        TempUserMst.EmailId = user.EmailId;
                        TempUserMst.MobileNo = user.MobileNo;
                        TempUserMst.CityId = CityId;
                        TempUserMst.FullName = user.FirstName + " " + user.LastName;

                        if (!string.IsNullOrEmpty(user.RefferalCode))
                        {
                            if (db.UserMsts.Any(i => i.RefferalCode == user.RefferalCode))
                            {
                                TempUserMst.RefferalUserId = db.UserMsts.Where(i => i.RefferalCode == user.RefferalCode).FirstOrDefault().RefferalUserId;
                            }
                            else
                            {
                                response.Key = false;
                                response.Msg = "InValid Refferal Code!";
                                return Json(response, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            TempUserMst.RefferalUserId = db.UserMsts.Where(i => i.RefferalUserId == 0).FirstOrDefault().UserId;
                        }

                        db.SaveChanges();
                    }
                    else
                    {
                        response.Key = false;
                        response.Msg = "User Name/EmailId Already Exist!";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                response.Key = true;
                response.Id = TempUserMst.UserId;
                response.Msg = user.RefferalCode;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private bool CheckUserNameExistForTemp(string UserName, int Id = 0)
        {
            return db.TempUserMsts.Any(i => i.TempUserId != Id && i.UserName == UserName && i.IsActive);
        }

        private bool CheckEmailIdExistForTemp(string EmailId, int Id = 0)
        {
            return db.TempUserMsts.Any(i => i.TempUserId != Id && i.EmailId == EmailId && i.IsActive);
        }

        #endregion "User Registration"

        public string RenderRazorViewToString(ControllerContext controllerContext, string viewName, object model = null)
        {
            controllerContext.Controller.ViewData.Model = model;

            using (var stringWriter = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}