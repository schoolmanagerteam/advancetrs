﻿using Elmah;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using TRS.Models;
using TRSImplementation;

namespace TRS.Areas.Utility.Controllers
{
    public class GeneralController : clsBase
    {
        // GET: Utility/General/GetSelectedProductsListList
        public PartialViewResult GetSelectedProductsListList(string cart)
        {
            List<SelectedProductsListEnt> listSelectedProductsListEnt = new List<SelectedProductsListEnt>();
            try
            {
                cart = cart.Replace(" ", "+");
                var SplitProductInfo = objManager.Decrypt(cart).Split(',');
                foreach (var products in SplitProductInfo)
                {
                    var productInfo = products.Split('_');
                    var prodid = Convert.ToInt32(productInfo[0]);
                    var qty = Convert.ToInt32(productInfo[1]);
                    var pvid = 0;
                    if (productInfo.Length > 2)
                    {
                        pvid = Convert.ToInt32(productInfo[2]);
                    }
                    var objProductMst = db.ProductMsts.Where(i => i.ProductId == prodid).FirstOrDefault();
                    var VariantName = "";
                    if (pvid > 0)
                    {
                        var objProductVariantMst = db.ProductVariantMsts.Where(i => i.ProductVariantId == pvid).FirstOrDefault();
                        if (objProductVariantMst != null) { VariantName = objProductVariantMst.VariantName; }
                    }
                    listSelectedProductsListEnt.Add(new SelectedProductsListEnt
                    {
                        Amount = ((objProductMst.SalePrice) * qty).ToString(),
                        ProductName = objProductMst.ProductName,
                        ProductPrice = (objProductMst.SalePrice).ToString(),
                        Qty = qty.ToString(),
                        ProductVariantName = VariantName
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("~/Views/Shared/_ViewSelectedProductsList.cshtml", listSelectedProductsListEnt);
        }

        public ActionResult ResizeImages()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var path = Server.MapPath("/Images/ProfileImages/");

                foreach (string fileName in Directory.GetFiles(path))
                {
                    var _comPath = fileName;
                    // fileName  is the file name
                    WebImage img = new WebImage(_comPath);
                    if (img.Height > img.Width)
                    {
                        img.Resize(400, 600, true, true);
                        img.Save(_comPath);
                    }
                    else
                    {
                        img.Resize(600, 400, true, true);
                        img.Save(_comPath);
                    }
                }
                response.Key = true;
                response.Msg = "All image is resized.";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProfileImageRotateLeft(string name)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                int index = name.IndexOf("?");
                if (index > 0)
                    name = name.Substring(0, index);

                var _comPath = Server.MapPath("/Images/ProfileImages/") + name;
                WebImage img = new WebImage(_comPath);
                img.RotateLeft();
                img.Save(_comPath);

                response.Key = true;
                response.Msg = "All image is resized.";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ProfileImageRotateRight(string name)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                int index = name.IndexOf("?");
                if (index > 0)
                    name = name.Substring(0, index);

                var _comPath = Server.MapPath("/Images/ProfileImages/") + name;
                WebImage img = new WebImage(_comPath);
                img.RotateRight();
                img.Save(_comPath);

                response.Key = true;
                response.Msg = "All image is resized.";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OrderReceipt(string id)
        {
            List<PROC_GET_ORDER_INVOICE_DETAILS_Result> Orders = db.PROC_GET_ORDER_INVOICE_DETAILS(id).OrderBy(p => p.ProductName).ToList();
            var groupedResults = (from s in Orders group s by s.OrderNo into g select new { orderNo = g.Key }).ToList();

            foreach (var groupedResult in groupedResults)
            {
                double CGST = 0;
                double SGST = 0;
                var OrderDetails = Orders.Where(p => p.OrderNo == groupedResult.orderNo).ToList();
                foreach (var item in OrderDetails)
                {
                    item.ProductPrice = Convert.ToDouble(((item.ProductPrice / (item.IsAyurvedic ? 105 : 118)) * 100).Value.ToString("#.00"));
                    item.Amount = Convert.ToDouble((item.ProductPrice * item.Qty).Value.ToString("#.00"));
                    var CGSTPer = item.IsAyurvedic ? 2.5 : 9;
                    var SGSTPer = item.IsAyurvedic ? 2.5 : 9;

                    CGST += (item.Amount.HasValue ? Convert.ToDouble((item.Amount.Value * CGSTPer / 100)) : 0);
                    SGST += (item.Amount.HasValue ? Convert.ToDouble((item.Amount.Value * SGSTPer / 100)) : 0);
                }
                SGST = Convert.ToDouble(SGST.ToString("#.00"));
                CGST = Convert.ToDouble(CGST.ToString("#.00"));
                var TotalAmount = OrderDetails.Sum(i => i.Amount);
                OrderDetails.ForEach(i => { i.TotalAmount = (TotalAmount.HasValue ? TotalAmount.Value : 0); i.SGST = SGST; i.CGST = CGST; });
            }
            return View(Orders);
        }

        public PartialViewResult GetConfirmProductsList(string cart, string UserAddressDtlId, bool IsCOD)
        {
            List<SelectedProductsListEnt> listSelectedProductsListEnt = new List<SelectedProductsListEnt>();
            try
            {
                cart = cart.Replace(" ", "+");
                var SplitProductInfo = objManager.Decrypt(cart).Split(',');
                foreach (var products in SplitProductInfo)
                {
                    var productInfo = products.Split('_');
                    var prodid = Convert.ToInt32(productInfo[0]);
                    var qty = Convert.ToInt32(productInfo[1]);
                    var pvid = 0;
                    if (productInfo.Length > 2)
                    {
                        pvid = Convert.ToInt32(productInfo[2]);
                    }
                    var objProductMst = db.ProductMsts.Where(i => i.ProductId == prodid).FirstOrDefault();
                    var VariantName = "";
                    if (pvid > 0)
                    {
                        var objProductVariantMst = db.ProductVariantMsts.Where(i => i.ProductVariantId == pvid).FirstOrDefault();
                        if (objProductVariantMst != null) { VariantName = objProductVariantMst.VariantName; }
                    }
                    listSelectedProductsListEnt.Add(new SelectedProductsListEnt
                    {
                        Amount = ((objProductMst.SalePrice) * qty).ToString(),
                        ProductName = objProductMst.ProductName,
                        ProductPrice = (objProductMst.SalePrice).ToString(),
                        Qty = qty.ToString(),
                        ProductVariantName = VariantName,
                        ImagePath = objProductMst.ImagePath,
                        ProductId = objProductMst.ProductId
                    });
                }

                decimal ProductTotalAmt = listSelectedProductsListEnt.Sum(x => Convert.ToDecimal(x.Amount));

                Int64 _UserAddressDtlId = Convert.ToInt64(UserAddressDtlId);

                var ObjUserAddress = db.UserAddressDtls.Where(x => x.UserAddressDtlId == _UserAddressDtlId).FirstOrDefault();
                if (ObjUserAddress != null)
                {
                    var objCity = db.CityMsts.Where(x => x.CityId == ObjUserAddress.CityId).FirstOrDefault();
                    var objState = db.StateMsts.Where(x => x.StateId == objCity.StateId).FirstOrDefault();

                    ViewBag.UsrAddress = ObjUserAddress.Address;
                    ViewBag.UsrPinCode = ObjUserAddress.PinCode;
                    ViewBag.UsrCity = objCity.CityName;
                    ViewBag.UsrState = objState.StateName;
                }

                ViewBag.UserAddressDtlId = _UserAddressDtlId;
                ViewBag.IsCOD = IsCOD;

                decimal MilesAmount = 0, OrdDeliveryCharge = 0;
                if (!IsCOD && !objclsLogin.RedirectFrmAdmin)
                {
                    List<string> listConfigs = new List<string>();
                    listConfigs.Add(ConfigKeys.MinimumOrderAmountForDeliveryChargeApply);
                    listConfigs.Add(ConfigKeys.OrderDeliveryCharge);
                    listConfigs.Add(ConfigKeys.MaxMilesUsePercentage);
                    listConfigs.Add(ConfigKeys.AllowToUseMilesAmtInDirectPurchase);

                    var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                    decimal MinmumOrderAmountForDeliveryChargeApply = 0, OrderDeliveryCharge = 0, MaxMilesUsePercentage = 0;
                    var AllowToUseMilesAmtInDirectPurchase = false;

                    if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MinimumOrderAmountForDeliveryChargeApply))
                    {
                        try
                        {
                            MinmumOrderAmountForDeliveryChargeApply = Convert.ToDecimal(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MinimumOrderAmountForDeliveryChargeApply).Value);
                        }
                        catch (Exception ex)
                        {
                            ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }

                    if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.OrderDeliveryCharge))
                    {
                        try
                        {
                            OrderDeliveryCharge = Convert.ToDecimal(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OrderDeliveryCharge).Value);
                        }
                        catch (Exception ex)
                        {
                            ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }

                    if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MaxMilesUsePercentage))
                    {
                        try
                        {
                            MaxMilesUsePercentage = Convert.ToDecimal(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MaxMilesUsePercentage).Value);
                        }
                        catch (Exception ex)
                        {
                            ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }

                    if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowToUseMilesAmtInDirectPurchase))
                    {
                        try
                        {
                            AllowToUseMilesAmtInDirectPurchase = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AllowToUseMilesAmtInDirectPurchase).Value);
                        }
                        catch (Exception ex)
                        {
                            ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }

                    //if (!objclsLogin.RedirectFrmAdmin)
                    //{
                    if (MinmumOrderAmountForDeliveryChargeApply > ProductTotalAmt)
                    {
                        OrdDeliveryCharge = OrderDeliveryCharge;
                        ViewBag.ReqAmountForFreeDelivery = (MinmumOrderAmountForDeliveryChargeApply - ProductTotalAmt);
                        ProductTotalAmt = (ProductTotalAmt + OrderDeliveryCharge);
                    }
                    else
                    {
                        OrdDeliveryCharge = 0;
                    }
                    //}

                    //NOTE : redirectfrom admin then prepaid order onlu use miles..
                    //if (!objclsLogin.RedirectFrmAdmin || (objclsLogin.RedirectFrmAdmin && 1 == 1))
                    //{
                    decimal MaximunMilesCanBeUse = 0;
                    MaximunMilesCanBeUse = Convert.ToInt64((ProductTotalAmt * MaxMilesUsePercentage) / 100);

                    if (objclsLogin.IsRegistrationActivated || (AllowToUseMilesAmtInDirectPurchase && !objclsLogin.IsRegistrationActivated))
                    {
                        //MilesAmount = objManager.GetUserMilesAmount(objclsLogin.UserId);
                        var objMilesAmount = db.UserMilesDtls.Where(x => x.UserId == objclsLogin.UserId);
                        if (objMilesAmount.Any())
                        {
                            double dblMilesAmount = db.UserMilesDtls.Where(x => x.UserId == objclsLogin.UserId).Select(x => (x.TotalCreditMiles - x.TotalDebitMiles)).FirstOrDefault();
                            MilesAmount = Math.Floor(Convert.ToDecimal(dblMilesAmount));
                        }
                    }

                    if ((decimal)MilesAmount > MaximunMilesCanBeUse)
                    {
                        MilesAmount = (decimal)MaximunMilesCanBeUse;
                        ProductTotalAmt = ProductTotalAmt - MaximunMilesCanBeUse;
                    }
                    else
                        ProductTotalAmt = ProductTotalAmt - MilesAmount;
                    //}
                }

                ViewBag.ProdTotalAmount = ProductTotalAmt;
                ViewBag.MilesAmount = MilesAmount;
                ViewBag.RedirectFrmAdmin = objclsLogin.RedirectFrmAdmin;
                ViewBag.DeliveryCharge = OrdDeliveryCharge;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("~/Views/Shared/_ViewConfirmProductsList.cshtml", listSelectedProductsListEnt);
        }
    }
}