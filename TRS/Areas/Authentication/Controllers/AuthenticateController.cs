﻿using Elmah;
using Newtonsoft.Json;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using TRS.Models;
using TRS;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace TRS.Areas.Authentication.Controllers
{
    //[NoCache]
    public class AuthenticateController : clsBase
    {
        // GET: Authentication/Authenticate
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), CookiesFilter]
        public ActionResult Index()
        {
            try
            {
                int Member = Convert.ToInt32(UserType.Member);
                //ViewBag.ReferralCodes = db.UserMsts.Where(i => i.UserType == Member && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).OrderBy(i => i.RefferalCode).Select(i => new AutoCompleteEnt { key = i.RefferalCode.ToString(), val = (i.FullName + "_" + i.RefferalCode).Replace(" ", "_").Replace("#", "_xxx_") }).ToList();
                //ViewBag.ReferralCodes = db.UserMsts.Where(i => i.UserType == Member).OrderBy(i => i.RefferalCode).Select(i => new AutoCompleteEnt { key = i.RefferalCode.ToString(), val = (i.FullName + "_" + i.RefferalCode).Replace(" ", "_").Replace("#", "_xxx_") }).ToList();
                ViewBag.ReferralCodes = "";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        // GET: Authentication/Authenticate/Login
        public JsonResult Login(string UserName, string Password, bool? RememberMe, bool RedirectFrmAdmin = false)
        {
            bool IsValiduser = false;
            int ValiduserBy = 0;
            LoginResponse response = new LoginResponse();
            int UserId = 0;
            bool IsAdminUser = false;
            bool IsOfficeUser = false;
            clsLogin clsLogin = null;
            try
            {
                if (db.UserMsts.Any(i => i.UserName.ToLower() == UserName.ToLower() && i.Password == Password))
                {
                    IsValiduser = true;
                    ValiduserBy = 1;
                }
                if (!IsValiduser && db.UserMsts.Any(i => i.EmailId.ToLower() == UserName.ToLower() && i.Password == Password))
                {
                    IsValiduser = true;
                    ValiduserBy = 2;
                }
                if (!IsValiduser && db.UserMsts.Any(i => i.RefferalCode.ToLower() == UserName.ToLower() && i.Password == Password))
                {
                    IsValiduser = true;
                    ValiduserBy = 3;
                }
                if (IsValiduser)
                {
                    response.Key = true;
                    var objUser = new UserMst();
                    if (ValiduserBy == 1)
                    {
                        objUser = db.UserMsts.Where(i => i.UserName.ToLower() == UserName.ToLower() && i.Password == Password).FirstOrDefault();
                    }
                    else if (ValiduserBy == 2)
                    {
                        objUser = db.UserMsts.Where(i => i.EmailId.ToLower() == UserName.ToLower() && i.Password == Password).FirstOrDefault();
                    }
                    else if (ValiduserBy == 3)
                    {
                        objUser = db.UserMsts.Where(i => i.RefferalCode.ToLower() == UserName.ToLower() && i.Password == Password).FirstOrDefault();
                    }
                    if (objUser.IsBlocked.HasValue && objUser.IsBlocked.Value)
                    {
                        response.Key = false;
                        response.Msg = "Your account has been blocked! Kindly contact to administrator for unlock acount.";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                    objManager.SetLoginSessionInformation(objUser);
                    clsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];

                    //SET RedirectFrmAdmin Property..
                    clsLogin.RedirectFrmAdmin = RedirectFrmAdmin;
                    System.Web.HttpContext.Current.Session[clsImplementationMessage.LoginInfo] = clsLogin;
                    clsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];

                    if (clsLogin.UserType == UserType.Member)
                    {
                        response.IsProfileCompleted = clsLogin.IsProfileCompleted;
                        response.IsRegistrated = clsLogin.IsRegistrationActivated;
                    }
                    else
                    {
                        response.IsProfileCompleted = true;
                    }
                    UserId = objUser.UserId;
                    response.Msg = Convert.ToInt32(objUser.UserType).ToString();
                }
                else
                {
                    if (db.FranchiseeMsts.Any(i => i.UserName.ToLower() == UserName.ToLower() && i.Password == Password))
                    {
                        response.Key = true;
                        var objFranchisee = db.FranchiseeMsts.Where(i => i.UserName.ToLower() == UserName.ToLower() && i.Password == Password).FirstOrDefault();
                        objManager.SetLoginSessionInformation(null, false, "", objFranchisee);
                        clsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                        UserId = clsLogin.UserId;
                        response.Msg = Convert.ToInt32(clsLogin.UserType).ToString();
                    }
                    else
                    {
                        List<string> listConfigKeys = new List<string>();
                        listConfigKeys.Add(ConfigKeys.AdminPwd);
                        listConfigKeys.Add(ConfigKeys.AdminUser);
                        listConfigKeys.Add(ConfigKeys.OfficeUserUserName);
                        listConfigKeys.Add(ConfigKeys.OfficeUserPassword);
                        listConfigKeys.Add(ConfigKeys.OfficeUserUserId);
                        listConfigKeys.Add(ConfigKeys.AdminUserId);

                        listConfigKeys.Add(ConfigKeys.OfficeUserUserName1);
                        listConfigKeys.Add(ConfigKeys.OfficeUserPassword1);
                        listConfigKeys.Add(ConfigKeys.OfficeUserUserId1);

                        listConfigKeys.Add(ConfigKeys.OfficeUserUserName2);
                        listConfigKeys.Add(ConfigKeys.OfficeUserPassword2);
                        listConfigKeys.Add(ConfigKeys.OfficeUserUserId2);

                        listConfigKeys.Add(ConfigKeys.OfficeUserUserName3);
                        listConfigKeys.Add(ConfigKeys.OfficeUserPassword3);
                        listConfigKeys.Add(ConfigKeys.OfficeUserUserId3);

                        var listConfig = db.ConfigMsts.Where(i => listConfigKeys.Contains(i.Key.ToUpper())).ToList();
                        var AdminUser = "";
                        var AdminPwd = "";
                        var OfficeUserUserName = "";
                        var OfficeUserPassword = "";

                        var OfficeUserUserName1 = "";
                        var OfficeUserPassword1 = "";

                        var OfficeUserUserName2 = "";
                        var OfficeUserPassword2 = "";

                        var OfficeUserUserName3 = "";
                        var OfficeUserPassword3 = "";

                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.AdminUser))
                        {
                            AdminUser = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdminUser).Value;
                        }
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.AdminPwd))
                        {
                            AdminPwd = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdminPwd).Value;
                        }
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName))
                        {
                            OfficeUserUserName = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName).Value;
                        }
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserPassword))
                        {
                            OfficeUserPassword = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserPassword).Value;
                        }

                        //OfficeUserUserName1
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName1))
                        {
                            OfficeUserUserName1 = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName1).Value;
                        }
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserPassword1))
                        {
                            OfficeUserPassword1 = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserPassword1).Value;
                        }

                        //OfficeUserUserName2
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName2))
                        {
                            OfficeUserUserName2 = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName2).Value;
                        }
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserPassword2))
                        {
                            OfficeUserPassword2 = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserPassword2).Value;
                        }

                        //OfficeUserUserName3
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName3))
                        {
                            OfficeUserUserName3 = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName3).Value;
                        }
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserPassword3))
                        {
                            OfficeUserPassword3 = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserPassword3).Value;
                        }

                        if (UserName == AdminUser && Password == AdminPwd)
                        {
                            UserId = Convert.ToInt32(listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdminUserId.ToUpper()).Value);
                            UserMst objUser = new UserMst();
                            objUser.UserId = UserId;

                            objManager.SetLoginSessionInformation(objUser, true, AdminUser);
                            response.Key = true;
                            response.Msg = Convert.ToInt32(UserType.Admin).ToString();
                            IsAdminUser = true;
                        }
                        else if (UserName.ToLower() == OfficeUserUserName.ToLower() && Password == OfficeUserPassword)
                        {
                            UserId = Convert.ToInt32(listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserId.ToUpper()).Value);
                            UserMst objUser = new UserMst();
                            objUser.UserId = UserId;

                            objManager.SetLoginSessionInformation(objUser, true, OfficeUserUserName, null, true, OfficeUserUserName);
                            response.Key = true;
                            response.Msg = Convert.ToInt32(UserType.Office).ToString();
                            IsOfficeUser = true;
                        }
                        //OfficeUserUserName1
                        else if (UserName.ToLower() == OfficeUserUserName1.ToLower() && Password == OfficeUserPassword1)
                        {
                            UserId = Convert.ToInt32(listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserId1.ToUpper()).Value);
                            UserMst objUser = new UserMst();
                            objUser.UserId = UserId;

                            objManager.SetLoginSessionInformation(objUser, true, OfficeUserUserName1, null, true, OfficeUserUserName1);
                            response.Key = true;
                            response.Msg = Convert.ToInt32(UserType.Office).ToString();
                            IsOfficeUser = true;
                        }
                        //OfficeUserUserName2
                        else if (UserName.ToLower() == OfficeUserUserName2.ToLower() && Password == OfficeUserPassword2)
                        {
                            UserId = Convert.ToInt32(listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserId2.ToUpper()).Value);
                            UserMst objUser = new UserMst();
                            objUser.UserId = UserId;

                            objManager.SetLoginSessionInformation(objUser, true, OfficeUserUserName2, null, true, OfficeUserUserName2);
                            response.Key = true;
                            response.Msg = Convert.ToInt32(UserType.Office).ToString();
                            IsOfficeUser = true;
                        }
                        //OfficeUserUserName3
                        else if (UserName.ToLower() == OfficeUserUserName3.ToLower() && Password == OfficeUserPassword3)
                        {
                            UserId = Convert.ToInt32(listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserId3.ToUpper()).Value);
                            UserMst objUser = new UserMst();
                            objUser.UserId = UserId;

                            objManager.SetLoginSessionInformation(objUser, true, OfficeUserUserName3, null, true, OfficeUserUserName3);
                            response.Key = true;
                            response.Msg = Convert.ToInt32(UserType.Office).ToString();
                            IsOfficeUser = true;
                        }
                        else
                        {
                            response.Key = false;
                            response.Msg = "Invalid Username and Password";
                        }
                    }
                }

                //User OTP....
                if (IsAdminUser || IsOfficeUser)
                {
                    LoginResponse userOTP = SaveUserOTP(UserId);
                    if (!userOTP.Key)
                    {
                        response.Key = userOTP.Key;
                        response.Msg = userOTP.Msg;
                        Session[clsImplementationMessage.LoginInfo] = null;
                        Session.Abandon();
                    }

                    //clsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                    //if (clsLogin != null)
                    //{
                    //    clsLogin.IsOTPAuthenticate = true;
                    //    System.Web.HttpContext.Current.Session[clsImplementationMessage.LoginInfo] = clsLogin;
                    //}
                }

                if (response.Key && RememberMe.HasValue && RememberMe.Value)
                {
                    HttpCookie uid = new HttpCookie("uid", objManager.HigherEncrypt(UserName));
                    // set the cookie's expiration date
                    uid.Expires = DateTime.Now.AddDays(30);
                    // set the cookie on client's browser
                    HttpContext.Response.Cookies.Add(uid);

                    if (!IsAdminUser && !IsOfficeUser)
                    {
                        HttpCookie pid = new HttpCookie("pid", objManager.HigherEncrypt(Password));
                        // set the cookie's expiration date
                        pid.Expires = DateTime.Now.AddDays(30);
                        // set the cookie on client's browser
                        HttpContext.Response.Cookies.Add(pid);

                        HttpCookie id = new HttpCookie("id", objManager.HigherEncrypt(UserId.ToString()));
                        // set the cookie's expiration date
                        id.Expires = DateTime.Now.AddDays(30);
                        // set the cookie on client's browser
                        HttpContext.Response.Cookies.Add(id);

                        HttpCookie type = new HttpCookie("typ", objManager.HigherEncrypt(clsLogin.UserType.GetHashCode().ToString()));
                        // set the cookie's expiration date
                        type.Expires = DateTime.Now.AddDays(30);
                        // set the cookie on client's browser
                        HttpContext.Response.Cookies.Add(type);
                    }

                    HttpCookie isa = new HttpCookie("isa", objManager.HigherEncrypt(IsAdminUser.ToString()));
                    // set the cookie's expiration date
                    isa.Expires = DateTime.Now.AddDays(30);
                    // set the cookie on client's browser
                    HttpContext.Response.Cookies.Add(isa);

                    HttpCookie iso = new HttpCookie("iso", objManager.HigherEncrypt(IsOfficeUser.ToString()));
                    // set the cookie's expiration date
                    iso.Expires = DateTime.Now.AddDays(30);
                    // set the cookie on client's browser
                    HttpContext.Response.Cookies.Add(iso);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public void SetLoginSessionInformation(UserMst objUser)
        {
            clsLogin clsLogin = new clsLogin();
            try
            {
                if (objUser.UserType == Convert.ToInt32(UserType.Member))
                {
                    clsLogin.FirstName = objUser.FirstName;
                    clsLogin.FullName = objUser.FullName;
                    clsLogin.LastName = objUser.LastName;
                    clsLogin.RefferalCode = objUser.RefferalCode;
                    clsLogin.UserId = objUser.UserId;
                    clsLogin.MobileNo = objUser.MobileNo.HasValue ? objUser.MobileNo.Value.ToString() : "";
                    clsLogin.EmailId = objUser.EmailId;
                    clsLogin.UserName = objUser.UserName;
                    clsLogin.UserType = UserType.Member;
                    clsLogin.IsProfileCompleted = !string.IsNullOrEmpty(objUser.Address) ? true : false;
                    clsLogin.IsRegistrationActivated = objUser.IsRegistrationActivated.HasValue && objUser.IsRegistrationActivated.Value ? true : false;
                }
                //else if (objUser.UserType == Convert.ToInt32(UserType.Franchisee))
                //{
                //    var objFranchisee = db.FranchiseeMsts.Where(i => i.UserId == objUser.UserId).FirstOrDefault();
                //    clsLogin.FirstName = objFranchisee.FirstName;
                //    clsLogin.FullName = objFranchisee.FullName;
                //    clsLogin.LastName = objFranchisee.LastName;
                //    clsLogin.UserId = objFranchisee.UserId;
                //    clsLogin.EmailId = objFranchisee.EmailId;
                //    clsLogin.UserName = objUser.UserName;
                //    clsLogin.FranchiseeName = objFranchisee.FranchiseeName;
                //    clsLogin.FranchiseeId = objFranchisee.FranchiseeId;
                //    clsLogin.UserType = UserType.Franchisee;
                //    clsLogin.IsProfileCompleted = !string.IsNullOrEmpty(objUser.Address) ? true : false;
                //}
                Session[clsImplementationMessage.LoginInfo] = clsLogin;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        // GET: Authentication/Authenticate/ForgotPassword
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // GET: Authentication/Authenticate/Signup
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Signup(string p, string rc)
        {
            var TempUserMst = new TempUserMst();
            try
            {
                ViewBag.p = p;
                ViewBag.RefferalCode = rc;
                //var objUserMst = db.UserMsts.Where(i => i.RefferalCode == rc && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).FirstOrDefault();
                var objUserMst = db.UserMsts.Where(i => i.RefferalCode == rc).FirstOrDefault();
                if (string.IsNullOrEmpty(rc) || objUserMst == null)
                {
                    return RedirectToAction("Index", "Authenticate");
                }
                if (!string.IsNullOrEmpty(p))
                {
                    ViewBag.DefaultPosition = p.ToUpper().Trim();
                    ViewBag.IsPositionEnabled = false;
                }
                else
                {
                    ViewBag.DefaultPosition = !string.IsNullOrEmpty(objUserMst.Position) ? objUserMst.Position : "L";
                    ViewBag.IsPositionEnabled = true;
                }

                //if (!db.UserMsts.Any(i => i.RefferalUserId == objUserMst.UserId && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value))
                //{
                //    ViewBag.DefaultPosition = !string.IsNullOrEmpty(objUserMst.Position) ? objUserMst.Position : "L";
                //    ViewBag.IsPositionEnabled = false;
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(p))
                //    {
                //        ViewBag.DefaultPosition = p.ToUpper().Trim();
                //        ViewBag.IsPositionEnabled = false;
                //    }
                //    else
                //    {
                //        ViewBag.DefaultPosition = "L";
                //        ViewBag.IsPositionEnabled = true;
                //    }
                //}

                ViewBag.Cities = db.CityMsts.OrderBy(i => i.CityName).Select(i => new AutoCompleteEnt { key = i.CityId.ToString(), val = i.CityName.Replace(" ", "_").Replace("#", "_xxx_") }).ToList();
            }
            catch (Exception ex)
            {
                ViewBag.DefaultPosition = "L";
                ViewBag.IsPositionEnabled = true;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            //ViewBag.IsPositionEnabled = true;
            return View(TempUserMst);
        }

        // GET: Authentication/Authenticate/Plans
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        //public ActionResult Plans(int id, string refcode)
        public ActionResult Plans(string rc)
        {
            //ViewBag.Id = id;
            //var objUserMst = db.UserMsts.Where(i => i.RefferalCode == refcode).FirstOrDefault();
            //if (string.IsNullOrEmpty(refcode) || objUserMst == null)
            //{
            //    return RedirectToAction("Signup", "Authenticate", new { id = id, refcode = refcode });
            //}
            var objUserMst = db.UserMsts.Where(i => i.RefferalCode == rc).FirstOrDefault();
            if (string.IsNullOrEmpty(rc) || objUserMst == null)
            {
                return RedirectToAction("Index", "Authenticate");
            }
            ViewBag.rc = rc;
            return View();
        }

        public JsonResult GetOTP(string MobileNo)
        {
            ResponseMsg response = new ResponseMsg();
            Int64 ActualMobileNo = Convert.ToInt64(MobileNo);
            var NoOfSameMobileNoAllow = 2;
            if (db.ConfigMsts.Any(i => i.Key.ToUpper() == ConfigKeys.NoOfSameMobileNoAllow.ToUpper()))
            {
                NoOfSameMobileNoAllow = Convert.ToInt32(db.ConfigMsts.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.NoOfSameMobileNoAllow.ToUpper()).Value);
            }
            if (db.UserMsts.Count(i => i.MobileNo == ActualMobileNo) >= NoOfSameMobileNoAllow)
            {
                response.Key = false;
                response.Msg = MobileNo + " is already register.";
                return Json(response);
            }

            Random generator = new Random();
            String r = generator.Next(0, 999999).ToString("D6");

            //var Msg = "OTP :- " + r.ToString();
            var Msg = string.Format("Your otp {0} is for registration with Maxener Wellness e comerce platform . Do not share . Team Maxener Wellness", r.ToString());
            try
            {
                decimal dMobileNumber = 0;
                if (!decimal.TryParse(MobileNo, out dMobileNumber))
                    dMobileNumber = 0;
                objManager.SendSMS(dMobileNumber, "1107160293295792769", Msg, "Registration OTP");
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            response.Key = true;
            response.Msg = r;
            return Json(response);
        }

        //public JsonResult SendSMSToAllUserDetails()
        //{
        //    ResponseMsg response = new ResponseMsg();
        //    try
        //    {
        //        List<SMSSendDetails> listSMSSendDetails = new List<SMSSendDetails>();
        //        var listUsers = db.UserMsts.Select(i => new { UserId = i.UserId, MobileNo = i.MobileNo, AlternativeMobileNo = i.AlternativeMobileNo, UserName = i.UserName, Password = i.Password, RefferalCode = i.RefferalCode }).ToList();
        //        foreach (var item in listUsers)
        //        {
        //            if (item.MobileNo.HasValue)
        //            {
        //                listSMSSendDetails.Add(new SMSSendDetails
        //                {
        //                    MobileNo = Convert.ToInt64(item.MobileNo.Value),
        //                    Msg = "Hello, Your Maxener Associate Login Details - LoginId : " + item.RefferalCode + " , UserName : " + item.UserName + " , Password : " + item.Password + " , URL :- " + HttpContext.Application[clsImplementationMessage.WebsiteURL].ToString()
        //                });
        //            }
        //        }

        //        foreach (var item in listSMSSendDetails)
        //        {
        //            objManager.SendSMS(item.MobileNo, item.Msg, "User Details");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorSignal.FromCurrentContext().Raise(ex);
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}

        //// GET: Authentication/Authenticate/Plans/1
        //[OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        ////public ActionResult Products(int id, int pid)
        //public ActionResult Products(string rc)
        //{
        //    List<PROC_GET_PRODUCT_LIST_Result> listProductMst = new List<PROC_GET_PRODUCT_LIST_Result>();
        //    try
        //    {
        //        //ViewBag.Id = id;
        //        //ViewBag.PId = pid;
        //        var objUserMst = db.UserMsts.Where(i => i.RefferalCode == rc).FirstOrDefault();
        //        if (string.IsNullOrEmpty(rc) || objUserMst == null)
        //        {
        //            return RedirectToAction("Index", "Authenticate");
        //        }
        //        listProductMst = db.PROC_GET_PRODUCT_LIST().ToList();
        //        ViewBag.ProductVariants = db.ProductVariantMsts.Select(i => new ProductVariantListEnt{ VariantName = i.VariantName, ProductVariantId = i.ProductVariantId.ToString(),ProductId=i.ProductId.ToString() }).ToList();
        //        ViewBag.rc = rc;
        //        ViewBag.PId = "1";
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorSignal.FromCurrentContext().Raise(ex);
        //    }
        //    return View(listProductMst);
        //}

        public ActionResult RegisterUser(TempRegistration user)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var TempUserMst = new UserMst();
                decimal CityId = 0;
                if (user.TempUserId == 0)
                {
                    if (!objManager.CheckUserNameExist(user.UserName) && !objManager.CheckEmailIdExist(user.EmailId))
                    {
                        List<string> listConfigs = new List<string>();
                        listConfigs.Add(ConfigKeys.RefernceMilesAmount);
                        var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                        double RefernceMilesAmount = 0;
                        if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.RefernceMilesAmount))
                        {
                            try
                            {
                                RefernceMilesAmount = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.RefernceMilesAmount).Value);
                            }
                            catch (Exception ex)
                            {
                                ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }

                        //var objRefferalUserMst = db.UserMsts.Where(i => i.RefferalCode == user.RefferalCode && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).FirstOrDefault();
                        var objRefferalUserMst = db.UserMsts.Where(i => i.RefferalCode == user.RefferalCode).FirstOrDefault();
                        if (string.IsNullOrEmpty(user.RefferalCode) || objRefferalUserMst == null)
                        {
                            response.Key = false;
                            response.Msg = "InValid Refferal Code!";
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }

                        if (user.FullName.IsNullOrEmpty())
                        {
                            response.Key = false;
                            response.Msg = "Enter First|Last Name!";
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }

                        TempUserMst.FirstName = TempUserMst.LastName = string.Empty;
                        if (!string.IsNullOrEmpty(TempUserMst.FullName))
                        {
                            var names = TempUserMst.FullName.Split(' ');
                            if (names != null && names.Length > 0)
                            {
                                TempUserMst.FirstName = names[0];
                                if (names.Length > 1)
                                    TempUserMst.LastName = names[1];
                            }
                        }

                        TempUserMst.FullName = !string.IsNullOrEmpty(user.FullName) ? user.FullName.Trim() : null;
                        TempUserMst.UserName = !string.IsNullOrEmpty(user.UserName) ? user.UserName.Trim() : null;
                        TempUserMst.Password = !string.IsNullOrEmpty(user.Password) ? user.Password.Trim() : null;
                        TempUserMst.EmailId = !string.IsNullOrEmpty(user.EmailId) ? user.EmailId.Trim() : null;
                        TempUserMst.MobileNo = user.MobileNo;
                        TempUserMst.Position = user.Position;
                        //TempUserMst.RefferalCode = objManager.GenerateRefferalCode();

                        //bool CheckRefferalCodeduplicate = db.UserMsts.Any(i => i.RefferalCode == TempUserMst.RefferalCode);
                        //while (CheckRefferalCodeduplicate)
                        //{
                        //    TempUserMst.RefferalCode = objManager.GenerateRefferalCode();
                        //    if (!db.UserMsts.Any(i => i.RefferalCode == TempUserMst.RefferalCode))
                        //        CheckRefferalCodeduplicate = false;
                        //}

                        TempUserMst.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        TempUserMst.RegMonth = TempUserMst.CreatedDate.Value.Month;
                        TempUserMst.RegYear = TempUserMst.CreatedDate.Value.Year;
                        TempUserMst.IsActive = true;
                        TempUserMst.RegistrationDate = TempUserMst.CreatedDate;
                        TempUserMst.UserType = UserType.Member.GetHashCode();

                        if (!string.IsNullOrEmpty(user.RefferalCode))
                        {
                            if (db.UserMsts.Any(i => i.RefferalCode == user.RefferalCode))
                            {
                                TempUserMst.RefferalUserId = db.UserMsts.Where(i => i.RefferalCode == user.RefferalCode).FirstOrDefault().UserId;
                            }
                            else
                            {
                                response.Key = false;
                                response.Msg = "InValid Refferal Code!";
                                return Json(response, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            TempUserMst.RefferalUserId = db.UserMsts.Where(i => i.RefferalUserId == 0).FirstOrDefault().UserId;
                        }

                        db.UserMsts.Add(TempUserMst);
                        db.SaveChanges();

                        #region Miles Credit Details

                        if (RefernceMilesAmount > 0)
                        {
                            var objMilesCreditDtl = new MilesCreditDtl();
                            objMilesCreditDtl.CreatedBy = TempUserMst.UserId;
                            objMilesCreditDtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objMilesCreditDtl.CreditAmount = (decimal)RefernceMilesAmount;
                            objMilesCreditDtl.CreditDate = objMilesCreditDtl.CreatedDate;
                            objMilesCreditDtl.IsActive = true;
                            objMilesCreditDtl.UserId = TempUserMst.RefferalUserId;
                            objMilesCreditDtl.UserIdFrom = TempUserMst.UserId;
                            objMilesCreditDtl.InsertType = (int)MilesCreditType.ForRegisterUser;
                            db.MilesCreditDtls.Add(objMilesCreditDtl);

                            var IsUserMilesExists = db.UserMilesDtls.Where(x => x.UserId == TempUserMst.RefferalUserId);
                            if (IsUserMilesExists.Any())
                            {
                                var ExistsUserMiles = db.UserMilesDtls.Where(x => x.UserId == TempUserMst.RefferalUserId).FirstOrDefault();
                                ExistsUserMiles.TotalCreditMiles = ExistsUserMiles.TotalCreditMiles + RefernceMilesAmount;
                            }
                            else
                            {
                                UserMilesDtl tblUserMilesDtl = new UserMilesDtl();
                                tblUserMilesDtl.UserId = TempUserMst.RefferalUserId;
                                tblUserMilesDtl.TotalCreditMiles = RefernceMilesAmount;
                                db.UserMilesDtls.Add(tblUserMilesDtl);
                            }

                            db.SaveChanges();
                        }

                        #endregion Miles Credit Details

                        var UserPosition = "";
                        var objRefferalUser = db.UserMsts.Where(i => i.UserId == TempUserMst.RefferalUserId).FirstOrDefault();
                        if (user.Position == "L")
                        {
                            UserPosition = "Left";
                            if (!objRefferalUser.LeftUserId.HasValue)
                            {
                                objRefferalUser.LeftUserId = TempUserMst.UserId;
                            }
                            else
                            {
                                var LastLeftUser = db.PROC_GET_ALL_LEFT_USER_TREE(objRefferalUser.UserId, false).Where(i => i.ChildUserId == null).FirstOrDefault();
                                var objLeftRefferalUser = db.UserMsts.Where(i => i.UserId == LastLeftUser.UserId).FirstOrDefault();
                                objLeftRefferalUser.LeftUserId = TempUserMst.UserId;
                            }
                        }
                        else if (user.Position == "R")
                        {
                            UserPosition = "Right";
                            if (!objRefferalUser.RightUserId.HasValue)
                            {
                                objRefferalUser.RightUserId = TempUserMst.UserId;
                            }
                            else
                            {
                                var LastRightUser = db.PROC_GET_ALL_RIGHT_USER_TREE(objRefferalUser.UserId, false).Where(i => i.ChildUserId == null).FirstOrDefault();
                                var objRightRefferalUser = db.UserMsts.Where(i => i.UserId == LastRightUser.UserId).FirstOrDefault();
                                objRightRefferalUser.RightUserId = TempUserMst.UserId;
                            }
                        }
                        db.SaveChanges();

                        if (!user.IsFreeAccount)
                        {
                            var login = Login(TempUserMst.UserName, TempUserMst.Password, false);
                            string json = new JavaScriptSerializer().Serialize(login.Data);
                            //LoginResponse ResponseMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginResponse>(login.ToString());
                            JavaScriptSerializer jss = new JavaScriptSerializer();
                            LoginResponse ResponseMsg = jss.Deserialize<LoginResponse>(json);
                            if (ResponseMsg.Key)
                            {
                                //return RedirectToAction("Index", "Purchase", new { area = "Member" });
                                response.Key = true;
                            }
                            if (TempUserMst.MobileNo.HasValue)
                            {
                                //var Msg = "Congratulation " + TempUserMst.FullName + " . You have been successful registered to maxener wellness - financial reward plan .Your user id - " + TempUserMst.UserName + " ,Password - " + TempUserMst.Password + "(do not share your pass word) to any other person.Complete your profile by loging in - " + HttpContext.Application[clsImplementationMessage.WebsiteURL].ToString();
                                var Msg = string.Format("Congratulation {0} . You have been successful registered to Maxener Wellness .Your user id - {1} ,Passcode - {2} (do not share your pass word) to any other person.Complete your profile by loging in - https://www.maxenerwellness.co.in Team Maxener Wellness"
                                                        , TempUserMst.FullName, TempUserMst.UserName, TempUserMst.Password);
                                objManager.SendSMS(TempUserMst.MobileNo, "1107160293313158853", Msg, "New Registration");
                            }

                            #region Notofication

                            var Notificationuser = "Hi, Congratulation Dear " + TempUserMst.FullName + " , Welcome To Team Maxener";
                            var objNotificationUser = new NotificationMst();
                            objNotificationUser.CreatedBy = TempUserMst.UserId;
                            objNotificationUser.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objNotificationUser.IsActive = true;
                            objNotificationUser.ModifiedBy = TempUserMst.UserId;
                            objNotificationUser.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objNotificationUser.Notification = Notificationuser;
                            objNotificationUser.NotificationTypeId = NotificationType.Information.GetHashCode();
                            objNotificationUser.UserId = TempUserMst.UserId;
                            db.NotificationMsts.Add(objNotificationUser);

                            var NotificationRefferaluser = "Congratulation " + objRefferalUser.FirstName + ", " + TempUserMst.FullName + " Joined Your " + UserPosition + " Side Team Member Check Now!";
                            var objNotificationRefferalUser = new NotificationMst();
                            objNotificationRefferalUser.CreatedBy = TempUserMst.UserId;
                            objNotificationRefferalUser.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objNotificationRefferalUser.IsActive = true;
                            objNotificationRefferalUser.ModifiedBy = TempUserMst.UserId;
                            objNotificationRefferalUser.ModifiedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                            objNotificationRefferalUser.Notification = NotificationRefferaluser;
                            objNotificationRefferalUser.NotificationTypeId = NotificationType.Action.GetHashCode();
                            objNotificationRefferalUser.UserId = objRefferalUser.UserId;
                            objNotificationRefferalUser.URL = "/Member/Teams/Team";
                            db.NotificationMsts.Add(objNotificationRefferalUser);

                            db.SaveChanges();

                            #endregion Notofication
                        }
                    }
                    else
                    {
                        response.Key = false;
                        response.Msg = "User Name/EmailId Already Exist!";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (!CheckUserNameExistForTemp(user.UserName, user.TempUserId) && !CheckEmailIdExistForTemp(user.EmailId, user.TempUserId))
                    {
                        if (!string.IsNullOrEmpty(user.CityName))
                        {
                            var objCity = db.CityMsts.Where(i => i.CityName.Trim() == user.CityName.Replace("_xxx_", "#").Replace("_", " ")).FirstOrDefault();
                            if (objCity != null) { CityId = objCity.CityId; }
                        }

                        TempUserMst = db.UserMsts.Where(i => i.UserId == user.TempUserId).FirstOrDefault();
                        TempUserMst.FirstName = user.FirstName;
                        TempUserMst.LastName = user.LastName;
                        TempUserMst.UserName = user.UserName;
                        TempUserMst.Password = user.Password;
                        TempUserMst.EmailId = user.EmailId;
                        TempUserMst.MobileNo = user.MobileNo;
                        TempUserMst.CityId = CityId;
                        TempUserMst.FullName = user.FirstName + " " + user.LastName;

                        if (!string.IsNullOrEmpty(user.RefferalCode))
                        {
                            if (db.UserMsts.Any(i => i.RefferalCode == user.RefferalCode))
                            {
                                TempUserMst.RefferalUserId = db.UserMsts.Where(i => i.RefferalCode == user.RefferalCode).FirstOrDefault().RefferalUserId;
                            }
                            else
                            {
                                response.Key = false;
                                response.Msg = "InValid Refferal Code!";
                                return Json(response, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            TempUserMst.RefferalUserId = db.UserMsts.Where(i => i.RefferalUserId == 0).FirstOrDefault().UserId;
                        }

                        db.SaveChanges();
                    }
                    else
                    {
                        response.Key = false;
                        response.Msg = "User Name/EmailId Already Exist!";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                response.Key = true;
                response.Id = TempUserMst.UserId;
                response.Msg = user.RefferalCode;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PaymentSuccess(string order_id, string payment_id, string signature)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var KeyDetails = objManager.GetPaymentKeyDetails();
                RazorpayClient client = new RazorpayClient(KeyDetails.PaymentKey, KeyDetails.PaymentSecret);

                Dictionary<string, string> attributes = new Dictionary<string, string>();

                attributes.Add("razorpay_payment_id", payment_id);
                attributes.Add("razorpay_order_id", order_id);
                attributes.Add("razorpay_signature", signature);

                Utils.verifyPaymentSignature(attributes);

                //Refund refund = new Razorpay.Api.Payment((string) paymentId).Refund();
                Order order = client.Order.Fetch(order_id);
                Razorpay.Api.Payment payment = client.Payment.Fetch(payment_id);

                var pinfo = order["notes"].pinfo;
                var id = order["notes"].id;
                var amount = order["notes"].amount;

                //Dictionary<string, object> options = new Dictionary<string, object>();
                //options.Add("amount",Convert.ToDouble(amount));
                //Payment paymentCaptured = payment.Capture(options);
                if (payment["status"] == "captured")
                {
                    //response.Key = PaymentResponse(Convert.ToInt32(id), pinfo.ToString(), order_id, payment_id, signature);
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckRefferalCodeExist(string refcode)
        {
            ResponseMsg objResponse = new ResponseMsg();
            try
            {
                if (IsRefferalCodeExist(refcode))
                {
                    objResponse.Key = true;
                    //objResponse.Msg = db.UserMsts.Where(i => i.RefferalCode == refcode.Trim() && i.IsActive == true && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value).FirstOrDefault().FullName;
                    objResponse.Msg = db.UserMsts.Where(i => i.RefferalCode == refcode.Trim() && i.IsActive == true).FirstOrDefault().FullName;
                }
                else
                {
                    objResponse.Key = false;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }

        public bool IsRefferalCodeExist(string refcode)
        {
            var IsExist = false;
            try
            {
                //if (db.UserMsts.Any(i => i.RefferalCode == refcode.Trim() && i.IsActive == true && i.IsRegistrationActivated.HasValue && i.IsRegistrationActivated.Value))
                if (db.UserMsts.Any(i => i.RefferalCode == refcode.Trim() && i.IsActive == true))
                {
                    IsExist = true;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return IsExist;
        }

        public void SendEmail()
        {
            objManager.SendMail("", "Test Mail", "This is test mail<br/> for email testing.");
        }

        public JsonResult SendForgotPassword(string UserName)
        {
            bool IsValiduser = false;
            ResponseMsg response = new ResponseMsg();
            try
            {
                int ValiduserBy = 0;

                if (db.UserMsts.Any(i => i.UserName.ToLower() == UserName.ToLower()))
                {
                    IsValiduser = true;
                    ValiduserBy = 1;
                }
                if (!IsValiduser && db.UserMsts.Any(i => i.EmailId.ToLower() == UserName.ToLower()))
                {
                    IsValiduser = true;
                    ValiduserBy = 2;
                }
                if (!IsValiduser && db.UserMsts.Any(i => i.RefferalCode.ToLower() == UserName.ToLower()))
                {
                    IsValiduser = true;
                    ValiduserBy = 3;
                }
                if (IsValiduser)
                {
                    response.Key = true;
                    var objUser = new UserMst();
                    if (ValiduserBy == 1)
                    {
                        objUser = db.UserMsts.Where(i => i.UserName.ToLower() == UserName.ToLower()).FirstOrDefault();
                    }
                    else if (ValiduserBy == 2)
                    {
                        objUser = db.UserMsts.Where(i => i.EmailId.ToLower() == UserName.ToLower()).FirstOrDefault();
                    }
                    else if (ValiduserBy == 3)
                    {
                        objUser = db.UserMsts.Where(i => i.RefferalCode.ToLower() == UserName.ToLower()).FirstOrDefault();
                    }

                    if (objUser != null)
                    {
                        //var MobileMsg = "as per your forgot password request," + Environment.NewLine + " User Name : " + objUser.UserName + Environment.NewLine + ", Password : " + objUser.Password + Environment.NewLine + "Team maxener";
                        var MobileMsg = string.Format("Dear {0} , As per your forget details request .Please find the details as follow - user id no - {1} passcode -{2} user name - {3} . Team Maxener Wellness."
                                                     , objUser.FullName, objUser.RefferalCode, objUser.Password, objUser.UserName);

                        var EmailMsg = "as per your forgot password request,<br/> User Name : " + objUser.UserName + "<br/>Password : " + objUser.Password + Environment.NewLine + "<br/>Team maxener";

                        if (objUser.MobileNo.HasValue)
                        {
                            objManager.SendSMS(objUser.MobileNo, "1107160293306954626", MobileMsg, "Forgot Password");
                        }
                        try
                        {
                            response = objManager.SendMail(objUser.EmailId, "Forgot Password", EmailMsg);
                        }
                        catch (Exception ex)
                        {
                            ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                        if (response.Key)
                        {
                            response.Msg = "Password has been send to your emailid and Mobileno.";
                        }
                        else
                        {
                            response.Msg = "password not send successfully!";
                        }
                    }
                }
                else
                {
                    response.Key = false;
                    response.Msg = "Invalid Username or EmailId";
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #region Common Functions

        private bool CheckUserNameExistForTemp(string UserName, int Id = 0)
        {
            return db.TempUserMsts.Any(i => i.TempUserId != Id && i.UserName == UserName && i.IsActive);
        }

        private bool CheckEmailIdExistForTemp(string EmailId, int Id = 0)
        {
            return db.TempUserMsts.Any(i => i.TempUserId != Id && i.EmailId == EmailId && i.IsActive);
        }

        private bool CheckUserNameExist(string UserName, int Id = 0)
        {
            return db.UserMsts.Any(i => i.UserId != Id && i.UserName == UserName && i.IsActive);
        }

        private bool CheckEmailIdExist(string EmailId, int Id = 0)
        {
            return db.UserMsts.Any(i => i.UserId != Id && i.EmailId == EmailId && i.IsActive);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2").ToLower());
            }
            return result.ToString();
        }

        public string WelcomeEmail(string UserName, string Password, string PlanName)
        {
            string Msg = "";
            try
            {
                Msg = "Thank you For Joining Maxener Wellness with " + PlanName + " Plan.<br/>Your Credential as Below:<br/>UserName : " + UserName + "<br/>Password : " + Password + "<br/>Click here For <a href='" + HttpContext.Application[clsImplementationMessage.WebsiteURL].ToString() + "/Authentication/Authenticate' target='_blank'> Login <a/a><br/><br/>Regards,<br/>Maxener Wellness";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Msg;
        }

        public JsonResult AddProductToCart(List<SelectedProduct> productinfo, int pid)
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                double amount = 0;
                var pinfo = "";
                if (productinfo == null)
                {
                    response.Key = false;
                    response.Msg = "Please Purchase Atleast One Product!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                var productsId = productinfo.Select(i => i.ProductId);
                var listProducts = db.ProductMsts.Where(i => productsId.Contains(i.ProductId));
                foreach (var item in listProducts)
                {
                    var SelectedProdInfo = productinfo.FirstOrDefault(i => i.ProductId == item.ProductId);
                    if (pinfo.Length == 0)
                    {
                        pinfo = item.ProductId + "_" + SelectedProdInfo.Qty + "_" + SelectedProdInfo.ProductVariantId;
                    }
                    else
                    {
                        pinfo += "," + item.ProductId + "_" + SelectedProdInfo.Qty + "_" + SelectedProdInfo.ProductVariantId;
                    }
                    amount += (SelectedProdInfo.Qty * item.SalePrice.Value);
                }
                int SelectedPlan = Convert.ToInt32(pid);
                var objPlanMst = db.PlanMsts.Where(i => i.PlanId == SelectedPlan).FirstOrDefault();

                if (amount < objPlanMst.PlanAmount)
                {
                    response.Key = false;
                    response.Msg = "Please Purchase Atleast Amount of " + objPlanMst.PlanAmount.ToString() + "!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                pinfo = objManager.Encrypt(pinfo);
                response.Key = true;
                response.Msg = pinfo;
                response.Id = listProducts.Count();
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCartProductList(string p)
        {
            var Status = false;
            List<CartProducts> listCartProducts = new List<CartProducts>();
            try
            {
                p = p.Replace(" ", "+");
                var SplitProductInfo = objManager.Decrypt(p).Split(',');
                foreach (var products in SplitProductInfo)
                {
                    var productInfo = products.Split('_');
                    var prodid = Convert.ToInt32(productInfo[0]);
                    var qty = Convert.ToInt32(productInfo[1]);
                    var pvid = 0;
                    if (productInfo.Length > 2)
                    {
                        pvid = Convert.ToInt32(productInfo[2]);
                    }
                    listCartProducts.Add(new CartProducts
                    {
                        ProductId = prodid,
                        Qty = qty,
                        ProductVariantId = pvid
                    });
                    Status = true;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { Key = Status, Products = listCartProducts }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateMatchingIncome(int? UserId = 0, string FromDate = "", string ToDate = "")
        {
            ResponseMsg response = new ResponseMsg();
            DateTime? dtFromDate = null;
            DateTime? dtToDate = null;
            try
            {
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrWhiteSpace(FromDate))
                {
                    dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                }
                if (!string.IsNullOrEmpty(ToDate) && !string.IsNullOrWhiteSpace(ToDate))
                {
                    dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                }

                using (TRSEntities db = new TRSEntities(3600))
                {
                    var listUser = db.UserMsts.Where(i => i.UserId == (UserId == 0 ? i.UserId : UserId)).Select(i => i.UserId).ToList();
                    foreach (var NewUserId in listUser)
                    {
                        db.PROC_INSERT_MATCHING_INCOME_OF_USER(NewUserId, dtFromDate, dtToDate, null, null);
                    }
                }

                response.Key = true;
                response.Msg = "Matching Income Updated Successfully!";
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateGoldIncome(int? UserId = 0, string FromDate = "", string ToDate = "")
        {
            ResponseMsg response = new ResponseMsg();
            response.Key = true;
            response.Msg = "Gold Income is stopped !";

            //DateTime? dtFromDate = null;
            //DateTime? dtToDate = null;
            //try
            //{
            //    if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrWhiteSpace(FromDate))
            //    {
            //        dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
            //    }
            //    if (!string.IsNullOrEmpty(ToDate) && !string.IsNullOrWhiteSpace(ToDate))
            //    {
            //        dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
            //    }
            //    //var listUser = db.UserMsts.Where(i => i.UserId == (UserId == 0 ? i.UserId : UserId) && (i.IsGoldMember.HasValue && i.IsGoldMember.Value) && (!i.IsDimondMember.HasValue || (i.IsDimondMember.HasValue && !i.IsDimondMember.Value))).Select(i => i.UserId);
            //    //foreach (var NewUserId in listUser)
            //    //{
            //    //db.PROC_INSERT_GOLD_INCOME_OF_USER(0, UserId, dtFromDate, dtToDate);
            //    //}
            //    response.Key = true;
            //    response.Msg = "Gold Income Updated Successfully!";
            //}
            //catch (Exception ex)
            //{
            //    response.Key = false;
            //    response.Msg = ex.Message;
            //    ErrorSignal.FromCurrentContext().Raise(ex);
            //}
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateDiamondIncome(int? UserId = 0, string FromDate = "", string ToDate = "")
        {
            ResponseMsg response = new ResponseMsg();
            DateTime? dtFromDate = null;
            DateTime? dtToDate = null;
            try
            {
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrWhiteSpace(FromDate))
                {
                    dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                }
                if (!string.IsNullOrEmpty(ToDate) && !string.IsNullOrWhiteSpace(ToDate))
                {
                    dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                }
                //var listUser = db.UserMsts.Where(i => i.UserId == (UserId == 0 ? i.UserId : UserId) && (i.IsGoldMember.HasValue && i.IsGoldMember.Value) && (!i.IsDimondMember.HasValue || (i.IsDimondMember.HasValue && !i.IsDimondMember.Value))).Select(i => i.UserId);
                //foreach (var NewUserId in listUser)
                //{
                db.PROC_INSERT_DIAMOND_INCOME_OF_USER(0, UserId, dtFromDate, dtToDate);
                //}
                response.Key = true;
                response.Msg = "Diamond Income Updated Successfully!";
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateSilverIncome(int? UserId = 0, string FromDate = "", string ToDate = "")
        {
            ResponseMsg response = new ResponseMsg();
            DateTime? dtFromDate = null;
            DateTime? dtToDate = null;
            try
            {
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrWhiteSpace(FromDate))
                {
                    dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                }
                if (!string.IsNullOrEmpty(ToDate) && !string.IsNullOrWhiteSpace(ToDate))
                {
                    dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                }

                var affectedRows = db.PROC_INSERT_SILVER_INCOME_OF_USER(0, UserId, dtFromDate, dtToDate);
                if (affectedRows > 0)
                {
                    response.Key = true;
                    response.Msg = "Silver Income Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculatePrimeAssociateIncome(int? UserId = 0, string FromDate = "", string ToDate = "")
        {
            ResponseMsg response = new ResponseMsg();
            DateTime? dtFromDate = null;
            DateTime? dtToDate = null;
            try
            {
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrWhiteSpace(FromDate))
                {
                    dtFromDate = objManager.ConvertDDMMYYToMMDDYY(FromDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                }
                if (!string.IsNullOrEmpty(ToDate) && !string.IsNullOrWhiteSpace(ToDate))
                {
                    dtToDate = objManager.ConvertDDMMYYToMMDDYY(ToDate, DateFormats.DDMMYYYY.ToString(), DateFormats.MMDDYYYY.ToString());
                }

                var affectedRows = db.PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER(0, UserId, dtFromDate, dtToDate);
                if (affectedRows > 0)
                {
                    response.Key = true;
                    response.Msg = "Prime Member Income Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateGoldSilverIncome(int? UserId = 0, string FromDate = "", string ToDate = "")
        {
            ResponseMsg response = new ResponseMsg();
            var Msg = "";
            var IsMatchingIncomeSuccess = false;
            var IsGoldSuccess = false;
            var IsDiamondSuccess = false;
            var IsSilverSuccess = false;
            var IsPrimeAssociateSuccess = false;
            try
            {
                try
                {
                    var objMatching = CalculateMatchingIncome(UserId, FromDate, ToDate);
                    if (((ResponseMsg)objMatching.Data).Key)
                    {
                        IsMatchingIncomeSuccess = true;
                        Msg += ((ResponseMsg)objMatching.Data).Msg;
                    }
                    else
                    {
                        Msg = "UnSuccessfully Attemp For Matching Income!";
                    }
                }
                catch (Exception ex)
                {
                    Msg += "CalculateMatchingIncome : " + ex.Message + ".";
                }
                if (IsMatchingIncomeSuccess)
                {
                    try
                    {
                        var objDiamond = CalculateDiamondIncome(UserId, FromDate, ToDate);
                        if (((ResponseMsg)objDiamond.Data).Key)
                        {
                            Msg += ((ResponseMsg)objDiamond.Data).Msg;
                            IsDiamondSuccess = true;
                        }
                        else
                        {
                            Msg = "UnSuccessfully Attemp For Matching Income!";
                        }
                    }
                    catch (Exception ex)
                    {
                        Msg += "CalculateDiamondIncome : " + ex.Message + ".";
                    }

                    try
                    {
                        var objGold = CalculateGoldIncome(UserId, FromDate, ToDate);
                        if (((ResponseMsg)objGold.Data).Key)
                        {
                            Msg += ((ResponseMsg)objGold.Data).Msg;
                            IsGoldSuccess = true;
                        }
                        else
                        {
                            Msg = "UnSuccessfully Attemp For Matching Income!";
                        }
                    }
                    catch (Exception ex)
                    {
                        Msg += "CalculateGoldIncome : " + ex.Message + ".";
                    }

                    try
                    {
                        var objSilver = CalculateSilverIncome(UserId, FromDate, ToDate);
                        if (((ResponseMsg)objSilver.Data).Key)
                        {
                            Msg += ((ResponseMsg)objSilver.Data).Msg;
                            IsSilverSuccess = true;
                        }
                        else
                        {
                            Msg = "UnSuccessfully Attemp For Matching Income!";
                        }
                    }
                    catch (Exception ex)
                    {
                        Msg += "CalculateSilverIncome : " + ex.Message + ".";
                    }

                    try
                    {
                        var objPrime = CalculatePrimeAssociateIncome(UserId, FromDate, ToDate);
                        if (((ResponseMsg)objPrime.Data).Key)
                        {
                            Msg += ((ResponseMsg)objPrime.Data).Msg;
                            IsPrimeAssociateSuccess = true;
                        }
                        else
                        {
                            Msg = "UnSuccessfully Attemp For Matching Income!";
                        }
                    }
                    catch (Exception ex)
                    {
                        Msg += "CalculatePrimeAssociateIncome : " + ex.Message + ".";
                    }
                }
                if (IsGoldSuccess && IsPrimeAssociateSuccess && IsSilverSuccess && IsDiamondSuccess)
                {
                    if (db.ConfigMsts.Any(i => i.Key.ToUpper() == ConfigKeys.LastCalculateDate.ToUpper()))
                    {
                        var objConfig = db.ConfigMsts.Where(i => i.Key.ToUpper() == ConfigKeys.LastCalculateDate.ToUpper()).FirstOrDefault();
                        objConfig.Value = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value.ToString("yyyy-MM-dd");
                        db.SaveChanges();
                    }
                    else
                    {
                        var objConfig = new ConfigMst();
                        objConfig.CreatedBy = 1;
                        objConfig.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objConfig.IsActive = true;
                        objConfig.Key = ConfigKeys.LastCalculateDate.ToUpper();
                        objConfig.ModifiedBy = 1;
                        objConfig.ModifiedDate = objConfig.CreatedDate;
                        objConfig.Value = objConfig.CreatedDate.ToString("yyyy-MM-dd");
                        db.ConfigMsts.Add(objConfig);
                        db.SaveChanges();
                    }
                }
                response.Key = true;
                response.Msg = Msg;
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDBChanges()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                string fname = Server.MapPath("~/DBChanges.sql");
                string DBUpdates = System.IO.File.ReadAllText(fname);

                bool Status = ExecuteAnyStringQuery(DBUpdates);
                if (Status)
                {
                    response.Key = true;
                    response.Msg = "Database Update Successfully!";
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public bool ExecuteAnyStringQuery(string query)
        {
            DbTransaction transaction = null;
            bool Status = false;
            try
            {
                var ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                SqlConnection connection = new SqlConnection(ConnectionString);
                Microsoft.SqlServer.Management.Smo.Server server = new Microsoft.SqlServer.Management.Smo.Server(new Microsoft.SqlServer.Management.Common.ServerConnection(connection));
                server.ConnectionContext.ExecuteNonQuery(query);
                Status = true;
            }
            catch (SqlException)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw;
            }
            catch (Exception)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw;
            }
            return Status;
        }

        public JsonResult UpdateAllUserStatus()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                var MainUser = db.UserMsts.Where(i => i.RefferalUserId == 0).FirstOrDefault();
                var listParentMemberDtl = db.FN_GET_ALL_CHILD_USER_TREE(MainUser.UserId, true).Select(i => new ParentMemberDtl { FullName = i.FullName, UserId = i.UserId.Value }).ToList();
                try
                {
                    List<string> listConfigKeys = new List<string>();
                    listConfigKeys.Add(ConfigKeys.GoldIncomeBV);
                    listConfigKeys.Add(ConfigKeys.SilverIncomeBV);
                    listConfigKeys.Add(ConfigKeys.PrimeIncomeBV);
                    listConfigKeys.Add(ConfigKeys.DiamondIncomeBV);

                    var listConfig = objManager.GetConfigValues(listConfigKeys);
                    double GoldIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.GoldIncomeBV));
                    double SilverIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.SilverIncomeBV));
                    double PrimeIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.PrimeIncomeBV));
                    double DiamondIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.DiamondIncomeBV));

                    List<UserMatchingBVDtl> listUserMatchingBVDtl = new List<UserMatchingBVDtl>();
                    foreach (var item in listParentMemberDtl)
                    {
                        var objParentUserMst = db.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                        if ((!objParentUserMst.IsGoldMember.HasValue || !objParentUserMst.IsGoldMember.Value))
                        {
                            var MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                            listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });

                            if (MatchingBV >= GoldIncomeBV)
                            {
                                objParentUserMst.IsPrimeMember = false;
                                objParentUserMst.IsSilverMember = false;
                                objParentUserMst.IsDimondMember = false;
                                objParentUserMst.IsGoldMember = true;

                                var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)GoldIncomeBV).FirstOrDefault();
                                objParentUserMst.GoldMemberDate = dt;

                                db.SaveChanges();
                            }
                        }
                    }

                    foreach (var item in listParentMemberDtl)
                    {
                        var objParentUserMst = db.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                        if ((!objParentUserMst.IsDimondMember.HasValue || !objParentUserMst.IsDimondMember.Value))
                        {
                            double MatchingBV = 0;
                            if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                            {
                                MatchingBV = listUserMatchingBVDtl.FirstOrDefault(i => i.UserId == item.UserId).MatchingBV;
                            }
                            else
                            {
                                MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });
                            }
                            if (MatchingBV >= DiamondIncomeBV)
                            {
                                objParentUserMst.IsGoldMember = false;
                                objParentUserMst.IsPrimeMember = false;
                                objParentUserMst.IsSilverMember = false;
                                objParentUserMst.IsDimondMember = true;

                                var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)DiamondIncomeBV).FirstOrDefault();
                                objParentUserMst.DimondMemberDate = dt;

                                db.SaveChanges();
                            }
                        }
                    }

                    foreach (var item in listParentMemberDtl)
                    {
                        var objParentUserMst = db.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                        if (!objParentUserMst.IsPrimeMember.HasValue || !objParentUserMst.IsPrimeMember.Value)
                        {
                            double MatchingBV = 0;
                            if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                            {
                                MatchingBV = listUserMatchingBVDtl.FirstOrDefault(i => i.UserId == item.UserId).MatchingBV;
                            }
                            else
                            {
                                MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });
                            }
                            var listParentParentMemberDtl = db.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).Select(i => i.UserId.Value).ToArray();

                            if (MatchingBV >= PrimeIncomeBV
                                && db.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsGoldMember == true))
                            {
                                objParentUserMst.IsDimondMember = false;
                                objParentUserMst.IsGoldMember = false;
                                objParentUserMst.IsSilverMember = false;
                                objParentUserMst.IsPrimeMember = true;

                                var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)PrimeIncomeBV).FirstOrDefault();
                                objParentUserMst.PrimeMemberDate = dt;

                                db.SaveChanges();
                            }
                        }
                    }
                    foreach (var item in listParentMemberDtl)
                    {
                        var objParentUserMst = db.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                        if (!objParentUserMst.IsSilverMember.HasValue || !objParentUserMst.IsSilverMember.Value)
                        {
                            double MatchingBV = 0;
                            if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                            {
                                MatchingBV = listUserMatchingBVDtl.FirstOrDefault(i => i.UserId == item.UserId).MatchingBV;
                            }
                            else
                            {
                                MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                            }
                            var listParentParentMemberDtl = db.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).Select(i => i.UserId.Value).ToArray();

                            if (MatchingBV >= SilverIncomeBV
                                && db.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsDimondMember == true))
                            {
                                objParentUserMst.IsDimondMember = false;
                                objParentUserMst.IsGoldMember = false;
                                objParentUserMst.IsPrimeMember = false;
                                objParentUserMst.IsSilverMember = true;

                                var dt = db.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)SilverIncomeBV).FirstOrDefault();
                                objParentUserMst.SilverMemberDate = dt;
                                db.SaveChanges();
                            }
                        }
                    }
                    response.Key = true;
                    response.Msg = "All Users Status Updated Successfully!";
                }
                catch (Exception ex)
                {
                    response.Key = false;
                    response.Msg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetIPAddress()
        {
            ResponseMsg response = new ResponseMsg();
            try
            {
                string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipList))
                {
                    response.Msg = ipList.Split(',')[0];
                }
                response.Msg = Request.ServerVariables["REMOTE_ADDR"];
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion Common Functions

        public ActionResult CalculateAllIncomes()
        {
            return View();
        }

        public JsonResult GetNextUserForCalculation(int UserId)
        {
            var objUsers = db.UserMsts.OrderBy(i => i.UserId).FirstOrDefault(i => i.UserId > UserId);
            return Json(objUsers.UserId);
        }

        public JsonResult GetAllUsers()
        {
            var objUsers = db.UserMsts.Select(i => i.UserId).ToList();
            return Json(objUsers);
        }

        #region OTP Authenticate

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public ActionResult OTPAuthenticate()
        {
            try
            {
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        public LoginResponse SaveUserOTP(int userId, bool isReGenerate = false)
        {
            LoginResponse response = new LoginResponse();
            if (userId > 0)
            {
                try
                {
                    var objUser = db.UserMsts.Where(i => i.UserId == userId).FirstOrDefault();

                    var OTPValidMinutes = db.ConfigMsts.Where(p => p.Key == ConfigKeys.OTPValidMinutes).FirstOrDefault().Value;
                    double OTPMin = 60;
                    if (!double.TryParse(OTPValidMinutes, out OTPMin))
                        OTPMin = 60;

                    if (objUser.OTPValidUptoDateTime > DateTime.Now && !isReGenerate)
                    {
                        response.Key = true;
                        response.Msg = string.Format("Please use last generated OTP for the login process. Note: Generated OTP valid up to next {0} mins.", OTPMin);
                    }
                    else
                    {
                        objUser.OTPValidUptoDateTime = DateTime.Now.AddMinutes(OTPMin);
                        objUser.UserOTP = Convert.ToInt32(GenerateOTPNumber());
                        db.SaveChanges();

                        if (objUser.MobileNo != null && objUser.MobileNo > 0)
                        {
                            try
                            {
                                string smstextFormat = string.Format("Dear user your admin login OTP - {0} . Do not share with others. Maxener Wellness", objUser.UserOTP.ToString());
                                objManager.SendSMS(objUser.MobileNo, "1107160520527071215", smstextFormat, "Authentication OTP", true);
                                response.Key = true;
                                response.Msg = string.Format("OTP sent successfully on registered mobile number. Note: Generated OTP valid up to next {0} mins.", OTPMin);
                            }
                            catch (Exception ex)
                            {
                                ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }
                        else
                        {
                            response.Key = false;
                            response.Msg = "Authentication OTP hasn't generated due to invalid mobile number. Kindly contact with administrator!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    response.Key = false;
                    response.Msg = "An error occurred while trying to generate OTP";
                }
            }
            else
            {
                response.Key = false;
                response.Msg = "Invalid Username and Password.";
            }

            return response;
        }

        public JsonResult ReGenerateOTP()
        {
            LoginResponse response = new LoginResponse();
            try
            {
                clsLogin clsLogin = null;
                clsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                response = SaveUserOTP(clsLogin.UserId, true);
            }
            catch (Exception ex)
            {
                response.Key = false;
                response.Msg = "An error occurred while trying to generate OTP";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public string GenerateOTPNumber()
        {
            string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "123456789";
            string characters = numbers; //characters += alphabets + small_alphabets + ;

            int length = 6;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true), SessionExpireFilter]
        public JsonResult CheckOTPAuthenticate(int UserOTP)
        {
            LoginResponse response = new LoginResponse();
            clsLogin clsLogin = null;
            clsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];

            if (clsLogin != null)
            {
                var objUser = db.UserMsts.Where(i => i.UserId == clsLogin.UserId).FirstOrDefault();

                if (objUser.UserOTP == UserOTP && objUser.OTPValidUptoDateTime > DateTime.Now)
                {
                    response.Key = true;
                    response.Msg = Convert.ToString(objUser.UserType);

                    //IsOTPAuthenticate Property..
                    clsLogin = (clsLogin)Session[clsImplementationMessage.LoginInfo];
                    clsLogin.IsOTPAuthenticate = true;
                    System.Web.HttpContext.Current.Session[clsImplementationMessage.LoginInfo] = clsLogin;
                }
                else if (objUser.UserOTP == UserOTP && objUser.OTPValidUptoDateTime < DateTime.Now)
                {
                    response.Key = false;
                    response.Msg = "OTP is Expired. Please regenerate new OTP";
                }
                else if (objUser.UserOTP != UserOTP)
                {
                    response.Key = false;
                    response.Msg = "Invalid OTP. Please regenerate new OTP";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion OTP Authenticate
    }
}