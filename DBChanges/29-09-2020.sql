USE [AdvanceTRS_20Sep]
GO
IF OBJECT_ID('dbo.ServiceRunningStatus', 'U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[ServiceRunningStatus](
	[ID] [tinyint] IDENTITY(1,1) NOT NULL,
	[ServiceName] [varchar](500) NOT NULL,
	[RunningStatus] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ServiceRunningStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO
IF OBJECT_ID('dbo.Log4NetLog', 'U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Log4NetLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [varchar](4000) NOT NULL,
	[Exception] [varchar](2000) NULL,
 CONSTRAINT [PK_Log4NetLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO
IF OBJECT_ID('dbo.UserStatusUpdateQueue', 'U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[UserStatusUpdateQueue](
	[UserStatusUpdateQueueId] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderUserId] [int] NOT NULL,
	[UserPurchaseHdrId] [bigint] NOT NULL,
	[IsProcess] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_UserStatusUpdateQueue] PRIMARY KEY CLUSTERED 
(
	[UserStatusUpdateQueueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF COL_LENGTH('UserPurchaseDtl', 'IsCreditPointsProcess') IS NULL
BEGIN

	ALTER TABLE UserPurchaseDtl
	ADD IsCreditPointsProcess BIT DEFAULT 0;

END

GO
--IF COL_LENGTH('UserPurchaseDtl', 'IsCreditPointsProcess') IS NOT NULL
--BEGIN

--	--Set IsCreditPointsProcess:1 for Order Completed/Confirm
--	UPDATE dtl 
--		SET dtl.IsCreditPointsProcess = 1 
--	FROM 
--		UserPurchaseDtl dtl Inner join UserPurchaseHdr hdr ON dtl.UserPurchaseHdrId = hdr.UserPurchaseHdrId
--	WHERE 
--		hdr.OrderStatusId IN (4,5)
		
--	--Set IsCreditPointsProcess:0 for Order Generate
--	UPDATE dtl 
--		SET dtl.IsCreditPointsProcess = 0 
--	FROM 
--		UserPurchaseDtl dtl Inner join UserPurchaseHdr hdr ON dtl.UserPurchaseHdrId = hdr.UserPurchaseHdrId
--	WHERE 
--		hdr.OrderStatusId NOT IN (4,5)
		
--END

GO
IF NOT EXISTS (SELECT TOP 1 1 FROM Sys.Procedures WHERE name = 'PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL_BYSERVICE' AND type IN (N'P',N'PC'))
BEGIN
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL_BYSERVICE] AS'
END
GO
ALTER PROCEDURE [dbo].[PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL_BYSERVICE]                        
AS                  
BEGIN                  
             
 If(OBJECT_ID('tempdb..#OrderMst') Is Not Null)            
 BEGIN            
 Drop Table #OrderMst      
 End      
      
 If(OBJECT_ID('tempdb..#OrderDetail') Is Not Null)            
 BEGIN            
 Drop Table #OrderDetail      
 End      
         
 SELECT Distinct TOP 5 hdr.UserPurchaseHdrId          
    ,hdr.UserId        
 ,hdr.CreatedDate        
    ,hdr.IsRepurchaseOrder      
 ,hdr.OrderStatusId      
   INTO #OrderMst           
   FROM UserPurchaseHdr hdr Inner join UserPurchaseDtl dtl ON hdr.UserPurchaseHdrId = dtl.UserPurchaseHdrId          
      WHERE hdr.OrderStatusId IN (4,5) AND (dtl.IsCreditPointsProcess = 0  OR dtl.IsCreditPointsProcess IS NULL)          
 ORDER BY hdr.UserPurchaseHdrId DESC       
      
 SELECT p.RepurchaseMatchingAmt          
    ,p.RepurchaseMatchingPerc          
    ,p.DirectMatchingAmt          
    ,p.DirectMatchingPerc          
    ,dtl.Qty          
    ,dtl.UserPurchaseDtlId          
    ,hdr.UserPurchaseHdrId          
    ,hdr.UserId        
 ,hdr.CreatedDate        
    ,hdr.IsRepurchaseOrder          
    ,(CASE WHEN hdr.IsRepurchaseOrder = 1 THEN p.RepurchaseMatchingPerc ELSE p.DirectMatchingPerc END) AS CreditPerc          
 ,CAST((CASE WHEN hdr.IsRepurchaseOrder = 1 THEN CAST(((p.RepurchaseMatchingAmt * p.RepurchaseMatchingPerc)/100) AS bigint) * dtl.Qty ELSE CAST(((p.DirectMatchingAmt * p.DirectMatchingPerc)/100) AS bigint) * dtl.Qty END)AS decimal(18,2)) AS CreditPoints  
   INTO #OrderDetail           
   FROM #OrderMst hdr Inner join UserPurchaseDtl dtl ON hdr.UserPurchaseHdrId = dtl.UserPurchaseHdrId          
                Inner join ProductMst p ON p.ProductId = dtl.ProductId          
      WHERE hdr.OrderStatusId IN (4,5) AND (dtl.IsCreditPointsProcess = 0  OR dtl.IsCreditPointsProcess IS NULL)            
      ORDER BY hdr.UserPurchaseHdrId DESC         
        
 WHILE EXISTS (SELECT 1 FROM #OrderDetail)            
 BEGIN            
        
  DECLARE @OrderUserId int,              
    @UserPurchaseDtlId bigint,            
    @CreditPerc float,            
    @CreditPoints numeric(18,2),            
    @includeSelf BIT,        
    @OrderCreatedDate DATETIME,      
    @UserPurchaseHdrId BIGINT      
        
   SELECT TOP 1 @UserPurchaseHdrId = UserPurchaseHdrId,@OrderCreatedDate = CreatedDate , @OrderUserId = UserId, @UserPurchaseDtlId = UserPurchaseDtlId,@CreditPerc = CreditPerc, @CreditPoints=CreditPoints, @includeSelf = IsRepurchaseOrder         
    FROM #OrderDetail        
             
        
   INSERT INTO [dbo].[PointsCreditDtl]            
    ([UserId]            
    ,[UserPurchaseDtlId]            
    ,[CreditPerc]            
    ,[CreditDate]            
    ,[CreditPoints]            
    ,[IsActive]            
    ,[CreatedBy]            
    ,[CreatedDate])        
   SELECT t.UserId,@UserPurchaseDtlId,@CreditPerc,@OrderCreatedDate,@CreditPoints,1,@OrderUserId,GETDATE() FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@OrderUserId,@includeSelf) t        
   WHERE NOT EXISTS (SELECT TOP 1 1 FROM PointsCreditDtl pcd WHERE pcd.UserId = t.UserId AND pcd.UserPurchaseDtlId = @UserPurchaseDtlId)         
        
   UPDATE UserPurchaseDtl SET IsCreditPointsProcess = 1, ModifiedDate = GETDATE() WHERE UserPurchaseDtlId = @UserPurchaseDtlId        
           
   IF NOT EXISTS(SELECT TOP 1 1 FROM UserPurchaseDtl WHERE UserPurchaseHdrId = @UserPurchaseHdrId AND (IsCreditPointsProcess = 0 OR IsCreditPointsProcess IS NULL))      
   BEGIN      
		EXEC PROC_INSERT_ALL_PARENT_USER_NOTIFICATIONMST @OrderUserId,1,1
		--Insert record into UserStatusUpdateQueue tbl...
		INSERT INTO [dbo].[UserStatusUpdateQueue]
			   ([OrderUserId]
			   ,[UserPurchaseHdrId]
			   ,[CreatedBy]
			   ,[CreatedDate])
		 VALUES (@OrderUserId,@UserPurchaseHdrId,-1,GETDATE())
   END      
         
   DELETE FROM #OrderDetail WHERE UserPurchaseDtlId = @UserPurchaseDtlId        
        
 END            
          
END 

GO
IF NOT EXISTS (SELECT TOP 1 1 FROM Sys.Procedures WHERE name = 'PROC_GET_USER_UPDATE_ALLUSERSTATUS' AND type IN (N'P',N'PC'))
BEGIN
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [PROC_GET_USER_UPDATE_ALLUSERSTATUS] AS'
END
GO
ALTER Procedure [dbo].[PROC_GET_USER_UPDATE_ALLUSERSTATUS]              
AS              
BEGIN              
  
  SELECT TOP 100 UserStatusUpdateQueueId,OrderUserId FROM UserStatusUpdateQueue WHERE IsProcess IS NULL ORDER BY UserStatusUpdateQueueId ASC 

END
GO

ALTER Procedure [dbo].[PROC_GET_USER_ORDER_HDR]            
(            
@UserId int=0,            
@FranchiseeId int=0            
)            
AS            
BEGIN            
 IF @FranchiseeId is null            
 BEGIN            
  SET @FranchiseeId=0            
 END             
 --Declare @UserId int=9            
 Select uph.UserPurchaseHdrId,uph.OrderNo,uph.TotalOrderAmount,uph.OrderDate,uph.FranchiseeId,            
   uph.OrderStatusId,            
   --fm.FranchiseeName ,fm.MobileNo as FranchiseeMobileNo,osm.Name as StatusName,            
   '' as FranchiseeName,null as FranchiseeMobileNo,osm.Name as StatusName,            
   um.FullName as CustomerName,um.MobileNo as CustomerMobileNo,            
   case when len(ltrim(rtrim((ISNULL(uph.DeliveryAddress,'')))))>0 then uph.DeliveryAddress else um.Address end as CustomerAddress,            
   uph.CourierTrackingNo,ISNULL(IsRepurchaseOrder,1)IsRepurchaseOrder,            
   ISNULL(BusinessVolume,0)BusinessVolume,um.RefferalCode,CONVERT(varchar(20),uph.OrderDate,103) as OdrDate,            
   CASE WHEN ISNULL(IsRepurchaseOrder,1)=1 then 'Repurchase' ELSE 'Direct' END as OrderType,            
   uph.OrderReceivedById,orbm.OrderReceivedBy,RazorpayOrderId,RazorpayPaymentId,RazorpaySignature, ISNULL(uph.Notes,'')AS Notes,        
   uph.IsOrderPlaceByAdmin,    
   (CASE WHEN upd.CreditProcessCnt = upd.totalItem THEN 'Generated' Else 'Inprocess' END) AS CreditPointStatus    
 from UserPurchaseHdr as uph              
 INNER JOIN (            
   SELECT UserPurchaseHdrId,SUM(ISNULL(DirectPurchaseVolume,0)+ISNULL(RepurchaseMatchingVolume,0)) as BusinessVolume    
   ,SUM(CASE WHEN IsCreditPointsProcess = 1 THEN 1 ELSE 0 END)AS CreditProcessCnt    
   ,COUNT(1) AS totalItem    
   FROM UserPurchaseDtl            
   group by UserPurchaseHdrId            
 ) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId            
 INNER JOIN UserMst as um on uph.UserId=um.UserId            
 LEFT JOIN OrderStatusMst as osm on uph.OrderStatusId=osm.OrderStatusId            
 LEFT JOIN FranchiseeMst as fm on uph.FranchiseeId=fm.FranchiseeId            
 LEFT JOIN OrderReceivedByMst as orbm on uph.OrderReceivedById=orbm.OrderReceivedById            
 where uph.UserId=case when @UserId=0 then uph.UserId else @UserId end             
  and ISNULL(uph.FranchiseeId,0)=case when @FranchiseeId=0 then ISNULL(uph.FranchiseeId,0) else @FranchiseeId end            
END

GO

ALTER PROCEDURE [dbo].[PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL]          
(          
 @OrderUserId int,          
 @UserPurchaseDtlId bigint,        
 @CreditPerc float,        
 @CreditPoints numeric(18,2),        
 @includeSelf BIT        
)          
AS          
BEGIN          
          
 IF OBJECT_ID('tempdb..#TblPointsCreditDtl') IS NOT NULL        
 BEGIN        
  DROP TABLE #TblPointsCreditDtl        
 END        
         
         
 Create Table #TblPointsCreditDtl        
 (        
  UserId int        
  ,UserPurchaseDtlId bigint        
  ,CreditPerc float        
  ,CreditDate datetime        
  ,CreditPoints numeric(18,2)        
  ,IsActive bit        
  ,CreatedBy numeric(18,0)        
  ,CreatedDate datetime        
 )          
    
  Declare @OrderDatetime DATETIME  
  SELECT @OrderDatetime = CreatedDate FROM UserPurchaseDtl WHERE UserPurchaseDtlId = @UserPurchaseDtlId;  
        
  INSERT INTO #TblPointsCreditDtl        
  (UserId,UserPurchaseDtlId,CreditPerc,CreditDate,CreditPoints,IsActive,CreatedBy,CreatedDate)        
  SELECT UserId,@UserPurchaseDtlId,@CreditPerc,@OrderDatetime,@CreditPoints,1,@OrderUserId,GETDATE() FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@OrderUserId,@includeSelf)        
        
 INSERT INTO [dbo].[PointsCreditDtl]        
           ([UserId]        
           ,[UserPurchaseDtlId]        
           ,[CreditPerc]        
           ,[CreditDate]        
           ,[CreditPoints]        
           ,[IsActive]        
           ,[CreatedBy]        
           ,[CreatedDate])        
     SELECT t.[UserId]        
           ,t.[UserPurchaseDtlId]        
           ,t.[CreditPerc]        
           ,t.[CreditDate]        
           ,t.[CreditPoints]        
           ,t.[IsActive]        
           ,t.[CreatedBy]        
           ,t.[CreatedDate]        
  FROM #TblPointsCreditDtl t  
  WHERE NOT EXISTS (SELECT TOP 1 1 FROM [PointsCreditDtl] pcd WHERE pcd.UserId = t.UserId AND pcd.UserPurchaseDtlId = t.UserPurchaseDtlId)   
          
END     

GO

IF NOT EXISTS (SELECT TOP 1 1 FROM ServiceRunningStatus WHERE ServiceName = 'PointsCredit Process')
BEGIN
	INSERT INTO [dbo].[ServiceRunningStatus]
			   ([ServiceName]
			   ,[IsActive])
		 VALUES
			   ('PointsCredit Process'
			   ,1)
END
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM ServiceRunningStatus WHERE ServiceName = 'UpdateAllUserStatus Process')
BEGIN
		INSERT INTO [dbo].[ServiceRunningStatus]
			   ([ServiceName]
			   ,[IsActive])
		 VALUES
			   ('UpdateAllUserStatus Process'
			   ,1)
END
		   
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM ServiceRunningStatus WHERE ServiceName = 'UpdateOrderUserStatus Process')
BEGIN   
    INSERT INTO [dbo].[ServiceRunningStatus]
           ([ServiceName]
           ,[IsActive])
     VALUES
           ('UpdateOrderUserStatus Process'
           ,1)
END

GO


IF NOT EXISTS (SELECT TOP 1 1 FROM [ConfigMst] WHERE [Key] = 'POINTSCREDITDTLGENERATEWHILEPLACEORDER')
BEGIN

	INSERT INTO [dbo].[ConfigMst]
           ([Key]
           ,[Value]
           ,[IsActive]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[IsEditable]
           ,[Description])
     VALUES
           ('POINTSCREDITDTLGENERATEWHILEPLACEORDER'
           ,'false'
           ,1
           ,1
           ,GETDATE()
           ,1
           ,GETDATE()
           ,1
           ,'pointscreditdtl data generate while place order.')

END 

GO

ALTER PROC [dbo].[Temp_GetOrderData_DV]            
@startDate datetime = '2020/05/01',            
@endDate datetime='2020/06/07',            
@orderStatusId int=0,            
@isRepurchaseOrder int =0            
AS             
BEGIN            
          
 Select          
          
 uph.CreatedDate AS 'Order DateTime',          
 uph.OrderNo 'Reference No',          
 '' AS BV,          
 uph.RazorpayPaymentId AS PaymentId,          
 uph.CourierTrackingNo AS 'Courier Name',          
 '' AS [Zone],          
 '' AS 'Courier Charges Actual',          
 '' AS 'Courier Charges EST',          
 '' AS 'Prepaid Or COD',          
 '' AS 'Waybill',            
 um.FullName as 'Consignee Name',          
   case          
      when          
         ct.CityName IS NULL           
      then          
  (          
         select          
            TOP 1 CITY           
         from          
            [dbo].[Pincodes]           
         WHERE          
            PINCODE = um.PinCode)           
         else          
            ct.CityName           
   end          
   as 'City',          
   case          
      when          
         st.StateName IS NULL           
      then          
(          
         select          
            TOP 1 State           
         from          
            [dbo].[Pincodes]           
         WHERE          
            PINCODE = um.PinCode)           
         else          
            st.StateName           
   end          
   as 'State',          
   case          
      when          
         cm.CountryName IS NULL           
      then          
(          
         select          
            TOP 1 Country           
         from          
            [dbo].[Pincodes]           
         WHERE          
            PINCODE = um.PinCode)           
         else          
            cm.CountryName           
   end          
   as 'Country',          
   case          
      when          
         len(ltrim(rtrim((ISNULL(uph.DeliveryAddress, ''))))) > 0           
      then          
         uph.DeliveryAddress           
      else          
         um.Address           
   end          
   as [Address],           
   um.PinCode,           
   um.MobileNo as 'Phone',          
   um.AlternativeMobileNo as 'MobileNo',          
   '' AS [Weight],           
   CASE          
      WHEN          
         ISNULL(osm.OrderStatusId, 0) = 1           
      then          
         'COD'           
      WHEN          
         ISNULL(osm.OrderStatusId, 0) = 5           
      then          
         'Prepaid'           
      ELSE          
         osm.Name           
   END          
   as 'Payment Mode', 
   CASE          
      WHEN          
         uph.IsCODOrder = 1           
      then          
         'COD'           
      WHEN          
         uph.IsCODOrder = 0           
      then          
         'Prepaid'           
      ELSE          
         ''           
   END          
   as 'Order Payment Mode',
   '' AS PackageAmount,          
   (          
      CASE          
         WHEN          
            ISNULL(IsRepurchaseOrder, 1) = 1           
         THEN          
            'Repurchase'           
         ELSE          
            'Direct'           
      END          
   )          
   AS 'Order Type',           
   (          
      CASE          
         WHEN          
            ISNULL(osm.OrderStatusId, 0) = 1           
         THEN          
            CAST(uph.TotalOrderAmount AS VARCHAR(50))           
         ELSE          
            ''           
      END          
   )          
   AS 'Cod Amount',           
   '' AS 'Product to be Shipped',           
   'Maxener wellness , behind ujala avenue , nr. Dena bank , vishala circle , saarkhej narol road ahmedbad' AS 'Return Address',          
   '380055' AS 'Return Pin',           
   (          
      select          
         ISNULL(PV.VariantName, '') AS VariantName,          
         PM.ProductName,          
         ISNULL(PM.WeightGM, 0) AS WeightGM,          
         --ISNULL(PO.Qty, 0) AS Qty,          
   (CASE          
         WHEN          
            ISNULL(IsRepurchaseOrder, 1) = 1           
         THEN          
            ISNULL(PO.Qty*2, 0)           
         ELSE          
            ISNULL(PO.Qty, 0)           
   END) AS Qty,  
   ISNULL(DirectMatchingVolume,0)AS DirectMatchingVolume,          
   ISNULL(RepurchaseMatchingVolume,0)AS RepurchaseMatchingVolume           
   from          
         UserPurchaseDtl PO           
         INNER JOIN          
            ProductMst PM           
            ON PO.ProductId = PM.ProductId           
         LEFT JOIN          
            ProductVariantMst PV           
            ON PO.ProductVariantId = PV.ProductVariantId           
      WHERE          
         UserPurchaseHdrId = uph.UserPurchaseHdrId FOR JSON AUTO           
 )          
   AS ProductsDetails,  
   uph.Notes AS 'Order Notes'  
  FROM          
            UserPurchaseHdr as uph           
            INNER JOIN UserMst as um on uph.UserId = um.UserId           
            LEFT JOIN OrderStatusMst as osm on uph.OrderStatusId = osm.OrderStatusId           
            LEFT JOIN FranchiseeMst as fm on uph.FranchiseeId = fm.FranchiseeId           
            LEFT JOIN OrderReceivedByMst as orbm on uph.OrderReceivedById = orbm.OrderReceivedById           
            LEFT JOIN CityMst as ct on um.CityId = ct.CityId           
            LEFT JOIN StateMst as st on st.StateId = ct.StateId           
     LEFT JOIN CountryMst as cm on cm.CountryId = st.CountryId           
         WHERE          
            uph.OrderDate >= @startDate           
            and uph.OrderDate <= @endDate           
            AND uph.IsRepurchaseOrder =           
            CASE          
               WHEN          
                  @isRepurchaseOrder = 0           
               THEN          
                  uph.IsRepurchaseOrder           
               WHEN          
                  @isRepurchaseOrder = 1           
               THEN          
                  'TRUE'           
               WHEN          
                  @isRepurchaseOrder = 2           
               THEN          
                  'FALSE'           
               ELSE          
                  uph.IsRepurchaseOrder           
            END          
         ORDER BY          
            uph.OrderDate            
          
/*          
 --Declare @UserId int=9            
 Select '' AS 'Waybill', uph.OrderNo 'Reference No',um.FullName as 'Consignee Name',            
 --um.PinCode            
 case when ct.CityName IS NULL then (select TOP 1 CITY from [dbo].[Pincodes] WHERE PINCODE=um.PinCode) else ct.CityName end as 'City',            
 case when st.StateName IS NULL then (select TOP 1 State from [dbo].[Pincodes] WHERE PINCODE=um.PinCode) else st.StateName end as 'State',            
 case when cm.CountryName IS NULL then (select TOP 1 Country from [dbo].[Pincodes] WHERE PINCODE=um.PinCode) else cm.CountryName end as 'Country',             
 case when len(ltrim(rtrim((ISNULL(uph.DeliveryAddress,'')))))>0 then uph.DeliveryAddress else um.Address end as Address,            
 um.PinCode,um.MobileNo as 'Phone', '' AS Weight,             
 CASE WHEN ISNULL(osm.OrderStatusId,0)=1 then 'COD' WHEN ISNULL(osm.OrderStatusId,0)=5 then 'Prepaid'            
 ELSE osm.Name END as 'Payment Mode',            
 (CASE WHEN ISNULL(IsRepurchaseOrder,1)=1 THEN 'Repurchase' ELSE 'Direct' END) AS 'Order Type',            
  (CASE WHEN ISNULL(osm.OrderStatusId,0)=1 THEN CAST(uph.TotalOrderAmount AS VARCHAR(50)) ELSE '' END) AS 'Cod Amount',             
  '' AS 'Product to be Shipped',            
 'Maxener wellness , behind ujala avenue , nr. Dena bank , vishala circle , saarkhej narol road ahmedbad' AS 'Return Address', '380055' AS 'Return Pin',             
 (select ISNULL(PV.VariantName,'') AS VariantName, PM.ProductName, ISNULL(PM.WeightGM,0) AS WeightGM, ISNULL(PO.Qty,0) AS Qty from UserPurchaseDtl PO            
    INNER JOIN ProductMst PM ON PO.ProductId = PM.ProductId            
    LEFT JOIN ProductVariantMst PV ON PO.ProductVariantId = PV.ProductVariantId            
    WHERE UserPurchaseHdrId=uph.UserPurchaseHdrId FOR JSON AUTO            
 ) AS ProductsDetails             
 from UserPurchaseHdr as uph              
 --INNER JOIN (            
 --  SELECT UserPurchaseHdrId,SUM(ISNULL(DirectPurchaseVolume,0)+ISNULL(RepurchaseMatchingVolume,0)) as BusinessVolume             
 --  FROM UserPurchaseDtl            
 --  group by UserPurchaseHdrId            
 --) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId            
 INNER JOIN UserMst as um on uph.UserId=um.UserId            
 LEFT JOIN OrderStatusMst as osm on uph.OrderStatusId=osm.OrderStatusId            
 LEFT JOIN FranchiseeMst as fm on uph.FranchiseeId=fm.FranchiseeId            
 LEFT JOIN OrderReceivedByMst as orbm on uph.OrderReceivedById=orbm.OrderReceivedById            
 LEFT JOIN CityMst as ct on um.CityId =ct.CityId            
 LEFT JOIN StateMst as st on st.StateId =ct.StateId            
 LEFT JOIN CountryMst as cm on cm.CountryId =st.CountryId            
 WHERE uph.OrderDate >= @startDate and uph.OrderDate <= @endDate AND            
 uph.IsRepurchaseOrder= CASE             
                          WHEN @isRepurchaseOrder = 0 THEN uph.IsRepurchaseOrder            
        WHEN @isRepurchaseOrder = 1 THEN 'TRUE'            
        WHEN @isRepurchaseOrder = 2 THEN 'FALSE'            
                    ELSE uph.IsRepurchaseOrder END  ORDER BY uph.OrderDate   */           
              
END

GO

IF COL_LENGTH('UserPurchaseHdr', 'IsCODOrder') IS NULL
BEGIN
	ALTER TABLE UserPurchaseHdr
	ADD IsCODOrder BIT NULL
END