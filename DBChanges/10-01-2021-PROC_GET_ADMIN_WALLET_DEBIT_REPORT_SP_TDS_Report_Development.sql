ALTER Procedure [dbo].[PROC_GET_ADMIN_WALLET_DEBIT_REPORT]        
(        
 @UserId int = 0,    
 @FromDate DATE,    
 @ToDate DATE  
)        
AS        
BEGIN        
      
 If(OBJECT_ID('tempdb..#TblWallet') Is Not Null)                          
 BEGIN                          
 Drop Table #TblWallet                 
 End  
   
 If(OBJECT_ID('tempdb..#TblFinYearWallet') Is Not Null)                          
 BEGIN                          
 Drop Table #TblFinYearWallet                 
 End  
  
 If(OBJECT_ID('tempdb..#TempSumTbl') Is Not Null)                          
 BEGIN                          
 Drop Table #TempSumTbl                 
 End  
  
   
  
 Declare @finYearStartDt DATE,  
   @finYearEndDt DATE  
 select @finYearStartDt = DATEFROMPARTS(Yr, 4, 1), @finYearEndDt = DATEFROMPARTS(Yr + 1, 3, 31) from   
(select case when DATEPART(month, @FromDate) < 4 then DATEPART(year, @FromDate) - 1 else DATEPART(year, @FromDate) end Yr) a  
      
 Declare @TDSPercWithoutPAN decimal        
   ,@TDSPercWithPAN NUMERIC(18,2)        
  ,@TDSDexuctionLimit decimal        
 Select @TDSPercWithoutPAN = [Value] from ConfigMst where [Key]='TDSPercWithoutPAN'        
 Select @TDSPercWithPAN = [Value] from ConfigMst where [Key]='TDSPercWithPAN'        
 Select @TDSDexuctionLimit= [Value] from ConfigMst where [Key]='TDSDexuctionLimit'         
 SET @TDSPercWithoutPAN=ISNULL(@TDSPercWithoutPAN,20)        
 SET @TDSPercWithPAN=ISNULL(@TDSPercWithPAN,5)        
        
 Select wd.*,um.RefferalCode,um.FirstName,um.LastName,um.MiddleName,um.FullName,um.RegistrationDate,        
   BankName,BankAccNo, CONVERT(varchar(11),DebitDate,103) as PayoutDate,        
   um.MobileNo,um.EmailId,PANNo,  
   CAST(0 as float) NetPayable_Year,  
   CAST(0 as float) TDSAmount_Year  
   INTO #TblFinYearWallet        
 from WalletDebitDtl as wd        
 INNER JOIN UserMst as um on wd.UserId = um.UserId    
 WHERE wd.DebitDate BETWEEN @finYearStartDt AND @finYearEndDt    
 order by DebitDate desc        
      
 UPDATE pd         
 SET         
  TDSPerc = case when len(PANNo) > 0 then @TDSPercWithPAN else @TDSPercWithoutPAN end         
 FROM #TblFinYearWallet as pd       
      
 Update pd set TDSAmount = (CASE WHEN DebitAmount > @TDSDexuctionLimit         
        THEN ((DebitAmount) * TDSPerc/100 )         
       ELSE 0         
        END)        
 FROM #TblFinYearWallet as pd    
   
 SELECT UserId,sum(NetPayable)AS NetPayable,sum(TDSAmount) AS TDSAmount INTO #TempSumTbl FROM #TblFinYearWallet group BY UserId  
  
 UPDATE pd SET NetPayable_Year = t.NetPayable, TDSAmount_Year = CONVERT(DECIMAL(10, 2),t.TDSAmount)  
 FROM #TblFinYearWallet pd INNER JOIN #TempSumTbl t ON pd.UserId = t.UserId  
  
 SELECT * INTO #TblWallet FROM #TblFinYearWallet WHERE DebitDate BETWEEN @FromDate AND @ToDate    
 ORDER BY DebitDate desc         
  
  SELECT * FROM 
 (SELECT WalletDebitDtlId    
  ,UserId    
  ,DebitDate    
  ,DebitAmount    
  ,UTRRefNo    
  ,AccountNo    
  ,ISFCCode   
  ,TDSAmount_Year  
  ,TDSAmount    
  ,TDSPerc    
  ,AdminCharge    
  ,AdminPerc    
  ,NetPayable_Year  
  ,NetPayable  
  ,IsActive    
  ,CreatedBy    
  ,CreatedDate    
  ,DirectIncomePay    
  ,PointsIncomePay    
  ,VoltIncomePay    
  ,LeaderIncomePay    
  ,UplineSupportIncomePay    
  ,SilverIncomePay    
  ,DiamondIncomePay,RefferalCode,FirstName,LastName,MiddleName,FullName,RegistrationDate,        
  BankName,BankAccNo, CONVERT(varchar(11),DebitDate,103) as PayoutDate,        
  MobileNo,EmailId,PANNo      
 FROM #TblWallet)AS tbl    
      
 -- Select wd.*,um.RefferalCode,um.FirstName,um.LastName,um.MiddleName,um.FullName,um.RegistrationDate,        
 --  BankName,BankAccNo, CONVERT(varchar(11),DebitDate,103) as PayoutDate,        
 --  um.MobileNo,um.EmailId,PANNo        
 --from WalletDebitDtl as wd        
 --INNER JOIN UserMst as um on wd.UserId=um.UserId        
 --order by DebitDate desc        
      
      
END      