USE [maxener]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_KYC_DETAILS]    Script Date: 01/30/2021 11:30:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[PROC_GET_KYC_DETAILS]
(
@Page TINYINT,
@Size INT,
@KYCType varchar(20),
@UserId varchar(100) = null,
@FirstName varchar(100) = null,
@MiddleName varchar(100) = null,
@LastName varchar(100) = null,
@PanNo varchar(100) = null,
@BankName varchar(100) = null,
@BankAcc varchar(100) = null,
@BankIFSC varchar(100) = null,
@BankingName varchar(100) = null,
@MobileNo VARCHAR(100) = null,
@TotalCount varchar(50) output
)
AS
BEGIN

	If(OBJECT_ID('tempdb..#tblUserMst') Is Not Null)                
	BEGIN                
		Drop Table #tblUserMst          
	End    

	SELECT um.*,
		rum.RefferalCode as RefferalByCode,rum.FullName as RefferalByName,
		 CONVERT(varchar, um.ActivateDate, 103) as ActivationDate
		INTO #tblUserMst
	FROM UserMst as um
	INNER JOIN UserMst as rum on um.RefferalUserId = rum.UserId
	WHERE 1 = 1
	AND
	((@KYCType = 'applied' AND ((um.KYCImage IS NOT NULL AND um.IsKYCVerified IS NULL) OR (um.KYCBackImage IS NOT NULL AND um.IsKYCVerified IS NULL) OR (um.BankDocument IS NOT NULL AND um.IsBankDocumentVerified IS NULL) OR (um.PANImage IS NOT NULL AND um.IsPANVerified IS NULL)))
	OR
	(@KYCType = 'confirmed' AND (um.IsKYCVerified = 1 AND um.IsBankDocumentVerified = 1 AND um.IsPANVerified = 1))
	OR
	(@KYCType = 'rejected' AND ((um.IsKYCVerified = 0 AND um.KYCRejectComment IS NOT NULL) OR (um.IsBankDocumentVerified = 0 AND um.BankDocumentRejectComment IS NOT NULL) OR (um.IsPANVerified = 0 AND um.PANRejectComment IS NOT NULL))))
	AND (NULLIF(@UserId, '') IS NULL OR um.UserId like '%'+@UserId+'%')
	AND (NULLIF(@FirstName, '') IS NULL OR um.FirstName like '%'+@FirstName+'%')
	AND (NULLIF(@MiddleName, '') IS NULL OR um.MiddleName like '%'+@MiddleName+'%')
	AND (NULLIF(@LastName, '') IS NULL OR um.LastName like '%'+@LastName+'%')
	AND (NULLIF(@PanNo, '') IS NULL OR um.PANNo like '%'+@PanNo+'%')
	AND (NULLIF(@BankName, '') IS NULL OR um.BankName like '%'+@BankName+'%')
	AND (NULLIF(@BankAcc, '') IS NULL OR um.BankAccNo like '%'+@BankAcc+'%')
	AND (NULLIF(@BankIFSC, '') IS NULL OR um.ISFCCode like '%'+@BankIFSC+'%')
	AND (NULLIF(@BankingName, '') IS NULL OR um.BankingName like '%'+@BankingName+'%')
	AND (NULLIF(@MobileNo, '') IS NULL OR um.MobileNo like '%'+@MobileNo+'%')
	

	SELECT @TotalCount = COUNT(1) FROM #tblUserMst
	
    SELECT * FROM #tblUserMst
	ORDER BY RegistrationDate DESC
	OFFSET (@Page * @Size) ROWS
	FETCH NEXT @Size ROWS ONLY 

	SELECT @TotalCount

END
