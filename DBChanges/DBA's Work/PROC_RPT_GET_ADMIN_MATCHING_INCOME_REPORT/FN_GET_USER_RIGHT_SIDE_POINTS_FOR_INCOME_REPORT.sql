USE [stage_db]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_FOR_INCOME_REPORT]    Script Date: 12/27/2020 12:42:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--SELECT * FROM [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_FOR_INCOME_REPORT] (1,11)
CREATE Function [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_FOR_INCOME_REPORT]
(
@UserId int,
@RightUserId int
)
RETURNS @CreditDtl table(RightPoints numeric(18,0), CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max),RightBV float)
AS
BEGIN

	;WITH DirectReports_IN  AS
		(  
			SELECT LeftUserId, RightUserId, UserId
			FROM dbo.UserMst   
			WHERE UserId =  @RightUserId
			UNION ALL  
			SELECT e.LeftUserId, e.RightUserId, e.UserId
			FROM dbo.UserMst AS e  
				INNER JOIN DirectReports_IN AS d  
				ON (e.UserId = d.LeftUserId) or (e.UserId = d.RightUserId)
		), 
		DirectReports AS (
		Select @UserId as UserId		
		UNION 
		Select UserId from DirectReports_IN 
		)	
	insert into @CreditDtl
	Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='L' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as RightPoints,
			pcd.CreditDate,um.UserId,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='L' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end)RightBV	
	from DirectReports as ru	
	INNER JOIN dbo.UserPurchaseHdr as uph on ru.UserId=uph.CreatedBy
	INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN dbo.UserMst as um on ru.UserId=um.UserId
	Where pcd.UserId=@UserId			
	group by pcd.CreditDate,um.UserId,um.RefferalCode
	OPTION (maxrecursion 0);

	RETURN
END
GO

