USE [stage_db]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_FOR_INCOME_REPORT]    Script Date: 12/27/2020 12:41:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_FOR_INCOME_REPORT]
(
@UserId int,
@LeftUserId int
)
RETURNS @CreditDtl table(LeftPoints numeric(18,2), CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max),LeftBV float)
AS
BEGIN

	;WITH DirectReports_IN  AS
		(  
			SELECT LeftUserId, RightUserId, UserId
			FROM dbo.UserMst   
			WHERE UserId =  @LeftUserId
			UNION ALL  
			SELECT e.LeftUserId, e.RightUserId, e.UserId
			FROM dbo.UserMst AS e  
				INNER JOIN DirectReports_IN AS d  
				ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
		), 
		DirectReports AS (
		Select @UserId as UserId		
		UNION 
		Select UserId from DirectReports_IN 
		)	
	insert into @CreditDtl
	Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as LeftPoints,
			pcd.CreditDate,lu.UserId,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV			
	from DirectReports as lu	
	INNER JOIN dbo.UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
	INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN dbo.UserMst as um on lu.UserId=um.UserId
	Where pcd.UserId=@UserId			
	group by pcd.CreditDate,lu.UserId,um.RefferalCode
	OPTION (maxrecursion 0);

	RETURN
END
GO

