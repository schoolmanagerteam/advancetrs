USE [stage_db]
GO

/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_DBA]    Script Date: 12/27/2020 12:40:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT '2019-09-20','2020-02-26'
--exec PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT '2019-09-20','2019-12-26'
ALTER Procedure [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_DBA]
(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	--Declare @FromDate date='2019-09-20'
	--Declare @ToDate date='2020-12-26'
	--Declare @UserId int=0
		SET FMTONLY OFF

	DECLARE @TotalLeftNN INT
	DECLARE @counter INT = 1
	DECLARE @MainUserId INT, @LeftUserId INT, @RightUserId INT 
	Select 0 as UserId,
	cast(0 as float) as Income,cast(0 as float) as BV,cast(0 as float) as TotalIncome,cast(0 as float) as TotalBV
	into #tmpMatchingDtl
	
	delete from #tmpMatchingDtl Where UserId=0
	
	CREATE TABLE #LoopLR
 (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	MainUserId INT,
	LeftUserId INT,
	RightUserId INT
 )

	INSERT INTO #LoopLR (MainUserId, LeftUserId, RightUserId)
	Select top(20) UserId , LeftUserId, RightUserId
	from dbo.UserMst 
	Where UserId=case when @UserId=0 then UserId else @UserId end
	ORDER BY 1

	 SET @TotalLeftNN = @@ROWCOUNT
	

	 WHILE (@counter < @TotalLeftNN)	
	BEGIN  

		SELECT 
			@MainUserId = MainUserId,			
			@LeftUserId = LeftUserId,
			@RightUserId = RightUserId
		FROM #LoopLR 
		WHERE Id = @counter
		
			Declare @TotalLeftIncome float
			Declare @TotalLeftBV float
			Declare @LeftIncome float
			Declare @LeftBV float
			Declare @TotalRightIncome float
			Declare @TotalRightBV float
			Declare @RightIncome float
			Declare @RightBV float
			
			Select	@TotalLeftIncome=SUM(LeftPoints),
					@TotalLeftBV=SUM(LeftBV), 		
					@LeftIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftPoints end),
					@LeftBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftBV end)
			from [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_FOR_INCOME_REPORT](@MainUserId,@LeftUserId)
			
			Select	@TotalRightIncome=SUM(RightPoints),
					@TotalRightBV=SUM(RightBV), 		
					@RightIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightPoints end),
					@RightBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightBV end)		
			from [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_FOR_INCOME_REPORT](@MainUserId,@RightUserId)
			
			Declare @FinalIncome float=case when ISNULL(@LeftIncome,0)>=ISNULL(@RightIncome,0) then ISNULL(@RightIncome,0) ELSE ISNULL(@LeftIncome,0) END
			Declare @FinalBV float=case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END
			Declare @FinalTotalIncome float=case when ISNULL(@TotalLeftIncome,0)>=ISNULL(@TotalRightIncome,0) then ISNULL(@TotalRightIncome,0) ELSE ISNULL(@TotalLeftIncome,0) END
			Declare @FinalTotalBV float=case when ISNULL(@TotalLeftBV,0)>=ISNULL(@TotalRightBV,0) then ISNULL(@TotalRightBV,0) ELSE ISNULL(@TotalLeftBV,0) END
			
			insert into #tmpMatchingDtl
			Select @MainUserId,
					@FinalIncome as Income,@FinalBV as BV,@FinalTotalIncome as TotalIncome,@FinalTotalBV as TotalBV				
			
			SET @counter = @counter + 1		  
	END 

	CLOSE db_match_bv  
	DEALLOCATE db_match_bv 

	Select t.UserId,um.UserName, um.RefferalCode,um.FullName,cast(t.Income as numeric(18,0))Income,cast(t.BV as numeric(18,0))BV,
			cast(t.TotalIncome as numeric(18,0))TotalIncome,cast(t.TotalBV as numeric(18,0))TotalBV 
	from #tmpMatchingDtl as t
	INNER JOIN dbo.UserMst um ON um.UserId = t.UserId
	
	drop table #tmpMatchingDtl
	drop table #LoopLR
END
GO

