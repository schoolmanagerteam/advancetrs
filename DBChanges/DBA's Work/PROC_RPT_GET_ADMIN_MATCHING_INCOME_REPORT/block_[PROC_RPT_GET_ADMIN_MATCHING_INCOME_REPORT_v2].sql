USE [stage_db]
GO
/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT]    Script Date: 12/26/2020 10:15:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT '2019-09-20','2020-02-26'
--exec PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT '2019-09-20','2019-12-26'
--CREATE Procedure [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_v2]
--(
@FromDate date=null,
@ToDate date=null,
@UserId int=0
)
AS
BEGIN
	--Declare @FromDate date='2019-09-20'
	--Declare @ToDate date='2020-12-26'
	--Declare @UserId int=0
		SET FMTONLY OFF

	Select 0 as UserId,cast('' as nvarchar(max)) as UserName,cast('' as nvarchar(max)) as RefferalCode,cast('' as nvarchar(max)) as FullName,
	cast(0 as float) as Income,cast(0 as float) as BV,cast(0 as float) as TotalIncome,cast(0 as float) as TotalBV
	into #tmpMatchingDtl
	
	delete from #tmpMatchingDtl Where UserId=0
	Declare @TempUserId int

	DECLARE db_match_bv CURSOR FOR 
	Select TOP(20) UserId , LeftUserId, RightUserId
	INTO #UserIds
	from UserMst 
	ORDER BY 1
	Where UserId=case when @UserId=0 then UserId else @UserId end
	OPEN db_match_bv  
	FETCH NEXT FROM db_match_bv INTO @TempUserId  

	--SELECT COUNT(1) FROM UserMst WHERE LeftUserId is null and RightUserId is null

	SELECT * FROM #UserIds



	;WITH DirectReports AS   
	(  
		SELECT LeftUserId, RightUserId, UserId
		FROM dbo.UserMst   
		WHERE UserId =10
		UNION ALL  
		SELECT e.LeftUserId, e.RightUserId, e.UserId
		FROM dbo.UserMst AS e  
			INNER JOIN DirectReports AS d  
			ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
	)  	
	Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as LeftPoints,
			--pcd.CreditDate,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV
			 
			--SUM(CreditPoints) as LeftPoints,pcd.CreditDate,lu.FullName,um.RefferalCode
	from (
	SELECT UserId
	FROM DirectReports  
	UNION 
	SELECT 10 as UserId) as lu
	--INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId
	INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on lu.UserId=um.UserId
	Where pcd.UserId=1			
	--group by pcd.CreditDate,lu.FullName,um.RefferalCode
	
	OPTION (maxrecursion 0);



	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		
			Declare @TotalLeftIncome float
			Declare @TotalLeftBV float
			Declare @LeftIncome float
			Declare @LeftBV float
			Declare @TotalRightIncome float
			Declare @TotalRightBV float
			Declare @RightIncome float
			Declare @RightBV float
			
			Select	@TotalLeftIncome=SUM(LeftPoints),
					@TotalLeftBV=SUM(LeftBV), 		
					@LeftIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftPoints end),
					@LeftBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftBV end)
			from FN_GET_USER_LEFT_SIDE_POINTS(@TempUserId,default,default)
			
			Select	@TotalRightIncome=SUM(RightPoints),
					@TotalRightBV=SUM(RightBV), 		
					@RightIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightPoints end),
					@RightBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightBV end)		
			from FN_GET_USER_RIGHT_SIDE_POINTS(@TempUserId,default,default)
			
			Declare @FinalIncome float=case when ISNULL(@LeftIncome,0)>=ISNULL(@RightIncome,0) then ISNULL(@RightIncome,0) ELSE ISNULL(@LeftIncome,0) END
			Declare @FinalBV float=case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END
			Declare @FinalTotalIncome float=case when ISNULL(@TotalLeftIncome,0)>=ISNULL(@TotalRightIncome,0) then ISNULL(@TotalRightIncome,0) ELSE ISNULL(@TotalLeftIncome,0) END
			Declare @FinalTotalBV float=case when ISNULL(@TotalLeftBV,0)>=ISNULL(@TotalRightBV,0) then ISNULL(@TotalRightBV,0) ELSE ISNULL(@TotalLeftBV,0) END
			
				insert into #tmpMatchingDtl
				Select @TempUserId as UserId,um.UserName,um.RefferalCode,um.FullName,
						@FinalIncome as Income,@FinalBV as BV,@FinalTotalIncome as TotalIncome,@FinalTotalBV as TotalBV
				From UserMst as um
				Where UserId=@TempUserId
			
		  FETCH NEXT FROM db_match_bv INTO @TempUserId 
	END 

	CLOSE db_match_bv  
	DEALLOCATE db_match_bv 

	Select UserId,UserName,RefferalCode,FullName,cast(t.Income as numeric(18,0))Income,cast(t.BV as numeric(18,0))BV,
			cast(t.TotalIncome as numeric(18,0))TotalIncome,cast(t.TotalBV as numeric(18,0))TotalBV 
	from #tmpMatchingDtl as t
	drop table #tmpMatchingDtl
END