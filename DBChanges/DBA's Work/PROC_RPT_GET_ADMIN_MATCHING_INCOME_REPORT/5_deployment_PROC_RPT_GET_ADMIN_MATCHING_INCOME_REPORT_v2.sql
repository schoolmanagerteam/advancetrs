USE [stage_db]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_FOR_INCOME_REPORT]    Script Date: 12/27/2020 1:20:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_FOR_INCOME_REPORT]
(
@UserId int,
@LeftUserId int
)
RETURNS @CreditDtl table(LeftPoints numeric(18,2), CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max),LeftBV float)
AS
BEGIN

	;WITH DirectReports_IN  AS
		(  
			SELECT LeftUserId, RightUserId, UserId
			FROM dbo.UserMst   
			WHERE UserId =  @LeftUserId
			UNION ALL  
			SELECT e.LeftUserId, e.RightUserId, e.UserId
			FROM dbo.UserMst AS e  
				INNER JOIN DirectReports_IN AS d  
				ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
		), 
		DirectReports AS (
		Select @UserId as UserId		
		UNION 
		Select UserId from DirectReports_IN 
		)	
	insert into @CreditDtl
	Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as LeftPoints,
			pcd.CreditDate,lu.UserId,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV			
	from DirectReports as lu	
	INNER JOIN dbo.UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
	INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN dbo.UserMst as um on lu.UserId=um.UserId
	Where pcd.UserId=@UserId			
	group by pcd.CreditDate,lu.UserId,um.RefferalCode
	OPTION (maxrecursion 0);

	RETURN
END
GO

USE [stage_db]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_FOR_INCOME_REPORT]    Script Date: 12/27/2020 1:20:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--SELECT * FROM [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_FOR_INCOME_REPORT] (1,11)
CREATE Function [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_FOR_INCOME_REPORT]
(
@UserId int,
@RightUserId int
)
RETURNS @CreditDtl table(RightPoints numeric(18,0), CreditDate datetime, FullName nvarchar(max),RefferalCode nvarchar(max),RightBV float)
AS
BEGIN

	;WITH DirectReports_IN  AS
		(  
			SELECT LeftUserId, RightUserId, UserId
			FROM dbo.UserMst   
			WHERE UserId =  @RightUserId
			UNION ALL  
			SELECT e.LeftUserId, e.RightUserId, e.UserId
			FROM dbo.UserMst AS e  
				INNER JOIN DirectReports_IN AS d  
				ON (e.UserId = d.LeftUserId) or (e.UserId = d.RightUserId)
		), 
		DirectReports AS (
		Select @UserId as UserId		
		UNION 
		Select UserId from DirectReports_IN 
		)	
	insert into @CreditDtl
	Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='L' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as RightPoints,
			pcd.CreditDate,um.UserId,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='L' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end)RightBV	
	from DirectReports as ru	
	INNER JOIN dbo.UserPurchaseHdr as uph on ru.UserId=uph.CreatedBy
	INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN dbo.UserMst as um on ru.UserId=um.UserId
	Where pcd.UserId=@UserId			
	group by pcd.CreditDate,um.UserId,um.RefferalCode
	OPTION (maxrecursion 0);

	RETURN
END
GO

USE [stage_db]
GO

/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_v2]    Script Date: 12/27/2020 1:21:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_v2]
(
@FromDate date=NULL,
@ToDate date=NULL,
@UserId int=0
)
AS
BEGIN

	SET FMTONLY OFF;
	SET NOCOUNT ON;
	SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @TotalLeftNN INT
	DECLARE @counter INT = 1
	DECLARE @MainUserId INT, @LeftUserId INT, @RightUserId INT 
	
	SELECT	0 as UserId,
			cast(0 as float) as Income,cast(0 as float) as BV,
			cast(0 as float) as TotalIncome,cast(0 as float) as TotalBV
	INTO #tmpMatchingDtl
	
	DELETE FROM #tmpMatchingDtl WHERE UserId=0
	
	CREATE TABLE #LoopLR
	 (
		Id INT IDENTITY(1,1) PRIMARY KEY,
		MainUserId INT,
		LeftUserId INT,
		RightUserId INT
	 )

	INSERT INTO #LoopLR (MainUserId, LeftUserId, RightUserId)
	Select top(500) UserId , LeftUserId, RightUserId
	from dbo.UserMst 
	Where UserId=case when @UserId=0 then UserId else @UserId end
	ORDER BY 1 

	 SET @TotalLeftNN = @@ROWCOUNT
	

	 WHILE (@counter <= @TotalLeftNN)	
	BEGIN  

		SELECT 
			@MainUserId = MainUserId,			
			@LeftUserId = LeftUserId,
			@RightUserId = RightUserId
		FROM #LoopLR 
		WHERE Id = @counter
		
			DECLARE @TotalLeftIncome float
			DECLARE @TotalLeftBV float
			DECLARE @LeftIncome float
			DECLARE @LeftBV float
			DECLARE @TotalRightIncome float
			DECLARE @TotalRightBV float
			DECLARE @RightIncome float
			DECLARE @RightBV float
			
			SELECT	@TotalLeftIncome=SUM(LeftPoints),
					@TotalLeftBV=SUM(LeftBV), 		
					@LeftIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftPoints end),
					@LeftBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then LeftBV end)
			FROM [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_FOR_INCOME_REPORT](@MainUserId,@LeftUserId)
			
			SELECT	@TotalRightIncome=SUM(RightPoints),
					@TotalRightBV=SUM(RightBV), 		
					@RightIncome=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightPoints end),
					@RightBV=SUM(case when (CAST(CreditDate as date)>=@FromDate or @FromDate is null) and (CAST(CreditDate as date)<=@ToDate or @ToDate is null) then RightBV end)		
			FROM [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_FOR_INCOME_REPORT](@MainUserId,@RightUserId)
			
			Declare @FinalIncome float=case when ISNULL(@LeftIncome,0)>=ISNULL(@RightIncome,0) then ISNULL(@RightIncome,0) ELSE ISNULL(@LeftIncome,0) END
			Declare @FinalBV float=case when ISNULL(@LeftBV,0)>=ISNULL(@RightBV,0) then ISNULL(@RightBV,0) ELSE ISNULL(@LeftBV,0) END
			Declare @FinalTotalIncome float=case when ISNULL(@TotalLeftIncome,0)>=ISNULL(@TotalRightIncome,0) then ISNULL(@TotalRightIncome,0) ELSE ISNULL(@TotalLeftIncome,0) END
			Declare @FinalTotalBV float=case when ISNULL(@TotalLeftBV,0)>=ISNULL(@TotalRightBV,0) then ISNULL(@TotalRightBV,0) ELSE ISNULL(@TotalLeftBV,0) END
			
			insert into #tmpMatchingDtl
			Select @MainUserId,
					@FinalIncome as Income,@FinalBV as BV,@FinalTotalIncome as TotalIncome,@FinalTotalBV as TotalBV				
			
			SET @counter = @counter + 1		  
	END 

	SELECT t.UserId,um.UserName, um.RefferalCode,um.FullName,cast(t.Income as numeric(18,0))Income,cast(t.BV as numeric(18,0))BV,
			cast(t.TotalIncome as numeric(18,0))TotalIncome,cast(t.TotalBV as numeric(18,0))TotalBV 
	FROM #tmpMatchingDtl as t
	INNER JOIN dbo.UserMst um ON um.UserId = t.UserId
	
	DROP TABLE #tmpMatchingDtl
	DROP TABLE #LoopLR
END
GO

