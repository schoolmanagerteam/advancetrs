GO
IF NOT EXISTS (SELECT TOP 1 1 FROM Sys.Procedures WHERE name = 'FN_GET_ALL_PARENT_USER_TREE_SP' AND type IN (N'P',N'PC'))
BEGIN
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [FN_GET_ALL_PARENT_USER_TREE_SP] AS'
END
GO
ALTER PROCEDURE [dbo].[FN_GET_ALL_PARENT_USER_TREE_SP]                    
(                    
 @UserId int,                    
 @IncludeSelf bit = 0                    
)                    
AS                    
BEGIN                    
              
  If(OBJECT_ID('tempdb..#TblResult') Is Not Null)              
  Begin              
   Drop Table #TblResult              
  End              
         
  If(OBJECT_ID('tempdb..#TblUserMst') Is Not Null)              
  Begin              
   Drop Table #TblUserMst              
  End        
      
  IF OBJECT_ID('tempdb..#TblUserMstWithParent') IS NOT NULL      
  BEGIN      
 DROP TABLE #TblUserMstWithParent      
  END      
              
 CREATE TABLE #TblResult(UserId int, FullName NVARCHAR(MAX),RefferalCode NVARCHAR(MAX),Position CHAR(10))              
                
 SELECT Position,FullName,RefferalCode,UserId,RightUserId,LeftUserId INTO #TblUserMst FROM UserMst         
          
 SELECT t.UserId,t.FullName,t.RefferalCode,t.Position,(CASE WHEN t.Position = 'R' THEN RightResult.UserId ELSE LEFTResult.UserId END) AS ParentId INTO #TblUserMstWithParent from #TblUserMst t      
 OUTER APPLY      
 (      
  SELECT t1.UserId FROM #TblUserMst t1 WHERE t1.LeftUserId = t.UserId      
 ) LeftResult      
 OUTER APPLY      
 (      
  SELECT t1.UserId FROM #TblUserMst t1 WHERE t1.RightUserId = t.UserId      
 ) RightResult      
      
 CREATE NONCLUSTERED INDEX ix_tempposts ON #TblUserMstWithParent(UserId,ParentId);      
      
 ;WITH tblParent AS      
 (      
  SELECT UserId,FullName,RefferalCode,Position,ParentId      
   FROM #TblUserMstWithParent WHERE UserId = @UserId      
  UNION ALL      
  SELECT t.UserId,t.FullName,t.RefferalCode,t.Position,t.ParentId      
   FROM #TblUserMstWithParent t JOIN tblParent  ON t.UserId = tblParent.ParentId      
 )      
 INSERT INTO #TblResult      
 SELECT UserId,FullName,RefferalCode,Position FROM tblParent WHERE UserId <> @UserId      
 OPTION(MAXRECURSION 0)       
      
 IF(@IncludeSelf = 1)      
 BEGIN      
  INSERT INTO #TblResult      
  SELECT UserId,FullName,RefferalCode,Position FROM #TblUserMst WHERE UserId = @UserId      
 END      
      
 SELECT UserId,FullName,RefferalCode,Position FROM #TblResult     
   
END 