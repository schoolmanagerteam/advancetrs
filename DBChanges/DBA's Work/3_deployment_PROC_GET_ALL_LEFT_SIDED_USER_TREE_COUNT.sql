USE [stage_db]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PROC_GET_ALL_LEFT_SIDED_USER_TREE_COUNT]
(
@UserId int,
@IncludeSelf bit=1
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;

	Declare @LeftUserId int, @count int =null, @IsRegister bit=null
	Select @LeftUserId=LeftUserId, @IsRegister =IsRegistrationActivated  from dbo.UserMst where userId = @UserId

	;WITH DirectReports AS   
	(  
		SELECT LeftUserId, RightUserId, UserId, IsRegistrationActivated
		FROM dbo.UserMst   
		WHERE UserId =@LeftUserId
		UNION ALL  
		SELECT e.LeftUserId, e.RightUserId, e.UserId,e.IsRegistrationActivated
		FROM dbo.UserMst AS e  
			INNER JOIN DirectReports AS d  
			ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)			
	)  	
	SELECT @count = COUNT(1) 
	FROM DirectReports  
	WHERE IsRegistrationActivated = 1
	OPTION (maxrecursion 0);
	
	SELECT ISNULL(@count,0) + CASE WHEN @IncludeSelf=1 AND @IsRegister= 1 THEN 1 ELSE 0 END as [count]

END
GO
