USE [stage_db]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]    Script Date: 12-19-2020 08:57:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--CREATE PROCEDURE [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST_Bhvain]          
--(          
SET STATISTICS TIME,IO ON
 DECLARE  @UserId int          =10
--)          
--AS          
--BEGIN    

-- DROP TABLE #UserData      
 SELECT     
   um.UserId   
    ,um.LeftUserId as MainLeftUserId 
	, um.RightUserId as MainRightUserId
  ,um.RefferalUserId    
  ,um.RefferalCode    
  ,um.FullName    
  ,um.RegistrationDate    
  ,um.RegMonth    
  ,um.RegYear    
  ,um.ActivateDate    
  ,um.MobileNo    
  ,um.DOB    
  ,um.EmailId  
  ,um.Position as MainPosition
  ,cm.CityName      
  --,(LeftBV.TotalLeftBV+RightBV.TotalRightBV)As TotalBV      
  ,CAST(NULL AS FLOAT) AS TotalLeftBV        
  ,CAST(NULL AS FLOAT) AS TotalRightBV        
  ,DirectTeam.DirectTeamTotal        
  ,DirectTeam.DirectTeamActiveTotal        
  ,DirectTeam.DirectTeamInActiveTotal 
 INTO #UserData   
 FROM UserMst as um							-- 71329      
 LEFT JOIN CityMst as cm on um.CityId=cm.CityId					-- 762
 --OUTER APPLY        
 --(        
 --Select SUM(LeftBV)AS TotalLeftBV FROM [FN_GET_USER_LEFT_SIDE_POINTS](um.UserId,NULL,NULL)        
 --)LeftBV        
 --OUTER APPLY        
 --(        
 --Select SUM(RightBV)AS TotalRightBV FROM [FN_GET_USER_RIGHT_SIDE_POINTS](um.UserId,NULL,NULL)        
 --)RightBV        
 OUTER APPLY        
 (        
 Select SUM(1)As DirectTeamTotal        
     ,SUM(case when u.IsRegistrationActivated = 1 then 1 else 0 END)AS DirectTeamActiveTotal        
     ,SUM(case when (u.IsRegistrationActivated = 0 OR u.IsRegistrationActivated IS NULL)  then 1 else 0 END)AS DirectTeamInActiveTotal        
     FROM UserMst u where u.RefferalUserId = um.UserId         
 )DirectTeam        
 WHERE         
 um.RefferalUserId= @UserId and um.IsActive=1        


 ;WITH UPDATE_LEFT_NULL AS (

 Select	UD.UserId,
			SUM(case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) as LeftBV			
	from	
	#UserData UD 
	INNER JOIN UserPurchaseHdr as uph on UD.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId	AND pcd.UserId = UD.UserId
	Where UD.MainLeftUserId IS NULL			
	group by UD.UserId
 )
 UPDATE Udata
 SET TotalLeftBV = ULN.LeftBV
 FROM #UserData Udata
 INNER JOIN UPDATE_LEFT_NULL ULN ON ULN.UserId = Udata.UserId


 DECLARE @TotalLeftNN INT
 DECLARE @counter INT = 1

 DECLARE @LoopUserId INT, @LeftUserId INT, @RightUserId INT , @TotalLeftBV FLOAT


 CREATE TABLE #LoopLR
 (
	Id INT IDENTITY(1,1),
	MainUserId INT,
	LeftUserId INT,
	RightUserId INT
 )

 INSERT INTO #LoopLR (MainUserId, LeftUserId)
 SELECT UserId, MainLeftUserId FROM #UserData WHERE MainLeftUserId IS NOT NULL	

 SET @TotalLeftNN = @@ROWCOUNT
 

	 WHILE (@counter < @TotalLeftNN)
	BEGIN
	
		SELECT 
			@LoopUserId = MainUserId,
			@LeftUserId = LeftUserId
		FROM #LoopLR 
		WHERE Id = @counter

		;WITH DirectReports_IN (LeftUserId, RightUserId,UserId) AS
		(  
			SELECT LeftUserId, RightUserId, UserId
			FROM dbo.UserMst   
			WHERE UserId =  @LeftUserId
			UNION ALL  
			SELECT e.LeftUserId, e.RightUserId, e.UserId
			FROM dbo.UserMst AS e  
				INNER JOIN DirectReports_IN AS d  
				ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
		), 
		DirectReports AS (
		Select @LoopUserId as UserId		
		UNION 
		Select UserId from DirectReports_IN 
		)					
		SELECT  	
			@TotalLeftBV = SUM(case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 		
		from	
		DirectReports DR 
		INNER JOIN UserPurchaseHdr as uph on DR.UserId=uph.CreatedBy
		INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
		INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
		--INNER JOIN UserMst as um on DR.UserId=um.UserId		
		WHERE pcd.UserId=@LoopUserId		
		 OPTION (maxrecursion 0)

		 UPDATE #UserData
		 SET TotalLeftBV = @TotalLeftBV
		 WHERE UserId = @LoopUserId

		 SET @counter = @counter + 1

	 END


  ;WITH UPDATE_RIGHT_NULL AS (
  Select	UD.UserId,
			SUM(case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end)RightBV			
	from #UserData UD 
	INNER JOIN UserPurchaseHdr as uph on UD.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId	AND pcd.UserId = UD.UserId
	Where UD.MainRightUserId IS NULL			
	group by UD.UserId	
	)
 UPDATE Udata
 SET TotalRightBV = ULN.RightBV
 FROM #UserData Udata
 INNER JOIN UPDATE_RIGHT_NULL ULN ON ULN.UserId = Udata.UserId

 
--  UPDATE Udata
-- SET TotalLeftBV = LeftBV.TotalLeftBV
-- FROM #UserData Udata
--OUTER APPLY        
-- (        
-- Select SUM(LeftBV)AS TotalLeftBV FROM [FN_GET_USER_LEFT_SIDE_POINTS](Udata.UserId,NULL,NULL)        
-- )LeftBV      
-- WHERE Udata.MainLeftUserId IS NOT NULL

 --UPDATE Udata
 --SET TotalRightBV = RightBV.TotalRightBV
 --FROM #UserData Udata
 -- OUTER APPLY        
 --(        
 --Select SUM(RightBV)AS TotalRightBV FROM [FN_GET_USER_RIGHT_SIDE_POINTS](Udata.UserId,NULL,NULL)        
 --)RightBV    
 --WHERE Udata.MainRightUserId IS NOT NULL

  SELECT     
   um.UserId    
  ,um.RefferalUserId    
  ,um.RefferalCode    
  ,um.FullName    
  ,um.RegistrationDate    
  ,um.RegMonth    
  ,um.RegYear    
  ,um.ActivateDate    
  ,um.MobileNo    
  ,um.DOB    
  ,um.EmailId    
  ,um.CityName      
  ,(um.TotalLeftBV+um.TotalRightBV)As TotalBV      
  ,um.TotalLeftBV        
  ,um.TotalRightBV        
  ,um.DirectTeamTotal        
  ,um.DirectTeamActiveTotal        
  ,um.DirectTeamInActiveTotal        
 FROM #UserData as um 

 GO

     


 

 SELECT userId, MainLeftUserId, MainRightUserId,TotalLeftBV, TotalRightBV FROM #UserData WHERE UserId=26729
 
 
 ORDER BY UserId
 
 
 --Select SUM(LeftBV)AS TotalLeftBV FROM [FN_GET_USER_LEFT_SIDE_POINTS](um.UserId,NULL,NULL) 
 SELECT 26729 as UserID,* FROM [FN_GET_USER_LEFT_SIDE_POINTS](26729,NULL,NULL) 
 UNION
 SELECT 25 as UserID,* FROM [FN_GET_USER_LEFT_SIDE_POINTS](25,NULL,NULL) 

 SELECT 346 as UserID,* FROM [FN_GET_USER_LEFT_SIDE_POINTS](2347,NULL,NULL) 
 SELECT * FROM [FN_GET_ALL_LEFT_SIDED_USER_TREE](181,default)
 Select SUM(LeftBV)AS TotalLeftBV FROM [FN_GET_USER_LEFT_SIDE_POINTS](181,NULL,NULL)     

 SELECT UserId , SUM(leftBV) FROM (
 Select	UD.UserId,
			SUM(case when pcd.CreatedBy=pcd.UserId and UD.MainPosition='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV			
	from	
	#UserData UD 
	INNER JOIN UserPurchaseHdr as uph on UD.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId	AND pcd.UserId = UD.UserId
	Where UD.MainLeftUserId IS NULL			
	group by UD.UserId
	) M
	GROUP BY UserId


	SELECT * FROM  [FN_GET_ALL_LEFT_SIDED_USER_TREE](181,default) as lu
	SELECT * FROM  UserMst WHERE UserId=181
	Select	um.UserId,
	
	--SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as LeftPoints,
	--		pcd.CreditDate,um.FullName,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV
			 
			--SUM(CreditPoints) as LeftPoints,pcd.CreditDate,lu.FullName,um.RefferalCode
	from --[FN_GET_ALL_LEFT_SIDED_USER_TREE](181,default) as lu
	--INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId
	--INNER JOIN 
	UserPurchaseHdr as uph 
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN #UserData UD ON UD.UserId = uph.CreatedBy AND UD.UserId=181
	INNER JOIN UserMst as um on um.UserId = UD.UserId	
	Where pcd.UserId=181
	AND  uph.CreatedBy=181
			--and ((cast(pcd.CreditDate as date)>=@FromDate) or (@FromDate is null))
			--and ((cast(pcd.CreditDate as date)<=@ToDate) or (@ToDate is null))
	group by um.UserId --,pcd.CreditDate,um.FullName,um.RefferalCode
 


 SELECT * FROM [FN_GET_ALL_LEFT_SIDED_USER_TREE](25,default)

 Select	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' then CreditPoints when pcd.CreatedBy<>pcd.UserId then CreditPoints end) as LeftPoints,
			pcd.CreditDate,lu.FullName,um.RefferalCode,
			SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV
			 
			--SUM(CreditPoints) as LeftPoints,pcd.CreditDate,lu.FullName,um.RefferalCode
	from [FN_GET_ALL_LEFT_SIDED_USER_TREE](UD.UserId,default) as lu
	--INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId
	INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on lu.UserId=um.UserId
	INNER JOIN #UserData UD ON UD.UserId = um.UserId AND UD.UserId IN (25,27)
	--Where pcd.UserId=@UserId
			--and ((cast(pcd.CreditDate as date)>=@FromDate) or (@FromDate is null))
			--and ((cast(pcd.CreditDate as date)<=@ToDate) or (@ToDate is null))
	group by pcd.CreditDate,lu.FullName,um.RefferalCode


 
        
--END 

