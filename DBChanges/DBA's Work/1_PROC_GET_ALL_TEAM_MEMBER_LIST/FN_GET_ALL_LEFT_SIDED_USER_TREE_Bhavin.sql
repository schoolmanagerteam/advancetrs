USE [stage_db]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]    Script Date: 12-20-2020 12:57:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from [FN_GET_ALL_LEFT_SIDED_USER_TREE](1,0)
ALTER Function [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE_Bhavin]
(
@LeftUserId int,
@UserId int
)
RETURNS @Usertbl table(UserId int)
AS
BEGIN

	--Declare @tbl table(LeftUserId int, RightUserId int,UserId int,FullName nvarchar(max),RefferalCode nvarchar(max), UserLevel int,IsRegistrationActivated bit,RegistrationDate datetime);

	--Declare @UserId int=1
--	Declare @LeftUserId int
--	Select @LeftUserId=LeftUserId from UserMst where userId = @UserId

		WITH DirectReports_IN (LeftUserId, RightUserId,UserId) AS
		(  
			SELECT LeftUserId, RightUserId, UserId
			FROM dbo.UserMst   
			WHERE UserId =  @LeftUserId
			UNION ALL  
			SELECT e.LeftUserId, e.RightUserId, e.UserId
			FROM dbo.UserMst AS e  
				INNER JOIN DirectReports_IN AS d  
				ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
		)		
		INSERT INTO @Usertbl
		Select UserId from DirectReports_IN 
		OPTION (maxrecursion 0) 
		
		RETURN;
END						
GO



SELECT * FROM [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE_Bhavin](695,24) as lu 