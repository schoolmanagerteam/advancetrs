USE [stage_db]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_Bhavin]    Script Date: 12-20-2020 04:16:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Function [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_Bhavin]
(
@LeftUserId int,
@UserId int
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @output  FLOAT

		;WITH DirectReports_IN  AS
		(  
			SELECT LeftUserId, RightUserId, UserId
			FROM dbo.UserMst   
			WHERE UserId =  @LeftUserId
			UNION ALL  
			SELECT e.LeftUserId, e.RightUserId, e.UserId
			FROM dbo.UserMst AS e  
				INNER JOIN DirectReports_IN AS d  
				ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
		), 
		DirectReports AS (
		Select @UserId as UserId		
		UNION 
		Select UserId from DirectReports_IN 
		)					
	Select	@output = SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end)		 			
	from DirectReports as lu 
	INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN UserMst as um on lu.UserId=um.UserId
	Where pcd.UserId=@UserId	
	OPTION (maxrecursion 0) 	
	
	RETURN @output
END