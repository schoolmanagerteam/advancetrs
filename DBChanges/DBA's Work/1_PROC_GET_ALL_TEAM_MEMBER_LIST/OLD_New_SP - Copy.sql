USE [stage_db]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST_Bhvain]    Script Date: 12-20-2020 04:16:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST_Bhvain]          
(          
 @UserId int,
 @pageNumber int,
 @pageSize int
)          
AS          
BEGIN    


	IF @pageNumber = 0
	BEGIN
		SELECT @pagesize= COUNT(1) FROM UserMst as um	WHERE um.RefferalUserId= @UserId and um.IsActive=1        
	END
	ELSE 
	BEGIN
		SET @pageNumber = ((@pageNumber-1)*@pageSize)
	END


 SELECT   
   um.UserId   
    ,um.LeftUserId as MainLeftUserId 
	, um.RightUserId as MainRightUserId
  ,um.RefferalUserId    
  ,um.RefferalCode    
  ,um.FullName    
  ,um.RegistrationDate    
  ,um.RegMonth    
  ,um.RegYear    
  ,um.ActivateDate    
  ,um.MobileNo    
  ,um.DOB    
  ,um.EmailId  
  ,um.Position as MainPosition
  ,cm.CityName      
  --,(LeftBV.TotalLeftBV+RightBV.TotalRightBV)As TotalBV      
  ,CAST(NULL AS FLOAT) AS TotalLeftBV        
  ,CAST(NULL AS FLOAT) AS TotalRightBV        
  ,DirectTeam.DirectTeamTotal        
  ,DirectTeam.DirectTeamActiveTotal        
  ,DirectTeam.DirectTeamInActiveTotal 
 
 INTO #UserData   
 FROM UserMst as um							-- 71329      
 LEFT JOIN CityMst as cm on um.CityId=cm.CityId					-- 762
 --OUTER APPLY        
 --(        
 --Select SUM(LeftBV)AS TotalLeftBV FROM [FN_GET_USER_LEFT_SIDE_POINTS](um.UserId,NULL,NULL)        
 --)LeftBV        
 --OUTER APPLY        
 --(        
 --Select SUM(RightBV)AS TotalRightBV FROM [FN_GET_USER_RIGHT_SIDE_POINTS](um.UserId,NULL,NULL)        
 --)RightBV        
 OUTER APPLY        
 (        
 Select SUM(1)As DirectTeamTotal        
     ,SUM(case when u.IsRegistrationActivated = 1 then 1 else 0 END)AS DirectTeamActiveTotal        
     ,SUM(case when (u.IsRegistrationActivated = 0 OR u.IsRegistrationActivated IS NULL)  then 1 else 0 END)AS DirectTeamInActiveTotal        
     FROM UserMst u where u.RefferalUserId = um.UserId         
 )DirectTeam        
 WHERE         
 um.RefferalUserId= @UserId 
 and um.IsActive=1 
  ORDER BY um.UserId   
 OFFSET @pageNumber ROWS 
FETCH NEXT @pageSize ROWS ONLY       


 ;WITH UPDATE_LEFT_NULL AS (

 Select	UD.UserId,
			SUM(case when pcd.CreatedBy=pcd.UserId and UD.MainPosition='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV			
	from	
	#UserData UD 
	INNER JOIN UserPurchaseHdr as uph on UD.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId	AND pcd.UserId = UD.UserId
	Where UD.MainLeftUserId IS NULL			
	group by UD.UserId
 )
 UPDATE Udata
 SET TotalLeftBV = ULN.LeftBV
 FROM #UserData Udata
 INNER JOIN UPDATE_LEFT_NULL ULN ON ULN.UserId = Udata.UserId

  ;WITH UPDATE_RIGHT_NULL AS (
  Select	UD.UserId,
			SUM(case when pcd.CreatedBy=pcd.UserId and UD.MainPosition='L' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end)RightBV			
	from #UserData UD 
	INNER JOIN UserPurchaseHdr as uph on UD.UserId=uph.CreatedBy
	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId	AND pcd.UserId = UD.UserId
	Where UD.MainRightUserId IS NULL			
	group by UD.UserId	
	)
 UPDATE Udata
 SET TotalRightBV = ULN.RightBV
 FROM #UserData Udata
 INNER JOIN UPDATE_RIGHT_NULL ULN ON ULN.UserId = Udata.UserId

 --DECLARE @TotalLeftNN INT
 --DECLARE @counter INT = 1

 --DECLARE @LoopUserId INT, @LeftUserId INT, @RightUserId INT , @TotalLeftBV FLOAT


 --CREATE TABLE #LoopLR
 --(
	--Id INT IDENTITY(1,1),
	--MainUserId INT,
	--LeftUserId INT,
	--RightUserId INT
 --)

 --INSERT INTO #LoopLR (MainUserId, LeftUserId)
 --SELECT UserId, MainLeftUserId FROM #UserData WHERE MainLeftUserId IS NOT NULL	

 --SET @TotalLeftNN = @@ROWCOUNT

 --SELECT @TotalLeftNN as TotalLeftNN
 

	-- WHILE (@counter < @TotalLeftNN)
	--BEGIN
	
	--	SELECT 
	--		@LoopUserId = MainUserId,
	--		@LeftUserId = LeftUserId
	--	FROM #LoopLR 
	--	WHERE Id = @counter

		--;WITH DirectReports_IN (LeftUserId, RightUserId,UserId) AS
		--(  
		--	SELECT LeftUserId, RightUserId, UserId
		--	FROM dbo.UserMst   
		--	WHERE UserId =  @LeftUserId
		--	UNION ALL  
		--	SELECT e.LeftUserId, e.RightUserId, e.UserId
		--	FROM dbo.UserMst AS e  
		--		INNER JOIN DirectReports_IN AS d  
		--		ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
		--), 
		--DirectReports AS (
		--Select @LoopUserId as UserId		
		--UNION 
		--Select UserId from DirectReports_IN 
		--)					
	--	SELECT  	
	--		@TotalLeftBV = SUM(case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 		
	--	from	
	--	DirectReports DR 
	--	INNER JOIN UserPurchaseHdr as uph on DR.UserId=uph.CreatedBy
	--	INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	--	INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	--	--INNER JOIN UserMst as um on DR.UserId=um.UserId		
	--	WHERE pcd.UserId=@LoopUserId		
	--	 OPTION (maxrecursion 0)

	--	 UPDATE #UserData
	--	 SET TotalLeftBV = @TotalLeftBV
	--	 WHERE UserId = @LoopUserId

	--	 SET @counter = @counter + 1

	-- END


 
  UPDATE Udata
 SET TotalLeftBV = [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_Bhavin](Udata.MainLeftUserId, Udata.UserId) 
 FROM #UserData Udata
 WHERE Udata.MainLeftUserId IS NOT NULL


   UPDATE Udata
 SET TotalRightBV = [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_Bhavin](Udata.MainRightUserId, Udata.UserId) 
 FROM #UserData Udata
 WHERE Udata.MainRightUserId IS NOT NULL


  SELECT     
   um.UserId    
  ,um.RefferalUserId    
  ,um.RefferalCode    
  ,um.FullName    
  ,um.RegistrationDate    
  ,um.RegMonth    
  ,um.RegYear    
  ,um.ActivateDate    
  ,um.MobileNo    
  ,um.DOB    
  ,um.EmailId    
  ,um.CityName      
  ,(um.TotalLeftBV+um.TotalRightBV)As TotalBV      
  ,um.TotalLeftBV        
  ,um.TotalRightBV        
  ,um.DirectTeamTotal        
  ,um.DirectTeamActiveTotal        
  ,um.DirectTeamInActiveTotal        
 FROM #UserData as um 

  DROP TABLE #UserData     
  --DROP TABLE #LoopLR 
 END 
