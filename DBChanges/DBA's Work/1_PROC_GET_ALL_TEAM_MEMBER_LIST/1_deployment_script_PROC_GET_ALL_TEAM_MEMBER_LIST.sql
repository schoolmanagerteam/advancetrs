USE [stage_db]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_v2]
(
@LeftUserId int,
@UserId int
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @output  FLOAT

		;WITH DirectReports_IN  AS
		(  
			SELECT LeftUserId, RightUserId, UserId
			FROM dbo.UserMst   
			WHERE UserId =  @LeftUserId
			UNION ALL  
			SELECT e.LeftUserId, e.RightUserId, e.UserId
			FROM dbo.UserMst AS e  
				INNER JOIN DirectReports_IN AS d  
				ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
		), 
		DirectReports AS (
		Select @UserId as UserId		
		UNION 
		Select UserId from DirectReports_IN 
		)					
	Select	@output = SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end)		 			
	from DirectReports as lu 
	INNER JOIN dbo.UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
	INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN dbo.UserMst as um on lu.UserId=um.UserId
	Where pcd.UserId=@UserId	
	OPTION (maxrecursion 0) 	
	
	RETURN @output
END
GO


USE [stage_db]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_v2]
(
@RightUserId int,
@UserId int
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @output  FLOAT

		;WITH DirectReports_IN AS   
	(  
		SELECT LeftUserId, RightUserId, UserId
		FROM dbo.UserMst   
		WHERE UserId = @RightUserId
		UNION ALL  
		SELECT e.LeftUserId, e.RightUserId, e.UserId
		FROM dbo.UserMst AS e  
			INNER JOIN DirectReports_IN AS d  
			ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
	) ,
	DirectReports AS 
	(
		SELECT @UserId as UserId
		UNION
		SELECT UserId FROM DirectReports_IN
	)
	Select	
		@output = 	SUM(case when pcd.CreatedBy=pcd.UserId and um.Position='L' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end)
	from DirectReports as ru	
	INNER JOIN dbo.UserPurchaseHdr as uph on ru.UserId=uph.CreatedBy
	INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
	INNER JOIN dbo.UserMst as um on ru.UserId=um.UserId
	Where pcd.UserId=@UserId
	OPTION (maxrecursion 0) 	
	
	RETURN @output
END
GO


CREATE NONCLUSTERED INDEX IXUserPurchaseHdr__CreatedBy__INC
ON [dbo].[UserPurchaseHdr] ([CreatedBy])
INCLUDE ([UserPurchaseHdrId],[IsRepurchaseOrder])
GO


CREATE NONCLUSTERED INDEX IXUserPurchaseDtl__UserPurchaseHdrId_INC
ON [dbo].[UserPurchaseDtl] ([UserPurchaseHdrId])
INCLUDE ([UserPurchaseDtlId],[DirectPurchaseVolume],[RepurchaseMatchingVolume])
GO



CREATE NONCLUSTERED INDEX IXPointsCreditDtl__UserId_UserPurchaseDtlId
ON [dbo].[PointsCreditDtl] ([UserId],[UserPurchaseDtlId])
GO


USE [stage_db]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]          
(          
 @UserId int,
 @pageNumber int,
 @pageSize int
)          
AS          
BEGIN    

	SET NOCOUNT ON;
	SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;

	IF @pageNumber = 0
	BEGIN
		SELECT @pagesize= COUNT(1) FROM UserMst as um	WHERE um.RefferalUserId= @UserId and um.IsActive=1        
	END
	ELSE 
	BEGIN
		SET @pageNumber = ((@pageNumber-1)*@pageSize)
	END


 SELECT   
   um.UserId   
    ,um.LeftUserId as MainLeftUserId 
	, um.RightUserId as MainRightUserId
  ,um.RefferalUserId    
  ,um.RefferalCode    
  ,um.FullName    
  ,um.RegistrationDate    
  ,um.RegMonth    
  ,um.RegYear    
  ,um.ActivateDate    
  ,um.MobileNo    
  ,um.DOB    
  ,um.EmailId  
  ,um.Position as MainPosition
  ,cm.CityName      
  ,CAST(NULL AS FLOAT) AS TotalLeftBV        
  ,CAST(NULL AS FLOAT) AS TotalRightBV        
  ,DirectTeam.DirectTeamTotal        
  ,DirectTeam.DirectTeamActiveTotal        
  ,DirectTeam.DirectTeamInActiveTotal  
 INTO #UserData   
 FROM dbo.UserMst as um			
 LEFT JOIN CityMst as cm on um.CityId=cm.CityId	 
 OUTER APPLY        
 (        
 Select SUM(1)As DirectTeamTotal        
     ,SUM(case when u.IsRegistrationActivated = 1 then 1 else 0 END)AS DirectTeamActiveTotal        
     ,SUM(case when (u.IsRegistrationActivated = 0 OR u.IsRegistrationActivated IS NULL)  then 1 else 0 END)AS DirectTeamInActiveTotal        
     FROM dbo.UserMst u where u.RefferalUserId = um.UserId         
 )DirectTeam        
 WHERE         
 um.RefferalUserId= @UserId 
 and um.IsActive=1 
  ORDER BY um.UserId   
 OFFSET @pageNumber ROWS 
FETCH NEXT @pageSize ROWS ONLY       


 ;WITH UPDATE_LEFT_NULL AS (

 Select	UD.UserId,
			SUM(case when pcd.CreatedBy=pcd.UserId and UD.MainPosition='R' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end) as LeftBV			
	from	
	#UserData UD 
	INNER JOIN dbo.UserPurchaseHdr as uph on UD.UserId=uph.CreatedBy
	INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId	AND pcd.UserId = UD.UserId
	Where UD.MainLeftUserId IS NULL			
	group by UD.UserId
 )
 UPDATE Udata
 SET TotalLeftBV = ULN.LeftBV
 FROM #UserData Udata
 INNER JOIN UPDATE_LEFT_NULL ULN ON ULN.UserId = Udata.UserId

  ;WITH UPDATE_RIGHT_NULL AS (
  Select	UD.UserId,
			SUM(case when pcd.CreatedBy=pcd.UserId and UD.MainPosition='L' 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) 
				when pcd.CreatedBy<>pcd.UserId 
					then (case when uph.IsRepurchaseOrder=1 then RepurchaseMatchingVolume else DirectPurchaseVolume end) end)RightBV			
	from #UserData UD 
	INNER JOIN dbo.UserPurchaseHdr as uph on UD.UserId=uph.CreatedBy
	INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
	INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId	AND pcd.UserId = UD.UserId
	Where UD.MainRightUserId IS NULL			
	group by UD.UserId	
	)
 UPDATE Udata
 SET TotalRightBV = ULN.RightBV
 FROM #UserData Udata
 INNER JOIN UPDATE_RIGHT_NULL ULN ON ULN.UserId = Udata.UserId 
 
  UPDATE Udata
 SET TotalLeftBV = [dbo].[FN_GET_USER_LEFT_SIDE_POINTS_v2](Udata.MainLeftUserId, Udata.UserId) 
 FROM #UserData Udata
 WHERE Udata.MainLeftUserId IS NOT NULL


   UPDATE Udata
 SET TotalRightBV = [dbo].[FN_GET_USER_RIGHT_SIDE_POINTS_v2](Udata.MainRightUserId, Udata.UserId) 
 FROM #UserData Udata
 WHERE Udata.MainRightUserId IS NOT NULL


  SELECT     
   um.UserId    
  ,um.RefferalUserId    
  ,um.RefferalCode    
  ,um.FullName    
  ,um.RegistrationDate    
  ,um.RegMonth    
  ,um.RegYear    
  ,um.ActivateDate    
  ,um.MobileNo    
  ,um.DOB    
  ,um.EmailId    
  ,um.CityName      
  ,(um.TotalLeftBV+um.TotalRightBV)As TotalBV      
  ,um.TotalLeftBV        
  ,um.TotalRightBV        
  ,um.DirectTeamTotal        
  ,um.DirectTeamActiveTotal        
  ,um.DirectTeamInActiveTotal        
 FROM #UserData as um 

  DROP TABLE #UserData       
 END 

GO


