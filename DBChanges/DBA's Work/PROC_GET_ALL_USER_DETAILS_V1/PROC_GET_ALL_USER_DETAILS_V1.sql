USE [maxener]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_USER_DETAILS_V1]    Script Date: 01/30/2021 12:34:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[PROC_GET_ALL_USER_DETAILS_V1]
(
@Page TINYINT,
@Size INT,
@UserId INT = null,
@Type varchar(100) = null,
@Year SMALLINT = null,
@Month SMALLINT = null,
@ActivateDate varchar(100) = null,
@MobileNo VARCHAR(100) = null,
@Password VARCHAR(100) = null,
@RefferalByCode VARCHAR(20) = null,
@RefferalCode VARCHAR(20) = null,
@RegistrationDate varchar(100) = NULL,
@UserName VARCHAR(100) = null,
@UserCodeOrMobile VARCHAR(20) = null,
@TotalCount varchar(50) output
)
AS
BEGIN

	If(OBJECT_ID('tempdb..#tblUserMst') Is Not Null)                
	BEGIN                
		Drop Table #tblUserMst          
	End    

	SELECT um.*,
		rum.RefferalCode as RefferalByCode,rum.FullName as RefferalByName,
		 CONVERT(varchar, um.ActivateDate, 103) as ActivationDate
		INTO #tblUserMst
	FROM UserMst as um
	INNER JOIN UserMst as rum on um.RefferalUserId = rum.UserId
	WHERE 1 = 1
	AND
	((NULLIF(@UserCodeOrMobile, '') IS NOT NULL AND ((um.RefferalCode like '%'+@UserCodeOrMobile+'%') 
									OR (um.AlternativeMobileNo like '%'+@UserCodeOrMobile+'%' OR um.MobileNo like '%'+@UserCodeOrMobile+'%')))
	OR
	(NULLIF(@UserCodeOrMobile, '') IS NULL AND 
	((@Type IS NULL OR @Type = 'silver' AND um.IsSilverMember = 1)
	OR (@Type IS NULL OR @Type = 'prime' AND um.IsPrimeMember = 1)
	OR (@Type IS NULL OR @Type = 'all' AND um.RefferalUserId <> 0)
	OR (@Type IS NULL OR @Type = 'gold' AND um.IsGoldMember = 1)
	OR (@Type IS NULL OR @Type = 'diamond' AND um.IsDimondMember = 1)
	OR (@Type IS NULL OR @Type = 'inactive' AND ISNULL(um.IsRegistrationActivated,0) = 0)
	OR (@Type IS NULL OR @Type = 'active' AND um.IsRegistrationActivated = 1)
	OR (@Type IS NULL OR @Type = 'last30' AND CONVERT(VARCHAR, um.RegistrationDate, 103) >= CONVERT(VARCHAR,DATEADD(D, -30, GETDATE()),103)))

	AND (@Month IS NULL OR um.RegMonth = @Month)
	AND (@Year IS NULL OR um.RegYear = @Year)
	AND (NULLIF(@RegistrationDate, '') IS NULL OR CONVERT(varchar, um.RegistrationDate, 103) = @RegistrationDate)
	AND (NULLIF(@ActivateDate, '') IS NULL OR CONVERT(varchar, um.ActivateDate, 103) = @ActivateDate)
	AND (NULLIF(@MobileNo, '') IS NULL OR um.MobileNo like '%'+@MobileNo+'%')
	AND (NULLIF(@Password, '') IS NULL OR um.[Password] like '%'+@Password+'%')
	AND (NULLIF(@RefferalCode, '') IS NULL OR  um.RefferalCode like '%'+@RefferalCode+'%')
	AND (NULLIF(@RefferalByCode, '') IS NULL OR um.RefferalCode like '%'+@RefferalByCode+'%')
	AND (NULLIF(@UserName, '') IS NULL OR um.UserName like '%'+@UserName+'%')))
	
	

	SELECT @TotalCount = COUNT(1) FROM #tblUserMst
	
    SELECT * FROM #tblUserMst
	ORDER BY RegistrationDate DESC
	OFFSET (@Page * @Size) ROWS
	FETCH NEXT @Size ROWS ONLY 

	SELECT @TotalCount

END