--SELECT UserId from UserMst order by 1

SELECT CASE WHEN database_id = 32767 then 'Resource' ELSE DB_NAME(database_id)END AS DBName
      ,OBJECT_SCHEMA_NAME(object_id,database_id) AS [SCHEMA_NAME]  
      ,OBJECT_NAME(object_id,database_id)AS [OBJECT_NAME]
      ,cached_time
      ,last_execution_time
      ,execution_count
      ,total_worker_time / execution_count AS AVG_CPU
      ,total_elapsed_time / execution_count AS AVG_ELAPSED
      ,total_logical_reads / execution_count AS AVG_LOGICAL_READS
      ,total_logical_writes / execution_count AS AVG_LOGICAL_WRITES
      ,total_physical_reads  / execution_count AS AVG_PHYSICAL_READS
FROM sys.dm_exec_procedure_stats  
WHERE OBJECT_NAME(object_id,database_id) IN ('PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_DBA','PROC_RPT_GET_ADMIN_MATCHING_INCOME_REPORT_v2')
ORDER BY AVG_LOGICAL_READS DESC