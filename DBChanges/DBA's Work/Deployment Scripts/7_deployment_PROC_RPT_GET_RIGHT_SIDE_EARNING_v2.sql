USE [stage_db]
GO

CREATE NONCLUSTERED INDEX IX__Usermst__UserId_INC_ref_code_fullName
ON [dbo].UserMst ([UserId])
INCLUDE (RefferalCode,FullName)
GO


USE [stage_db]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING_v2]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;

IF @FromDate IS NULL AND @ToDate IS NULL
BEGIN
Select SUM(CreditPoints)CreditPoints,cast(pcd.CreditDate as date)CreditDate,um.RefferalCode,
um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount as OrderAmount,
uph.UserPurchaseHdrId,
SUM(ISNULL(ISNULL(DirectPurchaseVolume,DirectMatchingVolume),0)+ISNULL(RepurchaseMatchingVolume,0))RightBV
from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,1) as lu
INNER JOIN dbo.UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
INNER JOIN dbo.UserMst as um on lu.UserId=um.UserId
Where pcd.UserId=@UserId
group by cast(pcd.CreditDate as date),um.RefferalCode,um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount,uph.UserPurchaseHdrId
END
ELSE
BEGIN
Select SUM(CreditPoints)CreditPoints,cast(pcd.CreditDate as date)CreditDate,um.RefferalCode,
um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount as OrderAmount,
uph.UserPurchaseHdrId,
SUM(ISNULL(ISNULL(DirectPurchaseVolume,DirectMatchingVolume),0)+ISNULL(RepurchaseMatchingVolume,0))RightBV
from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,1) as lu
INNER JOIN dbo.UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
INNER JOIN dbo.UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
INNER JOIN dbo.PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
INNER JOIN dbo.UserMst as um on lu.UserId=um.UserId
Where pcd.UserId=@UserId
AND cast(pcd.CreditDate as date)>=@FromDate
AND cast(pcd.CreditDate as date)<=@ToDate
group by cast(pcd.CreditDate as date),um.RefferalCode,um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount,uph.UserPurchaseHdrId
END


END
GO

