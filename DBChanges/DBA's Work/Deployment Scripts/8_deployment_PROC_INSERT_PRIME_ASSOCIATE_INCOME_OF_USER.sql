USE [stage_db]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER]
(
@ActionedBy int,
@UserId int=0,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;
	--Declare @ActionedBy int=1
	--Declare @FromDate datetime
	--Declare @ToDate datetime

	Declare @TempUserId int
	Declare @TempChildUserId int
	Declare @TempPrimeMemberDate datetime
	Declare @LastCalculateDate datetime
	Declare @TempFromDate datetime
	Declare @Today datetime=dbo.fn_GetServerDate()
	Declare @TotalLeftPoint numeric(18,0)
	Declare @TotalRightPoint numeric(18,0)	
	Declare @MatchingPoint numeric(18,0)				
	Declare @PrimeMemberPerc float


	--EXEC PROC_INSERT_MATCHING_INCOME_OF_USER

	SELECT @LastCalculateDate=CAST(Value as datetime) FROM dbo.ConfigMst where [Key]='LastCalculateDate'

	IF NOT EXISTS(SELECT 1 FROM dbo.ConfigMst Where [Key]='PrimeMemberPerc')
	BEGIN
		SELECT @PrimeMemberPerc=Value FROM dbo.ConfigMst Where [Key]='PrimeMemberPerc'
	END
	ELSE
	BEGIN
		SET @PrimeMemberPerc=10
	END

	If @FromDate is null
	BEGIN
		SET @FromDate=CAST(@Today AS DATE)
	END
	If @ToDate is null
	BEGIN
		SET @ToDate=CAST(@Today AS DATE)
	END
	
	Declare @tblPrimeMembers table(UserId int,PrimeMemberDate datetime)

	Declare @PrimeMemberCnt int=0

Declare @PrimeUserId int
Declare @GoldUserId int
DECLARE db_cursorprime CURSOR FOR 
Select UserId,PrimeMemberDate from dbo.UserMst
Where IsPrimeMember=1 and ISNULL(IsSilverMember,0)=0

OPEN db_cursorprime  
FETCH NEXT FROM db_cursorprime INTO @PrimeUserId,@TempPrimeMemberDate

WHILE @@FETCH_STATUS = 0  
BEGIN  

--SELECT @PrimeUserId
SET @GoldUserId=null
Select top 1 @GoldUserId=UserId from [dbo].[FN_GET_ALL_PARENT_USER_TREE_BY_LEVEL](@PrimeUserId,0) Where IsGoldMember=1 order by UserLevel

IF ISNULL(@GoldUserId,0)>0
BEGIN
	Delete from @tblPrimeMembers
	
	insert into @tblPrimeMembers
	Select um.UserId,PrimeMemberDate 
	from dbo.UserMst as um
	INNER JOIN (
		Select UserId from dbo.FN_GET_ALL_LEFT_SIDED_USER_TREE(@GoldUserId,0)
		union
		Select UserId from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@GoldUserId,0)
	) as childs on um.UserId=childs.UserId
	Where 
		--RefferalUserId=@GoldUserId	AND 
		IsPrimeMember=1 AND 
		um.UserId=case when @UserId=0 then um.UserId else @UserId end
	
	IF  @LastCalculateDate is null
	BEGIN
		SET @TempFromDate=cast(@TempPrimeMemberDate as date)
	END
	ELSE
	BEGIN
		SET @TempFromDate=@LastCalculateDate
	END
	
	IF cast(@TempFromDate as date)<cast(@TempPrimeMemberDate as date)
	BEGIN
		SET @TempFromDate=cast(@TempPrimeMemberDate as date)
	END
	
	SET @TempFromDate=cast(@TempFromDate as DATE)
	SET @ToDate=cast(@ToDate as DATE)
	
	WHILE @TempFromDate<=@ToDate
	BEGIN
		
		Declare @GoldIncome numeric(18,0)
		--SET @GoldIncome=ISNULL(@GoldIncome,0)			
		--SELECT @GoldIncome=SUM(CreditPoints) 
		--from LeaderAndUplinePointsCreditDtl as mpcd
		--Where UserId=@GoldUserId AND mpcd.CreditDate=@TempFromDate and IsLeaderPoint=1
		
		SELECT @GoldIncome=SUM(MatchingPoints)
		FROM dbo.MatchingPointsCreditDtl
		Where UserId=@GoldUserId AND CreditDate=@TempFromDate
		
		SET @GoldIncome=ISNULL(@GoldIncome,0)
		
		Select @PrimeMemberCnt=COUNT(distinct UserId) from @tblPrimeMembers Where cast(PrimeMemberDate as date)<=cast(@TempFromDate as date)
		--Select @PrimeMemberCnt,@GoldUserId,@TempFromDate
		IF ISNULL(@PrimeMemberCnt,0)>0
		BEGIN
			SET @GoldIncome=cast((@GoldIncome/@PrimeMemberCnt) as numeric(18,0))
		END
		ELSE
		BEGIN
			SET @GoldIncome=0			
		END
		
		IF EXISTS(SELECT 1 from dbo.LeaderAndUplinePointsCreditDtl Where UserId=@PrimeUserId AND CreditDate=@TempFromDate and FromCreditUserId=@GoldUserId and IsLeaderPoint=0)
		BEGIN
			UPDATE LeaderAndUplinePointsCreditDtl
			SET CreditPerc=@PrimeMemberPerc,
				CreditDate=@TempFromDate,
				CreditPoints=cast((@PrimeMemberPerc*@GoldIncome/100) as numeric(18,0)),
				IsActive=1,
				CreatedBy=@ActionedBy,
				CreatedDate=dbo.fn_GetServerDate(),
				IsLeaderPoint=0
			WHERE UserId=@PrimeUserId AND CreditDate=@TempFromDate and FromCreditUserId=@GoldUserId and IsLeaderPoint=0
		END
		ELSE
		BEGIN
			IF @GoldIncome>0
			BEGIN
				INSERT INTO dbo.LeaderAndUplinePointsCreditDtl(UserId
														   ,FromCreditUserId
														   ,CreditPerc
														   ,CreditDate
														   ,CreditPoints
														   ,IsActive
														   ,CreatedBy
														   ,CreatedDate
														   ,IsLeaderPoint)
				SELECT @PrimeUserId as UserId,
					@GoldUserId as FromCreditUserId,
					@PrimeMemberPerc as CreditPerc,
					@TempFromDate as CreditDate,
					cast((@PrimeMemberPerc*@GoldIncome/100) as numeric(18,0)) as CreditPoints,
					1 as IsActive,
					@ActionedBy as CreatedBy,
					dbo.fn_GetServerDate() as CreatedDate,
					cast(0 as bit) as IsLeaderPoint
					
				INSERT INTO dbo.NotificationMst (NotificationTypeId,UserId,Notification,URL,IsRead,ReadDate,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				values(1,@PrimeUserId,'You Got New Upline Support Income In Your Wallet Check Now',null,null,null,1,1,dbo.fn_GetServerDate(),1,dbo.fn_GetServerDate())
			END
		END						
		
		SET @TempFromDate=@TempFromDate+1
	END
		
END

FETCH NEXT FROM db_cursorprime INTO @PrimeUserId,@TempPrimeMemberDate
END
CLOSE db_cursorprime  
DEALLOCATE db_cursorprime	
	
END
GO