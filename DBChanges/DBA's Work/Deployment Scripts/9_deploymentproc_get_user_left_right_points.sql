USE [stage_db]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_USER_LEFT_SIDE_POINTS]    Script Date: 12/27/2020 3:05:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[PROC_GET_USER_LEFT_SIDE_POINTS]
(
@UserId int,
@FromDate date=null,
@ToDate date=null
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;


SELECT * FROM [dbo].[FN_GET_USER_LEFT_SIDE_POINTS](@UserId,@FromDate,@ToDate)

END
GO

USE [stage_db]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[PROC_GET_USER_RIGHT_SIDE_POINTS]
(
@UserId int,
@FromDate date=null,
@ToDate date=null
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;

SELECT * FROM dbo.FN_GET_USER_RIGHT_SIDE_POINTS(@UserId,@FromDate,@ToDate)

END