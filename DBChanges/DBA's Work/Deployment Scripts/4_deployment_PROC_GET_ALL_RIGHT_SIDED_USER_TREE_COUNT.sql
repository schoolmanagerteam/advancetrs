USE [stage_db]
GO

CREATE NONCLUSTERED INDEX IXUserMst__AutoSizeColumn__INC
ON [dbo].UserMst (userId)
INCLUDE (LeftUserId,RightUserId,IsRegistrationActivated )

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PROC_GET_ALL_RIGHT_SIDED_USER_TREE_COUNT]
(
@UserId int,
@IncludeSelf bit=1,
@IsRegistered bit
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;
	
	Declare @RightUserId int=null , @IsRegister bit=null, @count int =null
	Select @RightUserId=RightUserId,  @IsRegister =IsRegistrationActivated  from dbo.UserMst where userId = @UserId

	;WITH DirectReports  AS   
	(  
		SELECT LeftUserId, RightUserId, UserId, IsRegistrationActivated 
		FROM dbo.UserMst   
		WHERE UserId =@RightUserId
		UNION ALL  
		SELECT e.LeftUserId, e.RightUserId, e.UserId, e.IsRegistrationActivated
		FROM dbo.UserMst AS e  
			INNER JOIN DirectReports AS d  
			ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
	)  
	SELECT @count = COUNT(1) 
	FROM DirectReports  
	WHERE IsRegistrationActivated = @IsRegistered
	OPTION (maxrecursion 0);
	
	SELECT ISNULL(@count,0) + CASE WHEN @IncludeSelf=1 AND @IsRegister= @IsRegistered THEN 1 ELSE 0 END as [count]
END

