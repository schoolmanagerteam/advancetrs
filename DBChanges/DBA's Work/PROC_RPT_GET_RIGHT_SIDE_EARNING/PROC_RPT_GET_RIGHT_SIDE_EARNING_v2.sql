USE [stage_db]
GO

/****** Object:  StoredProcedure [dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING_v2]    Script Date: 12/27/2020 2:54:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[PROC_RPT_GET_RIGHT_SIDE_EARNING_v2]
(
@UserId int,
@FromDate datetime=null,
@ToDate datetime=null
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION  ISOLATION LEVEL READ UNCOMMITTED;

IF @FromDate IS NULL AND @ToDate IS NULL
BEGIN
Select SUM(CreditPoints)CreditPoints,cast(pcd.CreditDate as date)CreditDate,um.RefferalCode,
um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount as OrderAmount,
uph.UserPurchaseHdrId,
SUM(ISNULL(ISNULL(DirectPurchaseVolume,DirectMatchingVolume),0)+ISNULL(RepurchaseMatchingVolume,0))RightBV
from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,1) as lu
INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
INNER JOIN UserMst as um on lu.UserId=um.UserId
Where pcd.UserId=@UserId
group by cast(pcd.CreditDate as date),um.RefferalCode,um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount,uph.UserPurchaseHdrId
END
ELSE
BEGIN
Select SUM(CreditPoints)CreditPoints,cast(pcd.CreditDate as date)CreditDate,um.RefferalCode,
um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount as OrderAmount,
uph.UserPurchaseHdrId,
SUM(ISNULL(ISNULL(DirectPurchaseVolume,DirectMatchingVolume),0)+ISNULL(RepurchaseMatchingVolume,0))RightBV
from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,1) as lu
INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.CreatedBy
INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
INNER JOIN UserMst as um on lu.UserId=um.UserId
Where pcd.UserId=@UserId
AND cast(pcd.CreditDate as date)>=@FromDate
AND cast(pcd.CreditDate as date)<=@ToDate
group by cast(pcd.CreditDate as date),um.RefferalCode,um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount,uph.UserPurchaseHdrId
END


/*
--Declare @UserId int=15,
--@FromDate datetime=null,
--@ToDate datetime=null
--Declare @RightUsers table(UserId int, LeftUserId int, FullName nvarchar(max),UserLevel int)
--insert into @RightUsers
--exec PROC_GET_ALL_RIGHT_USER_TREE @UserId,1
Select SUM(CreditPoints)CreditPoints,cast(pcd.CreditDate as date)CreditDate,um.RefferalCode,
um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount as OrderAmount,
uph.UserPurchaseHdrId,
SUM(ISNULL(ISNULL(DirectPurchaseVolume,DirectMatchingVolume),0)+ISNULL(RepurchaseMatchingVolume,0))RightBV
from dbo.FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,1) as lu
INNER JOIN UserPurchaseHdr as uph on lu.UserId=uph.UserId
AND (uph.OrderDate>=@FromDate or @FromDate is null)
AND (uph.OrderDate<=@ToDate or @ToDate is null)
INNER JOIN UserPurchaseDtl as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId
INNER JOIN PointsCreditDtl as pcd on upd.UserPurchaseDtlId=pcd.UserPurchaseDtlId
INNER JOIN UserMst as um on uph.UserId=um.UserId and ((pcd.UserId=pcd.CreatedBy and um.Position='L') or (pcd.UserId<>pcd.CreatedBy))
Where pcd.UserId=@UserId
group by cast(pcd.CreditDate as date),um.RefferalCode,um.FullName,uph.OrderDate,uph.OrderNo,uph.TotalOrderAmount,uph.UserPurchaseHdrId
*/


END
GO

