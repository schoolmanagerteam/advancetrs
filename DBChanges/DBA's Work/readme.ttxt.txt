PC UserName : Manish
Password : Manish@123




input id  = 

@UserId

Prob input id = 10

sample input id = 




Use stage_db
GO


SET STATISTICS TIME,IO ON
-- Experiments 
-- old timing 2 min 21 sec -- with temp
-- with inline and table valued function : 2 min 37 sec
EXEC [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST_Bhvain]   @UserId=10

GO



/*
Missing Index Details from SQLQuery2.sql - 192.168.0.100.stage_db (sa (68))
The Query Processor estimates that implementing the following index could improve the query cost by 62.0096%.
*/

/*
USE [stage_db]
GO
	CREATE NONCLUSTERED INDEX IXUserPurchaseHdr__CreatedBy__INC
	ON [dbo].[UserPurchaseHdr] ([CreatedBy])
	INCLUDE ([UserPurchaseHdrId],[IsRepurchaseOrder])
GO
*/


/*
Missing Index Details from SQLQuery2.sql - 192.168.0.100.stage_db (sa (68))
The Query Processor estimates that implementing the following index could improve the query cost by 80.3869%.
*/

/*
USE [stage_db]
GO
CREATE NONCLUSTERED INDEX IXUserPurchaseDtl__UserPurchaseHdrId_INC
ON [dbo].[UserPurchaseDtl] ([UserPurchaseHdrId])
INCLUDE ([UserPurchaseDtlId],[DirectPurchaseVolume],[RepurchaseMatchingVolume], DirectMatchingVolume)
GO
*/


/*
Missing Index Details from SQLQuery2.sql - 192.168.0.100.stage_db (sa (68))
The Query Processor estimates that implementing the following index could improve the query cost by 33.1455%.
*/

/*
USE [stage_db]
GO
CREATE NONCLUSTERED INDEX IXPointsCreditDtl__UserId_UserPurchaseDtlId
ON [dbo].[PointsCreditDtl] ([UserId],[UserPurchaseDtlId])

GO
*/



CREATE NONCLUSTERED INDEX IXPointsCreditDtl__UserId_UserPurchaseDtlId__INC
ON [dbo].[PointsCreditDtl] ([UserId],[UserPurchaseDtlId])
INCLUDE ([CreditDate],[CreditPoints])


DROP INDEX IXUserMst__UserId_INC
ON [dbo].UserMst
		USE [stage_db]
GO
CREATE NONCLUSTERED INDEX IXUserMst__UserId_INC
ON [dbo].UserMst ([UserId])
INCLUDE ( LeftUserId, RightUserId)