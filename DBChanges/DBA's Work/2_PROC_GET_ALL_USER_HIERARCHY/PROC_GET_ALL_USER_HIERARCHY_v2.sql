USE [stage_db]
GO

/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_USER_HIERARCHY_v2]    Script Date: 12/26/2020 12:51:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[PROC_GET_ALL_USER_HIERARCHY_v2]
(
@UserId int,
@IncludeSelf bit
)
AS
BEGIN
	
	SET FMTONLY OFF


	SELECT * 
	INTO #User_tree_data
	FROM FN_GET_ALL_CHILD_USER_TREE(@UserId,@IncludeSelf) as ac

	
	SELECT * FROM (
	Select um.UserId, um.FirstName, um.FullName, um.RefferalUserId,
		   um.RefferalCode, um.MobileNo,
			case when um.Position='L' then 'Left' else 'Right' end as PositionName,
			case when um.Position='L' then 0 else 1 end as PositionOrder,
			
			ISNULL(um.IsRegistrationActivated,0) as IsActive,
			pum.UserId as ParentUserId,
			UserLevel,
			
			um.ProfileImage
			,rum.FirstName as RefferedByUser, 
			rum.FullName as RefferedByUserFullName
			,ISNULL(cast((case when um.LeftUserId is null and um.RightUserId is null then 0 else 1 end) as bit),1) as HasChilds,
			um.Position,
			um.LeftUserId,
			um.RightUserId,
			cast(0 as float) as BV
	from #User_tree_data as ac
	INNER JOIN dbo.UserMst as um on ac.UserId=um.UserId
	LEFT JOIN dbo.UserMst as pum on ac.UserId=pum.LeftUserId
	LEFT JOIN dbo.UserMst as rum on um.RefferalUserId=rum.UserId
	UNION 
	Select um.UserId, um.FirstName, um.FullName, um.RefferalUserId,
		   um.RefferalCode, um.MobileNo,
			case when um.Position='L' then 'Left' else 'Right' end as PositionName,
			case when um.Position='L' then 0 else 1 end as PositionOrder,
			
			ISNULL(um.IsRegistrationActivated,0) as IsActive,
			pum.UserId as ParentUserId,
			UserLevel,
			
			um.ProfileImage
			,rum.FirstName as RefferedByUser, 
			rum.FullName as RefferedByUserFullName
			,ISNULL(cast((case when um.LeftUserId is null and um.RightUserId is null then 0 else 1 end) as bit),1) as HasChilds,
			um.Position,
			um.LeftUserId,
			um.RightUserId,
			cast(0 as float) as BV
	from #User_tree_data as ac
	INNER JOIN dbo.UserMst as um on ac.UserId=um.UserId
	LEFT JOIN dbo.UserMst as pum on ac.UserId=pum.RightUserId
	LEFT JOIN dbo.UserMst as rum on um.RefferalUserId=rum.UserId
	) as Final
	WHERE ParentUserId IS NOT NULL
	ORDER BY UserLevel,PositionOrder
	
	DROP TABLE #User_tree_data
END
GO

