USE [stage_db]
GO
/****** Object:  StoredProcedure [dbo].[PROC_GET_ALL_USER_HIERARCHY]    Script Date: 12/25/2020 10:10:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[PROC_GET_ALL_USER_HIERARCHY]
(
@UserId int,
@IncludeSelf bit=0
)
AS
BEGIN
	
	SET FMTONLY OFF
	Select UserId,FirstName,FullName,RefferalUserId,RefferalCode,MobileNo,
						Position,IsRegistrationActivated,ProfileImage,LeftUserId,RightUserId
	into #tmpUsers
	FROM UserMst 
	--FROM UserMst(nolock) 
	SELECT * FROM (
	Select um.UserId,um.FirstName,um.FullName,um.RefferalUserId,um.RefferalCode,um.MobileNo,
			case when um.Position='L' then 'Left' else 'Right' end as PositionName,
			case when um.Position='L' then 0 else 1 end as PositionOrder,
			ISNULL(um.IsRegistrationActivated,0) as IsActive,pum.UserId as ParentUserId,UserLevel,
			um.ProfileImage,rum.FirstName as RefferedByUser,rum.FullName as RefferedByUserFullName,
			ISNULL(cast((case when um.LeftUserId is null and um.RightUserId is null then 0 else 1 end) as bit),1) as HasChilds,
			um.Position,um.LeftUserId,um.RightUserId,cast(0 as float) as BV
	from FN_GET_ALL_CHILD_USER_TREE(@UserId,@IncludeSelf) as ac
	INNER JOIN #tmpUsers as um on ac.UserId=um.UserId
	LEFT JOIN #tmpUsers as pum on ((ac.UserId=pum.LeftUserId) or (ac.UserId=pum.RightUserId))
	LEFT JOIN #tmpUsers as rum on um.RefferalUserId=rum.UserId
	) as Final
	order by UserLevel,PositionOrder
END