USE [stage_db]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]    Script Date: 12/25/2020 10:13:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Select * from [FN_GET_ALL_LEFT_SIDED_USER_TREE](1,0)
CREATE Function [dbo].[FN_GET_ALL_LEFT_SIDED_USER_TREE]
(
@UserId int,
@IncludeSelf bit=1
)
RETURNS @Usertbl table(LeftUserId int, RightUserId int,UserId int,FullName nvarchar(max),RefferalCode nvarchar(max), UserLevel int,IsRegistered bit,RegDate datetime)
AS
BEGIN

	Declare @tbl table(LeftUserId int, RightUserId int,UserId int,FullName nvarchar(max),RefferalCode nvarchar(max), UserLevel int,IsRegistrationActivated bit,RegistrationDate datetime);

	--Declare @UserId int=1
	Declare @LeftUserId int
	Select @LeftUserId=LeftUserId from UserMst where userId = @UserId

	;WITH DirectReports(LeftUserId, RightUserId,UserId,FullName,RefferalCode, UserLevel,IsRegistrationActivated,RegistrationDate) AS   
	(  
		SELECT LeftUserId, RightUserId, UserId,FullName,RefferalCode, 1 AS UserLevel,IsRegistrationActivated,RegistrationDate
		FROM dbo.UserMst   
		WHERE UserId =@LeftUserId
		UNION ALL  
		SELECT e.LeftUserId, e.RightUserId, e.UserId,e.FullName,e.RefferalCode, UserLevel + 1,e.IsRegistrationActivated ,e.RegistrationDate
		FROM dbo.UserMst AS e  
			INNER JOIN DirectReports AS d  
			ON (e.UserId = d.LeftUserId   ) or (e.UserId = d.RightUserId)
	)  
	insert into @tbl
	SELECT LeftUserId, RightUserId,UserId,FullName,RefferalCode, UserLevel,ISNULL(IsRegistrationActivated,0)IsRegistered,RegistrationDate
	FROM DirectReports  
	OPTION (maxrecursion 0);
	
	IF @IncludeSelf=0
	BEGIN
		insert into @Usertbl
		Select * from @tbl
	END
	ELSE
	BEGIN
		insert into @Usertbl
		
		Select LeftUserId, RightUserId,UserId,FullName,RefferalCode,0,ISNULL(IsRegistrationActivated,0)IsRegistered,RegistrationDate
		from UserMst where UserId=@UserId
		UNION ALL
		Select * from @tbl
	END

	RETURN
END
GO

