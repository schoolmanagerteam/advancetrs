USE [stage_db]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GET_ALL_CHILD_USER_TREE]    Script Date: 12/25/2020 10:12:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Function [dbo].[FN_GET_ALL_CHILD_USER_TREE]
(
@UserId int,
@IncludeSelf bit=0
)
RETURNS @Usertbl table(UserId int,  FullName nvarchar(max),UserLevel int)
AS
BEGIN

	insert into @Usertbl
	Select UserId,FullName,UserLevel from FN_GET_ALL_LEFT_SIDED_USER_TREE(@UserId,@IncludeSelf)
	UNION
	
	Select UserId,FullName,UserLevel from FN_GET_ALL_RIGHT_SIDED_USER_TREE(@UserId,@IncludeSelf)
	
	RETURN
END