Use AdvanceTRS_09Oct
GO
IF OBJECT_ID('dbo.UserPurchaseHdrTemp', 'U') IS NULL 
BEGIN
	
	CREATE TABLE [dbo].[UserPurchaseHdrTemp](
		[UserPurchaseHdrId] [bigint] IDENTITY(1,1) NOT NULL,
		[UserId] [int] NOT NULL,
		[OrderNo] [bigint] NOT NULL,
		[TotalOrderAmount] [float] NOT NULL,
		[OrderDate] [date] NOT NULL,
		[IsActive] [bit] NOT NULL,
		[CreatedBy] [numeric](18, 0) NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[ModifiedBy] [numeric](18, 0) NULL,
		[ModifiedDate] [datetime] NULL,
		[FranchiseeId] [int] NULL,
		[OrderStatusId] [int] NULL,
		[CourierTrackingNo] [nvarchar](50) NULL,
		[RazorpayOrderId] [nvarchar](max) NULL,
		[RazorpayPaymentId] [nvarchar](max) NULL,
		[RazorpaySignature] [nvarchar](max) NULL,
		[IsRepurchaseOrder] [bit] NULL,
		[DeliveryAddress] [nvarchar](max) NULL,
		[OrderReceivedById] [int] NULL,
		[IsOrderPlaceByAdmin] [bit] NULL,
		[Notes] [varchar](100) NULL,
		[IsCODOrder] [bit] NULL,
		[IsSuccess] [bit] NULL,
		[IsProcess] [bit] NULL,
		[RetryCount] [tinyint] NULL,
		[RazorpayPaymentResponse] [nvarchar](max) NULL,
		[ProcessDateTime] [datetime] NULL,
	 CONSTRAINT [PK_UserPurchaseHdrTemp_UserPurchaseHdrId] PRIMARY KEY CLUSTERED 
	(
		[UserPurchaseHdrId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	ALTER TABLE [dbo].[UserPurchaseHdrTemp] ADD  DEFAULT ('FALSE') FOR [IsOrderPlaceByAdmin]
	ALTER TABLE [dbo].[UserPurchaseHdrTemp] ADD  DEFAULT (NULL) FOR [Notes]

END
GO

IF OBJECT_ID('dbo.UserPurchaseDtlTemp', 'U') IS NULL 
BEGIN

	CREATE TABLE [dbo].[UserPurchaseDtlTemp](
		[UserPurchaseDtlId] [bigint] IDENTITY(1,1) NOT NULL,
		[UserPurchaseHdrId] [bigint] NOT NULL,
		[ProductId] [int] NULL,
		[ProductPrice] [float] NULL,
		[Qty] [int] NULL,
		[Amount] [float] NULL,
		[IsActive] [bit] NOT NULL,
		[CreatedBy] [numeric](18, 0) NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[ModifiedBy] [numeric](18, 0) NULL,
		[ModifiedDate] [datetime] NULL,
		[ProductVariantId] [int] NULL,
		[RegularPrice] [float] NULL,
		[DirectPurchaseVolume] [float] NULL,
		[DirectMatchingVolume] [float] NULL,
		[RepurchaseMatchingVolume] [float] NULL,
		[IsCreditPointsProcess] [bit] NULL,
	 CONSTRAINT [PK_UserPurchaseDtlTemp_UserPurchaseDtlId] PRIMARY KEY CLUSTERED 
	(
		[UserPurchaseDtlId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[UserPurchaseDtlTemp] ADD  DEFAULT ((0)) FOR [IsCreditPointsProcess]

	ALTER TABLE [dbo].[UserPurchaseDtlTemp]  WITH CHECK ADD FOREIGN KEY([UserPurchaseHdrId])
	REFERENCES [dbo].[UserPurchaseHdrTemp] ([UserPurchaseHdrId])

END

GO
IF COL_LENGTH('UserPurchaseHdr', 'IsFailedOrderVerified') IS NULL
BEGIN

	ALTER TABLE UserPurchaseHdr
	ADD IsFailedOrderVerified BIT DEFAULT 0;

END
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM Sys.Procedures WHERE name = 'PROC_GET_ORDER_FAILED_VERIFY_QUEUE' AND type IN (N'P',N'PC'))
BEGIN
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [PROC_GET_ORDER_FAILED_VERIFY_QUEUE] AS'
END
GO
ALTER PROCEDURE [dbo].[PROC_GET_ORDER_FAILED_VERIFY_QUEUE]                        
AS                      
BEGIN                      
          
  SELECT TOP 50 UserPurchaseHdrId,UserId,OrderNo,OrderDate,RazorpayOrderId,CreatedDate FROM UserPurchaseHdrTemp WHERE IsSuccess IS NULL       
          AND IsProcess IS NULL      
          AND (      
          RetryCount IS NULL       
          OR (RetryCount = 1 AND DATEADD(MINUTE,60,ProcessDateTime) > GETDATE())      
          OR (RetryCount = 2 AND DATEADD(MINUTE,120,ProcessDateTime) > GETDATE())      
          OR (RetryCount = 3 AND DATEADD(MINUTE,240,ProcessDateTime) > GETDATE())      
          OR (RetryCount = 4 AND DATEADD(MINUTE,480,ProcessDateTime) > GETDATE()))  
  ORDER BY UserPurchaseHdrId ASC      
        
END 
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM [ServiceRunningStatus] WHERE [ServiceName] = 'OrderFailedverify Process')
BEGIN
	INSERT INTO [dbo].[ServiceRunningStatus]
			   ([ServiceName]
			   ,[IsActive])
		 VALUES
			   ('OrderFailedverify Process'
			   ,1)
END
