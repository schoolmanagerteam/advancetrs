Use AdvanceTRS_12Sep
GO
IF NOT EXISTS (
		SELECT TOP 1 1
		FROM Sys.Procedures
		WHERE name = 'PROC_GET_ORDER_INVOICE_DETAILS'
			AND type IN (
				N'P'
				,N'PC'
				)
		)
BEGIN
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [PROC_GET_ORDER_INVOICE_DETAILS] AS'
END
GO
ALTER Procedure [dbo].[PROC_GET_ORDER_INVOICE_DETAILS]  
(  
	@UserPurchaseHdrId int = 0  
)  
AS  
BEGIN  

Declare @MinRegistrationPoints INT = 0
SELECT @MinRegistrationPoints = [Value] FROM ConfigMst WHERE [Key] = 'MinRegistrationPoints'

Select um.FullName,um.[Address] as [Address],um.GSTNo,EmailId,MobileNo,uph.OrderNo,uph.OrderDate,  
   pm.ProductName,pcm.CategoryName,upd.Qty,  
   upd.ProductPrice,upd.Amount,cm.CityName,uph.TotalOrderAmount,  
   CAST(CASE WHEN A.TotalDirectPoints >= @MinRegistrationPoints THEN 1 ELSE 0 END AS BIT) AS IsRepurchase,
   um.RefferalCode,ISNULL(IsAyurvedic,0)IsAyurvedic
 from UserPurchaseHdr as uph   
 INNER JOIN UserPurchaseDtl upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId   
 INNER JOIN ProductMst as pm on upd.ProductId=pm.ProductId   
 INNER JOIN ProductCategoryMst as pcm on pm.ProductCategoryId=pcm.ProductCategoryId  
 INNER JOIN UserMst as um on uph.UserId=um.UserId  
 LEFT JOIN CityMst as cm on um.CityId=cm.CityId  
 OUTER APPLY
 (
   SELECT 
		SUM(ISNULL(DirectPurchaseVolume,0))As TotalDirectPoints 
	FROM 
		UserPurchaseDtl InnerUpd Inner join UserPurchaseHdr Inneruph ON InnerUpd.UserPurchaseHdrId = Inneruph.UserPurchaseHdrId 
	WHERE 
		Inneruph.UserId = uph.UserId AND InnerUpd.UserPurchaseDtlId < upd.UserPurchaseDtlId
 ) A
 Where uph.UserPurchaseHdrId=case when @UserPurchaseHdrId = 0 then uph.UserPurchaseHdrId else @UserPurchaseHdrId end

  
 /*
 Select um.FullName,um.[Address] as [Address],um.GSTNo,EmailId,MobileNo,uph.OrderNo,uph.OrderDate,  
   pm.ProductName,pcm.CategoryName,upd.Qty,  
   upd.ProductPrice,upd.Amount,cm.CityName,uph.TotalOrderAmount,  
   cast(case when uph.UserPurchaseHdrId=oh.UserPurchaseHdrId  then 0 else 1 end as bit) as IsRepurchase,  
   um.RefferalCode,ISNULL(IsAyurvedic,0)IsAyurvedic  
 from UserPurchaseHdr as uph   
 INNER JOIN UserPurchaseDtl upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId   
 INNER JOIN ProductMst as pm on upd.ProductId=pm.ProductId   
 INNER JOIN ProductCategoryMst as pcm on pm.ProductCategoryId=pcm.ProductCategoryId  
 INNER JOIN UserMst as um on uph.UserId=um.UserId  
 LEFT JOIN CityMst as cm on um.CityId=cm.CityId  
 LEFT JOIN (  
    Select * from (  
   Select Row_Number() over(partition by UserId order by UserPurchaseHdrId) as RowNo,UserPurchaseHdrId,UserId  
   from UserPurchaseHdr  
   ) as oh   
   Where oh.RowNo=1  
   ) as oh on uph.UserPurchaseHdrId=oh.UserPurchaseHdrId  
 Where uph.UserPurchaseHdrId=case when @UserPurchaseHdrId=0 then uph.UserPurchaseHdrId else @UserPurchaseHdrId end 
 */ 


END


