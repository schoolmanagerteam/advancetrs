Use AdvanceTRS_05Nov
GO
IF COL_LENGTH('ProductMst', 'JoinningProductType') IS NULL
BEGIN
	ALTER TABLE ProductMst
	ADD JoinningProductType tinyint NULL;
END
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM [ConfigMst] WHERE [Key] = 'EnableSMSAlertAPI')
BEGIN
	INSERT INTO [dbo].[ConfigMst]
			   ([Key]
			   ,[Value]
			   ,[IsActive]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate])
		 VALUES
			   ('EnableSMSAlertAPI'
			   ,'false'
			   ,1
			   ,1
			   ,GETDATE()
			   ,1
			   ,GETDATE())
END
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM [ConfigMst] WHERE [Key] = 'SMSAlertAPIURL')
BEGIN
INSERT INTO [dbo].[ConfigMst]
           ([Key]
           ,[Value]
           ,[IsActive]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])
     VALUES
           ('SMSAlertAPIURL'
           ,'https://www.smsalert.co.in/api/push.json?apikey={0}&sender={1}&mobileno={2}&text={3}'
           ,1
           ,1
           ,GETDATE()
           ,1
           ,GETDATE())
END
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM [ConfigMst] WHERE [Key] = 'SMSAlertAPIKey')
BEGIN
INSERT INTO [dbo].[ConfigMst]
           ([Key]
           ,[Value]
           ,[IsActive]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])
     VALUES
           ('SMSAlertAPIKey'
           ,'5fa6ae124fe7f'
           ,1
           ,1
           ,GETDATE()
           ,1
           ,GETDATE())
END
GO
ALTER PROCEDURE [dbo].[PROC_GET_ALL_TEAM_MEMBER_LIST]          
(          
 @UserId int          
)          
AS          
BEGIN          
 SELECT     
   um.UserId    
  ,um.RefferalUserId    
  ,um.RefferalCode    
  ,um.FullName    
  ,um.RegistrationDate    
  ,um.RegMonth    
  ,um.RegYear    
  ,um.ActivateDate    
  ,um.MobileNo    
  ,um.DOB    
  ,um.EmailId    
  ,cm.CityName      
  ,(LeftBV.TotalLeftBV+RightBV.TotalRightBV)As TotalBV      
  ,LeftBV.TotalLeftBV        
  ,RightBV.TotalRightBV        
  ,DirectTeam.DirectTeamTotal        
  ,DirectTeam.DirectTeamActiveTotal        
  ,DirectTeam.DirectTeamInActiveTotal        
 FROM UserMst as um          
 LEFT JOIN CityMst as cm on um.CityId=cm.CityId          
 OUTER APPLY        
 (        
 Select SUM(LeftBV)AS TotalLeftBV FROM [FN_GET_USER_LEFT_SIDE_POINTS](um.UserId,NULL,NULL)        
 )LeftBV        
 OUTER APPLY        
 (        
 Select SUM(RightBV)AS TotalRightBV FROM [FN_GET_USER_RIGHT_SIDE_POINTS](um.UserId,NULL,NULL)        
 )RightBV        
 OUTER APPLY        
 (        
 Select SUM(1)As DirectTeamTotal        
     ,SUM(case when u.IsRegistrationActivated = 1 then 1 else 0 END)AS DirectTeamActiveTotal        
     ,SUM(case when (u.IsRegistrationActivated = 0 OR u.IsRegistrationActivated IS NULL)  then 1 else 0 END)AS DirectTeamInActiveTotal        
     FROM UserMst u where u.RefferalUserId = um.UserId         
 )DirectTeam        
 WHERE         
 um.RefferalUserId= @UserId and um.IsActive=1          
        
END 

GO
ALTER PROCEDURE [dbo].[PROC_GET_PRODUCT_LIST]  
AS  
BEGIN  
 Select 
pm.ProductId
,pm.ProductName
,pm.ProductCategoryId
,pm.ShortDesc
,pm.LongDescription
,pm.SKU
,pm.IsPublished
,pm.IsFeatured
,pm.IsDisplayInCatelog
,pm.SalePrice
,pm.RegularPrice
,pm.InStock
,pm.Stock
,pm.LowStock
,pm.ImagePath
,pm.IsActive
,pm.CreatedBy
,pm.CreatedDate
,pm.ModifiedBy
,pm.ModifiedDate
,pm.BackCoverImagePath
,pm.ProductVideoURL
,pm.IsAyurvedic
,pm.DirectPurchasePerc
,pm.DirectMatchingPerc
,pm.RepurchaseMatchingPerc
,pm.DirectPurchaseAmt
,pm.DirectMatchingAmt
,pm.RepurchaseMatchingAmt
,pm.WeightGM
,ISNULL(pm.JoinningProductType,1)AS JoinningProductType
,pcm.CategoryName  
 from ProductMst as pm  
 INNER JOIN ProductCategoryMst as pcm on pm.ProductCategoryId=pcm.ProductCategoryId  
 where pm.IsActive=1  
 Order by pm.ProductCategoryId  
END