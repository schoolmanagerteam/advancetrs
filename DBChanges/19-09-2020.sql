Use AdvanceTRS_19Sep
GO
ALTER Function [dbo].[FN_GET_ALL_PARENT_USER_TREE]    
(    
 @UserId int,    
 @IncludeSelf bit = 1    
)    
RETURNS @Usertbl table(UserId int, FullName NVARCHAR(MAX),RefferalCode NVARCHAR(MAX),Position CHAR(10))    
AS    
BEGIN    
 
   Declare @ActualUserId int=@UserId    
  Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);    
    
  --Declare @UserId int=8    
  Declare @LoopBreak bit=0    
  Declare @UserList table(UserId int)    
    
  While @LoopBreak=0    
  BEGIN    
   insert into @UserList values(@UserId)    
      
   Declare @Position char(1)    
   Select @Position=Position from UserMst where UserId=@UserId    
   IF @Position='L'    
   BEGIN    
    Select @UserId=UserId from UserMst where LeftUserId=@UserId    
   END    
   ELSE IF @Position='R'    
   BEGIN    
    Select @UserId=UserId from UserMst where RightUserId=@UserId    
   END     
   ELSE    
    SET @LoopBreak=1    
  END    
    
     
  IF @IncludeSelf=0    
  BEGIN    
   insert into @Usertbl    
   Select ul.UserId,um.FullName,um.RefferalCode,um.Position    
   from @UserList as ul    
   INNER JOIN UserMst as um on ul.UserId=um.UserId    
   where ul.UserId<>@ActualUserId    
  END    
  ELSE    
  BEGIN    
   insert into @Usertbl    
   Select ul.UserId,um.FullName,um.RefferalCode,um.Position    
   from @UserList as ul    
   INNER JOIN UserMst as um on ul.UserId=um.UserId    
  END    
    
  RETURN      

 /*
 Declare @ActualUserId int = @UserId;   
 WITH tblChild AS  
 (  
  SELECT UserMst.UserId  
   FROM UserMst WHERE LeftUserId = @ActualUserId  
  UNION ALL  
  SELECT UserMst.UserId FROM UserMst JOIN tblChild ON (UserMst.LeftUserId = tblChild.UserId)  
 )  
 insert into @Usertbl(UserId,FullName,RefferalCode,Position)  
 SELECT t.UserId,um.FullName,um.RefferalCode,um.Position FROM tblChild t INNER JOIN UserMst um on t.UserId = um.UserId  
 OPTION(MAXRECURSION 32767)  
  
 SET @ActualUserId = @UserId;   
 WITH tblChild AS  
 (  
  SELECT UserMst.UserId  
   FROM UserMst WHERE RightUserId = @ActualUserId  
  UNION ALL  
  SELECT UserMst.UserId FROM UserMst JOIN tblChild ON (UserMst.RightUserId = tblChild.UserId)  
 )  
 insert into @Usertbl(UserId,FullName,RefferalCode,Position)  
 SELECT t.UserId,um.FullName,um.RefferalCode,um.Position FROM tblChild t INNER JOIN UserMst um on t.UserId = um.UserId  
 OPTION(MAXRECURSION 32767)  
   
 IF(@IncludeSelf = 1)  
 BEGIN  
  insert into @Usertbl(UserId,FullName,RefferalCode,Position)  
  SELECT um.UserId,um.FullName,um.RefferalCode,um.Position FROM UserMst um WHERE um.UserId = @UserId  
 END  
  
RETURN   */
  

  
END    


