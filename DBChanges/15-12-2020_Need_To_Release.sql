ALTER PROCEDURE [dbo].[PROC_GET_ORDER_FAILED_VERIFY_QUEUE]                                
AS                              
BEGIN                              
                  
  SELECT TOP 50 UserPurchaseHdrId,UserId,OrderNo,OrderDate,RazorpayOrderId,CreatedDate FROM UserPurchaseHdrTemp WHERE IsSuccess IS NULL               
          AND IsProcess IS NULL              
          AND (              
          (RetryCount IS NULL AND DATEADD(HOUR,1,CreatedDate) < GETDATE())               
          OR (RetryCount = 1 AND DATEADD(HOUR,3,CreatedDate) < GETDATE())              
          OR (RetryCount = 2 AND DATEADD(HOUR,6,CreatedDate) < GETDATE())              
          OR (RetryCount = 3 AND DATEADD(HOUR,12,CreatedDate) < GETDATE())              
          OR (RetryCount = 4 AND DATEADD(HOUR,24,CreatedDate) < GETDATE()))         
  ORDER BY UserPurchaseHdrId ASC              
                
END         
      
GO

ALTER Procedure [dbo].[PROC_GET_ORDER_INVOICE_DETAILS]    
(                
  @UserPurchaseHdrIds varchar(max)            
)                
AS                
BEGIN                
              
Declare @MinRegistrationPoints INT = 0              
SELECT @MinRegistrationPoints = [Value] FROM ConfigMst WHERE [Key] = 'MinRegistrationPoints'              
              
Select um.FullName,um.[Address] as [Address],um.GSTNo,EmailId,MobileNo,uph.OrderNo,uph.OrderDate,                
   pm.ProductName,pcm.CategoryName,upd.Qty,                
   upd.ProductPrice,upd.Amount,cm.CityName,uph.TotalOrderAmount,                
   CAST(CASE WHEN A.TotalDirectPoints >= @MinRegistrationPoints THEN 1 ELSE 0 END AS BIT) AS IsRepurchase,              
   um.RefferalCode,ISNULL(IsAyurvedic,0)IsAyurvedic,            
   ISNULL(uph.DeliveryAddress,'')AS DeliveryAddress,            
   um.PinCode,            
   CAST(0 AS float) AS TotalAmount,          
   CAST(0 AS float) AS CGST,          
   CAST(0 AS float) AS SGST          
 from UserPurchaseHdr as uph                 
 INNER JOIN UserPurchaseDtl upd on uph.UserPurchaseHdrId= upd.UserPurchaseHdrId                 
 INNER JOIN ProductMst as pm on upd.ProductId=pm.ProductId                 
 INNER JOIN ProductCategoryMst as pcm on pm.ProductCategoryId=pcm.ProductCategoryId                
 INNER JOIN UserMst as um on uph.UserId=um.UserId            
 INNER JOIN dbo.FN_SPLIT(@UserPurchaseHdrIds,',') t ON t.ID = uph.UserPurchaseHdrId             
 LEFT JOIN CityMst as cm on um.CityId = cm.CityId                
 OUTER APPLY              
 (              
   SELECT               
  SUM(ISNULL(DirectPurchaseVolume,0))As TotalDirectPoints               
 FROM               
  UserPurchaseDtl InnerUpd Inner join UserPurchaseHdr Inneruph ON InnerUpd.UserPurchaseHdrId = Inneruph.UserPurchaseHdrId               
 WHERE               
  Inneruph.UserId = uph.UserId AND InnerUpd.UserPurchaseDtlId < upd.UserPurchaseDtlId              
 ) A              
              
END 