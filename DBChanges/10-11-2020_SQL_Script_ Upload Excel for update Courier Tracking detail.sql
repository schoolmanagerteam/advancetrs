Use AdvanceTRS_05Nov
GO
IF COL_LENGTH('UserPurchaseHdr', 'IsSMSSendforCourierTracking') IS NULL
BEGIN
	ALTER TABLE UserPurchaseHdr
	ADD IsSMSSendforCourierTracking BIT DEFAULT 0;
END
GO
ALTER PROC [dbo].[Temp_GetOrderData_DV]                    
@startDate datetime = '2020/05/01',                    
@endDate datetime='2020/06/07',                    
@orderStatusId int=0,                    
@isRepurchaseOrder int =0                    
AS                     
BEGIN                    
                  
 Select                  
                  
 uph.CreatedDate AS 'Order DateTime',                  
 uph.OrderNo 'Reference No',                  
 '' AS BV,                  
 uph.RazorpayPaymentId AS PaymentId,                  
 uph.CourierTrackingNo AS 'Courier Name',                  
 '' AS [Zone],                  
 '' AS 'Courier Charges Actual',                  
 '' AS 'Courier Charges EST',                  
 '' AS 'Prepaid Or COD',                  
 '' AS 'Waybill',                    
 um.FullName as 'Consignee Name',                  
   case                  
      when                  
         ct.CityName IS NULL                   
      then                  
  (                  
         select                  
            TOP 1 CITY                   
         from                  
            [dbo].[Pincodes]                   
         WHERE                  
            PINCODE = um.PinCode)                   
         else                  
            ct.CityName                   
   end                  
   as 'City',                  
   case                  
      when                  
         st.StateName IS NULL                   
      then                  
(                  
         select                  
            TOP 1 State                   
         from                  
            [dbo].[Pincodes]                   
         WHERE                  
            PINCODE = um.PinCode)                   
         else                  
            st.StateName                   
   end                  
   as 'State',                  
   case                  
      when                  
         cm.CountryName IS NULL                   
      then                  
(                  
         select                  
            TOP 1 Country                   
         from                  
            [dbo].[Pincodes]                   
         WHERE                  
            PINCODE = um.PinCode)                   
         else                  
            cm.CountryName                   
   end                  
   as 'Country',                  
   case                  
      when                  
         len(ltrim(rtrim((ISNULL(uph.DeliveryAddress, ''))))) > 0                   
      then                  
         uph.DeliveryAddress                   
      else                  
         um.Address                   
   end                  
   as [Address],                   
   um.PinCode,                   
   um.MobileNo as 'Phone',                  
   um.AlternativeMobileNo as 'MobileNo',                  
   '' AS [Weight],                   
   CASE                  
      WHEN                  
         ISNULL(osm.OrderStatusId, 0) = 1                   
      then                  
         'COD'                   
      WHEN                  
         ISNULL(osm.OrderStatusId, 0) = 5                   
      then                  
         'Prepaid'                   
      ELSE                  
         osm.Name                   
   END                  
   as 'Payment Mode',         
   CASE                  
      WHEN                  
         uph.IsCODOrder = 1                   
      then                  
         'COD'                   
      WHEN                  
         uph.IsCODOrder = 0                   
      then                  
         'Prepaid'                   
      ELSE                  
         ''                   
   END                  
   as 'Order Payment Mode',        
   '' AS PackageAmount,                  
   (                  
      CASE                  
         WHEN                  
            ISNULL(IsRepurchaseOrder, 1) = 1                   
         THEN                  
           'Repurchase'                   
         ELSE                  
            'Direct'                   
      END                  
   )                  
   AS 'Order Type',                   
   (                  
      CASE              
         WHEN                  
            ISNULL(osm.OrderStatusId, 0) = 1                   
         THEN                  
            CAST(uph.TotalOrderAmount AS VARCHAR(50))                   
         ELSE                  
            ''               
      END                  
   )                  
   AS 'Cod Amount',                   
   '' AS 'Product to be Shipped',                   
   'Maxener wellness , behind ujala avenue , nr. Dena bank , vishala circle , saarkhej narol road ahmedbad' AS 'Return Address',                  
   '380055' AS 'Return Pin',                   
   (                  
      select                  
         ISNULL(PV.VariantName, '') AS VariantName,                  
         PM.ProductName,                  
         ISNULL(PM.WeightGM, 0) AS WeightGM,                  
         --ISNULL(PO.Qty, 0) AS Qty,                  
   (CASE                  
         WHEN                  
            ISNULL(IsRepurchaseOrder, 1) = 1                   
         THEN                  
            ISNULL(PO.Qty*2, 0)                   
         ELSE                  
            ISNULL(PO.Qty, 0)                   
   END) AS Qty,          
   ISNULL(DirectMatchingVolume,0)AS DirectMatchingVolume,                  
   ISNULL(RepurchaseMatchingVolume,0)AS RepurchaseMatchingVolume                   
   from                  
         UserPurchaseDtl PO                   
         INNER JOIN                  
            ProductMst PM                   
            ON PO.ProductId = PM.ProductId                   
         LEFT JOIN                  
            ProductVariantMst PV                   
            ON PO.ProductVariantId = PV.ProductVariantId                   
      WHERE                  
         UserPurchaseHdrId = uph.UserPurchaseHdrId FOR JSON AUTO                   
 )                  
   AS ProductsDetails,          
   uph.Notes AS 'Order Notes',      
   uph.UserPurchaseHdrId As OrderId,      
   uph.CourierTrackingNo,  
   '' As CourierCompany  
  FROM                  
            UserPurchaseHdr as uph                   
            INNER JOIN UserMst as um on uph.UserId = um.UserId                   
            LEFT JOIN OrderStatusMst as osm on uph.OrderStatusId = osm.OrderStatusId                   
            LEFT JOIN FranchiseeMst as fm on uph.FranchiseeId = fm.FranchiseeId                   
            LEFT JOIN OrderReceivedByMst as orbm on uph.OrderReceivedById = orbm.OrderReceivedById                   
            LEFT JOIN CityMst as ct on um.CityId = ct.CityId                   
            LEFT JOIN StateMst as st on st.StateId = ct.StateId                   
     LEFT JOIN CountryMst as cm on cm.CountryId = st.CountryId                   
         WHERE                  
            uph.OrderDate >= @startDate                   
            and uph.OrderDate <= @endDate                   
            AND uph.IsRepurchaseOrder =                   
            CASE                  
               WHEN                  
                  @isRepurchaseOrder = 0                   
               THEN                  
                  uph.IsRepurchaseOrder                   
               WHEN                  
                  @isRepurchaseOrder = 1                   
               THEN                  
                  'TRUE'                   
               WHEN                  
                  @isRepurchaseOrder = 2                   
               THEN                  
                  'FALSE'                   
               ELSE                  
                  uph.IsRepurchaseOrder                   
            END                  
         ORDER BY                  
            uph.OrderDate                    
                  
/*                  
 --Declare @UserId int=9                    
 Select '' AS 'Waybill', uph.OrderNo 'Reference No',um.FullName as 'Consignee Name',                    
 --um.PinCode                    
 case when ct.CityName IS NULL then (select TOP 1 CITY from [dbo].[Pincodes] WHERE PINCODE=um.PinCode) else ct.CityName end as 'City',                    
 case when st.StateName IS NULL then (select TOP 1 State from [dbo].[Pincodes] WHERE PINCODE=um.PinCode) else st.StateName end as 'State',                    
 case when cm.CountryName IS NULL then (select TOP 1 Country from [dbo].[Pincodes] WHERE PINCODE=um.PinCode) else cm.CountryName end as 'Country',                     
 case when len(ltrim(rtrim((ISNULL(uph.DeliveryAddress,'')))))>0 then uph.DeliveryAddress else um.Address end as Address,                    
 um.PinCode,um.MobileNo as 'Phone', '' AS Weight,                     
 CASE WHEN ISNULL(osm.OrderStatusId,0)=1 then 'COD' WHEN ISNULL(osm.OrderStatusId,0)=5 then 'Prepaid'                    
 ELSE osm.Name END as 'Payment Mode',                    
 (CASE WHEN ISNULL(IsRepurchaseOrder,1)=1 THEN 'Repurchase' ELSE 'Direct' END) AS 'Order Type',                    
  (CASE WHEN ISNULL(osm.OrderStatusId,0)=1 THEN CAST(uph.TotalOrderAmount AS VARCHAR(50)) ELSE '' END) AS 'Cod Amount',                     
  '' AS 'Product to be Shipped',                    
 'Maxener wellness , behind ujala avenue , nr. Dena bank , vishala circle , saarkhej narol road ahmedbad' AS 'Return Address', '380055' AS 'Return Pin',                     
 (select ISNULL(PV.VariantName,'') AS VariantName, PM.ProductName, ISNULL(PM.WeightGM,0) AS WeightGM, ISNULL(PO.Qty,0) AS Qty from UserPurchaseDtl PO                    
    INNER JOIN ProductMst PM ON PO.ProductId = PM.ProductId                    
    LEFT JOIN ProductVariantMst PV ON PO.ProductVariantId = PV.ProductVariantId                    
    WHERE UserPurchaseHdrId=uph.UserPurchaseHdrId FOR JSON AUTO                    
 ) AS ProductsDetails                     
 from UserPurchaseHdr as uph                      
 --INNER JOIN (                    
 --  SELECT UserPurchaseHdrId,SUM(ISNULL(DirectPurchaseVolume,0)+ISNULL(RepurchaseMatchingVolume,0)) as BusinessVolume                     
 --  FROM UserPurchaseDtl                    
 --  group by UserPurchaseHdrId                    
 --) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId                    
 INNER JOIN UserMst as um on uph.UserId=um.UserId                    
 LEFT JOIN OrderStatusMst as osm on uph.OrderStatusId=osm.OrderStatusId                    
 LEFT JOIN FranchiseeMst as fm on uph.FranchiseeId=fm.FranchiseeId                    
 LEFT JOIN OrderReceivedByMst as orbm on uph.OrderReceivedById=orbm.OrderReceivedById                    
 LEFT JOIN CityMst as ct on um.CityId =ct.CityId                    
 LEFT JOIN StateMst as st on st.StateId =ct.StateId                    
 LEFT JOIN CountryMst as cm on cm.CountryId =st.CountryId                    
 WHERE uph.OrderDate >= @startDate and uph.OrderDate <= @endDate AND                    
 uph.IsRepurchaseOrder= CASE                     
                          WHEN @isRepurchaseOrder = 0 THEN uph.IsRepurchaseOrder                    
        WHEN @isRepurchaseOrder = 1 THEN 'TRUE'                    
        WHEN @isRepurchaseOrder = 2 THEN 'FALSE'                    
                    ELSE uph.IsRepurchaseOrder END  ORDER BY uph.OrderDate   */                   
                      
END   
  