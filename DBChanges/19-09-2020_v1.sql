Use AdvanceTRS_19Sep
GO
If(OBJECT_ID('tempdb..#Maintbl') Is Not Null)
Begin
    Drop Table #Maintbl
End

If(OBJECT_ID('tempdb..#UserPurchaseDtlIds') Is Not Null)
Begin
    Drop Table #UserPurchaseDtlIds
End

If(OBJECT_ID('tempdb..#TblResults') Is Not Null)
Begin
    Drop Table #TblResults
End

SELECT pcd.*,hdr.IsRepurchaseOrder,hdr.UserId AS OrderUserId INTO #Maintbl FROM [PointsCreditDtl] pcd INNER JOIN UserPurchaseDtl dtl ON pcd.UserPurchaseDtlId = dtl.UserPurchaseDtlId 
													  INNER JOIN UserPurchaseHdr hdr ON hdr.UserPurchaseHdrId = dtl.UserPurchaseHdrId 
WHERE pcd.CreatedDate >= '2020-09-17 23:53:00.000' AND pcd.CreatedDate <= GETDATE() AND hdr.OrderStatusId IN (4,5)

SELECT distinct UserPurchaseDtlId INTO #UserPurchaseDtlIds FROM #Maintbl

SELECT tbl.UserPurchaseDtlId,A.CreditPerc,A.CreditPoints,A.IsRepurchaseOrder,A.OrderUserId INTO #TblResults FROM #UserPurchaseDtlIds tbl
OUTER APPLY
(
	SELECT TOP 1 pcd.CreditPerc,pcd.CreditPoints,pcd.IsRepurchaseOrder,pcd.OrderUserId FROM #Maintbl pcd WHERE tbl.UserPurchaseDtlId = pcd.UserPurchaseDtlId
)A

--SELECT * FROM #TblResults

WHILE EXISTS (SELECT 1 FROM #TblResults)
BEGIN

	DECLARE @OrderUserId int,      
			@UserPurchaseDtlId bigint,    
			@CreditPerc float,    
			@CreditPoints numeric(18,2),    
			@includeSelf BIT

	SELECT TOP 1 @OrderUserId=OrderUserId,@UserPurchaseDtlId = UserPurchaseDtlId,@CreditPerc=CreditPerc,@CreditPoints=CreditPoints, @includeSelf = IsRepurchaseOrder FROM #TblResults
	PRINT 'UserPurchaseDtlId Id :'+ CONVERT(VARCHAR,@UserPurchaseDtlId)
	---------------------
	
	INSERT INTO [dbo].[PointsCreditDtl]    
           ([UserId]    
           ,[UserPurchaseDtlId]    
           ,[CreditPerc]    
           ,[CreditDate]    
           ,[CreditPoints]    
           ,[IsActive]    
           ,[CreatedBy]    
           ,[CreatedDate]) 
	SELECT t.UserId,@UserPurchaseDtlId,@CreditPerc,GETDATE(),@CreditPoints,1,@OrderUserId,GETDATE() FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@OrderUserId,@includeSelf) t
	WHERE NOT EXISTS (SELECT TOP 1 1 FROM #Maintbl pcd WHERE pcd.UserId = t.UserId AND pcd.UserPurchaseDtlId = @UserPurchaseDtlId) 

	------------------------
	PRINT 'Added ' + '- UserPurchaseDtlId:'+ CONVERT(VARCHAR,@UserPurchaseDtlId)
	
	DELETE FROM #TblResults WHERE UserPurchaseDtlId = @UserPurchaseDtlId
END

DROP TABLE #TblResults

GO
SELECT * INTO MatchingPointsCreditDtl_backup FROM MatchingPointsCreditDtl
SELECT * INTO PointsCreditDtl_backup FROM PointsCreditDtl
GO
SELECT COUNT(1) FROM MatchingPointsCreditDtl WHERE CreditDate >= '2020-09-17 00:00:00.000'
--DELETE FROM MatchingPointsCreditDtl WHERE CreditDate >= '2020-09-17 00:00:00.000'
