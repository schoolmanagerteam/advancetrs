Use AdvanceTRS_20Sep
GO
ALTER Procedure [dbo].[PROC_GET_USER_ORDER_HDR]        
(        
@UserId int=0,        
@FranchiseeId int=0        
)        
AS        
BEGIN        
 IF @FranchiseeId is null        
 BEGIN        
  SET @FranchiseeId=0        
 END         
 --Declare @UserId int=9        
 Select uph.UserPurchaseHdrId,uph.OrderNo,uph.TotalOrderAmount,uph.OrderDate,uph.FranchiseeId,        
   uph.OrderStatusId,        
   --fm.FranchiseeName ,fm.MobileNo as FranchiseeMobileNo,osm.Name as StatusName,        
   '' as FranchiseeName,null as FranchiseeMobileNo,osm.Name as StatusName,        
   um.FullName as CustomerName,um.MobileNo as CustomerMobileNo,        
   case when len(ltrim(rtrim((ISNULL(uph.DeliveryAddress,'')))))>0 then uph.DeliveryAddress else um.Address end as CustomerAddress,        
   uph.CourierTrackingNo,ISNULL(IsRepurchaseOrder,1)IsRepurchaseOrder,        
   ISNULL(BusinessVolume,0)BusinessVolume,um.RefferalCode,CONVERT(varchar(20),uph.OrderDate,103) as OdrDate,        
   CASE WHEN ISNULL(IsRepurchaseOrder,1)=1 then 'Repurchase' ELSE 'Direct' END as OrderType,        
   uph.OrderReceivedById,orbm.OrderReceivedBy,RazorpayOrderId,RazorpayPaymentId,RazorpaySignature, ISNULL(uph.Notes,'')AS Notes,    
   uph.IsOrderPlaceByAdmin  
 from UserPurchaseHdr as uph          
 INNER JOIN (        
   SELECT UserPurchaseHdrId,SUM(ISNULL(DirectPurchaseVolume,0)+ISNULL(RepurchaseMatchingVolume,0)) as BusinessVolume         
   FROM UserPurchaseDtl        
   group by UserPurchaseHdrId        
 ) as upd on uph.UserPurchaseHdrId=upd.UserPurchaseHdrId        
 INNER JOIN UserMst as um on uph.UserId=um.UserId        
 LEFT JOIN OrderStatusMst as osm on uph.OrderStatusId=osm.OrderStatusId        
 LEFT JOIN FranchiseeMst as fm on uph.FranchiseeId=fm.FranchiseeId        
 LEFT JOIN OrderReceivedByMst as orbm on uph.OrderReceivedById=orbm.OrderReceivedById        
 where uph.UserId=case when @UserId=0 then uph.UserId else @UserId end         
  and ISNULL(uph.FranchiseeId,0)=case when @FranchiseeId=0 then ISNULL(uph.FranchiseeId,0) else @FranchiseeId end        

END 