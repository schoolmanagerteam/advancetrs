Use AdvanceTRS_12Sep
GO
IF NOT EXISTS (SELECT TOP 1 1 FROM [ConfigMst] WHERE [Key] = 'ORDERGENBYADMINSTOPSMS')
BEGIN
INSERT INTO [dbo].[ConfigMst]
           ([Key]
           ,[Value]
           ,[IsActive]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[IsEditable]
           ,[Description])
     VALUES
           ('ORDERGENBYADMINSTOPSMS'
           ,'true'
           ,1
           ,1
           ,GETDATE()
           ,1
           ,GETDATE()
           ,1
           ,'Not sending Order notification SMS to user while Order place by admin')
END

GO
IF NOT EXISTS (
		SELECT TOP 1 1
		FROM Sys.Procedures
		WHERE name = 'PROC_INSERT_ALL_PARENT_USER_NOTIFICATIONMST'
			AND type IN (
				N'P'
				,N'PC'
				)
		)
BEGIN
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [PROC_INSERT_ALL_PARENT_USER_NOTIFICATIONMST] AS'
END
GO
ALTER PROCEDURE [dbo].[PROC_INSERT_ALL_PARENT_USER_NOTIFICATIONMST]    
(    
 @OrderUserId INT  
 ,@NotificationTypeId INT  
 ,@includeSelf BIT  
)    
AS    
BEGIN    
    
 IF OBJECT_ID('tempdb..#TblNotificationMst') IS NOT NULL  
 BEGIN  
  DROP TABLE #TblNotificationMst  
 END  
   
 Create Table #TblNotificationMst  
 (  
  NotificationTypeId int  
  ,UserId int  
  ,[Notification] nvarchar(max)  
  ,[IsActive] BIT  
  ,CreatedBy numeric(18,0)  
  ,CreatedDate smalldatetime  
  ,ModifiedBy numeric(18,0)  
  ,ModifiedDate smalldatetime  
 )    
  
  INSERT INTO #TblNotificationMst  
  ([NotificationTypeId]  
     ,[UserId]  
     ,[Notification]  
     ,[IsActive]  
     ,[CreatedBy]  
     ,[CreatedDate]  
     ,[ModifiedBy]  
     ,[ModifiedDate])  
  SELECT   
  @NotificationTypeId  
  ,UserId  
  ,'You Got New Income In Your Wallet Check Now'  
  ,1  
  ,@OrderUserId  
  ,GETDATE()  
  ,@OrderUserId  
  ,GETDATE()  
 FROM   
  dbo.[FN_GET_ALL_PARENT_USER_TREE](@OrderUserId,@includeSelf)  
  
 INSERT INTO [dbo].[NotificationMst]  
           ([NotificationTypeId]  
           ,[UserId]  
           ,[Notification]  
           ,[IsActive]  
           ,[CreatedBy]  
           ,[CreatedDate]  
           ,[ModifiedBy]  
           ,[ModifiedDate])  
     SELECT [NotificationTypeId]  
           ,[UserId]  
           ,[Notification]  
           ,[IsActive]  
           ,[CreatedBy]  
           ,[CreatedDate]  
           ,[ModifiedBy]  
           ,[ModifiedDate]  
  FROM #TblNotificationMst  
    
END  

GO

IF NOT EXISTS (
		SELECT TOP 1 1
		FROM Sys.Procedures
		WHERE name = 'PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL'
			AND type IN (
				N'P'
				,N'PC'
				)
		)
BEGIN
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL] AS'
END
GO
ALTER PROCEDURE [dbo].[PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL]      
(      
 @OrderUserId int,      
 @UserPurchaseDtlId bigint,    
 @CreditPerc float,    
 @CreditPoints numeric(18,2),    
 @includeSelf BIT    
)      
AS      
BEGIN      
      
 IF OBJECT_ID('tempdb..#TblPointsCreditDtl') IS NOT NULL    
 BEGIN    
  DROP TABLE #TblPointsCreditDtl    
 END    
     
     
 Create Table #TblPointsCreditDtl    
 (    
  UserId int    
  ,UserPurchaseDtlId bigint    
  ,CreditPerc float    
  ,CreditDate datetime    
  ,CreditPoints numeric(18,2)    
  ,IsActive bit    
  ,CreatedBy numeric(18,0)    
  ,CreatedDate datetime    
 )      
    
    
  INSERT INTO #TblPointsCreditDtl    
  (UserId,UserPurchaseDtlId,CreditPerc,CreditDate,CreditPoints,IsActive,CreatedBy,CreatedDate)    
  SELECT UserId,@UserPurchaseDtlId,@CreditPerc,GETDATE(),@CreditPoints,1,@OrderUserId,GETDATE() FROM dbo.[FN_GET_ALL_PARENT_USER_TREE](@OrderUserId,@includeSelf)    
    
 INSERT INTO [dbo].[PointsCreditDtl]    
           ([UserId]    
           ,[UserPurchaseDtlId]    
           ,[CreditPerc]    
           ,[CreditDate]    
           ,[CreditPoints]    
           ,[IsActive]    
           ,[CreatedBy]    
           ,[CreatedDate])    
     SELECT [UserId]    
           ,[UserPurchaseDtlId]    
           ,[CreditPerc]    
           ,[CreditDate]    
           ,[CreditPoints]    
           ,[IsActive]    
           ,[CreatedBy]    
           ,[CreatedDate]    
  FROM #TblPointsCreditDtl    
      
END 

GO

--SELECT * FROM [FN_GET_ALL_PARENT_USER_TREE_opt](17569,1)    
--SELECT * FROM [FN_GET_ALL_PARENT_USER_TREE](17569,1)  
ALTER Function [dbo].[FN_GET_ALL_PARENT_USER_TREE]    
(    
 @UserId int,    
 @IncludeSelf bit = 1    
)    
RETURNS @Usertbl table(UserId int, FullName NVARCHAR(MAX),RefferalCode NVARCHAR(MAX),Position CHAR(10))    
AS    
BEGIN    
  
 Declare @ActualUserId int = @UserId;   
 WITH tblChild AS  
 (  
  SELECT UserMst.UserId  
   FROM UserMst WHERE LeftUserId = @ActualUserId  
  UNION ALL  
  SELECT UserMst.UserId FROM UserMst JOIN tblChild ON (UserMst.LeftUserId = tblChild.UserId)  
 )  
 insert into @Usertbl(UserId,FullName,RefferalCode,Position)  
 SELECT t.UserId,um.FullName,um.RefferalCode,um.Position FROM tblChild t INNER JOIN UserMst um on t.UserId = um.UserId  
 OPTION(MAXRECURSION 32767)  
  
 SET @ActualUserId = @UserId;   
 WITH tblChild AS  
 (  
  SELECT UserMst.UserId  
   FROM UserMst WHERE RightUserId = @ActualUserId  
  UNION ALL  
  SELECT UserMst.UserId FROM UserMst JOIN tblChild ON (UserMst.RightUserId = tblChild.UserId)  
 )  
 insert into @Usertbl(UserId,FullName,RefferalCode,Position)  
 SELECT t.UserId,um.FullName,um.RefferalCode,um.Position FROM tblChild t INNER JOIN UserMst um on t.UserId = um.UserId  
 OPTION(MAXRECURSION 32767)  
   
 IF(@IncludeSelf = 1)  
 BEGIN  
  insert into @Usertbl(UserId,FullName,RefferalCode,Position)  
  SELECT um.UserId,um.FullName,um.RefferalCode,um.Position FROM UserMst um WHERE um.UserId = @UserId  
 END  
  
RETURN   
  
/*  
   Declare @ActualUserId int=@UserId    
  Declare @tbl table(UserId int, ChildUserId int, FullName nvarchar(max),UserLevel int);    
    
  --Declare @UserId int=8    
  Declare @LoopBreak bit=0    
  Declare @UserList table(UserId int)    
    
  While @LoopBreak=0    
  BEGIN    
   insert into @UserList values(@UserId)    
      
   Declare @Position char(1)    
   Select @Position=Position from UserMst where UserId=@UserId    
   IF @Position='L'    
   BEGIN    
    Select @UserId=UserId from UserMst where LeftUserId=@UserId    
   END    
   ELSE IF @Position='R'    
   BEGIN    
    Select @UserId=UserId from UserMst where RightUserId=@UserId    
   END     
   ELSE    
    SET @LoopBreak=1    
  END    
    
     
  IF @IncludeSelf=0    
  BEGIN    
   insert into @Usertbl    
   Select ul.UserId,um.FullName,um.RefferalCode,um.Position    
   from @UserList as ul    
   INNER JOIN UserMst as um on ul.UserId=um.UserId    
   where ul.UserId<>@ActualUserId    
  END    
  ELSE    
  BEGIN    
   insert into @Usertbl    
   Select ul.UserId,um.FullName,um.RefferalCode,um.Position    
   from @UserList as ul    
   INNER JOIN UserMst as um on ul.UserId=um.UserId    
  END    
    
  RETURN      
*/  
  
END    


