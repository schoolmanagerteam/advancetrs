//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class SMSSendDtl
    {
        public decimal SMSSendId { get; set; }
        public string SMSText { get; set; }
        public decimal MobileNo { get; set; }
        public System.DateTime SendDate { get; set; }
        public Nullable<bool> SendSuccess { get; set; }
        public bool IsActive { get; set; }
        public string SMSResponseId { get; set; }
        public string SMSSendType { get; set; }
    }
}
