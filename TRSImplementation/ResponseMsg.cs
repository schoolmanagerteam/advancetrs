﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRSImplementation
{
    public class ResponseMsg
    {
        public ResponseMsg()
        {

        }

        public bool Key { get; set; }
        public int Id { get; set; }
        public string Msg { get; set; }
        public bool IsInfo { get; set; }
    }
    public class AddCartResponse : ResponseMsg
    {
        public bool IsRegistrationActivated { get; set; }
        public string RegistrationPoints { get; set; }
    }
    public class LoginResponse : ResponseMsg
    {
        public bool IsProfileCompleted { get; set; }
        public bool IsRegistrated { get; set; }
    }

    public class AutoCompleteEnt
    {
        public string key { get; set; }
        public string val { get; set; }
    }

    public class SMSResponse
    {
        public List<SMSMessage> messages { get; set; }
    }

    public class SMSMessage
    {
        public string to { get; set; }
        public SMSStatus status { get; set; }
        public string messageId { get; set; }
        public string smsCount { get; set; }

    }
    public class SMSStatus
    {
        public string groupId { get; set; }
        public string groupName { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        
    }

    public class SMSSendDetails
    {
        public Int64 MobileNo { get; set; }
        public string Msg { get; set; }
    }
}
