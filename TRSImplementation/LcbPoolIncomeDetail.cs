//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class LcbPoolIncomeDetail
    {
        public long LcbPoolIncomeDetailId { get; set; }
        public long LcbPoolIncomeId { get; set; }
        public int DirectRefferalUserId { get; set; }
        public decimal LcbPoolIncomeAmt { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string LevelDetail { get; set; }
        public Nullable<decimal> MatchingBV { get; set; }
    
        public virtual LcbPoolIncome LcbPoolIncome { get; set; }
    }
}
