﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static TRSImplementation.clsImplementationEnum;
using Elmah;
using System.Configuration;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data;
using System.Reflection;

namespace TRSImplementation
{
    public class Manager : clsBase
    {
        public Manager()
        {
        }

        public void InsertUserPointsOnPurchase(TRSEntities context, UserPurchaseDtl objUserPurchaseDtl, int UserId, int RefferalUserId, WalletCreditType walletType, bool isPointsCreditdtlGenerateWhilePlaceOrder)
        {
            try
            {
                var ServerDate = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                if (walletType == WalletCreditType.Repurchase)
                {
                    var objProductMst = db.ProductMsts.Where(i => i.ProductId == objUserPurchaseDtl.ProductId).FirstOrDefault();
                    var MatchingPerc = Convert.ToDouble(objProductMst.RepurchaseMatchingPerc);

                    double RepurchaseMatching = 0;
                    RepurchaseMatching = (Int64)(Convert.ToDouble((objProductMst.RepurchaseMatchingAmt.Value * objProductMst.RepurchaseMatchingPerc) / 100));
                    var CreditAmount = (decimal)(decimal.Round((decimal)(RepurchaseMatching * objUserPurchaseDtl.Qty), 0, MidpointRounding.AwayFromZero));
                    objUserPurchaseDtl.RepurchaseMatchingVolume = objProductMst.RepurchaseMatchingAmt.Value * objUserPurchaseDtl.Qty;

                    if (objUserPurchaseDtl.UserPurchaseDtlId > 0 && isPointsCreditdtlGenerateWhilePlaceOrder)
                        db.PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL(UserId, objUserPurchaseDtl.UserPurchaseDtlId, MatchingPerc, CreditAmount, true);
                }
                else
                {
                    var objProductMst = db.ProductMsts.Where(i => i.ProductId == objUserPurchaseDtl.ProductId).FirstOrDefault();
                    var DirectMatchingPerc = Convert.ToDouble(objProductMst.DirectMatchingPerc);

                    double DirectMatching = 0;
                    DirectMatching = (Int64)(Convert.ToDouble((objProductMst.DirectMatchingAmt.Value * objProductMst.DirectMatchingPerc) / 100));
                    var CreditAmount = (decimal)(decimal.Round((decimal)(DirectMatching * objUserPurchaseDtl.Qty), 0, MidpointRounding.AwayFromZero));
                    objUserPurchaseDtl.DirectMatchingVolume = objProductMst.DirectMatchingAmt.Value * objUserPurchaseDtl.Qty;

                    if (objUserPurchaseDtl.UserPurchaseDtlId > 0 && isPointsCreditdtlGenerateWhilePlaceOrder)
                        db.PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL(UserId, objUserPurchaseDtl.UserPurchaseDtlId, DirectMatchingPerc, CreditAmount, false);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertUserPointsOnPurchase_Temp(TRSEntities context, UserPurchaseDtlTemp objUserPurchaseDtl, WalletCreditType walletType)
        {
            try
            {
                var ServerDate = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                if (walletType == WalletCreditType.Repurchase)
                {
                    var objProductMst = db.ProductMsts.Where(i => i.ProductId == objUserPurchaseDtl.ProductId).FirstOrDefault();
                    var MatchingPerc = Convert.ToDouble(objProductMst.RepurchaseMatchingPerc);

                    double RepurchaseMatching = 0;
                    RepurchaseMatching = (Int64)(Convert.ToDouble((objProductMst.RepurchaseMatchingAmt.Value * objProductMst.RepurchaseMatchingPerc) / 100));
                    var CreditAmount = (decimal)(decimal.Round((decimal)(RepurchaseMatching * objUserPurchaseDtl.Qty), 0, MidpointRounding.AwayFromZero));
                    objUserPurchaseDtl.RepurchaseMatchingVolume = objProductMst.RepurchaseMatchingAmt.Value * objUserPurchaseDtl.Qty;
                }
                else
                {
                    var objProductMst = db.ProductMsts.Where(i => i.ProductId == objUserPurchaseDtl.ProductId).FirstOrDefault();
                    var DirectMatchingPerc = Convert.ToDouble(objProductMst.DirectMatchingPerc);

                    double DirectMatching = 0;
                    DirectMatching = (Int64)(Convert.ToDouble((objProductMst.DirectMatchingAmt.Value * objProductMst.DirectMatchingPerc) / 100));
                    var CreditAmount = (decimal)(decimal.Round((decimal)(DirectMatching * objUserPurchaseDtl.Qty), 0, MidpointRounding.AwayFromZero));
                    objUserPurchaseDtl.DirectMatchingVolume = objProductMst.DirectMatchingAmt.Value * objUserPurchaseDtl.Qty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWalletCredit(TRSEntities context, UserPurchaseHdr objUserPurchaseHdr, int ActionBy, int RefferalUserId, WalletCreditType CreditTypeId, decimal ProductWalletAmountDeduct = 0)
        {
            try
            {
                WalletCreditDtl wallet = new WalletCreditDtl();
                wallet.CreatedBy = ActionBy;
                wallet.CreatedDate = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                wallet.CreditDate = wallet.CreatedDate;
                wallet.CreditType = Convert.ToInt32(CreditTypeId);
                wallet.IsActive = true;
                wallet.UserId = RefferalUserId;
                wallet.UserPurchaseHdrId = objUserPurchaseHdr.UserPurchaseHdrId;
                decimal CreditAmount = 0;
                foreach (var item in objUserPurchaseHdr.UserPurchaseDtls)
                {
                    var objProductMst = db.ProductMsts.Where(i => i.ProductId == item.ProductId).FirstOrDefault();
                    double DirectAmt = 0;
                    DirectAmt = (Int64)(Convert.ToDouble((objProductMst.DirectPurchaseAmt.Value * objProductMst.DirectPurchasePerc) / 100));
                    CreditAmount += (decimal.Round((decimal)(DirectAmt * item.Qty), 0, MidpointRounding.AwayFromZero));
                    item.DirectPurchaseVolume = objProductMst.DirectPurchaseAmt.Value * item.Qty;
                }

                wallet.CreditAmount = decimal.Round(CreditAmount, 0, MidpointRounding.AwayFromZero);
                context.WalletCreditDtls.Add(wallet);
                if (CreditTypeId == WalletCreditType.Direct)
                {
                    InsertCompanyShareCredit(context, objUserPurchaseHdr, ActionBy, RefferalUserId);
                }

                NotificationMst objNotification = new NotificationMst();
                objNotification.CreatedBy = ActionBy;
                objNotification.CreatedDate = wallet.CreatedDate;
                objNotification.IsActive = true;
                objNotification.ModifiedBy = ActionBy;
                objNotification.ModifiedDate = wallet.CreatedDate;
                objNotification.Notification = "You Got New Income In Your Wallet Check Now";
                objNotification.NotificationTypeId = NotificationType.Information.GetHashCode();
                objNotification.UserId = RefferalUserId;
                context.NotificationMsts.Add(objNotification);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertCompanyShareCredit(TRSEntities context, UserPurchaseHdr objUserPurchaseHdr, int ActionBy, int RefferalUserId)
        {
            try
            {
                var objWeekDatas = context.PROC_GET_FIRST_LAST_DATE_OF_WEEK().FirstOrDefault();
                //var TotalRefferalUsers = context.UserMsts.Where(i => i.RefferalUserId == RefferalUserId && i.IsRegistrationActivated == true && i.RegistrationDate >= objWeekDatas.StartDate && i.RegistrationDate <= objWeekDatas.EndDate).ToList();
                //var NoOfMemberForCompanyShare = Convert.ToInt32(context.ConfigMsts.Where(i => i.Key == "NoOfMemberForCompanyShare").FirstOrDefault().Value);
                List<string> listConfigKeys = new List<string>();
                listConfigKeys.Add(ConfigKeys.VoltIncomeBV);
                listConfigKeys.Add(ConfigKeys.VoltIncomeAmount);
                listConfigKeys.Add(ConfigKeys.CompanySharePerc);
                var listConfig = GetConfigValues(listConfigKeys);

                double VoltIncomeBV = Convert.ToDouble(GetConfigValueFromConfigList(listConfig, ConfigKeys.VoltIncomeBV));
                decimal VoltIncomeAmount = Convert.ToDecimal(GetConfigValueFromConfigList(listConfig, ConfigKeys.VoltIncomeAmount));
                double CompanySharePerc = Convert.ToDouble(GetConfigValueFromConfigList(listConfig, ConfigKeys.CompanySharePerc));
                var DirectBV = GetDirectBVValues(RefferalUserId, objWeekDatas.StartDate.Value, objWeekDatas.EndDate.Value);
                var objUserMst = context.UserMsts.Where(i => i.UserId == RefferalUserId).FirstOrDefault();
                double NoOfVolts = 0;
                if (objUserMst.NoOfVolts.HasValue)
                {
                    NoOfVolts = objUserMst.NoOfVolts.Value;
                }
                else
                {
                    NoOfVolts = 1;
                }
                var listDirectAmount = db.PROC_RPT_GET_COMPANY_SHARE_AMOUNT(RefferalUserId, objWeekDatas.StartDate.Value, objWeekDatas.EndDate.Value).ToList();

                foreach (var item in objUserPurchaseHdr.UserPurchaseDtls)
                {
                    DirectBV += (item.DirectPurchaseVolume.HasValue ? item.DirectPurchaseVolume.Value : 0);
                }
                var TotalVoltNeedToBeInAccount = (int)(DirectBV / Convert.ToDouble(VoltIncomeBV));
                var TotalVoltInAccount = listDirectAmount.Count;
                if (TotalVoltNeedToBeInAccount >= (TotalVoltInAccount + 1))
                {
                    while (TotalVoltNeedToBeInAccount > TotalVoltInAccount)
                    {
                        CompanyShareCreditDtl objCompanyShare = new CompanyShareCreditDtl();
                        objCompanyShare.CreatedBy = ActionBy;
                        objCompanyShare.CreatedDate = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objCompanyShare.CreditDate = objCompanyShare.CreatedDate;
                        objCompanyShare.IsActive = true;
                        objCompanyShare.UserId = RefferalUserId;
                        objCompanyShare.UserPurchaseHdrId = objUserPurchaseHdr.UserPurchaseHdrId;
                        objCompanyShare.CreditAmt = decimal.Round(VoltIncomeAmount, 2, MidpointRounding.AwayFromZero);
                        objCompanyShare.CreditPerc = CompanySharePerc;
                        objUserPurchaseHdr.CompanyShareCreditDtls.Add(objCompanyShare);
                        TotalVoltInAccount++;
                        NoOfVolts++;
                    }
                    objUserMst.NoOfVolts = NoOfVolts;
                }

                //var TotalRefferalCnt = TotalRefferalUsers.Count + 1;    // added 1 for current register user count for refferal registered user
                //if (TotalRefferalCnt % NoOfMemberForCompanyShare == 0)
                //{
                //    var CompanySharePerc = Convert.ToDouble(context.ConfigMsts.Where(i => i.Key == "CompanySharePerc").FirstOrDefault().Value);

                //    decimal CompanyShareAmount = 0;
                //    CompanyShareAmount = 200;//(decimal)((objUserPurchaseHdr.TotalOrderAmount * CompanySharePerc) / 100);

                //    CompanyShareCreditDtl objCompanyShare = new CompanyShareCreditDtl();
                //    objCompanyShare.CreatedBy = ActionBy;
                //    objCompanyShare.CreatedDate = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                //    objCompanyShare.CreditDate = objCompanyShare.CreatedDate;
                //    objCompanyShare.IsActive = true;
                //    objCompanyShare.UserId = RefferalUserId;
                //    objCompanyShare.UserPurchaseHdrId = objUserPurchaseHdr.UserPurchaseHdrId;
                //    objCompanyShare.CreditAmt = decimal.Round(CompanyShareAmount, 2, MidpointRounding.AwayFromZero);
                //    objCompanyShare.CreditPerc = CompanySharePerc;
                //    objUserPurchaseHdr.CompanyShareCreditDtls.Add(objCompanyShare);
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GenerateOrderNo(bool isUserPurchaseHdrTemps = false)
        {
            var orderNo = string.Empty;
            try
            {
                var ServerDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

                var Month = ServerDate.ToString("MM");
                var Year = ServerDate.Year.ToString().Substring(2, 2);
                long DbOrderNo = 0;

                if (isUserPurchaseHdrTemps)
                    DbOrderNo = db.UserPurchaseHdrTemps.Where(i => i.OrderDate.Month == ServerDate.Month && i.OrderDate.Year == ServerDate.Year).Max(i => i.OrderNo);
                else
                    DbOrderNo = db.UserPurchaseHdrs.Where(i => i.OrderDate.Month == ServerDate.Month && i.OrderDate.Year == ServerDate.Year).Max(i => i.OrderNo);

                if (DbOrderNo > 0)
                {
                    orderNo = Convert.ToString(DbOrderNo + 1);
                }
                else
                    orderNo = Year + Month + "0001";

                //long lngOrderNo = Convert.ToInt64(orderNo);
                //if (isUserPurchaseHdrTemps)
                //{
                //    while (db.UserPurchaseHdrTemps.Any(p => p.OrderNo == lngOrderNo))
                //    {
                //        orderNo = GenerateOrderNo(isUserPurchaseHdrTemps);
                //        lngOrderNo = Convert.ToInt64(orderNo);
                //    }
                //}
                //else
                //{
                //    while (db.UserPurchaseHdrs.Any(p => p.OrderNo == lngOrderNo))
                //    {
                //        orderNo = GenerateOrderNo(isUserPurchaseHdrTemps);
                //        lngOrderNo = Convert.ToInt64(orderNo);
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return orderNo;
        }

        //public string GeneratePointsOrderNo()
        //{
        //    var orderNo = "";
        //    try
        //    {
        //        var ServerDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

        //        var Month = ServerDate.ToString("MM");
        //        var Year = ServerDate.Year.ToString().Substring(2, 2);
        //        var objMaxOrderNo = db.PointsPurchaseHdrs.Where(i => i.OrderDate.Month == ServerDate.Month && i.OrderDate.Year == ServerDate.Year).OrderByDescending(i => i.OrderNo).FirstOrDefault();
        //        if (objMaxOrderNo != null)
        //        {
        //            orderNo = Year + Month + (Convert.ToInt32(objMaxOrderNo.OrderNo.ToString().Substring(4, 5)) + 1).ToString("00000");
        //        }
        //        else
        //        {
        //            orderNo = Year + Month + "00001";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return orderNo;
        //}
        public string GenerateRefferalCode()
        {
            var refferalCode = "";
            try
            {
                var ServerDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;

                var Month = ServerDate.ToString("MM");
                var Year = ServerDate.Year.ToString().Substring(2, 2);
                var objMaxRefCode = db.UserMsts.Where(i => i.RegMonth == ServerDate.Month && i.RegYear == ServerDate.Year).OrderByDescending(i => i.UserId).FirstOrDefault();
                if (objMaxRefCode != null)
                {
                    string temp = Convert.ToString(Convert.ToInt32(objMaxRefCode.RefferalCode.Substring(4, objMaxRefCode.RefferalCode.Length - 4)) + 1);
                    string strFormat = string.Empty;

                    if (temp.Length < 4)
                        strFormat = string.Format("D{0}", 4);
                    else
                        strFormat = string.Format("D{0}", temp.Length);

                    refferalCode = Year + Month + (Convert.ToInt32(temp)).ToString(strFormat);
                }
                else
                {
                    refferalCode = Year + Month + "0001";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return refferalCode;
        }

        //public Int64 GetFinalProductWalletAmountForUser(int UserId)
        //{
        //    decimal FinalAmount = 0;
        //    try
        //    {
        //        var ProductwalletCreditAmt = db.ProductWalletCreditDtls.Where(i => i.UserId == UserId).ToList().Sum(i => i.CreditAmount);
        //        var ProductwalletDebitAmt = db.ProductWalletDebitDtls.Where(i => i.UserId == UserId).ToList().Sum(i => i.DebitAmount);

        //        FinalAmount = ProductwalletCreditAmt - ProductwalletDebitAmt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return (Int64)FinalAmount;
        //}

        public TempProductWalletDebitDtl InsertTempProductWalletDebitDtl(int UserId, decimal OrderAmount)
        {
            TempProductWalletDebitDtl tempdtl = new TempProductWalletDebitDtl();
            try
            {
                var ProductAmount = 0;//GetFinalProductWalletAmountForUser(UserId);
                if (ProductAmount > 0)
                {
                    tempdtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                    tempdtl.OrderAmount = OrderAmount;

                    decimal FinalAmount = 0;
                    if (ProductAmount < OrderAmount)
                    {
                        FinalAmount = ProductAmount;
                    }
                    else { FinalAmount = OrderAmount; }
                    tempdtl.ProductWalletAmount = FinalAmount;
                    tempdtl.UserId = UserId;
                    db.TempProductWalletDebitDtls.Add(tempdtl);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tempdtl;
        }

        public bool InsertWalletDebitDtl(int ActionBy, int UserId, double DirectIncomePay, double PointsIncomePay, string UTRRefNo, double TotalVoltIncome, double TotalLeaderIncome, double TotalUplineSupportIncome, double TotalSilverIncome, double TotalDiamondIncome, double UplineMonthlyPay, double RetailRewardMonthlyPay, ref double NetPayable, DateTime PayoutDate)
        {
            var Status = false;
            try
            {
                List<string> listConfigs = new List<string>();
                listConfigs.Add(ConfigKeys.TDSPercWithoutPAN);
                listConfigs.Add(ConfigKeys.TDSPercWithPAN);
                listConfigs.Add(ConfigKeys.AdminChargePerc);
                listConfigs.Add(ConfigKeys.TDSDEXUCTIONLIMIT);

                var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();
                var TDSPercWithoutPAN = "20";
                var TDSPercWithPAN = "5";
                var AdminChargePerc = "5";
                double TDSDEXUCTIONLIMIT = 14999;

                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithoutPAN.ToUpper()))
                {
                    TDSPercWithoutPAN = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithoutPAN.ToUpper()).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithPAN.ToUpper()))
                {
                    TDSPercWithPAN = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.TDSPercWithPAN.ToUpper()).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AdminChargePerc.ToUpper()))
                {
                    AdminChargePerc = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.AdminChargePerc.ToUpper()).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.TDSDEXUCTIONLIMIT.ToUpper()))
                {
                    TDSDEXUCTIONLIMIT = Convert.ToDouble(listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.TDSDEXUCTIONLIMIT.ToUpper()).FirstOrDefault().Value);
                }

                var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
                var TDSPercentage = !string.IsNullOrEmpty(objUserMst.PANNo) && !string.IsNullOrWhiteSpace(objUserMst.PANNo) ? TDSPercWithPAN : TDSPercWithoutPAN;
                var PaidAmount = DirectIncomePay + PointsIncomePay + TotalVoltIncome + TotalLeaderIncome + TotalUplineSupportIncome + TotalSilverIncome + TotalDiamondIncome + UplineMonthlyPay + RetailRewardMonthlyPay;

                Int64 TDS = 0;
                //if (TDSDEXUCTIONLIMIT < PaidAmount)
                TDS = (Int64)((PaidAmount * Convert.ToDouble(TDSPercentage)) / 100);

                var AdminCharge = (Int64)((PaidAmount * Convert.ToDouble(AdminChargePerc)) / 100);
                NetPayable = (Int64)(PaidAmount - AdminCharge - TDS);

                WalletDebitDtl objWalletDebotDtl = new WalletDebitDtl();
                objWalletDebotDtl.AccountNo = objUserMst.BankAccNo;
                objWalletDebotDtl.AdminCharge = AdminCharge;
                objWalletDebotDtl.AdminPerc = Convert.ToDouble(AdminChargePerc);
                objWalletDebotDtl.CreatedBy = ActionBy;
                objWalletDebotDtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                objWalletDebotDtl.DebitAmount = PaidAmount;
                objWalletDebotDtl.DebitDate = PayoutDate;
                objWalletDebotDtl.IsActive = true;
                objWalletDebotDtl.ISFCCode = objUserMst.ISFCCode;
                objWalletDebotDtl.NetPayable = NetPayable;
                objWalletDebotDtl.TDSAmount = TDS;
                objWalletDebotDtl.TDSPerc = Convert.ToDouble(TDSPercentage);
                objWalletDebotDtl.UserId = UserId;
                objWalletDebotDtl.UTRRefNo = UTRRefNo;
                objWalletDebotDtl.PointsIncomePay = PointsIncomePay;
                objWalletDebotDtl.DirectIncomePay = DirectIncomePay;
                objWalletDebotDtl.VoltIncomePay = TotalVoltIncome;
                objWalletDebotDtl.LeaderIncomePay = TotalLeaderIncome;
                objWalletDebotDtl.UplineSupportIncomePay = TotalUplineSupportIncome;
                objWalletDebotDtl.SilverIncomePay = TotalSilverIncome;
                objWalletDebotDtl.DiamondIncomePay = TotalDiamondIncome;
                objWalletDebotDtl.UplineMonthlyPay = UplineMonthlyPay;
                objWalletDebotDtl.RetailRewardMonthlyPay = RetailRewardMonthlyPay;
                db.WalletDebitDtls.Add(objWalletDebotDtl);

                //Upline Income Payout..
                var UplineIncomes = (from s in db.UplineIncomeSummaries
                            join sl in db.UplineIncomeDetails on s.UplineIncomeSummaryId equals sl.UplineIncomeSummaryId
                            where sl.UserId == UserId
                            && sl.IsPayout == false
                            && s.Month < PayoutDate.Month
                            && s.Year <= PayoutDate.Year
                            select sl).ToList();

                if (UplineIncomes != null && UplineIncomes.Any())
                {
                    long UplineIncomeSummaryId = UplineIncomes.FirstOrDefault().UplineIncomeSummaryId;
                    UplineIncomeSummary uplineIncomesummary = db.UplineIncomeSummaries.Where(p => p.UplineIncomeSummaryId == UplineIncomeSummaryId).FirstOrDefault();
                    uplineIncomesummary.IsLock = true;
                    uplineIncomesummary.UpdatedDate = DateTime.Now;

                    foreach (var item in UplineIncomes)
                    {
                        item.IsPayout = true;
                        item.UTRRefNo = UTRRefNo;
                        item.UpdatedDate = DateTime.Now;
                    }
                }

                //RetailReward Income Payout..
                var RetailRewards = (from s in db.RetailRewardIncomeSummaries
                                     join sl in db.RetailRewardIncomeDetails on s.RetailRewardIncomeSummaryId equals sl.RetailRewardIncomeSummaryId
                                     where sl.UserId == UserId
                                     && sl.IsPayout == false
                                     && s.Month < PayoutDate.Month
                                     && s.Year <= PayoutDate.Year
                                     select sl).ToList();

                if (RetailRewards != null && RetailRewards.Any())
                {
                    long RetailRewardIncomeSummaryId = RetailRewards.FirstOrDefault().RetailRewardIncomeSummaryId;
                    RetailRewardIncomeSummary retailRewardIncomeSummary = db.RetailRewardIncomeSummaries.Where(p => p.RetailRewardIncomeSummaryId == RetailRewardIncomeSummaryId).FirstOrDefault();
                    retailRewardIncomeSummary.IsLock = true;
                    retailRewardIncomeSummary.UpdatedDate = DateTime.Now;

                    foreach (var item in RetailRewards)
                    {
                        item.IsPayout = true;
                        item.UTRRefNo = UTRRefNo;
                        item.UpdatedDate = DateTime.Now;
                    }
                }
                
                db.SaveChanges();
                Status = true;
            }
            catch (Exception ex)
            {
                Status = false;
                throw ex;
            }
            return Status;
        }

        public bool InsertLedgerAndUplinePointsCreditDtl(int ActionBy, Int64 UserPurchaseHdrId, int RefferalUserId, List<int> listParentMemberDtl)
        {
            var Status = false;
            var IsRefferalGoldMember = false;
            try
            {
                var objUserPurchaseHdr = db.UserPurchaseHdrs.Where(i => i.UserPurchaseHdrId == UserPurchaseHdrId).FirstOrDefault();
                List<string> listConfigs = new List<string>();
                listConfigs.Add(ConfigKeys.GoldMemberPerc);
                listConfigs.Add(ConfigKeys.SilverMemberPerc);

                var listConfigDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();
                decimal GoldMemberPerc = 100;
                decimal SilverMemberPerc = 10;
                if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.GoldMemberPerc))
                {
                    GoldMemberPerc = Convert.ToDecimal(listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.GoldMemberPerc).FirstOrDefault().Value);
                }
                if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.SilverMemberPerc))
                {
                    SilverMemberPerc = Convert.ToDecimal(listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.SilverMemberPerc).FirstOrDefault().Value);
                }

                var objRefferalUserId = db.UserMsts.Where(i => i.UserId == RefferalUserId).FirstOrDefault();
                IsRefferalGoldMember = objRefferalUserId.IsRegistrationActivated.HasValue && objRefferalUserId.IsRegistrationActivated.Value;

                var listSilverMembers = db.UserMsts.Where(i => listParentMemberDtl.Contains(i.UserId) && i.IsSilverMember == true).ToList();
                List<LeaderAndUplinePointsCreditDtl> listLeaderAndUplinePointsCreditDtl = new List<LeaderAndUplinePointsCreditDtl>();
                foreach (var item in objUserPurchaseHdr.UserPurchaseDtls)
                {
                    var objPointsCreditDtl = db.PointsCreditDtls.Where(i => i.UserId == objUserPurchaseHdr.UserId && i.UserPurchaseDtlId == item.UserPurchaseDtlId).FirstOrDefault();
                    var GoldMemberPoint = (Int64)(Convert.ToDouble((objPointsCreditDtl.CreditPoints * GoldMemberPerc) / 100));
                    if (IsRefferalGoldMember)
                    {
                        var objGoldPoint = new LeaderAndUplinePointsCreditDtl();
                        objGoldPoint.CreatedBy = ActionBy;
                        objGoldPoint.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objGoldPoint.CreditDate = objGoldPoint.CreatedDate;
                        objGoldPoint.CreditPerc = (double)GoldMemberPerc;
                        objGoldPoint.CreditPoints = GoldMemberPoint;
                        objGoldPoint.IsActive = true;
                        objGoldPoint.IsLeaderPoint = true;
                        objGoldPoint.UserId = RefferalUserId;
                        //objGoldPoint.UserPurchaseDtlId = item.UserPurchaseDtlId;
                        listLeaderAndUplinePointsCreditDtl.Add(objGoldPoint);
                    }

                    foreach (var silver in listSilverMembers)
                    {
                        var objSilverPoint = new LeaderAndUplinePointsCreditDtl();
                        objSilverPoint.CreatedBy = ActionBy;
                        objSilverPoint.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objSilverPoint.CreditDate = objSilverPoint.CreatedDate;
                        objSilverPoint.CreditPerc = (double)SilverMemberPerc;
                        objSilverPoint.CreditPoints = (Int64)(Convert.ToDouble((GoldMemberPoint * SilverMemberPerc) / 100));
                        objSilverPoint.IsActive = true;
                        objSilverPoint.IsLeaderPoint = false;
                        objSilverPoint.UserId = silver.UserId;
                        //objSilverPoint.UserPurchaseDtlId = item.UserPurchaseDtlId;
                        listLeaderAndUplinePointsCreditDtl.Add(objSilverPoint);
                    }
                }
                db.LeaderAndUplinePointsCreditDtls.AddRange(listLeaderAndUplinePointsCreditDtl);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Status;
        }

        //public void InsertProductWalletDebitDtl(TRSEntities context, UserPurchaseHdr objUserPurchaseHdr, decimal ProductWalletAmountDeduct)
        //{
        //    try
        //    {
        //        if (ProductWalletAmountDeduct > 0)
        //        {
        //            ProductWalletDebitDtl objProductWalletDebitDtl = new ProductWalletDebitDtl();
        //            objProductWalletDebitDtl.CreatedBy = objUserPurchaseHdr.CreatedBy;
        //            objProductWalletDebitDtl.CreatedDate = objUserPurchaseHdr.CreatedDate;
        //            objProductWalletDebitDtl.DebitAmount = ProductWalletAmountDeduct;
        //            objProductWalletDebitDtl.DebitDate = objUserPurchaseHdr.OrderDate;
        //            objProductWalletDebitDtl.IsActive = true;
        //            objProductWalletDebitDtl.UserId = objUserPurchaseHdr.UserId;
        //            objProductWalletDebitDtl.UserPurchaseHdrId = objUserPurchaseHdr.UserPurchaseHdrId;
        //            context.ProductWalletDebitDtls.Add(objProductWalletDebitDtl);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public void InsertWalletCreditForFranchisee(TRSEntities context, UserPurchaseHdr objUserPurchaseHdr, int ActionBy)
        {
            try
            {
                if (objUserPurchaseHdr.FranchiseeId.HasValue)
                {
                    if (!db.FranchiseeWalletCreditDtls.Any(i => i.UserPurchaseHdrId == objUserPurchaseHdr.UserPurchaseHdrId))
                    {
                        var objFranchisee = db.FranchiseeMsts.Where(i => i.FranchiseeId == objUserPurchaseHdr.FranchiseeId).FirstOrDefault();
                        FranchiseeWalletCreditDtl wallet = new FranchiseeWalletCreditDtl();
                        wallet.CreatedBy = ActionBy;
                        wallet.CreatedDate = context.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        wallet.CreditDate = wallet.CreatedDate;
                        wallet.IsActive = true;
                        wallet.FranchiseeId = objUserPurchaseHdr.FranchiseeId.Value;
                        wallet.UserPurchaseHdrId = objUserPurchaseHdr.UserPurchaseHdrId;
                        wallet.Percentage = objFranchisee.Percentage.HasValue ? objFranchisee.Percentage.Value : 0;
                        decimal CreditAmount = 0;
                        CreditAmount = (decimal)((objUserPurchaseHdr.TotalOrderAmount * objFranchisee.Percentage) / 100);
                        wallet.CreditAmount = Convert.ToDouble(decimal.Round(CreditAmount, 2, MidpointRounding.AwayFromZero));
                        context.FranchiseeWalletCreditDtls.Add(wallet);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public bool InsertWalletDebitDtl(int ActionBy, int UserId, double PaidAmount, string UTRRefNo)
        //{
        //    var Status = false;
        //    try
        //    {
        //        var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
        //        var TDSPercentage =!string.IsNullOrEmpty(objUserMst.PANNo) && !string.IsNullOrWhiteSpace(objUserMst.PANNo)?TDSPerc:TDSPercWithoutPAN;
        //        var TDS = (Int64)((PaidAmount * TDSPercentage) / 100);
        //        var AdminCharge = (Int64)((PaidAmount * AdminChargePerc) / 100);
        //        var NetPayable = (Int64)(PaidAmount - AdminCharge - TDS);

        //        WalletDebitDtl objWalletDebotDtl = new WalletDebitDtl();
        //        objWalletDebotDtl.AccountNo = objUserMst.BankAccNo;
        //        objWalletDebotDtl.AdminCharge = AdminCharge;
        //        objWalletDebotDtl.AdminPerc = AdminChargePerc;
        //        objWalletDebotDtl.CreatedBy = ActionBy;
        //        objWalletDebotDtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
        //        objWalletDebotDtl.DebitAmount = PaidAmount;
        //        objWalletDebotDtl.DebitDate = objWalletDebotDtl.CreatedDate;
        //        objWalletDebotDtl.IsActive = true;
        //        objWalletDebotDtl.ISFCCode = objUserMst.ISFCCode;
        //        objWalletDebotDtl.NetPayable = NetPayable;
        //        objWalletDebotDtl.TDSAmount = TDS;
        //        objWalletDebotDtl.TDSPerc = TDSPercentage;
        //        objWalletDebotDtl.UserId = UserId;
        //        objWalletDebotDtl.UTRRefNo = UTRRefNo;
        //        db.WalletDebitDtls.Add(objWalletDebotDtl);
        //        db.SaveChanges();
        //        Status = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Status = false;
        //        throw ex;
        //    }
        //    return Status;
        //}
        //public bool InsertCashbackDtl(int ActionBy, int UserId, double PaidAmount, string UTRRefNo)
        //{
        //    var Status = false;
        //    try
        //    {
        //        var objUserMst = db.UserMsts.Where(i => i.UserId == UserId).FirstOrDefault();
        //        var TDSPercentage = !string.IsNullOrEmpty(objUserMst.PANNo) && !string.IsNullOrWhiteSpace(objUserMst.PANNo) ? TDSPerc : TDSPercWithoutPAN;
        //        var TDS = (Int64)((PaidAmount * TDSPercentage) / 100);
        //        var AdminCharge = (Int64)((PaidAmount * AdminChargePerc) / 100);
        //        var NetPayable = (Int64)(PaidAmount - AdminCharge - TDS);
        //        if (!db.CashbackDtls.Any(i => i.UserId == UserId))
        //        {
        //            CashbackDtl objCashbackDtl = new CashbackDtl();
        //            objCashbackDtl.AccountNo = objUserMst.BankAccNo;
        //            objCashbackDtl.AdminCharge = AdminCharge;
        //            objCashbackDtl.AdminPerc = AdminChargePerc;
        //            objCashbackDtl.CreatedBy = ActionBy;
        //            objCashbackDtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
        //            objCashbackDtl.CashbackAmount = PaidAmount;
        //            objCashbackDtl.CashbackDate = objCashbackDtl.CreatedDate;
        //            objCashbackDtl.IsActive = true;
        //            objCashbackDtl.ISFCCode = objUserMst.ISFCCode;
        //            objCashbackDtl.NetCashbackAmt = NetPayable;
        //            objCashbackDtl.TDSAmount = TDS;
        //            objCashbackDtl.TDSPerc = TDSPercentage;
        //            objCashbackDtl.UserId = UserId;
        //            objCashbackDtl.UTRRefNo = UTRRefNo;
        //            db.CashbackDtls.Add(objCashbackDtl);

        //            objUserMst.IsCashbackPayout = true;
        //            db.SaveChanges();
        //        }
        //        Status = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Status = false;
        //        throw ex;
        //    }
        //    return Status;
        //}
        public ResponseMsg SendMail(string ToMailId, string Subject, string Body)
        {
            List<string> listConfigs = new List<string>();
            listConfigs.Add(ConfigKeys.EmailFrom);
            listConfigs.Add(ConfigKeys.EmailHostName);
            listConfigs.Add(ConfigKeys.EmailPort);
            listConfigs.Add(ConfigKeys.EmailPwd);
            listConfigs.Add(ConfigKeys.EmailEnableSSLOnMail);
            var listConfigDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();
            var EmailFrom = "";
            var EmailHostName = "";
            var EmailPort = "";
            var EmailPwd = "";
            var EmailEnableSSLOnMail = false;
            if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.EmailFrom))
            {
                EmailFrom = listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.EmailFrom).FirstOrDefault().Value;
            }
            if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.EmailHostName))
            {
                EmailHostName = listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.EmailHostName).FirstOrDefault().Value;
            }
            if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.EmailPort))
            {
                EmailPort = listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.EmailPort).FirstOrDefault().Value;
            }
            if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.EmailPwd))
            {
                EmailPwd = listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.EmailPwd).FirstOrDefault().Value;
            }
            if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.EmailEnableSSLOnMail))
            {
                EmailEnableSSLOnMail = Convert.ToBoolean(listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.EmailEnableSSLOnMail).FirstOrDefault().Value);
            }

            ResponseMsg response = new ResponseMsg();
            MailMessage m = new MailMessage();
            SmtpClient sc = new SmtpClient();
            m.From = new MailAddress(EmailFrom);
            m.To.Add(ToMailId);
            m.Subject = Subject;
            m.Body = Body;
            m.IsBodyHtml = true;
            sc.Host = EmailHostName;
            string str2 = EmailFrom.ToLower();
            if (EmailFrom.Contains("gmail.com"))
            {
                try
                {
                    sc.Port = Convert.ToInt32(EmailPort);
                    sc.Credentials = new System.Net.NetworkCredential(EmailFrom, EmailPwd);
                    sc.EnableSsl = EmailEnableSSLOnMail;
                    sc.Send(m);
                    response.Msg = "Email Send successfully";
                    response.Key = true;
                }
                catch (Exception ex)
                {
                    response.Msg = "Please double check the From Address and Password to confirm that both of them are correct. ";
                    response.Msg += "If you are using gmail smtp to send email for the first time, please refer to this KB to setup your gmail account: http://www.smarterasp.net/support/kb/a1546/send-email-from-gmail-with-smtp-authentication-but-got-5_5_1-authentication-required-error.aspx?KBSearchID=137388";
                    throw ex;
                }
            }
            else
            {
                try
                {
                    sc.Port = Convert.ToInt32(EmailPort); ;
                    sc.Credentials = new System.Net.NetworkCredential(EmailFrom, EmailPwd);
                    sc.EnableSsl = EmailEnableSSLOnMail;
                    sc.Send(m);
                    response.Msg = "Email Send successfully";
                    response.Key = true;
                }
                catch (Exception ex)
                {
                    response.Msg = "Please double check the From Address and Password to confirm that both of them are correct.";
                    throw ex;
                }
            }
            return response;
        }

        public string Encrypt(string clearText)
        {
            string EncryptionKey = clsImplementationMessage.SecretKey;
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public string Decrypt(string cipherText)
        {
            string EncryptionKey = clsImplementationMessage.SecretKey;
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public string HigherEncrypt(string clearText)
        {
            string EncryptionKey = clsImplementationMessage.HigherSecretKey;
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public string HigherDecrypt(string cipherText)
        {
            string EncryptionKey = clsImplementationMessage.HigherSecretKey;
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public PaymentKeyDetails GetPaymentKeyDetails()
        {
            var keyDetails = new PaymentKeyDetails();
            var listConfig = db.ConfigMsts.ToList();
            try
            {
                var isTestModeActive = Convert.ToBoolean(ConfigurationManager.AppSettings["IsTestMode"].ToString());

                if (isTestModeActive)
                {
                    var PaymentKey = listConfig.Where(i => i.Key.ToUpper() == ("PaymentKeyTest").ToUpper()).FirstOrDefault().Value;
                    var PaymentSecret = listConfig.Where(i => i.Key.ToUpper() == ("PaymentSecretTest").ToUpper()).FirstOrDefault().Value;
                    keyDetails.PaymentKey = objManager.Decrypt(PaymentKey);
                    keyDetails.PaymentSecret = objManager.Decrypt(PaymentSecret);
                }
                else
                {
                    var PaymentKey = listConfig.Where(i => i.Key.ToUpper() == ("PaymentKey").ToUpper()).FirstOrDefault().Value;
                    var PaymentSecret = listConfig.Where(i => i.Key.ToUpper() == ("PaymentSecret").ToUpper()).FirstOrDefault().Value;
                    keyDetails.PaymentKey = objManager.Decrypt(PaymentKey);
                    keyDetails.PaymentSecret = objManager.Decrypt(PaymentSecret);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return keyDetails;
        }

        //public Int64 GetFinalUserPoints(int UserId)
        //{
        //    var objPointsCredit = db.PointsCreditDtls.Where(i => i.UserId == UserId && i.IsActive == true).ToList();
        //    var CreditPoints = (Int64)objPointsCredit.Sum(i => i.CreditPoints);
        //    var objPointsPurchase=db.PointsPurchaseHdrs.Where(i => i.UserId == UserId && i.IsActive == true).ToList();
        //    Int64 PurchasePoints = 0;
        //    Int64 FinalPoints = 0;
        //    if (objPointsPurchase.Count > 0)
        //    {
        //        PurchasePoints = (Int64)objPointsPurchase.Sum(i => i.TotalOrderAmount);
        //    }
        //    FinalPoints = CreditPoints - PurchasePoints;
        //    return FinalPoints;
        //}

        //public bool InsertPointsPurchase(int UserId,string pinfo)
        //{
        //    var Status = false;
        //    try
        //    {
        //        #region Purchase Orders
        //        double TotalAmount = 0;
        //        var SplitPackageInfo = objManager.Decrypt(pinfo).Split(',');
        //        var objPointsPurchaseHdr = new PointsPurchaseHdr();
        //        var listUserPurchaseDtl = new List<UserPurchaseDtl>();
        //        foreach (var packages in SplitPackageInfo)
        //        {
        //            var packageInfo = packages.Split('_');
        //            var packageid = Convert.ToInt32(packageInfo[0]);
        //            var qty = Convert.ToInt32(packageInfo[1]);
        //            var objPackageMst = db.PackageMsts.Where(i => i.PackageId == packageid).FirstOrDefault();

        //            TotalAmount += (qty * objPackageMst.PackagePrice.Value);
        //            var objPointsPurchaseDtl = new PointsPurchaseDtl();
        //            objPointsPurchaseDtl.Amount = (qty * objPackageMst.PackagePrice.Value);
        //            objPointsPurchaseDtl.CreatedBy = UserId;
        //            objPointsPurchaseDtl.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
        //            objPointsPurchaseDtl.IsActive = true;
        //            objPointsPurchaseDtl.PackageId = packageid;
        //            objPointsPurchaseDtl.PackagePrice = objPackageMst.PackagePrice.Value;
        //            objPointsPurchaseDtl.Qty = qty;
        //            objPointsPurchaseHdr.PointsPurchaseDtls.Add(objPointsPurchaseDtl);
        //        }
        //        objPointsPurchaseHdr.OrderNo = Convert.ToInt64(Convert.ToInt64(objManager.GeneratePointsOrderNo()));
        //        objPointsPurchaseHdr.CreatedBy = UserId;
        //        objPointsPurchaseHdr.CreatedDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
        //        objPointsPurchaseHdr.IsActive = true;
        //        objPointsPurchaseHdr.OrderDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
        //        objPointsPurchaseHdr.TotalOrderAmount = TotalAmount;
        //        objPointsPurchaseHdr.UserId = UserId;
        //        db.PointsPurchaseHdrs.Add(objPointsPurchaseHdr);
        //        db.SaveChanges();
        //        Status = true;
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return Status;
        //}

        public string GetTimespam()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }

        public DateTime ConvertDDMMYYToMMDDYY(string Date, string originalformat, string newformat)
        {
            DateTime datereturn = DateTime.MinValue;
            string[] datedata = new string[0];
            try
            {
                datedata = Date.Replace('-', '/').Split('/');
                if (datedata.Length == 0 || datedata.Length == 1)
                {
                    datedata = Date.Split('-');
                }
                if (originalformat.ToUpper() == DateFormats.DDMMYYYY.ToUpper())
                {
                    datereturn = new DateTime(Convert.ToInt32(datedata[2]), Convert.ToInt32(datedata[1]), Convert.ToInt32(datedata[0]));
                }
                else if (originalformat.ToUpper() == DateFormats.MMDDYYYY.ToUpper())
                {
                    datereturn = new DateTime(Convert.ToInt32(datedata[2]), Convert.ToInt32(datedata[0]), Convert.ToInt32(datedata[1]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return datereturn;
        }

        public void SendSMS(decimal? MobileNo, string Templateid, string Msg, string SMSSendType, bool isUsedForAuthentication = false)
        {
            if (!isUsedForAuthentication)
            {
                if (ConfigurationManager.AppSettings["IsTestMode"] != null && Convert.ToBoolean(ConfigurationManager.AppSettings["IsTestMode"].ToString()))
                {
                    MobileNo = ConfigurationManager.AppSettings["TestMobileNo"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestMobileNo"].ToString()) ? Convert.ToInt64(ConfigurationManager.AppSettings["TestMobileNo"].ToString()) : 0;
                }
            }
            if (MobileNo > 0 && MobileNo.ToString().Length == 10)
            {
                List<string> listConfigs = new List<string>();
                listConfigs.Add(ConfigKeys.SMSPassword);
                listConfigs.Add(ConfigKeys.SMSSenderId);
                listConfigs.Add(ConfigKeys.SMSURL);
                listConfigs.Add(ConfigKeys.SMSUserName);
                listConfigs.Add(ConfigKeys.ENABLESMSALERTAPI);
                listConfigs.Add(ConfigKeys.SMSALERTAPIURL);
                listConfigs.Add(ConfigKeys.SMSALERTAPIKEY);
                listConfigs.Add(ConfigKeys.INDIADLTPRINCIPALENTITYID);

                var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();
                var SMSURL = "";
                var SMSUserName = "";
                var SMSPassword = "";
                var SMSSenderId = "";
                bool EnableSMSAlertAPI = false;
                var SMSAlertAPIURL = "";
                var SMSAlertAPIKey = "";
                var INDIADLTPRINCIPALENTITYIDValue = "";

                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.INDIADLTPRINCIPALENTITYID))
                {
                    INDIADLTPRINCIPALENTITYIDValue = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.INDIADLTPRINCIPALENTITYID).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.SMSURL))
                {
                    SMSURL = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.SMSURL).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.SMSUserName))
                {
                    SMSUserName = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.SMSUserName).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.SMSPassword))
                {
                    SMSPassword = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.SMSPassword).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.SMSSenderId))
                {
                    SMSSenderId = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.SMSSenderId).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.SMSALERTAPIURL))
                {
                    SMSAlertAPIURL = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.SMSALERTAPIURL).FirstOrDefault().Value;
                }
                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.SMSALERTAPIKEY))
                {
                    SMSAlertAPIKey = listSMSDtl.Where(i => i.Key.ToUpper() == ConfigKeys.SMSALERTAPIKEY).FirstOrDefault().Value;
                }

                if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.ENABLESMSALERTAPI))
                {
                    try
                    {
                        EnableSMSAlertAPI = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.ENABLESMSALERTAPI).Value);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                if (EnableSMSAlertAPI)
                    SMSURL = SMSAlertAPIURL;

                if (SMSURL != "" && SMSUserName != "" && SMSPassword != "" && SMSSenderId != "")
                {
                    //string url = SMSURL + "?uname=" + SMSUserName + "&pass=" + SMSPassword + "&send=" + SMSSenderId + "&dest=91" + MobileNo.ToString() + "&msg=" + Msg + "&concat=1&intl=1";
                    string url = SMSURL;
                    if (EnableSMSAlertAPI)
                    {
                        url = string.Format(url, SMSAlertAPIKey, SMSSenderId, MobileNo, Msg);
                    }
                    else
                    {
                        //url = SMSURL + "?username=" + SMSUserName + "&password=" + SMSPassword + "&to=91" + MobileNo.ToString() + "&text=" + Msg;
                        url = string.Format(url, SMSUserName, SMSPassword, SMSSenderId, MobileNo.ToString(), Msg, Templateid, INDIADLTPRINCIPALENTITYIDValue);
                    }
                    HttpWebRequest httpreq = (HttpWebRequest)WebRequest.Create(url);
                    try
                    {
                        SMSSendDtl objSMSSendDtl = new SMSSendDtl();
                        objSMSSendDtl.IsActive = true;
                        objSMSSendDtl.MobileNo = (decimal)MobileNo;
                        objSMSSendDtl.SendDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                        objSMSSendDtl.SMSSendType = SMSSendType;
                        objSMSSendDtl.SMSText = Msg;
                        try
                        {
                            HttpWebResponse httpres = (HttpWebResponse)httpreq.GetResponse();
                            StreamReader sr = new StreamReader(httpres.GetResponseStream());
                            string results = sr.ReadToEnd();
                            sr.Close();

                            if (results.Length > 0)
                            {
                                var SMSResponseId = results.ToString();
                                if (EnableSMSAlertAPI)
                                {
                                    //var objSMSResponse = JsonConvert.DeserializeObject<SMSResponse>(SMSResponseId);
                                    //if (objSMSResponse != null && objSMSResponse.messages.FirstOrDefault() != null)
                                    //{
                                    //    objSMSSendDtl.SendSuccess = true;
                                    //    objSMSSendDtl.SMSResponseId = objSMSResponse.messages.FirstOrDefault().messageId.ToString();
                                    //}
                                }
                                else
                                {
                                    var objSMSResponse = JsonConvert.DeserializeObject<SMSResponse>(SMSResponseId);
                                    if (objSMSResponse != null && objSMSResponse.messages.FirstOrDefault() != null)
                                    {
                                        objSMSSendDtl.SendSuccess = true;
                                        objSMSSendDtl.SMSResponseId = objSMSResponse.messages.FirstOrDefault().messageId.ToString();
                                    }
                                }
                            }
                            db.SMSSendDtls.Add(objSMSSendDtl);
                            db.SaveChanges();
                        }
                        catch (WebException e)
                        {
                            using (WebResponse response = e.Response)
                            {
                                HttpWebResponse httpResponse = (HttpWebResponse)response;
                                Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                                using (Stream data = response.GetResponseStream())
                                {
                                    string text = new StreamReader(data).ReadToEnd();
                                    Console.WriteLine(text);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //throw ex;
                    }
                }
            }
        }

        public void SetLoginSessionInformation(UserMst objUser, bool IsAdminUser = false, string AdminUser = "", FranchiseeMst objFranchisess = null, bool IsOfficeUser = false, string OfficeUser = "")
        {
            clsLogin clsLogin = new clsLogin();
            try
            {
                if (!IsAdminUser)
                {
                    if (objUser != null)
                    {
                        if (objUser.UserType == Convert.ToInt32(UserType.Member))
                        {
                            clsLogin.FirstName = objUser.FirstName;
                            clsLogin.FullName = objUser.FullName;
                            clsLogin.LastName = objUser.LastName;
                            clsLogin.RefferalCode = objUser.RefferalCode;
                            clsLogin.UserId = objUser.UserId;
                            clsLogin.MobileNo = objUser.MobileNo.HasValue ? objUser.MobileNo.Value.ToString() : "";
                            clsLogin.EmailId = objUser.EmailId;
                            clsLogin.UserName = objUser.UserName;
                            clsLogin.UserType = UserType.Member;
                            clsLogin.IsProfileCompleted = (!string.IsNullOrEmpty(objUser.Address) && (!objUser.FirstName.IsNullOrEmpty() || !objUser.LastName.IsNullOrEmpty()  || !objUser.FullName.IsNullOrEmpty())) ? true : false;
                            clsLogin.IsRegistrationActivated = objUser.IsRegistrationActivated.HasValue && objUser.IsRegistrationActivated.Value ? true : false;

                            if (db.UserPurchaseHdrs.Any(i => i.UserId == clsLogin.UserId))
                            {
                                clsLogin.IsAnyOrderGenerated = true;
                            }
                            if (!clsLogin.IsRegistrationActivated)
                            {
                                var CurrentDate = db.PROC_GET_SERVER_DATE().FirstOrDefault().Value;
                                var Days30AfterRegistration = objUser.RegistrationDate.Value.AddDays(30);
                                var Diff = Convert.ToInt64((Days30AfterRegistration - CurrentDate).TotalDays);

                                clsLogin.BlockDayRemaining = Diff;
                            }
                        }
                    }
                    else if (objFranchisess != null)
                    {
                        clsLogin.FirstName = objFranchisess.FirstName;
                        clsLogin.FullName = objFranchisess.FullName;
                        clsLogin.LastName = objFranchisess.LastName;
                        clsLogin.UserId = objFranchisess.FranchiseeId;
                        clsLogin.MobileNo = objFranchisess.MobileNo.HasValue ? objFranchisess.MobileNo.Value.ToString() : "";
                        clsLogin.EmailId = objFranchisess.EmailId;
                        clsLogin.UserName = objFranchisess.UserName;
                        clsLogin.UserType = UserType.Franchisee;
                        clsLogin.FranchiseeId = objFranchisess.FranchiseeId;
                        clsLogin.IsProfileCompleted = !string.IsNullOrEmpty(objFranchisess.Address) ? true : false;
                    }
                }
                else
                {
                    if (!IsOfficeUser)
                    {
                        clsLogin.UserId = objUser.UserId;
                        clsLogin.UserName = AdminUser;
                        clsLogin.IsAdmin = true;
                        clsLogin.UserType = UserType.Admin;
                    }
                    else if (IsOfficeUser)
                    {
                        clsLogin.UserId = objUser.UserId;
                        clsLogin.UserName = OfficeUser;
                        clsLogin.IsAdmin = false;
                        clsLogin.UserType = UserType.Office;
                    }
                }
                System.Web.HttpContext.Current.Session[clsImplementationMessage.LoginInfo] = clsLogin;
            }
            catch (Exception ex)
            {
                //ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public double CalculateWalletCreditAmountRegistration(ProductMst item, double Qty, ref double CreditAmount, WalletCreditType walletType)
        {
            //double CreditAmount = 0;
            try
            {
                List<string> listConfigKeys = new List<string>();
                listConfigKeys.Add(ConfigKeys.MinRegistrationPoints);
                listConfigKeys.Add(ConfigKeys.DirectPerc);

                var listConfigDtl = db.ConfigMsts.Where(i => listConfigKeys.Contains(i.Key.ToUpper())).ToList();
                double MinRegistrationPoints = 0;
                if (listConfigDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints))
                {
                    MinRegistrationPoints = Convert.ToDouble(listConfigDtl.Where(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints).FirstOrDefault().Value);
                }

                if (walletType == WalletCreditType.Direct)
                {
                    double DirectAmt = 0;
                    DirectAmt = (Int64)(Convert.ToDouble((item.DirectPurchasePerc.Value * item.DirectPurchaseAmt) / 100));
                    CreditAmount += (double)(decimal.Round((decimal)(DirectAmt * Qty), 0, MidpointRounding.AwayFromZero));
                }
                else if (walletType == WalletCreditType.Repurchase)
                {
                    double RepurchaseAmt = 0;
                    RepurchaseAmt = item.RepurchaseMatchingAmt.Value;// (Int64)(Convert.ToDouble((item.RepurchaseMatchingPerc.Value * item.RepurchaseMatchingAmt) / 100));
                    CreditAmount += (double)(decimal.Round((decimal)(RepurchaseAmt * Qty), 0, MidpointRounding.AwayFromZero));
                }
            }
            catch (Exception ex)
            {
            }
            return CreditAmount;
        }

        public bool CheckUserNameExist(string UserName, int Id = 0)
        {
            return db.UserMsts.Any(i => i.UserId != Id && i.UserName == UserName);
        }

        public bool CheckEmailIdExist(string EmailId, int Id = 0)
        {
            return db.UserMsts.Any(i => i.UserId != Id && i.EmailId == EmailId);
        }

        public List<ConfigMst> GetConfigValues(List<string> listConfigKeys)
        {
            var listConfigs = db.ConfigMsts.Where(i => listConfigKeys.Contains(i.Key.ToUpper())).ToList();
            return listConfigs;
        }

        public string GetConfigValueFromConfigList(List<ConfigMst> listConfigs, string Key)
        {
            var Value = "";
            if (listConfigs.Any(i => i.Key.ToUpper() == Key.ToUpper()))
            {
                Value = listConfigs.Where(i => i.Key.ToUpper() == Key.ToUpper()).FirstOrDefault().Value;
            }
            return Value;
        }

        public decimal GetUserMilesAmount(Int64 UserId)
        {
            var listMilesCreditAmt = db.MilesCreditDtls.Where(i => i.UserId == UserId).ToList();
            var MilesCreditAmt = listMilesCreditAmt.Count > 0 ? listMilesCreditAmt.Sum(i => i.CreditAmount) : 0;
            var listMilesDebitAmt = db.MilesDebitDtls.Where(i => i.UserId == UserId).ToList();
            var MilesDebitAmt = listMilesDebitAmt.Count > 0 ? listMilesDebitAmt.Sum(i => i.DebitAmount) : 0;
            var FinalAmount = MilesCreditAmt - (decimal)MilesDebitAmt;
            return FinalAmount;
        }

        public decimal GetUserMilesCreditAmount(Int64 UserId)
        {
            var listMilesCreditAmt = db.MilesCreditDtls.Where(i => i.UserId == UserId).ToList();
            var MilesCreditAmt = listMilesCreditAmt.Count > 0 ? listMilesCreditAmt.Sum(i => i.CreditAmount) : 0;
            return MilesCreditAmt;
        }

        public decimal GetUserMilesDebitAmount(Int64 UserId)
        {
            var listMilesDebitAmt = db.MilesDebitDtls.Where(i => i.UserId == UserId).ToList();
            var MilesDebitAmt = listMilesDebitAmt.Count > 0 ? listMilesDebitAmt.Sum(i => i.DebitAmount) : 0;
            return (decimal)MilesDebitAmt;
        }

        public double GetMatchingBVValues(int UserId, DateTime? FromDate, DateTime? ToDate)
        {
            using (TRSEntities db1 = new TRSEntities(10000))
            {
                List<PROC_GET_USER_LEFT_SIDE_POINTS_Result> listLeftIncome = new List<PROC_GET_USER_LEFT_SIDE_POINTS_Result>();
                List<PROC_GET_USER_RIGHT_SIDE_POINTS_Result> listRightIncome = new List<PROC_GET_USER_RIGHT_SIDE_POINTS_Result>();
                IEnumerable<PROC_GET_USER_LEFT_SIDE_POINTS_Result> LeftIncomeQuery = new List<PROC_GET_USER_LEFT_SIDE_POINTS_Result>();
                IEnumerable<PROC_GET_USER_RIGHT_SIDE_POINTS_Result> RightIncomeQuery = new List<PROC_GET_USER_RIGHT_SIDE_POINTS_Result>();
                if (FromDate.HasValue)
                {
                    LeftIncomeQuery = db1.PROC_GET_USER_LEFT_SIDE_POINTS(UserId, null, null).Where(i => i.CreditDate >= FromDate.Value);
                    RightIncomeQuery = db1.PROC_GET_USER_RIGHT_SIDE_POINTS(UserId, null, null).Where(i => i.CreditDate >= FromDate.Value);
                }
                else
                {
                    LeftIncomeQuery = db1.PROC_GET_USER_LEFT_SIDE_POINTS(UserId, null, null);
                    RightIncomeQuery = db1.PROC_GET_USER_RIGHT_SIDE_POINTS(UserId, null, null);
                }
                if (ToDate.HasValue)
                {
                    LeftIncomeQuery = LeftIncomeQuery.Where(i => i.CreditDate <= ToDate.Value);
                    RightIncomeQuery = RightIncomeQuery.Where(i => i.CreditDate <= ToDate.Value);
                }
                else
                {
                    LeftIncomeQuery = db1.PROC_GET_USER_LEFT_SIDE_POINTS(UserId, null, null);
                    RightIncomeQuery = db1.PROC_GET_USER_RIGHT_SIDE_POINTS(UserId, null, null);
                }

                //listLeftIncome = LeftIncomeQuery.ToList();
                //listRightIncome = RightIncomeQuery.ToList();

                var TotalLeftBV = LeftIncomeQuery.Sum(i => i.LeftBV);
                var TotalRightBV = RightIncomeQuery.Sum(i => i.RightBV);

                double MatchingBV = 0;
                MatchingBV = (TotalLeftBV.HasValue ? TotalLeftBV.Value : 0) >= (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                        ? (TotalRightBV.HasValue ? TotalRightBV.Value : 0)
                        : (TotalLeftBV.HasValue ? TotalLeftBV.Value : 0);
                return MatchingBV;
            }
        }

        public double GetDirectBVValues(int UserId, DateTime? FromDate = null, DateTime? ToDate = null)
        {
            double DirectBV = 0;
            var listTotalBV = db.FN_GET_USER_TOTAL_DIRECT_BV(UserId, FromDate, ToDate).Sum(i => i.TotalBV);
            DirectBV = (listTotalBV.HasValue ? listTotalBV.Value : 0);
            return DirectBV;
        }

        public double? GetRightBV(int UserId)
        {
            double? BV = 0;
            try
            {
                var objRightPoints = db.PROC_GET_USER_RIGHT_SIDE_POINTS(UserId, null, null).ToList();
                BV = objRightPoints.Sum(i => i.RightBV);
            }
            catch (Exception)
            {
            }
            return BV ?? 0;
        }

        public double? GetLeftBV(int UserId)
        {
            double? BV = 0;
            try
            {
                var objLeftPoints = db.PROC_GET_USER_LEFT_SIDE_POINTS(UserId, null, null).ToList();
                BV = objLeftPoints.Sum(i => i.LeftBV);
            }
            catch (Exception)
            {
            }
            return BV ?? 0;
        }

        public List<BonanzaQualifiersEnt> GetBonanzaQualifierUsers(int Month, int Year)
        {
            var listBonanzaUsers = db.PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH(Month, Year).ToList();
            DateTime Date = new DateTime(Year, Month, 1);
            var MonthDuration = db.fn_GetFirstLastDateOfMonthByDate(Date).FirstOrDefault();
            var CompanyVolumes = db.PROC_GET_ADMIN_VOLUME_DETAILS(MonthDuration.StartDate, MonthDuration.EndDate).FirstOrDefault();

            decimal BVMultiplier = 0;
            double FinalMonthBV = 0;
            if (listBonanzaUsers.Count > 0)
            {
                ////var CurrentMonthLeftBV = listBonanzaUsers.Sum(i => i.CurrentMonthLeftBV);
                ////var CurrentMonthRightBV = listBonanzaUsers.Sum(i => i.CurrentMonthRightBV);

                ////var FinalMonthBV = (CurrentMonthLeftBV ?? 0) >= (CurrentMonthRightBV ?? 0) ? (CurrentMonthRightBV ?? 0) : (CurrentMonthLeftBV ?? 0);
                FinalMonthBV = listBonanzaUsers.Sum(i => i.CurrentMonthBV);
                if (FinalMonthBV > 0)
                {
                    BVMultiplier = Convert.ToDecimal((CompanyVolumes.ActualBonanzaVolume ?? 0) / (Int64)FinalMonthBV);
                }

                foreach (var item in listBonanzaUsers)
                {
                    item.Amount = (Int64)(BVMultiplier * Convert.ToDecimal(item.CurrentMonthBV) * 10);
                }
            }
            ////List<BonanzaQualifiersEnt> listBonanzaQualifiersEnt = new List<BonanzaQualifiersEnt>();
            var listBonanzaQualifiersEnt = listBonanzaUsers.Select(i => new BonanzaQualifiersEnt
            {
                Amount = i.Amount,
                CurrentMonthBV = (Int64)i.CurrentMonthBV,
                CurrentMonthLeftBV = i.CurrentMonthLeftBV,
                CurrentMonthRightBV = i.CurrentMonthRightBV,
                FullName = i.FullName,
                IsPaid = false,
                Month = Month,
                PaidDate = string.Empty,
                Previous2rdMonthBV = (Int64)(i.Previous2rdMonthBV),
                Previous3rdMonthBV = (Int64)(i.Previous3rdMonthBV),
                PreviousMonthBV = (Int64)(i.PreviousMonthBV),
                RefferalCode = i.RefferalCode,
                UserId = i.UserId,
                UTRRefNo = string.Empty,
                Year = Year,
                PaidAmount = (Int64)(i.Amount),
                AllUsersTotalMonthBV = (Int64)FinalMonthBV,
                ActualBonanzaVolume = (CompanyVolumes.ActualCompanyTurnOverVolume ?? 0),
                BVMultiplier = BVMultiplier * 10
            }).ToList();
            return listBonanzaQualifiersEnt;
        }

        public List<BonanzaQualifiersEnt> GetCompanyTurnOverQualifierUsers(int Month, int Year)
        {
            var listBonanzaUsers = db.PROC_GET_COMPANY_TURN_OVER_QUALIFIER_USERS_BY_MONTH(Month, Year).ToList();
            DateTime Date = new DateTime(Year, Month, 1);
            var MonthDuration = db.fn_GetFirstLastDateOfMonthByDate(Date).FirstOrDefault();
            var CompanyVolumes = db.PROC_GET_ADMIN_VOLUME_DETAILS(MonthDuration.StartDate, MonthDuration.EndDate).FirstOrDefault();

            decimal BVMultiplier = 0;
            if (listBonanzaUsers.Count > 0)
            {
                ////var CurrentMonthLeftBV = listBonanzaUsers.Sum(i => i.CurrentMonthLeftBV);
                ////var CurrentMonthRightBV = listBonanzaUsers.Sum(i => i.CurrentMonthRightBV);

                ////var FinalMonthBV = (CurrentMonthLeftBV ?? 0) >= (CurrentMonthRightBV ?? 0) ? (CurrentMonthRightBV ?? 0) : (CurrentMonthLeftBV ?? 0);
                var FinalMonthBV = listBonanzaUsers.Sum(i => i.CurrentMonthBV);
                if (FinalMonthBV > 0)
                {
                    BVMultiplier = Convert.ToDecimal((CompanyVolumes.ActualCompanyTurnOverVolume ?? 0) / FinalMonthBV);
                }

                foreach (var item in listBonanzaUsers)
                {
                    item.Amount = (Int64)(BVMultiplier * Convert.ToDecimal(item.CurrentMonthBV) * 10);
                }
            }
            ////List<BonanzaQualifiersEnt> listBonanzaQualifiersEnt = new List<BonanzaQualifiersEnt>();
            var listBonanzaQualifiersEnt = listBonanzaUsers.Select(i => new BonanzaQualifiersEnt
            {
                Amount = i.Amount,
                CurrentMonthBV = i.CurrentMonthBV,
                CurrentMonthLeftBV = i.CurrentMonthLeftBV,
                CurrentMonthRightBV = i.CurrentMonthRightBV,
                FullName = i.FullName,
                IsPaid = false,
                Month = Month,
                PaidDate = string.Empty,
                Previous2rdMonthBV = (Int64)(i.Previous2rdMonthBV),
                Previous3rdMonthBV = (Int64)(i.Previous3rdMonthBV),
                PreviousMonthBV = (Int64)(i.PreviousMonthBV),
                RefferalCode = i.RefferalCode,
                UserId = i.UserId,
                UTRRefNo = string.Empty,
                Year = Year,
                PaidAmount = (Int64)(i.Amount)
            }).ToList();
            return listBonanzaQualifiersEnt;
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            
            return dataTable;
        }
    }

    public static class HtmlRequestHelper
    {
        public static string Id(this HtmlHelper htmlHelper)
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("id"))
                return (string)routeValues["id"];
            else if (HttpContext.Current.Request.QueryString.AllKeys.Contains("id"))
                return HttpContext.Current.Request.QueryString["id"];

            return string.Empty;
        }

        public static string Controller(this HtmlHelper htmlHelper)
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("controller"))
                return (string)routeValues["controller"];

            return string.Empty;
        }

        public static string Action(this HtmlHelper htmlHelper)
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("action"))
                return (string)routeValues["action"];

            return string.Empty;
        }
    }

    public class clsLogin
    {
        public clsLogin()
        {
        }

        public int UserId { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RefferalCode { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string UserName { get; set; }
        public string FranchiseeName { get; set; }
        public int FranchiseeId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsProfileCompleted { get; set; }
        public bool IsRegistrationActivated { get; set; }
        public bool IsAnyOrderGenerated { get; set; }
        public double? BlockDayRemaining { get; set; }
        public UserType UserType { get; set; }
        public bool IsOTPAuthenticate { get; set; }
        public bool RedirectFrmAdmin { get; set; }
    }

    public class DateFormats
    {
        public DateFormats()
        {
        }

        public const string DDMMYYYY = "dd/MM/yyyy";
        public const string MMDDYYYY = "MM/dd/yyyy";
    }

    public class PaymentKeyDetails
    {
        public string PaymentKey { get; set; }
        public string PaymentSecret { get; set; }
    }

    public class ParentMemberDtl
    {
        public int UserId { get; set; }
        public int ChildUserId { get; set; }
        public string FullName { get; set; }
        public int UserLevel { get; set; }
        public bool IsRegistered { get; set; }
    }

    public class ConfigKeys
    {
        public const string WebsiteURL = "WEBSITEURL";
        public const string EmailFrom = "EMAILFROM";
        public const string EmailPwd = "EMAILPWD";
        public const string EmailHostName = "EMAILHOSTNAME";
        public const string EmailPort = "EMAILPORT";
        public const string EmailEnableSSLOnMail = "EMAILENABLESSLONMAIL";
        public const string UploadPhotoInMB = "UPLOADPHOTOINMB";
        public const string Version = "VERSION";
        public const string BOLTURL = "BOLTURL";
        public const string BOLTURLTest = "BOLTURLTEST";
        public const string BOLTURLLive = "BOLTURLLIVE";
        public const string AdminUser = "ADMINUSER";
        public const string AdminPwd = "ADMINPWD";
        public const string PaymentKey = "PAYMENTKEY";
        public const string PaymentSecret = "PAYMENTSECRET";
        public const string CashbackRefferalCnt = "CASHBACKREFFERALCNT";
        public const string RegistrationAmt = "REGISTRATIONAMT";
        public const string DirectPerc = "DIRECTPERC";
        public const string DirectMatchingPerc = "DIRECTMATCHINGPERC";
        public const string AyurvedicMatchingPer = "AYURVEDICMATCHINGPER";
        public const string GymMatchingPer = "GYMMATCHINGPER";
        public const string GoldSilverPer = "GOLDSILVERPER";
        public const string CompanySharePerc = "GOLDSILVERPER";
        public const string NoOfMemberForCompanyShare = "NOOFMEMBERFORCOMPANYSHARE";
        public const string PaymentKeyTest = "PAYMENTKEYTEST";
        public const string PaymentSecretTest = "PAYMENTSECRETTEST";
        public const string PaymentKeyLive = "PAYMENTKEYLIVE";
        public const string PaymentSecretLive = "PAYMENTSECRETLIVE";
        public const string SMSURL = "SMSURL";
        public const string SMSUserName = "SMSUSERNAME";
        public const string SMSPassword = "SMSPASSWORD";
        public const string SMSSenderId = "SMSSENDERID";
        public const string BonanzaOfferCnt = "BONANZAOFFERCNT";
        public const string TDSPercWithoutPAN = "TDSPERCWITHOUTPAN";
        public const string TDSPercWithPAN = "TDSPERCWITHPAN";
        public const string AdminChargePerc = "ADMINCHARGEPERC";
        public const string MinRegistrationPoints = "MINREGISTRATIONPOINTS";
        public const string RequiredMemberForGold = "REQUIREDMEMBERFORGOLD";
        public const string RequiredMemberForSilver = "REQUIREDMEMBERFORSILVER";
        public const string GoldMemberPerc = "GOLDMEMBERPERC";
        public const string SilverMemberPerc = "SILVERMEMBERPERC";
        public const string AdminConfirmationRequired = "ADMINCONFIRMATIONREQUIRED";
        public const string MultiOrderSystem = "MULTIORDERSYSTEM";
        public const string AllowCODAsFirstOrder = "ALLOWCODASFIRSTORDER";
        public const string LastCalculateDate = "LASTCALCULATEDATE";
        public const string AdvertisementHeader = "ADVERTISEMENTHEADER";
        public const string AdvertisementValue = "ADVERTISEMENTVALUE";
        public const string AdvertisementVideoURL = "ADVERTISEMENTVIDEOURL";
        public const string VoltIncomeBV = "VOLTINCOMEBV";
        public const string GoldIncomeBV = "GOLDINCOMEBV";
        public const string SuperGoldIncomeBV = "SUPERGOLDINCOMEBV";
        public const string SilverIncomeBV = "SILVERINCOMEBV";
        public const string VoltIncomeAmount = "VOLTINCOMEAMOUNT";
        public const string PrimeIncomeBV = "PRIMEINCOMEBV";
        public const string DiamondIncomeBV = "DIAMONDINCOMEBV";
        public const string AllowToPurchaseBelowMinRegistrationPoints = "ALLOWTOPURCHASEBELOWMINREGISTRATIONPOINTS";
        public const string NoOfSameMobileNoAllow = "NOOFSAMEMOBILENOALLOW";
        public const string CODAmount = "CODAMOUNT";
        public const string ShopNowAdvertise = "SHOPNOWADVERTISE";
        public const string RefernceMilesAmount = "REFERNCEMILESAMOUNT";
        public const string MaxMilesUsePercentage = "MAXMILESUSEPERCENTAGE";
        public const string OfficeUserUserName = "OFFICEUSERUSERNAME";
        public const string OfficeUserPassword = "OFFICEUSERPASSWORD";
        public const string AllowToUseMilesAmtInDirectPurchase = "ALLOWTOUSEMILESAMTINDIRECTPURCHASE";
        public const string DefaultLevelLoadInGeneology = "DEFAULTLEVELLOADINGENEOLOGY";
        public const string ChildLevelLoadInGeneology = "CHILDLEVELLOADINGENEOLOGY";
        public const string CompanyTurnOverUserBV = "COMPANYTURNOVERUSERBV";
        public const string BonanzaUserBVPreviousMonths = "BONANZAUSERBVPREVIOUSMONTHS";
        public const string BonanzaUserBV = "BONANZAUSERBV";
        public const string OTPValidMinutes = "OTP_Valid_Minutes";
        public const string OfficeUserUserId = "OfficeUserUserId";
        public const string AdminUserId = "AdminUserId";

        public const string OfficeUserUserName1 = "OFFICEUSERUSERNAME1";
        public const string OfficeUserPassword1 = "OFFICEUSERPASSWORD1";
        public const string OfficeUserUserId1 = "OFFICEUSERUSERID1";

        public const string OfficeUserUserName2 = "OFFICEUSERUSERNAME2";
        public const string OfficeUserPassword2 = "OFFICEUSERPASSWORD2";
        public const string OfficeUserUserId2 = "OFFICEUSERUSERID2";

        public const string OfficeUserUserName3 = "OFFICEUSERUSERNAME3";
        public const string OfficeUserPassword3 = "OFFICEUSERPASSWORD3";
        public const string OfficeUserUserId3 = "OFFICEUSERUSERID3";

        public const string ORDERGENBYADMINSTOPSMS = "ORDERGENBYADMINSTOPSMS";
        public const string POINTSCREDITDTLGENERATEWHILEPLACEORDER = "POINTSCREDITDTLGENERATEWHILEPLACEORDER";
        public const string ENABLESMSALERTAPI = "ENABLESMSALERTAPI";
        public const string SMSALERTAPIURL = "SMSALERTAPIURL";
        public const string SMSALERTAPIKEY = "SMSALERTAPIKEY";
        public const string INDIADLTPRINCIPALENTITYID = "INDIADLTPRINCIPALENTITYID";
        public const string TDSDEXUCTIONLIMIT = "TDSDEXUCTIONLIMIT";

        public const string MinimumOrderAmountForDeliveryChargeApply = "MINIMUMORDERAMOUNTFORDELIVERYCHARGEAPPLY";
        public const string OrderDeliveryCharge = "ORDERDELIVERYCHARGE";
        public const string TDSEffectiveDate = "TDSEFFECTIVEDATE";
    }

    public class BonanzaQualifiersEnt : PROC_GET_BONANZA_QUALIFIER_USERS_BY_MONTH_Result
    {
        public string PaidDate { get; set; } = "";
        public bool? IsPaid { get; set; }
        public string UTRRefNo { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public Int64 BonanzaRewardDtlId { get; set; } = 0;
        public Int64 CompanyTurnOverRewardDtlId { get; set; } = 0;
        public double? PaidAmount { get; set; } = 0;

        public double? AllUsersTotalMonthBV { get; set; } = 0;
        public decimal BVMultiplier { get; set; }
        public double? ActualBonanzaVolume { get; set; }
    }

    public class WalletDebitDtlExtra
    {
        public System.DateTime DebitDate { get; set; }
        public Nullable<double> DirectIncome { get; set; }
        public Nullable<double> MatchingIncome { get; set; }
        public Nullable<double> VoltIncome { get; set; }
        public Nullable<double> UplineSupportIncomePay { get; set; }
        public Nullable<double> OtherIncome { get; set; }
        public Nullable<double> LeaderIncomePay { get; set; }
        public Nullable<double> SilverIncomePay { get; set; }
        public Nullable<double> UplineMonthlyPay { get; set; }
        public Nullable<double> RetailRewardMonthlyPay { get; set; }
        public Nullable<double> TDSPerc { get; set; }
        public Nullable<double> TDSAmount { get; set; }
        public Nullable<double> AdminPerc { get; set; }
        public Nullable<double> AdminCharge { get; set; }
        public Nullable<double> NetPayable { get; set; }
        public double DebitAmount { get; set; }
        public string UTRRefNo { get; set; }
    }
}