//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    
    public partial class PROC_GET_ALL_BUSINESS_TOOL_LIST_FOR_VIEW_EDIT_Result
    {
        public int BusinessToolsId { get; set; }
        public int BusinessToolsCategoryId { get; set; }
        public string BusinessToolsName { get; set; }
        public string VideoURL { get; set; }
        public string ImageName { get; set; }
        public string PdfName { get; set; }
        public bool IsActive { get; set; }
        public decimal CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public decimal ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string CategoryName { get; set; }
        public string Language { get; set; }
    }
}
