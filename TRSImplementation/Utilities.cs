﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRSImplementation
{
    public static class Utilities
    {
        public static string GetMaxenerwellnessUrl()
        {
            return System.Configuration.ConfigurationManager.AppSettings["MaxenerwellnessUrl"] != null ? System.Configuration.ConfigurationManager.AppSettings["MaxenerwellnessUrl"] : "";
        }

        public static bool IsNullOrEmpty(this string value)
        {
            if (value != null)
                value = value.Trim();
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// ToDataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(IList<T> data,bool isIncludeColumnAsRow)// T is any generic type
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                //table.Columns.Add(prop.Name, prop.PropertyType);
                table.Columns.Add(prop.Name);
            }

            if (isIncludeColumnAsRow)
            {
                DataRow firstRow = table.NewRow();
                for (var i = 0; i < table.Columns.Count; i++)
                {
                    firstRow.SetField(i, table.Columns[i].ColumnName);
                }
                table.Rows.InsertAt(firstRow, 0);
            }

            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        /// <summary>
        /// GetExcelPackageForExport
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Listdata"></param>
        /// <param name="ExcludeColumns"></param>
        /// <param name="WorksheetName"></param>
        /// <param name="isIncludeColumnAsRow"></param>
        /// <returns></returns>
        public static ExcelPackage GetExcelPackageForExport<T>(IList<T> Listdata,List<string> ExcludeColumns,string WorksheetName, bool isIncludeColumnAsRow)
        {
            ExcelPackage excel = new ExcelPackage();

            var Orders = Utilities.ToDataTable(Listdata, isIncludeColumnAsRow);

            if (ExcludeColumns.Any())
            {
                foreach (var column in ExcludeColumns)
                    Orders.Columns.Remove(column);
            }
            
            var workSheet = excel.Workbook.Worksheets.Add(WorksheetName);
            var totalCols = Orders.Columns.Count;
            var totalRows = Orders.Rows.Count;

            for (var col = 1; col <= totalCols; col++)
            {
                workSheet.Cells[1, col].Value = Orders.Columns[col - 1].ColumnName;
            }
            for (var row = 1; row <= totalRows; row++)
            {
                for (var col = 0; col < totalCols; col++)
                {
                    workSheet.Cells[row + 1, col + 1].Value = Orders.Rows[row - 1][col];
                }
            }
            return excel;
        }

        /// <summary>
        /// CreateExcelWorksheet
        /// </summary>
        /// <param name="fInfor"></param>
        /// <param name="dt"></param>
        public static void CreateExcelWorksheet(FileInfo fInfor,DataTable dt,string excelTabName)
        {
            using (var package = new ExcelPackage(fInfor))
            {
                package.Workbook.Worksheets.Add(excelTabName);

                int ExcelTbIndex = 0;
                foreach (ExcelWorksheet worksheet in package.Workbook.Worksheets)
                {
                    int startRow = 1;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            int columnIndx = 0;
                            int totalcolumn = dt.Columns.Count;
                            while (columnIndx < totalcolumn)
                            {
                                worksheet.Cells[startRow, columnIndx + 1].Value = dr[columnIndx];
                                columnIndx++;
                            }
                            startRow++;
                        }
                    }
                    ExcelTbIndex++;
                }
                package.SaveAs(fInfor);
            }
        }

        public static void WriteLog(string strLog)
        {
            string logFilePath = System.Configuration.ConfigurationManager.AppSettings["WriteLogPath"] != null ? System.Configuration.ConfigurationManager.AppSettings["WriteLogPath"] : "";
            if (!string.IsNullOrEmpty(logFilePath))
            {
                StreamWriter log;
                FileStream fileStream = null;
                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo;
                string msgFormat = string.Format("{0}-{1}", DateTime.Now.ToLongTimeString(), strLog);

                logFilePath = Path.Combine(logFilePath, "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt");
                logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();
                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                log = new StreamWriter(fileStream);
                log.WriteLine(msgFormat);
                log.Close();
            }
        }

    }
}
