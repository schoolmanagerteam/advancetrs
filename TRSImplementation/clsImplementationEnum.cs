﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRSImplementation
{
    public class clsImplementationEnum
    {
        public clsImplementationEnum()
        {

        }

        public enum WalletCreditType
        {
            Direct = 1,
            Repurchase = 2,
            FranchiseeReferral = 3
        }
        public enum UserType
        {
            Admin = 0,
            Member = 1,
            Franchisee = 2,
            Office = 3
        }

        public enum OrderStatus
        {
            OrderGenerate = 1,
            AssignToFranchisee = 2,
            ProcessByFranchisee = 3,
            OrderCompleted = 4,
            OrderConfirm = 5
        }
        public enum NotificationType
        {
            Information = 1,
            Action = 2,
        }
        public enum QuerySupportStatus
        {
            Pending = 1,
            Resolve = 2,
        }

        public enum UserBVStatus
        {
            Prime = 1,
            Silver = 2,
            Gold = 3,
            Diamond = 4,
            SuperGold = 5,
        }

        public enum ServiceName
        {
            PointsCreditProcess = 1,
            UpdateAllUserStatusProcess = 2,
            UpdateOrderUserStatusProcess = 3,
            OrderFailedverifyProcess = 4,
            GenerateIncomeProcess = 5,
            FastageMilesCreditProcess = 6,
            UserMilesLapsedProcess = 7
        }

        public enum JoinningProductType
        {
            ForAllActiveUser = 1,
            ForActiveRegistrationUser = 2,
            ForInActiveRegistrationUser = 3
        }

        public enum MilesCreditType
        {
            ForRegisterUser = 0,
            ForFileUpload = 1,
            ForVoltIncome = 2
        }
    }
}
