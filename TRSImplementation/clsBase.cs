﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace TRSImplementation
{
    public partial class TRSEntities : DbContext
    {
        public TRSEntities(int timeout)
      : base("name=TRSEntities")
        {
            this.Database.CommandTimeout = timeout;
        }
    }

    [NoCache]
    public class clsBase : Controller
    {
        public readonly TRSEntities db;

        public static bool IsSecureConnection = System.Configuration.ConfigurationManager.AppSettings["IsSecureConnection"] != null ?
                                                Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsSecureConnection"]) : false;

        //public static string WebsiteURL = System.Configuration.ConfigurationManager.AppSettings["WebsiteURL"];
        public static string MerchantKey = System.Configuration.ConfigurationManager.AppSettings["MerchantKey"];

        public static string MerchantSalt = System.Configuration.ConfigurationManager.AppSettings["MerchantSalt"];
        public static string BOLT_KIT_ASP = System.Configuration.ConfigurationManager.AppSettings["BOLT_KIT_ASP.NET"];
        //public static string BOLT_URL = System.Configuration.ConfigurationManager.AppSettings["BOLTURL"];

        //public static string EmailPwd = System.Configuration.ConfigurationManager.AppSettings["EmailPwd"];
        //public static string EmailFrom = System.Configuration.ConfigurationManager.AppSettings["EmailFrom"];
        //public static string EmailHostName = System.Configuration.ConfigurationManager.AppSettings["EmailHostName"];
        public static string AdminUser = System.Configuration.ConfigurationManager.AppSettings["AdminUser"];

        public static string AdminPassword = System.Configuration.ConfigurationManager.AppSettings["AdminPwd"];

        //public static string EmailPort = System.Configuration.ConfigurationManager.AppSettings["EmailPort"];
        //public static bool EmailEnableSSLOnMail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EmailEnableSSLOnMail"]);
        public const int TDSPerc = 5;

        public const int TDSPercWithoutPAN = 20;
        public const int AdminChargePerc = 5;

        #region Base Contsructor

        public clsBase()
        {
            if (db == null)
            {
                db = new TRSEntities(3600);
            }
        }

        #endregion Base Contsructor

        private Manager _objManager = null;

        public Manager objManager
        {
            get { if (_objManager == null) { return _objManager = new Manager(); } else { return _objManager; } }
            set { _objManager = value; }
        }

        private clsLogin _objclsLogin = null;

        public clsLogin objclsLogin
        {
            get
            {
                if (_objclsLogin == null)
                {
                    if (System.Web.HttpContext.Current.Session[clsImplementationMessage.LoginInfo] == null)
                    {
                        return _objclsLogin = new clsLogin();
                    }
                    else
                    {
                        return _objclsLogin = (clsLogin)System.Web.HttpContext.Current.Session[clsImplementationMessage.LoginInfo];
                    }
                }
                else { return _objclsLogin; }
            }
            set { _objclsLogin = value; }
        }

        #region Session Expire Filter

        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
        public class SessionExpireFilterAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                string actionName = filterContext.ActionDescriptor.ActionName;
                string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

                if (System.Web.HttpContext.Current.Session[clsImplementationMessage.LoginInfo] == null)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        //filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/SessionTimeOut"));
                        filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index"));
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index"));
                    }
                    return;
                }
                else
                {
                    var _objclsLogin = (clsLogin)System.Web.HttpContext.Current.Session[clsImplementationMessage.LoginInfo];
                    if (_objclsLogin.UserId == 0 && _objclsLogin.IsAdmin == false)
                    {
                        filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index"));
                    }
                    else if ((_objclsLogin.UserType == clsImplementationEnum.UserType.Admin || _objclsLogin.UserType == clsImplementationEnum.UserType.Office)
                        && actionName != "OTPAuthenticate" && actionName != "CheckOTPAuthenticate"
                        && !_objclsLogin.IsOTPAuthenticate)
                    {
                        filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index"));
                    }
                    else if ((_objclsLogin.IsAdmin == false && _objclsLogin.UserType == clsImplementationEnum.UserType.Office)
                        && (actionName == "Profite" || actionName == "JoiningProducts" || actionName == "RepurchaseProducts" ||
                        actionName == "GymProducts" || actionName == "Config" || 
                        (actionName == "Product" || (actionName == "Index" && controllerName == "Product")) || actionName == "Variant")) //|| actionName == "VoltIncome" || actionName == "TopRetailer"  || actionName == "BonanzaReward" || actionName == "Sales" 
                    {
                        filterContext.Result = new RedirectResult(String.Concat("~/Authentication/Authenticate/Index"));
                    }
                }
                base.OnActionExecuting(filterContext);
            }
        }

        #endregion Session Expire Filter

        #region Session Expire Filter

        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
        public class CookiesFilterAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                if (System.Web.HttpContext.Current.Request.Cookies["uid"] != null)
                {
                    var db = new TRSEntities(3600);
                    var UserName = "";
                    var Password = "";
                    int id = 0;
                    int typ = 0;
                    bool IsAdminUser = false;
                    bool IsOfficeUser = false;
                    Manager objManager = new Manager();
                    UserName = objManager.HigherDecrypt(System.Web.HttpContext.Current.Request.Cookies["uid"].Value);
                    if (System.Web.HttpContext.Current.Request.Cookies["isa"] != null)
                    {
                        IsAdminUser = Convert.ToBoolean(objManager.HigherDecrypt(System.Web.HttpContext.Current.Request.Cookies["isa"].Value));
                    }
                    if (System.Web.HttpContext.Current.Request.Cookies["iso"] != null)
                    {
                        IsOfficeUser = Convert.ToBoolean(objManager.HigherDecrypt(System.Web.HttpContext.Current.Request.Cookies["iso"].Value));
                    }
                    if (IsAdminUser)
                    {
                        List<string> listConfigKeys = new List<string>();
                        listConfigKeys.Add(ConfigKeys.AdminUser);

                        var listConfig = db.ConfigMsts.Where(i => listConfigKeys.Contains(i.Key.ToUpper())).ToList();
                        var AdminUser = "";
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.AdminUser))
                        {
                            AdminUser = listConfig.Where(i => i.Key.ToUpper() == ConfigKeys.AdminUser).FirstOrDefault().Value;
                        }
                        if (AdminUser == UserName)
                        {
                            objManager.SetLoginSessionInformation(null, true, UserName);

                            filterContext.Result = new RedirectResult(String.Concat("~/Admin/Dashboard/Index"));
                        }
                    }
                    else if (IsOfficeUser)
                    {
                        List<string> listConfigKeys = new List<string>();
                        listConfigKeys.Add(ConfigKeys.OfficeUserUserName);

                        var listConfig = db.ConfigMsts.Where(i => listConfigKeys.Contains(i.Key.ToUpper())).ToList();
                        var OfficeUserUserName = "";
                        if (listConfig.Any(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName))
                        {
                            OfficeUserUserName = listConfig.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.OfficeUserUserName).Value;
                        }
                        if (OfficeUserUserName == UserName)
                        {
                            objManager.SetLoginSessionInformation(null, true, UserName, null, true, OfficeUserUserName);

                            filterContext.Result = new RedirectResult(String.Concat("~/Admin/Orders/Index"));
                        }
                    }
                    else
                    {
                        Password = objManager.HigherDecrypt(System.Web.HttpContext.Current.Request.Cookies["pid"].Value);
                        id = Convert.ToInt32(objManager.HigherDecrypt(System.Web.HttpContext.Current.Request.Cookies["id"].Value));
                        if (System.Web.HttpContext.Current.Request.Cookies["typ"] != null)
                        {
                            typ = Convert.ToInt32(objManager.HigherDecrypt(System.Web.HttpContext.Current.Request.Cookies["typ"].Value));
                        }
                        else
                        {
                            typ = clsImplementationEnum.UserType.Member.GetHashCode();
                        }
                        if (typ == clsImplementationEnum.UserType.Member.GetHashCode())
                        {
                            var objUserMst = db.UserMsts.Where(i => i.UserId == id).FirstOrDefault();
                            if (objUserMst != null)
                            {
                                if ((objUserMst.UserName == UserName || objUserMst.EmailId == UserName) && objUserMst.Password == Password)
                                {
                                    objManager.SetLoginSessionInformation(objUserMst);
                                }
                                if (objUserMst.UserType.Value == clsImplementationEnum.UserType.Member.GetHashCode())
                                {
                                    filterContext.Result = new RedirectResult(String.Concat("~/Member/Dashboard/Index"));
                                }
                            }
                        }
                        else if (typ == clsImplementationEnum.UserType.Franchisee.GetHashCode())
                        {
                            var objFranchiseeMst = db.FranchiseeMsts.Where(i => i.FranchiseeId == id).FirstOrDefault();
                            if (objFranchiseeMst != null)
                            {
                                if ((objFranchiseeMst.UserName == UserName) && objFranchiseeMst.Password == Password)
                                {
                                    objManager.SetLoginSessionInformation(null, false, "", objFranchiseeMst);
                                    filterContext.Result = new RedirectResult(String.Concat("~/Franchisee/Dashboard/Index"));
                                }
                            }
                        }
                    }
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
        }

        #endregion Session Expire Filter

        #region No Catch Any Data

        public class NoCacheAttribute : ActionFilterAttribute
        {
            public override void OnResultExecuting(ResultExecutingContext filterContext)
            {
                if (filterContext == null) throw new ArgumentNullException("filterContext");

                var cache = GetCache(filterContext);

                cache.SetExpires(DateTime.UtcNow.AddDays(-1));
                cache.SetValidUntilExpires(false);
                cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
                cache.SetCacheability(HttpCacheability.NoCache);
                cache.SetNoStore();

                base.OnResultExecuting(filterContext);
            }

            protected virtual HttpCachePolicyBase GetCache(ResultExecutingContext filterContext)
            {
                return filterContext.HttpContext.Response.Cache;
            }
        }

        #endregion No Catch Any Data

        #region Json Method

        new public JsonResult Json(object data)
        {
            return Json(data, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Json(object data, Encoding contentEncoding)
        {
            return Json(data, "application/json", contentEncoding, JsonRequestBehavior.AllowGet);
        }

        new public JsonResult Json(object data, JsonRequestBehavior behavior)
        {
            return Json(data, "application/json", Encoding.UTF8, behavior);
        }

        new public JsonResult Json(object data, string contentType)
        {
            return Json(data, contentType, Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        new public JsonResult Json(object data, string contentType, Encoding contentEncoding)
        {
            return Json(data, contentType, contentEncoding, JsonRequestBehavior.AllowGet);
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        #endregion Json Method
    }
}