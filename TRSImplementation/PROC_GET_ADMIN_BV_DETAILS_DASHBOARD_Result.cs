//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    
    public partial class PROC_GET_ADMIN_BV_DETAILS_DASHBOARD_Result
    {
        public double WeekBV { get; set; }
        public double MonthBV { get; set; }
        public double WeekDirectBV { get; set; }
        public double WeekRepurchaseBV { get; set; }
    }
}
