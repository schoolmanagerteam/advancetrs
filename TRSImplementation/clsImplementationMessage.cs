﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRSImplementation
{
   public class clsImplementationMessage
    {
        public const string LoginInfo = "LoginInfo";
        public const string Version = "Version";
        public const string BOLT_URL = "BOLT_URL";
        public const string WebsiteURL = "WebsiteURL";
        public const string SecretKey = "A18H89I09";
        public const string HigherSecretKey = "M@x01E_04nE@19R";
    }
}
