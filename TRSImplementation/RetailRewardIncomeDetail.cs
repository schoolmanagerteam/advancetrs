//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class RetailRewardIncomeDetail
    {
        public long RetailRewardIncomeDetailId { get; set; }
        public long RetailRewardIncomeSummaryId { get; set; }
        public int UserId { get; set; }
        public decimal SelfPurchaseBV { get; set; }
        public decimal DirectTeamPurchaseBV { get; set; }
        public decimal TotalBV { get; set; }
        public decimal RetailRewardIncome { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public bool IsQualified { get; set; }
        public string UTRRefNo { get; set; }
        public Nullable<bool> IsPayout { get; set; }
    
        public virtual RetailRewardIncomeSummary RetailRewardIncomeSummary { get; set; }
    }
}
