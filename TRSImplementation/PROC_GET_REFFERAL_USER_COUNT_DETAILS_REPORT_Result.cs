//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    
    public partial class PROC_GET_REFFERAL_USER_COUNT_DETAILS_REPORT_Result
    {
        public int RefferalByUserId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public Nullable<decimal> MobileNo { get; set; }
        public string EmailId { get; set; }
        public string RefferalCode { get; set; }
        public Nullable<System.DateTime> ActivateDate { get; set; }
        public string ActivationDate { get; set; }
        public Nullable<bool> IsRegistrationActivated { get; set; }
    }
}
