//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    
    public partial class FN_GET_USER_LEADER_UPLINE_SUPPORT_POINTS_DATE_WISE_Result
    {
        public Nullable<decimal> UserId { get; set; }
        public Nullable<System.DateTime> CreditDate { get; set; }
        public Nullable<decimal> CreditPoints { get; set; }
    }
}
