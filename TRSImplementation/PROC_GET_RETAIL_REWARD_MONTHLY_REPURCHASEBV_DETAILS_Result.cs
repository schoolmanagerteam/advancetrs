//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    
    public partial class PROC_GET_RETAIL_REWARD_MONTHLY_REPURCHASEBV_DETAILS_Result
    {
        public long OrderNo { get; set; }
        public Nullable<double> RepurchaseMatchingVolume { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string RefferalCode { get; set; }
    }
}
