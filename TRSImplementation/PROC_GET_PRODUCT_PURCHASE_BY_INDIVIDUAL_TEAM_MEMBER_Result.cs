//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TRSImplementation
{
    using System;
    
    public partial class PROC_GET_PRODUCT_PURCHASE_BY_INDIVIDUAL_TEAM_MEMBER_Result
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public System.DateTime OrderDate { get; set; }
        public long OrderNo { get; set; }
        public string PurchaseType { get; set; }
        public int CreditType { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string ProductName { get; set; }
        public Nullable<double> Amount { get; set; }
    }
}
