﻿namespace TRSCalculations
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Net;
    using System.Net.Mail;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using TRSImplementation;

    internal class Program
    {
        private static string GetCalculationTime(TimeSpan ts) => 
            $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{(ts.Milliseconds / 10):00}";

        public static List<int> GetUserList()
        {
            using (TRSEntities entities = new TRSEntities(0xe10))
            {
                ParameterExpression expression = Expression.Parameter(typeof(UserMst), "i");
                ParameterExpression[] parameters = new ParameterExpression[] { expression };
                IOrderedQueryable<UserMst> source = entities.UserMsts.OrderBy<UserMst, int>(Expression.Lambda<Func<UserMst, int>>(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_UserId)), parameters));
                expression = Expression.Parameter(typeof(UserMst), "i");
                ParameterExpression[] expressionArray2 = new ParameterExpression[] { expression };
                return source.Select<UserMst, int>(Expression.Lambda<Func<UserMst, int>>(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_UserId)), expressionArray2)).ToList<int>();
            }
        }

        private static void Main(string[] args)
        {
            string str = DateTime.Now.AddDays((double) Convert.ToInt32(ConfigurationManager.AppSettings["DayMinusReport"].ToString())).ToString("dd/MM/yyyy");
            string toMailId = ConfigurationManager.AppSettings["ReportSendTo"].ToString();
            string str3 = ConfigurationManager.AppSettings["ApplicationRunFor"].ToString();
            Stopwatch stopwatch = new Stopwatch();
            int count = 0;
            try
            {
                DateTime? nullable;
                List<ParentMemberDtl> list2;
                Manager manager;
                double num8;
                double num9;
                double num10;
                List<UserMatchingBVDtl> list5;
                ParameterExpression expression;
                List<ParentMemberDtl>.Enumerator enumerator;
                bool? isGoldMember;
                if (str3 != "1")
                {
                    if (str3 == "2")
                    {
                        stopwatch.Start();
                        list2 = null;
                        using (TRSEntities entities6 = new TRSEntities(0xe10))
                        {
                            expression = Expression.Parameter(typeof(UserMst), "i");
                            ParameterExpression[] parameters = new ParameterExpression[] { expression };
                            expression = Expression.Parameter(typeof(FN_GET_ALL_CHILD_USER_TREE_Result), "i");
                            ParameterExpression[] expressionArray2 = new ParameterExpression[] { expression };
                            IOrderedQueryable<FN_GET_ALL_CHILD_USER_TREE_Result> source = entities6.FN_GET_ALL_CHILD_USER_TREE(new int?(entities6.UserMsts.Where<UserMst>(Expression.Lambda<Func<UserMst, bool>>(Expression.Equal(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_RefferalUserId)), Expression.Constant(0, typeof(int))), parameters)).FirstOrDefault<UserMst>().UserId), true).OrderBy<FN_GET_ALL_CHILD_USER_TREE_Result, int?>(Expression.Lambda<Func<FN_GET_ALL_CHILD_USER_TREE_Result, int?>>(Expression.Property(expression, (MethodInfo) methodof(FN_GET_ALL_CHILD_USER_TREE_Result.get_UserLevel)), expressionArray2));
                            expression = Expression.Parameter(typeof(FN_GET_ALL_CHILD_USER_TREE_Result), "i");
                            MemberBinding[] bindings = new MemberBinding[] { Expression.Bind((MethodInfo) methodof(ParentMemberDtl.set_FullName), Expression.Property(expression, (MethodInfo) methodof(FN_GET_ALL_CHILD_USER_TREE_Result.get_FullName))), Expression.Bind((MethodInfo) methodof(ParentMemberDtl.set_UserId), Expression.Property(Expression.Property(expression, (MethodInfo) methodof(FN_GET_ALL_CHILD_USER_TREE_Result.get_UserId)), (MethodInfo) methodof(int?.get_Value, int?))), Expression.Bind((MethodInfo) methodof(ParentMemberDtl.set_UserLevel), Expression.Property(Expression.Property(expression, (MethodInfo) methodof(FN_GET_ALL_CHILD_USER_TREE_Result.get_UserLevel)), (MethodInfo) methodof(int?.get_Value, int?))) };
                            ParameterExpression[] expressionArray3 = new ParameterExpression[] { expression };
                            expression = Expression.Parameter(typeof(ParentMemberDtl), "i");
                            ParameterExpression[] expressionArray4 = new ParameterExpression[] { expression };
                            list2 = source.Select<FN_GET_ALL_CHILD_USER_TREE_Result, ParentMemberDtl>(Expression.Lambda<Func<FN_GET_ALL_CHILD_USER_TREE_Result, ParentMemberDtl>>(Expression.MemberInit(Expression.New(typeof(ParentMemberDtl)), bindings), expressionArray3)).OrderBy<ParentMemberDtl, int>(Expression.Lambda<Func<ParentMemberDtl, int>>(Expression.Property(expression, (MethodInfo) methodof(ParentMemberDtl.get_UserLevel)), expressionArray4)).ToList<ParentMemberDtl>();
                        }
                        count = list2.Count;
                        List<string> listConfigKeys = new List<string> { 
                            "GOLDINCOMEBV",
                            "SILVERINCOMEBV",
                            "PRIMEINCOMEBV",
                            "DIAMONDINCOMEBV"
                        };
                        manager = new Manager();
                        List<ConfigMst> configValues = manager.GetConfigValues(listConfigKeys);
                        double num7 = Convert.ToDouble(manager.GetConfigValueFromConfigList(configValues, "GOLDINCOMEBV"));
                        num8 = Convert.ToDouble(manager.GetConfigValueFromConfigList(configValues, "SILVERINCOMEBV"));
                        num9 = Convert.ToDouble(manager.GetConfigValueFromConfigList(configValues, "PRIMEINCOMEBV"));
                        num10 = Convert.ToDouble(manager.GetConfigValueFromConfigList(configValues, "DIAMONDINCOMEBV"));
                        list5 = new List<UserMatchingBVDtl>();
                        using (TRSEntities entities7 = new TRSEntities(0x2710))
                        {
                            int num11 = 1;
                            Console.WriteLine("IsGoldMember Update Status Start");
                            Console.WriteLine("");
                            using (enumerator = list2.GetEnumerator())
                            {
                                while (true)
                                {
                                    ParentMemberDtl item;
                                    while (true)
                                    {
                                        if (enumerator.MoveNext())
                                        {
                                            <>c__DisplayClass0_0 class_;
                                            item = enumerator.Current;
                                            object[] objArray7 = new object[] { "IsGoldMember Status Updated Start For UserNo : ", num11, " ,UserId : ", item.UserId, " ,UserName : ", item.FullName };
                                            Console.WriteLine(string.Concat(objArray7));
                                            Console.WriteLine("");
                                            expression = Expression.Parameter(typeof(UserMst), "i");
                                            ParameterExpression[] parameters = new ParameterExpression[] { expression };
                                            UserMst mst2 = entities7.UserMsts.Where<UserMst>(Expression.Lambda<Func<UserMst, bool>>(Expression.Equal(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_UserId)), Expression.Property(Expression.Field(Expression.Constant(class_, typeof(<>c__DisplayClass0_0)), fieldof(<>c__DisplayClass0_0.item)), (MethodInfo) methodof(ParentMemberDtl.get_UserId))), parameters)).FirstOrDefault<UserMst>();
                                            if (mst2.IsGoldMember != null)
                                            {
                                                isGoldMember = mst2.IsGoldMember;
                                                if (isGoldMember.Value)
                                                {
                                                    break;
                                                }
                                            }
                                            nullable = null;
                                            nullable = null;
                                            double num12 = manager.GetMatchingBVValues(item.UserId, nullable, nullable);
                                            UserMatchingBVDtl dtl1 = new UserMatchingBVDtl();
                                            dtl1.UserId = item.UserId;
                                            dtl1.MatchingBV = num12;
                                            list5.Add(dtl1);
                                            if (num12 >= num7)
                                            {
                                                mst2.IsPrimeMember = false;
                                                mst2.IsSilverMember = false;
                                                mst2.IsGoldMember = true;
                                                mst2.GoldMemberDate = entities7.PROC_GET_SERVER_DATE().FirstOrDefault<DateTime?>();
                                                entities7.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            goto TR_0082;
                                        }
                                        break;
                                    }
                                    object[] objArray8 = new object[] { "IsGoldMember Status Updated End For UserNo : ", num11, " ,UserId : ", item.UserId, " ,UserName : ", item.FullName };
                                    Console.WriteLine(string.Concat(objArray8));
                                    Console.WriteLine("");
                                    Console.WriteLine("");
                                    num11++;
                                }
                            }
                        TR_0082:
                            Console.WriteLine("IsGoldMember Update Status Start");
                            Console.WriteLine("");
                            Console.WriteLine("");
                            goto TR_0081;
                        }
                    }
                }
                else
                {
                    stopwatch.Start();
                    int num3 = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfUserForCalc"].ToString());
                    List<int> userList = GetUserList();
                    Console.WriteLine("Total Users : " + userList.Count);
                    Console.WriteLine("");
                    Console.WriteLine("");
                    int num4 = 1;
                    int num5 = 1;
                    count = userList.Count;
                    while (true)
                    {
                        if (num4 > userList.Count)
                        {
                            Console.WriteLine("Diamond Income Calculation Start");
                            Console.WriteLine("");
                            using (TRSEntities entities2 = new TRSEntities(0xe10))
                            {
                                nullable = null;
                                nullable = null;
                                if (entities2.PROC_INSERT_DIAMOND_INCOME_OF_USER(0, 0, nullable, nullable) > 0)
                                {
                                    Console.WriteLine("Diamond Income Calculated Successfully");
                                }
                            }
                            Console.WriteLine("Diamond Income Calculation End");
                            Console.WriteLine("");
                            Console.WriteLine("");
                            Console.WriteLine("Gold Income Calculation Start");
                            Console.WriteLine("");
                            using (TRSEntities entities3 = new TRSEntities(0xe10))
                            {
                                nullable = null;
                                nullable = null;
                                if (entities3.PROC_INSERT_GOLD_INCOME_OF_USER(0, 0, nullable, nullable) > 0)
                                {
                                    Console.WriteLine("Gold Income Calculated Successfully");
                                }
                            }
                            Console.WriteLine("Gold Income Calculation End");
                            Console.WriteLine("");
                            Console.WriteLine("");
                            Console.WriteLine("Silver Income Calculation Start");
                            Console.WriteLine("");
                            using (TRSEntities entities4 = new TRSEntities(0xe10))
                            {
                                nullable = null;
                                nullable = null;
                                if (entities4.PROC_INSERT_SILVER_INCOME_OF_USER(0, 0, nullable, nullable) > 0)
                                {
                                    Console.WriteLine("Silver Income Calculated Successfully");
                                }
                            }
                            Console.WriteLine("Silver Income Calculation End");
                            Console.WriteLine("");
                            Console.WriteLine("");
                            Console.WriteLine("Prime Associate Income Calculation Start");
                            Console.WriteLine("");
                            using (TRSEntities entities5 = new TRSEntities(0xe10))
                            {
                                nullable = null;
                                nullable = null;
                                if (entities5.PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER(0, 0, nullable, nullable) > 0)
                                {
                                    Console.WriteLine("Prime Associate Income Calculated Successfully");
                                }
                            }
                            Console.WriteLine("Prime Associate Income Calculation End");
                            Console.WriteLine("");
                            Console.WriteLine("");
                            stopwatch.Stop();
                            TimeSpan ts = stopwatch.Elapsed;
                            object[] objArray6 = new object[] { "Success, Total Users : ", count, ", Total Time : ", GetCalculationTime(ts) };
                            SendMail(toMailId, "Income Calculate Date :- " + str, string.Concat(objArray6));
                            break;
                        }
                        Stopwatch stopwatch2 = new Stopwatch();
                        stopwatch2.Start();
                        int num6 = num3 * num5;
                        Console.WriteLine("User No : " + num4);
                        object[] objArray1 = new object[] { "Matching Income Calculation Start For From User No : ", num4, " to To User No : ", num6 };
                        Console.WriteLine(string.Concat(objArray1));
                        using (TRSEntities entities = new TRSEntities(0xe10))
                        {
                            nullable = null;
                            nullable = null;
                            if (entities.PROC_INSERT_MATCHING_INCOME_OF_USER(0, nullable, nullable, new int?(num4), new int?(num6)) > 0)
                            {
                                object[] objArray2 = new object[] { "Matching Income Calculated Successfully For From User No : ", num4, " to To User No : ", num6 };
                                Console.WriteLine(string.Concat(objArray2));
                            }
                        }
                        object[] objArray3 = new object[] { "Matching Income Calculation End For From User No : ", num4, " to To User No : ", num6 };
                        Console.WriteLine(string.Concat(objArray3));
                        stopwatch2.Stop();
                        TimeSpan span2 = stopwatch2.Elapsed;
                        string str4 = $"{span2.Hours:00}:{span2.Minutes:00}:{span2.Seconds:00}.{span2.Milliseconds / 10:00}";
                        object[] objArray5 = new object[] { "RunTime For From User No : ", num4, " to To User No : ", num6, " :- ", str4 };
                        Console.WriteLine(string.Concat(objArray5));
                        Console.WriteLine("");
                        Console.WriteLine("");
                        num4 = (num3 * num5) + 1;
                        num5++;
                    }
                }
                return;
            TR_0045:
                stopwatch.Stop();
                TimeSpan elapsed = stopwatch.Elapsed;
                object[] objArray15 = new object[] { "Success, Total Users : ", count, ", Total Time : ", GetCalculationTime(elapsed) };
                SendMail(toMailId, "User Status Update Date :- " + str, string.Concat(objArray15));
                return;
            TR_005A:
                using (TRSEntities entities10 = new TRSEntities(0x2710))
                {
                    int num17 = 1;
                    Console.WriteLine("IsSilverMember Update Status Start");
                    Console.WriteLine("");
                    using (enumerator = list2.GetEnumerator())
                    {
                        while (true)
                        {
                            ParentMemberDtl dtl1;
                            while (true)
                            {
                                if (enumerator.MoveNext())
                                {
                                    <>c__DisplayClass0_4 class_5;
                                    dtl1 = enumerator.Current;
                                    object[] objArray13 = new object[] { "IsSilverMember Status Updated Start For UserNo : ", num17, " ,UserId : ", dtl1.UserId, " ,UserName : ", dtl1.FullName };
                                    Console.WriteLine(string.Concat(objArray13));
                                    Console.WriteLine("");
                                    expression = Expression.Parameter(typeof(UserMst), "i");
                                    ParameterExpression[] parameters = new ParameterExpression[] { expression };
                                    UserMst mst5 = entities10.UserMsts.Where<UserMst>(Expression.Lambda<Func<UserMst, bool>>(Expression.Equal(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_UserId)), Expression.Property(Expression.Field(Expression.Constant(class_5, typeof(<>c__DisplayClass0_4)), fieldof(<>c__DisplayClass0_4.item)), (MethodInfo) methodof(ParentMemberDtl.get_UserId))), parameters)).FirstOrDefault<UserMst>();
                                    if (mst5.IsSilverMember != null)
                                    {
                                        isGoldMember = mst5.IsSilverMember;
                                        if (isGoldMember.Value)
                                        {
                                            break;
                                        }
                                    }
                                    double matchingBV = 0.0;
                                    if (list5.Any<UserMatchingBVDtl>(i => i.UserId == dtl1.UserId))
                                    {
                                        matchingBV = list5.FirstOrDefault<UserMatchingBVDtl>(i => (i.UserId == dtl1.UserId)).MatchingBV;
                                    }
                                    else
                                    {
                                        nullable = null;
                                        nullable = null;
                                        matchingBV = manager.GetMatchingBVValues(dtl1.UserId, nullable, nullable);
                                    }
                                    int[] listParentParentMemberDtl = (from i in entities10.PROC_GET_ALL_PARENT_USER_TREE(new int?(dtl1.UserId), false) select i.UserId.Value).ToArray<int>();
                                    if (matchingBV >= num8)
                                    {
                                        <>c__DisplayClass0_5 class_6;
                                        expression = Expression.Parameter(typeof(UserMst), "i");
                                        Expression[] arguments = new Expression[] { Expression.Field(Expression.Constant(class_6, typeof(<>c__DisplayClass0_5)), fieldof(<>c__DisplayClass0_5.listParentParentMemberDtl)), Expression.Property(expression, (MethodInfo) methodof(UserMst.get_UserId)) };
                                        ParameterExpression[] expressionArray12 = new ParameterExpression[] { expression };
                                        if (entities10.UserMsts.Any<UserMst>(Expression.Lambda<Func<UserMst, bool>>(Expression.AndAlso(Expression.Call(null, (MethodInfo) methodof(Enumerable.Contains), arguments), Expression.Equal(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_IsDimondMember)), Expression.Convert(Expression.Constant(true, typeof(bool)), typeof(bool?)))), expressionArray12)))
                                        {
                                            mst5.IsPrimeMember = false;
                                            mst5.IsSilverMember = true;
                                            mst5.SilverMemberDate = entities10.PROC_GET_SERVER_DATE().FirstOrDefault<DateTime?>();
                                            entities10.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    goto TR_0046;
                                }
                                break;
                            }
                            object[] objArray14 = new object[] { "IsSilverMember Status Updated End For UserNo : ", num17, " ,UserId : ", dtl1.UserId, " ,UserName : ", dtl1.FullName };
                            Console.WriteLine(string.Concat(objArray14));
                            Console.WriteLine("");
                            Console.WriteLine("");
                            num17++;
                        }
                    }
                TR_0046:
                    Console.WriteLine("IsSilverMember Update Status Start");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    goto TR_0045;
                }
            TR_006F:
                using (TRSEntities entities9 = new TRSEntities(0x2710))
                {
                    int num15 = 1;
                    Console.WriteLine("IsPrimeMember Update Status Start");
                    Console.WriteLine("");
                    using (enumerator = list2.GetEnumerator())
                    {
                        while (true)
                        {
                            ParentMemberDtl dtl2;
                            while (true)
                            {
                                if (enumerator.MoveNext())
                                {
                                    <>c__DisplayClass0_2 class_3;
                                    dtl2 = enumerator.Current;
                                    object[] objArray11 = new object[] { "IsPrimeMember Status Updated Start For UserNo : ", num15, " ,UserId : ", dtl2.UserId, " ,UserName : ", dtl2.FullName };
                                    Console.WriteLine(string.Concat(objArray11));
                                    Console.WriteLine("");
                                    expression = Expression.Parameter(typeof(UserMst), "i");
                                    ParameterExpression[] parameters = new ParameterExpression[] { expression };
                                    UserMst mst4 = entities9.UserMsts.Where<UserMst>(Expression.Lambda<Func<UserMst, bool>>(Expression.Equal(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_UserId)), Expression.Property(Expression.Field(Expression.Constant(class_3, typeof(<>c__DisplayClass0_2)), fieldof(<>c__DisplayClass0_2.item)), (MethodInfo) methodof(ParentMemberDtl.get_UserId))), parameters)).FirstOrDefault<UserMst>();
                                    if (mst4.IsPrimeMember != null)
                                    {
                                        isGoldMember = mst4.IsPrimeMember;
                                        if (isGoldMember.Value)
                                        {
                                            break;
                                        }
                                    }
                                    double matchingBV = 0.0;
                                    if (list5.Any<UserMatchingBVDtl>(i => i.UserId == dtl2.UserId))
                                    {
                                        matchingBV = list5.FirstOrDefault<UserMatchingBVDtl>(i => (i.UserId == dtl2.UserId)).MatchingBV;
                                    }
                                    else
                                    {
                                        nullable = null;
                                        nullable = null;
                                        matchingBV = manager.GetMatchingBVValues(dtl2.UserId, nullable, nullable);
                                        UserMatchingBVDtl item = new UserMatchingBVDtl();
                                        item.UserId = dtl2.UserId;
                                        item.MatchingBV = matchingBV;
                                        list5.Add(item);
                                    }
                                    int[] numArray1 = (from i in entities9.PROC_GET_ALL_PARENT_USER_TREE(new int?(dtl2.UserId), false) select i.UserId.Value).ToArray<int>();
                                    if (matchingBV >= num9)
                                    {
                                        <>c__DisplayClass0_3 class_4;
                                        expression = Expression.Parameter(typeof(UserMst), "i");
                                        Expression[] arguments = new Expression[] { Expression.Field(Expression.Constant(class_4, typeof(<>c__DisplayClass0_3)), fieldof(<>c__DisplayClass0_3.listParentParentMemberDtl)), Expression.Property(expression, (MethodInfo) methodof(UserMst.get_UserId)) };
                                        ParameterExpression[] expressionArray9 = new ParameterExpression[] { expression };
                                        if (entities9.UserMsts.Any<UserMst>(Expression.Lambda<Func<UserMst, bool>>(Expression.AndAlso(Expression.Call(null, (MethodInfo) methodof(Enumerable.Contains), arguments), Expression.Equal(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_IsGoldMember)), Expression.Convert(Expression.Constant(true, typeof(bool)), typeof(bool?)))), expressionArray9)))
                                        {
                                            mst4.IsPrimeMember = true;
                                            mst4.PrimeMemberDate = entities9.PROC_GET_SERVER_DATE().FirstOrDefault<DateTime?>();
                                            entities9.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    goto TR_005B;
                                }
                                break;
                            }
                            object[] objArray12 = new object[] { "IsPrimeMember Status Updated End For UserNo : ", num15, " ,UserId : ", dtl2.UserId, " ,UserName : ", dtl2.FullName };
                            Console.WriteLine(string.Concat(objArray12));
                            Console.WriteLine("");
                            Console.WriteLine("");
                            num15++;
                        }
                    }
                TR_005B:
                    Console.WriteLine("IsPrimeMember Update Status Start");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    goto TR_005A;
                }
            TR_0081:
                using (TRSEntities entities8 = new TRSEntities(0x2710))
                {
                    int num13 = 1;
                    Console.WriteLine("IsDimondMember Update Status Start");
                    Console.WriteLine("");
                    using (enumerator = list2.GetEnumerator())
                    {
                        while (true)
                        {
                            ParentMemberDtl dtl3;
                            while (true)
                            {
                                if (enumerator.MoveNext())
                                {
                                    <>c__DisplayClass0_1 class_2;
                                    dtl3 = enumerator.Current;
                                    object[] objArray9 = new object[] { "IsDimondMember Status Updated Start For UserNo : ", num13, " ,UserId : ", dtl3.UserId, " ,UserName : ", dtl3.FullName };
                                    Console.WriteLine(string.Concat(objArray9));
                                    Console.WriteLine("");
                                    expression = Expression.Parameter(typeof(UserMst), "i");
                                    ParameterExpression[] parameters = new ParameterExpression[] { expression };
                                    UserMst mst3 = entities8.UserMsts.Where<UserMst>(Expression.Lambda<Func<UserMst, bool>>(Expression.Equal(Expression.Property(expression, (MethodInfo) methodof(UserMst.get_UserId)), Expression.Property(Expression.Field(Expression.Constant(class_2, typeof(<>c__DisplayClass0_1)), fieldof(<>c__DisplayClass0_1.item)), (MethodInfo) methodof(ParentMemberDtl.get_UserId))), parameters)).FirstOrDefault<UserMst>();
                                    if (mst3.IsDimondMember != null)
                                    {
                                        isGoldMember = mst3.IsDimondMember;
                                        if (isGoldMember.Value)
                                        {
                                            break;
                                        }
                                    }
                                    double matchingBV = 0.0;
                                    if (list5.Any<UserMatchingBVDtl>(i => i.UserId == dtl3.UserId))
                                    {
                                        matchingBV = list5.FirstOrDefault<UserMatchingBVDtl>(i => (i.UserId == dtl3.UserId)).MatchingBV;
                                    }
                                    else
                                    {
                                        nullable = null;
                                        nullable = null;
                                        matchingBV = manager.GetMatchingBVValues(dtl3.UserId, nullable, nullable);
                                        UserMatchingBVDtl item = new UserMatchingBVDtl();
                                        item.UserId = dtl3.UserId;
                                        item.MatchingBV = matchingBV;
                                        list5.Add(item);
                                    }
                                    if (matchingBV >= num10)
                                    {
                                        mst3.IsGoldMember = false;
                                        mst3.IsPrimeMember = false;
                                        mst3.IsSilverMember = false;
                                        mst3.IsDimondMember = true;
                                        mst3.DimondMemberDate = entities8.PROC_GET_SERVER_DATE().FirstOrDefault<DateTime?>();
                                        entities8.SaveChanges();
                                    }
                                }
                                else
                                {
                                    goto TR_0070;
                                }
                                break;
                            }
                            object[] objArray10 = new object[] { "IsDimondMember Status Updated End For UserNo : ", num13, " ,UserId : ", dtl3.UserId, " ,UserName : ", dtl3.FullName };
                            Console.WriteLine(string.Concat(objArray10));
                            Console.WriteLine("");
                            Console.WriteLine("");
                            num13++;
                        }
                    }
                TR_0070:
                    Console.WriteLine("IsDimondMember Update Status Start");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    goto TR_006F;
                }
            }
            catch (Exception exception)
            {
                stopwatch.Stop();
                TimeSpan elapsed = stopwatch.Elapsed;
                string message = exception.Message;
                if (exception.InnerException != null)
                {
                    message = message + "Inner Exception : " + exception.InnerException.Message;
                }
                Console.WriteLine(exception.Message);
                object[] objArray16 = new object[] { "Fail : ", message, ", Total Users : ", count, ", Total Time : ", GetCalculationTime(elapsed) };
                SendMail(toMailId, "Income Calculate Date :- " + str, string.Concat(objArray16));
                Console.ReadLine();
            }
        }

        public static bool SendMail(string ToMailId, string Subject, string Body)
        {
            string userName = ConfigurationManager.AppSettings["EmailFrom"].ToString();
            string str4 = ConfigurationManager.AppSettings["EnabledSSL"].ToString();
            SmtpClient client1 = new SmtpClient(ConfigurationManager.AppSettings["HostName"].ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["Port"].ToString()));
            client1.Credentials = new NetworkCredential(userName, ConfigurationManager.AppSettings["Password"].ToString());
            client1.EnableSsl = Convert.ToBoolean(str4);
            SmtpClient client = client1;
            char[] separator = new char[] { ';', ',' };
            foreach (string str5 in ToMailId.Split(separator))
            {
                client.Send(userName, str5, Subject, Body);
            }
            return false;
        }

        [Serializable, CompilerGenerated]
        private sealed class <>c
        {
            public static readonly Program.<>c <>9 = new Program.<>c();
            public static Func<PROC_GET_ALL_PARENT_USER_TREE_Result, int> <>9__0_4;
            public static Func<PROC_GET_ALL_PARENT_USER_TREE_Result, int> <>9__0_7;

            internal int <Main>b__0_4(PROC_GET_ALL_PARENT_USER_TREE_Result i) => 
                i.UserId.Value;

            internal int <Main>b__0_7(PROC_GET_ALL_PARENT_USER_TREE_Result i) => 
                i.UserId.Value;
        }

        public class UserMatchingBVDtl
        {
            public int UserId { get; set; }

            public double MatchingBV { get; set; }
        }
    }
}

