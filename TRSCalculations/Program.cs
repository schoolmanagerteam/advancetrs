﻿// Decompiled with JetBrains decompiler
// Type: TRSCalculations.Program
// Assembly: TRSCalculations, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E175158A-6191-4706-B720-2EE988CA53D5
// Assembly location: F:\Projects\Maxwellness\Scheduler\Release\TRSCalculations.exe

using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using TRSImplementation;

namespace TRSCalculations
{
    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Environment.MachineName);

        public static void WriteLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string msgFormat = string.Format("{0}-{1}", DateTime.Now, strLog);
            string logFilePath = "F:\\Logs\\";
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }

        private static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            int int32_1 = Convert.ToInt32(ConfigurationManager.AppSettings["DayMinusReport"].ToString());
            DateTime dateTime = DateTime.Now;
            dateTime = dateTime.AddDays((double)int32_1);
            string str1 = dateTime.ToString("dd/MM/yyyy");
            string ToMailId = ConfigurationManager.AppSettings["ReportSendTo"].ToString();
            string str2 = ConfigurationManager.AppSettings["ApplicationRunFor"].ToString();

            bool chkIsRegistrationActivated = false;
            if (!bool.TryParse(ConfigurationManager.AppSettings["chkIsRegistrationActivated"].ToString(), out chkIsRegistrationActivated))
                chkIsRegistrationActivated = false;

            using (TRSEntities trsEntities = new TRSEntities(3600))
            {
                DateTime dtRange = dateTime.AddMonths(-1);
                trsEntities.NotificationMsts.RemoveRange(trsEntities.NotificationMsts.Where(p => p.CreatedDate <= dtRange));
                trsEntities.SaveChanges();
            }

            Stopwatch stopwatch1 = new Stopwatch();
            int num1 = 0;
            try
            {
                if (str2 == "1")//Matching Income
                {
                    stopwatch1.Start();
                    int int32_2 = Convert.ToInt32(ConfigurationManager.AppSettings["NoOfUserForCalc"].ToString());
                    List<int> userList = Program.GetUserList();
                    Logger.Info("Total Users : " + (object)userList.Count);
                    int num2 = 1;
                    int num3 = 1;
                    num1 = userList.Count;

                    DateTime FrmDt = DateTime.Now.AddDays(-7);
                    DateTime ToDt = FrmDt.AddDays(1);

                    while (num2 <= userList.Count)
                    {
                        try
                        {
                            Stopwatch stopwatch2 = new Stopwatch();
                            stopwatch2.Start();
                            int num4 = int32_2 * num3;
                            Logger.Info("User No : " + (object)num2);
                            Logger.Info("Matching Income Calculation Start For From User No : " + (object)num2 + " to To User No : " + (object)num4);
                            using (TRSEntities trsEntities = new TRSEntities(3600))
                            {
                                if (trsEntities.PROC_INSERT_MATCHING_INCOME_OF_USER(new int?(0), new DateTime?(), new DateTime?(), new int?(num2), new int?(num4)) > 0)
                                    Logger.Info("Matching Income Calculated Successfully For From User No : " + (object)num2 + " to To User No : " + (object)num4);
                            }
                            Logger.Info("Matching Income Calculation End For From User No : " + (object)num2 + " to To User No : " + (object)num4);
                            stopwatch2.Stop();
                            TimeSpan elapsed = stopwatch2.Elapsed;
                            string str3 = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", (object)elapsed.Hours, (object)elapsed.Minutes, (object)elapsed.Seconds, (object)(elapsed.Milliseconds / 10));
                            Logger.Info("RunTime For From User No : " + (object)num2 + " to To User No : " + (object)num4 + " :- " + str3);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("Error Occured:" + ex.Message, ex);
                        }
                        finally
                        {
                            num2 = int32_2 * num3 + 1;
                            ++num3;
                        }
                    }
                    Logger.Info("Diamond Income Calculation Start");
                    using (TRSEntities trsEntities = new TRSEntities(3600))
                    {
                        if (trsEntities.PROC_INSERT_DIAMOND_INCOME_OF_USER(new int?(0), new int?(0), new DateTime?(), new DateTime?()) > 0)
                            Logger.Info("Diamond Income Calculated Successfully");
                    }
                    Logger.Info("Diamond Income Calculation End");

                    Logger.Info("Gold Income Calculation Start");

                    using (TRSEntities trsEntities = new TRSEntities(3600))
                    {
                        if (trsEntities.PROC_INSERT_GOLD_INCOME_OF_USER(new int?(0), new int?(0), new DateTime?(), new DateTime?()) > 0)
                            Logger.Info("Gold Income Calculated Successfully");
                    }
                    Logger.Info("Gold Income Calculation End");

                    #region "Silver || Prime Income disable"

                    //Logger.Info("Silver Income Calculation Start");
                    //
                    //using (TRSEntities trsEntities = new TRSEntities(3600))
                    //{
                    //    if (trsEntities.PROC_INSERT_SILVER_INCOME_OF_USER(new int?(0), new int?(0), new DateTime?(), new DateTime?()) > 0)
                    //        Logger.Info("Silver Income Calculated Successfully");
                    //}
                    //Logger.Info("Silver Income Calculation End");
                    //
                    //
                    //Logger.Info("Prime Associate Income Calculation Start");
                    //
                    //using (TRSEntities trsEntities = new TRSEntities(3600))
                    //{
                    //    if (trsEntities.PROC_INSERT_PRIME_ASSOCIATE_INCOME_OF_USER(new int?(0), new int?(0), new DateTime?(), new DateTime?()) > 0)
                    //        Logger.Info("Prime Associate Income Calculated Successfully");
                    //}
                    //Logger.Info("Prime Associate Income Calculation End");

                    #endregion "Silver || Prime Income disable"

                    stopwatch1.Stop();
                    TimeSpan elapsed1 = stopwatch1.Elapsed;
                    Program.SendMail(ToMailId, "Income Calculate Date :- " + str1, "Success, Total Users : " + (object)num1 + ", Total Time : " + Program.GetCalculationTime(elapsed1));
                }
                else
                {
                    if (!(str2 == "2"))
                        return;
                    stopwatch1.Start();
                    List<ParentMemberDtl> parentMemberDtlList = (List<ParentMemberDtl>)null;
                    using (TRSEntities trsEntities = new TRSEntities(3600))
                    {
                        UserMst userMst = trsEntities.UserMsts.Where<UserMst>((Expression<Func<UserMst, bool>>)(i => i.RefferalUserId == 0)).FirstOrDefault<UserMst>();
                        ParameterExpression parameterExpression;
                        // ISSUE: method reference
                        // ISSUE: method reference
                        // ISSUE: method reference
                        //parentMemberDtlList = trsEntities.FN_GET_ALL_CHILD_USER_TREE(new int?(userMst.UserId), new bool?(true)).OrderBy<FN_GET_ALL_CHILD_USER_TREE_Result, int?>((Expression<Func<FN_GET_ALL_CHILD_USER_TREE_Result, int?>>) (i => i.UserLevel)).Select<FN_GET_ALL_CHILD_USER_TREE_Result, ParentMemberDtl>(Expression.Lambda<Func<FN_GET_ALL_CHILD_USER_TREE_Result, ParentMemberDtl>>((Expression) Expression.MemberInit(Expression.New(typeof (ParentMemberDtl)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(ParentMemberDtl.set_FullName),null)))); //unable to render the statement

                        if (chkIsRegistrationActivated)
                        {
                            parentMemberDtlList = trsEntities.FN_GET_ALL_CHILD_USER_TREE(new int?(userMst.UserId), new bool?(true))
                                        .Where(p => p.IsRegistered == true)
                                        .Select(i => new ParentMemberDtl { FullName = i.FullName, UserId = i.UserId.Value }).ToList();
                        }
                        else
                        {
                            parentMemberDtlList = trsEntities.FN_GET_ALL_CHILD_USER_TREE(new int?(userMst.UserId), new bool?(true))
                                        .Select(i => new ParentMemberDtl { FullName = i.FullName, UserId = i.UserId.Value }).ToList();
                        }
                    }
                    num1 = parentMemberDtlList.Count;
                    List<string> listConfigKeys = new List<string>();
                    listConfigKeys.Add("GOLDINCOMEBV");
                    listConfigKeys.Add("SILVERINCOMEBV");
                    listConfigKeys.Add("PRIMEINCOMEBV");
                    listConfigKeys.Add("DIAMONDINCOMEBV");
                    Manager manager = new Manager();
                    List<ConfigMst> configValues = manager.GetConfigValues(listConfigKeys);
                    double num2 = Convert.ToDouble(manager.GetConfigValueFromConfigList(configValues, "GOLDINCOMEBV"));
                    double num3 = Convert.ToDouble(manager.GetConfigValueFromConfigList(configValues, "SILVERINCOMEBV"));
                    double num4 = Convert.ToDouble(manager.GetConfigValueFromConfigList(configValues, "PRIMEINCOMEBV"));
                    double num5 = Convert.ToDouble(manager.GetConfigValueFromConfigList(configValues, "DIAMONDINCOMEBV"));
                    List<Program.UserMatchingBVDtl> source = new List<Program.UserMatchingBVDtl>();
                    using (TRSEntities trsEntities = new TRSEntities(10000))
                    {
                        int num6 = 1;
                        Logger.Info("IsGoldMember Update Status Start");

                        foreach (ParentMemberDtl parentMemberDtl in parentMemberDtlList)
                        {
                            ParentMemberDtl item = parentMemberDtl;
                            Logger.Info("IsGoldMember Status Updated Start For UserNo : " + (object)num6 + " ,UserId : " + (object)item.UserId + " ,UserName : " + item.FullName);

                            UserMst userMst = trsEntities.UserMsts.Where<UserMst>((Expression<Func<UserMst, bool>>)(i => i.UserId == item.UserId)).FirstOrDefault<UserMst>();
                            if (!userMst.IsGoldMember.HasValue || !userMst.IsGoldMember.Value)
                            {
                                double matchingBvValues = manager.GetMatchingBVValues(item.UserId, new DateTime?(), new DateTime?());
                                source.Add(new Program.UserMatchingBVDtl()
                                {
                                    UserId = item.UserId,
                                    MatchingBV = matchingBvValues
                                });
                                if (matchingBvValues >= num2)
                                {
                                    userMst.IsPrimeMember = new bool?(false);
                                    userMst.IsSilverMember = new bool?(false);
                                    userMst.IsDimondMember = new bool?(false);
                                    userMst.IsGoldMember = new bool?(true);

                                    var dt = trsEntities.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)num2).FirstOrDefault();
                                    userMst.GoldMemberDate = dt;
                                    trsEntities.SaveChanges();
                                }
                            }
                            Logger.Info("IsGoldMember Status Updated End For UserNo : " + (object)num6 + " ,UserId : " + (object)item.UserId + " ,UserName : " + item.FullName);
                            ++num6;
                        }
                        Logger.Info("IsGoldMember Update Status Start");
                    }
                    using (TRSEntities trsEntities = new TRSEntities(10000))
                    {
                        int num6 = 1;
                        Logger.Info("IsDimondMember Update Status Start");

                        foreach (ParentMemberDtl parentMemberDtl in parentMemberDtlList)
                        {
                            ParentMemberDtl item = parentMemberDtl;
                            Logger.Info("IsDimondMember Status Updated Start For UserNo : " + (object)num6 + " ,UserId : " + (object)item.UserId + " ,UserName : " + item.FullName);

                            UserMst userMst = trsEntities.UserMsts.Where<UserMst>((Expression<Func<UserMst, bool>>)(i => i.UserId == item.UserId)).FirstOrDefault<UserMst>();
                            if (!userMst.IsDimondMember.HasValue || !userMst.IsDimondMember.Value)
                            {
                                double num7;
                                if (source.Any<Program.UserMatchingBVDtl>((Func<Program.UserMatchingBVDtl, bool>)(i => i.UserId == item.UserId)))
                                {
                                    num7 = source.FirstOrDefault<Program.UserMatchingBVDtl>((Func<Program.UserMatchingBVDtl, bool>)(i => i.UserId == item.UserId)).MatchingBV;
                                }
                                else
                                {
                                    num7 = manager.GetMatchingBVValues(item.UserId, new DateTime?(), new DateTime?());
                                    source.Add(new Program.UserMatchingBVDtl()
                                    {
                                        UserId = item.UserId,
                                        MatchingBV = num7
                                    });
                                }
                                if (num7 >= num5)
                                {
                                    userMst.IsGoldMember = new bool?(false);
                                    userMst.IsPrimeMember = new bool?(false);
                                    userMst.IsSilverMember = new bool?(false);
                                    userMst.IsDimondMember = new bool?(true);

                                    var dt = trsEntities.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)num5).FirstOrDefault();
                                    userMst.DimondMemberDate = dt;
                                    trsEntities.SaveChanges();
                                }
                            }
                            Logger.Info("IsDimondMember Status Updated End For UserNo : " + (object)num6 + " ,UserId : " + (object)item.UserId + " ,UserName : " + item.FullName);
                            ++num6;
                        }
                        Logger.Info("IsDimondMember Update Status Start");
                    }
                    using (TRSEntities trsEntities = new TRSEntities(10000))
                    {
                        int num6 = 1;
                        Logger.Info("IsPrimeMember Update Status Start");

                        foreach (ParentMemberDtl parentMemberDtl in parentMemberDtlList)
                        {
                            ParentMemberDtl item = parentMemberDtl;
                            Logger.Info("IsPrimeMember Status Updated Start For UserNo : " + (object)num6 + " ,UserId : " + (object)item.UserId + " ,UserName : " + item.FullName);

                            UserMst userMst = trsEntities.UserMsts.Where<UserMst>((Expression<Func<UserMst, bool>>)(i => i.UserId == item.UserId)).FirstOrDefault<UserMst>();
                            if (!userMst.IsPrimeMember.HasValue || !userMst.IsPrimeMember.Value)
                            {
                                double num7;
                                if (source.Any<Program.UserMatchingBVDtl>((Func<Program.UserMatchingBVDtl, bool>)(i => i.UserId == item.UserId)))
                                {
                                    num7 = source.FirstOrDefault<Program.UserMatchingBVDtl>((Func<Program.UserMatchingBVDtl, bool>)(i => i.UserId == item.UserId)).MatchingBV;
                                }
                                else
                                {
                                    num7 = manager.GetMatchingBVValues(item.UserId, new DateTime?(), new DateTime?());
                                    source.Add(new Program.UserMatchingBVDtl()
                                    {
                                        UserId = item.UserId,
                                        MatchingBV = num7
                                    });
                                }
                                int[] listParentParentMemberDtl = trsEntities.PROC_GET_ALL_PARENT_USER_TREE(new int?(item.UserId), new bool?(false)).Select<PROC_GET_ALL_PARENT_USER_TREE_Result, int>((Func<PROC_GET_ALL_PARENT_USER_TREE_Result, int>)(i => i.UserId.Value)).ToArray<int>();
                                if (num7 >= num4)
                                {
                                    if (trsEntities.UserMsts.Any<UserMst>((Expression<Func<UserMst, bool>>)(i => listParentParentMemberDtl.Contains<int>(i.UserId) && i.IsGoldMember == (bool?)true)))
                                    {
                                        userMst.IsGoldMember = new bool?(false);
                                        userMst.IsSilverMember = new bool?(false);
                                        userMst.IsDimondMember = new bool?(false);
                                        userMst.IsPrimeMember = new bool?(true);

                                        var dt = trsEntities.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)num4).FirstOrDefault();

                                        userMst.PrimeMemberDate = dt;
                                        trsEntities.SaveChanges();
                                    }
                                }
                            }
                            Logger.Info("IsPrimeMember Status Updated End For UserNo : " + (object)num6 + " ,UserId : " + (object)item.UserId + " ,UserName : " + item.FullName);
                            ++num6;
                        }
                        Logger.Info("IsPrimeMember Update Status Start");
                    }
                    using (TRSEntities trsEntities = new TRSEntities(10000))
                    {
                        int num6 = 1;
                        Logger.Info("IsSilverMember Update Status Start");

                        foreach (ParentMemberDtl parentMemberDtl in parentMemberDtlList)
                        {
                            ParentMemberDtl item = parentMemberDtl;
                            Logger.Info("IsSilverMember Status Updated Start For UserNo : " + (object)num6 + " ,UserId : " + (object)item.UserId + " ,UserName : " + item.FullName);

                            UserMst userMst = trsEntities.UserMsts.Where<UserMst>((Expression<Func<UserMst, bool>>)(i => i.UserId == item.UserId)).FirstOrDefault<UserMst>();
                            if (!userMst.IsSilverMember.HasValue || !userMst.IsSilverMember.Value)
                            {
                                double num7 = !source.Any<Program.UserMatchingBVDtl>((Func<Program.UserMatchingBVDtl, bool>)(i => i.UserId == item.UserId)) ? manager.GetMatchingBVValues(item.UserId, new DateTime?(), new DateTime?()) : source.FirstOrDefault<Program.UserMatchingBVDtl>((Func<Program.UserMatchingBVDtl, bool>)(i => i.UserId == item.UserId)).MatchingBV;
                                int[] listParentParentMemberDtl = trsEntities.PROC_GET_ALL_PARENT_USER_TREE(new int?(item.UserId), new bool?(false)).Select<PROC_GET_ALL_PARENT_USER_TREE_Result, int>((Func<PROC_GET_ALL_PARENT_USER_TREE_Result, int>)(i => i.UserId.Value)).ToArray<int>();
                                if (num7 >= num3)
                                {
                                    if (trsEntities.UserMsts.Any<UserMst>((Expression<Func<UserMst, bool>>)(i => listParentParentMemberDtl.Contains<int>(i.UserId) && i.IsDimondMember == (bool?)true)))
                                    {
                                        userMst.IsPrimeMember = new bool?(false);
                                        userMst.IsGoldMember = new bool?(false);
                                        userMst.IsDimondMember = new bool?(false);
                                        userMst.IsSilverMember = new bool?(true);

                                        var dt = trsEntities.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(userMst.UserId, (decimal?)num3).FirstOrDefault();
                                        userMst.SilverMemberDate = dt;
                                        trsEntities.SaveChanges();
                                    }
                                }
                            }
                            Logger.Info("IsSilverMember Status Updated End For UserNo : " + (object)num6 + " ,UserId : " + (object)item.UserId + " ,UserName : " + item.FullName);
                            ++num6;
                        }
                        Logger.Info("IsSilverMember Update Status Start");
                    }
                    stopwatch1.Stop();
                    TimeSpan elapsed = stopwatch1.Elapsed;
                    Program.SendMail(ToMailId, "User Status Update Date :- " + str1, "Success, Total Users : " + (object)num1 + ", Total Time : " + Program.GetCalculationTime(elapsed));
                }
            }
            catch (Exception ex)
            {
                stopwatch1.Stop();
                TimeSpan elapsed = stopwatch1.Elapsed;
                string str3 = ex.Message;
                if (ex.InnerException != null)
                    str3 = str3 + "Inner Exception : " + ex.InnerException.Message;

                Logger.Error("Income Calculate Date: -" + str1, ex);
                Program.SendMail(ToMailId, "Income Calculate Date :- " + str1, "Fail : " + str3 + ", Total Users : " + (object)num1 + ", Total Time : " + Program.GetCalculationTime(elapsed));
                Console.ReadLine();
            }
        }

        public static List<int> GetUserList()
        {
            using (TRSEntities trsEntities = new TRSEntities(3600))
                return trsEntities.UserMsts.OrderBy<UserMst, int>((Expression<Func<UserMst, int>>)(i => i.UserId)).Select<UserMst, int>((Expression<Func<UserMst, int>>)(i => i.UserId)).ToList<int>();
        }

        private static string GetCalculationTime(TimeSpan ts)
        {
            return string.Format("{0:00}:{1:00}:{2:00}.{3:00}", (object)ts.Hours, (object)ts.Minutes, (object)ts.Seconds, (object)(ts.Milliseconds / 10));
        }

        public static bool SendMail(string ToMailId, string Subject, string Body)
        {
            string host = ConfigurationManager.AppSettings["HostName"].ToString();
            string str1 = ConfigurationManager.AppSettings["Port"].ToString();
            string str2 = ConfigurationManager.AppSettings["EmailFrom"].ToString();
            string password = ConfigurationManager.AppSettings["Password"].ToString();
            string str3 = ConfigurationManager.AppSettings["EnabledSSL"].ToString();
            bool flag = false;
            int int32 = Convert.ToInt32(str1);
            SmtpClient smtpClient = new SmtpClient(host, int32)
            {
                Credentials = (ICredentialsByHost)new NetworkCredential(str2, password),
                EnableSsl = Convert.ToBoolean(str3)
            };
            string str4 = ToMailId;
            char[] chArray = new char[2] { ';', ',' };
            foreach (string recipients in str4.Split(chArray))
                smtpClient.Send(str2, recipients, Subject, Body);
            return flag;
        }

        public class UserMatchingBVDtl
        {
            public int UserId { get; set; }

            public double MatchingBV { get; set; }
        }
    }
}