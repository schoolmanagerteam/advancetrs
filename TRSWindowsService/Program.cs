﻿using log4net;
using Newtonsoft.Json;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using TRSImplementation;

namespace MyFirstService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            log4net.Config.XmlConfigurator.Configure();

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            ServiceBase.Run(ServicesToRun);

            //Service1 obj = new Service1();
            //obj.OrderFailedverifyProcess();
            //obj.UpdateAllUserStatusActive();
            //obj.UpdateOrderUserStatusActive();
            //obj.PointsCreditProcessActive();

            #region Sample Code for Razorpay.Api

            //Manager objManager = new Manager();
            //var KeyDetails = objManager.GetPaymentKeyDetails();
            //RazorpayClient client = new RazorpayClient(KeyDetails.PaymentKey, KeyDetails.PaymentSecret);

            //Order RazorOrder1 = client.Order.Fetch("order_G1KKTyd8uA3EtS");
            //List<Payment> lstPayment = RazorOrder1.Payments();

            //foreach (Payment payment in lstPayment)
            //{
            //    bool capturedStatus = (bool)payment["captured"];
            //    if (capturedStatus)
            //    {   
            //        break;
            //    }
            //}

            //List<Payment> payments = client.Orders.fetchPayments("order_FyyaVfZ3pNbzGV");

            //RazorpayClient.orders

            //client.orders.fetchPayments("order_FyyaVfZ3pNbzGV")

            //using (var httpClient = new HttpClient())
            //{
            //    using (var request = new HttpRequestMessage(new System.Net.Http.HttpMethod("GET"), "https://api.razorpay.com/v1/orders/order_DovFx48wjYEr2I/payments"))
            //    {
            //        var base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}-X", KeyDetails.PaymentKey, KeyDetails.PaymentSecret)));
            //        request.Headers.TryAddWithoutValidation("Authorization", $"Basic {base64authorization}");

            //        var response = await httpClient.GetAsync("https://api.razorpay.com/v1/orders/order_DovFx48wjYEr2I/payments");
            //        var result = response.Content.ReadAsStringAsync();
            //    }
            //}

            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("https://api.razorpay.com/v1/orders/{0}/payments", "order_FyyaVfZ3pNbzGV"));
            //request.Method = new string 
            //request.ContentLength = 0;
            //request.ContentType = "application/json";

            //string userAgent = string.Format("{0} {1}", RazorpayClient.Version, getAppDetailsUa());
            //request.UserAgent = "razorpay-dot-net/" + userAgent;

            //string authString = string.Format("{0}:{1}", RazorpayClient.Key, RazorpayClient.Secret);
            //request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(
            //    Encoding.UTF8.GetBytes(authString));

            //foreach (KeyValuePair<string, string> header in RazorpayClient.Headers)
            //{
            //    request.Headers[header.Key] = header.Value;
            //}

            //using (HttpClient client1 = new HttpClient())
            //{
            //    //client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("key", KeyDetails.PaymentKey);
            //    //client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("secret", KeyDetails.PaymentSecret);

            //    //var content = new FormUrlEncodedContent(new[]
            //    //{
            //    //    new KeyValuePair<string, string>("key",KeyDetails.PaymentKey),
            //    //    new KeyValuePair<string, string>("secret", KeyDetails.PaymentSecret),
            //    //});
            //    //string authorisation = string.Format("key:{0},secret:{1}", KeyDetails.PaymentKey, KeyDetails.PaymentSecret);
            //    //client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("basic", authorisation);

            //    //client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

            //    //var base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}-X", KeyDetails.PaymentKey, KeyDetails.PaymentSecret)));
            //    var base64authorization = string.Format("{0}:{1}-X", KeyDetails.PaymentKey, KeyDetails.PaymentSecret);

            //    client1.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", $"Basic {base64authorization}");
            //    //client1.DefaultRequestHeaders.Add("key", KeyDetails.PaymentKey);
            //    //client1.DefaultRequestHeaders.Add("secret", KeyDetails.PaymentSecret);

            //    string requestUrl = string.Format("https://api.razorpay.com/v1/orders/{0}/payments", "order_FyyaVfZ3pNbzGV");
            //    var response = client1.GetAsync(requestUrl);

            //    if (response.Result.StatusCode == System.Net.HttpStatusCode.OK)
            //    {
            //        var result = response.Result.Content.ReadAsStringAsync();
            //    }
            //    else
            //    {
            //        var result = response.Result.Content.ReadAsStringAsync();
            //        Console.WriteLine(result.Result);
            //    }
            //}

            //Razorpay.Api.Payment payment1 = client.Payment.Fetch("pay_FxASFhUfiPUPfM");

            //bool isFound = false;
            //int SkipInt = 0;

            //while (!isFound)
            //{
            //    Dictionary<string, object> options = new Dictionary<string, object>();
            //    options.Add("from", "1604417497"); //Utils.ToUnixTimestamp(startTime)
            //    options.Add("to", "1604590297");//Utils.ToUnixTimestamp(endTime)//"1604511000"

            //    DateTime endTime = DateTime.UtcNow;
            //    DateTime startTime = endTime.Add(TimeSpan.FromHours(-48));
            //    options.Add("from", Utils.ToUnixTimestamp(startTime));
            //    options.Add("to", Utils.ToUnixTimestamp(endTime));
            //    options.Add("count", "100");

            //    if (SkipInt > 0)
            //        options.Add("skip", SkipInt);

            //    List<Payment> result = client.Payment.All(options);
            //    if (!result.Any())
            //        break;
            //    foreach (Payment Ordpayment in result)
            //    {
            //        string orderId = (string)(Ordpayment.Attributes["order_id"]).Value;
            //        string paymentId = (string)(Ordpayment.Attributes["id"]).Value;
            //        if (orderId == "order_FxOYS4tfSINmbG")
            //        {
            //            Razorpay.Api.Payment payment = client.Payment.Fetch(paymentId);
            //            isFound = true;
            //        }
            //    }
            //    SkipInt++;
            //}

            #endregion

        }
    }
}
