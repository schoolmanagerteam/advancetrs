﻿using log4net;
using Newtonsoft.Json;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Web.Configuration;
using TRSImplementation;
using static TRSImplementation.clsImplementationEnum;

namespace MyFirstService
{
    /// <summary>
    /// Service1
    /// </summary>
    public partial class Service1 : ServiceBase
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Environment.MachineName);
        private Manager objManager = new Manager();

        private Timer pointsCredit_Timer = new Timer();
        private bool isPointsCreditTimerProcessing = false;

        private Timer updateAllUserStatus_Timer = new Timer();
        private bool isUpdateAllUserStatusTimerProcessing = false;

        private Timer updateOrderUserStatus_Timer = new Timer();
        private bool isUpdateOrderUserStatusTimerProcessing = false;

        private Timer OrderFailedVerify_Timer = new Timer();
        private bool isOrderFailedverifyTimerProcessing = false;

        private Timer GenerateIncomeProcess_Timer = new Timer();
        private bool isGenerateIncomeProcessTimerProcessing = false;

        private Timer FastageMilesCredit_Timer = new Timer();
        private bool isFastageMilesCreditTimerProcessing = false;

        private Timer UserMilesLapsed_Timer = new Timer();
        private bool isUserMilesLapsedTimerProcessing = false;

        private TRSEntities db = new TRSEntities();

        public Service1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// OnStart
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            Logger.Info("Service is started");

            int pointsCreditTimerInterval = 60;
            if (!int.TryParse(ConfigurationManager.AppSettings["pointsCreditTimer_Interval"], out pointsCreditTimerInterval))
                pointsCreditTimerInterval = 60;

            int updateAllUserStatusTimerInterval = 1440;
            if (!int.TryParse(ConfigurationManager.AppSettings["updateAllUserStatusTimer_Interval"], out updateAllUserStatusTimerInterval))
                updateAllUserStatusTimerInterval = 1440;

            int updateOrderUserStatusTimerInterval = 180;
            if (!int.TryParse(ConfigurationManager.AppSettings["updateOrderUserStatusTimer_Interval"], out updateOrderUserStatusTimerInterval))
                updateOrderUserStatusTimerInterval = 180;

            int OrderFailedverifyTimerInterval = 180;
            if (!int.TryParse(ConfigurationManager.AppSettings["OrderFailedverifyTimer_Interval"], out OrderFailedverifyTimerInterval))
                OrderFailedverifyTimerInterval = 180;

            int GenerateIncomeProcessTimerInterval = 180;
            if (!int.TryParse(ConfigurationManager.AppSettings["GenerateIncomeProcessTimer_Interval"], out GenerateIncomeProcessTimerInterval))
                GenerateIncomeProcessTimerInterval = 180;

            int FastageMilesCreditTimerInterval = 120;
            if (!int.TryParse(ConfigurationManager.AppSettings["FastageMilesCreditTimer_Interval"], out FastageMilesCreditTimerInterval))
                FastageMilesCreditTimerInterval = 120;

            int UserMilesLapsedTimerInterval = 120;
            if (!int.TryParse(ConfigurationManager.AppSettings["UserMilesLapsedTimer_Interval"], out UserMilesLapsedTimerInterval))
                UserMilesLapsedTimerInterval = 120;

            pointsCredit_Timer.Elapsed += new ElapsedEventHandler(pointsCreditTimer_OnElapsedTime);
            pointsCredit_Timer.Interval = 1000 * 60 * pointsCreditTimerInterval;
            pointsCredit_Timer.Enabled = true;

            updateOrderUserStatus_Timer.Elapsed += new ElapsedEventHandler(updateOrderUserStatusTimer_OnElapsedTime);
            updateOrderUserStatus_Timer.Interval = 1000 * 60 * updateOrderUserStatusTimerInterval;
            updateOrderUserStatus_Timer.Enabled = true;

            //updateAllUserStatus_Timer.Elapsed += new ElapsedEventHandler(updateAllUserStatusTimer_OnElapsedTime);
            //updateAllUserStatus_Timer.Interval = 1000 * 60 * updateAllUserStatusTimerInterval;
            //updateAllUserStatus_Timer.Enabled = true;

            OrderFailedVerify_Timer.Elapsed += new ElapsedEventHandler(OrderFailedverifyTimer_OnElapsedTime);
            OrderFailedVerify_Timer.Interval = 1000 * 60 * OrderFailedverifyTimerInterval;
            OrderFailedVerify_Timer.Enabled = true;

            GenerateIncomeProcess_Timer.Elapsed += new ElapsedEventHandler(GenerateIncomeProcessTimer_OnElapsedTime);
            GenerateIncomeProcess_Timer.Interval = 1000 * 60 * GenerateIncomeProcessTimerInterval;
            GenerateIncomeProcess_Timer.Enabled = true;

            FastageMilesCredit_Timer.Elapsed += new ElapsedEventHandler(FastageMilesCreditTimer_OnElapsedTime);
            FastageMilesCredit_Timer.Interval = 1000 * 60 * FastageMilesCreditTimerInterval;
            FastageMilesCredit_Timer.Enabled = true;

            UserMilesLapsed_Timer.Elapsed += new ElapsedEventHandler(UserMilesLapsedTimer_OnElapsedTime);
            UserMilesLapsed_Timer.Interval = 1000 * 60 * UserMilesLapsedTimerInterval;
            UserMilesLapsed_Timer.Enabled = true;

        }

        protected override void OnStop()
        {
            isUserMilesLapsedTimerProcessing = isFastageMilesCreditTimerProcessing = isGenerateIncomeProcessTimerProcessing = isUpdateOrderUserStatusTimerProcessing = isUpdateAllUserStatusTimerProcessing = isPointsCreditTimerProcessing = false;
            TimerDispose(pointsCredit_Timer);
            TimerDispose(updateAllUserStatus_Timer);
            TimerDispose(updateOrderUserStatus_Timer);
            TimerDispose(OrderFailedVerify_Timer);
            TimerDispose(GenerateIncomeProcess_Timer);
            TimerDispose(FastageMilesCredit_Timer);
            TimerDispose(UserMilesLapsed_Timer);
            Logger.Info("Service is stopped");
        }

        protected void TimerDispose(Timer _timer)
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
        }



        #region Update Order User Status Timer

        /// <summary>
        /// updateOrderUserStatusTimer_OnElapsedTime
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void updateOrderUserStatusTimer_OnElapsedTime(object source, ElapsedEventArgs e)
        {
            UpdateOrderUserStatusActive();
        }

        public void UpdateOrderUserStatusActive()
        {
            if (db.ServiceRunningStatus.Any(p => p.ID == (int)clsImplementationEnum.ServiceName.UpdateOrderUserStatusProcess && !p.IsActive))
            {
                Logger.Info("Service: UpdateOrderUserStatusProcess Inactive");
                return;
            }

            using (var context = new TRSEntities(3600))
            {
                var objServiceRunningStatus = context.ServiceRunningStatus.Where(p => p.ID == (int)clsImplementationEnum.ServiceName.UpdateOrderUserStatusProcess).FirstOrDefault();
                objServiceRunningStatus.RunningStatus = context.PROC_GET_SERVER_DATE().FirstOrDefault();
                context.SaveChanges();
            }

            if (isUpdateOrderUserStatusTimerProcessing)
            {
                Logger.Info("Privious updateOrderUserStatus_Timer in processing...");
                return;
            }

            Logger.Info("Start: updateOrderUserStatusTimer_OnElapsedTime");
            try
            {
                UpdateOrderUserStatus();
            }
            catch (Exception ex)
            {
                Logger.Error("Error Exception in updateOrderUserStatus Service.", ex);
            }
            finally
            {
                Logger.Info("End: updateOrderUserStatusTimer_OnElapsedTime");
            }
        }

        /// <summary>
        /// UpdateOrderUserStatus
        /// </summary>
        public void UpdateOrderUserStatus()
        {
            isUpdateOrderUserStatusTimerProcessing = true;
            Manager objManager = new Manager();
            try
            {
                Logger.Info("Calling UpdateOrderUserStatus");
                var dt_CurrentDatetime = db.PROC_GET_SERVER_DATE().FirstOrDefault();
                List<PROC_GET_USER_UPDATE_ALLUSERSTATUS_Result> Users = db.PROC_GET_USER_UPDATE_ALLUSERSTATUS().ToList();
                Logger.Info(string.Format("Func:UpdateOrderUserStatus. Total Users:{0}", Users.Count));
                using (var context = new TRSEntities(3600))
                {
                    foreach (PROC_GET_USER_UPDATE_ALLUSERSTATUS_Result UsersData in Users)
                    {
                        try
                        {
                            var listParentMemberDtl = context.UserMsts.Where(i => i.UserId == UsersData.OrderUserId).Select(i => new ParentMemberDtl { FullName = i.FullName, UserId = i.UserId }).ToList();

                            List<string> listConfigKeys = new List<string>();
                            listConfigKeys.Add(ConfigKeys.GoldIncomeBV);
                            listConfigKeys.Add(ConfigKeys.SilverIncomeBV);
                            listConfigKeys.Add(ConfigKeys.PrimeIncomeBV);
                            listConfigKeys.Add(ConfigKeys.DiamondIncomeBV);

                            var listConfig = objManager.GetConfigValues(listConfigKeys);
                            double GoldIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.GoldIncomeBV));
                            double SilverIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.SilverIncomeBV));
                            double PrimeIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.PrimeIncomeBV));
                            double DiamondIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.DiamondIncomeBV));

                            List<UserMatchingBVDtl> listUserMatchingBVDtl = new List<UserMatchingBVDtl>();
                            foreach (var item in listParentMemberDtl)
                            {
                                var MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });

                                var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                                if (!objParentUserMst.IsGoldMember.HasValue || !objParentUserMst.IsGoldMember.Value)
                                {
                                    if (MatchingBV >= GoldIncomeBV)
                                    {
                                        objParentUserMst.IsDimondMember = objParentUserMst.IsSilverMember = objParentUserMst.IsPrimeMember = false;
                                        objParentUserMst.IsGoldMember = true;
                                        var dt = context.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)GoldIncomeBV).FirstOrDefault();
                                        objParentUserMst.GoldMemberDate = dt;
                                    }
                                }
                                if (!objParentUserMst.IsDimondMember.HasValue || !objParentUserMst.IsDimondMember.Value)
                                {
                                    if (MatchingBV >= DiamondIncomeBV)
                                    {
                                        objParentUserMst.IsPrimeMember = false;
                                        objParentUserMst.IsSilverMember = false;
                                        objParentUserMst.IsGoldMember = false;
                                        objParentUserMst.IsDimondMember = true;
                                        var dt = context.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)DiamondIncomeBV).FirstOrDefault();
                                        objParentUserMst.DimondMemberDate = dt;
                                    }
                                }
                            }

                            foreach (var item in listParentMemberDtl)
                            {
                                var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                                if (!objParentUserMst.IsPrimeMember.HasValue || !objParentUserMst.IsPrimeMember.Value)
                                {
                                    double MatchingBV = 0;
                                    if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                                    {
                                        MatchingBV = listUserMatchingBVDtl.Where(i => i.UserId == item.UserId).FirstOrDefault().MatchingBV;
                                    }
                                    else
                                    {
                                        MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                        listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });
                                    }
                                    var listParentParentMemberDtl = context.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).ToList().Select(i => i.UserId.Value).ToArray();

                                    if (MatchingBV >= PrimeIncomeBV
                                        && context.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsGoldMember == true))
                                    {
                                        objParentUserMst.IsSilverMember = false;
                                        objParentUserMst.IsGoldMember = false;
                                        objParentUserMst.IsDimondMember = false;
                                        objParentUserMst.IsPrimeMember = true;

                                        var dt = context.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)PrimeIncomeBV).FirstOrDefault();

                                        objParentUserMst.PrimeMemberDate = dt;
                                    }
                                }

                                if (!objParentUserMst.IsSilverMember.HasValue || !objParentUserMst.IsSilverMember.Value)
                                {
                                    double MatchingBV = 0;
                                    if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                                    {
                                        MatchingBV = listUserMatchingBVDtl.Where(i => i.UserId == item.UserId).FirstOrDefault().MatchingBV;
                                    }
                                    else
                                    {
                                        MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                    }
                                    var listParentParentMemberDtl = context.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).ToList().Select(i => i.UserId.Value).ToArray();

                                    if (MatchingBV >= SilverIncomeBV
                                        && context.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsDimondMember == true))
                                    {
                                        objParentUserMst.IsGoldMember = false;
                                        objParentUserMst.IsDimondMember = false;
                                        objParentUserMst.IsPrimeMember = false;
                                        objParentUserMst.IsSilverMember = true;
                                        var dt = context.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)SilverIncomeBV).FirstOrDefault();
                                        objParentUserMst.SilverMemberDate = dt;
                                    }
                                }
                            }

                            var UserStatusUpdateQueue = context.UserStatusUpdateQueues.Where(p => p.UserStatusUpdateQueueId == UsersData.UserStatusUpdateQueueId).FirstOrDefault();
                            UserStatusUpdateQueue.IsProcess = true;
                            UserStatusUpdateQueue.UpdatedBy = -1;
                            UserStatusUpdateQueue.UpdatedDate = dt_CurrentDatetime;

                            context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(string.Format("Func:UpdateOrderUserStatus. UserId:{0}", UsersData.OrderUserId), ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Func:UpdateOrderUserStatus.", ex);
            }
            finally
            {
                isUpdateOrderUserStatusTimerProcessing = false;
            }
        }

        #endregion Update Order User Status Timer

        #region Update All User Status Timer

        private void updateAllUserStatusTimer_OnElapsedTime(object source, ElapsedEventArgs e)
        {
            UpdateAllUserStatusActive();
        }

        public void UpdateAllUserStatusActive()
        {
            if (db.ServiceRunningStatus.Any(p => p.ID == (int)clsImplementationEnum.ServiceName.UpdateAllUserStatusProcess && !p.IsActive))
            {
                Logger.Info("Service: UpdateAllUserStatusProcess Inactive");
                return;
            }

            using (var context = new TRSEntities(3600))
            {
                var objServiceRunningStatus = context.ServiceRunningStatus.Where(p => p.ID == (int)clsImplementationEnum.ServiceName.UpdateAllUserStatusProcess).FirstOrDefault();
                objServiceRunningStatus.RunningStatus = context.PROC_GET_SERVER_DATE().FirstOrDefault();
                context.SaveChanges();
            }

            if (isUpdateAllUserStatusTimerProcessing)
            {
                Logger.Info("Privious updateAllUserStatus_Timer in processing...");
                return;
            }

            Logger.Info("Start: updateAllUserStatusTimer_OnElapsedTime");
            try
            {
                UpdateAllUserStatus();
            }
            catch (Exception ex)
            {
                Logger.Error("Error Exception in updateAllUserStatusTimer Service.", ex);
            }
            finally
            {
                Logger.Info("End: updateAllUserStatusTimer_OnElapsedTime");
            }
        }

        public void UpdateAllUserStatus()
        {
            Logger.Info("Calling UpdateAllUserStatus");
            isUpdateAllUserStatusTimerProcessing = true;
            try
            {
                using (var context = new TRSEntities(3600))
                {
                    Manager obj = new Manager();
                    var objManager = obj.objManager;
                    var MainUser = context.UserMsts.Where(i => i.RefferalUserId == 0).OrderBy(i => i.UserId).FirstOrDefault();
                    var listParentMemberDtl = context.FN_GET_ALL_CHILD_USER_TREE(MainUser.UserId, true).Select(i => new ParentMemberDtl { FullName = i.FullName, UserId = i.UserId.Value }).ToList();

                    List<string> listConfigKeys = new List<string>();
                    listConfigKeys.Add(ConfigKeys.GoldIncomeBV);
                    listConfigKeys.Add(ConfigKeys.SilverIncomeBV);
                    listConfigKeys.Add(ConfigKeys.PrimeIncomeBV);
                    listConfigKeys.Add(ConfigKeys.DiamondIncomeBV);

                    var listConfig = objManager.GetConfigValues(listConfigKeys);
                    double GoldIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.GoldIncomeBV));
                    double SilverIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.SilverIncomeBV));
                    double PrimeIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.PrimeIncomeBV));
                    double DiamondIncomeBV = Convert.ToDouble(objManager.GetConfigValueFromConfigList(listConfig, ConfigKeys.DiamondIncomeBV));

                    Logger.Info(string.Format("UpdateAllUserStatus: {0} Inprocess..", "GoldIncomeBV"));

                    List<UserMatchingBVDtl> listUserMatchingBVDtl = new List<UserMatchingBVDtl>();
                    foreach (var item in listParentMemberDtl)
                    {
                        var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                        if ((!objParentUserMst.IsGoldMember.HasValue || !objParentUserMst.IsGoldMember.Value))
                        {
                            var MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                            listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });

                            if (MatchingBV >= GoldIncomeBV)
                            {
                                objParentUserMst.IsDimondMember = false;
                                objParentUserMst.IsPrimeMember = false;
                                objParentUserMst.IsSilverMember = false;
                                objParentUserMst.IsGoldMember = true;

                                var dt = context.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)GoldIncomeBV).FirstOrDefault();
                                objParentUserMst.GoldMemberDate = dt;
                                context.SaveChanges();
                            }
                        }
                    }

                    Logger.Info(string.Format("UpdateAllUserStatus: {0} Inprocess..", "DiamondIncomeBV"));

                    foreach (var item in listParentMemberDtl)
                    {
                        var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                        if ((!objParentUserMst.IsDimondMember.HasValue || !objParentUserMst.IsDimondMember.Value))
                        {
                            double MatchingBV = 0;
                            if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                            {
                                MatchingBV = listUserMatchingBVDtl.FirstOrDefault(i => i.UserId == item.UserId).MatchingBV;
                            }
                            else
                            {
                                MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });
                            }
                            if (MatchingBV >= DiamondIncomeBV)
                            {
                                objParentUserMst.IsGoldMember = false;
                                objParentUserMst.IsPrimeMember = false;
                                objParentUserMst.IsSilverMember = false;
                                objParentUserMst.IsDimondMember = true;

                                var dt = context.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)DiamondIncomeBV).FirstOrDefault();

                                objParentUserMst.DimondMemberDate = dt;
                                context.SaveChanges();
                            }
                        }
                    }

                    Logger.Info(string.Format("UpdateAllUserStatus: {0} Inprocess..", "PrimeIncomeBV"));

                    foreach (var item in listParentMemberDtl)
                    {
                        var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                        if (!objParentUserMst.IsPrimeMember.HasValue || !objParentUserMst.IsPrimeMember.Value)
                        {
                            double MatchingBV = 0;
                            if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                            {
                                MatchingBV = listUserMatchingBVDtl.FirstOrDefault(i => i.UserId == item.UserId).MatchingBV;
                            }
                            else
                            {
                                MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                                listUserMatchingBVDtl.Add(new UserMatchingBVDtl { UserId = item.UserId, MatchingBV = MatchingBV });
                            }
                            var listParentParentMemberDtl = context.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).Select(i => i.UserId.Value).ToArray();

                            if (MatchingBV >= PrimeIncomeBV
                                && context.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsGoldMember == true))
                            {
                                objParentUserMst.IsDimondMember = false;
                                objParentUserMst.IsSilverMember = false;
                                objParentUserMst.IsGoldMember = false;
                                objParentUserMst.IsPrimeMember = true;

                                var dt = context.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)PrimeIncomeBV).FirstOrDefault();

                                objParentUserMst.PrimeMemberDate = dt;
                                context.SaveChanges();
                            }
                        }
                    }

                    Logger.Info(string.Format("UpdateAllUserStatus: {0} Inprocess..", "SilverIncomeBV"));

                    foreach (var item in listParentMemberDtl)
                    {
                        var objParentUserMst = context.UserMsts.Where(i => i.UserId == item.UserId).FirstOrDefault();
                        if (!objParentUserMst.IsSilverMember.HasValue || !objParentUserMst.IsSilverMember.Value)
                        {
                            double MatchingBV = 0;
                            if (listUserMatchingBVDtl.Any(i => i.UserId == item.UserId))
                            {
                                MatchingBV = listUserMatchingBVDtl.FirstOrDefault(i => i.UserId == item.UserId).MatchingBV;
                            }
                            else
                            {
                                MatchingBV = objManager.GetMatchingBVValues(item.UserId, null, null);
                            }
                            var listParentParentMemberDtl = context.PROC_GET_ALL_PARENT_USER_TREE(item.UserId, false).Select(i => i.UserId.Value).ToArray();

                            if (MatchingBV >= SilverIncomeBV
                                && context.UserMsts.Any(i => listParentParentMemberDtl.Contains(i.UserId) && i.IsDimondMember == true))
                            {
                                objParentUserMst.IsDimondMember = false;
                                objParentUserMst.IsGoldMember = false;
                                objParentUserMst.IsPrimeMember = false;
                                objParentUserMst.IsSilverMember = true;

                                var dt = context.PROC_GET_DATE_FOR_REACHED_MATCHING_BV_LIMIT(objParentUserMst.UserId, (decimal?)SilverIncomeBV).FirstOrDefault();

                                objParentUserMst.SilverMemberDate = dt;
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Func:Update_All_UserStatus.", ex);
            }
            finally
            {
                isUpdateAllUserStatusTimerProcessing = false;
            }
        }

        #endregion Update All User Status Timer

        #region Points Credit Timer

        /// <summary>
        /// pointsCreditTimer_OnElapsedTime
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void pointsCreditTimer_OnElapsedTime(object source, ElapsedEventArgs e)
        {
            PointsCreditProcessActive();
        }

        public void PointsCreditProcessActive()
        {
            if (db.ServiceRunningStatus.Any(p => p.ID == (int)clsImplementationEnum.ServiceName.PointsCreditProcess && !p.IsActive))
            {
                Logger.Info("Service: UpdateAllUserStatusProcess Inactive");
                return;
            }

            using (var context = new TRSEntities(3600))
            {
                var objServiceRunningStatus = context.ServiceRunningStatus.Where(p => p.ID == (int)clsImplementationEnum.ServiceName.PointsCreditProcess).FirstOrDefault();
                objServiceRunningStatus.RunningStatus = context.PROC_GET_SERVER_DATE().FirstOrDefault();
                context.SaveChanges();
            }

            if (isPointsCreditTimerProcessing)
            {
                Logger.Info("Privious PointsCredit_Timer in processing...");
                return;
            }

            Logger.Info("Start: pointsCreditTimer_OnElapsedTime");
            try
            {
                PointsCreditProcess();
            }
            catch (Exception ex)
            {
                Logger.Error("Error Exception in pointsCreditTimer Service.", ex);
            }
            finally
            {
                Logger.Info("End: pointsCreditTimer_OnElapsedTime");
            }
        }

        /// <summary>
        /// PointsCreditProcess
        /// </summary>
        public void PointsCreditProcess()
        {
            isPointsCreditTimerProcessing = true;
            try
            {
                Logger.Info("Calling PointsCreditProcess");
                using (var context = new TRSEntities(3600))
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        context.PROC_INSERT_ALL_PARENT_USER_POINTSCREDITDTL_BYSERVICE();
                        context.SaveChanges();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Func:PointsCreditProcess.", ex);
            }
            finally
            {
                isPointsCreditTimerProcessing = false;
            }
        }

        #endregion Points Credit Timer

        #region Order Failed verify Timer

        private void OrderFailedverifyTimer_OnElapsedTime(object source, ElapsedEventArgs e)
        {
            OrderFailedverifyProcessActive();
        }

        public void OrderFailedverifyProcessActive()
        {
            if (db.ServiceRunningStatus.Any(p => p.ID == (int)clsImplementationEnum.ServiceName.OrderFailedverifyProcess && !p.IsActive))
            {
                Logger.Info("Service: OrderFailedverifyProcess Inactive");
                return;
            }

            using (var context = new TRSEntities(3600))
            {
                var objServiceRunningStatus = context.ServiceRunningStatus.Where(p => p.ID == (int)clsImplementationEnum.ServiceName.OrderFailedverifyProcess).FirstOrDefault();
                objServiceRunningStatus.RunningStatus = context.PROC_GET_SERVER_DATE().FirstOrDefault();
                context.SaveChanges();
            }

            if (isOrderFailedverifyTimerProcessing)
            {
                Logger.Info("Privious OrderFailedVerify_Timer in processing...");
                return;
            }

            Logger.Info("Start: OrderFailedverifyTimer_OnElapsedTime");
            try
            {
                OrderFailedverifyProcess();
            }
            catch (Exception ex)
            {
                Logger.Error("Error Exception in OrderFailedverifyTimer Service.", ex);
            }
            finally
            {
                Logger.Info("End: OrderFailedverifyTimer_OnElapsedTime");
            }
        }

        public void OrderFailedverifyProcess()
        {
            isOrderFailedverifyTimerProcessing = true;
            try
            {
                Logger.Info("Calling OrderFailedverifyProcess");

                List<PROC_GET_ORDER_FAILED_VERIFY_QUEUE_Result> ORderFailures = db.PROC_GET_ORDER_FAILED_VERIFY_QUEUE().ToList();
                if (ORderFailures.Any())
                {
                    Logger.Info("ORderFailures Has data");
                    var KeyDetails = objManager.GetPaymentKeyDetails();
                    RazorpayClient client = new RazorpayClient(KeyDetails.PaymentKey, KeyDetails.PaymentSecret);
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                    Logger.Info("Get RazorpayClient");

                    using (var context = new TRSEntities(3600))
                    {
                        foreach (PROC_GET_ORDER_FAILED_VERIFY_QUEUE_Result order in ORderFailures)
                        {
                            Logger.Info(string.Format("Process OrderFailedverifyProcess for RazorpayOrderId: {0}", order.RazorpayOrderId));

                            using (DbContextTransaction transaction = context.Database.BeginTransaction())
                            {
                                UserPurchaseHdrTemp objUserPurchaseHdrTemp = context.UserPurchaseHdrTemps.Where(p => p.UserPurchaseHdrId == order.UserPurchaseHdrId).FirstOrDefault();
                                bool isPaymentRecordFound = false;
                                string reasonNote = string.Empty;
                                try
                                {
                                    Order razorpayOrder = client.Order.Fetch(objUserPurchaseHdrTemp.RazorpayOrderId);
                                    if (razorpayOrder != null)
                                    {
                                        List<Payment> razorOrderPayments = razorpayOrder.Payments();
                                        if (razorOrderPayments != null && razorOrderPayments.Any())
                                        {
                                            foreach (Payment payment in razorOrderPayments)
                                            {
                                                objUserPurchaseHdrTemp.RazorpayPaymentResponse = JsonConvert.SerializeObject(payment);

                                                bool capturedStatus = (bool)payment["captured"];
                                                if (capturedStatus)
                                                {
                                                    string orderId = (string)(payment.Attributes["order_id"]).Value;
                                                    string paymentId = (string)(payment.Attributes["id"]).Value;

                                                    if (db.UserPurchaseHdrs.Any(p => p.RazorpayOrderId == objUserPurchaseHdrTemp.RazorpayOrderId))
                                                    {
                                                        reasonNote = "Order is already exists with same order id";
                                                        objUserPurchaseHdrTemp.IsProcess = true;
                                                    }
                                                    else if (PlaceOrder(objUserPurchaseHdrTemp, orderId, paymentId, (bool)objUserPurchaseHdrTemp.IsCODOrder, objUserPurchaseHdrTemp.DeliveryAddress, ""))
                                                    {
                                                        objUserPurchaseHdrTemp.IsSuccess = objUserPurchaseHdrTemp.IsProcess = true;
                                                        reasonNote = "Order is placed successfully (With verified razorpay payment).";
                                                    }
                                                    else
                                                    {
                                                        reasonNote = "Error occurred during while placing order";
                                                        objUserPurchaseHdrTemp.IsProcess = true;
                                                    }
                                                    isPaymentRecordFound = true;
                                                    break;
                                                }
                                            }

                                            if (!isPaymentRecordFound)
                                                reasonNote = "Razorpay payment record is not found with captured {true} status.";
                                        }
                                        else
                                            reasonNote = "Razorpay payment record is not found for this order.";
                                    }
                                    else
                                        reasonNote = "Razorpay order record is not found for this order.";

                                    context.SaveChanges();
                                    transaction.Commit();
                                }
                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    Logger.Error(string.Format("Func:OrderFailedverifyProcess(Loop). RazorpayOrderId: {0}", order.RazorpayOrderId), ex);
                                    reasonNote = ex.Message;
                                }
                                finally
                                {
                                    objUserPurchaseHdrTemp.Notes = reasonNote;

                                    if (objUserPurchaseHdrTemp.RetryCount is null)
                                        objUserPurchaseHdrTemp.RetryCount = 0;
                                    objUserPurchaseHdrTemp.RetryCount += 1;
                                    objUserPurchaseHdrTemp.ProcessDateTime = context.PROC_GET_SERVER_DATE().FirstOrDefault();

                                    if (objUserPurchaseHdrTemp.RetryCount == 4)
                                        objUserPurchaseHdrTemp.IsProcess = true;

                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                }
                else
                    Logger.Info("No record found for OrderFailedverifyProcess");
            }
            catch (Exception ex)
            {
                Logger.Error("Func:OrderFailedverifyProcess.", ex);
            }
            finally
            {
                isOrderFailedverifyTimerProcessing = false;
            }
        }

        protected bool PlaceOrder(UserPurchaseHdrTemp objUserPurchaseHdrTemp, string order_id, string payment_id, bool IsCOD, string DeliveryAddress, string OrderNote)
        {
            Logger.Info("Calling PlaceOrder");
            bool result = false;
            try
            {
                var KeyDetails = objManager.GetPaymentKeyDetails();
                RazorpayClient client = new RazorpayClient(KeyDetails.PaymentKey, KeyDetails.PaymentSecret);

                Order order = client.Order.Fetch(order_id);
                var pinfo = order["notes"].pinfo;
                var tempid = order["notes"].tempid;
                var amount = order["notes"].amount;
                var milesamt = order["notes"].milesamt;
                var OrdDelChrg = order["notes"].OrdDelChrg;

                double MilesAmount = milesamt != null && !string.IsNullOrEmpty(Convert.ToString(milesamt)) ? Convert.ToDouble(milesamt) : 0;
                double OrdDeliveryCharge = (!string.IsNullOrEmpty(Convert.ToString(OrdDelChrg)) ? Convert.ToDouble(OrdDelChrg) : 0);
                if (PlaceOrderProcess(objUserPurchaseHdrTemp, pinfo.ToString(), tempid.ToString(), order_id, payment_id, "", DeliveryAddress, IsCOD, MilesAmount, OrderNote, OrdDeliveryCharge))
                    result = true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Func:OrderFailedverifyProcess(PlaceOrder). RazorpayOrderId: {0}", objUserPurchaseHdrTemp.RazorpayOrderId), ex);
                return false;
            }
            return result;
        }

        public bool PlaceOrderProcess(UserPurchaseHdrTemp objUserPurchaseHdrTemp, string pinfo, string tempid, string OrderId, string PaymentId, string Signature, string DeliveryAddress, bool IsCOD, double MilesAmt, string OrderNote, double OrdDeliveryCharge)
        {
            Logger.Info("Calling PlaceOrderProcess");
            bool Status = false;
            Int64 UserPurchaseHdrId = 0;
            double CreditAmount = 0;
            decimal OrderReceivedById = 0;
            try
            {
                using (var context = new TRSEntities(3600))
                {
                    DateTime dt_CurrentDatetime = objUserPurchaseHdrTemp.CreatedDate;
                    var isPointsCreditdtlGenerateWhilePlaceOrder = false;
                    //context.Database.Log = Console.Write;
                    clsLogin objclsLogin = new clsLogin { UserId = objUserPurchaseHdrTemp.UserId, RedirectFrmAdmin = false };
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            List<string> listConfigs = new List<string>();
                            listConfigs.Add(ConfigKeys.AdminConfirmationRequired);
                            listConfigs.Add(ConfigKeys.MinRegistrationPoints);
                            listConfigs.Add(ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints);
                            listConfigs.Add(ConfigKeys.ORDERGENBYADMINSTOPSMS);
                            listConfigs.Add(ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER);

                            var listSMSDtl = db.ConfigMsts.Where(i => listConfigs.Contains(i.Key.ToUpper())).ToList();

                            var AdminConfirmationRequired = false;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AdminConfirmationRequired))
                            {
                                try
                                {
                                    AdminConfirmationRequired = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AdminConfirmationRequired).Value);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                            double MinRegistrationPoints = 0;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints))
                            {
                                try
                                {
                                    MinRegistrationPoints = Convert.ToDouble(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.MinRegistrationPoints).Value);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }

                            bool AllowToPurchaseBelowMinRegistrationPoints = false;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints))
                            {
                                try
                                {
                                    AllowToPurchaseBelowMinRegistrationPoints = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.AllowToPurchaseBelowMinRegistrationPoints).Value);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }

                            bool ORDER_GEN_BYADMIN_STOPSMS = true;
                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.ORDERGENBYADMINSTOPSMS))
                            {
                                try
                                {
                                    ORDER_GEN_BYADMIN_STOPSMS = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.ORDERGENBYADMINSTOPSMS).Value);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }

                            if (listSMSDtl.Any(i => i.Key.ToUpper() == ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER))
                            {
                                try
                                {
                                    isPointsCreditdtlGenerateWhilePlaceOrder = Convert.ToBoolean(listSMSDtl.FirstOrDefault(i => i.Key.ToUpper() == ConfigKeys.POINTSCREDITDTLGENERATEWHILEPLACEORDER).Value);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }

                            if (!string.IsNullOrEmpty(OrderId) && !string.IsNullOrEmpty(PaymentId) && !string.IsNullOrEmpty(Signature) && !IsCOD)
                            {
                                AdminConfirmationRequired = false;
                            }

                            if (!IsCOD)
                                AdminConfirmationRequired = false;

                            var OrderAlreadyExist = false;
                            if (!string.IsNullOrEmpty(OrderId) && db.UserPurchaseHdrs.Any(i => i.RazorpayOrderId == OrderId))
                            {
                                OrderAlreadyExist = true;
                            }
                            if (!OrderAlreadyExist)
                            {
                                var objOrderReceivedBy = db.OrderReceivedByMsts.FirstOrDefault(i => i.OrderReceivedBy.ToUpper() == ("Razor Pay").ToUpper());
                                OrderReceivedById = objOrderReceivedBy != null ? objOrderReceivedBy.OrderReceivedById : 0;
                                var objUserMst = context.UserMsts.Where(i => i.UserId == objclsLogin.UserId).FirstOrDefault();
                                var objReferralUser = context.UserMsts.Where(i => i.UserId == objUserMst.RefferalUserId).FirstOrDefault();

                                bool IsUserRegistrationActivated = (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value);

                                var TempProductWalletDebitDtlId = Convert.ToInt64(tempid);
                                var objTempProductWalletDebitDtl = context.TempProductWalletDebitDtls.Where(i => i.TempProductWalletDebitDtlId == TempProductWalletDebitDtlId).FirstOrDefault();

                                #region Purchase Orders

                                double TotalAmount = 0;
                                var SplitProductInfo = objManager.Decrypt(pinfo).Split(',');
                                var objUserPurchaseHdr = new UserPurchaseHdr();
                                var listUserPurchaseDtl = new List<UserPurchaseDtl>();

                                foreach (var products in SplitProductInfo)
                                {
                                    var productInfo = products.Split('_');
                                    var prodid = Convert.ToInt32(productInfo[0]);
                                    var qty = Convert.ToInt32(productInfo[1]);
                                    var pvid = 0;
                                    if (productInfo.Length > 2)
                                    {
                                        pvid = Convert.ToInt32(productInfo[2]);
                                    }
                                    var objProductMst = context.ProductMsts.Where(i => i.ProductId == prodid).FirstOrDefault();

                                    objProductMst.Stock = objProductMst.Stock.HasValue ? objProductMst.Stock.Value - qty : -qty;

                                    TotalAmount += (qty * objProductMst.SalePrice.Value);
                                    var objUserPurchaseDtl = new UserPurchaseDtl();
                                    objUserPurchaseDtl.Amount = (qty * objProductMst.SalePrice.Value);
                                    objUserPurchaseDtl.CreatedBy = objclsLogin.UserId;
                                    objUserPurchaseDtl.CreatedDate = dt_CurrentDatetime;
                                    objUserPurchaseDtl.IsActive = true;
                                    objUserPurchaseDtl.ProductId = prodid;
                                    objUserPurchaseDtl.ProductPrice = objProductMst.SalePrice.Value;
                                    objUserPurchaseDtl.RegularPrice = objProductMst.RegularPrice.Value;
                                    objUserPurchaseDtl.Qty = qty;
                                    objUserPurchaseDtl.ProductVariantId = pvid;

                                    #region Points

                                    if (!AdminConfirmationRequired)
                                    {
                                        if (OrderReceivedById > 0)
                                        {
                                            objUserPurchaseHdr.OrderReceivedById = Convert.ToInt32(OrderReceivedById);
                                        }
                                        if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                        {
                                            objManager.InsertUserPointsOnPurchase(context, objUserPurchaseDtl, objclsLogin.UserId, objUserMst.RefferalUserId, WalletCreditType.Direct, isPointsCreditdtlGenerateWhilePlaceOrder);
                                            objManager.CalculateWalletCreditAmountRegistration(objProductMst, qty, ref CreditAmount, WalletCreditType.Direct);
                                        }
                                        else
                                        {
                                            objManager.InsertUserPointsOnPurchase(context, objUserPurchaseDtl, objclsLogin.UserId, objUserMst.RefferalUserId, WalletCreditType.Repurchase, isPointsCreditdtlGenerateWhilePlaceOrder);
                                        }
                                    }

                                    #endregion Points

                                    objUserPurchaseHdr.UserPurchaseDtls.Add(objUserPurchaseDtl);
                                }

                                if (!AdminConfirmationRequired && isPointsCreditdtlGenerateWhilePlaceOrder)
                                    db.PROC_INSERT_ALL_PARENT_USER_NOTIFICATIONMST(objclsLogin.UserId, NotificationType.Information.GetHashCode(), true);

                                objUserPurchaseHdr.IsFailedOrderVerified = true;
                                objUserPurchaseHdr.IsCODOrder = IsCOD;
                                objUserPurchaseHdr.Notes = OrderNote;
                                objUserPurchaseHdr.IsOrderPlaceByAdmin = objclsLogin.RedirectFrmAdmin;
                                //objUserPurchaseHdr.OrderNo = Convert.ToInt64(Convert.ToInt64(objManager.GenerateOrderNo()));
                                objUserPurchaseHdr.CreatedBy = objclsLogin.UserId;
                                objUserPurchaseHdr.CreatedDate = dt_CurrentDatetime;
                                objUserPurchaseHdr.IsActive = true;
                                objUserPurchaseHdr.OrderDate = dt_CurrentDatetime;
                                objUserPurchaseHdr.TotalOrderAmount = (TotalAmount - MilesAmt) + OrdDeliveryCharge;
                                objUserPurchaseHdr.UserId = objclsLogin.UserId;
                                objUserPurchaseHdr.RazorpayOrderId = OrderId;
                                objUserPurchaseHdr.RazorpayPaymentId = PaymentId;
                                objUserPurchaseHdr.RazorpaySignature = Signature;

                                if (!string.IsNullOrEmpty(DeliveryAddress) && !string.IsNullOrWhiteSpace(DeliveryAddress))
                                {
                                    objUserPurchaseHdr.DeliveryAddress = DeliveryAddress;
                                }
                                if (AdminConfirmationRequired)
                                {
                                    objUserPurchaseHdr.OrderStatusId = OrderStatus.OrderGenerate.GetHashCode();
                                }
                                else
                                {
                                    objUserPurchaseHdr.OrderStatusId = OrderStatus.OrderConfirm.GetHashCode();
                                }
                                if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                {
                                    objUserPurchaseHdr.IsRepurchaseOrder = false;
                                }
                                else
                                {
                                    objUserPurchaseHdr.IsRepurchaseOrder = true;
                                }
                                if (MilesAmt > 0)
                                {
                                    var objMilesDebitDtl = new MilesDebitDtl();
                                    objMilesDebitDtl.CreatedBy = objUserPurchaseHdr.CreatedBy;
                                    objMilesDebitDtl.CreatedDate = objUserPurchaseHdr.CreatedDate;
                                    objMilesDebitDtl.DebitAmount = Math.Round(MilesAmt, 2);
                                    objMilesDebitDtl.DebitDate = objUserPurchaseHdr.OrderDate;
                                    objMilesDebitDtl.IsActive = true;
                                    objMilesDebitDtl.UserId = objUserPurchaseHdr.UserId;
                                    objUserPurchaseHdr.MilesDebitDtls.Add(objMilesDebitDtl);

                                    List<MilesCreditDtl> UserCreditMiles = context.MilesCreditDtls.Where(x => x.UserId == objUserPurchaseHdr.UserId && (x.CreditAmount - x.UsedCreditAmount) > 0).OrderBy(x => x.MilesCreditDtlId).ToList();
                                    decimal MileDebitRemain = Convert.ToDecimal(MilesAmt);
                                    foreach (var item in UserCreditMiles)
                                    {
                                        if (MileDebitRemain > 0)
                                        {
                                            decimal RemainForUsed = (item.CreditAmount - item.UsedCreditAmount);
                                            if (RemainForUsed < MileDebitRemain)
                                            {
                                                item.UsedCreditAmount = (item.UsedCreditAmount + RemainForUsed);
                                                MileDebitRemain = MileDebitRemain - RemainForUsed;
                                            }
                                            else
                                            {
                                                item.UsedCreditAmount = (item.UsedCreditAmount + MileDebitRemain);
                                                MileDebitRemain = 0;
                                            }
                                        }

                                        if (MileDebitRemain == 0)
                                            break;
                                    }

                                    var objUserMiles = context.UserMilesDtls.Where(x => x.UserId == objUserPurchaseHdr.UserId).FirstOrDefault();
                                    objUserMiles.TotalDebitMiles = objUserMiles.TotalDebitMiles + MilesAmt;
                                }

                                objUserPurchaseHdr.DeliveryCharge = OrdDeliveryCharge;

                                context.UserPurchaseHdrs.Add(objUserPurchaseHdr);

                                #endregion Purchase Orders

                                if (!objUserMst.IsRegistrationActivated.HasValue || !objUserMst.IsRegistrationActivated.Value)
                                {
                                    if (!AdminConfirmationRequired)
                                    {
                                        //Multiple purchase id activation
                                        var RegistrationPoints = objUserMst.RegistrationPoints.HasValue ? objUserMst.RegistrationPoints.Value : 0;
                                        RegistrationPoints += CreditAmount;

                                        if (RegistrationPoints >= MinRegistrationPoints || AllowToPurchaseBelowMinRegistrationPoints)
                                        {
                                            objUserMst.IsRegistrationActivated = true;
                                            objUserMst.ActivateDate = dt_CurrentDatetime;
                                        }
                                        objUserMst.RegistrationPoints = RegistrationPoints;

                                        #region Wallet Credit

                                        if (objReferralUser.RefferalUserId != 0)
                                        {
                                            objManager.InsertWalletCredit(context, objUserPurchaseHdr, objclsLogin.UserId, objUserMst.RefferalUserId, WalletCreditType.Direct);
                                        }

                                        #endregion Wallet Credit
                                    }

                                    if (!objUserMst.IsRegistrationMsgSend.HasValue || (objUserMst.IsRegistrationMsgSend.HasValue && !objUserMst.IsRegistrationMsgSend.Value))
                                    {
                                        if (objUserMst.MobileNo.HasValue)
                                        {
                                            //var Msg = "Congratulation " + objUserMst.FullName + " . You have been successful registered to maxener wellness - financial reward plan .Your user id - " + objUserMst.UserName + " ,Password - " + objUserMst.Password + "(do not share your pass word) to any other person.Complete your profile by loging in - website(Maxener Wellness)";
                                            var Msg = string.Format("Congratulation {0} . You have been successful registered to Maxener Wellness .Your user code - {1} ,user name - {2} ,Passcode - {3} (do not share your pass word) to any other person.Complete your profile by loging in - https://www.maxenerwellness.co.inTeam Maxener Wellness"
                                                        , objUserMst.FullName, objUserMst.RefferalCode, objUserMst.UserName, objUserMst.Password);

                                            objManager.SendSMS(objUserMst.MobileNo, "1107160293313158853", Msg, "New Registration");
                                            objUserMst.IsRegistrationMsgSend = true;
                                        }
                                        if (objReferralUser.MobileNo.HasValue)
                                        {
                                            var Msg = "Congratulations, " + objUserMst.FullName + " successfully registered under your " + (objUserMst.Position == "L" ? "left" : "right") + " side. Team Maxener Wellness";
                                            objManager.SendSMS(objReferralUser.MobileNo, string.Empty, Msg, "Team Joining");
                                            objUserMst.IsRegistrationMsgSend = true;
                                        }
                                    }
                                }
                                context.SaveChanges();
                                transaction.Commit();
                                UserPurchaseHdrId = objUserPurchaseHdr.UserPurchaseHdrId;

                                if (!objclsLogin.RedirectFrmAdmin || !ORDER_GEN_BYADMIN_STOPSMS)
                                {
                                    try
                                    {
                                        var OrderMsg = "Dear  " + objUserMst.FirstName + " : your order is to be received by order number: " + objUserPurchaseHdr.OrderNo + ", it will be deliver soon. Team Maxener";
                                        //objManager.SendSMS(Convert.ToInt64(objUserMst.MobileNo.Value), OrderMsg, "New Order");
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            Logger.Error(string.Format("Func:OrderFailedverifyProcess(PlaceOrderProcess). Error while SaveChanges. RazorpayOrderId: {0}", objUserPurchaseHdrTemp.RazorpayOrderId), ex);
                            return false;
                        }
                    }
                }
                Status = true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Func:OrderFailedverifyProcess(PlaceOrderProcess). RazorpayOrderId: {0}", objUserPurchaseHdrTemp.RazorpayOrderId), ex);
                return false;
            }
            return Status;
        }

        #endregion Order Failed verify Timer


        #region GENERATE INCOME PROCESS"

        /// <summary>
        /// GenerateIncomeProcessTimer_OnElapsedTime
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void GenerateIncomeProcessTimer_OnElapsedTime(object source, ElapsedEventArgs e)
        {
            GenerateIncomeProcessActive();
        }

        /// <summary>
        /// GenerateIncomeProcessActive
        /// </summary>
        public void GenerateIncomeProcessActive()
        {
            if (db.ServiceRunningStatus.AsNoTracking().Any(p => p.ID == (int)clsImplementationEnum.ServiceName.GenerateIncomeProcess && !p.IsActive))
            {
                Logger.Info("Service: GenerateIncomeProcess Inactive");
                return;
            }

            ServiceRunningStatu objServiceRunningStatusValidation = db.ServiceRunningStatus.AsNoTracking().Where(p => p.ID == (int)clsImplementationEnum.ServiceName.GenerateIncomeProcess).FirstOrDefault();
            if (objServiceRunningStatusValidation.RunningStatus.HasValue && objServiceRunningStatusValidation.RunningStatus.Value.Date == DateTime.Now.Date)
                return;

            using (var context = new TRSEntities(3600))
            {
                var objServiceRunningStatus = context.ServiceRunningStatus.Where(p => p.ID == (int)clsImplementationEnum.ServiceName.GenerateIncomeProcess).FirstOrDefault();
                objServiceRunningStatus.RunningStatus = context.PROC_GET_SERVER_DATE().FirstOrDefault();
                context.SaveChanges();
            }

            if (isGenerateIncomeProcessTimerProcessing)
            {
                Logger.Info("Privious GenerateIncomeProcessTimer in processing...");
                return;
            }

            Logger.Info("Start: GenerateIncomeProcessTimer_OnElapsedTime");
            try
            {
                GenerateIncomeProcess();
            }
            catch (Exception ex)
            {
                Logger.Error("Error Exception in GenerateIncomeProcessTimer Service.", ex);
            }
            finally
            {
                Logger.Info("End: GenerateIncomeProcessTimer_OnElapsedTime");
            }
        }

        /// <summary>
        /// PointsCreditProcess
        /// </summary>
        public void GenerateIncomeProcess()
        {
            isGenerateIncomeProcessTimerProcessing = true;
            try
            {
                Logger.Info("Calling GenerateIncomeProcess");
                var dt = DateTime.Now.AddDays(-1);


                Logger.Info("Calling PROC_GENERATE_UPLINE_MONTHLY_INCOME");
                using (var context = new TRSEntities(3600))
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        context.PROC_GENERATE_UPLINE_MONTHLY_INCOME(dt.Month, dt.Year);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                }

                Logger.Info("Calling PROC_GENERATE_RETAIL_REWARD_MONTHLY_INCOME");
                using (var context = new TRSEntities(3600))
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        context.PROC_GENERATE_RETAIL_REWARD_MONTHLY_INCOME(dt.Month, dt.Year);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error("Func:GenerateIncomeProcess.", ex);
            }
            finally
            {
                isGenerateIncomeProcessTimerProcessing = false;
            }
        }

        #endregion

        #region Fastage Miles Credit Timer

        /// <summary>
        /// FastageMilesCreditTimer_OnElapsedTime
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void FastageMilesCreditTimer_OnElapsedTime(object source, ElapsedEventArgs e)
        {
            FastageMilesCreditProcessActive();
        }

        public void FastageMilesCreditProcessActive()
        {
            try
            {
                if (db.ServiceRunningStatus.Any(p => p.ID == (int)clsImplementationEnum.ServiceName.FastageMilesCreditProcess && !p.IsActive))
                {
                    Logger.Info("Service: FastageMilesCreditProcessActive Inactive");
                    return;
                }

                using (var context = new TRSEntities(3600))
                {
                    var objServiceRunningStatus = context.ServiceRunningStatus.Where(p => p.ID == (int)clsImplementationEnum.ServiceName.FastageMilesCreditProcess).FirstOrDefault();
                    objServiceRunningStatus.RunningStatus = context.PROC_GET_SERVER_DATE().FirstOrDefault();
                    context.SaveChanges();
                }

                if (isFastageMilesCreditTimerProcessing)
                {
                    Logger.Info("Privious FastageMilesCredit_Timer in processing...");
                    return;
                }

                Logger.Info("Start: FastageMilesCreditTimer_OnElapsedTime");

                FastageMilesCreditProcess();
            }
            catch (Exception ex)
            {
                Logger.Error("Error Exception in FastageMilesCreditTimer Service.", ex);
            }
            finally
            {
                Logger.Info("End: FastageMilesCreditTimer_OnElapsedTime");
            }
        }

        /// <summary>
        /// FastageMilesCreditProcess
        /// </summary>
        public void FastageMilesCreditProcess()
        {
            isFastageMilesCreditTimerProcessing = true;

            try
            {
                Logger.Info("Calling FastageMilesCreditProcess");
                using (var context = new TRSEntities(3600))
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        context.PROC_INSERT_FASTAGE_ACTIVATION_MILES_EARN(0);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Func:FastageMilesCreditProcess.", ex);
            }
            finally
            {
                isFastageMilesCreditTimerProcessing = false;
            }
        }

        #endregion Fastage Miles Credit Timer

        #region User Miles Lapsed Timer

        /// <summary>
        /// UserMilesLapsedTimer_OnElapsedTime
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void UserMilesLapsedTimer_OnElapsedTime(object source, ElapsedEventArgs e)
        {
            UserMilesLapsedProcessActive();
        }

        public void UserMilesLapsedProcessActive()
        {
            try
            {
                if (db.ServiceRunningStatus.Any(p => p.ID == (int)clsImplementationEnum.ServiceName.UserMilesLapsedProcess && !p.IsActive))
                {
                    Logger.Info("Service: UserMilesLapsedProcessActive Inactive");
                    return;
                }

                using (var context = new TRSEntities(3600))
                {
                    var objServiceRunningStatus = context.ServiceRunningStatus.Where(p => p.ID == (int)clsImplementationEnum.ServiceName.UserMilesLapsedProcess).FirstOrDefault();
                    objServiceRunningStatus.RunningStatus = context.PROC_GET_SERVER_DATE().FirstOrDefault();
                    context.SaveChanges();
                }

                if (isUserMilesLapsedTimerProcessing)
                {
                    Logger.Info("Privious UserMilesLapsed_Timer in processing...");
                    return;
                }

                Logger.Info("Start: UserMilesLapsedTimer_OnElapsedTime");

                UserMilesLapsedProcess();
            }
            catch (Exception ex)
            {
                Logger.Error("Error Exception in UserMilesLapsedTimer Service.", ex);
            }
            finally
            {
                Logger.Info("End: UserMilesLapsedTimer_OnElapsedTime");
            }
        }

        /// <summary>
        /// UserMilesLapsedProcess
        /// </summary>
        public void UserMilesLapsedProcess()
        {
            isUserMilesLapsedTimerProcessing = true;

            try
            {
                Logger.Info("Calling UserMilesLapsedProcess");
                using (var context = new TRSEntities(3600))
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {
                        context.PROC_INSERT_USER_MILES_LAPSED();
                        context.SaveChanges();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Func:UserMilesLapsedProcess.", ex);
            }
            finally
            {
                isUserMilesLapsedTimerProcessing = false;
            }
        }

        #endregion User Miles Lapsed Timer
    }

    public class UserMatchingBVDtl
    {
        public int UserId { get; set; }
        public double MatchingBV { get; set; }
    }
}